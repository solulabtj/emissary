<?php
class AgentJob extends AppModel {
	var $name = 'AgentJob'; 
	var $primaryKey = 'agent_job_id';
	
	public $belongsTo = array(
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
		'AgentUserInfo'=>array('className'=>'AgentUserInfo',
		'foreignKey'=>'agent_id'
		),
		'Job'=>array('className'=>'Job',
		'foreignKey'=>'job_id'
		),
    );
	public $hasMany = array(
		
    );
	
}