<?php
class JobQuestionOption extends AppModel {
	var $name = 'JobQuestionOption'; 
	var $primaryKey = 'job_option_id';
	
	
	public $belongsTo = array(
		'JobQuestion'=>array('className'=>'JobQuestion',
		'foreignKey'=>'job_question_id'
		),
    );
		

}