<?php
class AgentTestAnswer extends AppModel {
	var $useTable = 'agent_test_answers';
	var $name = 'AgentTestAnswer'; 
	var $primaryKey = 'agent_test_answer_id';
	/*public $belongsTo = array(
		'AgentUserInfo'=>array('className'=>'AgentUserInfo',
		'foreignKey'=>'agent_id'
		),
    );
	*/
	
	public $belongsTo = array(
		'TestQuestion'=>array('className'=>'TestQuestion',
		'foreignKey'=>'test_question_id'
		),
		
    );
		

}