<?php
class JobImage extends AppModel {
	var $name = 'JobImage'; 
	var $primaryKey = 'job_image_id';
	
	public $belongsTo = array(
		'Job'=>array('className'=>'Job',
		'foreignKey'=>'job_id'
		),
    );
	
}