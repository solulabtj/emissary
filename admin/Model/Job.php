<?php
class Job extends AppModel {
	var $name = 'Job'; 
	var $primaryKey = 'job_id';
	
	public $belongsTo = array(
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
    );
	public $hasMany = array(
		/*'JobImage'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),*/	
		'AgentJob'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),	
			
    );
	
}