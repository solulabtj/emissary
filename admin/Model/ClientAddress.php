<?php
class ClientAddress extends AppModel {
	var $useTable = 'client_address'; 
	var $primaryKey = 'client_address_id';
	
	public $belongsTo = array(
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
    );
	
	
}