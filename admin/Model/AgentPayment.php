<?php
class AgentPayment extends AppModel {
	var $useTable = 'agent_payments';
	var $primaryKey = 'agent_payment_id';
	public $hasMany = array(
    );
	
	public $belongsTo = array(
		'AgentUserInfo'=>array('className'=>'AgentUserInfo',
		'foreignKey'=>'agent_id'
		),	
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),	
    );
	
		

}