<?php
class ClientImage extends AppModel {
	var $name = 'ClientImage'; 
	var $primaryKey = 'client_image_id';
	
	public $belongsTo = array(
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
    );
		
}