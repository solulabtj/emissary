<?php
class TestQuestion extends AppModel {
	var $name = 'TestQuestion'; 
	var $primaryKey = 'test_question_id';
	public $hasMany = array(
		'TestQuestionOption'=> array(
			'foreignKey' => 'test_question_id',
			'dependent' => false
		),	
		'AgentTestAnswer'=> array(
			'foreignKey' => 'test_question_id',
			'dependent' => false
		),	
    );
	
		

}