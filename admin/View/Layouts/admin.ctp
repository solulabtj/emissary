<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="msapplication-tap-highlight" content="no">
<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
<title>Materialize - Material Design Admin Template</title>


<?php
echo $this->Html->css('materialize');	
echo $this->Html->css('style');	
echo $this->Html->css('style-fullscreen');	
echo $this->Html->css('custom-style');


?>


<link href="<?php echo ADMIN_ROOT ?>js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
<link href="<?php echo ADMIN_ROOT ?>js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
<link href="<?php echo ADMIN_ROOT ?>js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
<link href="<?php echo ADMIN_ROOT ?>css/jquery.datetimepicker.css" type="text/css" rel="stylesheet"/>
<?php

?>


	<?php echo $this->Html->charset(); ?>
	<title>
		<?php //echo $cakeDescription ?>:
		<?php //echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	
			<?php echo $this->element('header');?>
		
        <div id="main"> 
          <!-- START WRAPPER -->
          <div class="wrapper"> 
            
            <!-- //////////////////////////////////////////////////////////////////////////// --> 
            
            <!-- START CONTENT -->
            <section id="content"> 
              
              <!--start container-->
              
                <div class="row">
                <div class="container">
                    <section class="panel"> 
                    <?php echo $this->element('leftmenu');?>
                    <div class="main_content">
						<?php echo $this->fetch('content'); ?>
                        <?php   echo $this->element('footer');    ?>
                    </div>
                    </section>
                </div>
              </div>
              <!--card widgets start--> 
              
              <!--end container--> 
            </section>
            <!-- END CONTENT --> 
            
            <!-- //////////////////////////////////////////////////////////////////////////// --> 
            
          </div>
          <!-- END WRAPPER --> 
          
        </div>
        <!-- END MAIN --> 
		
			
	
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
