<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
    
	function validate_category()
	{
			var error = false;
            
			var job_category_tite = $('#job_category_tite').val();
			
			if(job_category_tite=='')
			{
				$('#error_title').html('Job Category Title is required.');
				error = true;
			}			
			else
			{
				$('#error_title').html('');
			}
			
            if (error) {
                return false;
            }
			
     }
	 
	 
	  function lettersOnly(evt) {
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		  ((evt.which) ? evt.which : 0));
		if (charCode == 32)
			return true;
		if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		  (charCode < 97 || charCode > 122)) {
			return false;
		}
		else
			return true;
	}
	 
</script>


<article>
<header class="darken-1 panel-heading"> <strong>Add Job Category</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                  <form method="post" action="<?php echo ADMIN_ROOT.'jobs/category_add'; ?>" >
					
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Job Category Title</label>
                  <input type="text" class="col s12 m4 validate" name="job_category_title" id="job_category_tite" onkeypress="return lettersOnly(event)" value="">
                  
                   </div>
                   <div id="error_title" class="error"></div>
                   
                  
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_category();" class="btn waves-effect waves-light cyan">Add Job Category</button>
                   </div>
                   </form>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>