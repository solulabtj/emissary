<style>
    .error{
        color: red;
    }

    [type="radio"]:checked + label:after {

        border: 2px solid #127ebd !important;
        background-color: #127ebd !important;
    }

    [type="radio"].with-gap:checked + label:before {
        border: 2px solid #127ebd !important;
    }

</style>
<script>
    $(document).ready(function () {
        $('#last_active').datetimepicker({
            timepicker: false,
            format: 'm/d/Y',
            formatDate: '/m/d/Y',
        });
        
        $('#date_send').datetimepicker({
            timepicker: false,
            format: 'm/d/Y',
            formatDate: '/m/d/Y',
        });
    });
</script>
<script type="text/javascript">
    
     function validate_agent_notification(e)
    {

        var notify_type = 0;
        var error = false;
        var agent_rank = $('#agent_rank').val();
        var last_active = $('#last_active').val();
        var minimum_points = $('#minimum_points').val();
        var test_status = $('#test_status').val();
        var send_now = $('#send_now_a').val();
        var send_later = $('#send_later_a').val();
        var notification_text = $('#notification_text').val();
        $(".notification_type").each(function (oIndex, oValue) {
            if ($(this).is(':checked'))
            {
                notify_type++;
            }
        });


        if (notification_text == 0 || notification_text == '' || notification_text == "NULL")
        {
            $("#notification_text_error").html("Notification text cannot be empty");
            error = true;
        } else
        {
            $("#notification_text_error").html("");
        }

     

        if (notify_type <= 0)
        {
            $("#notify_error").html("Please select notification type.");
            error = true;
        } else
        {
            $("#notify_error").html("");
        }
        if (error) {
            return false;
        }

    }


    function validate_client_notification(e)
    {
        //e.preventDefault();
        var notify_type = 0;
        var error = false;
        var notification_text = $('#notification_text_c').val();        

        if (notification_text == 0 || notification_text == '' || notification_text == "NULL")
        {
            $("#notification_text_error_c").html("Notification text cannot be empty");
            error = true;
        } else
        {
            $("#notification_text_error_c").html("");
        }

       
        if (error) {
            return false;
        }
     

    }

    
    
    $(document).ready(function () {
        $("input[name$='a_c']").click(function () {
            var test = $(this).val();

            $("div.desc").hide();
            $("#notif" + test).show();
        });
    });
</script>
<article>
    <header class="darken-1 panel-heading"> <strong>Notification List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
            <!--<a href="javascript:;" class="fa fa-times"></a>--> 
        </span> </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <?php echo $this->Session->flash(); ?>                      
            <!--<div class="panel-body"><i class="livicon"></i><a href="<?php echo ADMIN_ROOT . 'notifications/notification'; ?>" >Add Notification Settings</a></div>-->
            <div class="s12 m12 24">
                <!-- ui tabs -->
                <div class="s12 m12 l12">
                    <div class="s12 tab-content">
                        <!--<div id="test3" class="s12">
                            <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Notification Text</th>
                                        <th>Agent Rank</th>                            
                                        <th>Minimum Points</th>        
                                        <th>Notification Type</th>
                                        <th>Notification to</th>               
                                        <th>Notification Sent Time</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($listing as $row) { ?>  
                                        <tr>
                                            <td><?php echo $row['Notification']['notification_text']; ?></td>
                                            <td><?php echo $row['Notification']['agent_rank']; ?></td>
                                            <td><?php echo $row['Notification']['minimum_points']; ?></td>
                                            <td>
                                                <?php
                                                if ($row['Notification']['is_email'] == '1' && $row['Notification']['is_push'] == '1') {
                                                    echo "Email " . ',' . "Push Notification";
                                                } else if ($row['Notification']['is_email'] == '1') {
                                                    echo "Email";
                                                } else if ($row['Notification']['is_push'] == '1') {
                                                    echo "Push Notification";
                                                }
                                                ?>
                                            </td>
                                            <td>

                                                <?php
                                                if ($row['Notification']['to_agent'] == '1' && $row['Notification']['to_client'] == '1') {
                                                    echo "Client " . ',' . "Agent";
                                                } else if ($row['Notification']['to_agent'] == '1') {
                                                    echo "Agent";
                                                } else if ($row['Notification']['to_client'] == '1') {
                                                    echo "Client";
                                                }
                                                ?>                            

                                            </td>
                                            <td><?php echo date("m-d-Y", strtotime($row['Notification']['notification_sent_time'])); ?></td>
                                        </tr>
                                     <?php } ?>	
                                </tbody>
                            </table>
                        </div>-->
                        
                        
                        
                        
                        
                        <div id="test5" class="s12">
                            <!--<form method="post" action="">-->
                            <div class="radio_group col s12 m3 custom_radio">
                                <input type="radio" name="a_c" id="agents" checked="checked" value="2">
                                <label for="agents">Agents</label>
                            </div>
                            <div class="radio_group col s12 m3 custom_radio">
                                <input type="radio" name="a_c" id="clients" value="3">
                                <label for="clients">Clients</label>
                            </div>

                            <div id="notif2" class="desc"> <!--Agents-->
                                <form method="post" action="<?php echo ADMIN_ROOT . 'notifications/send_notification'; ?>" >

                                    <div id="notification_error" class="error"></div>

                                    <input type="hidden" name="notification_type" value="1" />
                                    <div class="input_field col s6 m6">
                                        <label for="agent_rank">Select Agent Rank</label>
                                        <select name="agent_rank" id="agent_rank" >
                                            <option value="">Select Rank</option>
                                            <?php
                                            if (isset($ranks) && !empty($ranks)) {
                                                foreach ($ranks as $rank) {
                                                    ?>
                                                    <option value="<?php echo $rank['AgentRank']['rank']; ?>"><?php echo $rank['AgentRank']['rank_title']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>


                                        </select>					                    
                                        <div id="agent_rank_error" class="error"></div>
                                    </div>

                                    <div class="input_field col s6 m6">
                                        <label for="last_active">Last Active</label>                      
                                        <input type="text" name="last_active" id="last_active" value="" readonly="true" />			                    
                                        <div id="last_active_error" class="error"></div>
                                    </div>

                                    <div class="input_field col s6 m6">
                                        <label for="minimum_points">Minimum points</label>
                                        <input type="text" class="" name="minimum_points" id="minimum_points" onkeypress="return onlyNumbers(event);" value="">

                                        <div id="minimum_points_error" class="error"></div>
                                    </div>

                                    <div class="input_field col s6 m6">
                                        <label for="test_status">Test Status</label>	
                                        <select name="test_status" id="test_status" >
                                            <option value="">Select Status</option>
                                            <option value="1">Pass</option>
                                            <option value="0">Fail</option>
                                        </select>
                                        <div id="test_status_error" class="error"></div>
                                    </div>


                                    <div class="input_field col s12 m6"> 
                                        <h5>Send Notifications Via:</h5>
                                        <label class="col s12">
                                            <input type="checkbox" class="validate notification_type" name="email_notify" id="email_notify" value="1" >E-mail
                                        </label>

                                        <label class="col s12">
                                            <input type="checkbox" class="validate notification_type" name="push_notify" id="push_notify" value="1" >Push Notifications
                                        </label>
                                        <div id="notify_error" class="error"></div>
                                    </div> 

                                    <!--<div class="radio_group col s12 m6"> 
                                        <h5>Sending Options:</h5>
                                        <input type="radio" name="send_option" value="1" class="with-gap" id="send_now_a">
                                        <label for="send_now_a">Send Now</label>

                                        <input type="radio" name="send_option" value="0" class="with-gap" id="send_later_a">
                                        <label for="send_later_a">Send Later</label>                      
                                        <input type="text" name="send_time" id="date_send" value="" readonly="true" />
                                        <div id="send_option_error" class="error"></div>
                                    </div>-->
                                    <div class="input_field col s12 m12" style="width: 50%;margin-top: -130px;">
                                        <label for="notification_text">Notification Content</label>					                  
                                        <textarea rows="4" class="materialize-textarea" type="text" name="notification_text" id="notification_text" ></textarea>
                                        <div id="notification_text_error" class="error"></div>
                                    </div>
                                    <div class="input_field col s12 m12">
                                        <button type="submit" onclick="return validate_agent_notification(event);" class="btn waves-effect waves-light cyan">Submit</button>
                                    </div>
                                </form>
                            </div>

                            <div id="notif3" class="desc" style="display: none;"><!--Clients-->
                                <form method="post" action="<?php echo ADMIN_ROOT . 'notifications/send_notification'; ?>" >

                                    <div id="notification_error" class="error"></div>

                                    <input type="hidden" name="notification_type" value="2" />
                                    <div class="radio_group col s12 m12"> 

                                        <div class="col m4">
                                            <input type="radio" name="notify_reason" value="1" class="with-gap" id="111">
                                            <label for="111">Have Last Payment Due</label>
                                        </div>

                                        <div class="col m4">
                                            <input type="radio" name="notify_reason" value="2" class="with-gap" id="222">
                                            <label for="222">Have Ongoing Jobs</label>   
                                        </div>

                                        <div class="col m4">
                                            <input type="radio" name="notify_reason" value="3" class="with-gap" id="333">
                                            <label for="333">Have Not Accepted job Requests (5+)</label>                     
                                        </div>

                                        <div id="notify_reason_error_c" class="error"></div>
                                    </div>

                                    <div class="input_field col s6 m6">
                                        <h5>Job Due</h5>
                                        <label for="jobs_end">Have Jobs ending in</label>
                                        <select name="jobs_ending_in" id="jobs_ending_in_c" >
                                            <option value="0">Select week</option>
                                            <option value="1">1 week</option>
                                            <option value="2">2 week</option>
                                            <option value="3">3 week</option>
                                        </select>
                                        <div id="jobs_end_error_c" class="error"></div>
                                    </div>
                                   <!-- <div class="radio_group col s12 m6"> 
                                        <h5>Sending Options:</h5>
                                        <input type="radio" name="send_option_c" value="1" class="with-gap" id="send_now_c">
                                        <label for="send_now_c">Send Now</label>

                                        <input type="radio" name="send_option_c" value="0" class="with-gap" id="send_later_c">
                                        <label for="send_later_c">Send Later</label>                      
                                        <input type="date" name="send_time" id="date_send" value=""  />
                                        <div id="send_option_error_c" class="error"></div>
                                    </div>-->
                                    <div class="input_field col s12 m12">
                                        <label for="notification_text_c">Notification Content</label>					                  
                                        <textarea rows="4" class="materialize-textarea" type="text" name="notification_text_c" id="notification_text_c" ></textarea>
                                        <div id="notification_text_error_c" class="error"></div>
                                    </div>
                                    <div class="input_field col s12 m12">
                                        <button type="submit" onclick="return validate_client_notification(event);" class="btn waves-effect waves-light cyan">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!--</form>-->
                        </div>                                                                        
                    </div>
                </div>
                <!-- ui tabs end -->
            </div>
        </div>
    </div>
</article>