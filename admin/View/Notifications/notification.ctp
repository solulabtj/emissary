<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
    
	function validate_setting()
		{
			var error = false;					
			var ch_1 = $("input[name='notification_to1']:checkbox");
			var check1 = ch_1.is(":checked");
			
			var ch_2 = $("input[name='notification_to2']:checkbox");
			var check2 = ch_2.is(":checked");
			var email_notify = $("input[name='email_notify']:checkbox");
			var email_notify1 = email_notify.is(":checked");
			
			var push_notify = $("input[name='push_notify']:checkbox");
			var push_notify1 = push_notify.is(":checked");			
			
			var send_option = $('input[name=send_option]:checked').val();
			
			var date_send = $('#date_send').val();
			var notification_text = $('#notification_text').val();
			var agent_rank = $('#agent_rank').val();
			var minimum_points = $('#minimum_points').val();
			//alert(notification_text);
			if(check1=='0' && check2=='0')
			{
				$('#notification_error').html('Select client or agent.');
				error = true;
			}
			else
			{
				$('#notification_error').html('');				
			}
			
			if(email_notify1=='0' && push_notify1=='0')
			{
				$('#notifications_error').html('Select at least one checkbox.');
				error = true;
			}
			else
			{
				$('#notifications_error').html('');				
			}
			
			if(send_option==undefined)
			{
				$('#send_error').html('Select sending option.');
				error = true;
			}
			else if(send_option=='0' && date_send=='')
			{				
				$('#send_error').html('Select date for send later option.');
				error = true;
			}
			else
			{
				$('#send_error').html('');				
			}
			
			if(notification_text=='')
			{
				$('#text_error').html('Notification text is required.');
				error = true;
			}
			else
			{
				$('#text_error').html('');				
			}
			
			if(agent_rank=='')
			{
				$('#agent_error').html('Agent rank is required.');
				error = true;
			}
			else
			{
				$('#agent_error').html('');				
			}
			
			if(minimum_points=='')
			{
				$('#point_error').html('Minimum points is required.');
				error = true;
			}
			else
			{
				$('#point_error').html('');
			}
			
			if(error)
			{
				return false;
			}
			
		}
	 
</script>


<article>
<header class="darken-1 panel-heading"> <strong>Notifications Options</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                  <form method="post" action="<?php echo ADMIN_ROOT.'notifications/notification'; ?>" >
					
                   <label class="col s12 m2">Filter</label>
                   <br/>
                   <div class="input_field col s12 m12">
                  
                  <input type="checkbox" class="col s12 m4 validate" name="notification_to1" id="notification_to1" value="1" >Agents
                  <br/>
                  
                  <input type="checkbox" class="col s12 m4 validate" name="notification_to2" id="notification_to2" value="1" >Clients
                  
                  </div>
                  
                  <div id="notification_error" class="error"></div>
                  
                   <br/>
                  
                  <div class="input_field col s12 m12">
                  	
                    <select name="agent_rank" id="agent_rank" >
                    	
                        <option value="">Select Agent Rank</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    
                    </select>
                    
                    <div id="agent_error" class="error"></div>
                    
                    <br/>
                    
                    <!--<select >
                    	
                        <option value="">Select Country</option>
                        <option value="">1</option>
                    
                    </select>
                    <br/>
                    <select >
                    	
                        <option value="">Select State</option>
                        <option value="">1</option>
                    
                    </select>
                  	<br/>
                    <select >
                    	
                        <option value="">Select City</option>
                        <option value="">1</option>
                    
                    </select>
                  	<br/>-->
                    
                    <select name="minimum_points" id="minimum_points" >
                    	
                        <option value="">Minimum Points</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    
                    </select>
                    <div id="point_error" class="error"></div>
                  	<br/>
                    
                    
                    
                  </div>
                    
                 <label class="col s12 m2">Send Notifications Via:</label>
                 
                 <div class="input_field col s12 m12">
                  
                  <input type="checkbox" class="col s12 m4 validate" name="email_notify" id="email_notify" value="1" >E-mail
                  <br/>
                  
                  <input type="checkbox" class="col s12 m4 validate" name="push_notify" id="push_notify" value="1" >Push Notifications
                  
                  </div>
                  
                  <div id="notifications_error" class="error"></div>
                  
                  <label class="col s12 m2">Sending Options:</label>
                 
                 <div>
                  	<div class="radio_group col s12 m3">
                      <input type="radio" name="send_option" value="1" class="with-gap" id="test48">
                      <label for="test48">Send Now</label>
                    </div>
                    <div class="radio_group col s12 m4">
                      <input type="radio" name="send_option" value="0" class="with-gap" id="test49">
                      <label for="test49">Send Later</label>                      
                      <input type="date" name="date_send" id="date_send" value=""  />
                    </div>
                  </div>
                  
                    <div id="send_error" class="error"></div>
                    
                  <div class="input_field col s12 m12">
                  <label class="col s12 m2">Notification Content</label>
                  
                  <input type="text" name="notification_text" id="notification_text" />
                  <div id="text_error" class="error"></div>
                  </div>
                  
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_setting();" class="btn waves-effect waves-light cyan">Submit</button>
                   </div>
                   </form>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>