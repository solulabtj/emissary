<article>
                <header class="darken-1 panel-heading"> <strong>Add City</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  </span> </header>
                <div class="panel-body"> 
                  <div class="adv-table">                     
                      <div class="s12 m12 24" id="add-new-table">
                        <table class="responsive-table display" cellspacing="0">
                        	<tr>
                            	<td>City Name</td>
                                <td><input name="data[Citie][name]" required type="text" class="validate" onkeypress="return lettersOnly(event)" id="searchTextField" placeholder="Enter a location" autocomplete="off">
      <button id="get_latlon" class="btn btn-primary" type="button">Get Lat Lng</button></td>
                            </tr>
                            <tr>
                            	<td>Country</td>
                                <td><input name="data[Citie][country_id]" disabled="disabled" value="India" type="text" class="span6 typeahead" id="CitieCountryId"></td>
                            </tr>
                            <tr>
                            	<td>Latitude</td>
                                <td><input name="data[Citie][lattitude]" id="lat" required type="text"></td>
                            </tr>
                            <tr>
                            	<td>Longitude</td>
                                <td><input name="data[Citie][longitude]" id="lon" required type="text"></td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                                <td><button type="submit" class="btn btn-round" onclick="history.go(-1)">Add City</button>
                                <button type="button" class="btn btn-round" onclick="history.go(-1)">Cancel</button><br />
<br />
</td>
                            </tr>
                        </table>
                      </div>
                  </div>
                </div>
              </article>