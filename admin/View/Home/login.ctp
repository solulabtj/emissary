<!DOCTYPE html>
<html lang="en">
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <link rel="shortcut icon" href="<?php echo ADMIN_ROOT?>img/favicon.png">
        <title>Lock Screen</title>

        <!-- Bootstrap core CSS -->
        <!--<link href="http://rajatlala.com/mentor/assets/admin/bs3/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://rajatlala.com/mentor/assets/admin/css/bootstrap-reset.css" rel="stylesheet">-->

        <!-- Custom styles for this template -->
        <link href="<?php echo ADMIN_ROOT?>css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="<?php echo ADMIN_ROOT?>css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="<?php echo ADMIN_ROOT?>css/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen,projection">
        <script type="text/javascript" src="<?php echo ADMIN_ROOT?>/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ROOT?>/js/jquery.mousewheel.pack.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ROOT?>/js/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ROOT?>/js/jquery.validate.min.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready(function () {
                $('.fancybox').fancybox({
                    autoSize: true,
                    width: "auto",
                    height: "80%"
                });
                
                $(".close-box").click(function (){
                    $.fancybox.close();
                });
                
            });
        </script>
        </head>

        <body class="lock-screen" onload="startTime()">
<div class="lock-wrapper">
          <div id="time"></div>
          <form role="form" class="form-inline" action="<?php echo ADMIN_ROOT ?>" method="post">
    <div class="lock-box text-center">
              <div>
        <label class="error" style="color:#fff;"> </label>
      </div>
              <img src="<?php echo ADMIN_ROOT?>images/avatar.jpg" alt="lock avatar"/>
              <div class="lock-name"> 
        <!--<input type="email" placeholder="Email" id="exampleInputPassword2" class="form-control lock-input">-->
        <input type="email" name="Email" value="" class="form-control lock-input" id="exampleInputEmail1" placeholder="Email" required  />
      </div>
              <div class="lock-pwd">
        <div class="form-group">
                  <input type="password" name="Password" value="" class="form-control lock-input" id="exampleInputPassword1" placeholder="Password"  />
                  <button class="btn btn-lock" type="submit"><i class="mdi-navigation-arrow-forward"></i></button>
                </div>
      </div>
            </div>
    <a href="#inline1" class="fancybox" style="float:right;" title="Forgot Password?">Forgot Password ?</a>
  </form>
        </div>
<div id="inline1" style="width:400px;display: none;">
          <div class="col-lg-12">
    <section class="panel">
              <header class="panel-heading"> <strong>Forgot Password?</strong> </header>
              <div class="panel-body">
        <div class="form">
                  <form class="cmxform form-horizontal" id="forgotpassword" name="forgotpassword" method="post">
            <div class="form-group ">
                      <label for="vBannerTitle" class="control-label col-lg-3">Email</label>
                      <div class="col-lg-9">
                <input type="text" name="forgot_email" value="" class="form-control" placeholder="Enter email" id="forgot_email"  />
              </div>
                      <div class="form-group">
                <div class="col-lg-offset-3 col-lg-6">
                          <button class="btn btn-3d-success" type="submit">Send</button>
                          <a href="javascript:void(0);" class="btn btn-3d-success close-box" type="button" >Cancel</a><!--onclick="logalertify('admin/country');"--> 
                        </div>
              </div>
                    </div>
          </form>
                </div>
      </div>
            </section>
  </div>
        </div>
<script>
    $(document).ready(function () {
        $('#forgotpassword').validate({
            rules: {
                "forgot_email": {
                    "required": true,
                    "maxlength": 50
                }
            },
            messages: {
                "forgot_email": {
                    "required": "Please enter email address.",
                    "maxlength": "Please enter less than {0} characters."
                }
            },
            submitHandler: function (form) {
                var email = $("#forgot_email").val();
                if (email !== "") {
                    jQuery.ajax({
                        type: "POST",
                        url: "http://rajatlala.com/mentor/vendorlogin/forgotpassword",
                        data: {email: email},
                        success: function (output) {
                            if (output === "mail-sent") {
                                $.fancybox.close();
                            } else {
                                return false;
                            }
                        }
                    });
                }
            }
        });
    });
    function startTime()
    {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
        t = setTimeout(function () {
            startTime()
        }, 500);
    }

    function checkTime(i)
    {
        if (i < 10)
        {
            i = "0" + i;
        }
        return i;
    }
</script>
</body>
</html>