<!-- tinymce -->
<script type="text/javascript" src="<?php echo HTTP_ROOT ?>tinymce/js/tinymce/tinymce.min.js"></script>

<article>
                <header class="darken-1 panel-heading"> <strong>Newsletter Template</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  </span> </header>
                <div class="panel-body"> 
                  <div class="adv-table">                     
                      <div class="s12 m12 24" id="add-new-table">
                        <table class="responsive-table display" cellspacing="0">
                        	<tr>
                            	<td>Name<span class="stred">*</span></td>
                                <td><input name="data[NewsletterTemplate][name]" type="text" size="45" class="validate" onkeypress="return lettersOnly(event)" maxlength="255" id="NewsletterTemplateName"></td>
                            </tr>
                            <tr>
                            	<td>Subject<span class="stred">*</span></td>
                                <td><input name="data[Newsletter][subject]" type="text" size="45" required="required" value="" id="subject" class="validate" onkeypress="return lettersOnly(event)" maxlength="255"></td>
                            </tr>
                            <tr>
                            	<td>Content</td>
                                <td><textarea class="tinymce" rows="15" cols="80" style="width: 100%;"></textarea>
            <span role="application" aria-labelledby="elm1_voice" id="elm1_parent" class="mceEditor defaultSkin"><span class="mceVoiceLabel" style="display:none;" id="elm1_voice">{#aria.rich_text_area}</span></span></td>
                            </tr>
                            
                            <tr>
                            	<td>&nbsp;</td>
                                <td><button type="submit" class="btn btn-round" onclick="history.go(-1)">Save</button>
                                <br />
<br />
</td>
                            </tr>
                        </table>
                        
                      </div>
                  </div>
                </div>
              </article>

<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>			  