<article>
  <header class="darken-1 panel-heading"> <strong>Newsletter Management</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    </span> </header>
  <div class="panel-body"> <a href="<?php echo HTTP_ROOT ?>home/send_newsletter" class="btn btn-round btn-success"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-I15">
    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
    <path fill="#ffffff" stroke="none" d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM24,17.4C24,17.730999999999998,23.731,18,23.4,18H18V23.4C18,23.730999999999998,17.731,24,17.4,24H14.599999999999998C14.267999999999997,24,13.999999999999998,23.731,13.999999999999998,23.4V18H8.6C8.269,18,8,17.731,8,17.4V14.599999999999998C8,14.269,8.269,14,8.6,14H14V8.6C14,8.269,14.269,8,14.6,8H17.4C17.731,8,18,8.269,18,8.6V14H23.4C23.730999999999998,14,24,14.269,24,14.6V17.4Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
    </svg></i> Send Newsletter</a>
    <div class="adv-table">
      <div id="dynamic_table_wrapper" class="dataTables_wrapper" role="grid">
        <div class="s12 m12 24">
          <table id="data-table-simple" class="responsive-table display" cellspacing="0">
          <thead>
                            <tr>
                              <th><input type="checkbox" onclick="checkAll(this)" id="chkAll"></th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Created</th>
                              <th>Action</th>
                              
                            </tr>
                          </thead>
                          <tfoot>
                            <tr>
                              <th><input type="checkbox" onclick="checkAll(this)" id="chkAll"></th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Created</th>
                              <th>Action</th>
                            </tr>
                          </tfoot>
            <tbody>
              
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="1" id="check1" onclick="checkbox()"></td>
                <td valign="top">Subhankar Mitra</td>
                <td valign="top">candeosubhankar@gmail.com </td>
                <td valign="top"> September 4, 2014, 8:17 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="2" id="check2" onclick="checkbox()"></td>
                <td valign="top">Govind Jha</td>
                <td valign="top">candeogovind@gmail.com </td>
                <td valign="top"> September 5, 2014, 2:04 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="3" id="check3" onclick="checkbox()"></td>
                <td bgcolor="" valign="top">Zuriy Live</td>
                <td bgcolor="" valign="top">zuriylive@gmail.com </td>
                <td bgcolor="" valign="top"> September 6, 2014, 4:08 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="4" id="check4" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top">ct@gmail.com </td>
                <td valign="top"> September 11, 2014, 9:13 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="5" id="check5" onclick="checkbox()"></td>
                <td bgcolor="" valign="top"></td>
                <td bgcolor="" valign="top">raj.rr7@gmail.com </td>
                <td bgcolor="" valign="top"> September 11, 2014, 9:15 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="6" id="check6" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top">rajatlala313@gmail.com </td>
                <td valign="top"> September 11, 2014, 9:16 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="7" id="check7" onclick="checkbox()"></td>
                <td bgcolor="" valign="top"></td>
                <td bgcolor="" valign="top">lala.krishna@gmail.com </td>
                <td bgcolor="" valign="top"> October 12, 2014, 8:04 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="8" id="check8" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top">dsfds </td>
                <td valign="top"> October 14, 2014, 11:29 pm </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="9" id="check9" onclick="checkbox()"></td>
                <td bgcolor="" valign="top"></td>
                <td bgcolor="" valign="top">cmt@sds.com </td>
                <td bgcolor="" valign="top"> October 15, 2014, 3:45 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="10" id="check10" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top">cmt@sds.com </td>
                <td valign="top"> October 15, 2014, 3:45 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="11" id="check11" onclick="checkbox()"></td>
                <td bgcolor="" valign="top"></td>
                <td bgcolor="" valign="top">talkwithsoni@gmail.com </td>
                <td bgcolor="" valign="top"> October 15, 2014, 3:46 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="12" id="check12" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top"></td>
                <td valign="top"> October 16, 2014, 1:16 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="13" id="check13" onclick="checkbox()"></td>
                <td bgcolor="" valign="top"></td>
                <td bgcolor="" valign="top">rajat.lala@zuiry.com </td>
                <td bgcolor="" valign="top"> October 16, 2014, 1:17 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="14" id="check14" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top">xfdfdas@asd.xo </td>
                <td valign="top"> October 18, 2014, 10:15 pm </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="15" id="check15" onclick="checkbox()"></td>
                <td bgcolor="" valign="top"></td>
                <td bgcolor="" valign="top">cmthakkar@gmail.com </td>
                <td bgcolor="" valign="top"> November 11, 2014, 3:53 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="16" id="check16" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top">exam1.story1@gmail.com </td>
                <td valign="top"> November 24, 2014, 9:41 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="17" id="check17" onclick="checkbox()"></td>
                <td bgcolor="" valign="top"></td>
                <td bgcolor="" valign="top">cmt@gmail.com </td>
                <td bgcolor="" valign="top"> November 24, 2014, 3:57 pm </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="18" id="check18" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top">aaa@bbb.ccc </td>
                <td valign="top"> November 25, 2014, 8:02 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" bgcolor="" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="19" id="check19" onclick="checkbox()"></td>
                <td bgcolor="" valign="top"></td>
                <td bgcolor="" valign="top">athawaledhaval@yahoo.in </td>
                <td bgcolor="" valign="top"> November 30, 2014, 2:01 pm </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
              <tr>
                <td align="center" valign="top"><input type="checkbox" name="data[NewsletterSubcriber][ids][]" value="20" id="check20" onclick="checkbox()"></td>
                <td valign="top"></td>
                <td valign="top"></td>
                <td valign="top"> December 24, 2014, 11:18 am </td>
                <td valign="top" align="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-content-block" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="" style="cursor:pointer;"><i class="mdi-communication-email" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> </td>
              </tr>
            <input type="hidden" name="data[NewsletterSubcriber][total]" id="total_chk" value="20">
            </tbody>
          </table>
          <div style="padding-top:10px;">
        <div style="float:left; margin-right:15px;"><select name="data[NewsletterSubcriber][action_id]" id="NewsletterSubcriberActionId">
<option value="">Select</option>
<option value="0">Block</option>
<option value="1">Unblock</option>
</select></div>
		<div class="new_footer">
        <input type="submit" class="btn" value="Apply to Selected"></div>
        </div>
        </div>
      </div>
    </div>
  </div>
</article>
