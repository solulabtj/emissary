<article>
  <header class="darken-1 panel-heading"> <strong>Nation/City</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    <!--<a href="javascript:;" class="fa fa-times"></a>--> 
    </span> </header>
  <ul class="collapsible collapsible-accordion" id="slide-out">
    <li class="bold active"><a class="collapsible-header waves-effect waves-cyan active">Search</a>
      <div class="collapsible-body" style="display: block;">
        <form method="get" name="filters" action="">
          <div class="filter">
            <label class="searchLabel">Nation Title</label>
            <input class="form-control searchInput validate" onkeypress="return lettersOnly(event)" type="text" name="data[EmailTemplate][title]" value="">
          </div>
          <div class="filter">
            <label class="searchLabel">Code</label>
            <input class="form-control searchInput" type="text" name="data[EmailTemplate][subject]" value="">
          </div>
          <div class="filter">
            <label class="searchLabel">Status</label>
            <select class="searchInput" name="data[Nation][status]">
              <option selected="selected" value="">Select</option>
              <option value="0">Not Active</option>
              <option value="1">Active</option>
            </select>
          </div>
          <div class="filter">
            <button type="submit" class="btn btn-sm">Filter</button>
          </div>
        </form>
      </div>
    </li>
  </ul>
  <div class="panel-body"> <a href="<?php echo HTTP_ROOT ?>home/add_new_nation" class="btn btn-round btn-success"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-I15">
    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
    <path fill="#ffffff" stroke="none" d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM24,17.4C24,17.730999999999998,23.731,18,23.4,18H18V23.4C18,23.730999999999998,17.731,24,17.4,24H14.599999999999998C14.267999999999997,24,13.999999999999998,23.731,13.999999999999998,23.4V18H8.6C8.269,18,8,17.731,8,17.4V14.599999999999998C8,14.269,8.269,14,8.6,14H14V8.6C14,8.269,14.269,8,14.6,8H17.4C17.731,8,18,8.269,18,8.6V14H23.4C23.730999999999998,14,24,14.269,24,14.6V17.4Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
    </svg></i> Add Nation</a>
    <div class="adv-table">
                    
                      
                      <div class="s12 m12 24">
    <table id="data-table-simple" class="responsive-table display">
      <thead>
        <tr>
          <th><input type="checkbox" id="checkAll"></th>
          <th class="sorting">Title</th>
          <th class="sorting">Code</th>
          <th class="sorting">Time-Zone</th>
          <th class="sorting">Zone</th>
          <th class="sorting">Created</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="248"></td>
          <td>Zimbabwe</td>
          <td>ZW</td>
          <td>Africa/Harare</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="247"></td>
          <td>Zambia</td>
          <td>ZM</td>
          <td>Africa/Lusaka</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a><br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="244"></td>
          <td>Yemen</td>
          <td>YE</td>
          <td>Asia/Aden</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="64"></td>
          <td>Western Sahara</td>
          <td>EH</td>
          <td>Africa/El_Aaiun</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="242"></td>
          <td>Wallis and Futuna</td>
          <td>WF</td>
          <td>Pacific/Wallis</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="240"></td>
          <td>Vietnam</td>
          <td>VN</td>
          <td>Asia/Ho_Chi_Minh</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="237"></td>
          <td>Venezuela</td>
          <td>VE</td>
          <td>America/Caracas</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="235"></td>
          <td>Vatican</td>
          <td>VA</td>
          <td>Europe/Vatican</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="241"></td>
          <td>Vanuatu</td>
          <td>VU</td>
          <td>Pacific/Efate</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
        <tr>
          <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Nation][check][]" value="234"></td>
          <td>Uzbekistan</td>
          <td>UZ</td>
          <td>Asia/Tashkent</td>
          <td>z1</td>
          <td>Nov 30, -0001</td>
          <td><a title="Edit Content" href="<?php echo HTTP_ROOT ?>home/nation_edit" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Delete" href="<?php echo HTTP_ROOT ?>home/" onclick="if(!confirm('Are you sure to delete this Nation?')){return false;}" style="cursor:pointer;"> <i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i> </a> <br></td>
        </tr>
      </tbody>
    </table>
    </div></div>
  </div>
</article>
