<article>
  <header class="darken-1 panel-heading"> <strong>Newsletter Template Management</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    </span> </header>
  <div class="panel-body"> <a href="<?php echo HTTP_ROOT ?>home/add_newsletter" class="btn btn-round btn-success"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-I15">
    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
    <path fill="#ffffff" stroke="none" d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM24,17.4C24,17.730999999999998,23.731,18,23.4,18H18V23.4C18,23.730999999999998,17.731,24,17.4,24H14.599999999999998C14.267999999999997,24,13.999999999999998,23.731,13.999999999999998,23.4V18H8.6C8.269,18,8,17.731,8,17.4V14.599999999999998C8,14.269,8.269,14,8.6,14H14V8.6C14,8.269,14.269,8,14.6,8H17.4C17.731,8,18,8.269,18,8.6V14H23.4C23.730999999999998,14,24,14.269,24,14.6V17.4Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
    </svg></i> Add Template</a>
    <div class="adv-table">
      <div id="dynamic_table_wrapper" class="dataTables_wrapper" role="grid">
        <div class="s12 m12 24">
          <table id="data-table-simple" class="responsive-table display" cellspacing="0">
            <thead>
              <tr>
                <th><input type="checkbox" id="checkAll"></th>
                <th>Name</th>
                <th>Subject</th>
                <th>Created</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th><input type="checkbox" id="checkAll"></th>
                <th>Name</th>
                <th>Subject</th>
                <th>Created</th>
                <th>Action</th>
              </tr>
            </tfoot>
            <tbody>
              <tr>
                <td>Test</td>
                <td>Test</td>
                <td>Test</td>
                <td>Test</td>
                <td>Test</td>
              </tr>
            </tbody>
          </table>
          <div style="padding-top:10px;">
        <div style="float:left; margin-right:15px;"><select name="data[NewsletterSubcriber][action_id]" id="NewsletterSubcriberActionId">
<option value="">Select</option>
<option value="0">Block</option>
<option value="1">Unblock</option>
</select></div>
		<div class="new_footer">
        <input type="submit" class="btn" value="Apply to Selected"></div>
        </div>
        </div>
      </div>
    </div>
  </div>
</article>
