<article>
  <header class="darken-1 panel-heading"> <strong>Cities List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    <!--<a href="javascript:;" class="fa fa-times"></a>--> 
    </span> </header>
  <div class="panel-body"> <a href="<?php echo HTTP_ROOT ?>Home/add_city" class="btn btn-round btn-success"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-I15">
    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
    <path fill="#ffffff" stroke="none" d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM24,17.4C24,17.730999999999998,23.731,18,23.4,18H18V23.4C18,23.730999999999998,17.731,24,17.4,24H14.599999999999998C14.267999999999997,24,13.999999999999998,23.731,13.999999999999998,23.4V18H8.6C8.269,18,8,17.731,8,17.4V14.599999999999998C8,14.269,8.269,14,8.6,14H14V8.6C14,8.269,14.269,8,14.6,8H17.4C17.731,8,18,8.269,18,8.6V14H23.4C23.730999999999998,14,24,14.269,24,14.6V17.4Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
    </svg></i> Add City</a>
    <div class="adv-table">
      <div id="dynamic_table_wrapper" class="dataTables_wrapper" role="grid">
        <div class="s12 m12 24">
          <table id="data-table-simple" class="responsive-table display" role="grid" aria-describedby="cityTable_info">
            <thead>
              <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="cityTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="City: activate to sort column descending">City</th>
                <th class="sorting" tabindex="0" aria-controls="cityTable" rowspan="1" colspan="1" aria-label="Country: activate to sort column ascending">Country</th>
                <th class="sorting" tabindex="0" aria-controls="cityTable" rowspan="1" colspan="1" aria-label="Latitude: activate to sort column ascending">Latitude</th>
                <th class="sorting" tabindex="0" aria-controls="cityTable" rowspan="1" colspan="1" aria-label="Longitude: activate to sort column ascending">Longitude</th>
                <th class="sorting" tabindex="0" aria-controls="cityTable" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending">Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr role="row" class="odd">
                <td class="sorting_1">BANGALORE</td>
                <td>India</td>
                <td>12.953997</td>
                <td>77.630936</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="even">
                <td class="sorting_1">Delhi</td>
                <td>India</td>
                <td>28.645441</td>
                <td>77.090759</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="odd">
                <td class="sorting_1">Indore</td>
                <td>India</td>
                <td>22.719568</td>
                <td>75.857727</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="even">
                <td class="sorting_1">Kolkata</td>
                <td>India</td>
                <td>22.572645</td>
                <td>88.363892</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="odd">
                <td class="sorting_1">Mumbai</td>
                <td>India</td>
                <td>19.075983</td>
                <td>72.877655</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="even">
                <td class="sorting_1">Pune</td>
                <td>India</td>
                <td>18.520430</td>
                <td>73.856743</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="odd">
                <td class="sorting_1">Solapur</td>
                <td>India</td>
                <td>17.659920</td>
                <td>75.906387</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="odd">
                <td class="sorting_1">BANGALORE</td>
                <td>India</td>
                <td>12.953997</td>
                <td>77.630936</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="even">
                <td class="sorting_1">Delhi</td>
                <td>India</td>
                <td>28.645441</td>
                <td>77.090759</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="odd">
                <td class="sorting_1">Indore</td>
                <td>India</td>
                <td>22.719568</td>
                <td>75.857727</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="even">
                <td class="sorting_1">Kolkata</td>
                <td>India</td>
                <td>22.572645</td>
                <td>88.363892</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="odd">
                <td class="sorting_1">Mumbai</td>
                <td>India</td>
                <td>19.075983</td>
                <td>72.877655</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="even">
                <td class="sorting_1">Pune</td>
                <td>India</td>
                <td>18.520430</td>
                <td>73.856743</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
              <tr role="row" class="odd">
                <td class="sorting_1">Solapur</td>
                <td>India</td>
                <td>17.659920</td>
                <td>75.906387</td>
                <td class="center"><span class="actions"><a href="<?php echo HTTP_ROOT ?>home/city_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a></span> <span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</article>
