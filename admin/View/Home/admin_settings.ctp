
<article>
<header class="darken-1 panel-heading"> <strong>Admin Setting</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  <div style="margin-bottom:20px;">
                    <ul class="tabs tab-demo z-depth-1">
                    <li class="tab col s3"><a href="#test1" class="active">Admin Detail</a>
                      </li>
                      <li class="tab col s3"><a href="#test2">Create Sub Admin</a>
                      </li>
                      <li class="tab col s3"><a href="#test3">Sub Admin List</a>
                      </li>
                      <li class="tab col s3"><a href="#test4">Change Password</a>
                      </li>
                    </ul>
                  </div>
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                       <form>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">User Name</label>
                 <input type="text" class="col s12 m4 validate" onkeypress="return lettersOnly(event)" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Email</label>
                 <input type="text" class="col s12 m4 validate" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Mobile</label>
                 <input type="text" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" class="btn waves-effect waves-light cyan">Update User Detail</button>
                   </div>
                   </form>
                    </div>
                    <div id="test2" class="s12">
                      <form>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">User Name</label>
                 <input type="text" class="col s12 m4 validate"  onkeypress="return lettersOnly(event)" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Email</label>
                 <input type="text" class="col s12 m4 validate" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Mobile</label>
                 <input type="text" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Password</label>
                 <input type="password" class="col s12 m4 validate" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Confirm Password</label>
                 <input type="password" class="col s12 m4 validate" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" class="btn waves-effect waves-light cyan">Add Sub Admin</button>
                   </div>
                   </form>
                    </div>
                    <div id="test3" class="s12">
                     <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Ip Address</th>
                            <th>Created By</th>
                            <th>Created at</th>
                             <th>Status</th>
                             <th>Action</th>
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
                      
                        <tr>
                            <td>Alexender</td>
                            <td>Alexender@hotmail.com</td>
                            <td>999-888-7777</td>
                            <td>122.170.159.3</td>
                            <td>Admin</td>
                            <td>7th September, 2015 05:02 PM</td>
                            <td>Active</td>
                            <td><a class="waves-effect waves-light darken-4"href="javascript:void(0)"><i class="mdi-action-delete"></i></a></td>
                        </tr>
                        <tr>
                            <td>Alexender</td>
                            <td>Alexender@hotmail.com</td>
                            <td>999-888-7777</td>
                            <td>122.170.159.3</td>
                            <td>Admin</td>
                            <td>7th September, 2015 05:02 PM</td>
                            <td>Active</td>
                            <td><a class="waves-effect waves-light darken-4"href="javascript:void(0)"><i class="mdi-action-delete"></i></a></td>
                        </tr>
                       </tbody>
                  </table>
                    </div>
                    <div id="test4" class="s12">
                     <form>
                  
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Old Password</label>
                 <input type="password" class="col s12 m4 validate" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">New Password</label>
                 <input type="password" class="col s12 m4 validate" value="">
                   </div>
                   
                    <div class="input_field col s12 m12">
                  <label class="col s12 m2">Confirm Password</label>
                 <input type="password" class="col s12 m4 validate" value="">
                   </div>
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" class="btn waves-effect waves-light cyan">Change Password</button>
                   </div>
                   </form>
                    </div>
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>