<article>
  <header class="darken-1 panel-heading"> <strong>List Promo Code</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    <!--<a href="javascript:;" class="fa fa-times"></a>--> 
    </span> </header>
  <div class="panel-body"> <a href="<?php echo HTTP_ROOT ?>home/add_promocodes" class="btn btn-round btn-success"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-I15">
    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
    <path fill="#ffffff" stroke="none" d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM24,17.4C24,17.730999999999998,23.731,18,23.4,18H18V23.4C18,23.730999999999998,17.731,24,17.4,24H14.599999999999998C14.267999999999997,24,13.999999999999998,23.731,13.999999999999998,23.4V18H8.6C8.269,18,8,17.731,8,17.4V14.599999999999998C8,14.269,8.269,14,8.6,14H14V8.6C14,8.269,14.269,8,14.6,8H17.4C17.731,8,18,8.269,18,8.6V14H23.4C23.730999999999998,14,24,14.269,24,14.6V17.4Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
    </svg></i> Add Promocode</a>
    <div class="adv-table">
      <div id="dynamic_table_wrapper" class="dataTables_wrapper" role="grid">
        <div class="s12 m12 24">
          <div style="overflow:scroll;">
            <table id="data-table-simple" class="responsive-table display promocode-button" role="grid" aria-describedby="promolistTable_info">
              <thead>
                <tr role="row">
                  <th class="sorting_asc" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Code: activate to sort column descending" style="width: 71px;">Code</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Value: activate to sort column ascending" style="width: 40px;">Value</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Disc.(%): activate to sort column ascending" style="width: 33px;">Disc.(%)</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Flat Price: activate to sort column ascending" style="width: 35px;">Flat Price</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Start Date: activate to sort column ascending" style="width: 57px;">Start Date</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="End Date: activate to sort column ascending" style="width: 57px;">End Date</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="City: activate to sort column ascending" style="width: 28px;">City</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="No. of Coupon: activate to sort column ascending" style="width: 54px;">No. of Coupon</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="No. of Used: activate to sort column ascending" style="width: 35px;">No. of Used</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Apply to all Cate.: activate to sort column ascending" style="width: 40px;">Apply to all Cate.</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Apply to all SubCate.: activate to sort column ascending" style="width: 62px;">Apply to all SubCate.</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Apply to all Deal: activate to sort column ascending" style="width: 40px;">Apply to all Deal</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Apply to all Services: activate to sort column ascending" style="width: 58px;">Apply to all Services</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Only for new users: activate to sort column ascending" style="width: 38px;">Only for new users</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Minimum Deal cost applicable: activate to sort column ascending" style="width: 72px;">Minimum Deal cost applicable</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Max discount: activate to sort column ascending" style="width: 61px;">Max discount</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Send Notification: activate to sort column ascending" style="width: 84px;">Send Notification</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 45px;">Status</th>
                  <th class="sorting" tabindex="0" aria-controls="promolistTable" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 46px;">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr role="row" class="odd">
                  <td class="sorting_1">12345</td>
                  <td>0</td>
                  <td>60 %</td>
                  <td>0</td>
                  <td>13th April, 2015 10:10</td>
                  <td>15th May, 2015 09:20</td>
                  <td>Pune</td>
                  <td>10</td>
                  <td></td>
                  <td>No</td>
                  <td>No</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>100</td>
                  <td>999</td>
                  <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">30percent</td>
                  <td>0</td>
                  <td>30 %</td>
                  <td>0</td>
                  <td>17th July, 2015 06:00</td>
                  <td>19th July, 2015 08:00</td>
                  <td>Pune</td>
                  <td>10</td>
                  <td></td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>100</td>
                  <td>0</td>
                 <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Active"><span class="label ">Inactive</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="odd">
                  <td class="sorting_1">ABCD30</td>
                  <td>0</td>
                  <td>30 %</td>
                  <td>0</td>
                  <td>22nd July, 2015 07:00</td>
                  <td>24th July, 2014 07:00</td>
                  <td>Pune</td>
                  <td>5</td>
                  <td></td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>50</td>
                  <td>200</td>
                  <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">ABD20</td>
                  <td>0</td>
                  <td>20 %</td>
                  <td>0</td>
                  <td>7th August, 2015 19:00</td>
                  <td>7th August, 2015 20:00</td>
                  <td></td>
                  <td>10</td>
                  <td>1</td>
                  <td>No</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>No</td>
                  <td>50</td>
                  <td>200</td>
                  <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="odd">
                  <td class="sorting_1">AHD120</td>
                  <td>200</td>
                  <td>0 %</td>
                  <td>0</td>
                  <td>1st January, 2015 23:45</td>
                  <td>1st January, 2016 23:45</td>
                  <td></td>
                  <td>50</td>
                  <td>1</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>20</td>
                  <td>0</td>
                  <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">AHD60</td>
                  <td>0</td>
                  <td>60 %</td>
                  <td>0</td>
                  <td>31st July, 2015 01:00</td>
                  <td>31st December, 2015 12:00</td>
                  <td></td>
                  <td>50</td>
                  <td>2</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>100</td>
                  <td>900</td>
                  <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="odd">
                  <td class="sorting_1">AHM50</td>
                  <td>0</td>
                  <td>50 %</td>
                  <td>0</td>
                  <td>30th July, 2015 09:00</td>
                  <td>30th July, 2015 12:00</td>
                  <td></td>
                  <td>10</td>
                  <td></td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>500</td>
                  <td>500</td>
                  <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">ASH123</td>
                  <td>0</td>
                  <td>50 %</td>
                  <td>0</td>
                  <td>12th August, 2015 02:10</td>
                  <td>20th August, 2015 10:10</td>
                  <td>Pune</td>
                  <td>2</td>
                  <td></td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>100</td>
                  <td>100</td>
                  <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="odd">
                  <td class="sorting_1">AUG1</td>
                  <td>0</td>
                  <td>20 %</td>
                  <td>0</td>
                  <td>1st August, 2015 10:00</td>
                  <td>5th August, 2015 06:00</td>
                  <td>Pune</td>
                  <td>4</td>
                  <td>4</td>
                  <td>No</td>
                  <td>No</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>50</td>
                  <td>100</td>
                 <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">AUG10</td>
                  <td>0</td>
                  <td>25 %</td>
                  <td>0</td>
                  <td>10th August, 2015 19:00</td>
                  <td>10th August, 2015 23:00</td>
                  <td>Pune</td>
                  <td>2</td>
                  <td>2</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>No</td>
                  <td>50</td>
                  <td>100</td>
                  <td class=""><a class="btn btn-success" href="#">Send</a></td>
                  <td class="center"><a href="#" title="Make Deactive"><span class="label label-success">Active</span></a></td>
                  <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>
