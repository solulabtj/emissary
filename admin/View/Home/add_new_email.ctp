<article>
                <header class="darken-1 panel-heading"> <strong>Add New Email</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  </span> </header>
                <div class="panel-body"> 
                  <div class="adv-table">                     
                      <div class="s12 m12 24" id="add-new-table">
                        <table class="responsive-table display" cellspacing="0">
                        	<tr>
                            	<td>Title</td>
                                <td><input name="Name" class="validate" onkeypress="return lettersOnly(event)" type="text" value="" /></td>
                            </tr>
                            <tr>
                            	<td>Subject</td>
                                <td><input name="Position" class="validate" onkeypress="return lettersOnly(event)" type="text" value="" /></td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                                <td><button type="submit" class="btn btn-round" onclick="history.go(-1)">Submit</button>
                                <button type="button" class="btn btn-round" onclick="history.go(-1)">Cancel</button><br />
<br />
</td>
                            </tr>
                        </table>
                      </div>
                  </div>
                </div>
              </article>