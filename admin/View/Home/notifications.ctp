<article>
                <header class="darken-1 panel-heading"> <strong>Notification List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
                <div class="panel-body"> <a href="<?php echo HTTP_ROOT ?>home/send_notification" class="btn btn-round btn-success"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-I15">
                  <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
                  <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                  <path fill="#ffffff" stroke="none" d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM24,17.4C24,17.730999999999998,23.731,18,23.4,18H18V23.4C18,23.730999999999998,17.731,24,17.4,24H14.599999999999998C14.267999999999997,24,13.999999999999998,23.731,13.999999999999998,23.4V18H8.6C8.269,18,8,17.731,8,17.4V14.599999999999998C8,14.269,8.269,14,8.6,14H14V8.6C14,8.269,14.269,8,14.6,8H17.4C17.731,8,18,8.269,18,8.6V14H23.4C23.730999999999998,14,24,14.269,24,14.6V17.4Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                  </svg></i> Send Notification</a>
                  
                  
                  <div class="adv-table">
                    <div id="dynamic_table_wrapper" class="dataTables_wrapper" role="grid">
                      
                      <div class="s12 m12 24">
                        <table id="data-table-simple" class="responsive-table display" role="grid" aria-describedby="tificationTable_info">
                    <thead>
                        <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="tificationTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Text: activate to sort column descending" style="width: 270px;">Text</th><th class="sorting" tabindex="0" aria-controls="tificationTable" rowspan="1" colspan="1" aria-label="Targeted Audiance: activate to sort column ascending" style="width: 66px;">Targeted Audiance</th><th class="sorting" tabindex="0" aria-controls="tificationTable" rowspan="1" colspan="1" aria-label="Send Type: activate to sort column ascending" style="width: 35px;">Send Type</th><th class="sorting" tabindex="0" aria-controls="tificationTable" rowspan="1" colspan="1" aria-label="Notification Cost: activate to sort column ascending" style="width: 84px;">Notification Cost</th><th class="sorting" tabindex="0" aria-controls="tificationTable" rowspan="1" colspan="1" aria-label="Created: activate to sort column ascending" style="width: 56px;">Created</th><th class="sorting" tabindex="0" aria-controls="tificationTable" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 46px;">Action</th></tr>
                    </thead>   
                    <tbody>        
                        <tr role="row" class="odd">
                                <td class="sorting_1">123</td>
                                <td>Merchant, Users</td>
                                <td>SMS, Email, App</td>
                                <td></td>
                                <td>23rd July, 2015 10:10 AM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr><tr role="row" class="even">
                                <td class="sorting_1">another test</td>
                                <td>Users</td>
                                <td>App</td>
                                <td></td>
                                <td>30th July, 2015 04:17 PM</td>
								<td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>                            </tr><tr role="row" class="odd">
                                <td class="sorting_1">Appoiment app notification by paresh</td>
                                <td>Merchant, Users</td>
                                <td>SMS, Email, App</td>
                                <td>12</td>
                                <td>15th December, 2015 04:34 AM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr><tr role="row" class="even">
                                <td class="sorting_1">Appoiment app notification by paresh</td>
                                <td>Merchant, Users</td>
                                <td>SMS, Email, App</td>
                                <td>12</td>
                                <td>18th June, 2015 06:13 AM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr><tr role="row" class="odd">
                                <td class="sorting_1">ashwini makeup</td>
                                <td>Users</td>
                                <td>SMS, Email, App</td>
                                <td></td>
                                <td>31st July, 2015 02:52 PM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr><tr role="row" class="even">
                                <td class="sorting_1">Banner : 80% off on Facials</td>
                                <td>Merchant, Users</td>
                                <td>App</td>
                                <td></td>
                                <td>10th August, 2015 07:07 PM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr><tr role="row" class="odd">
                                <td class="sorting_1">Banner : 80% off on Facials</td>
                                <td>Merchant, Users</td>
                                <td>App</td>
                                <td></td>
                                <td>10th August, 2015 07:08 PM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr><tr role="row" class="even">
                                <td class="sorting_1">Banner : 80% off on Facials</td>
                                <td>Users</td>
                                <td>App</td>
                                <td></td>
                                <td>10th August, 2015 07:30 PM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr><tr role="row" class="odd">
                                <td class="sorting_1">Banner : ABCD 3</td>
                                <td>Users</td>
                                <td>App</td>
                                <td></td>
                                <td>10th August, 2015 07:34 PM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr><tr role="row" class="even">
                                <td class="sorting_1">Banner : ABCD 3</td>
                                <td>Users</td>
                                <td>App</td>
                                <td></td>
                                <td>10th August, 2015 07:41 PM</td>
                                <td class="center"><span class="actions"><a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></span></td>
                            </tr></tbody>
                </table>
                      </div>
                    </div>
                  </div>
                </div>
              </article>