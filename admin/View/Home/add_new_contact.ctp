<article>
                <header class="darken-1 panel-heading"> <strong>Add New Contact</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  </span> </header>
                <div class="panel-body"> 
                  <div class="adv-table">                     
                      <div class="s12 m12 24" id="add-new-table">
                        <table class="responsive-table display" cellspacing="0">
                        	<tr>
                            	<td>Name</td>
                                <td><input name="Name" class="validate" onkeypress="return lettersOnly(event)" type="text" value="" /></td>
                            </tr>
                            <tr>
                            	<td>Email</td>
                                <td><input name="Email" type="text" value="" /></td>
                            </tr>
                            <tr>
                            	<td>Date Added</td>
                                <td><input name="data[start_date]" id="start_date some_class_1" class="validate date-input-css some_class" onkeypress="return onlyDotsandNumbers (event)" required="required" type="text"></td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                                <td><button type="submit" class="btn btn-round" onclick="history.go(-1)">Submit</button>
                                <button type="button" class="btn btn-round" onclick="history.go(-1)">Cancel</button><br />
<br />
</td>
                            </tr>
                        </table>
                      </div>
                  </div>
                </div>
              </article>