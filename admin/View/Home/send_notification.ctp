<article>
                <header class="darken-1 panel-heading"> <strong>Send Notification</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  </span> </header>
                <div class="panel-body"> 
                  <div class="adv-table">                     
                      <div class="s12 m12 24" id="add-new-table">
                        <table class="responsive-table display" cellspacing="0">
                        	<tr>
                            	<td>Notification Text</td>
                                <td><input name="data[Notification][notification_text]" id="notification_text" required="required" class="validate" onkeypress="return lettersOnly(event)" type="text">                                    <div id="notification_text_error" class="error"></div></td>
                            </tr>
                            <tr>
                            	<td>Targeted Audience</td>
                                <td><div class="input select"><input type="hidden" name="data[Notification][notification_targeted_audiance]" value="" id="target">

<div class="checkbox"><input type="checkbox" name="data[Notification][notification_targeted_audiance][]" value="1" id="target1"> &nbsp;Merchant</div>
<div class="checkbox"><input type="checkbox" name="data[Notification][notification_targeted_audiance][]" value="2" id="target2"> &nbsp;Users</div>
</div>	
                                    <div id="target_error" class="error"></div></td>
                            </tr>
                            <tr>
                            	<td>Cities</td>
                                <td><select name="city_id" id="city_id">
								
                                                                            <option value="67">Delhi</option>
                                                                                <option value="70">BANGALORE</option>
                                                                                <option value="81">Kolkata</option>
                                                                                <option value="83">Pune</option>
                                                                                <option value="84">Mumbai</option>
                                                                                <option value="85">Indore</option>
                                                                                <option value="86">Solapur</option>
                                                                        </select></td>
                            </tr>
                            <tr>
                            	<td>Notification Type</td>
                                <td><div class="input select"><input type="hidden" name="data[Notification][notification_send_type]" value="" id="ntype">

<div class="checkbox"><input type="checkbox" name="data[Notification][notification_send_type][]" value="3" id="ntype3"> &nbsp;APP</div>
</div></td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                                <td><button type="submit" class="btn btn-round" onclick="history.go(-1)">Send Notification</button>
                                <br />
<br />
</td>
                            </tr>
                        </table>
                      </div>
                  </div>
                </div>
              </article>