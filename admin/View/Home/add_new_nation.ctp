<article>
  <header class="darken-1 panel-heading"> <strong>Add New Nation</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> </span> </header>
  <div class="panel-body">
    <div class="adv-table">
      <div class="s12 m12 24" id="add-new-table">
        <table class="responsive-table display" cellspacing="0">
          <tr>
            <td>Title</td>
            <td><input name="Name" class="validate" onkeypress="return lettersOnly(event)" type="text" value="" /></td>
          </tr>
          <tr>
            <td>Code</td>
            <td><input name="Code" type="text" value="" /></td>
          </tr>
          <tr>
            <td>Time-Zone</td>
            <td><input name="Time-Zone" type="text" value="" /></td>
          </tr>
          <tr>
            <td>Zone</td>
            <td><!--<select class="form-control searchInput valid" name="data[Nation][zone]">
                <option selected="selected" value="">Select</option>
                <option value="z1" selected="selected">Zone 1</option>
                <option value="z2">Zone 2</option>
              </select>-->
              <select>
              <option value="Selected">Select</option>
              <option value="Zone1">Zone1</option>
              <option value="Zone2">Zone2</option>
              </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><button type="submit" class="btn btn-round" onclick="history.go(-1)">Submit</button>
              <button type="button" class="btn btn-round" onclick="history.go(-1)">Cancel</button>
              <br />
              <br /></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</article>
