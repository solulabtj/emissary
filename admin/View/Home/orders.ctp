<article>
  <header class="darken-1 panel-heading"> <strong>Orders List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    <!--<a href="javascript:;" class="fa fa-times"></a>--> 
    </span> </header>
  <ul class="collapsible collapsible-accordion" id="slide-out">
    <li class="bold active"><a class="collapsible-header waves-effect waves-cyan active">Search</a>
      <div class="collapsible-body" style="display: block;">
        <form method="get" name="filters" action="">
          <div class="filter">
            <label class="searchLabel">Name</label>
            <input class="form-control searchInput validate" onkeypress="return lettersOnly(event)" type="text" name="data[EmailTemplate][title]" value="">
          </div>
          <div class="filter">
            <label class="searchLabel">Email</label>
            <input class="form-control searchInput" type="text" name="data[EmailTemplate][subject]" value="">
          </div>
          <div class="filter">
            <label class="searchLabel">Status</label>
            <select class="searchInput" name="data[Order][active]">
              <option selected="selected" value="">Select</option>
              <option value="0">Inactive</option>
              <option value="1">Active</option>
            </select>
          </div>
          <div class="filter">
            <button type="submit" class="btn btn-sm">Filter</button>
          </div>
        </form>
      </div>
    </li>
  </ul>
  <div class="panel-body">
    <div class="adv-table">
      <div class="s12 m12 24">
      <div style="overflow:scroll;">
	  <table id="data-table-simple" class="responsive-table display">
          <thead>
            <tr>
              <th><input type="checkbox" id="checkAll"></th>
              <th class="sorting">First Name </th>
              <th class="sorting">Last Name </th>
              <th class="sorting">Email </th>
              <th class="sorting">Phone </th>
              <th class="sorting">Order Address </th>
              <th class="sorting">Shipping Address </th>
              <th class="sorting">Weight </th>
              <th class="sorting">Sub Total </th>
              <th class="sorting">Tax </th>
              <th class="sorting">Shipping </th>
              <th class="sorting">Total </th>
              <th class="sorting">Status </th>
              <th class="sorting">Created </th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="113"></td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
              <td>test</td>
            </tr>
            
            
          </tbody>
        </table>
        
        </div>
      </div>
    </div>
  </div>
</article>
