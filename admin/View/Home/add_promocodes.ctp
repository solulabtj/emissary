<article>
  <header class="darken-1 panel-heading"> <strong>Add Promo Code</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    <!--<a href="javascript:;" class="fa fa-times"></a>--> 
    </span> </header>
  <div class="panel-body">
    <div class="adv-table">
      <div id="dynamic_table_wrapper" class="dataTables_wrapper" role="grid">
        <div class="s12 m12 24" id="add-new-table">
          <table class="responsive-table display promocode-checkbox" cellspacing="0">
            <tr>
              <td><label class="control-label" for="typeahead">Promo Code</label></td>
              <td><input name="data[start_date]" id="start_date" required="required" type="text"></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Promo Value</label></td>
              <td><input name="data[end_date]" id="end_date" required="required" type="text"></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Discount Percentage (%)</label></td>
              <td><input name="data[end_date]" id="end_date" required="required" type="text"></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Flat Price</label></td>
              <td><input name="data[end_date]" id="end_date" required="required" type="text"></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Start Date</label></td>
              <td><input name="start_date" class="date-input-css some_class" id="end_date start_date some_class_1" required="required" type="text"></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">End Date</label></td>
              <td><input name="end_date" id="end_date some_class_2" required="required" type="text" class="date-input-css some_class"></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Cities </label></td>
              <td><select name="city_id" id="city_id">
                  <option value="67">Delhi</option>
                  <option value="70">BANGALORE</option>
                  <option value="81">Kolkata</option>
                  <option value="83">Pune</option>
                  <option value="84">Mumbai</option>
                  <option value="85">Indore</option>
                  <option value="86">Solapur</option>
                </select></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Number of Coupons</label></td>
              <td><input name="data[end_date]" id="end_date" required="required" type="text"></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Category</label></td>
              <td><select onChange="get_subcategories(this.value)" name="category" id="category">
        <optgroup label="Is Applicable">
        <option value="all_cat">Applicable to all category</option>
        <!--<option value="not_appli">Not Applicable</option>--> 
        </optgroup>
        <optgroup label="Category List">
        <option value="131">Hair Care</option>
        <option value="132">Skin Care</option>
        <option value="133">Beauty &amp; Make Up</option>
        <option value="134">Nail Art &amp; Tattoo</option>
        <option value="135">Body Care &amp; Spa</option>
        <option value="141">@Home Services</option>
        <option value="144">Combo-Services</option>
        <option value="147">Fitness</option>
        </optgroup>
      </select></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Subcategory</label></td>
              <td><select onChange="get_deal_service(this.value)" name="subcategory" id="subcategory">
        <optgroup label="Is Applicable">
        <option value="all_subcat">Applicable to all Subcategory</option>
        <!--<option value="not_appli">Not Applicable</option>--> 
        </optgroup>
      </select></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Deal</label></td>
              <td><select name="deal" id="deal">
        <optgroup label="Is Applicable">
        <option value="all_deals">Applicable to all deal</option>
        <option value="not_appli">Not Applicable</option>
        </optgroup>
      </select></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Services</label></td>
              <td><select name="service" id="service">
        <optgroup label="Is Applicable">
        <option value="all_services">Applicable to all services</option>
        <option value="not_appli">Not Applicable</option>
        </optgroup>
      </select></td>
            </tr>
            
            <tr>
              <td><label class="control-label" for="typeahead">Min. Deal Offer Cost</label></td>
              <td><div class="input select">
                  <input name="data[Promocode][min_deal_offer_cost]" maxlength="3" width="50%" required type="text" id="PromocodeMinDealOfferCost">
                </div></td>
            </tr>
            <tr>
              <td><label class="control-label" for="typeahead">Max Discount</label></td>
              <td><div class="input select">
                  <input name="data[Promocode][min_deal_offer_cost]" maxlength="3" width="50%" required type="text" id="PromocodeMinDealOfferCost">
                </div></td>
            </tr>
            <tr>
            	<td>&nbsp;</td>
                <td class="ckeckbox"><input type="checkbox" name="data[Promocode][only_for_new_users][]" value="1" id="target1">
          Only for New Users</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><button type="Save" class="btn btn-round" onclick="history.go(-1)">Save</button>
                <button type="button" class="btn btn-round" onclick="history.go(-1)">Cancel</button>
                <br />
                <br /></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</article>
