<article>
  <header class="darken-1 panel-heading"> <strong>Members</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    <!--<a href="javascript:;" class="fa fa-times"></a>--> 
    </span> </header>
  <ul class="collapsible collapsible-accordion" id="slide-out">
    <li class="bold active"><a class="collapsible-header waves-effect waves-cyan active">Search</a>
      <div class="collapsible-body" style="display: block;">
        <form method="get" name="filters" action="">
          <div class="filter">
            <label class="searchLabel">First Name</label>
            <input class="form-control searchInput validate" onkeypress="return lettersOnly(event)" type="text" name="data[EmailTemplate][title]" value="">
          </div>
          <div class="filter">
            <label class="searchLabel">Surname</label>
            <input class="form-control searchInput validate" onkeypress="return lettersOnly(event)" type="text" name="data[EmailTemplate][subject]" value="">
          </div>
          <div class="filter">
            <label class="searchLabel">Email</label>
            <input class="form-control searchInput" type="text" name="data[EmailTemplate][subject]" value="">
          </div>
          <div class="filter">
            <label class="searchLabel">Status</label>
            <select class="form-control searchInput" name="data[Member][status]">
              <option selected="selected" value="">Select</option>
              <option value="0">Not Active</option>
              <option value="1">Active</option>
            </select>
          </div>
          <div class="filter">
            <button type="submit" class="btn btn-sm">Filter</button>
          </div>
        </form>
      </div>
    </li>
  </ul>
  <div class="panel-body"> <a href="<?php echo HTTP_ROOT ?>home/add_new_member" class="btn btn-round btn-success"><i class="livicon" data-s="16" data-n="plus-alt" data-c="#fff" data-hc="0" id="I15" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;" id="canvas-for-I15">
    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc>
    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
    <path fill="#ffffff" stroke="none" d="M16,2C8.268,2,2,8.268,2,16S8.268,30,16,30S30,23.732,30,16S23.732,2,16,2ZM24,17.4C24,17.730999999999998,23.731,18,23.4,18H18V23.4C18,23.730999999999998,17.731,24,17.4,24H14.599999999999998C14.267999999999997,24,13.999999999999998,23.731,13.999999999999998,23.4V18H8.6C8.269,18,8,17.731,8,17.4V14.599999999999998C8,14.269,8.269,14,8.6,14H14V8.6C14,8.269,14.269,8,14.6,8H17.4C17.731,8,18,8.269,18,8.6V14H23.4C23.730999999999998,14,24,14.269,24,14.6V17.4Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
    </svg></i> Add New Email</a>
    <div class="adv-table">
      <div class="s12 m12 24">
        <table id="data-table-simple" class="responsive-table display">
          <thead>
            <tr>
              <th><input type="checkbox" id="checkAll"></th>
              <th class="sorting">First Name</th>
              <th class="sorting">Sur Name</th>
              <th class="sorting">Email Address</th>
              <th class="sorting">Last Login</th>
              <th class="sorting">Date Added</th>
              
              <!--<th class="sorting"><a href="/maheswarDev/admin/Users/members_listing/sort:Member.activation_key/direction:asc">Email Verification</a></th>-->
              
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="113"></td>
              <td>test</td>
              <td>test</td>
              <td>test@test.com</td>
              <td> Not logged in yet </td>
              <td>Aug 06, 2015</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-warning">Pending</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="111"></td>
              <td>test</td>
              <td>test</td>
              <td>arunbhat89@gmail.com</td>
              <td>07/20/2015 06:12:06 AM</td>
              <td>Jul 20, 2015</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-success">Approved</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="84"></td>
              <td>Maheswar</td>
              <td>Mikki</td>
              <td></td>
              <td>06/18/2015 09:05:48 PM</td>
              <td>Nov 30, -0001</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-success">Approved</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="85"></td>
              <td>M</td>
              <td>Mikki</td>
              <td>M2092657893@Google.com</td>
              <td>06/18/2015 09:07:04 PM</td>
              <td>Nov 30, -0001</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-success">Approved</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="86"></td>
              <td>Maheswar</td>
              <td>Mikki</td>
              <td>Maheswar_Mikki625105041@Twitter.com</td>
              <td>06/18/2015 09:09:55 PM</td>
              <td>Nov 30, -0001</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-success">Approved</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="87"></td>
              <td>Chintan</td>
              <td>Thakkar</td>
              <td></td>
              <td>06/18/2015 07:57:10 AM</td>
              <td>Nov 30, -0001</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-success">Approved</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="88"></td>
              <td>Chintan</td>
              <td>Thakkar</td>
              <td></td>
              <td>06/18/2015 07:57:51 AM</td>
              <td>Nov 30, -0001</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-success">Approved</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="89"></td>
              <td>Tejendra</td>
              <td>Gehlot</td>
              <td>Tejendra822394607@Google.com</td>
              <td>06/18/2015 08:00:12 AM</td>
              <td>Nov 30, -0001</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-success">Approved</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="90"></td>
              <td>Chintan</td>
              <td>Thakkar</td>
              <td>cmthakkar@gmail.com</td>
              <td>06/18/2015 05:01:16 PM</td>
              <td>Nov 30, -0001</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-warning">Pending</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" class="checkbox selectCheck" name="data[Member][check][]" value="91"></td>
              <td>Chintan</td>
              <td>Thakkar</td>
              <td></td>
              <td>06/18/2015 08:03:17 AM</td>
              <td>Nov 30, -0001</td>
              <!-- <td>
												                                                 <span class="isApproveMsg label label-success">Approved</span>
                                                	
											</td> -->
              <td><a href="<?php echo HTTP_ROOT ?>home/member_edit" style="cursor:pointer;"><i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i></a> <a title="Deactivate Status" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a title="Change Password" href="<?php echo HTTP_ROOT ?>home/" style="cursor:pointer;"> <i class="mdi-editor-mode-edit" data-s="18" data-n="edit" data-c="#262926" data-hc="0" id="VS1" style="width: 16px; height: 16px;"></i> </a> <a href="#" class="delete-1" style="cursor:pointer;" onClick="return checkDelete(1)"><i class="mdi-action-delete" data-s="18" data-n="" data-c="#262926" data-hc="0" id="DS1" style="width: 16px; height: 16px;"></i></a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</article>
