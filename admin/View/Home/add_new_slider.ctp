<!-- tinymce -->
<script type="text/javascript" src="<?php echo HTTP_ROOT ?>tinymce/js/tinymce/tinymce.min.js"></script>

<article>
  <header class="darken-1 panel-heading"> <strong>Add Content Detail</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
    <!--<a href="javascript:;" class="fa fa-times"></a>--> 
    </span> </header>
  <div class="panel-body">
    <div class="adv-table">
      <div class="s12 m12 24">
        <div class="box-body">
          <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control required" id="title" name="data[HomeContent][title]">
          </div>
          <div class="form-group">
            <label for="image">Image</label>
            <input type="file" id="image" name="image">
            <p class="help-block">Top Banner Image should be 1400x400 pixel.</p>
          </div>
          <div class="form-group">
            <label for="content">Content</label>
            <textarea class="tinymce" rows="15" cols="80" style="width: 100%;"></textarea>
            <span role="application" aria-labelledby="elm1_voice" id="elm1_parent" class="mceEditor defaultSkin"><span class="mceVoiceLabel" style="display:none;" id="elm1_voice">{#aria.rich_text_area}</span></span></div>
        </div>
        

      </div>
      <button type="submit" class="btn btn-round" onclick="history.go(-1)">Submit</button>
                                <button type="button" class="btn btn-round" onclick="history.go(-1)">Cancel</button><br />
    </div>
  </div>
</article>

<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>