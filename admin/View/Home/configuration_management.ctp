<article>
                <header class="darken-1 panel-heading"> <strong>Configuration Management</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  </span> </header>
                <div class="panel-body"> 
                  <div class="adv-table">                     
                      <div class="s12 m12 24" id="add-new-table">
                        <table class="responsive-table display" cellspacing="0">
                        	<tr>
                            	<td>Admin Name*</td>
                                <td><input name="data[AdminSetting][admin_name]" type="text" size="45" maxlength="255" value="Admin" id="AdminSettingAdminName"></td>
                            </tr>
                            <tr>
                            	<td>Admin Email*</td>
                                <td><input name="data[AdminSetting][admin_email]" type="text" size="45" maxlength="255" value="admin@zuriy.com" id="AdminSettingAdminEmail"></td>
                            </tr>
                            <tr>
                            	<td>Information Email*</td>
                                <td><input name="data[AdminSetting][information_email]" type="text" size="45" maxlength="255" value="info@zuriy.com" id="AdminSettingInformationEmail"></td>
                            </tr>
                            <tr>
                            	<td>Site Url*</td>
                                <td><input name="data[AdminSetting][site_address]" type="text" size="45" maxlength="255" value="http://www.zuriy.com" id="AdminSettingSiteAddress"></td>
                            </tr>
                            <tr>
                            	<td>Site Name*</td>
                                <td><input name="data[AdminSetting][site_name]" type="text" size="45" maxlength="255" value="Zuriy" id="AdminSettingSiteName"></td>
                            </tr>
                            <tr>
                            	<td>Site Location*</td>
                                <td><textarea name="data[AdminSetting][site_location]" size="45" cols="30" rows="6" id="AdminSettingSiteLocation">14, Gandhigram Society
Opposite Gandhi Ashram P. O.
Gandhi Ashram
Ahmedabad 380027
Gujarat</textarea></td>
                            </tr>
                            <tr>
                            	<td>Phone*</td>
                                <td><input name="data[AdminSetting][contact_number]" type="text" class="validate" onkeypress="return onlyNumbers(event)" size="45" maxlength="255" value="+91-9427026888" id="AdminSettingContactNumber"></td>
                            </tr>
                            <tr>
                            	<td>Fax*</td>
                                <td><input name="data[AdminSetting][fax_number]" type="text" size="45" maxlength="255" value="" id="AdminSettingFaxNumber"></td>
                            </tr>
                            <tr>
                            	<td>Google plus Client Id*</td>
                                <td><input name="data[AdminSetting][google_plus_client_id]" type="text" size="45" value="1021912501068-sbedj32mu0gjqiq9qrpd5kjfak4jqcaa.apps.googleusercontent.com" id="AdminSettingGooglePlusClientId"></td>
                            </tr>
                            <tr>
                            	<td>Facebook App Id*</td>
                                <td><input name="data[AdminSetting][facebook_app_id]" type="text" size="45" value="301096600083533" id="AdminSettingFacebookAppId"></td>
                            </tr>
                            <tr>
                            	<td>Linkedin Api Key*</td>
                                <td><input name="data[AdminSetting][linkedin_api_key]" type="text" size="45" value="757vc9wllsf7fb" id="AdminSettingLinkedinApiKey"></td>
                            </tr>
                            <tr>
                            	<td>Facebook Link*</td>
                                <td><input name="data[AdminSetting][facebook_link]" type="text" size="45" maxlength="255" value="http://facebook.com/ZuriyLive" id="AdminSettingFacebookLink"></td>
                            </tr>
                            <tr>
                            	<td>Twitter Link*</td>
                                <td><input name="data[AdminSetting][twitter_link]" type="text" size="45" maxlength="255" value="http://twitter.com/ZuriyLive" id="AdminSettingTwitterLink"></td>
                            </tr>
                            <tr>
                            	<td>Google plus Link*</td>
                                <td><input name="data[AdminSetting][google_plus_link]" type="text" size="45" maxlength="255" value="https://plus.google.com/101015855632510923339/about" id="AdminSettingGooglePlusLink"></td>
                            </tr>
                            <tr>
                            	<td>Linkedin Link*</td>
                                <td><input name="data[AdminSetting][linkedin_link]" type="text" size="45" maxlength="255" value="https://www.linkedin.com/company/zuriy" id="AdminSettingLinkedinLink"></td>
                            </tr>
                            <tr>
                            	<td>Pinterest Link*</td>
                                <td><input name="data[AdminSetting][pinterest_link]" type="text" size="45" maxlength="255" value="http://www.pinterest.com/ZuriyLive/" id="AdminSettingPinterestLink"></td>
                            </tr>
                            <tr>
                            	<td>Google Analytics</td>
                                <td><textarea name="data[AdminSetting][google_analytics]" label="" rows="4" size="45" style="height:auto" id="AdminSettingGoogleAnalytics">&lt;script&gt;
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56287592-1', 'auto');
  ga('send', 'pageview');

&lt;/script&gt;</textarea></td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                                <td><button type="submit" class="btn btn-round" onclick="history.go(-1)">Submit</button><br />
<br />
</td>
                            </tr>
                        </table>
                      </div>
                  </div>
                </div>
              </article>