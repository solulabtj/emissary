<!DOCTYPE html>
<html lang="en">
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <link rel="shortcut icon" href="<?php echo ADMIN_ROOT?>img/favicon.png">
        <title>Lock Screen</title>

        <!-- Bootstrap core CSS -->
        <!--<link href="http://rajatlala.com/mentor/assets/admin/bs3/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://rajatlala.com/mentor/assets/admin/css/bootstrap-reset.css" rel="stylesheet">-->

        <!-- Custom styles for this template -->
        <link href="<?php echo ADMIN_ROOT?>css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="<?php echo ADMIN_ROOT?>css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="<?php echo ADMIN_ROOT?>css/jquery.fancybox.css" type="text/css" rel="stylesheet" media="screen,projection">
        <script type="text/javascript" src="<?php echo ADMIN_ROOT?>/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ROOT?>/js/jquery.mousewheel.pack.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ROOT?>/js/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ROOT?>/js/jquery.validate.min.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready(function () {
                $('.fancybox').fancybox({
                    autoSize: true,
                    width: "auto",
                    height: "80%"
                });
                
                $(".close-box").click(function (){
                    $.fancybox.close();
                });
                
            });
        </script>
        </head>

        <body class="lock-screen" onload="startTime()">
<div class="lock-wrapper">
          <div id="time"></div>
          <?php echo $this->Form->create('Admin',array('class'=>'form-horizontal')); ?>
          <!--<form role="form" class="form-inline" action="<?php //echo ADMIN_ROOT ?>admins/login" method="post">-->
    <div class="lock-box text-center">
              <div>
        <label class="error" style="color:#fff;"> <?php echo $this->Session->flash(); ?>
        
        <?php isset($msg)?$msg:'';
			pr($msg); ?>
        
        <div id="new_error" class="error"></div>
        
        <div id="confirm_error" class="error"></div>
        
        <div id="confirm_match" class="error"></div>
         </label>
      </div>
              <img src="<?php echo ADMIN_ROOT?>images/avatar.jpg" alt="lock avatar"/>
              <div class="lock-name"> 
        <!--<input type="email" placeholder="Email" id="exampleInputPassword2" class="form-control lock-input">-->
        <!--<input type="text" name="username" value="" class="form-control lock-input" id="exampleInputEmail1" placeholder="Email" required  />-->
        <?php echo $this->Form->input('new_password',array('type' => 'password','class'=>'form-control lock-input',"placeholder"=>"New Password",'div'=>false,'label'=>false,'id'=>'new_password_id'));?>
      </div>
              <div class="lock-pwd">
        <div class="form-group">
        <?php echo $this->Form->input('confirm_password',array('type' => 'password','class'=>'form-control lock-input',"placeholder"=>"Confirm Password",'div'=>false,'label'=>false,'id'=>'confirm_password_id'));?>
                  <!--<input type="password" name="Password" value="" class="form-control lock-input" id="exampleInputPassword1" placeholder="Password"  />-->
                  
                  <button class="btn btn-lock" onclick="return validate_login();" type="submit"><i class="mdi-navigation-arrow-forward"></i></button>
                </div>
      </div>
            </div>
    <a href="<?php echo ADMIN_ROOT.'admins/login'; ?>" class="fancybox" style="float:right;" title="Login">Login</a>
        </div>
        

<script type="text/javascript" >
	
	function validate_login()
		{
			var new_password = $('#new_password_id').val();
			var confirm_password = $('#confirm_password_id').val();
			var error = false;
			
			if(new_password == '')
			{
				$('#new_error').html('New Password is required.');
				error = true;
			}
			else
			{
				$('#new_error').html('');				
			}
			
			if(confirm_password == '')
			{
				$('#confirm_error').html('Confirm Password is required.');
				error = true;
			}
			else
			{
				$('#confirm_error').html('');				
			}
			
			if(new_password!='' && confirm_password!='')
			{
				if(new_password!=confirm_password)
				{
					$('#confirm_match').html('Password and Confirm Password do not match.');
					error = true;
				}
				else
				{
					$('#confirm_match').html('');
				}
			}
			
			if(error)
			{
				return false;
			}
			
		}
		
		$(document).ready(function () {
		
		
		
    });
    function startTime()
    {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
        t = setTimeout(function () {
            startTime()
        }, 500);
    }

    function checkTime(i)
    {
        if (i < 10)
        {
            i = "0" + i;
        }
        return i;
    }

</script>
</body>
</html>