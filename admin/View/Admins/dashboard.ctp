<?php
//pr($clientList);exit;
  $passCount = $testChart[0][0]['passCount'];
  $failCount = $testChart[0][0]['failCount'];

  //pr($jobChart);exit;
  $completedJobs = $jobChart[0][0]['completedJobs'];
  $inProgress = $jobChart[0][0]['inProgress'];
  $futureJobs = $jobChart[0][0]['futureJobs'];

  $styleArr = array();
  $styleArr['role'] = 'style'; 
  //pr($styleArr);exit;
  $elem = array('Agent Name','Points Earned',$styleArr);
  $agentLead = array();
  array_push($agentLead, $elem);
  
  foreach($leaderboard as $key=>$lead)
  {
    $elemPush = array();

    $agentPoint = (int)$lead[0]['totalPoints'];

    array_push($elemPush, $lead['ui']['agent_name']);
    array_push($elemPush, $agentPoint);
    array_push($elemPush, 'red');
    array_push($agentLead, $elemPush);
  }
  //pr($agentLead);

  $agentLeadJson = json_encode($agentLead,true);
  //echo $agentLeadJson; exit;

?>


<article>

<div class="row">
	<div class="col s8">
	
	<div class="pd300">
	<h4 class="md_title">Recently Registered Companies</h4>
		<table id="" class="responsive-table bordered custom hoverable display" cellspacing="0">
      <thead>
          <tr>
              <th>Company Name</th>
              <th>Email Address</th>
              <th>Mobile Number</th>
              <th>Status</th>
          </tr>
      </thead>               
      <tbody>
        <?php
        foreach($clientList as $client)
        {
          if($client['ClientUserInfo']['is_active'] == 1)
          {
            $clientStatus = 'Active'; 
          }
          else
          {
            $clientStatus = "Inactive";
          }
          ?>
          <tr>
              <td><?php echo $client['ClientUserInfo']['company_name']; ?></td>
              <td><?php echo $client['ClientUserInfo']['email_address']; ?></td>
              <td><?php echo $client['ClientUserInfo']['mobile_number']; ?></td>
              <td><?php echo $clientStatus; ?></td>                   
          </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
    <div class="btn custom-green" style = "float:right; margin-top:10px;">
     <a href="<?php echo ADMIN_ROOT ?>clients/client_view">View All</a>
	  </div>
  </div>
	
	<div class="pd300">
	<h4 class="md_title">New Agents</h4>
		<table id="" class="responsive-table bordered custom hoverable display" cellspacing="0">
      <thead>
          <tr>
              <th>Full Name</th>
              <th>Email Address</th>
              <th>Mobile Number</th>
              <th>Status</th>
          </tr>
      </thead>               
      <tbody>
        <?php
        foreach($agentList as $agent)
        {
          if($agent['AgentUserInfo']['is_active'] == 1)
          {
            $agentStatus = 'Active'; 
          }
          else
          {
            $agentStatus = "Inactive";
          }
          ?>
          <tr>
              <td><?php echo $agent['AgentUserInfo']['agent_name']; ?></td>
              <td><?php echo $agent['AgentUserInfo']['email_address']; ?></td>
              <td><?php echo $agent['AgentUserInfo']['mobile_number']; ?></td>
              <td><?php echo $agentStatus; ?></td>                   
          </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
    <div class="btn custom-green" style = "float:right;margin-top:10px;">
      <a href="<?php echo ADMIN_ROOT ?>agents/agent_view">View All</a>
	  </div>
  </div>
	</div>
	
	<div class="col s4">
	<div class="pd300">
		<h4 class="md_title">What's Happening</h4>
		<ul class="activity_notification">
      <?php
      foreach($displayMsgs as $msg)
      {
		$msgTime ="";  
		
		if($row['AgentUserInfo']['last_login']=="0000-00-00 00:00:00" || $row['AgentUserInfo']['last_login']=="")
		{
        	$msgTime ='';
			$class_active = '';
		}
		else
		{
			$msgTime = date("m-d-Y H:i",strtotime($row['AgentUserInfo']['last_login']));
			$class_active ='mdi-action-schedule';
		}
		
      ?>
			<li>
				<div class="activity">
					<?php echo $msg['AdminDashMsg']['display_msg']; ?>
				</div>
				<span class="activity_time"><i class="<?php echo $class_active ?>"></i><?php echo $msgTime; ?></span>
			</li>
			<?php
      }
      ?>
		</ul>
	</div>
	</div>
	
</div>



<div class="row">


<div id="testResultChart" style="width: 50%; height: 500px; float:right;"></div>


<div id="allJobChart" style="width: 50%; height: 500px;"></div>


<div id="agentLeaderChart" style="width: 50%;"></div>


</div>
</article>

<script type="text/javascript">
$(document).ready(function(e) {
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(testResults);
    google.charts.setOnLoadCallback(allJobs);
    google.charts.setOnLoadCallback(agentLeaders);
});
  
  var passCount = 0;
  var failCount = 0;
  var completedJobs = 0;
  var inProgress = 0;
  var futureJobs = 0;
  var agentLead = '';
 
  passCount = <?php echo $passCount; ?>;
  failCount = <?php echo $failCount; ?>;
  completedJobs = <?php echo $completedJobs; ?>;
  inProgress = <?php echo $inProgress; ?>;
  futureJobs = <?php echo $futureJobs; ?>;
  agentLead = <?php echo $agentLeadJson; ?>;
  
  function testResults() {
     var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Pass',     passCount],
          ['Fail',     failCount]
        ]);

        var options = {
          title: 'All Test Results',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('testResultChart'));
        chart.draw(data, options);
      }

  function allJobs() {
     var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Completed',     completedJobs],
          ['Future',      futureJobs],
          ['In progress',  inProgress],
        ]);

        var options = {
          title: 'All Jobs',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('allJobChart'));
        chart.draw(data, options);
      }

  function agentLeaders() {
      var data = google.visualization.arrayToDataTable(agentLead);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Agent Leaders",
        width: 600,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("agentLeaderChart"));
      chart.draw(view, options);
  }
</script>