<?php 
  //pr($testChart);exit;
  $passCount = $testChart[0][0]['passCount'];
  $failCount = $testChart[0][0]['failCount'];

  //pr($jobChart);exit;
  $completedJobs = $jobChart[0][0]['completedJobs'];
  $inProgress = $jobChart[0][0]['inProgress'];
  $futureJobs = $jobChart[0][0]['futureJobs'];

  $styleArr = array();
  $styleArr['role'] = 'style'; 
  //pr($styleArr);exit;
  $elem = array('Agent Name','Points Earned',$styleArr);
  $agentLead = array();
  array_push($agentLead, $elem);
  
  foreach($leaderboard as $key=>$lead)
  {
    $elemPush = array();

    $agentPoint = (int)$lead[0]['totalPoints'];

    array_push($elemPush, $lead['ui']['agent_name']);
    array_push($elemPush, $agentPoint);
    array_push($elemPush, 'red');
    array_push($agentLead, $elemPush);
  }
  //pr($agentLead);

  $agentLeadJson = json_encode($agentLead,true);
  //echo $agentLeadJson; exit;
  /*$completedJobs = $testChart[0][0]['completedJobs'];
  $inProgress = $testChart[0][0]['inProgress'];
  $futureJobs = $testChart[0][0]['futureJobs'];*/
?>


<article>
  

<div>
<div id="piechart_3d" style="width: 900px; height: 500px;"></div>
</div>

<div>
<div id="piechart_3d_2" style="width: 900px; height: 500px;"></div>
</div>
<div>
<div id="columnchart_values" style="width: 900px; height: 300px;"></div>
</div>
</article>

<script type="text/javascript">
$(document).ready(function(e) {
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(testResults);
	google.charts.setOnLoadCallback(allJobs);
  google.charts.setOnLoadCallback(agentLeaders);
});
	
	var passCount = 0;
  var failCount = 0;
  var completedJobs = 0;
  var inProgress = 0;
  var futureJobs = 0;
  var agentLead = '';
 
  passCount = <?php echo $passCount; ?>;
  failCount = <?php echo $failCount; ?>;
  completedJobs = <?php echo $completedJobs; ?>;
  inProgress = <?php echo $inProgress; ?>;
  futureJobs = <?php echo $futureJobs; ?>;
  agentLead = <?php echo $agentLeadJson; ?>;
  
  function testResults() {
	   var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Pass',     passCount],
          ['Fail',     failCount]
        ]);

        var options = {
          title: 'All Test Results',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }

  function allJobs() {
     var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Completed',     completedJobs],
          ['Future',      futureJobs],
          ['In progress',  inProgress],
        ]);

        var options = {
          title: 'All Jobs',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d_2'));
        chart.draw(data, options);
      }

  function agentLeaders() {
      var data = google.visualization.arrayToDataTable(agentLead);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Agent Leaders",
        width: 600,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
</script>