<?php //pr($ranks);exit;//pr($admin_settings); exit; ?>

<style>
    .error{
        color: red;
    }

    [type="radio"]:checked + label:after {

        border: 2px solid #127ebd !important;
        background-color: #127ebd !important;
    }

    [type="radio"].with-gap:checked + label:before {
        border: 2px solid #127ebd !important;
    }

</style>

<script>

    function validate_update()
    {
        var error = false;
        var username_update = $('#username_update').val();
        var email_update = $('#email_update').val();
        var mobile_update = $('#mobile_update').val();

        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = regex.test(email_update);

        if (username_update == '')
        {
            $('#usernameu_error').html('Username is required.');
            error = true;
        } else
        {
            $('#usernameu_error').html('');
        }

        if (mobile_update == '')
        {
            $('#mobileu_error').html('Mobile No. is required.');
            error = true;
        } else if (mobile_update.length > 25)
        {
            $('#mobileu_error').html('Mobile No must be in 25 digits.');
            error = true;
        } else
        {
            $('#mobileu_error').html('');
        }

        if (email_update == '')
        {
            $('#emailu_error').html('Please Enter email Address.');
            error = true;
        } else if (email == false)
        {
            $('#emailu_error').html('Please provide valid email Address.');
            error = true;
        } else
        {
            $('#emailu_error').html('');
        }

        if (error) {
            return false;
        }
        
        $("#test4").hide();
        $("#test5").hide();

    }

    function validate_subadmin()
    {
        var error = false;
        var username_sub = $('#username_sub').val();
        var email_sub = $('#email_sub').val();
        var mobile_sub = $('#mobile_sub').val();
        var password = $('#c_password').val();
        var confirm_password = $('#cc_password').val();

        var email_sub = $('#email_sub').val();

        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = regex.test(email_sub);

        if (username_sub == '')
        {
            $('#usernames_error').html('Username is required.');
            error = true;
        } else
        {
            $('#usernames_error').html('');
        }

        if (email_sub == '')
        {
            $('#emails_error').html('Email is required.');
            error = true;
        } else
        {
            $('#emails_error').html('');
        }

        if (mobile_sub == '')
        {
            $('#mobiles_error').html('Mobile No. is required.');
            error = true;
        } else if (mobile_sub.length > 10 || mobile_sub.length < 10)
        {
            $('#mobiles_error').html('Mobile No must be in 10 digits.');
            error = true;
        } else
        {
            $('#mobiles_error').html('');
        }

        if (password == '')
        {
            $('#passwords_error').html('Password is required.');
            error = true;
        } else if (password.length < 8)
        {
            $('#passwords_error').html('Password must be 8 character.');
            error = true;
        } else
        {
            $('#passwords_error').html('');
        }

        if (confirm_password == '')
        {
            $('#passwordss_error').html('Confirm Password is required.');
            error = true;
        } else
        {
            if (password != confirm_password)
            {
                $('#confirm_password_error').html('password and confirm password do not match.');
                error = true;
            }

            $('#passwordss_error').html('');
        }

        if (email_sub == '')
        {
            $('#emails_error').html('Please Enter email Address.');
            error = true;
        } else if (email == false)
        {
            $('#emails_error').html('Please provide valid email Address.');
            error = true;
        } else
        {
            $('#emails_error').html('');
        }



        if (error) {
            return false;
        }

    }

    function validate_changepassword()
    {
        var error = false;
        var password_old = $('#password_old').val();
        var password_new = $('#password_new').val();
        var password_new_confirm = $('#password_new_confirm').val();

        if (password_old == '')
        {
            $('#passwordc_error').html('Old password is required.');
            error = true;
        } else
        {
            $('#passwordc_error').html('');
        }

        if (password_new == '')
        {
            $('#passwordcc_error').html('New password is required.');
            error = true;
        } else
        {

            if (password_new.length < 8)
            {
                $('#password_length_error').html('New Password must be 8 characters.');
                error = true;
            }

            $('#passwordcc_error').html('');
        }

        if (password_new_confirm == '')
        {
            $('#passwordccc_error').html('Confirm password is required.');
            error = true;
        } else
        {
            if (password_new != password_new_confirm)
            {
                $('#password_confirm_error').html('password and confirm password do not match.');
                error = true;
            }

            $('#passwordccc_error').html('');
        }


        if (error) {
            return false;
        }
        
        $("#test1").hide();
        $("#test5").hide();

    }

    function delete_subadmin(admin_id)
    {

        var strconfirm = confirm("Are you sure you want to delete this admin account ?");
        if (strconfirm == true)
        {
            window.location = '<?php echo ADMIN_ROOT; ?>admins/delete_subadmin/' + admin_id;
        } else {
            return false;
        }
    }

    function onlyNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }


    function receive_email_change()
    {
        var receive_email = 0;
        if ($('#receive_email').is(':checked'))
        {
            receive_email = 1;
        }
        $.ajax({
            type: "POST",
            url: "<?php echo ADMIN_ROOT . 'admins/receive_email'; ?>",
            data: 'receive_email=' + receive_email,
            success: function (data) {
                alert(data);
                //window.location.reload();
            }
        });
    }

    function validate_settings()
    {
        var error = false;

        var k1 = document.getElementsByName("cost[]");
        var y1 = k1.length;

        //alert(y1);

        var min_package_fee = $('#min_package_fee').val();

        if (min_package_fee == '' || min_package_fee == 0)
        {
            $('#error_fee').html('Minimum Package Fee should be not null or Zero.');
            error = true;
        } else
        {
            $('#error_fee').html('');
        }

        for (var x = 0; x < y1; x++)
        {
            if (k1[x].value == null || k1[x].value == "")
            {
                $('#error_' + x).html('Please fill price.');
                error = true;
            } else
            {
                $('#error_' + x).html('');
            }
        }

        if (error)
        {
            return false;
        }
    }

    function fun_AllowOnlyAmountAndDot(item, evt)
    {
        evt = (evt) ? evt : window.event;

        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode == 46)
        {
            var regex = new RegExp(/\./g)

            var count = $(item).val().match(regex).length;
            if (count > 1)
            {
                return false;
            }

        }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function lettersOnly(evt) {
        evt = (evt) ? evt : event;
        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
        if (charCode == 32)
            return true;
        if (charCode > 31 && (charCode < 65 || charCode > 90) &&
                (charCode < 97 || charCode > 122)) {
            return false;
        } else
            return true;
    }


    function validate_agent_notification(e)
    {

        var notify_type = 0;
        var error = false;
        var agent_rank = $('#agent_rank').val();
        var last_active = $('#last_active').val();
        var minimum_points = $('#minimum_points').val();
        var test_status = $('#test_status').val();
        var send_now = $('#send_now_a').val();
        var send_later = $('#send_later_a').val();
        var notification_text = $('#notification_text').val();
        $(".notification_type").each(function (oIndex, oValue) {
            if ($(this).is(':checked'))
            {
                notify_type++;
            }
        });

        //alert('asdf');
        //alert($("#send_later_a").is(':checked'));
        //return false;
        if (agent_rank == '' || agent_rank == 0 || agent_rank == "NULL")
        {
            $("#agent_rank_error").html("Agent rank cannot be empty");
            error = true;
        } else
        {
            $("#agent_rank_error").html("");
        }

        if (last_active == '' || last_active == 0 || last_active == "NULL")
        {
            $("#last_active_error").html("Last Active cannot be empty");
            error = true;
        } else
        {
            $("#last_active_error").html("");
        }

        if (minimum_points == '' || minimum_points == 0 || minimum_points == "NULL")
        {
            $("#minimum_points_error").html("Minimum points cannot be empty");
            error = true;
        } else
        {
            $("#minimum_points_error").html("");
        }

        if (test_status > 1)
        {
            $("#test_status_error").html("Invalid Value for test status");
            error = true;
        } else
        {
            $("#test_status_error").html("");
        }

        if (notification_text == 0 || notification_text == '' || notification_text == "NULL")
        {
            $("#notification_text_error").html("Notification text cannot be empty");
            error = true;
        } else
        {
            $("#notification_text_error").html("");
        }

        if ($('input[name=send_option]:checked').length <= 0)
        {
            $("#send_option_error").html("Please select notification time.");
            error = true;
        } else
        {
            if ($("#send_later_a").is(':checked'))
            {
                send_time = $("#date_send").val();
                if (send_time == "" || send_time == 0 || send_time == "NULL")
                {
                    $("#send_option_error").html("Invalid value for send time");
                    error = true;
                } else
                {
                    $("#send_option_error").html("");
                }
            } else
            {
                $("#send_option_error").html("");
            }
        }

        if (notify_type <= 0)
        {
            $("#notify_error").html("Please select notification type.");
            error = true;
        } else
        {
            $("#notify_error").html("");
        }
        if (error) {
            return false;
        }
        
        $("#test1").hide();
        $("#test4").hide();
    }


    function validate_client_notification(e)
    {
        e.preventDefault();
        var notify_type = 0;
        var error = false;
        var notification_text = $('#notification_text_c').val();
        var jobs_ending_in = $('#jobs_ending_in_c').val();

        if ($('input[name=notify_reason]:checked').length <= 0)
        {
            $("#notify_reason_error_c").html("Please select notification purpose");
            error = true;
        } else
        {
            $("#notify_reason_error_c").html("");
        }

        if (notification_text == 0 || notification_text == '' || notification_text == "NULL")
        {
            $("#notification_text_error_c").html("Notification text cannot be empty");
            error = true;
        } else
        {
            $("#notification_text_error_c").html("");
        }

        if ($('input[name=send_option_c]:checked').length <= 0)
        {
            $("#send_option_error_c").html("Please select notification time.");
            error = true;
        } else
        {
            //alert($("#send_later_c").is(':checked'));
            if ($("#send_later_c").is(':checked'))
            {
                send_time = $("#date_send_c").val();
                if (send_time == "" || send_time == 0 || send_time == "NULL")
                {
                    $("#send_option_error_c").html("Invalid value for send time");
                    error = true;
                } else
                {
                    $("#send_option_error_c").html("");
                }
            } else
            {
                $("#send_option_error_c").html("");
            }
        }



        if (jobs_ending_in == '' || jobs_ending_in == 0 || jobs_ending_in == "NULL")
        {
            $("#jobs_end_error").html("Agent rank cannot be empty");
            error = true;
        } else
        {
            $("#jobs_end_error").html("");
        }

        if (error) {
            return false;
        }
        e.preventDefault();

    }


    $(document).ready(function () {
        $('#last_active').datetimepicker({
            timepicker: false,
            format: 'm/d/Y',
            formatDate: '/m/d/Y',
        });
        
        $('#date_send').datetimepicker({
            timepicker: false,
            format: 'm/d/Y',
            formatDate: '/m/d/Y',
        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("input[name$='a_c']").click(function () {
            var test = $(this).val();

            $("div.desc").hide();
            $("#notif" + test).show();
        });
    });
</script>

<?php //pr($subadmin_list);  ?>

<article>
    <header class="darken-1 panel-heading"> <strong>Admin Settings</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
            <!--<a href="javascript:;" class="fa fa-times"></a>--> 
        </span> </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
<?php echo $this->Session->flash(); ?>
            <div class="s12 m12 24">
                <!-- ui tabs -->

<?php //pr($admin_settings);  ?>

                <div class="s12 m12 l12">
                    <div style="margin-bottom:20px;">
                        <ul class="tabs tab-demo z-depth-1">
                            <li class="tab col s3"><a href="#test1" class="active">Admin Detail</a>
                            </li>

<?php /* ?><?php if($admin_settings['Admin']['admin_type']=='1') { ?>
  <li class="tab col s3"><a href="#test2">Create Sub Admin</a>
  </li>
  <li class="tab col s3"><a href="#test3">Sub Admin List</a>
  </li>
  <?php } ?>
  <?php */ ?>

                            <li class="tab col s3"><a href="#test4">Change Password</a></li>
                            <li class="tab col s3"><a href="#test5">Notifications</a></li>
                            <!--<li class="tab col s3"><a href="#test6">Manage Packages</a></li>-->
                        </ul>
                    </div>

                    <div class="s12 tab-content">


                        <div id="test1" class="s12">
                            <form method="post" action="<?php echo ADMIN_ROOT . 'admins/profile'; ?>" >

                                <input type="hidden" name="admin_id" value="<?php echo $admin_settings['Admin']['admin_id']; ?>" >

                                <div class="input_field col s12 m12">
                                    <label class="col s12 m2">User Name</label>
                                    <input type="text" class="col s12 m4 validate" name="username" id="username_update" onkeypress="return lettersOnly(event)" value="<?php echo $admin_settings['Admin']['username']; ?>">
                                </div>
                                <div id="usernameu_error" class="error"></div>

                                <div class="input_field col s12 m12">
                                    <label class="col s12 m2">Email</label>
                                    <input type="email" name="email" class="col s12 m4 validate" id="email_update"  value="<?php echo $admin_settings['Admin']['email_address']; ?>">
                                </div>
                                <div id="emailu_error" class="error"></div>

                                <div class="input_field col s12 m12">
                                    <label class="col s12 m2">Mobile</label>
                                    <input type="text" name="mobile_no" id="mobile_update" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="<?php echo $admin_settings['Admin']['mobile_number']; ?>">
                                </div>
                                <div id="mobileu_error" class="error"></div>

                                <div class="input_field col s12 m12">
                                    <label class="col s12 m2"></label>
                                    <button type="submit" onclick="return validate_update();" class="btn waves-effect waves-light cyan">Update User Detail</button>
                                </div>
                            </form>
                        </div>

                        <div id="test4" class="s12">
                            <form method="post" action="<?php echo ADMIN_ROOT . 'admins/changepassword'; ?>" >
                                <div class=" col s6 m6">
                                    <div class="input_field col s12 m12">
                                        <label>Old Password</label>
                                        <input type="password" name="password_old" id="password_old" required class="validate" value="">
                                    </div>
                                    <div id="passwordc_error" class="error"></div>


                                    <div class="input_field col s12 m12">
                                        <label>New Password</label>
                                        <input type="password" name="password_new" id="password_new" required class="validate" value="">
                                    </div>
                                    <div id="passwordcc_error" class="error"></div>


                                    <div class="input_field col s12 m12">
                                        <label class="col">Confirm Password</label>
                                        <input type="password" name="password_new_confirm" id="password_new_confirm" required class="validate" value="">
                                    </div>

                                    <div id="passwordccc_error" class="error"></div>

                                    <div id="password_length_error" class="error"></div>
                                    <div id="password_confirm_error" class="error"></div>
                                </div>


                                <div class=" col s6 m6">
                                    <h4>Receive Notification Via</h4><br/>
                                    <lable><input type="checkbox" name="receive_email" id="receive_email" value="1"<?php if ($admin_settings['Admin']['receive_email'] == 1) {
    echo "checked='checked'";
} ?> onclick="receive_email_change();" > E-mail</lable><br>
                                    <!--<lable><input type="checkbox" name="receive_push"  id="receive_push" value="1"<?php if ($agent_settings['AgentUserInfo']['receive_push'] == '1') {
    echo "checked='checked'";
} ?> onclick="receive_push_change();" > Push Notifications</lable>-->
                                </div>

                                <div class="input_field col s12 m12">
                                    <label class="col s12 m2"></label>
                                    <button type="submit" onclick="return validate_changepassword();" class="btn waves-effect waves-light cyan">Change Password</button>
                                </div>
                            </form>

                            <div class="clearfix"></div>

                        </div>



                        <div id="test5" class="s12">
                            <!--<form method="post" action="">-->
                            <div class="radio_group col s12 m3 custom_radio">
                                <input type="radio" name="a_c" id="agents" checked="checked" value="2">
                                <label for="agents">Agents</label>
                            </div>
                            <div class="radio_group col s12 m3 custom_radio">
                                <input type="radio" name="a_c" id="clients" value="3">
                                <label for="clients">Clients</label>
                            </div>

                            <div id="notif2" class="desc"> <!--Agents-->
                                <form method="post" action="<?php echo ADMIN_ROOT . 'admins/send_notification'; ?>" >

                                    <div id="notification_error" class="error"></div>

                                    <input type="hidden" name="notification_type" value="1" />
                                    <div class="input_field col s6 m6">
                                        <label for="agent_rank">Select Agent Rank</label>
                                        <select name="agent_rank" id="agent_rank" >
<?php
if (isset($ranks) && !empty($ranks)) {
    foreach ($ranks as $rank) {
        ?>
                                                    <option value="<?php echo $rank['AgentRank']['rank']; ?>"><?php echo $rank['AgentRank']['rank_title']; ?></option>
        <?php
    }
}
?>


                                        </select>					                    
                                        <div id="agent_rank_error" class="error"></div>
                                    </div>

                                    <div class="input_field col s6 m6">
                                        <label for="last_active">Last Active</label>                      
                                        <input type="text" name="last_active" id="last_active" value="" readonly="true" />			                    
                                        <div id="last_active_error" class="error"></div>
                                    </div>

                                    <div class="input_field col s6 m6">
                                        <label for="minimum_points">Minimum points</label>
                                        <input type="text" class="" name="minimum_points" id="minimum_points" onkeypress="return onlyNumbers(event);" value="">

                                        <div id="minimum_points_error" class="error"></div>
                                    </div>

                                    <div class="input_field col s6 m6">
                                        <label for="test_status">Test Status</label>	
                                        <select name="test_status" id="test_status" >
                                            <option value="1">Pass</option>
                                            <option value="0">Fail</option>
                                        </select>
                                        <div id="test_status_error" class="error"></div>
                                    </div>


                                    <div class="input_field col s12 m6"> 
                                        <h5>Send Notifications Via:</h5>
                                        <label class="col s12">
                                            <input type="checkbox" class="validate notification_type" name="email_notify" id="email_notify" value="1" >E-mail
                                        </label>

                                        <label class="col s12">
                                            <input type="checkbox" class="validate notification_type" name="push_notify" id="push_notify" value="1" >Push Notifications
                                        </label>
                                        <div id="notify_error" class="error"></div>
                                    </div> 

                                    <div class="radio_group col s12 m6"> 
                                        <h5>Sending Options:</h5>
                                        <input type="radio" name="send_option" value="1" class="with-gap" id="send_now_a">
                                        <label for="send_now_a">Send Now</label>

                                        <input type="radio" name="send_option" value="0" class="with-gap" id="send_later_a">
                                        <label for="send_later_a">Send Later</label>                      
                                        <input type="text" name="send_time" id="date_send" value="" readonly="true" />
                                        <div id="send_option_error" class="error"></div>
                                    </div>





                                    <div class="input_field col s12 m12">
                                        <label for="notification_text">Notification Content</label>					                  
                                        <textarea rows="4" class="materialize-textarea" type="text" name="notification_text" id="notification_text" ></textarea>
                                        <div id="notification_text_error" class="error"></div>
                                    </div>


                                    <div class="input_field col s12 m12">

                                        <button type="submit" onclick="return validate_agent_notification(event);" class="btn waves-effect waves-light cyan">Submit</button>
                                    </div>
                                </form>
                            </div>

                            <div id="notif3" class="desc" style="display: none;"><!--Clients-->
                                <form method="post" action="<?php echo ADMIN_ROOT . 'admins/send_notification'; ?>" >

                                    <div id="notification_error" class="error"></div>

                                    <input type="hidden" name="notification_type" value="2" />
                                    <div class="radio_group col s12 m12"> 

                                        <div class="col m4">
                                            <input type="radio" name="notify_reason" value="1" class="with-gap" id="111">
                                            <label for="111">Have Last Payment Due</label>
                                        </div>

                                        <div class="col m4">
                                            <input type="radio" name="notify_reason" value="2" class="with-gap" id="222">
                                            <label for="222">Have Ongoing Jobs</label>   
                                        </div>

                                        <div class="col m4">
                                            <input type="radio" name="notify_reason" value="3" class="with-gap" id="333">
                                            <label for="333">Have Not Accepted job Requests (5+)</label>                     
                                        </div>

                                        <div id="notify_reason_error_c" class="error"></div>
                                    </div>




                                    <div class="input_field col s6 m6">
                                        <h5>Job Due</h5>
                                        <label for="jobs_end">Have Jobs ending in</label>
                                        <select name="jobs_ending_in" id="jobs_ending_in_c" >
                                            <option value="1">1 week</option>
                                            <option value="2">2 week</option>
                                            <option value="3">3 week</option>
                                        </select>
                                        <div id="jobs_end_error_c" class="error"></div>
                                    </div>




                                    <div class="radio_group col s12 m6"> 
                                        <h5>Sending Options:</h5>
                                        <input type="radio" name="send_option_c" value="1" class="with-gap" id="send_now_c">
                                        <label for="send_now_c">Send Now</label>

                                        <input type="radio" name="send_option_c" value="0" class="with-gap" id="send_later_c">
                                        <label for="send_later_c">Send Later</label>                      
                                        <input type="date" name="send_time" id="date_send" value=""  />
                                        <div id="send_option_error_c" class="error"></div>
                                    </div>


                                    <div class="input_field col s12 m12">
                                        <label for="notification_text_c">Notification Content</label>					                  
                                        <textarea rows="4" class="materialize-textarea" type="text" name="notification_text" id="notification_text_c" ></textarea>
                                        <div id="notification_text_error_c" class="error"></div>
                                    </div>


                                    <div class="input_field col s12 m12">

                                        <button type="submit" onclick="return validate_client_notification(event);" class="btn waves-effect waves-light cyan">Submit</button>
                                    </div>
                                </form>
                            </div>

                            <!--</form>-->

                        </div>



                        <!--<div id="test6" class="s12">
                            <form method="post" action="<?php echo ADMIN_ROOT . 'admins/manage_package'; ?>" >
                                                    <div class="input_field col s12 m12">
<?php
if (isset($min_package) && !empty($min_package)) {

    $package_value = $min_package['MinPackage']['min_package_fee'];
    $package_id = $min_package['MinPackage']['min_package_id'];
} else {
    $package_value = 0;
    $package_id = 0;
}
?>
    
                                                    <input type="hidden" name="type" value="2" />
                                                    <input type="hidden" name="min_package_id" value="<?php echo $package_id; ?>" />
    
                                                    <label class="col s12 m2">Minimum Processing Fee</label>
                                                    <input type="text" class="col s12 m4 validate" name="min_package_fee" id="min_package_fee" onkeypress="return fun_AllowOnlyAmountAndDot(this,event);" value="<?php echo $package_value; ?>">
    
                                                    <label class="col s12 m2" >KD</label>
    
                                                    </div> 
                                                    <div id="error_fee" class="error"></div>
    
    
                                                    <h5>Standard per Survey Processing Fee</h5>
    
                                                    <table id="" class="responsive-table display" cellspacing="0">
                                                     
                                                    <thead>
                                                            <tr>
                                                                    <th>Average Survey Volume</th>
                                                            <th>Price</th>
                                                                            
                                                        </tr>
                                                    </thead>
    
                                                    <tfoot>
                                                     </tfoot>
    
                                                    <tbody>
                                                      
<?php if (isset($package_details) && !empty($package_details)) {
    ?>
        
    <?php $i = 0;
    foreach ($package_details as $package_detail) {
        ?>
                                                                    
                                                                <tr>
                                                                
                                                                <input type="hidden" name="package_detail_id[<?php echo $i ?>]" value="<?php echo $package_detail['PackageDetail']['package_detail_id']; ?>" />
                                                                
                                                                    <td><label class="col s12 m2"><?php echo $package_detail['PackageDetail']['label_name']; ?></label></td>
                                                                    <input type="hidden" name="label_name[]" value="<?php echo $package_detail['PackageDetail']['label_name']; ?>">
                                                                    
                                                                    <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="<?php echo $package_detail['PackageDetail']['cost']; ?>" 
                                                                    onkeypress="return fun_AllowOnlyAmountAndDot(this,event);" /><label class="col s12 m2">KD</label>
                                                                    
                                                                    <div class="error" id="error_<?php echo $i ?>" ></div> 
                                                                    </td>                           
                                                                   
                                                                 
                                                                </tr>
                                                                
        <?php $i++;
    }
} else { ?>
                                                          
                                                          
                                                          
                                                            <tr>
                                                            
                                                            
                                                                <td>
                                                                <label class="col s12 m2">1,000 or Less</label>
                                                                <input type="hidden" name="label_name[]" value="1,000 or Less">
                                                                <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this,event);" /><label class="col s12 m2">KD</label>
                                                                
                                                                <div class="error" id="error_0" ></div> 
                                                                </td>                           
                                                               
                                                             
                                                            </tr>
                                                            <tr>
                                                                <td><label class="col s12 m2">1,001–3,000</label></td>
                                                                <input type="hidden" name="label_name[]" value="1,001-3,000">
                                                                <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this,event);" /><label class="col s12 m2">KD</label>
                                                                
                                                                <div class="error" id="error_1" ></div> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><label class="col s12 m2">3,001-5,000</label></td>
                                                                <input type="hidden" name="label_name[]" value="3,001-5,000">
                                                                <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this,event);" /><label class="col s12 m2">KD</label>
                                                                <div class="error" id="error_2" ></div> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><label class="col s12 m2">5,001-8,000</label></td>
                                                                <input type="hidden" name="label_name[]" value="5,001-8,000">
                                                                <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this,event);" /><label class="col s12 m2">KD</label>
                                                                
                                                                <div class="error" id="error_3" ></div> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><label class="col s12 m2">8,001-10,000</label></td>
                                                                <input type="hidden" name="label_name[]" value="8,001-10,000">
                                                                <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this,event);" /><label class="col s12 m2">KD</label>
                                                                <div class="error" id="error_4" ></div> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><label class="col s12 m2">10,001 or More</label></td>
                                                                <input type="hidden" name="label_name[]" value="10,001 or More">
                                                                <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this,event);" /><label class="col s12 m2">KD</label>
                                                                
                                                                <div class="error" id="error_5" ></div> 
                                                                </td>
                                                            </tr>
                                                            
<?php } ?>  			
    
                                                       </tbody>
                                                    </table>
    
    
                                                    <div class="input_field col s12 m12">
                                                    <label class="col s12 m2"></label>
                                                    <button type="submit" onclick="return validate_settings();" class="btn waves-effect waves-light cyan">Save Package</button>
    
    
                                                    </div>
    
                                                    </form> 
                        </div>-->


<?php /* if($admin_settings['Admin']['admin_type']=='1') { ?>
  <div id="test2" class="s12">
  <form method="post" action="<?php echo ADMIN_ROOT.'admins/create_subadmin'; ?>" >
  <div class="input_field col s12 m12">
  <label class="col s12 m2">User Name</label>
  <input type="text" name="username" id="username_sub" class="col s12 m4 validate"  onkeypress="return lettersOnly(event)" value="" required >
  </div>
  <div id="usernames_error" class="error"></div>

  <div class="input_field col s12 m12">
  <label class="col s12 m2">Email</label>
  <input type="email" name="email" id="email_sub" class="col s12 m4 validate" value="" required >
  </div>
  <div id="emails_error" class="error"></div>

  <div class="input_field col s12 m12">
  <label class="col s12 m2">Mobile</label>
  <input type="text" name="mobile_no" id="mobile_sub" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="" required >
  </div>
  <div id="mobiles_error" class="error"></div>

  <div class="input_field col s12 m12">
  <label class="col s12 m2">Password</label>
  <input type="password" id="c_password" name="c_password" class="col s12 m4 validate" value="" required >
  </div>
  <div id="passwords_error" class="error"></div>

  <div class="input_field col s12 m12">
  <label class="col s12 m2">Confirm Password</label>
  <input type="password" id="cc_password" name="cc_password"  class="col s12 m4 validate" value="" required >
  </div>
  <div id="passwordss_error" class="error"></div>

  <div id="confirm_password_error" class="error"></div>
  <div id="confirm_password_length" class="error"></div>
  <div class="input_field col s12 m12">
  <label class="col s12 m2"></label>
  <button type="submit" onclick="return validate_subadmin();" class="btn waves-effect waves-light cyan">Add Sub Admin</button>
  </div>
  </form>
  </div>



  <div id="test3" class="s12">
  <table id="data-table-simple" class="responsive-table display" cellspacing="0">
  <thead>
  <tr>
  <th>User Name</th>
  <th>Email</th>
  <th>Mobile</th>

  <th>Created By</th>
  <th>Created at</th>
  <th>Status</th>
  <th>Action</th>
  </tr>
  </thead>

  <tfoot>
  </tfoot>

  <tbody>

  <?php foreach($subadmin_list as $row) { ?>
  <tr>
  <td><?php echo $row['Admin']['username']; ?></td>
  <td><?php echo $row['Admin']['email_address']; ?></td>
  <td><?php echo $row['Admin']['mobile_number']; ?></td>

  <td><?php echo $_SESSION['Auth']['User']['username']; ?></td>
  <td><?php echo date("jS F, Y h:i A",strtotime($row['Admin']['created'])); ?></td>
  <td class="center">
  <?php
  if ($row['Admin']['is_active'] == 0) { //inactive
  echo $this->Html->link('<span class="label">Inactive</span>', array('action' => 'activate_inactive/'.$row['Admin']['admin_id'].'/'.$row['Admin']['is_active']), array("title" => "Make Active", 'escape' => false));
  } else {
  echo $this->Html->link('<span class="label label-success">Active</span>', array('action' => 'activate_inactive/'.$row['Admin']['admin_id'].'/'.$row['Admin']['is_active']), array("title" => "Make Deactive", 'escape' => false));
  }
  ?>
  </td>
  <td><a class="waves-effect waves-light darken-4" href="javascript:;"  onclick="delete_subadmin(<?php echo $row['Admin']['admin_id'] ?>);" ><i class="mdi-action-delete"></i></a></td>
  </tr>
  <?php } ?>

  </tbody>
  </table>
  </div>

  <?php } */ ?>


                    </div>
                </div>
                <!-- ui tabs end -->
            </div>
        </div>
    </div>
</article>
