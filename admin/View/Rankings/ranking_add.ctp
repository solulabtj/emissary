<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
    
	function validate_category()
	{
			var error = false;
            
			var ranking_title = $('#ranking_title').val();
			var ranking_level = $('#ranking_level').val();
			var ranking_lower_limit = $('#ranking_lower_limit').val();
			var ranking_upper_limit = $('#ranking_upper_limit').val();
			
			if(ranking_title=='')
			{
				$('#error_title').html('Ranking Title is required.');
				error = true;
			}			
			else
			{
				$('#error_title').html('');
			}
			
			if(ranking_level=='')
			{
				$('#error_level').html('Ranking Level is required.');
				error = true;
			}			
			else
			{
				$('#error_level').html('');
			}
			
			if(ranking_lower_limit=='')
			{
				$('#error_lower').html('Ranking Lower Limit is required.');
				error = true;
			}			
			else
			{
				$('#error_lower').html('');
			}
			
			if(ranking_upper_limit=='')
			{
				$('#error_upper').html('Ranking Upper limit is required.');
				error = true;
			}			
			else
			{
				$('#error_upper').html('');
			}
			
			if(ranking_upper_limit!='' && ranking_lower_limit!='')
			{
				if(ranking_lower_limit>=ranking_upper_limit)
				{
					$('#lower_upper').html('Upper limit should be greater than lower limit.');
					error = true;
				}
				else
				{
					$('#lower_upper').html('');
				}
				
			}			
			
			
            if (error) {
                return false;
            }
			
     }
	 
	 function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
	}
	 
</script>


<article>
<header class="darken-1 panel-heading"> <strong>Add Ranking</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                  <form method="post" action="<?php echo ADMIN_ROOT.'rankings/ranking_add'; ?>" >
					
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Ranking Title</label>
                  <input type="text" class="col s12 m4 validate" name="ranking_title" id="ranking_title" onkeypress="return lettersOnly(event)" value="">
                  
                   </div>
                   <div id="error_title" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Ranking Level</label>
                  <input type="text" class="col s12 m4 validate" name="ranking_level" id="ranking_level" onkeypress="return onlyNumbers(event)" value="">
                  
                   </div>
                   <div id="error_level" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Ranking Lower Limit</label>
                  <input type="text" class="col s12 m4 validate" name="ranking_lower_limit" id="ranking_lower_limit" onkeypress="return onlyNumbers(event)" value="">
                  
                   </div>
                   <div id="error_lower" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Ranking Upper Limit</label>
                  <input type="text" class="col s12 m4 validate" name="ranking_upper_limit" id="ranking_upper_limit" onkeypress="return onlyNumbers(event)" value="">
                  
                   </div>
                   <div id="error_upper" class="error"></div>
                   
                   <div id="lower_upper" class="error"></div>
                  
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_category();" class="btn waves-effect waves-light cyan">Add Ranking</button>
                   </div>
                   </form>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>