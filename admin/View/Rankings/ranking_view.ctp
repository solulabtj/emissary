<script>
	
	function edit_ranking(ranking_id)
	{
		window.location='<?php echo ADMIN_ROOT; ?>rankings/edit_ranking/'+ranking_id;
	}
	
</script>

<article>
<header class="darken-1 panel-heading"> <strong>Ranking List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
<?php echo $this->Session->flash(); ?>
                      <!--<h2><i class="halflings-icon user"></i><span class="break"></span>Ranking List</h2>-->
                      
                      <div class="panel-body"><i class="livicon"></i><a href="<?php echo ADMIN_ROOT.'rankings/ranking_add'; ?>" >Add New Ranking</a></div>
                      
                   <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                 
                  <div class="s12 tab-content">
                    
                    
                    <div id="test3" class="s12">
                     <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                        
                            <th>Ranking Title</th>
                            
                            <th>Ranking Level</th>
                            <th>Ranking Lower Limit</th>
                            <th>Ranking Upper Limit</th>
                            <th>Status</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
                      
					<?php foreach($listing as $row) {  ?>  
                        <tr>
                            <td><?php echo $row['Ranking']['ranking_title']; ?></td>
                            
                            <td><?php echo $row['Ranking']['ranking_level']; ?></td>
                            
                            <td><?php echo $row['Ranking']['ranking_lower_limit']; ?></td>
                            
                            <td><?php echo $row['Ranking']['ranking_upper_limit']; ?></td>
                            
                            <td class="center">
                            	  
                                  <?php
                                    if ($row['Ranking']['is_active'] == 0) { //inactive
                                        echo $this->Html->link('<span class="label">Inactive</span>', array('action' => 'activate_inactive/'.$row['Ranking']['ranking_id'].'/'.$row['Ranking']['is_active']), array("title" => "Make Active", 'escape' => false));
                                    } else {
                                        echo $this->Html->link('<span class="label label-success">Active</span>', array('action' => 'activate_inactive/'.$row['Ranking']['ranking_id'].'/'.$row['Ranking']['is_active']), array("title" => "Make Deactive", 'escape' => false));
                                    }
                                    ?>
                                     
                            </td>
                            
                           <td>
                            <a class="waves-effect waves-light darken-4" href="javascript:;" onclick="edit_ranking('<?php echo $row['Ranking']['ranking_id']; ?>');" ><i class="mdi-editor-mode-edit"></i></a>
                           </td>
                            
                        </tr>
					<?php } ?>	
						
                       </tbody>
                  </table>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>