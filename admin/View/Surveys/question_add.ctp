<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
    
	function validate_settings()
	{
			var error = false;
            
			var question_count = $('#question_count').val();
			var question_validate;
			for(question_validate=1;question_validate<question_count;question_validate++)
			{
				var survey_category_id = $('#survey_category_id_'+question_validate).val(); 
				
				var survey_question_type = $('#survey_question_type_'+question_validate).val(); 
				
				var survey_question_description = $('#survey_question_description_'+question_validate).val(); 
				
				var survey_question_marks = $('#survey_question_marks_'+question_validate).val(); 
				
				if(survey_question_description=='')
				{
					$('#error_questions_'+question_validate).html('Survey Question Description is required.');
					error = true;
				}
				else
				{
					$('#error_questions_'+question_validate).html('');
				}
				
				if(survey_question_marks=='' || survey_question_marks=='0')
				{
					$('#error_marks_'+question_validate).html('Survey Question Mark is required OR should not be Zero.');
					error = true;
				}
				else
				{
					$('#error_marks_'+question_validate).html('');
				}
				
				if(survey_category_id=='')
				{
					$('#error_category_'+question_validate).html('Survey Category is required.');
					error = true;
				}
				else
				{
					$('#error_category_'+question_validate).html('');
				}
				
				if(survey_question_type=='')
				{
					$('#error_type_'+question_validate).html('Question Type is required.');
					error = true;
				}
				else if(survey_question_type=='C')
				{
					$('#error_type_'+question_validate).html('');
					
					/*var correctanswer = $('#apply_content_'+question_validate+' input[type=checkbox]:checked').length;
					
					if(correctanswer==0)
					{
						$('#error_option'+question_validate+'_5').html('Check at least one checkbox.');
						error = true;
					}
					else
					{
						$('#error_option'+question_validate+'_5').html('');
					}*/
					
					var limit;
					for(limit=1;limit<5;limit++)
					{
						var option_1 = $('#option_'+ question_validate +'_1').val();
						
						var option_2 = $('#option_'+ question_validate +'_2').val();
						
						var option_3 = $('#option_'+ question_validate +'_3').val();
						
						var option_4 = $('#option_'+ question_validate +'_4').val();
						
						if(option_1=='')
						{
							$('#error_option'+question_validate+'_1').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+question_validate+'_1').html('');
						}
						
						if(option_2=='')
						{
							$('#error_option'+question_validate+'_2').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+question_validate+'_2').html('');
						}
						
						if(option_3=='')
						{
							$('#error_option'+question_validate+'_3').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+question_validate+'_3').html('');
						}
						
						if(option_4=='')
						{
							$('#error_option'+question_validate+'_4').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+question_validate+'_4').html('');
						}
						
					}
				}
				
				else if(survey_question_type=='R')
				{
					var questionr1_count = $('#questionr'+question_validate+'_count').val();
					var limit;
					
					/*var correctanswer = $('#apply_contentRadio_'+question_validate+' input[type=checkbox]:checked').length;
					
					if(correctanswer==0)
					{
						$('#error_optionrrr'+question_validate).html('Check only one checkbox.');
						error = true;
					}
					if(correctanswer>1)
					{
						$('#error_optionrrr'+question_validate).html('Check only one checkbox.');
						error = true;
					}
					else
					{
						$('#error_optionrrr'+question_validate).html('');
					}*/
					
					for(limit=1;limit<questionr1_count;limit++)
					{
						var option = $('#optionr_'+ question_validate +'_'+limit).val();
						if(option=='')
						{
							$('#error_optionr'+question_validate+'_'+limit).html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_optionr'+question_validate+'_'+limit).html('');
						}
					}
				}
				
				else
				{
					$('#error_type_'+question_validate).html('');
				}
				
			}
			
            if (error) {
                return false;
            }
			
     }
	 
	 function add_option(clicked_id)
		{
			var option_count = clicked_id.split("_").pop();
			
			var questionr1_count = $('#questionr'+option_count+'_count').val();
			
			if(questionr1_count>=3)
			{
				$('#rn_'+option_count).show();
			}
			
			$.ajax({
						type: "POST",		   
						url: "<?php echo ADMIN_ROOT.'surveys/survey_question_radiooption'; ?>",
						data: "questionr1_count="+questionr1_count+"&option_count="+option_count,
						success: function(data){
								$('#questionr'+option_count+'_count').val((questionr1_count*1+1));
								$('#apply_contentRadioo_'+option_count).append(data);															
						}
					});
		}
	 
	 
	 function checkforradio(clicked_id)
		{
			var option_no = clicked_id.split("_").pop();			
			
			var parts = clicked_id.split('_');
			
			var question_no = parts[1];
			
			var correctanswer = $('.radiofor_'+question_no+' input[type=checkbox]:checked').length;
			//alert(correctanswer);
			if(correctanswer>=1)
			{
				$('.radiofor_'+question_no+' input[type=checkbox]:not(:checked)').attr('disabled', 'disabled');
			}
			else if(correctanswer==0)
			{
				$('.radiofor_'+question_no+' input[type=checkbox]').removeAttr('disabled');
			}
		}
	 
	 function remove_option(clicked_id)
		{
			var option_count = clicked_id.split("_").pop();
			
			var questionr1_count = $('#questionr'+option_count+'_count').val();
			
			var option_count1 = (questionr1_count*1-1);
				
			$('#optionrr_'+option_count+'_'+option_count1).remove();
			
			$('#questionr'+option_count+'_count').val((option_count1));
			
			if(option_count1==3)
			{
				$('#rn_'+option_count).hide();
			}
		}
	 
	 function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
	}
	
    
</script>


<article>
<header class="darken-1 panel-heading"> <strong>Add Survey Question</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                  <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  
                  <div class="s12 tab-content" >
                                  
                <form method="post" action="<?php echo ADMIN_ROOT.'surveys/question_add'; ?>" >
				
                <input type="hidden" name="question_count" id="question_count" value="2" />
                
                <input type="hidden" name="questionr1_count" id="questionr1_count" value="3" />
                 
               <div id="test1" class="s12">
                
				<div id="apply_question1" >
				
                <h4><i class="halflings-icon user"></i><span class="break"></span>Question 1</h4>
                
                <!--<div class="input_field col s12 m12" >
                  <label class="col s12 m2">Survey Category</label>
                  
                  <select name="survey_category_id_1" id="survey_category_id_1" >
                  	<option value="">Select Type
                    
                    <?php foreach($survey_category as $category) { ?>
                    
                    	<option value="<?php echo $category['SurveyCategories']['survey_category_id']; ?>"><?php echo $category['SurveyCategories']['survey_category_title']; ?>
                    
                    <?php } ?>
                    
                  </select>
                  
                 </div>-->
                  
                 <div id="error_category_1" class="error"></div>&nbsp;
                 
                <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Type</label>
                  
                  <select name="survey_question_type_1" id="survey_question_type_1" >
                  	<option value="">Select Type</option>              
                    <option value="C">Checkbox</option>
                    <option value="T">Textbox</option>
                    <option value="I">Image</option>
                    <option value="A">Audio</option>
                    <option value="R">Radio</option>
                    
                  </select>
                  
                 </div>
                 
                <div id="error_type_1" class="error"></div>
                
                &nbsp;
                <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Question Description</label>
                  <input type="text" class="col s12 m4 validate" name="survey_question_description_1" id="survey_question_description_1" onkeypress="return lettersOnly(event)" value="">
                  
                 </div>
                <div id="error_questions_1" class="error"></div>
                
                <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Question Marks</label>
                  <input type="text" class="col s12 m4 validate" name="survey_question_marks_1" id="survey_question_marks_1" onkeypress="return onlyNumbers(event)" value="">
                  
                 </div>
                <div id="error_marks_1" class="error"></div>
                 
                 
                 <div id="apply_contentRadio_1" style="display:none;">
                    	
                        <div id="apply_contentRadioo_1" class="radiofor_1">
                            <div class="input_field col s12 m12" id="optionrr_1_1" > 
                                 <label class="col s12 m2">Option 1</label>
                                 <input type="text" name="optionr_1_1" id="optionr_1_1" value="" class="col s12 m4 validate" >
                            <!--<input type="checkbox" onclick="checkforradio(this.id);" name="optionrrr_1_1" id="optionrrr_1_1" value="1" >Correct Answer-->   
                            </div>
                            <div id="error_optionr1_1" class="error"></div>
                            
                            <div class="input_field col s12 m12" id="optionrr_1_2" >
                                 <label class="col s12 m2">Option 2</label>
                                 <input type="text" name="optionr_1_2" id="optionr_1_2" value="" class="col s12 m4 validate" >
                            
                            <!--<input type="checkbox" onclick="checkforradio(this.id);" name="optionrrr_1_2" id="optionrrr_1_2" value="1" >Correct Answer-->
                            </div>
                            
                            <div id="error_optionr1_2" class="error"></div>
                        </div>
                        <br/>
                        <a id="n_1" onClick="add_option(this.id)" class="btn btn-lg btn-custom btn-success" >Add Option</a>
                        <a id="rn_1" onClick="remove_option(this.id)" class="btn btn-lg btn-custom btn-success" style="display:none;" >Remove Option</a>
                        
                        <!--<div id="error_optionrrr1" class="error"></div>-->
                            
                 </div>
                 
                 
                 <div id="apply_content_1" style="display:none;" >
                     <div class="input_field col s12 m12" id="option_div1" >
                         <label class="col s12 m2">Option 1</label>
                         <input type="text" name="option_1_1" id="option_1_1" class="col s12 m4 validate" value="">
                         <!--<input type="checkbox" name="option1_1" id="option1_1" value="1" >Correct Answer-->
                     </div>
         	         <div id="error_option1_1" class="error"></div> 
                     
                     <div class="input_field col s12 m12" id="option_div1" >
                         <label class="col s12 m2">Option 2</label>
                         <input type="text" name="option_1_2" id="option_1_2" class="col s12 m4 validate" value="">
                         <!--<input type="checkbox" name="option1_2" id="option1_2" value="1" >Correct Answer-->
                     </div>
         	         <div id="error_option1_2" class="error"></div>
                     
                     <div class="input_field col s12 m12" id="option_div1" >
                         <label class="col s12 m2">Option 3</label>
                         <input type="text" name="option_1_3" id="option_1_3" class="col s12 m4 validate" value="">
                         <!--<input type="checkbox" name="option1_3" id="option1_3" value="1" >Correct Answer-->
                     </div>
         	         <div id="error_option1_3" class="error"></div>
                     
                     <div class="input_field col s12 m12" id="option_div1" >
                         <label class="col s12 m2">Option 4</label>
                         <input type="text" name="option_1_4" id="option_1_4" class="col s12 m4 validate" value="">
                         <!--<input type="checkbox" name="option1_4" id="option1_4" value="1" >Correct Answer-->
                     </div>
         	         <div id="error_option1_4" class="error"></div>
                     
                     <!--<div id="error_option1_5" class="error"></div>-->
                                      
                  </div>
                  </div>
				  
                 </div>
                 
                <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_settings();" class="btn waves-effect waves-light cyan">Save All</button>
                 
				    
                         <button type="button" id="add_question" class="btn waves-effect waves-light cyan">Add More Question</button>
						 
						 <button type="button" id="remove_question" class="btn waves-effect waves-light cyan" style="display:none;" >Remove Question</button>
						 
                 </div>
				 
                   </form>
                </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>
