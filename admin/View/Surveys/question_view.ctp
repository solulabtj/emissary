<script>
	
	function edit_question(survey_question_id)
	{
		window.location='<?php echo ADMIN_ROOT; ?>surveys/edit_question/'+survey_question_id;
	}
	function delete_question(survey_question_id)
	{
		var strconfirm = confirm("Are you sure you want to delete this question ?");
		if (strconfirm == true)
		{
			window.location='<?php echo ADMIN_ROOT; ?>surveys/delete_question/'+survey_question_id;
		}
		else{
			return false;
		}
		
	}

</script>

<article>
<header class="darken-1 panel-heading"> <strong>Survey Question List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
<?php echo $this->Session->flash(); ?>
                      <!--<h2><i class="halflings-icon user"></i><span class="break"></span>Survey Question List</h2>-->
                      
                      <div class="panel-body"><i class="livicon"></i><a href="<?php echo ADMIN_ROOT.'surveys/question_add'; ?>" >Add New Survey Question</a></div>
                      
                   <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                 
                  <div class="s12 tab-content">
                    
                    
                    <div id="test3" class="s12">
                     <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Survey Question Description</th>
                            <th>Survey Question Marks</th>
                            <!--<th>Status</th>-->
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
                      
					<?php foreach($listing as $row) {  ?>  
                        <tr>
                            <td><?php echo $row['SurveyQuestion']['survey_question_description']; ?></td>
                            
                           	<td><?php echo $row['SurveyQuestion']['survey_question_marks']; ?></td>
                           	
                           <!-- <td class="center">
                                    <?php
									 
                                    /*if ($row['SurveyQuestion']['is_blocked'] == 0) { //inactive
                                        echo $this->Html->link('<span class="label">Block</span>', array('action' => 'activate_inactive_view/'.$row['SurveyQuestion']['survey_question_id'].'/'.$row['SurveyQuestion']['is_blocked']), array("title" => "Make Block", 'escape' => false));
                                    } else {
                                        echo $this->Html->link('<span class="label label-success">Unblock</span>', array('action' => 'activate_inactive_view/'.$row['SurveyQuestion']['survey_question_id'].'/'.$row['SurveyQuestion']['is_blocked']), array("title" => "Make Unblock", 'escape' => false));
                                    }*/
                                    ?>
                            </td>-->
                            
                           <td>
                           	
                            <?php if($row['SurveyQuestion']['survey_question_type']=='C' || $row['SurveyQuestion']['survey_question_type']=='R') { echo "You can't edit question with type checkbox OR radio."; } else { ?>
                            
                            	<a class="waves-effect waves-light darken-4" href="javascript:;" onclick="edit_question('<?php echo $row['SurveyQuestion']['survey_question_id']; ?>');" ><i class="mdi-editor-mode-edit"></i></a>
                            
                            <?php } ?>
                            
                            &nbsp;&nbsp;
                            <a class="waves-effect waves-light darken-4" href="javascript:;" onclick="delete_question('<?php echo $row['SurveyQuestion']['survey_question_id']; ?>');" ><i class="mdi-action-delete"></i></a>
                            
                           </td>
                            
                        </tr>
					<?php } ?>	
						
                       </tbody>
                  </table>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>