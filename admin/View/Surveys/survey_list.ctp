<article>
<header class="darken-1 panel-heading"> <strong>Survey List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
<?php echo $this->Session->flash(); ?>
                      <!--<h2><i class="halflings-icon user"></i><span class="break"></span>Survey List</h2>
                      -->
                      <?php //pr($count); ?>
                   <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                 
                  <div class="s12 tab-content">
                    
                    
                    <div id="test3" class="s12">
                     <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                        
                            <th>Survey Name</th>
                            <th>Company</th>
                            <!--<th>Categories</th>-->
                            <th>Questions</th>                            
                            <th>Created</th> 
                            <th>Status</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
                      
					<?php foreach($listing as $row) {  ?>  
                        <tr>
                            <td><?php echo $row['Survey']['survey_name']; ?></td>
                            <td><?php echo $row['ClientUserInfo']['company_name']; ?></td>
                            <!--<td><?php echo $row['Survey']['survey_category_id']; ?></td>-->
                            <td><?php echo $count[$row['Survey']['survey_id']]; ?></td>
                            <td>
							
							<?php 
							
							 $now = time(); 
							 $your_date = strtotime($row['Survey']['created']);
							 $datediff = $now - $your_date;
							 // echo floor($datediff/(60*60*24)).' days ago';
							 echo date("m-d-Y",strtotime($row['Survey']['created'])); 
							
							?></td>
                            
                            <td class="center">
                            	  
                                  <?php
                                    if ($row['Survey']['is_blocked'] == 0) { //inactive
                                        echo $this->Html->link('<span class="label">Published</span>', array('action' => 'publish_unpublish/'.$row['Survey']['survey_id'].'/'.$row['Survey']['is_blocked']), array("title" => "Unpublish survey", 'escape' => false));
                                    } else {
                                        echo $this->Html->link('<span class="label label-success">Unpublished</span>', array('action' => 'publish_unpublish/'.$row['Survey']['survey_id'].'/'.$row['Survey']['is_blocked']), array("title" => "Publish survey", 'escape' => false));
                                    }
                                  ?>
                                     
                            </td>
                            
                           <td>
                            <a  class="waves-effect waves-light darken-4" href="<?php echo ADMIN_ROOT."surveys/view_question_survey/".$row['Survey']['survey_id']; ?>" >View Details</a>
                           </td>
                            
                        </tr>
					<?php } ?>	
						
                       </tbody>
                  </table>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>