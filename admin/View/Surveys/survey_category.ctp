<script>
	
	function edit_jobcategory(survey_category_id)
	{
		window.location='<?php echo ADMIN_ROOT; ?>surveys/edit_jobcategory/'+survey_category_id;
	}
	
	function delete_jobcategory(survey_category_id)
	{
		
		var valid_del = window.confirm('Are you sure want to delete this record?');
		
		if(valid_del===true)
		{
			window.location='<?php echo ADMIN_ROOT; ?>surveys/activate_inactive/'+survey_category_id;
		}
		
	}
	
</script>

<article>
<header class="darken-1 panel-heading"> <strong>Survey Categories List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
<?php echo $this->Session->flash(); ?>
                      <!--<h2><i class="halflings-icon user"></i><span class="break"></span>Survey Category List</h2>-->
                      
                      <div class="panel-body"><i class="livicon"></i><a href="<?php echo ADMIN_ROOT.'surveys/category_add'; ?>" >Add New Survey Category</a></div>
                      
                   <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                 
                  <div class="s12 tab-content">
                    
                    
                    <div id="test3" class="s12">
                     <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                        
                            <th>Survey Category Title</th>
                            <th>Survey Category Image</th>
                            <!--<th>Status</th>-->
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
                      
					<?php foreach($listing as $row) {  ?>  
                        <tr>
                            <td><?php echo $row['SurveyCategories']['survey_category_title']; ?></td>
                            
                            <td><?php echo $row['SurveyCategories']['survey_category_image']; ?></td>
                            
                           <!-- <td class="center">
                            	  
                                  <?php
                                    /*if ($row['SurveyCategories']['is_blocked'] == 0) { //inactive
                                        echo $this->Html->link('<span class="label">Block</span>', array('action' => 'activate_inactive/'.$row['SurveyCategories']['survey_category_id'].'/'.$row['SurveyCategories']['is_blocked']), array("title" => "Make Block", 'escape' => false));
                                    } else {
                                        echo $this->Html->link('<span class="label label-success">Unblock</span>', array('action' => 'activate_inactive/'.$row['SurveyCategories']['survey_category_id'].'/'.$row['SurveyCategories']['is_blocked']), array("title" => "Make Unblock", 'escape' => false));
                                    }*/
                                    ?>
                                     
                            </td>-->
                            
                           <td>
                            <a class="waves-effect waves-light darken-4" href="javascript:;" onclick="edit_jobcategory('<?php echo $row['SurveyCategories']['survey_category_id']; ?>');" ><i class="mdi-editor-mode-edit"></i></a>
                            
                            <a class="waves-effect waves-light darken-4" href="javascript:;" onclick="delete_jobcategory('<?php echo $row['SurveyCategories']['survey_category_id']; ?>');" ><i class="mdi-action-delete"></i></a>
                            
                           </td>
                            
                        </tr>
					<?php } ?>	
						
                       </tbody>
                  </table>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>