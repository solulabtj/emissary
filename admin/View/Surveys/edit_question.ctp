<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">    
	
	function validate_settings()
	{
			var error = false;
            
			var survey_question_description = $('#survey_question_description').val();
			
			var survey_question_marks = $('#survey_question_marks').val();
			
			var type = $('#type_of_box').val();
			
			var survey_category_id = $('#survey_category_id').val();
			//alert(type);
			
			if(survey_category_id=='')
			{
				$('#error_category').html('Survey Category is required.');
				error = true;
			}
			else
			{
				$('#error_category').html('');
			}
			
			if(type=='')
			{
				$('#error_type').html('Type selection is required.');
				error = true;
			}			
			
			else if(type=='C')
			{
					for(option_limit=1;option_limit<=5;option_limit++)
					{
						var ID = $('#option_'+option_limit).val();
						if(ID=='')
						{
							$('#error_option'+option_limit).html('Option Entry is required.');
							error = true;
						}			
						else
						{
							$('#error_option'+option_limit).html('');
						}
					}	
					$('#error_type').html('');			
			}
			
			else
			{
				$('#error_type').html('');				
			}
			
			if(survey_question_description=='')
			{
				$('#error_questions').html('Question Description is required.');
				error = true;
			}			
			else
			{
				$('#error_questions').html('');						
			}
			
			if(survey_question_marks=='' || survey_question_marks=='0')
			{
				$('#error_marks').html('Question Marks is required OR should be not Zero.');
				error = true;
			}			
			else
			{
				$('#error_marks').html('');				
			}
			
			
            if (error) {
                return false;
            }
			
     }
	 
	 function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
	}
	
     
</script>

<?php //pr($singlerecord); pr($count); ?>

<article>
<header class="darken-1 panel-heading"> <strong>Update Survey Question</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                  <form method="post" action="<?php echo ADMIN_ROOT.'surveys/edit_question'; ?>" >
				
                  <input type="hidden" name="survey_question_id" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_id']; ?>" />
                  
                  <input type="hidden" name="type" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_type']; ?>" />
                  
                  <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Survey Category</label>
                  
                  <select name="survey_category_id" id="survey_category_id" >
                  	<option value="">Select Type
                    
                    <?php foreach($survey_category as $category) { ?>
                    
                    	<option value="<?php echo $category['SurveyCategories']['survey_category_id']; ?>"<?php if($category['SurveyCategories']['survey_category_id']==$singlerecord[0]['SurveyQuestion']['survey_category_id']) { echo "selected='selected'"; } ?>  ><?php echo $category['SurveyCategories']['survey_category_title']; ?>
                    
                    <?php } ?>
                    
                  </select>
                  
                 </div>
                  
                 <div id="error_category" class="error"></div>&nbsp;
                  
                  <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Type</label>
                  
                  <select name="type" id="type_of_box" disabled >
                  	<option value="" >Select Type
                    
                    <option value="C"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='C') { echo "selected='selected'";  } ?> >Checkbox
                    <option value="T"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='T') { echo "selected='selected'";  } ?> >Textbox
                    <option value="I"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='I') { echo "selected='selected'";  } ?> >Image
                    <option value="A"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='A') { echo "selected='selected'";  } ?> >Audio
                    
                  </select>
                  
                 </div>
                <div id="error_type" class="error"></div>&nbsp;
                  
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Question Description</label>
                  <input type="text" class="col s12 m4 validate" name="survey_question_description" id="survey_question_description" onkeypress="return lettersOnly(event)" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_description']; ?>">
                  
                   </div>
                   <div id="error_questions" class="error"></div>
                   
                 <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Question Marks</label>
                  <input type="text" class="col s12 m4 validate" name="survey_question_marks" id="survey_question_marks" onkeypress="return onlyNumbers(event)" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_marks']; ?>">
                  
                 </div>
                <div id="error_marks" class="error"></div>
                 
                 
                <?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='C') { 
				?>		
				<div id="apply_content">
					<?php 
					$j=0;
					for($i=1;$i<=$count;$i++) 
					{		
						?>
                 
                 
                 <div class="input_field col s12 m12" id="option_div<?php echo $i; ?>" >
                 <label class="col s12 m2">Option <?php echo $i; ?></label>
                 <input type="text" name="option_<?php echo $i; ?>" id="option_<?php echo $i; ?>" class="col s12 m4 validate" value="<?php echo $singlerecord[0]['SurveyQuestionOption'][$j]['answer_text']; ?>">
                 
                 </div>
                   
                  <div id="error_option<?php echo $i; ?>" class="error"></div>                  
                  
                  
                  
                 <?php $j++; } ?> 
                 
                 </div>
                 
                 <?php } ?>
                   
                 <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_settings();" class="btn waves-effect waves-light cyan">Update Question</button>
                   </div>
                   </form>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>