<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
    
	function validate_category()
	{
			var error = false;
            
			var survey_category_tite = $('#survey_category_tite').val();
			
			if(survey_category_tite=='')
			{
				$('#error_title').html('Survey Category Title is required.');
				error = true;
			}			
			else
			{
				$('#error_title').html('');
			}
			
            if (error) {
                return false;
            }
			
     }
	 
</script>


<article>
<header class="darken-1 panel-heading"> <strong>Update Survey Category</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                  <form method="post" action="<?php echo ADMIN_ROOT.'surveys/edit_jobcategory'; ?>" >
				<input type="hidden" name="survey_category_id" value="<?php echo $singlerecord[0]['SurveyCategories']['survey_category_id']; ?>" />
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Survey Category Title</label>
                  <input type="text" class="col s12 m4 validate" name="survey_category_tite" id="survey_category_tite" onkeypress="return lettersOnly(event)" value="<?php echo $singlerecord[0]['SurveyCategories']['survey_category_tite']; ?>" >
                  
                   </div>
                   <div id="error_title" class="error"></div>
                   
                  
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_category();" class="btn waves-effect waves-light cyan">Update Job Category</button>
                   </div>
                   </form>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>