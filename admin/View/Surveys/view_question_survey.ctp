<article>
    <header class="darken-1 panel-heading"> <strong>Survey Questions</strong>
        <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a></span> 
    </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <?php echo $this->Session->flash(); ?>
            <div class="s12 m12 24">
                <!-- ui tabs -->
                <div class="s12 m12 l12">
                    <div class="s12 tab-content">
                        <div id="test1" class="s12">
                            <?php
                            if (!empty($survey_questions)) {
                                foreach ($survey_questions as $survey_question) {
                                    $count = count($survey_question['SurveyQuestionOption']);
                                    ?>
                                    <div class="input-box"><?php echo "<b>Question</b> : "; ?><?php
                                        echo $survey_question['SurveyQuestion']['survey_question_description'];
                                        ?><br><?php
                                        if ($survey_question['SurveyQuestion']['survey_question_type'] == 'C' || $survey_question['SurveyQuestion']['survey_question_type'] == 'R') {
                                            if ($count == 0) {
                                                echo "<b>Option</b> : No option available . <br>";
                                            } else {
                                                for ($i = 0; $i < $count; $i++) {
                                                    echo "<b>Option</b> : " . $survey_question['SurveyQuestionOption'][$i]['answer_text'] . "<br>";
                                                }
                                            }
                                        } else {
                                            echo "This type questions dont have options. <br>";
                                        }
                                        ?>
                                        <?php echo "<b>Question Marks : </b>" . $survey_question['SurveyQuestion']['survey_question_marks']; ?>

                                        <br>
                                        <?php
                                        if ($survey_question['SurveyQuestion']['is_locked'] == '1') {
                                            ?>
                                            <a href="javascript:;" >Locked</a>
                                            <?php
                                        }
                                        ?>                                                                                                                
                                    </div>
                                    <br>
                                <?php } ?> 
                            <?php } else {
                                echo "No question found for this survey";
                            } ?>
                        </div>
                    </div>
                </div>
                <!-- ui tabs end -->
            </div>
        </div>
    </div>
</article>