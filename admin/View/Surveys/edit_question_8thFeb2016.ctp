<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">    
	
	function validate_settings()
	{
			var error = false;
            
			var survey_question_description = $('#survey_question_description').val();
			
			var survey_question_marks = $('#survey_question_marks').val();
			
			var type = $('#type_of_box').val();
			
			//alert(type);
			
			if(type=='')
			{
				$('#error_type').html('Type selection is required.');
				error = true;
			}			
			
			else if(type=='R' || type=='C')
			{
				var option_limit;			
					var for_loop = $('#option_count').val();
					//alert(for_loop);
					for(option_limit=1;option_limit<for_loop;option_limit++)
					{
						var ID = $('#option_'+option_limit).val();
						if(ID=='')
						{
							$('#error_option'+option_limit).html('Option Entry is required.');
							error = true;
						}			
						else
						{
							$('#error_option'+option_limit).html('');
						}
					}	
					$('#error_type').html('');			
			}
			
			else
			{
				$('#error_type').html('');				
			}
			
			if(survey_question_description=='')
			{
				$('#error_questions').html('Question Description is required.');
				error = true;
			}			
			else
			{
				$('#error_questions').html('');						
			}
			
			if(survey_question_marks=='' || survey_question_marks=='0')
			{
				$('#error_marks').html('Question Marks is required OR should be not Zero.');
				error = true;
			}			
			else
			{
				$('#error_marks').html('');				
			}
			
			
            if (error) {
                return false;
            }
			
     }
	 
	 function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
	}
	
     
</script>

<?php //pr($singlerecord); pr($count); ?>

<article>
<header class="darken-1 panel-heading"> <strong>Update Survey Question</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                  <form method="post" action="<?php echo ADMIN_ROOT.'surveys/edit_question'; ?>" >
				
                  <input type="hidden" name="survey_question_id" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_id']; ?>" />
                  
                  <input type="hidden" name="type" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_type']; ?>" />
                  
                  <?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='R' ||
				  			$singlerecord[0]['SurveyQuestion']['survey_question_type']=='C' ) { ?>
                  <?php if($count>0) { ?>
                  	<input type="hidden" name="option_count" id="option_count" value="<?php echo $count+1;; ?>" />
                  <?php } else { ?>
    				<input type="hidden" name="option_count" id="option_count" value="2" />              
                  <?php } ?>
                  
                  <?php } else { ?>
                  	<input type="hidden" name="option_count" id="option_count" value="2" />
                  <?php } ?>
                  
                  <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Type</label>
                  
                  <select name="type" id="type_of_box" disabled >
                  	<option value="" >Select Type
                    <option value="R"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='R') { echo "selected='selected'";  } ?> >Radio
                    <option value="C"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='C') { echo "selected='selected'";  } ?> >Checkbox
                    <option value="T"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='T') { echo "selected='selected'";  } ?> >Textbox
                    <option value="I"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='I') { echo "selected='selected'";  } ?> >Image
                    <option value="A"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='A') { echo "selected='selected'";  } ?> >Audio
                    
                  </select>
                  
                 </div>
                <div id="error_type" class="error"></div>
                  
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Question Description</label>
                  <input type="text" class="col s12 m4 validate" name="survey_question_description" id="survey_question_description" onkeypress="return lettersOnly(event)" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_description']; ?>">
                  
                   </div>
                   <div id="error_questions" class="error"></div>
                   
                 <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Question Marks</label>
                  <input type="text" class="col s12 m4 validate" name="survey_question_marks" id="survey_question_marks" onkeypress="return onlyNumbers(event)" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_marks']; ?>">
                  
                 </div>
                <div id="error_marks" class="error"></div>
                 
                 
                <?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='C' ||
							$singlerecord[0]['SurveyQuestion']['survey_question_type']=='R' ) { 
				?>		
					<div id="apply_content">
					<?php 
					$j=0;
					for($i=1;$i<=$count;$i++) 
					{		
							?>
                 
                 
                 <div class="input_field col s12 m12" id="option_div<?php echo $i; ?>" >
                 <label class="col s12 m2">Option <?php echo $i; ?></label>
                 <input type="text" name="option_<?php echo $i; ?>" id="option_<?php echo $i; ?>" class="col s12 m4 validate" value="<?php echo $singlerecord[0]['SurveyQuestionOption'][$j]['answer_text']; ?>">
                 
                 </div>
                   
                  <div id="error_option<?php echo $i; ?>" class="error"></div>                  
                  
                  
                  
                 <?php $j++; } ?> 
                 
                 </div>
                 
                 <div class="input_field col s12 m12" id="option_button" >
                         <label class="col s12 m2"></label>
                         <button type="button" id="add-option" class="btn waves-effect waves-light cyan">Add Option</button>&nbsp;
                         <button type="button" id="remove-option" class="btn waves-effect waves-light cyan">Remove Option</button>
                 </div>
                 
                 <?php } else { ?>
                 
                 <div id="apply_content">
                 <div class="input_field col s12 m12" id="option_div1" style="display:none;" >
                 <label class="col s12 m2">Option 1</label>
                 <input type="text" name="option_1" id="option_1" class="col s12 m4 validate" value="">
                 
                 </div>
                   
                  <div id="error_option1" class="error"></div>                  
                  </div>
                  
                 </div>
                 <div class="input_field col s12 m12" id="option_button" style="display:none;" >
                         <label class="col s12 m2"></label>
                         <button type="button" id="add-option" class="btn waves-effect waves-light cyan">Add Option</button>&nbsp;
                         <button type="button" id="remove-option" class="btn waves-effect waves-light cyan">Remove Option</button>
                 </div>
                 
                 
                 <?php } ?>
                   
                 <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_settings();" class="btn waves-effect waves-light cyan">Update Question</button>
                   </div>
                   </form>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>