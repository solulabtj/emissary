<?php //pr($listing); exit;   ?>

<article>
    <header class="darken-1 panel-heading"> <strong>Client Details</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
            <!--<a href="javascript:;" class="fa fa-times"></a>--> 
        </span> </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <div style="float:left; border:solid 1px black; width:20%;height:150px;">
                <?php
                $path = WEBSITE_WEBROOT . "/media/img/" . $listing['ClientUserInfo']['profile_image'];
                if ($listing['ClientUserInfo']['profile_image'] != "") {
                    if (file_exists($path)) {
                        ?>
                        <img src="<?php echo WEBSITE_ROOT . "/media/img/" . $listing['ClientUserInfo']['profile_image']; ?>" width="200px;">
                    <?php
                    } else {
                        echo "No Profile Image Set";
                    }
                } else {
                    echo "No Profile Image Set";
                }
                ?>
            </div>

            <div style="margin-left:270px;font-size:18px;">
                Name :<?php echo isset($listing['ClientUserInfo']['client_name']) ? $listing['ClientUserInfo']['client_name'] : ""; ?>
                <br><br>Email Address : <?php echo isset($listing['ClientUserInfo']['email_address']) ? $listing['ClientUserInfo']['email_address'] : ""; ?>
                <br><br>Mobile Number : <?php echo isset($listing['ClientUserInfo']['mobile_number']) ? $listing['ClientUserInfo']['mobile_number'] : ""; ?>
                <br><br>Company Name : <?php echo isset($listing['ClientUserInfo']['company_name']) ? $listing['ClientUserInfo']['company_name'] : ""; ?>
                <br><br>Agency Name : <?php echo isset($listing['ClientUserInfo']['agency_name']) ? $listing['ClientUserInfo']['agency_name'] : ""; ?>
                <br><br>Address : <?php echo isset($listing['ClientUserInfo']['client_address']) ? $listing['ClientUserInfo']['client_address'] : ""; ?>
                <br><br>City : <?php echo isset($listing['ClientUserInfo']['city_name']) ? $listing['ClientUserInfo']['city_name'] : ""; ?>
                <br><br>Postal Code : <?php echo isset($listing['ClientUserInfo']['postal_code']) ? $listing['ClientUserInfo']['postal_code'] : ""; ?>
                <br><br><a href="<?php echo ADMIN_ROOT . "clients/client_view"; ?>" class="btn waves-effect waves-light cyan">BACK</a>
            </div>
            <br/><br>
            <h5>Client Jobs</h5>
            <div id="test3" class="s12">
                <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Job ID</th>
                            <th>Posted Date</th>
                            <th>Submissions Received</th>
                            <th>Requests Received</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tfoot>
                    </tfoot>
                    <?php foreach ($listings as $value) { ?>
                        <?php
                        $status = "";
                        $date = strtotime(date("Y-m-d"));
                        $jobend = strtotime($value['Job']['job_end_date']);
                        
                        if($date > $jobend){
                            $status= "Completed";
                        }else{
                            $status= "On Going";
                        }
                        
                        ?>
                        <?php if ($status == "Applied" || $status == "Completed") { ?>
                            <tr>
                                <td><?php echo $value['Job']['job_id']; ?></td>
                                <td><?php echo $value['Job']['created']; ?></td>
                                <td><?php echo $value['Job']['submission_received']; ?></td>
                                <td><?php echo $value['Job']['request_received']; ?></td>
                                <td><?php echo $status; ?></td>
                            </tr>
    <?php } ?>
<?php } ?>
                </table>
            </div> 
        </div>
    </div>
</article>