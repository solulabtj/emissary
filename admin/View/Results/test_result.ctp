
<?php //pr($res); exit; ?>

<script type="text/javascript">
	
	function getAllresults()
	{
		var is_passed = $('#is_passed').val();
		
		$.ajax({
			
			type: "POST",		   
			url: "<?php echo ADMIN_ROOT.'results/getAllresults'; ?>",
			data: "is_passed="+is_passed,
			success: function(data){
					
					$('#unique_data').html(data);
					
			}
		
		});
	}

</script>


<article>
<header class="darken-1 panel-heading"> <strong>Test Result List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
<?php echo $this->Session->flash(); ?>
                      <!--<h2><i class="halflings-icon user"></i><span class="break"></span>Test Result List</h2>-->
                   <select name="is_passed" id="is_passed" onchange="getAllresults();" >
                   
                   	<option value='' >Result Type</option>
                    <option value='1' >Pass</option>
                    <option value='0' >Fail</option>
                   
                   </select>
                      
                   <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                 
                  <div class="s12 tab-content">
                    <div id="test3" class="s12">
                    
                    <div id="unique_data" >
                    <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                     
                    <thead>
                    	<tr>
                    		<th>Agent Name</th>
                            <th>Submission Date</th>
                            <th>Result</th>
                            
                            <th>Percentage</th>
                            <th>Action</th>
                                                  
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
                      
					
                    <?php foreach($listing as $row) {  ?>  
                        <tr>
                        	<td><?php echo $row['AgentUserInfo']['agent_name']; ?></td>
                            <td><?php //echo date("jS F, Y h:i A",strtotime($row['TestResult']['created']));
							   echo date("m-d-Y",strtotime($row['TestResult']['created']));
							 ?></td>
                            <td><?php if($row['TestResult']['is_passed']==1) { echo "Pass"; } else { echo "fail"; } ?></td>
                           
                            <td><?php echo $row['TestResult']['percentage_earned'].'%'; ?></td>
                            <td><a href="<?php echo ADMIN_ROOT.'results/show_answer/'.$row['TestResult']['agent_id'].'/'.$row['TestResult']['test_result_id'];?>">Show Answers</a></td>
                            
                            
                        </tr>
					<?php } ?>
                    
                       </tbody>
                  </table>
                  </div>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>