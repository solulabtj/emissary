
<article>
    <header class="darken-1 panel-heading"> <strong>View Answers</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
            <!--<a href="javascript:;" class="fa fa-times"></a>--> 
        </span> </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <?php
            foreach ($data as $data1) {

                foreach ($option_select[$data1['AgentTestAnswer']['test_question_id']] as $row2) {
                    $aryData[$data1['AgentTestAnswer']['test_question_id']][$row2['AgentTestAnswer']['agent_test_answer_id']] = $row2['AgentTestAnswer']['test_option_id'];
                }
            }

            echo $this->Session->flash();
            ?>
            <div class="s12 m12 24">
                <!-- ui tabs -->
                <div class="s12 m12 l12">

                    <div class="s12 tab-content">
                        <div id="test1" class="s12">
                            <form method="post" action="<?php echo ADMIN_ROOT; ?>" style="padding:10px 75px;">

                                <div class="input_field col s12 m6" style="padding-left:0px;">
                                    <label class="col s12 m5" style="padding-left:0px;">Agent Name: -- </label><?php echo $single['AgentUserInfo']['agent_name']; ?></div>
                                <div class="input_field col s12 m6">   
                                    <label class="col s12 m5">Submitted On: -- </label><?php echo date("m-d-Y h:i A", strtotime($single['TestResult']['created'])); ?>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                                <div class="input_field col s12 m6" style="padding-left:0px;">
                                    <label class="col s12 m5" style="padding-left:0px;">Result: -- </label><?php
                                    if ($single['TestResult']['is_passed'] == 1) {
                                        echo "<div style='color:#0F9;'>Pass</div>";
                                    } else {
                                        echo "<div style='color:#F00'>fail</div>";
                                    }
                                    ?></div>


                                <div class="input_field col s12 m6">

                                    <label class="col s12 m5">Percentage: -- </label><?php echo $single['TestResult']['percentage_earned'] . '%'; ?>                 

                                </div>  
                                <div class="clearfix"></div>
                                <br>

                                <div style="max-height:535px; overflow-y:scroll;">
                                    <h5>View Answers</h5>
                                    <div class="input_field col s12 m12" style="padding-bottom:15px; padding-left:0px; border-bottom:1px solid #ccc; margin-bottom:35px;">

                                    <?php foreach ($listing as $row) {
                                        if (!empty($row['AgentTestAnswer'])) {
                                            ?>

                                                <div style="padding-bottom:10px;">
                                                    <label>Question:-- </label>

                                                    <?php
                                                    echo $row['TestQuestion']['test_question_description'];

                                                    $aryData1 = array();
                                                    $aryData2 = array();
                                                    foreach ($aryData[$row['TestQuestion']['test_question_id']] as $row3) {
                                                        $aryData1[] = $row3;
                                                        $aryData2 = $aryData1;
                                                    }
                                                    ?>

                                                </div>


                                            <?php
                                            foreach ($option_data[$row['AgentTestAnswer']['agent_test_answer_id']] as $row1) {
                                                ?>

                                                    <div class="input_field">
                                                                                        <input type="checkbox" onclick="return false" value="<?php echo $row1['TestQuestionOption'] ['answer_text']; ?>"	
                                            <?php
                                            if (in_array($row1['TestQuestionOption']['test_option_id'], $aryData2)) {
                                                echo "checked='checked'";
                                            }
                                            ?>  > &nbsp;<?php echo $row1['TestQuestionOption']['answer_text']; ?>
                                                                                    </div>

                                                <?php } ?>

                                            </div>
                                            <div class="input_field col s12 m12" style="padding-bottom:15px; padding-left:0px; border-bottom:1px solid #ccc; margin-bottom:35px;">
                                            <?php }
                                        }
                                        ?>


                                    </div>
                                    <div class="input_field">
                                        <label></label>
                                        <a href="<?php echo ADMIN_ROOT . 'results/test_result'; ?>" class="btn waves-effect waves-light cyan">Back</a>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
                <!-- ui tabs end -->
            </div>
        </div>
    </div>
</article>
