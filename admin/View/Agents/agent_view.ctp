<article>
<header class="darken-1 panel-heading"> <strong>Agent List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">

     <?php echo $this->Session->flash(); ?>
                      <!--<h2><i class="halflings-icon user"></i><span class="break"></span>Agent List</h2>-->
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                 
                  <div class="s12 tab-content">
                    
                    
                    <div id="test3" class="s12">
                     <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Agent Name</th>
                            <th>Email Address</th>
                           <!-- <th>Paypal Email Address</th>-->
                            
                            <th>Mobile No.</th>
                            <th>City Name</th>
                            <th>Last Login</th>
                             <th>Status</th>
                            
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
                      
     <?php foreach($listing as $row) {  ?>  
                        <tr>
                            <td><a href="<?php echo WEBSITE_ROOT."admin/agents/agent_details_view/".$row['AgentUserInfo']['agent_id']; ?>"><?php echo $row['AgentUserInfo']['agent_name']; ?></a></td>
                            <td><?php echo $row['AgentUserInfo']['email_address']; ?></td>
                            <!--<td><?php echo $row['AgentUserInfo']['paypal_email_address']; ?></td>-->
                            
                            <td><?php echo $row['AgentUserInfo']['mobile_number']; ?></td>
                            <td><?php echo $row['AgentUserInfo']['city_name']; ?></td>
                            <td>
							<?php 
							
							 $now = time(); 
							 $your_date = strtotime($row['AgentUserInfo']['last_login']);
							 $datediff = $now - $your_date;
							// echo floor($datediff/(60*60*24)).' days ago';                                                        
							if($row['AgentUserInfo']['last_login'] != "0000-00-00 00:00:00")
							 echo date("m-d-Y",strtotime($row['AgentUserInfo']['last_login'])); 
							
							?></td>
                            <td class="center">
                             
                                <?php
                                    if ($row['AgentUserInfo']['is_blocked'] == 1) { //inactive
                                        echo $this->Html->link('<span class="label">Inactive</span>', array('action' => 'activate_inactive/'.$row['AgentUserInfo']['agent_id'].'/'.$row['AgentUserInfo']['is_blocked']), array("title" => "Make Active", 'escape' => false));
                                    } else {
                                        echo $this->Html->link('<span class="label label-success">Active</span>', array('action' => 'activate_inactive/'.$row['AgentUserInfo']['agent_id'].'/'.$row['AgentUserInfo']['is_blocked']), array("title" => "Make Deactive", 'escape' => false));
                                    }
                                    ?>
                                        
                            </td>
                            
                        </tr>
     <?php } ?> 
      
                       </tbody>
                  </table>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>