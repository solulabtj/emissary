<?php //pr($listing);     ?>

<article>
    <header class="darken-1 panel-heading"> <strong>Agent Details</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
            <!--<a href="javascript:;" class="fa fa-times"></a>--> 
        </span> </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <div style="float:left; border:solid 1px black; width:20%;height:150px;">
                <?php
                $path = WEBSITE_WEBROOT . "/media/img/" . $listing['AgentUserInfo']['profile_image'];
                if ($listing['AgentUserInfo']['profile_image'] != "") {
                    if (file_exists($path)) {
                        ?>
                        <img src="<?php echo WEBSITE_ROOT . "/media/img/" . $listing['AgentUserInfo']['profile_image']; ?>" width="212px" height="148px">
                        <?php
                    } else {
                        echo "No Profile Image Set";
                    }
                } else {
                    echo "No Profile Image Set";
                }
                ?>
            </div>
            <?php
            if ($listing['AgentUserInfo']['gender'] == "M") {
                $gender = "Male";
            } else {
                $gender = "Female";
            }
            ?>

            <div style="margin-left:270px;font-size:18px;">
                Name :<?php echo isset($listing['AgentUserInfo']['agent_name']) ? $listing['AgentUserInfo']['agent_name'] : ""; ?>
                <br><br>Email Address : <?php echo isset($listing['AgentUserInfo']['email_address']) ? $listing['AgentUserInfo']['email_address'] : ""; ?>
                <br><br>Mobile Number : <?php echo isset($listing['AgentUserInfo']['mobile_number']) ? $listing['AgentUserInfo']['mobile_number'] : ""; ?>
                <br><br>Date of Birth : <?php echo isset($listing['AgentUserInfo']['agent_dob']) ? $listing['AgentUserInfo']['agent_dob'] : ""; ?>
                <br><br>Gender : <?php echo $gender; ?>
                <br><br>Address 1 : <?php echo isset($listing['AgentUserInfo']['agent_address1']) ? $listing['AgentUserInfo']['agent_address1'] : ""; ?>
                <br><br>Address 2 : <?php echo isset($listing['AgentUserInfo']['agent_address2']) ? $listing['AgentUserInfo']['agent_address2'] : ""; ?>
                <br><br>Country : <?php echo isset($listing['AgentUserInfo']['country_name']) ? $listing['AgentUserInfo']['country_name'] : ""; ?>
                <!--<br><br>State : <?php //echo isset($listing['AgentUserInfo']['state_name'])?$listing['AgentUserInfo']['state_name']:"";    ?>-->
                <br><br>City : <?php echo isset($listing['AgentUserInfo']['city_name']) ? $listing['AgentUserInfo']['city_name'] : ""; ?>
                <br><br>Postal Code : <?php echo isset($listing['AgentUserInfo']['postal_code']) ? $listing['AgentUserInfo']['postal_code'] : ""; ?>
                <br><br><a href="<?php echo ADMIN_ROOT . "agents/agent_view"; ?>" class="btn waves-effect waves-light cyan">BACK</a>
            </div>
            <br/><br>
            <h5>Agent Jobs</h5>
            <div id="test3" class="s12">
                <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Job ID</th>
                            <th>Client Name</th>
                            <th> Job Completed On</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tfoot>
                    </tfoot>
                    <?php foreach ($joblisting as $value) { ?>
                    <?php 
                                $status = "";
                                if($value['AgentJob']['accepted_by_client'] == "" && $value['AgentJob']['declined_by_client'] == "" && $value['AgentJob']['completed_by_agent'] == "" && $value['AgentJob']['cancelled_by_agent'] == "" && $value['AgentJob']['disapproved_by_client'] == "" && $value['AgentJob']['approved_by_client'] == ""){
                                    $status = "Applied";
                                }
                                
                                if($value['AgentJob']['accepted_by_client'] == "1" && $value['AgentJob']['declined_by_client'] == "" && $value['AgentJob']['completed_by_agent'] == "1" && $value['AgentJob']['cancelled_by_agent'] == "" && $value['AgentJob']['disapproved_by_client'] == "" && $value['AgentJob']['approved_by_client'] == "1"){
                                    $status = "Completed";
                                }
                    
                    ?>
                    <?php if($status == "Applied" || $status == "Completed"){ ?>
                    <tr>
                        <td><?php echo $value['AgentJob']['job_id']; ?></td>
                        <td><?php echo $value['ClientUserInfo']['client_name']; ?></td>
                        <td><?php echo $value['AgentJob']['modified']; ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</article>