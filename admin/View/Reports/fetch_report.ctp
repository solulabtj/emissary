
<?php //pr($res); exit; ?>



<article>


    <div class="page-content">
        <div class="adv-table">

            <div class="s12 m12 24">
                <!-- ui tabs -->
                <div class="s12 m12 l12">

                    <div class="s12 tab-content">


                        <div id="test3" class="s12">
                            <table id="example" class="responsive-table display" cellspacing="0">

                                <script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/jquery-1.11.2.min.js"></script> 

                                <script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/plugins/data-tables/js/jquery.dataTables.min.js"></script>

                                <script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/plugins/data-tables/data-tables-script.js"></script> 

                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        $('#example').DataTable();
                                    });

                                </script>

                                <thead>
                                    <tr>
                    	<?php if($type==1) { ?>


                                        <th>Agent Name</th>
                                        <th>Email Address</th>
                                        <th>Rank</th>

                                        <th>Applied Jobs</th>
                                        <th>Completed jobs</th>
                                        <th>Test Status</th>

			<?php } else if($type==2) { ?>

                                        <th>Client Name</th>
                                        <th>Email Address</th>
                                        <th>On Going Jobs</th>
                                        <th>Total Posted Jobs</th>
                                        <th>Total Submissions Received</th>
                                        <th>Last Payment Made</th>
                                        <th>Status</th>

                        <?php } else if($type==3) { ?>

                                        <th>Agent Name</th>
                                        <th>Agent Score</th>
                                        <th>Maximum Score</th>

                                        <th>Answered Questions</th>
                                        <th>Total Questions</th>
                                        <th>Percentage</th>
                                        <th>Pass/Fail</th>

                        <?php } else if($type==4) { ?>

                                        <th>Agent Name</th>
                                        <th>Agent Email Address</th>
                                        <th>Booking Date</th>
                                        <th>Booking Code</th>
                                        <th>Test Cost</th>
                                        <th>Test Status</th>                                                                    

                        <?php } else if($type==5) { ?>

                                        <th>Payee Name</th>
                                        <th>Payee Email Address</th>
                                        <th>Transaction ID</th>
                                        <th>Payment Time</th>
                                        <th>Payment Amount</th>
                                        
                        <?php } else if($type==6) { ?>

                                        <th>Payee Name</th>
                                        <th>Payee Email Address</th>
                                        <th>Transaction ID</th>
                                        <th>Payment Time</th>
                                        <th>Payment Amount</th>                
                        <?php } ?>

                                    </tr>
                                </thead>

                                <tfoot>
                                </tfoot>

                                <tbody>

					<?php if($type==1) {  foreach($res as $row) {  ?>
                                        <?php 
                                             $rank_id  = $row['aui']['agent_rank']; 
                                                if($rank_id == "1"){
                                                    $rank = "Pro";
                                                }
                                                if($rank_id == "2"){
                                                    $rank = "Expert";
                                                }
                                                if($rank_id == "3"){
                                                    $rank = "Advanced";
                                                }
                                                if($rank_id == "4"){
                                                    $rank = "Normal";
                                                }
                                                if($rank_id == "5"){
                                                    $rank = "Beginner";
                                                }
                                                if($rank_id >= "6"){
                                                    $rank = "Not Defined";
                                                }

                                                 if($rank_id == "0"){
                                                    $rank = "Not Defined";
                                                }

                                                if($row['aui']['test_passed'] == "1"){
                                                    $test = "Passed";
                                                }else{
                                                    $test = "Failed";
                                                }
                                        ?>
                                    <tr>
                                        <td><?php echo $row['aui']['agent_name']; ?></td>
                                        <td><?php echo $row['aui']['email_address']; ?></td>
                                        <td><?php echo $rank; ?></td>
                                        <td><?php echo $row['0']['applied_count']; ?></td>
                                        <td><?php echo $row['0']['completed_count']; ?></td>
                                        <td><?php echo $test; ?></td>

                                    </tr>

					<?php } } ?>

                                    <?php if($type==2) { foreach($res as $row) { ?>  
                                    <?php if($row['cui']['status'] == 1){ $status = "Active"; }else{ $status = "Inactive"; } ?>    
                                    <tr>
                                        <td><?php echo $row['cui']['client_name']; ?></td>
                                        <td><?php echo $row['cui']['email_address']; ?></td>
                                        <td><?php echo $row['0']['ongoing_jobs']; ?></td>
                                        <td><?php echo $row['0']['jobs_posted']; ?></td>
                                        <td><?php echo $row['0']['submission_received']; ?></td>
                                        <td><?php echo $row['cs']['modified']; ?></td>
                                        <td><?php echo $status; ?></td>

                                    </tr>

					<?php } } ?>

                                    <?php if($type==3) {  foreach($res as $row) {  ?>  
                                    <tr>
                                        <td><?php echo $row['aui']['agent_name']; ?></td>
                                        <td><?php echo $row['tr']['agent_scored']; ?></td>
                                        <td><?php echo $row['tr']['maximum_score']; ?></td>
                                        <td><?php echo $row['tr']['questions_answered']; ?></td>
                                        <td><?php echo $row['tr']['total_questions']; ?></td>
                                        <td><?php echo $row['tr']['percentage_earned']; ?></td>
                                        <td><?php if($row['tr']['is_passed']==1) { echo "Pass"; } else { echo "Fail"; } ?></td>

                                    </tr>
					<?php } } ?>

                    <?php if($type==4) { foreach($res as $row) {  ?>  
                                    <tr>
                                        <td><?php echo $row['aui']['agent_name']; ?></td>
                                        <td><?php echo $row['aui']['email_address']; ?></td>
                                        <td><?php if($row['tb']['created'] != "0000-00-00 00:00:00"){ echo date("m/d/Y",strtotime($row['tb']['created']));} ?></td>
                                        <td><?php echo $row['tb']['test_booking_code']; ?></td>                                       
                                        <td><?php echo $row['tb']['test_cost']." KWD"; ?></td>
                                        <td><?php if($row['tb']['test_taken'] == 1){ echo "Taken";}else{ echo "Pending";} ?></td>
                                    </tr>
					<?php } } ?>

                    <?php if($type==5) { foreach($res as $row) {  ?>  

                                    <tr>
                                        <td><?php echo $row['aui']['agent_name']; ?></td>

                                        <td><?php echo $row['aui']['email_address']; ?></td>

                                        <td><?php echo $row['ap']['transaction_id']; ?></td>

                                        <td><?php if($row['ap']['payment_time'] != "0000-00-00 00:00:00"){ echo date("m/d/Y",  strtotime($row['ap']['payment_time']));} ?></td>

                                        <td><?php echo $row['ap']['amount_paid']." KWD"; ?></td>
                                    </tr>
                    <?php } } ?>                
                                 <?php if($type==6) { foreach($res as $row) {  ?>  

                                    <tr>
                                        <td><?php echo $row['cui']['client_name']; ?></td>

                                        <td><?php echo $row['cui']['email_address']; ?></td>

                                        <td><?php echo $row['cs']['transaction_id']; ?></td>

                                        <td><?php if($row['cs']['transaction_time'] != "0000-00-00 00:00:00"){ echo date("m/d/Y",  strtotime($row['cs']['transaction_time']));} ?></td>

                                        <td><?php echo $row['cs']['amount_paid']." KWD"; ?></td>
                                    </tr>
                
                    <?php } }  ?>	

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- ui tabs end -->
            </div>
        </div>
    </div>
</article>