<?php

/*echo "<pre>";
print_r($data['cities']);
echo "</pre>";
exit();
*/
$citydata = $data['cities'];
?>
<?php  //echo "<pre>";print_r($citydata);exit;?>

<script type="text/javascript">
    jQuery(function ($) {
        $("#start_date").mask("9999-99-99", {placeholder: 'yyyy-mm-dd'});
        $("#end_date").mask("9999-99-99", {placeholder: 'yyyy-mm-dd'});
    });
</script>

<div id="content" class="span10">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <?php
			 
            echo $this->Html->link(
    'Home',
    array(
        'controller' => 'home',
        'action' => 'index',
        'full_base' => true
    )
); ?>
            <i class="icon-angle-right"></i>
        </li>
        <li><?php echo $this->Html->link("Reports", array('controller' => 'categorydetails', 'action' => 'maincategory')); ?>
        </li>
    </ul>

    <div class="row-fluid sortable">
         <?php echo $this->Session->flash(); ?>

        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Reports</h2>

            </div>
            <div class="box-content">

                <form class="form-horizontal" name="reportsFrom" id="reportsFrom">  
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Start Date</label>
                        <div class="controls">
                                <?php
                                echo $this->Form->text('start_date', array('id' => 'start_date', 'required' => true, 'type' => 'input', 'class' => 'span6 typeahead'));
                                echo $this->Form->error('start_date', array('class' => 'error-message'));
                                ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">End Date</label>
                        <div class="controls">
                                <?php
                                echo $this->Form->text('end_date', array('id' => 'end_date', 'required' => true, 'type' => 'input', 'class' => 'span6 typeahead'));
                                echo $this->Form->error('end_date', array('class' => 'error-message'));
                                ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Cities </label>
                        <div class="controls">

                            <select name="city_id" id="city_id">

                                    <?php foreach ($citydata as $cityrow) {
                                        ?>
                                <option value="<?php echo $cityrow['City']['city_id']; ?>"><?php echo $cityrow['City']['name']; ?></option>
                                        <?php
                                    }
                                    ?>
                            </select>

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Type</label>
                        <div class="controls">

								<?php 
								$options =array(
									'1' => 'User',
									'2' => 'Merchant',
									'3' => 'Deals',
									'4' => 'Services',
									'5' => 'Promocodes'
								);

				echo $this->Form->input("",array('type' => 'select', 'options' => $options,'id' => 'viewType'));
								
							?>

                        </div>
                    </div>

                    <div class="control-group">

                        <div class="controls">
                            <input type="submit" id="submit" value="submit" name="Submit">  
                        </div>
                    </div>

                </form> 



                <div id="reportsData"></div>
            </div>
        </div><!--/span-->

    </div><!--/row-->
</div>		

<script>

    $('#submit').click(function (event) {
        startdate = $("#start_date").val();
        enddate = $("#end_date").val();
        city = $("#city_id").val();
        type = $("#viewType").val();
        //alert(form);	
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "http://appsolab.com/lookforsure/admin/reports/fetch_report",
            data: {startdate: startdate,
                enddate: enddate,
                city: city,
                type: type},
            success: function (data) {
                $('#reportsData').replaceWith($('#reportsData').html(data));
            }

        });
        event.preventDefault();
        return false;  //stop the actual form post !important!

    });

</script>