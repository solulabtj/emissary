<style>
    .error{
        color: red;
    }
</style>

<article>
    <header class="darken-1 panel-heading"> 
        <strong>Reports</strong> 
        <span class="tools pull-right"> 
            <a href="javascript:;" class="fa fa-chevron-down"></a> 
        </span>
    </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <div class="s12 m12 24">
                <!-- ui tabs -->
                <div class="s12 m12 l12">                  
                    <div class="s12 tab-content">
                        <div id="test1" class="s12">
                            <form method="post" >	

                                <div class="input_field col s12 m12" style="padding-bottom:15px;">
                                    <label class="col s12 m2">Type</label>                 	
                                    <select name="type" id="type" >
                                        <option value="0">Select Report Type
                                        <option value="1">Agents
                                        <option value="2">Clients
                                        <option value="3">Test Result
                                        <option value="4">Test Booking
                                        <option value="5">Test Payments
                                        <option value="6">Subscriptions Payments    
                                    </select>                    
                                </div>

                                <div id="start_date_div" class="input_field col s12 m12" style="display:none;" >
                                    <label class="col s12 m2">Active From</label> <label class="col s12 m2" style="display:none;">Start Date</label>
                                    <input type="text" class="col s12 m4 validate start_date" name="start_date" id="startdate" readonly="true">
                                </div>

                                <div id="end_date_div" class="input_field col s12 m12" style="display:none;">
                                    <label class="col s12 m2">End Date</label>
                                    <input type="text" class="col s12 m4 validate" class="end_date" name="end_date" id="enddate" readonly="true" >
                                </div>

                                <div id="rank_div" class="input_field col s12 m12" style="padding-bottom:15px;display:none;">
                                    <label class="col s12 m2">Rank</label>                 	
                                    <select id="rank"  name = "rank" >
                                        <option value="0">Select Rank</option>                                
                                        <?php foreach ($rank as $value) { ?>                                    
                                            <option value="<?php echo $value['agent_ranks']['rank']; ?>"><?php echo $value['agent_ranks']['rank_title']; ?></option>
                                        <?php } ?> 
                                    </select>                   
                                </div>  

                                <div id="test_status_div" class="input_field col s12 m12" style="padding-bottom:15px;display:none;">
                                    <label class="col s12 m2">Test Status</label>                 	
                                    <select id="test_status"  name = "test_status" >
                                        <option value="n">Select Status</option>
                                        <option value="1">Passed</option>
                                        <option value="0">Failed</option>
                                    </select>                    
                                </div>

                                <div id="booking_test_status_div" class="input_field col s12 m12" style="padding-bottom:15px;display:none;">
                                    <label class="col s12 m2">Booking Test Status</label>                 	
                                    <select id="booking_test_status"  name = "booking_test_status" >
                                        <option value="n">Select Status</option>
                                        <option value="1">Taken</option>
                                        <option value="0">Pending</option>
                                    </select>                    
                                </div>

                                <div id="client_status_div" class="input_field col s12 m12" style="padding-bottom:15px;display:none;">
                                    <label class="col s12 m2">Status</label>                 	
                                    <select id="client_status"  name = "client_status" >
                                        <option value="n">Select Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>                    
                                </div>


                                <div class="input_field col s12 m12">
                                    <label class="col s12 m2"></label>
                                    <a onclick="report_generate();" href="javascript:;" class="btn waves-effect waves-light cyan">Submit</a>
                                </div>
                            </form>
                        </div>                   
                    </div>
                </div>
                <!-- ui tabs end -->                           
                <div id="reportsDataContainer"><div id="reportsData"></div></div>
            </div>           
        </div>
        <div id="client_chart_div" style="clear:both;display:none;"></div>
    </div>
</article>

<script type="text/javascript" src="<?php echo WEBSITE_ROOT ?>app/webroot/js/googleChart.js"></script>
<script type="text/javascript">
                                        $(document).ready(function () {

                                            $('#startdate').datetimepicker({
                                                timepicker: false,
                                                format: 'm/d/Y',
                                                formatDate: '/m/d/Y',
                                            });
                                            
                                            $('#enddate').datetimepicker({
                                                timepicker: false,
                                                format: 'm/d/Y',
                                                formatDate: '/m/d/Y',
                                            });

                                            google.charts.load('current', {'packages': ['bar', 'corechart', 'line']});
                                            $("#type").on('change', function () {
                                                type = $("#type").val();

                                                if (type == 0) {
                                                    $("#client_chart_div").css("display", "none");
                                                    $("#start_date_div").css("display", "none");
                                                    $("#end_date_div").css("display", "none");
                                                    $("#rank_div").css("display", "none");
                                                    $("#test_status_div").css("display", "none");
                                                    $("#client_status_div").css("display", "none");
                                                    $("#booking_test_status_div").css("display", "none");
                                                }


                                                if (type == 1) {
                                                    google.charts.setOnLoadCallback(agent_report);
                                                    $("#start_date_div").css("display", "block");
                                                    $("#client_chart_div").css("display", "block");
                                                    $("#end_date_div").css("display", "none");
                                                    $("#rank_div").css("display", "block");
                                                    $("#test_status_div").css("display", "block");
                                                    $("#client_status_div").css("display", "none");
                                                    $("#booking_test_status_div").css("display", "none");
                                                }

                                                if (type == 2) {

                                                    google.charts.setOnLoadCallback(drawChart);
                                                    $("#start_date_div").css("display", "block");
                                                    $("#client_chart_div").css("margin-top", "20%");
                                                    $("#client_chart_div").css("display", "block");
                                                    $("#end_date_div").css("display", "none");
                                                    $("#rank_div").css("display", "none");
                                                    $("#test_status_div").css("display", "none");
                                                    $("#client_status_div").css("display", "block");
                                                    $("#booking_test_status_div").css("display", "none");
                                                }

                                                if (type == 3) {
                                                    google.charts.setOnLoadCallback(testResult);
                                                    $("#start_date_div").css("display", "block");
                                                    $("#client_chart_div").css("display", "block");
                                                    $("#end_date_div").css("display", "block");
                                                    $("#rank_div").css("display", "none");
                                                    $("#test_status_div").css("display", "block");
                                                    $("#client_status_div").css("display", "none");
                                                    $("#booking_test_status_div").css("display", "none");
                                                }

                                                if (type == 4) {
                                                    google.charts.setOnLoadCallback(testBooking);
                                                    $("#start_date_div").css("display", "block");
                                                    $("#client_chart_div").css("display", "block");
                                                    $("#start_date_div").css("display", "block");
                                                    $("#end_date_div").css("display", "block");
                                                    $("#rank_div").css("display", "none");
                                                    $("#test_status_div").css("display", "none");
                                                    $("#client_status_div").css("display", "none");
                                                    $("#booking_test_status_div").css("display", "block");
                                                }

                                                if (type == 5) {
                                                    google.charts.setOnLoadCallback(testPayment);
                                                    $("#client_chart_div").css("display", "block");
                                                    $("#start_date_div").css("display", "block");
                                                    $("#end_date_div").css("display", "block");
                                                    $("#rank_div").css("display", "none");
                                                    $("#test_status_div").css("display", "none");
                                                    $("#client_status_div").css("display", "none");
                                                    $("#booking_test_status_div").css("display", "none");
                                                }

                                                if (type == 6) {
                                                    google.charts.setOnLoadCallback(subPayment);
                                                    $("#client_chart_div").css("display", "block");
                                                    $("#start_date_div").css("display", "block");
                                                    $("#end_date_div").css("display", "block");
                                                    $("#rank_div").css("display", "none");
                                                    $("#test_status_div").css("display", "none");
                                                    $("#client_status_div").css("display", "none");
                                                    $("#booking_test_status_div").css("display", "none");
                                                }

                                            });
                                        });

                                        function report_generate()
                                        {
                                            startdate = $("#startdate").val();
                                            enddate = $("#enddate").val();
                                            rank = $("#rank").val();
                                            test_status = $("#test_status").val();
                                            client_status = $("#client_status").val();
                                            type = $("#type").val();
                                            booking_test = $("#booking_test_status").val();

                                            $.ajax({
                                                type: "POST",
                                                dataType: "html",
                                                url: "<?php echo ADMIN_ROOT . 'reports/fetch_report'; ?>",
                                                data: {startdate: startdate, enddate: enddate,
                                                    type: type, rank: rank, test_status: test_status, client_status: client_status, booking_test: booking_test
                                                },
                                                success: function (data) {
                                                    $("#client_chart_div").css("margin-top", 0);
                                                    $('#reportsData').replaceWith($('#reportsData').html(data));
                                                }

                                            });
                                            event.preventDefault();
                                            return false;  //stop the actual form post !important!
                                        }

                                        function testPayment() {

                                            $.ajax({
                                                type: "POST",
                                                dataType: "html",
                                                url: "<?php echo ADMIN_ROOT . 'reports/getTestPaymentGraphData'; ?>",
                                                //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
                                                success: function (response) {
                                                    response = JSON.parse(response);
                                                    var data = google.visualization.arrayToDataTable(response);

                                                    var options = {
                                                        chart: {
                                                            title: 'Test Payments Report',
                                                            subtitle: 'Dates & Payments ',
                                                        },
                                                        bars: 'vertical',
                                                        vAxis: {viewWindowMode: "explicit", viewWindow: {min: 0}},
                                                        height: 400,
                                                        colors: ['#1b9e77', '#d95f02', '#7570b3']
                                                    };

                                                    var chart = new google.charts.Bar(document.getElementById('client_chart_div'));

                                                    chart.draw(data, google.charts.Bar.convertOptions(options));
                                                }

                                            });
                                        }

                                        function subPayment() {

                                            $.ajax({
                                                type: "POST",
                                                dataType: "html",
                                                url: "<?php echo ADMIN_ROOT . 'reports/getSubPaymentGraphData'; ?>",
                                                //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
                                                success: function (response) {
                                                    response = JSON.parse(response);
                                                    var data = google.visualization.arrayToDataTable(response);

                                                    var options = {
                                                        chart: {
                                                            title: 'Subscriptions Payments Report',
                                                            subtitle: 'Dates & Payments ',
                                                        },
                                                        bars: 'vertical',
                                                        vAxis: {viewWindowMode: "explicit", viewWindow: {min: 0}},
                                                        height: 400,
                                                        colors: ['#1b9e77', '#d95f02', '#7570b3']
                                                    };

                                                    var chart = new google.charts.Bar(document.getElementById('client_chart_div'));

                                                    chart.draw(data, google.charts.Bar.convertOptions(options));
                                                }

                                            });
                                        }

                                        function testBooking() {

                                            $.ajax({
                                                type: "POST",
                                                dataType: "html",
                                                url: "<?php echo ADMIN_ROOT . 'reports/getTestBookingGraphData'; ?>",
                                                //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
                                                success: function (response) {
                                                    response = JSON.parse(response);
                                                    var data = google.visualization.arrayToDataTable(response);

                                                    var options = {
                                                        chart: {
                                                            title: 'Test Booking Report',
                                                            subtitle: 'Dates & Bookings ',
                                                        },
                                                        bars: 'vertical',
                                                        vAxis: {viewWindowMode: "explicit", viewWindow: {min: 0}},
                                                        height: 400,
                                                        colors: ['#1b9e77', '#d95f02', '#7570b3']
                                                    };

                                                    var chart = new google.charts.Bar(document.getElementById('client_chart_div'));

                                                    chart.draw(data, google.charts.Bar.convertOptions(options));
                                                }

                                            });
                                        }


                                        function drawChart() {

                                            $.ajax({
                                                type: "POST",
                                                dataType: "html",
                                                url: "<?php echo ADMIN_ROOT . 'reports/getjobSubmissionGraphData'; ?>",
                                                //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
                                                success: function (response) {
                                                    response = JSON.parse(response);
                                                    var data = google.visualization.arrayToDataTable(response);

                                                    var options = {
                                                        chart: {
                                                            title: 'Client Report',
                                                            subtitle: 'Submissions & Payment ',
                                                        },
                                                        bars: 'vertical',
                                                        vAxis: {viewWindowMode: "explicit", viewWindow: {min: 0}},
                                                        height: 400,
                                                        colors: ['#1b9e77', '#d95f02', '#7570b3']
                                                    };

                                                    var chart = new google.charts.Bar(document.getElementById('client_chart_div'));

                                                    chart.draw(data, google.charts.Bar.convertOptions(options));
                                                }

                                            });
                                        }
                                        function testResult() {

                                            $.ajax({
                                                type: "POST",
                                                dataType: "html",
                                                url: "<?php echo ADMIN_ROOT . 'reports/getTestResultGraphData'; ?>",
                                                //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
                                                success: function (res) {
                                                    result = JSON.parse(res);
                                                    var data = google.visualization.arrayToDataTable(result);
                                                    var options = {
                                                        title: 'Test Result'
                                                    };
                                                    var chart = new google.visualization.PieChart(document.getElementById('client_chart_div'));
                                                    chart.draw(data, options);
                                                }

                                            });
                                        }

                                        function agent_report() {

                                            $.ajax({
                                                type: "POST",
                                                dataType: "html",
                                                url: "<?php echo ADMIN_ROOT . 'reports/getGraphData'; ?>",
                                                //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
                                                success: function (response) {
                                                    // console.log(response.toString());
                                                    var data = new google.visualization.DataTable();
                                                    response = JSON.parse(response);
                                                    data.addColumn('string', 'X');
                                                    $.each(response.finalArr, function (index, value) {
                                                        // alert(value.agent_name);
                                                        data.addColumn('number', value.agent_name);
                                                    });

                                                    $.each(response.month, function (index, value) {
                                                        data.addRows([value]);
                                                    });
//                                        data.addRows([
//                                            [0, 0, 0], [1, 10, 5], [2, 23, 15], [3, 17, 9], [4, 18, 10], [5, 9, 5],
//                                            [6, 11, 3], [7, 27, 19], [8, 33, 25], [9, 40, 32], [10, 32, 24], [11, 35, 27]
//                                        ]);

                                                    var options = {
                                                        hAxis: {
                                                            title: 'Month'
                                                        },
                                                        vAxis: {
                                                            title: 'Completed Jobs',
                                                            viewWindowMode: "explicit",
                                                            viewWindow: {min: 0}
                                                        },
                                                        series: {
                                                            1: {curveType: 'function'}
                                                        }

                                                    };

                                                    var chart = new google.visualization.LineChart(document.getElementById('client_chart_div'));
                                                    chart.draw(data, options);
                                                }

                                            });
                                        }


</script>