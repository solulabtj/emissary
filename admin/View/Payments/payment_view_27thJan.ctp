<script>
	
	
	
</script>

<article>
<header class="darken-1 panel-heading"> <strong>Payment List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
<?php echo $this->Session->flash(); ?>
                      <h2><i class="halflings-icon user"></i><span class="break"></span>Payment List</h2>
                      
                      
                   <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                 
                  <div class="s12 tab-content">
                    <?php //pr($listing); exit; ?>
                    <div id="test3" class="s12">
                     <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                        
                            <th>Agent Name</th>
                            <th>Client Name</th>
                            <th>Amount Paid</th>
                            <th>Payment Time</th>
                            <th>Transaction ID</th>
                            
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
                      
					<?php foreach($listing as $row) {  ?>  
                        <tr>
                            <td><?php echo $row['AgentUserInfo']['agent_name']; ?></td>
                            
                            <td><?php echo $row['ClientUserInfo']['client_name']; ?></td>
                            
                            <td><?php echo $row['Payment']['amount_paid']; ?></td>
                            
                            <td><?php echo date("jS F, Y h:i A",strtotime($row['Payment']['payment_time'])); ?></td>
                            
                            <td><?php echo $row['Payment']['transaction_id']; ?></td>
                            
                        </tr>
					<?php } ?>	
						
                       </tbody>
                  </table>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>