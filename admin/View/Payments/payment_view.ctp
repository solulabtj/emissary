<script>
    
    function modal_callrefund(transaction_id, payment_id)
    {
        $('#payment_id').val(payment_id);
        $('#transactionID').val(transaction_id);
        $('#modal_checkwaitlist').show();
    }

    function cancel_refund()
    {
        window.location.reload();
    }

    function onlyNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

</script>

<article>
    <header class="darken-1 panel-heading"> <strong>Payment List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
            <!--<a href="javascript:;" class="fa fa-times"></a>--> 
        </span> </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <?php echo $this->Session->flash(); ?>                              
            <div class="s12 m12 24">
                <!-- ui tabs -->
                <select id="user_type">
                    <option value="0">Select User</option>
                    <option value="1">Agent</option>
                    <option value="2">Client</option>
                </select>
                <br/>
                <br/>
                <div class="s12 m12 l12" id="agent_user" style="display:none !important;">
                    <div class="s12 tab-content">                        
                        <div id="test3" class="s12">
                            <table id="" class="responsive-table display datatable" cellspacing="0">
                                <thead>
                                    <tr>                        	
                                        <th>Transaction ID</th>
                                        <th>Payee</th>                            
                                        <th>Amount</th>
                                        <th>Payment Type</th>
                                        <th>Payment Status</th>
                                        <th>Payment Time</th>                            
                                        <!--<th>Action</th>-->
                                    </tr>
                                </thead>
                                <tfoot>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($listing as $row) { ?>  
                                        <tr>
                                            <td><?php echo $row['TestBooking']['transaction_id']; ?></td>
                                            <td><?php echo $row['AgentUserInfo']['agent_name']; ?></td>    
                                            <td><?php echo $row['TestBooking']['test_cost'] . " KWD"; ?></td>
                                            <td><?php echo $row['TestBooking']['payment_type']; ?></td>
                                            <td><?php echo $row['TestBooking']['payment_status']; ?></td>
                                            <td><?php
                                                echo date("m-d-Y", strtotime($row['TestBooking']['created']));
                                                ?>
                                            </td>
                                            <!--<td><a onclick="modal_callrefund('<?php echo $row['TestBooking']['transaction_id'] ?>', '<?php echo $row['TestBooking']['test_booking_id'] ?>');" class="waves-effect waves-light modal-trigger cyan-text">Refund Payment</a></td>-->
                                        </tr>
                                    <?php } ?>	
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="s12 m12 l12" id="client_user" style="display:none !important;">
                    <div class="s12 tab-content">                        
                        <div id="test3" class="s12">
                            <table id="" class="responsive-table display datatable" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Transaction ID</th>
                                        <th>Payee</th>                            
                                        <th>Amount</th>
                                        <th>Payment Type</th>
                                        <th>Payment Status</th>
                                        <th>Payment Time</th>                            
                                        <!--<th>Action</th>-->
                                    </tr>
                                </thead>
                                <tfoot>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($listings as $row) { ?>  
                                        <tr>
                                            <td><?php echo $row['client_subscriptions']['transaction_id']; ?></td>
                                            <td><?php echo $row['client_user_infos']['client_name']; ?></td>    
                                            <td><?php echo $row['client_subscriptions']['amount_paid'] . " KWD"; ?></td>
                                            <td><?php echo "Subscription"; ?></td>
                                            <td><?php echo $row['client_subscriptions']['transaction_status']; ?></td>
                                            <td><?php
                                                echo date("m-d-Y", strtotime($row['client_subscriptions']['transaction_time']));
                                                ?>
                                            </td>
                                            <!--<td><a onclick="modal_callrefund('<?php echo $row['TestBooking']['transaction_id'] ?>', '<?php echo $row['TestBooking']['test_booking_id'] ?>');" class="waves-effect waves-light modal-trigger cyan-text">Refund Payment</a></td>-->
                                        </tr>
                                    <?php } ?>	
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>      
                <!-- ui tabs end -->
            </div>
        </div>
    </div>
</article>
<script>
    
    
    
    $("#user_type").on("change",function(){
        var val = $(this).val();
        
        if(val == 1){
            $("#agent_user").css("display","block");
            $("#client_user").css("display","none");
        }else if(val == 2){
            $("#agent_user").css("display","none");
            $("#client_user").css("display","block");
        }else{
            $("#agent_user").css("display","none");
            $("#client_user").css("display","none");
        }
    });
</script>    