<script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/jquery-1.11.2.min.js"></script> 
<!-- Start Page Loading -->
<!--<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>-->
<!-- End Page Loading --> 
<!-- START HEADER -->
<header id="header" class="page-topbar"> 
  <!-- start header nav-->
  
  
  
  <div class="navbar-fixed">
  
  <ul class="right hide-on-med-and-down" style="position:relative; z-index:9999999;">
          <!-- <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a> </li>
          <li><a class="tooltipped col s4 offset-s4 l2 offset-l1" data-position="left" data-delay="50" data-tooltip="Help"><i class="mdi-action-help"></i></a></li>-->
          <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle active">
                        <i class="mdi-social-person"></i>  <i class="mdi-navigation-arrow-drop-down"></i>
                    </a>
                    <!-- <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button" aria-expanded="false">
              <span class="caret"></span>
            </button> -->
                    <ul class="dropdown-menu dropdown-user in">
                        <li><a href="#" class="active"><i class="mdi-social-person"></i> User Profile</a></li>
                        <li><a href="#" class="active"><i class="mdi-action-settings"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo ADMIN_ROOT.'admins/logout'; ?>"><i class="mdi-action-exit-to-app"></i>Logout</a>
                        </li>
                    </ul>
                    <!-- <ul class="dropdown-menu">
              <li><a href="006_components@fa-briefcase%252Fdropdowns.html#" tabindex="-1">Action</a></li>
              <li><a href="006_components@fa-briefcase%252Fdropdowns.html#" tabindex="-1">Link</a></li>
              <li><a href="006_components@fa-briefcase%252Fdropdowns.html#" tabindex="-1">Link</a></li>
              <li class="divider"></li>
              <li><a href="006_components@fa-briefcase%252Fdropdowns.html#" tabindex="-1">Link</a></li>
            </ul> -->
           
                </li> 
        </ul>
  
    <nav class="cyan">
      <div class="nav-wrapper">
        <ul class="left">
        	<li class="no-hover"><a class="menu-sidebar-collapse btn-floating btn-flat btn-medium waves-effect waves-light cyan" data-activates="slide-out" href="#"><i class="mdi-navigation-menu"></i></a></li>
          <li>
          <a href="<?php echo ADMIN_ROOT ?>admins/dashboard" class="brand-logo darken-1"><img src="<?php echo ADMIN_ROOT ?>images/logo_nav.png" alt="Emissary"></a>
            <!-- <h1 class="logo-wrapper"><a href="<?php echo ADMIN_ROOT ?>home/index" class="brand-logo darken-1"><img src="<?php echo ADMIN_ROOT ?>images/logo_nav.png" alt="Emissary"></a> <span class="logo-text">Emissary</span></h1> -->
          </li>
        </ul>
        <!-- <div class="header-search-wrapper hide-on-med-and-down"> <i class="mdi-action-search"></i>
          <input type="text" name="Search" class="header-search-input z-depth-2" placeholder=""/>
        </div> -->




        
        <!-- <ul class="nav navbar-top-links navbar-right innerpagetop-nav in">
                
                <li class="dropdown active">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle active">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user in">
                        <li><a href="#" class="active"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#" class="active"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    
                </li>
                
            </ul> -->
      </div>
    </nav>
  </div>
  <!-- end header nav--> 
</header>
<!-- END HEADER --> 