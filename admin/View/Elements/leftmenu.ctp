 <!-- START LEFT SIDEBAR NAV-->
              <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y collapsible collapsible-accordion">
                  <!-- <li class="user-details cyan darken-2">
                    <div class="row">
                      <div class="col col s4 m4 l4"> <img src="<?php echo ADMIN_ROOT ?>images/avatar.jpg" alt="" class="circle responsive-img valign profile-image"> </div>
                      <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                          <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a> </li>
                          <li><a href="#"><i class="mdi-action-settings"></i> Settings</a> </li>
                          <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a> </li>
                          <li class="divider"></li>
                          <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a> </li>
                          <li><a href="<?php echo ADMIN_ROOT.'admins/logout'; ?>"><i class="mdi-hardware-keyboard-tab"></i> Logout</a> </li>
                        </ul>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">John Doe<i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Administrator</p>
                      </div>
                    </div>
                  </li> -->
                  <!--<li class="bold active"><a href="<?php //echo ADMIN_ROOT ?>home/index" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a> </li>-->
                  		<!--<li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" placeholder="Search..." class="form-control">
                                <span class="input-group-btn">
                                <button type="button" class="btn btn-default">
                                    <i class="mdi-action-search"></i>
                                </button>
                            </span>
                            </div>
                        </li>-->
                        <li class="bold"><a href="<?php echo ADMIN_ROOT ?>admins/profile" class="waves-effect waves-cyan"><i class="mdi-action-settings"></i> Admin Settings</a> </li>
                  <li class="bold"><a href="<?php echo ADMIN_ROOT ?>admins/dashboard" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i>Dashboard</a> </li>
                  <li class="bold"><a href="<?php echo ADMIN_ROOT ?>jobs/job_category" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i>Job Categories</a> </li>
                   <li class="bold"><a href="<?php echo ADMIN_ROOT ?>notifications/notification_view" class="waves-effect waves-cyan"><i class="mdi-action-settings-display"></i>Notification Settings</a></li> 
                  <li class="bold"><a href="<?php echo ADMIN_ROOT ?>payments/payment_view" class="waves-effect waves-cyan"><i class="mdi-action-payment"></i>Payments</a> </li>
                   <li class="bold"><a href="<?php echo ADMIN_ROOT ?>reports/reports" class="waves-effect waves-cyan"><i class="mdi-navigation-menu"></i>Reports</a> </li>
                   <li class="bold"><a href="<?php echo ADMIN_ROOT ?>surveys/survey_list" class="waves-effect waves-cyan"><i class="mdi-editor-format-list-bulleted"></i>Survey List</a> </li>
                  
                  <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-communication-email"></i>Surveys Management</a>
                   <div class="collapsible-body">
                      <ul>
                        <!--<li><a href="<?php //echo ADMIN_ROOT ?>surveys/survey_category"><i class="mdi-hardware-keyboard-arrow-right"></i><span>Survey Categories</span></a></li>-->
                         <li><a href="<?php echo ADMIN_ROOT ?>surveys/question_view"><i class="mdi-hardware-keyboard-arrow-right"></i><span>Survey Predefined Questions</span></a></li>
                       
                      </ul>
                    </div>
                  </li>
                  
                  <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-social-person"></i>Test Management</a>
                    <div class="collapsible-body">
                      <ul>
                      	<li><a href="<?php echo ADMIN_ROOT ?>questions/question_view"><i class="mdi-hardware-keyboard-arrow-right"></i><span>Manage Test questions</span></a></li>
                        <li><a href="<?php echo ADMIN_ROOT ?>questions/test_settings"><i class="mdi-hardware-keyboard-arrow-right"></i><span>Test Configuration</span></a></li>
                      
                        
                      </ul>
                    </div>
                  </li>
                  <li class="bold"><a href="<?php echo ADMIN_ROOT ?>results/test_result" class="waves-effect waves-cyan"><i class="mdi-communication-email"></i>Test Results</a> </li>
                  
                  <li class="bold"><a href="<?php echo ADMIN_ROOT ?>packages/manage_package" class="waves-effect waves-cyan"><i class="mdi-communication-email"></i>Manage Package</a> </li>
                  
                  
                  
                  <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-social-person"></i>User Management</a>
                    <div class="collapsible-body">
                      <ul>
                      <li><a href="<?php echo ADMIN_ROOT ?>agents/agent_view"><i class="mdi-hardware-keyboard-arrow-right"></i><span>Agents</span></a></li>
                        <li><a href="<?php echo ADMIN_ROOT ?>clients/client_view"><i class="mdi-hardware-keyboard-arrow-right"></i><span>Client</span></a></li>
                        
                      </ul>
                    </div>
                  </li>
                  
                  <li><a href="<?php echo ADMIN_ROOT.'admins/logout'; ?>" ><i class="mdi-action-exit-to-app" ></i><span>Logout<span></a></li>
                 
                  <!--<li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-communication-email"></i>Notification Settings</a>
                    <div class="collapsible-body">
                      <ul>
                        <li><a href="<?php echo ADMIN_ROOT ?>admins/profile"><i class="mdi-communication-page"></i><span>On New Job Post</span></a></li>
                        <li><a href="<?php echo ADMIN_ROOT ?>admins/profile"><i class="mdi-communication-page"></i><span>On New Applied Job</span></a></li>
                        <li><a href="<?php echo ADMIN_ROOT ?>admins/profile"><i class="mdi-communication-page"></i><span>On New Completed Job</span></a></li>
                       
                      </ul>
                    </div>
                  </li>-->
                  
                  
                 
                  
                  
                 
           			<!--<li class="bold"><a href="<?php //echo ADMIN_ROOT ?>rankings/ranking_view" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i>Ranking System</a> </li>-->
                
      
                </ul>
              </aside>
              <!-- END LEFT SIDEBAR NAV-->