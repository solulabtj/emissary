<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
    
	function validate_settings()
	{
			var error = false;
            
			//var correctanswer = $('input[type=checkbox]:checked').length;
			
			var question_count = $('#question_count').val();
			var question_validate;
			var limit;
			for(question_validate=1;question_validate<question_count;question_validate++)
			{
				var test_question_description = $('#test_question_description_'+question_validate).val();
				
				var correctanswer = $('#apply_content_'+question_validate+' input[type=checkbox]:checked').length;
				//alert(correctanswer);
				
				if(test_question_description=='')
				{
					$('#error_question_'+question_validate).html('Survey Question Description is required.');
					error = true;
				}
				else
				{
					$('#error_question_'+question_validate).html('');
				}
				
				if(correctanswer===0)
				{
					$('#error_option'+question_validate+'_5').html('Check at least one checkbox.');
					error = true;
				}
				else
				{
					$('#error_option'+question_validate+'_5').html('');
				}
				
				for(limit=1;limit<5;limit++)
				{
					var option_require = $('#option_'+question_validate+'_'+limit).val();
					
					if(option_require=='')
					{
						$('#error_option'+question_validate+'_'+limit).html('Option is required.');
						error = true;
					}
					else
					{
						$('#error_option'+question_validate+'_'+limit).html('');
					}
				}
			}
			
            if (error) {
                return false;
            }
			
     }
	 
	 function lettersOnly(evt) {
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		  ((evt.which) ? evt.which : 0));
		if (charCode == 32)
			return true;
		if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		  (charCode < 97 || charCode > 122)) {
			return false;
		}
		else
			return true;
	}
	 
</script>


<article>
<header class="darken-1 panel-heading"> <strong>Add Question</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  <div class="s12 tab-content">
                  <form method="post" action="<?php echo ADMIN_ROOT.'questions/question_add'; ?>" >
				  <input type="hidden" name="question_count" id="question_count" value="2" />
                  
                   <div id="test1" class="s12">
                  
                  <div id="apply_question1" >
                   
                  <h4><i class="halflings-icon user"></i><span class="break"></span>Question 1</h4>
                  
                  <div class="input_field col s12 m12">
                  <label class="col s12 m2">Question Description</label>
                  <input type="text" class="col s12 m4 validate" name="test_question_description_1" id="test_question_description_1" value="">
                  
                  </div>
                   <div id="error_question_1" class="error"></div>
                   
                  <div id="apply_content_1" >
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option A</label>
                 <input type="text" name="option_1_1" id="option_1_1" class="col s12 m4 validate" value="">
                 <input type="checkbox" name="option1_1" id="option1_1" value="1" >Correct Answer
                   </div>
                   <div id="error_option1_1" class="error"></div>
                   
                  <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option B</label>
                 <input type="text" name="option_1_2" id="option_1_2" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="">
                 <input type="checkbox" name="option1_2" id="option1_2" value="1" >Correct Answer
                   </div>
                   <div id="error_option1_2" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option C</label>
                 <input type="text" name="option_1_3" id="option_1_3" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="" >
                 <input type="checkbox" name="option1_3" id="option1_3" value="1" >Correct Answer
                   </div>
                   <div id="error_option1_3" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option D</label>
                 <input type="text" name="option_1_4" id="option_1_4" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="" >
                 <input type="checkbox" name="option1_4" id="option1_4" value="1" >Correct Answer
                   </div>
                   <div id="error_option1_4" class="error"></div>
                   
                   <div id="error_option1_5" class="error"></div>
                   </div>
                   
                   </div>
                   
                   </div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_settings();" class="btn waves-effect waves-light cyan">Save All</button>
                 
                 <button type="button" id="add_question_test" class="btn waves-effect waves-light cyan">Add More Question</button>
                 
                 <button type="button" id="remove_question_test" class="btn waves-effect waves-light cyan" style="display:none;" >Remove Question</button>
                 
                   </div>
                    
                   </form> 
                  </div>
                  
              </div>
              
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>