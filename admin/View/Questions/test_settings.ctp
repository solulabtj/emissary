<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
    
	function validate_settings()
	{
			var error = false;
            
			var no_of_questions = $('#no_of_questions').val();			
			var test_cost = $('#test_cost').val();
			var test_duration = $('#test_duration').val();
			var passing_percentage = $('#passing_percentage').val();
			
			var test_overview = $('#test_overview').val();
			
			if(no_of_questions=='')
			{
				$('#error_questions').html('No of Questions is required.');
				error = true;
			}
			else if(no_of_questions==0)
			{
				$('#error_questions').html('No of Questions must be greater than zero.');
				error = true;
			}
			
			else
			{
				$('#error_questions').html('');
			}
			
			if(test_overview=='')
			{
				$('#error_overview').html('Test Overview is required.');
				error = true;
			}
			else
			{
				$('#error_overview').html('');				
			}
			
			if(test_cost=='')
			{
				$('#error_cost').html('Test cost is required.');
				error = true;
			}
			else if(test_cost==0)
			{
				$('#error_cost').html('Test cost must be greater than zero.');
				error = true;
			}
			else
			{
				$('#error_cost').html('');
			}
			
			if(test_duration=='')
			{
				$('#error_duration').html('Test duration is required.');
				error = true;
			}
			else if(test_duration==0)
			{
				$('#error_duration').html('Test duration must be greater than zero.');
				error = true;
			}
			else
			{
				$('#error_duration').html('');
			}
			
			if(passing_percentage=='')
			{
				$('#error_percentage').html('Passing Percentage is required.');
				error = true;
			}
			else if(passing_percentage==0)
			{
				$('#error_percentage').html('Passing Percentage must be greater than zero.');
				error = true;
			}
			else if( passing_percentage >= 100)
			{
				$('#error_percentage').html('Passing Percentage must be less than 100.');
				error = true;	
			}
			else
			{
				$('#error_percentage').html('');
			}
			
            if (error) {
                return false;
            }
			
     }
	 
	 function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
	}
	
	function lettersOnly(evt) {
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		  ((evt.which) ? evt.which : 0));
		if (charCode == 32)
			return true;
		if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		  (charCode < 97 || charCode > 122)) {
			return false;
		}
		else
			return true;
	}
	
</script>


<article>
<header class="darken-1 panel-heading"> <strong>Test Configuration</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  <?php //pr($listing[0]['TestSettings']); ?>
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                    <form method="post" action="<?php echo ADMIN_ROOT.'questions/test_settings'; ?>" >
					
					<input type="hidden" name="test_setting_id" value="<?php echo $listing[0]['TestSettings']['test_setting_id']; ?>" >
					
                    <div class="input_field col s12 m12">
                  <label class="col s12 m2">Test Overview</label>
                  
                  <textarea class="col s12 m4" rows="5" cols="5" name="test_overview" id="test_overview" ><?php echo $listing[0]['TestSettings']['test_overview']; ?></textarea>
              
                   </div>  
                   
                   <div id="error_overview" class="error"></div>
                   
                   <br/>&nbsp;                  
                    
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">No of Questions</label>
                 <input type="text" class="col s12 m4 validate" name="no_of_questions" id="no_of_questions" onkeypress="return onlyNumbers(event)" value="<?php echo $listing[0]['TestSettings']['no_of_questions']; ?>">
                   </div>
                   <div id="error_questions" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Test Cost</label>
                 <input type="text" name="test_cost" onkeypress="return onlyNumbers(event)" id="test_cost" class="col s12 m4 validate" value="<?php echo $listing[0]['TestSettings']['test_cost']; ?>">
                   </div>
                   <div id="error_cost" class="error"></div>
                   
                  <div class="input_field col s12 m12">
                  <label class="col s12 m2">Test Duration</label>
                 <input type="text" name="test_duration" id="test_duration" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="<?php echo $listing[0]['TestSettings']['test_duration']; ?>">
                   </div>
                   <div id="error_duration" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Passing Percentage</label>
                 <input type="text" name="passing_percentage" id="passing_percentage" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="<?php echo $listing[0]['TestSettings']['passing_percentage']; ?>" >
                   </div>
                   <div id="error_percentage" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_settings();" class="btn waves-effect waves-light cyan">Update Test Config</button>
                   </div>
                   </form>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>