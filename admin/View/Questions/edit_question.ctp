<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
    
	function validate_settings()
	{
			var error = false;
            
			var test_question_description = $('#test_question_description').val();
			var option_1 = $('#option_1').val();
			var option_2 = $('#option_2').val();
			var option_3 = $('#option_3').val();
			var option_4 = $('#option_4').val();
			
			var correctanswer = $('input[type=checkbox]:checked').length;
			
			if(test_question_description=='')
			{
				$('#error_questions').html('Question Description is required.');
				error = true;
			}			
			else
			{
				$('#error_questions').html('');
			}
			
			if(option_1=='')
			{
				$('#error_option1').html('Option A is required.');
				error = true;
			}			
			else
			{
				$('#error_option1').html('');
			}
			
			if(option_2=='')
			{
				$('#error_option2').html('Option B is required.');
				error = true;
			}			
			else
			{
				$('#error_option2').html('');
			}
			
			if(option_3=='')
			{
				$('#error_option3').html('Option C is required.');
				error = true;
			}			
			else
			{
				$('#error_option3').html('');
			}
			
			if(option_4=='')
			{
				$('#error_option4').html('Option D is required.');
				error = true;
			}			
			else
			{
				$('#error_option4').html('');
			}
			
			if(correctanswer==0)
			{
				$('#error_option5').html('Select at least one checkbox.');
				error = true;
			}			
			else
			{
				$('#error_option5').html('');
			}
			
            if (error) {
                return false;
            }
			
     }
	 
	 function lettersOnly(evt) {
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		  ((evt.which) ? evt.which : 0));
		if (charCode == 32)
			return true;
		if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		  (charCode < 97 || charCode > 122)) {
			return false;
		}
		else
			return true;
	}
	 
</script>

<?php //pr($singlerecord); ?>

<article>
<header class="darken-1 panel-heading"> <strong>Update Question</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
                  <!--<a href="javascript:;" class="fa fa-times"></a>--> 
                  </span> </header>
<br />
<div class="page-content">
<div class="adv-table">
                      <?php echo $this->Session->flash(); ?>
                      <div class="s12 m12 24">
                   <!-- ui tabs -->
                   <div class="s12 m12 l12">
                  
                  <div class="s12 tab-content">
                    <div id="test1" class="s12">
                  <form method="post" action="<?php echo ADMIN_ROOT.'questions/edit_question'; ?>" >
				
                  <input type="hidden" name="test_question_id" value="<?php echo $singlerecord[0]['TestQuestion']['test_question_id']; ?>" />
                    
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Question Description</label>
                  <input type="text" class="col s12 m4 validate" name="test_question_description" id="test_question_description" onkeypress="return lettersOnly(event)" value="<?php echo $singlerecord[0]['TestQuestion']['test_question_description']; ?>">
                  
                   </div>
                   <div id="error_questions" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option A</label>
                 <input type="text" name="option_1" id="option_1" class="col s12 m4 validate" value="<?php echo $singlerecord[0]['TestQuestionOption'][0]['answer_text']; ?>">
                 <input type="checkbox" name="option1" id="option1" value="1"<?php if($singlerecord[0]['TestQuestionOption'][0]['is_correct']=='1') { echo "checked='checked'"; } ?> >Correct Answer
                   </div>
                   <div id="error_option1" class="error"></div>
                   
                  <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option B</label>
                 <input type="text" name="option_2" id="option_2" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="<?php echo $singlerecord[0]['TestQuestionOption'][1]['answer_text']; ?>">
                 <input type="checkbox" name="option2" id="option2" value="1"<?php if($singlerecord[0]['TestQuestionOption'][1]['is_correct']=='1') { echo "checked='checked'"; } ?> >Correct Answer
                   </div>
                   <div id="error_option2" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option C</label>
                 <input type="text" name="option_3" id="option_3" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="<?php echo $singlerecord[0]['TestQuestionOption'][2]['answer_text']; ?>" >
                 <input type="checkbox" name="option3" id="option3" value="1"<?php if($singlerecord[0]['TestQuestionOption'][2]['is_correct']=='1') { echo "checked='checked'"; } ?> >Correct Answer
                   </div>
                   <div id="error_option3" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option D</label>
                 <input type="text" name="option_4" id="option_4" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="<?php echo $singlerecord[0]['TestQuestionOption'][3]['answer_text']; ?>" >
                 <input type="checkbox" name="option4" id="option4" value="1"<?php if($singlerecord[0]['TestQuestionOption'][3]['is_correct']=='1') { echo "checked='checked'"; } ?> >Correct Answer
                   </div>
                   <div id="error_option4" class="error"></div>
                   
                   <div id="error_option5" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2"></label>
                 <button type="submit" onclick="return validate_settings();" class="btn waves-effect waves-light cyan">Update Question</button>
                   </div>
                   </form>
                    </div>
                    
                  </div>
              </div>
                   <!-- ui tabs end -->
          </div>
          </div>
          </div>
</article>