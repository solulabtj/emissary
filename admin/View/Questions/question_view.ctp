<script>

    function edit_question(test_question_id)
    {
        window.location = '<?php echo ADMIN_ROOT; ?>questions/edit_question/' + test_question_id;
    }

    function delete_question(test_question_id)
    {

        var valid_del = window.confirm('Are you sure want to delete this record?');

        if (valid_del === true)
        {
            window.location = '<?php echo ADMIN_ROOT; ?>questions/delete_question/' + test_question_id;
        }

    }

</script>
<article>
    <header class="darken-1 panel-heading"> <strong>Test Question List</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
            <!--<a href="javascript:;" class="fa fa-times"></a>--> 
        </span> </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <?php echo $this->Session->flash(); ?>
                      <!--<h2><i class="halflings-icon user"></i><span class="break"></span>Question List</h2>-->
            <div class="panel-body"><i class="livicon"></i><a href="<?php echo ADMIN_ROOT . 'questions/question_add'; ?>" >Add New Question</a></div>
            <div class="s12 m12 24">
                <!-- ui tabs -->
                <div class="s12 m12 l12">
                    <div class="s12 tab-content">
                        <div id="test3" class="s12">
                            <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Test Question Description</th>
                                        <th>Answer A</th>
                                        <th>Answer B</th>
                                        <th>Answer C</th>
                                        <th>Answer D</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($listing as $row) { ?>  
                                        <tr>
                                            <td><?php echo $row['TestQuestion']['test_question_description']; ?></td>
                                            <?php foreach ($row['TestQuestionOption'] as $row_option) {
                                                if ($row_option['is_correct'] == 0) {
                                                    ?>
                                                    <td style="color:black;"><?php echo $row_option['answer_text']; ?></td>
                                                <?php } else { ?>
                                                    <td style="color:#00F;;"><?php echo $row_option['answer_text']; ?></td>
                                                <?php }
                                            }
                                            ?>
                                            <td class="center">
                                                <?php
                                                if ($row['TestQuestion']['is_blocked'] == 0) { //inactive
                                                    echo $this->Html->link('<span class="label">Active</span>', array('action' => 'activate_inactive/' . $row['TestQuestion']['test_question_id'] . '/' . $row['TestQuestion']['is_blocked']), array("title" => "Make Inactive", 'escape' => false));
                                                } else {
                                                    echo $this->Html->link('<span class="label label-success">Inactive</span>', array('action' => 'activate_inactive/' . $row['TestQuestion']['test_question_id'] . '/' . $row['TestQuestion']['is_blocked']), array("title" => "Make Active", 'escape' => false));
                                                }
                                                ?>
                                            </td>   
                                            <td>
                                                <a class="waves-effect waves-light darken-4" href="javascript:;" onclick="edit_question('<?php echo $row['TestQuestion']['test_question_id']; ?>');" ><i class="mdi-editor-mode-edit"></i></a>
                                           <!-- <a class="waves-effect waves-light darken-4" href="javascript:;" onclick="delete_question('<?php echo $row['TestQuestion']['test_question_id']; ?>');" ><i class="mdi-action-delete"></i></a>--> 
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ui tabs end -->
            </div>
        </div>
    </div>
</article>