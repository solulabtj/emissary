<style>
    .error{
        color: red;
    }

    .col s12 m4 validate 
    {
        color:#000;
    }

</style>

<script type="text/javascript">

    function validate_settings()
    {
        var error = false;

        var k1 = document.getElementsByName("cost[]");
        var y1 = k1.length;

        //alert(y1);

        var min_package_fee = $('#min_package_fee').val();

        if (min_package_fee == '' || min_package_fee == 0)
        {
            $('#error_fee').html('Minimum Package Fee should be not null or Zero.');
            error = true;
        } else
        {
            $('#error_fee').html('');
        }

        for (var x = 0; x < y1; x++)
        {
            if (k1[x].value == null || k1[x].value == "")
            {
                $('#error_' + x).html('Please fill price.');
                error = true;
            } else
            {
                $('#error_' + x).html('');
            }
        }

        if (error)
        {
            return false;
        }
    }

    function fun_AllowOnlyAmountAndDot(item, evt)
    {
        evt = (evt) ? evt : window.event;

        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode == 46)
        {
            var regex = new RegExp(/\./g)

            var count = $(item).val().match(regex).length;
            if (count > 1)
            {
                return false;
            }

        }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function lettersOnly(evt) {
        evt = (evt) ? evt : event;
        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
        if (charCode == 32)
            return true;
        if (charCode > 31 && (charCode < 65 || charCode > 90) &&
                (charCode < 97 || charCode > 122)) {
            return false;
        } else
            return true;
    }

</script>


<article>
    <header class="darken-1 panel-heading"> <strong>Manage Packages</strong> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a> 
            <!--<a href="javascript:;" class="fa fa-times"></a>--> 
        </span> </header>
    <br />
    <div class="page-content">
        <div class="adv-table">
            <?php echo $this->Session->flash(); ?>
            <div class="s12 m12 24">
                <!-- ui tabs -->
                <div class="s12 m12 l12">
                    <div class="s12 tab-content">
                        <form method="post" action="" >

                            <div id="test1" class="s12">

                                <div class="input_field col s12 m12">

                                    <?php
                                    if (isset($min_packages) && !empty($min_packages)) {

                                        foreach ($min_packages as $min_package) {
                                            ?>

                                            <input type="hidden" name="type" value="2" />

                                            <input type="hidden" name="min_package_id" value="<?php echo $min_package['MinPackage']['min_package_id']; ?>" />

                                            <label class="col s12 m2">Minimum Processing Fee</label>
                                            <input type="text" class="col s12 m4 validate" name="min_package_fee" id="min_package_fee" onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" value="<?php echo $min_package['MinPackage']['min_package_fee']; ?>">

                                            <label class="col s12 m2" >KWD</label>

                                            <?php }
                                        } else { ?>
                                        <input type="hidden" name="type" value="1" />
                                        <label class="col s12 m2">Minimum Processing Fee</label>
                                        <input type="text" class="col s12 m4 validate" name="min_package_fee" id="min_package_fee" onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" value="">

                                        <label class="col s12 m2" >KWD</label>

                                    <?php } ?>

                                </div> 
                                <div id="error_fee" class="error"></div>

                            </div>

                            <h5>Standard per Survey Processing Fee</h5>

                            <table id="" class="responsive-table display" cellspacing="0">

                                <thead>
                                    <tr>
                                        <th>Average Survey Volume</th>
                                        <th>Price</th>

                                    </tr>
                                </thead>

                                <tfoot>
                                </tfoot>

                                <tbody>

                                    <?php if (isset($package_details) && !empty($package_details)) {
                                        ?>

                                    <?php $i = 0;
                                    foreach ($package_details as $package_detail) {
                                        ?>                      

                                            <tr>

                                        <input type="hidden" name="package_detail_id[<?php echo $i ?>]" value="<?php echo $package_detail['PackageDetail']['package_detail_id']; ?>" />

                                        <td><label class="col s12 m2"><?php echo $package_detail['PackageDetail']['label_name']; ?></label></td>
                                        <input type="hidden" name="label_name[]" value="<?php echo $package_detail['PackageDetail']['label_name']; ?>">

                                        <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="<?php echo $package_detail['PackageDetail']['cost']; ?>" 
                                                   onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" /><label class="col s12 m2">KWD</label>

                                            <div class="error" id="error_<?php echo $i ?>" ></div> 
                                        </td>                           
                                        </tr>

                                    <?php $i++;
                                }
                            } else { ?>
                                    <tr>
                                        <td>
                                            <label class="col s12 m2">1,000 or Less</label>
                                            <input type="hidden" name="label_name[]" value="1,000 or Less">
                                        <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" /><label class="col s12 m2">KD</label>
                                            <div class="error" id="error_0" ></div> 
                                        </td>                           
                                    </tr>
                                    <tr>
                                        <td><label class="col s12 m2">1,001–3,000</label></td>
                                    <input type="hidden" name="label_name[]" value="1,001-3,000">
                                    <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" /><label class="col s12 m2">KD</label>

                                        <div class="error" id="error_1" ></div> 
                                    </td>
                                    </tr>
                                    <tr>
                                        <td><label class="col s12 m2">3,001-5,000</label></td>
                                    <input type="hidden" name="label_name[]" value="3,001-5,000">
                                    <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" /><label class="col s12 m2">KD</label>
                                        <div class="error" id="error_2" ></div> 
                                    </td>
                                    </tr>
                                    <tr>
                                        <td><label class="col s12 m2">5,001-8,000</label></td>
                                    <input type="hidden" name="label_name[]" value="5,001-8,000">
                                    <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" /><label class="col s12 m2">KD</label>

                                        <div class="error" id="error_3" ></div> 
                                    </td>
                                    </tr>
                                    <tr>
                                        <td><label class="col s12 m2">8,001-10,000</label></td>
                                    <input type="hidden" name="label_name[]" value="8,001-10,000">
                                    <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" /><label class="col s12 m2">KD</label>
                                        <div class="error" id="error_4" ></div> 
                                    </td>
                                    </tr>
                                    <tr>
                                        <td><label class="col s12 m2">10,001 or More</label></td>
                                    <input type="hidden" name="label_name[]" value="10,001 or More">
                                    <td><input type="text" class="col s12 m4 validate" name="cost[]" id="cost" value="" onkeypress="return fun_AllowOnlyAmountAndDot(this, event);" /><label class="col s12 m2">KD</label>
                                        <div class="error" id="error_5" ></div> 
                                    </td>
                                    </tr>
                                    <?php } ?>  			
                                </tbody>
                            </table>
                            
                            <div class="input_field col s12 m12">
                                <label class="col s12 m2"></label>
                                <button type="submit" onclick="return validate_settings();" class="btn waves-effect waves-light cyan">Save Package</button>
                            </div>
                        </form> 
                    </div>
                </div>

                <!-- ui tabs end -->
            </div>
        </div>
    </div>
</article>