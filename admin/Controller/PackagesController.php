<?php
class PackagesController extends AppController {
	
   public $uses = array('MinPackage','PackageDetail');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function manage_package() 
	{
		$this->layout = 'admin';
		
		if($this->request->is('post'))
		{			
			$post = $this->request->data;
			//pr($post);exit;
			
			if($post['type']=='1')
			{
				
				$data1['min_package_fee'] = $post['min_package_fee'];
			
				$data2 = $this->MinPackage->save($data1);
				
				for($start=0;$start<=5;$start++)
				{
					$data3[$start]['label_name'] = $post['label_name'][$start];
					
					$data3[$start]['cost'] = $post['cost'][$start];
					
				}
			}
			else if($post['type']=='2')
			{
				$data1['min_package_id'] = $post['min_package_id'];
				
				$data1['min_package_fee'] = $post['min_package_fee'];
			
				$data2 = $this->MinPackage->save($data1);
				
				for($start=0;$start<=5;$start++)
				{
					$data3[$start]['package_detail_id'] = $post['package_detail_id'][$start];
					
					$data3[$start]['label_name'] = $post['label_name'][$start];
					
					$data3[$start]['cost'] = $post['cost'][$start];					
				}
			}
			
			$data4 = $this->PackageDetail->saveAll($data3);
			
			if(isset($data2) && !empty($data2) || isset($data4) && !empty($data4))
			{
				$this->Session->setFlash('Package details added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'packages','action'=>'manage_package'));
				exit();
			}
			else
			{
				$this->Session->setFlash('Unable to add Package Details.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'packages','action'=>'manage_package'));
				exit();
			}
		}
		
		$package_details = $this->PackageDetail->find('all');
		
		$min_packages = $this->MinPackage->find('all');
		
		$this->set('package_details',$package_details);
		
		$this->set('min_packages',$min_packages);
	}
	
}

?>