<?php
class SurveysController extends AppController {
	
   public $uses = array('SurveyCategories','SurveyQuestion','SurveyQuestionOption');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function survey_category() {
                $this->layout = 'admin';
                $test_q = $this->SurveyCategories->find('all',array('fields'=>array(),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function activate_inactive($survey_category_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->SurveyCategories->id = $survey_category_id; //exit;
        
        if($this->SurveyCategories->saveField('is_active',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
            exit();
        }
        
    }
	
	function category_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
            $post = $this->request->data;
			//pr($post);
			
			$data['ip_address'] = $this->request->clientIp();
			$data['survey_category_tite'] = $post['survey_category_tite'];
			$data['is_active'] = '1';
			//pr($data);exit;
			if($this->SurveyCategories->save($data)){
                
                $this->Session->setFlash('Survey category added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'survey_category'));
                        
            }else{
                
                $this->Session->setFlash('Error ! Survey category not added.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'survey_category'));
            }
        }
        
        $this->layout = 'admin';
        		
	}
	
	function edit_jobcategory($survey_category_id=null){
		
		$this->layout = 'admin';
		
		$ID = $survey_category_id;
       
        if($this->request->is('post')){
            
            $survey_category_id = $this->request->data('survey_category_id');
            $survey_category_tite = $this->request->data('survey_category_tite');
            
            $query = $this->SurveyCategories->query("UPDATE survey_categories SET survey_category_tite='$survey_category_tite' WHERE survey_category_id = '$survey_category_id'");
            
			//pr($query); if($query || empty($query)) { echo "success"; } else { echo "failed"; } exit;
			
            if (isset($query)) {
                
                $this->Session->setFlash('Survey category updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
                exit();
				
            } else {
                $this->Session->setFlash('Unable to update Survey category.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
                exit();
            }
        }
        
		$data = $this->SurveyCategories->find('all',array('conditions'=>array('survey_category_id'=>$ID)));
		
        $this->set('singlerecord',$data);
		
    }
	
	public function question_view() {
                $this->layout = 'admin';
                $test_q = $this->SurveyQuestion->find('all',array('fields'=>array(),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function activate_inactive_view($survey_question_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->SurveyQuestion->id = $survey_question_id; //exit;
        
        if($this->SurveyQuestion->saveField('is_active',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
            exit();
        }
        
        
    }
	
	function question_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
            $post = $this->request->data;
			//pr($post);exit;			
			
			$data['ip_address'] = $this->request->clientIp();
			$data['survey_question_description'] = $post['survey_question_description'];
			$data['survey_question_marks'] = $post['survey_question_marks'];
			$data['survey_question_type'] = $post['type'];
			$data['is_default'] = '1';
			$data['is_active'] = '1';
			//pr($data);exit;
			if($this->SurveyQuestion->save($data)){
                
				$survey_question_id = $this->SurveyQuestion->id;
				
				$count = $post['option_count'];
				
				$type = $post['type'];
				
				$data2 = array();
				
				if($type=='R' || $type=='C')
				{
					for($i=1;$i<$count;$i++)
					{
						$data1['survey_question_id'] = $survey_question_id;
						$data1['answer_text'] = $post['option_'.$i];
						
						array_push($data2,$data1);
					}
					
					if($this->SurveyQuestionOption->saveAll($data2))
					{
						$this->Session->setFlash('Question added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
					}
					else
					{
						$this->Session->setFlash('Error ! Question not added.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
					}
				
				}
				else
				{
					 $this->Session->setFlash('Question added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
                   
				}
				
                    
            }else{
                
                $this->Session->setFlash('Error ! Question not added.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
            }
        }
        
        $this->layout = 'admin';
        		
	}
	
	
	function edit_question($survey_question_id=null){
		
		$this->layout = 'admin';
		
		$ID = $survey_question_id;
       
        if($this->request->is('post')){
            //$post = $this->request->data;
			//pr($post);exit;
			
			//echo $post['type']; exit;
            $survey_question_id = $this->request->data('survey_question_id');
            $survey_question_description = $this->request->data('survey_question_description');
			$survey_question_marks = $this->request->data('survey_question_marks');
			$survey_question_type = $this->request->data('type');
			//$is_active = '1';
            $post = $this->request->data;
			
            $query = $this->SurveyQuestion->query("UPDATE survey_questions SET survey_question_description='$survey_question_description',survey_question_marks='$survey_question_marks',survey_question_type='$survey_question_type',is_default='1' WHERE survey_question_id = '$survey_question_id'");
            
            if (isset($query)) {
                
				$count = $post['option_count'];
				
				$type = $post['type'];
				
				$data2 = array();
				
				if($type=='R' || $type=='C')
				{
					$query1 = $this->SurveyQuestionOption->query("SELECT *FROM survey_question_options WHERE survey_question_id = '$survey_question_id'");
					
					$number_of_records = sizeof($query1);
					
					if($number_of_records>0)
					{
						$query2 = $this->SurveyQuestionOption->query("DELETE FROM survey_question_options WHERE survey_question_id = '$survey_question_id'");
					}
					
					for($i=1;$i<$count;$i++)
					{
						$data1['survey_question_id'] = $survey_question_id;
						$data1['answer_text'] = $post['option_'.$i];
						
						array_push($data2,$data1);
					}
					
					if($this->SurveyQuestionOption->saveAll($data2))
					{
						$this->Session->setFlash('Question updated successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
					}
					else
					{
						$this->Session->setFlash('Error ! Question not updated.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
					}
				
				}
				
				else
				{
					$this->Session->setFlash('Question updated successfully.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
                exit();
				}
				
                
            } else {
                $this->Session->setFlash('Unable to update Question.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
                exit();
            }
        }
        
		$data = $this->SurveyQuestion->find('all',array('conditions'=>array('survey_question_id'=>$ID)));
		
		//pr($data);exit;
		
		$count = sizeof($data[0]['SurveyQuestionOption']);
		
        $this->set('singlerecord',$data);
		$this->set('count',$count);
		
    }
	
}

?>