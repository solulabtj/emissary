<?php
class PaymentsController extends AppController {
	
   public $uses = array('AgentPayment','Refund','TestBooking','ClientSubscription');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function payment_view() {
              
	    //pr(phpinfo()); exit;
		
		//error_reporting(E_ALL);
			    
		$this->layout = 'admin';
				
        $test_q = $this->TestBooking->find('all',array('recursive'=>1));
        $sub_q = $this->ClientSubscription->query("select client_subscriptions.transaction_id,client_subscriptions.transaction_time,client_subscriptions.amount_paid,client_subscriptions.transaction_status,client_user_infos.client_name from client_subscriptions LEFT JOIN client_user_infos ON client_subscriptions.client_id = client_user_infos.client_id");	
        $this->set('listing',$test_q);
        $this->set('listings',$sub_q);
	}
	
	public function refundpayment()
	{
		if($this->request->is('post'))
		{
			
			$data = $this->request->data;
			
			$file_path = '../class.refund.php';
			
			if(file_exists($file_path))
			{
				include_once($file_path);
				
				$request = new PayPalRefund();
				
				$aryData['currencyCode']='USD';
				
				$aryData['transactionID']=$data['transactionID'];
				
				$aryData['memo']='Refund payment test.';
				
				if($data['type']==1)
				{
					$aryData['refundType']='Partial';
					$aryData['amount']=$data['amount'];
					
					$obj = $request->refundAmount($aryData);
					
					if(isset($obj) && isset($obj['ACK']) && $obj['ACK']=='Success')
					{
						$data1['transaction_id'] = $data['transactionID'];
						$data1['payment_id'] = $data['payment_id'];
						$data1['refund_transaction_id'] = $obj['REFUNDTRANSACTIONID'];
						$data1['refund_type'] = 'P';
						$data1['refund_amount'] = $obj['GROSSREFUNDAMT'];
						$data1['refund_time'] = $obj['TIMESTAMP'];
						
						$this->Refund->save($data1);
					}
					
					else if(isset($obj['L_ERRORCODE0']))
					{
						echo $obj['L_LONGMESSAGE0']; exit;
					}
					
					if(isset($obj['ERROR_MESSAGE']) && !empty($obj['ERROR_MESSAGE']))
					{
						echo $obj['ERROR_MESSAGE']; exit;	
					}
					
					echo $obj['ACK']; exit;	
								
				}
				
				else
				{
					$aryData['refundType']='Full';
					
					$obj = $request->refundAmount($aryData);
					
					if(isset($obj) && isset($obj['ACK']) && $obj['ACK']=='Success')
					{
						$data2['transaction_id'] = $data['transactionID'];
						$data2['payment_id'] = $data['payment_id'];
						$data2['refund_transaction_id'] = $obj['REFUNDTRANSACTIONID'];
						$data2['refund_type'] = 'F';
						$data2['refund_amount'] = $obj['GROSSREFUNDAMT'];
						$data2['refund_time'] = $obj['TIMESTAMP'];
						
						$this->Refund->save($data2);
					}
					else if(isset($obj['L_ERRORCODE0']))
					{
						echo $obj['L_LONGMESSAGE0']; exit;
					}
					
					if(isset($obj['ERROR_MESSAGE']) && !empty($obj['ERROR_MESSAGE']))
					{
						echo $obj['ERROR_MESSAGE']; exit;	
					}
					
					echo $obj['ACK']; exit;
					
				}
								
			}
		}
	}
	
	public function partial_refund()
	{
		if($this->request->is('post'))
		{
			$data = $this->request->data;
			
			$partial_payment = $this->TestBooking->find('first',array('conditions'=>array('test_booking_id'=>$data['payment_id'],'transaction_id'=>$data['transactionID'],'test_cost > '=>$data['amount']),'recursive'=>-1));
			
			if(isset($partial_payment) && !empty($partial_payment))
			{
				echo "1"; exit;
			}
			else
			{
				echo "0"; exit;
			}
			
		}
    }
	
	public function refund()
	{
		if($this->request->is('post'))
		{
			$data = $this->request->data;
			
			$payment = $this->TestBooking->find('first',array('conditions'=>array('test_booking_id'=>$data['payment_id'],'transaction_id'=>$data['transactionID']),'recursive'=>-1));
			
			if(isset($payment) && !empty($payment))
			{
				echo "1"; exit;
			}
			else
			{
				echo "0"; exit;
			}
		}
    }
	
}

?>