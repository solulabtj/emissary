<?php
class AdminsController extends AppController {

    public $paginate = array(
        'limit' => 25,
        'conditions' => array('status' => '1'),
        'order' => array('Admin.username' => 'asc')
    );
		
		
    public function beforeFilter() {
        $this->layout = 'login';
        parent::beforeFilter();
        $this->Auth->allow('login', 'add');
    }


    function create_subadmin(){
		//pr($_SESSION);exit;
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post')){
			
            $post = $this->request->data;
			
            $post['Admin']['ip_address'] = $this->request->clientIp();
			$post['Admin']['username'] = $post['username'];
			$post['Admin']['email_address'] = $post['email'];
			$post['Admin']['mobile_number'] = $post['mobile_no'];
            $post['Admin']['password'] = AuthComponent::password($post['c_password']);
            $post['Admin']['created_by_admin'] = $this->Auth->user('admin_id');
            //$post['Admin']['created_by_name'] = $this->Auth->user('username');
			$post['Admin']['is_active'] = '1';
            $post['Admin']['created'] = date('Y-m-d H:i:s');
            
            if($this->Admin->save($post)){
                
                $this->Session->setFlash('Your Sub Admin user add successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'admins','action'=>'profile'));
                        
            }else{
                
                $this->Session->setFlash('Error ! In add your sub admin.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'admins','action'=>'profile'));
            }
            
        }
        
        $this->layout = 'admin';
        
        
    }
    
    function activate_inactive($admin_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->Admin->id = $admin_id; //exit;
        
        if($this->Admin->saveField('is_active',$status)){
            $this->Session->setFlash('Sub Admin status update successful.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'admins','action' => 'profile'));
            exit();
        } else {
            $this->Session->setFlash('Error ! In change sub admin status.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'admins','action' => 'profile'));
            exit();
        }
        
        
    }
    
    function delete_subadmin($admin_id=null){
		
		//echo $_SESSION['Auth']['User']['type'];	exit;
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        $this->Admin->id = $admin_id; //exit;
        
        if($this->Admin->delete()){
			//echo "firsrt"; exit;
            $this->Session->setFlash('Your Sub Admin delete successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'admins','action' => 'profile'));
            exit();
        } else {
			//echo "second"; exit;
            $this->Session->setFlash('Error ! In delete sub admin.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'admins','action' => 'profile'));
            exit();
        }
    }
    
    function subadmin_list(){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        $this->layout = 'admin';
        $subadmin_data = $this->Admin->find('all',array('conditions'=>array('admin_type'=>0,'created_by_admin'=>$_SESSION['Auth']['User']['admin_id'])));
        $this->set('subadmin_list',$subadmin_data);
		
		//$this->render('profile',$data);
        
    }
    
    public function login() {

        if ($this->Session->check('Auth.User')) {
            $this->redirect(array('controller'=>'Home','action' => 'index'));
        }
		//echo AuthComponent::password($this->request->data['Admin']['password']);exit;
        if ($this->request->is('post')) {
			//echo AuthComponent::password($this->request->data['Admin']['password']);exit;
			//$this->Auth->password($this->request->data['password']);
			//pr($this->request->data);
			if($admin = $this->Admin->find('first',array('conditions'=>array('username'=>$this->request->data['Admin']['username']),'recursive'=>-1)))
			{
				/*pr($admin);
				$this->Session->write($admin);
				pr($_SESSION);*/
				//echo AuthComponent::password($this->request->data['Admin']['password']);exit;
				
				//echo "<pre>"; print_r($admin); exit;
				
				if($admin['Admin']['is_active'] == 1)
				{
					//debug($this->Auth->login()); die();
					//echo "first";exit;
					if ($this->Auth->login()) {
						//echo "second";exit;
						$this->redirect(array('controller' => 'Home','action' => 'index'));
					} else {
						//echo "third";exit;
						$this->Session->setFlash(__('Invalid username or password !'));
					}
				}
				else
				{
					//echo "fourth";exit;
					$this->Session->setFlash(__('You have been blocked by Administrator !'));	
				}
			}
			else
			{
				//echo "fifth";exit;
				$this->Session->setFlash(__('Invalid Username !'));	
			}
        }
    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    public function index() {
        $this->paginate = array(
            'limit' => 6,
            'order' => array('Admin.username' => 'asc')
        );
        $admins = $this->paginate('Admin');
        $this->set(compact('admins'));
    }

    public function add() {
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        if ($this->request->is('post')) {

            $this->Admin->create();
            if ($this->Admin->save($this->request->data)) {
                $this->Session->setFlash(__('The admin has been created'));
                $this->redirect(array('controller' => 'admins', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The admin could not be created. Please, try again.'));
            }
        }
    }

    public function edit($id = null) {

        if (!$id) {
            $this->Session->setFlash('Please provide a admin id');
            $this->redirect(array('action' => 'index'));
        }

        $admin = $this->Admin->findById($id);
        if (!$admin) {
            $this->Session->setFlash('Invalid Admin ID Provided');
            $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Admin->id = $id;
            if ($this->Admin->save($this->request->data)) {
                $this->Session->setFlash(__('The admin has been updated'));
                $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->Session->setFlash(__('Unable to update your admin.'));
            }
        }

        if (!$this->request->data) {
            $this->request->data = $admin;
        }
    }

    public function delete($id = null) {

        if (!$id) {
            $this->Session->setFlash('Please provide a admin id');
            $this->redirect(array('action' => 'index'));
        }

        $this->Admin->id = $id;
        if (!$this->Admin->exists()) {
            $this->Session->setFlash('Invalid admin id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Admin->saveField('status', 0)) {
            $this->Session->setFlash(__('Admin deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Admin was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

   

    public function activate($id = null) {
        if (!$id) {
            $this->Session->setFlash('Please provide a admin id');
            $this->redirect(array('action' => 'index'));
        }

        $this->Admin->id = $id;
        if (!$this->Admin->exists()) {
            $this->Session->setFlash('Invalid admin id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Admin->saveField('status', 1)) {
            $this->Session->setFlash(__('Admin re-activated'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Admin was not re-activated'));
        $this->redirect(array('action' => 'index'));
    }
    
    
    public function profile(){
        
		$this->layout = 'admin';
       
        if($this->request->is('post')){
            
            $username = $this->request->data('username');
            $email_address = $this->request->data('email');
            $mobile_number = $this->request->data('mobile_no');			
            $admin_id = $this->request->data('admin_id');
           
            $query = $this->Admin->query("UPDATE admins SET email_address='$email_address',mobile_number='$mobile_number',username='$username' WHERE admin_id = '$admin_id'");
            
            
            
            if ($query) {
                
                $this->Session->setFlash('Admin details update successful.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'admins','action' => 'profile'));
                exit();
                
            } else {
                $this->Session->setFlash('Unable to update your admin details.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'admins','action' => 'profile'));
                exit();
            }
        }
            
        $subadmin_data = $this->Admin->find('all',array('conditions'=>array('admin_type'=>0,'created_by_admin'=>$_SESSION['Auth']['User']['admin_id'])));
        $this->set('subadmin_list',$subadmin_data);
		
        $admin_id = $this->Auth->user('admin_id');
        $admin_id = $this->Auth->user('admin_id');
        $admindata = $this->Admin->find('first',array('conditions'=>array('admin_id'=>$admin_id)));
        $this->set('admin_settings',$admindata);
    }
    
    function changepassword(){
    
        if($this->request->is('post')){
            
            $oldpasstext = $this->request->data('password_old');
            $new_passwordtext = $this->request->data('password_new');
            
            $oldpass = AuthComponent::password($oldpasstext);
            $newpass = AuthComponent::password($new_passwordtext);
            
            $admin_id = $this->Auth->user('admin_id');
            
            $option = array(
                'conditions' => array('Admin.admin_id' => $admin_id,'password'=>$oldpass), //array of conditions
                'limit' => 1,
                'fields' => 'admin_id'
            );
            
            $checkpass_db =  $this->Admin->find('first',$option);
            
			//pr($checkpass_db); exit;
			
            if($checkpass_db['Admin']['admin_id']){
                $query = $this->Admin->query("UPDATE admins SET password='$newpass' WHERE admin_id = '$admin_id'");
            if ($query>0){
                    $this->Session->setFlash('Your password change successful.Next time login with new password.','default',
                    array('class' => 'successmsg'));
                    $this->redirect(array('controller'=>'admins','action' => 'profile'));
                    exit();
                } else {
                    $this->Session->setFlash('Error ! In change password.Please Try Again.','default',
                    array('class' => 'errormsg'));
                    $this->redirect(array('controller'=>'admins','action' => 'profile'));
                    exit();
                }
            }else{
                $this->Session->setFlash('Your old password is wrong.','default',
                array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'admins','action' => 'profile'));
                exit();
            }
            
        }
        
    }

}

?>