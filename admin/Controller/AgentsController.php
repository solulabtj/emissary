<?php
class AgentsController extends AppController {
	
   public $uses = array('AgentUserInfo','AgentJob');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function agent_view() {
                $this->layout = 'admin';
                $allagents = $this->AgentUserInfo->find('all', array('fields' => array('AgentUserInfo.*'),'order' => array('AgentUserInfo.agent_id' => 'ASC'), 'recursive' => -1));
                
                $this->set('listing',$allagents);
	}
	
	function activate_inactive($agent_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->AgentUserInfo->id = $agent_id; //exit;
        
        if($this->AgentUserInfo->saveField('is_blocked',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'agents','action' => 'agent_view'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'agents','action' => 'agent_view'));
            exit();
        }
        
        
    }
    
    public function agent_details_view() {
                $id = $this->params['pass']['0'];
                $this->layout = 'admin';
                $allagents = $this->AgentUserInfo->find('first', array('fields' => array('AgentUserInfo.*'),'conditions' => array('AgentUserInfo.agent_id' => $id),'order' => array('AgentUserInfo.agent_id' => 'ASC'), 'recursive' => -1));
                $agentjobs = $this->AgentJob->find('all', array('conditions' => array('AgentJob.agent_id' => $id)));
                /*echo "<pre>";
                pr($agentjobs);
                exit;*/
                $this->set('joblisting',$agentjobs);
                $this->set('listing',$allagents);
	}
	
}

?>