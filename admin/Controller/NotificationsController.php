<?php

error_reporting(E_ALL);
ini_set("display_errors", "ON");

App::uses('CakeEmail', 'Network/Email');

class NotificationsController extends AppController {

    public $uses = array('Notification', 'AgentRank', 'AgentNotification', 'ClientNotification', 'AgentUserInfo', 'ClientUserInfo', 'AgentDevice');

    public function beforeFilter() {

        parent::beforeFilter();
    }

    public function notification_view() {
        $this->layout = 'admin';

        $setting = $this->Notification->find('all', array('fields' => array('Notification.*'), 'recursive' => -1));
        $ranks = $this->AgentRank->find('all');
        $this->set('ranks', $ranks);
        $this->set('listing', $setting);
    }

    public function notification() {
        $this->layout = 'admin';

        if ($this->request->is('post')) {

            $post = $this->request->data;
            //pr($post); 
            date_default_timezone_set('Asia/Kolkata');

            $current_date = date("Y-m-d h:i:s", time());
            //exit;
            $data['notification_text'] = $post['notification_text'];
            $data['agent_rank'] = $post['agent_rank'];
            $data['minimum_points'] = $post['minimum_points'];
            $data['created_by_admin'] = $_SESSION['Auth']['User']['admin_id'];

            if (isset($post['notification_to1'])) {
                $data['to_agent'] = '1';
            } else {
                $data['to_agent'] = '0';
            }

            if (isset($post['notification_to2'])) {
                $data['to_client'] = '1';
            } else {
                $data['to_client'] = '0';
            }

            if (isset($post['email_notify'])) {
                $data['is_email'] = '1';
            } else {
                $data['is_email'] = '0';
            }

            if (isset($post['push_notify'])) {
                $data['is_push'] = '1';
            } else {
                $data['is_push'] = '0';
            }

            if (isset($post['send_option']) && $post['send_option'] == '0') {
                $data['notification_sent_time'] = $post['date_send'];
            } else {
                $data['notification_sent_time'] = $current_date;
            }

            if ($this->Notification->save($data)) {

                $this->Session->setFlash('Setting added successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'notifications', 'action' => 'notification_view'));
            } else {

                $this->Session->setFlash('Unable to add setting.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'notifications', 'action' => 'notification_view'));
            }
        }
    }

    public function send_notification() {
        $this->layout = 'admin';
        $notification_sent = false;
        if ($this->request->is('post')) {
            $post = $this->request->data;

            $subject = "EMISSARY NOTIFICATION";
            $message = $post['notification_text'];
            //$template = "admin_notification";
            $template = "";
            if ($post['notification_type'] == 1) {
                $agent_data['agent_rank'] = $post['agent_rank'];
                $agent_data['last_active'] = date("Y-m-d", strtotime($post['last_active']));
                $agent_data['minimum_points'] = $post['minimum_points'];
                $agent_data['test'] = $post['test_status'];
                $agent_data['is_email'] = isset($post['email_notify']) ? $post['email_notify'] : 0;
                $agent_data['is_push'] = isset($post['push_notify']) ? $post['push_notify'] : 0;
                $agent_data['notification_text'] = $post['notification_text'];
                $agent_data['notification_sent_time'] = NOW;
                $agent_data['is_sent'] = 1;

                $acondtion = array();

                if (isset($agent_data['agent_rank']) && $agent_data['agent_rank'] != "") {
                    $acondtion['AgentUserInfo.agent_rank'] = $agent_data['agent_rank'];
                }

                if (isset($agent_data['last_active']) && $agent_data['last_active'] != "1970-01-01" && $agent_data['last_active'] != "") {
                    $acondtion['AgentUserInfo.last_login <='] = $agent_data['last_active'];
                }

                if (isset($agent_data['minimum_points']) && $agent_data['minimum_points'] != "") {
                    $acondtion['AgentUserInfo.points_earned >='] = $agent_data['minimum_points'];
                }

                if (isset($agent_data['test']) && $agent_data['test'] != "") {
                    $acondtion['AgentUserInfo.test_passed'] = $agent_data['test'];
                }

                $acondtion['AgentUserInfo.is_active'] = 1;

                if ($this->AgentNotification->save($agent_data)) {

                    $agents = $this->AgentUserInfo->find('all', array('fields' => array('DISTINCT AgentUserInfo.email_address', 'AgentUserInfo.agent_id'), 'conditions' => $acondtion, 'recursive' => -1));

                    /*                     * ********** Mail Code ************ */

                    if ($agent_data['is_sent'] == 1 && $agent_data['is_email'] == 1) {

                        foreach ($agents as $agent) {
                            $agent_email[] = $agent['AgentUserInfo']['email_address'];
                        }

                        // $mail = $this->send_email(ADMIN_EMAIL, $agent_email, $subject, $message, $template);
                        if ($mail['message']) {
                           $data2 = true;
                        }
                    }

                    /*                     * ********** Push Code ************ */

                    $agent_devices = $this->AgentDevice->find('all', array('fields' => array('AgentDevice.*'), 'conditions' => $acondtion, 'recursive' => 1));

                    if ($agent_data['is_sent'] == 1 && $agent_data['is_push'] == 1) {

                        foreach ($agent_devices as $device) {
                            if ($device['AgentDevice']['device_type'] == 'android') {
                                $agent_android[] = $device['AgentDevice']['registration_id'];
                            } else {
                                $agent_ios[] = $device['AgentDevice']['registration_id'];
                            }
                        }

                        if (!empty($agent_android)) {

                            $android_devices = array_chunk($agent_android, 999);

                            foreach ($android_devices as $android_chunk) {
                                $this->send_push_android($android_chunk, $subject, $message);
                                $data2 = true;
                            }
                        }

                        if (!empty($agent_ios)) {
                            $this->send_push_ios($agent_ios, $subject, $message);
                            $data2 = true;
                        }
                    }
                }
            } else if ($post['notification_type'] == '2') {

                $client_data['reason'] = isset($post['notify_reason']) ? $post['notify_reason'] : "";
                $client_data['jobs_ending_in'] = isset($post['jobs_ending_in']) ? $post['jobs_ending_in'] : "";
                $client_data['notification_text'] = isset($post['notification_text_c']) ? $post['notification_text_c'] : "";
                $client_data['notification_sent_time'] = NOW;
                $client_data['is_sent'] = 1;


                if ($this->ClientNotification->save($client_data)) {

                    $wcondition = "";
                    $condition = "";
                    $group = "";
                    $field = "";

                    if (isset($post['notify_reason']) && $post['notify_reason'] == "1") {
                        $date = date("Y-m-d");
                        $condition = "WHERE client_user_infos.subscription_expiration < CURDATE() && client_user_infos.subscription_expiration != 0000-00-00";
                    }

                    if (isset($post['notify_reason']) && $post['notify_reason'] == "2") {
                        $condition = "WHERE agent_jobs.accepted_by_client = 1 AND agent_jobs.declined_by_client	 = 0 AND agent_jobs.completed_by_agent = 0 AND agent_jobs.cancelled_by_agent = 0 AND agent_jobs.disapproved_by_client = 0 AND agent_jobs.approved_by_client = 0 ";
                    }

                    if (isset($post['notify_reason']) && $post['notify_reason'] == "3") {
                        $field = ",COUNT(agent_jobs.client_id) as count";
                        $condition = "WHERE agent_jobs.accepted_by_client = 0 AND agent_jobs.declined_by_client	 = 0 AND agent_jobs.completed_by_agent = 0 AND agent_jobs.cancelled_by_agent = 0 AND agent_jobs.disapproved_by_client = 0 AND agent_jobs.approved_by_client = 0 ";
                        $group = "GROUP BY agent_jobs.client_id";
                    }


                    if ($post['jobs_ending_in'] == "1") {
                        $date = date("Y-m-d");
                        $week1 = date('Y-m-d', strtotime("+7 days"));
                        $wcondition = "WHERE jobs.job_end_date BETWEEN '$date' AND '$week1'";
                    }

                    if ($post['jobs_ending_in'] == "2") {
                        $week2 = date('Y-m-d', strtotime("+14 days"));
                        $wcondition = "WHERE jobs.job_end_date BETWEEN CURDATE() AND $week2";
                    }

                    if ($post['jobs_ending_in'] == "3") {
                        $week3 = date('Y-m-d', strtotime("+21 days"));
                        $wcondition = "WHERE jobs.job_end_date BETWEEN CURDATE() AND $week3";
                    }


                    $clients = $this->ClientUserInfo->query("select DISTINCT client_user_infos.email_address $field from client_user_infos LEFT JOIN agent_jobs ON client_user_infos.client_id = agent_jobs.client_id LEFT JOIN jobs ON client_user_infos.client_id = jobs.client_id $condition $wcondition $group");

                    if (isset($post['notify_reason']) && $post['notify_reason'] == "3") {
                        foreach ($clients as $client) {
                            if ($client[0]['count'] > "5") {
                                $client_email[] = $client['client_user_infos']['email_address'];
                            }
                        }
                    } else {
                        foreach ($clients as $client) {
                            $client_email[] = $client['client_user_infos']['email_address'];
                        }
                    }
                  
                    $this->send_email(ADMIN_EMAIL, $client_email, $subject, $message, $template);
                    $data2 = true;
                    $notification_sent = true;
                }
            }


            if (isset($data2) && !empty($data2) || isset($data4) && !empty($data4)) {
                $this->Session->setFlash('Notification sent successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'notifications', 'action' => 'notification_view'));
                exit();
            } else {
                $this->Session->setFlash('Unable to send notification.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'notifications', 'action' => 'notification_view'));
                exit();
            }
        }
    }

    public function send_email($from, $to, $subject, $message, $template) {

        $this->Email = new CakeEmail();
        $this->Email->config('smtp');
        $this->Email->to($to);
        $this->Email->subject($subject);
        $this->Email->emailFormat('html');

        $this->Email->from($from);
        $this->Email->template($template);

        ob_start();
        $mail = $this->Email->send($message);
        return $mail;
        ob_end_clean();
    }

}

?>