<?php
class JobsController extends AppController {
	
   public $uses = array('JobCategories');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function job_category() {
                $this->layout = 'admin';
                $test_q = $this->JobCategories->find('all',array('fields'=>array(),'conditions'=>array('JobCategories.is_active'=>1),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function delete_jobcategory($job_category_id=null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
       
        $status = 0;
        
        $this->JobCategories->id = $job_category_id; //exit;
        
        if($this->JobCategories->saveField('is_active',$status)){
			
            $this->Session->setFlash('Job Category deleted successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'jobs','action' => 'job_category'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! Unable to delete Job Category.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'jobs','action' => 'job_category'));
            exit();
        }
        
    }
	
	function activate_inactive($job_category_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->JobCategories->id = $job_category_id; //exit;
        
        if($this->JobCategories->saveField('is_blocked',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'jobs','action' => 'job_category'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'jobs','action' => 'job_category'));
            exit();
        }
        
    }
	
	function category_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
            $post = $this->request->data;
			//pr($post);
			
			$data['ip_address'] = $this->request->clientIp();
			$data['job_category_title'] = $post['job_category_title'];
			$data['is_active'] = '1';
			//pr($data);exit;
			if($this->JobCategories->save($data)){
                
                $this->Session->setFlash('Job category added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'jobs','action'=>'job_category'));
                        
            }else{
                
                $this->Session->setFlash('Error ! Job category not added.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'jobs','action'=>'job_category'));
            }
        }
        
        $this->layout = 'admin';
        		
	}
	
	function edit_jobcategory($job_category_id=null){
		
		$this->layout = 'admin';
		
		$ID = $job_category_id;
       
        if($this->request->is('post')){
            
            $job_category_id = $this->request->data('job_category_id');
            $job_category_title = $this->request->data('job_category_title');
            
            $query = $this->JobCategories->query("UPDATE job_categories SET job_category_title='$job_category_title' WHERE job_category_id = '$job_category_id'");
            
			//pr($query); if($query || empty($query)) { echo "success"; } else { echo "failed"; } exit;
			
            if (isset($query)) {
                
                $this->Session->setFlash('Job category updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'jobs','action' => 'job_category'));
                exit();
				
            } else {
                $this->Session->setFlash('Unable to update Job category.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'jobs','action' => 'job_category'));
                exit();
            }
        }
        
		$data = $this->JobCategories->find('all',array('conditions'=>array('job_category_id'=>$ID)));
		
        $this->set('singlerecord',$data);
		
    }
	
}

?>