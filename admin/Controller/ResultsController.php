<?php
class ResultsController extends AppController {
	
   public $uses = array('TestResult','AgentTestAnswer','TestQuestionOption');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function test_result() {
                $this->layout = 'admin';
                $test_results = $this->TestResult->find('all',array('fields'=>array('TestResult.*','AgentUserInfo.agent_name'), 'conditions'=> array('AgentUserInfo.agent_id >' => 0),'recursive'=>1,'order' => array('TestResult.created' => 'DESC')));
                $this->set('listing',$test_results);
	}
	
	public function getAllresults() 
	{
       
	   $is_passed = $_POST['is_passed'];
	   
	   if($is_passed=='1' || $is_passed=='0')
	   {
		   $test_results = $this->TestResult->find('all',array('fields'=>array('TestResult.*','AgentUserInfo.agent_name'), 'conditions'=> array('AgentUserInfo.agent_id >' => 0,'TestResult.is_passed' =>$is_passed),'recursive'=>1,'order' => array('TestResult.created' => 'DESC')));
		   
		   //pr($test_results); exit;
		   
		   ?>
		   		
                <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                <script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/jquery-1.11.2.min.js"></script> 

				<script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/plugins/data-tables/js/jquery.dataTables.min.js"></script>

				<script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/plugins/data-tables/data-tables-script.js"></script> 

				<script type="text/javascript">         
                        $(document).ready(function(){
                                            $('#data-table-simple').DataTable();
                                        });
                                        
                </script> 
                    <thead>
                    	<tr>
                    		<th>Agent Name</th>
                            <th>Submission Date</th>
                            <th>Result</th>
                            
                            <th>Percentage</th>
                            <th>Action</th>
                                                  
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
		   
		   <?php 
		   
		   foreach($test_results as $row) {  ?>  
                        <tr>
                        	<td><?php echo $row['AgentUserInfo']['agent_name']; ?></td>
                            <td><?php //echo date("jS F, Y h:i A",strtotime($row['TestResult']['created']));
							   echo date("m-d-Y",strtotime($row['TestResult']['created']));
							 ?></td>
                            <td><?php if($row['TestResult']['is_passed']==1) { echo "Pass"; } else { echo "fail"; } ?></td>
                           
                            <td><?php echo $row['TestResult']['percentage_earned'].'%'; ?></td>
                            <td><a href="<?php echo ADMIN_ROOT.'results/show_answer/'.$row['TestResult']['agent_id'].'/'.$row['TestResult']['test_result_id'];?>">Show Answers</a></td>
                            
                            
                        </tr>
			<?php } ?>
            
            	</tbody>
              </table>
            
            <?php 
			
			exit;
		   
	   }
	  
	   else
	   {
		   $test_results = $this->TestResult->find('all',array('fields'=>array('TestResult.*','AgentUserInfo.agent_name'), 'conditions'=> array('AgentUserInfo.agent_id >' => 0),'recursive'=>1,'order' => array('TestResult.created' => 'DESC')));
		   
		   ?>
		   	
            	<table id="data-table-simple" class="responsive-table display" cellspacing="0">
             	<script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/jquery-1.11.2.min.js"></script> 

				<script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/plugins/data-tables/js/jquery.dataTables.min.js"></script>

				<script type="text/javascript" src="<?php echo ADMIN_ROOT ?>js/plugins/data-tables/data-tables-script.js"></script> 

				<script type="text/javascript">         
                        $(document).ready(function(){
                                            $('#data-table-simple').DataTable();
                                        });
                                        
                </script>
                    <thead>
                    	<tr>
                    		<th>Agent Name</th>
                            <th>Submission Date</th>
                            <th>Result</th>
                            
                            <th>Percentage</th>
                            <th>Action</th>
                                                  
                        </tr>
                    </thead>
                 
                    <tfoot>
                     </tfoot>
                 
                    <tbody>
		   
		   <?php 
		   
		   foreach($test_results as $row) {  ?>  
                        <tr>
                        	<td><?php echo $row['AgentUserInfo']['agent_name']; ?></td>
                            <td><?php //echo date("jS F, Y h:i A",strtotime($row['TestResult']['created']));
							   echo date("m-d-Y",strtotime($row['TestResult']['created']));
							 ?></td>
                            <td><?php if($row['TestResult']['is_passed']==1) { echo "Pass"; } else { echo "fail"; } ?></td>
                           
                            <td><?php echo $row['TestResult']['percentage_earned'].'%'; ?></td>
                            <td><a href="<?php echo ADMIN_ROOT.'results/show_answer/'.$row['TestResult']['agent_id'].'/'.$row['TestResult']['test_result_id'];?>">Show Answers</a></td>
                            
                            
                        </tr>
			<?php } ?>
            	
                </tbody>
             </table>
                
            <?php
			
			exit;
	   }
	   
	   exit;
	}
	
	public function show_answer($agent_id=null,$test_result_id=null) {
		
				
                $this->layout = 'admin';
				
				$single = $this->TestResult->find('first',array('fields'=>array('TestResult.*','AgentUserInfo.agent_name'), 'conditions'=> array('AgentUserInfo.agent_id'=>$agent_id,'TestResult.test_result_id'=>$test_result_id),'recursive'=>1,'order' => array('TestResult.created' => 'DESC')));
				
                $test_results = $this->AgentTestAnswer->find('all',array('fields'=>array('AgentTestAnswer.*','TestQuestion.*'),'group' => 'AgentTestAnswer.test_question_id','conditions'=> array('AgentTestAnswer.agent_id'=>$agent_id),'recursive'=>1));
				
				$i=0;
				foreach($test_results as $result)
				{
					$test_question_id = $result['AgentTestAnswer']['test_question_id'];
					$agent_test_answer_id = $result['AgentTestAnswer']['agent_test_answer_id'];
					
					$option_data[$agent_test_answer_id] = $this->TestQuestionOption->find('all',array('fields'=>array('TestQuestionOption.*'),'conditions'=> array('TestQuestionOption.test_question_id'=>$test_question_id),'recursive'=>1));
					
					$option_select[$test_question_id] = $this->AgentTestAnswer->find('all',array('fields'=>array('AgentTestAnswer.*'),'conditions'=> array('AgentTestAnswer.test_question_id'=>$test_question_id,'AgentTestAnswer.agent_id'=>$agent_id),'recursive'=>1));
					
					$i++;
				}
				
				$data = $this->AgentTestAnswer->find('all',array('fields'=>array('AgentTestAnswer.*'),'group' => 'AgentTestAnswer.test_question_id','conditions'=> array('AgentTestAnswer.agent_id'=>$agent_id),'recursive'=>1));
				
				$this->set('single',$single);
				$this->set('data',$data);
				$this->set('option_select',$option_select);
                $this->set('listing',$test_results);
				$this->set('option_data',$option_data);
				
	}
	
	
	
}

?>