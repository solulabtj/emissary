<?php
class RankingsController extends AppController {
	
   public $uses = array('Ranking');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function ranking_view() {
                $this->layout = 'admin';
                $test_q = $this->Ranking->find('all',array('fields'=>array(),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function activate_inactive($ranking_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->Ranking->id = $ranking_id; //exit;
        
        if($this->Ranking->saveField('is_active',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'rankings','action' => 'ranking_view'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'rankings','action' => 'ranking_view'));
            exit();
        }
        
    }
	
	function ranking_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
            $post = $this->request->data;
			//pr($post); exit;
			
			$data['ip_address'] = $this->request->clientIp();
			$data['ranking_title'] = $post['ranking_title'];
			$data['ranking_level'] = $post['ranking_level'];
			$data['ranking_lower_limit'] = $post['ranking_lower_limit'];
			$data['ranking_upper_limit'] = $post['ranking_upper_limit'];
			$data['is_active'] = '1';
			//pr($data);exit;
			if($this->Ranking->save($data)){
                
                $this->Session->setFlash('Ranking added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'rankings','action'=>'ranking_view'));
                        
            }else{
                
                $this->Session->setFlash('Error ! Ranking not added.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'rankings','action'=>'ranking_view'));
            }
        }
        
        $this->layout = 'admin';
        		
	}
	
	function edit_ranking($ranking_id=null){
		
		$this->layout = 'admin';
		
		$ID = $ranking_id;
       
        if($this->request->is('post')){
            
			//pr($this->request->data);exit;
			
            $ranking_id = $this->request->data('ranking_id');
            $ranking_title = $this->request->data('ranking_title');
			$ranking_level = $this->request->data('ranking_level');
			$ranking_lower_limit = $this->request->data('ranking_lower_limit');
			$ranking_upper_limit = $this->request->data('ranking_upper_limit');
            
            $query = $this->Ranking->query("UPDATE rankings SET ranking_title='$ranking_title',ranking_level='$ranking_level',ranking_lower_limit='$ranking_lower_limit',ranking_upper_limit='$ranking_upper_limit' WHERE ranking_id = '$ranking_id'");
            
			//pr($query); if($query || empty($query)) { echo "success"; } else { echo "failed"; } exit;
			
            if (isset($query)) {
                
                $this->Session->setFlash('Ranking updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'rankings','action' => 'ranking_view'));
                exit();
				
            } else {
                $this->Session->setFlash('Unable to update Ranking.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'rankings','action' => 'ranking_view'));
                exit();
            }
        }
        
		$data = $this->Ranking->find('all',array('conditions'=>array('ranking_id'=>$ID)));
		
        $this->set('singlerecord',$data);
		
    }
	
}

?>