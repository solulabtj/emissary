<?php
class HomeController extends AppController {

    public $uses = array('Home');
	
	public function login() {
		
        $this->layout = 'login';
    }

	
    function index(){
        
        $this->layout = 'admin';
        
    }
	function xyz(){
        
        $this->layout = 'admin';
        
    }
	function add_city(){
        
        $this->layout = 'admin';
        
    }
	function add_new_blog(){
        
        $this->layout = 'admin';
        
    }
	function add_new_contact(){
        
        $this->layout = 'admin';
        
    }
	function add_new_email(){
        
        $this->layout = 'admin';
        
    }
	function add_new_member(){
        
        $this->layout = 'admin';
        
    }
	function add_new_nation(){
        
        $this->layout = 'admin';
        
    }
	function add_new_page(){
        
        $this->layout = 'admin';
        
    }
	function add_new_promotion(){
        
        $this->layout = 'admin';
        
    }
	function add_new_slider(){
        
        $this->layout = 'admin';
        
    }
	function add_newsletter(){
        
        $this->layout = 'admin';
        
    }
	function add_promocodes(){
        
        $this->layout = 'admin';
        
    }
	function admin_settings(){
        
        $this->layout = 'admin';
        
    }
	function blog_edit(){
        
        $this->layout = 'admin';
        
    }
	function blogs(){
        
        $this->layout = 'admin';
        
    }
	function cities(){
        
        $this->layout = 'admin';
        
    }
	function city_edit(){
        
        $this->layout = 'admin';
        
    }
	function configuration_management(){
        
        $this->layout = 'admin';
        
    }
	function contact_edit(){
        
        $this->layout = 'admin';
        
    }
	function contact_listing(){
        
        $this->layout = 'admin';
        
    }
	function email_edit(){
        
        $this->layout = 'admin';
        
    }
	function email_templates(){
        
        $this->layout = 'admin';
        
    }
	function manage_newslatter_template(){
        
        $this->layout = 'admin';
        
    }
	function manage_newsletters(){
        
        $this->layout = 'admin';
        
    }
	function member_edit(){
        
        $this->layout = 'admin';
        
    }
	function members(){
        
        $this->layout = 'admin';
        
    }
	function nation_city(){
        
        $this->layout = 'admin';
        
    }
	function nation_edit(){
        
        $this->layout = 'admin';
        
    }
	function notifications(){
        
        $this->layout = 'admin';
        
    }
	function orders(){
        
        $this->layout = 'admin';
        
    }
	function page_edit(){
        
        $this->layout = 'admin';
        
    }
	function pages(){
        
        $this->layout = 'admin';
        
    }
	function promocodes(){
        
        $this->layout = 'admin';
        
    }
	function promotion_edit(){
        
        $this->layout = 'admin';
        
    }
	function reports(){
        
        $this->layout = 'admin';
        
    }
	function send_newsletter(){
        
        $this->layout = 'admin';
        
    }
	function send_notification(){
        
        $this->layout = 'admin';
        
    }
	function set_default_timezone(){
        
        $this->layout = 'admin';
        
    }
	function slider(){
        
        $this->layout = 'admin';
        
    }
	function slider_edit(){
        
        $this->layout = 'admin';
        
    }
	
	
	
	
	
    
}

?>