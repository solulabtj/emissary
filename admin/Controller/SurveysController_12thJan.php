<?php
class SurveysController extends AppController {
	
   public $uses = array('JobCategories','JobQuestion','JobQuestionOption');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function survey_category() {
                $this->layout = 'admin';
                $test_q = $this->JobCategories->find('all',array('fields'=>array(),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function activate_inactive($job_category_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->JobCategories->id = $job_category_id; //exit;
        
        if($this->JobCategories->saveField('is_active',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
            exit();
        }
        
    }
	
	function category_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
            $post = $this->request->data;
			//pr($post);
			
			$data['ip_address'] = $this->request->clientIp();
			$data['job_category_tite'] = $post['job_category_tite'];
			$data['is_active'] = '1';
			//pr($data);exit;
			if($this->JobCategories->save($data)){
                
                $this->Session->setFlash('Job category added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'survey_category'));
                        
            }else{
                
                $this->Session->setFlash('Error ! Job category not added.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'survey_category'));
            }
        }
        
        $this->layout = 'admin';
        		
	}
	
	function edit_jobcategory($job_category_id=null){
		
		$this->layout = 'admin';
		
		$ID = $job_category_id;
       
        if($this->request->is('post')){
            
            $job_category_id = $this->request->data('job_category_id');
            $job_category_tite = $this->request->data('job_category_tite');
            
            $query = $this->JobCategories->query("UPDATE job_categories SET job_category_tite='$job_category_tite' WHERE job_category_id = '$job_category_id'");
            
            if ($query) {
                
                $this->Session->setFlash('Job category updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
                exit();
				
            } else {
                $this->Session->setFlash('Unable to update Job category.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
                exit();
            }
        }
        
		$data = $this->JobCategories->find('all',array('conditions'=>array('job_category_id'=>$ID)));
		
        $this->set('singlerecord',$data);
		
    }
	
	public function question_view() {
                $this->layout = 'admin';
                $test_q = $this->JobQuestion->find('all',array('fields'=>array(),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function activate_inactive_view($job_question_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->JobQuestion->id = $job_question_id; //exit;
        
        if($this->JobQuestion->saveField('is_active',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
            exit();
        }
        
        
    }
	
	function question_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
            $post = $this->request->data;
			//pr($post);
			
			$data['ip_address'] = $this->request->clientIp();
			$data['job_question_description'] = $post['job_question_description'];
			$data['is_active'] = '1';
			//pr($data);exit;
			if($this->JobQuestion->save($data)){
                
				$job_question_id = $this->JobQuestion->id;
				
				$data2 = array();
				
				for($i=1;$i<=4;$i++)
				{
					$data1['job_question_id'] = $job_question_id;
					$data1['answer_text'] = $post['option_'.$i];
					
					if(isset($post['option'.$i]))
					{
						$data1['is_correct'] = $post['option'.$i];
					}
					else
					{
						$data1['is_correct'] = '0';
					}
					
					//$this->JobQuestionOption->save($data1);
					
					array_push($data2,$data1);
				}
				
				$this->JobQuestionOption->saveAll($data2);
				
                $this->Session->setFlash('Question added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
                        
            }else{
                
                $this->Session->setFlash('Error ! Question not added.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
            }
        }
        
        $this->layout = 'admin';
        		
	}
	
	
	function edit_question($job_question_id=null){
		
		$this->layout = 'admin';
		
		$ID = $job_question_id;
       
        if($this->request->is('post')){
            
            $job_question_id = $this->request->data('job_question_id');
            $job_question_description = $this->request->data('job_question_description');
            $post = $this->request->data;
			
            $query = $this->JobQuestion->query("UPDATE job_questions SET job_question_description='$job_question_description' WHERE job_question_id = '$job_question_id'");
            
            if ($query) {
                
				$query1 = $this->JobQuestionOption->query("DELETE FROM job_question_options WHERE job_question_id = '$job_question_id'");
				
				$data2 = array();
				
				for($i=1;$i<=4;$i++)
				{
					$data1['job_question_id'] = $job_question_id;
					$data1['answer_text'] = $post['option_'.$i];
					
					if(isset($post['option'.$i]))
					{
						$data1['is_correct'] = $post['option'.$i];
					}
					else
					{
						$data1['is_correct'] = '0';
					}
					
					//$this->JobQuestionOption->save($data1);
					
					array_push($data2,$data1);
				}
				
				if($this->JobQuestionOption->saveAll($data2))
				{
				
                $this->Session->setFlash('Question updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
                exit();
				
				}
				
				else
				{
					$this->Session->setFlash('Unable to update Question.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
                exit();
				}
				
                
            } else {
                $this->Session->setFlash('Unable to update Question.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
                exit();
            }
        }
        
		$data = $this->JobQuestion->find('all',array('conditions'=>array('job_question_id'=>$ID)));
		
        $this->set('singlerecord',$data);
		
    }
	
}

?>