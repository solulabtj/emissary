<?php
class PaymentsController extends AppController {
	
   public $uses = array('Payment');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function payment_view() {
                $this->layout = 'admin';
                $test_q = $this->Payment->find('all',array('recursive'=>2));
                
                $this->set('listing',$test_q);
	}
	
}

?>