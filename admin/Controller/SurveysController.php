<?php
class SurveysController extends AppController {
	
   public $uses = array('SurveyCategories','SurveyQuestion','SurveyQuestionOption','Survey');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function survey_category() {
                $this->layout = 'admin';
                $test_q = $this->SurveyCategories->find('all',array('fields'=>array(),'conditions'=>array('SurveyCategories.is_blocked'=>0),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function activate_inactive($survey_category_id=null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
		$status = '1';
		
        $this->SurveyCategories->id = $survey_category_id; //exit;
        
        if($this->SurveyCategories->saveField('is_blocked',$status)){
			
            $this->Session->setFlash('Survey Category deleted successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! Unable to delete Survey Category.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
            exit();
        }
        
    }
	
	function category_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
            $post = $this->request->data;
			
			$extension_allow = array('png','jpg','jpeg','PNG','JPG','JPEG');
			$target_dir = WEBSITE_WEBROOT.'media/img/';
			if (isset($_FILES["survey_category_image"]["name"]) && !empty($_FILES["survey_category_image"]["name"])) 
			{
				 $pathi = "";				 
			 
				 $microtime = microtime();
					
				 $fileName = $_FILES["survey_category_image"]["name"];
				 $extension = explode(".", $fileName);
				 $Path = "";
			
				 if (in_array($extension[1], $extension_allow))
				  {
					   $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					  
					   $target_dir2 = $target_dir . $Pathi;
					   move_uploaded_file($_FILES["survey_category_image"]["tmp_name"], $target_dir2);
				  }
				  
				$data['survey_category_image'] = $Pathi;
			
		  }
		  
			$data['ip_address'] = $this->request->clientIp();
			$data['survey_category_title'] = $post['survey_category_title'];
			$data['is_active'] = '1';
			//pr($data);exit;
			if($this->SurveyCategories->save($data)){
                
                $this->Session->setFlash('Survey category added successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'survey_category'));
                        
            }else{
                
                $this->Session->setFlash('Error ! Survey category not added.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'survey_category'));
            }
        }
        
        $this->layout = 'admin';
        		
	}
	
	function edit_jobcategory($survey_category_id=null){
		
		$this->layout = 'admin';
		
		$ID = $survey_category_id;
       
        if($this->request->is('post')){
            
            $survey_category_id = $this->request->data('survey_category_id');
            $survey_category_title = $this->request->data('survey_category_title');
            //pr($this->request->data); pr($_FILES); exit;
			$extension_allow = array('png','jpg','jpeg','PNG','JPG','JPEG');
			$target_dir = WEBSITE_WEBROOT.'media/img/';
			if (isset($_FILES["survey_category_image"]["name"]) && !empty($_FILES["survey_category_image"]["name"])) 
			{
				 $old_image = $this->request->data('survey_category_image_old');
				 
				 @unlink($target_dir.$old_image);
				
				 $pathi = "";				 
			 
				 $microtime = microtime();
					
				 $fileName = $_FILES["survey_category_image"]["name"];
				 $extension = explode(".", $fileName);
				 $Path = "";
			
				 if (in_array($extension[1],$extension_allow))
				  {
					   $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					  
					   $target_dir2 = $target_dir . $Pathi;
					   move_uploaded_file($_FILES["survey_category_image"]["tmp_name"], $target_dir2);
				  }
				  
				$survey_category_image = $Pathi;
			
		  }
		  else
		  {
			  $survey_category_image = $this->request->data('survey_category_image_old');
		  }
			
            $query = $this->SurveyCategories->query("UPDATE survey_categories SET survey_category_title='$survey_category_title',survey_category_image='$survey_category_image' WHERE survey_category_id = '$survey_category_id'");
            
			//pr($query); if($query || empty($query)) { echo "success"; } else { echo "failed"; } exit;
			
            if (isset($query)) {
                
                $this->Session->setFlash('Survey category updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
                exit();
				
            } else {
                $this->Session->setFlash('Unable to update Survey category.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'survey_category'));
                exit();
            }
        }
        
		$data = $this->SurveyCategories->find('all',array('conditions'=>array('survey_category_id'=>$ID)));
		
        $this->set('singlerecord',$data);
		
    }
	
	public function question_view() {
                $this->layout = 'admin';
                $test_q = $this->SurveyQuestion->find('all',array('fields'=>array(),'conditions'=>array('SurveyQuestion.is_default'=>1),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function activate_inactive_view($survey_question_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->SurveyQuestion->id = $survey_question_id; //exit;
        
        if($this->SurveyQuestion->saveField('is_blocked',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
            exit();
        }
        
        
    }
	
	function question_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
            $post = $this->request->data;
			//pr($post);exit;			
			
			//$data['ip_address'] = $this->request->clientIp();
			
			$count = $post['question_count'];
			
			$data5 = array();
			
			$data6 = array();
			
			for($limit=1;$limit<$count;$limit++)
			{
				if($post['survey_question_type_'.$limit]=='C')
				{
					unset($data1);
					unset($data3);
					unset($survey_question_id_arr);
					unset($survey_question_id);
					
					$data1 = array();
					$data3 = array();
					$survey_question_id_arr = array();
					$survey_question_id = 0;
					
					$data1['survey_question_id'] = 0;
					//$data1['survey_category_id'] = $post['survey_category_id_'.$limit];
                                        $data1['survey_category_id'] = 0;
					$data1['survey_question_type'] = $post['survey_question_type_'.$limit];
					$data1['survey_question_description'] = $post['survey_question_description_'.$limit];
					$data1['survey_question_marks'] = $post['survey_question_marks_'.$limit];				
					$data1['is_default'] = '1';
					$data1['is_active'] = '1';
					
					if($this->SurveyQuestion->save($data1))
					{
						
						$survey_question_id_arr = $this->SurveyQuestion->find('first',array('order'=>array('SurveyQuestion.survey_question_id DESC'),'recursive' => -1));
						
						$survey_question_id = $survey_question_id_arr['SurveyQuestion']['survey_question_id'];
						
					}
					for($i=1;$i<5;$i++)
					{
						$data3[$i]['survey_question_id'] = $survey_question_id;
						$data3[$i]['answer_text'] = $post['option_'.$limit.'_'.$i];
						/*if(isset($post['option'.$limit.'_'.$i]))
						{
							$data3[$i]['is_correct'] = '1';
						}
						else
						{
							$data3[$i]['is_correct'] = '0';
						}*/
						//array_push($data4,$data3);
					}
					$data5[$limit] = $this->SurveyQuestionOption->saveAll($data3);
					
				}
				
				
				else if($post['survey_question_type_' . $limit] == 'R')
				{
					
                    unset($data11);
                    unset($data33);
                    unset($survey_question_id_arr);
                    unset($survey_question_id);

                    $data11 = array();
                    $data33 = array();
                    $survey_question_id_arr = array();
                    $survey_question_id = 0;

                    $data11['survey_question_id'] = 0;
                    
                    $data11['is_default'] = '1';
                    $data11['survey_category_id'] = $post['survey_category_id_' . $limit];
                    $data11['survey_question_type'] = $post['survey_question_type_' . $limit];
                    $data11['survey_question_description'] = $post['survey_question_description_' . $limit];
                    $data11['survey_question_marks'] = $post['survey_question_marks_' . $limit];

                    $data11['is_active'] = '1';
					
					$option_count = $post['questionr'.$limit.'_count'];

                    if ($this->SurveyQuestion->save($data11)) {

                        $survey_question_id_arr = $this->SurveyQuestion->find('first', array('order' => array('SurveyQuestion.survey_question_id DESC'), 'recursive' => -1));

                        $survey_question_id = $survey_question_id_arr['SurveyQuestion']['survey_question_id'];
                    }
                    for ($i = 1;$i<$option_count; $i++) {
                        $data33[$i]['survey_question_id'] = $survey_question_id;
                        $data33[$i]['answer_text'] = $post['optionr_' . $limit . '_'.$i];
						/*if(isset($post['optionrrr_' . $limit . '_'.$i]))
						{
							$data33[$i]['is_correct'] = '1';
						}
						else
						{
							$data33[$i]['is_correct'] = '0';
						}*/

                        //array_push($data4,$data3);
                    }
                    $data55[$limit] = $this->SurveyQuestionOption->saveAll($data33);
                
				}
				
				
				else
				{
					$data2[$limit]['survey_category_id'] = $post['survey_category_id_'.$limit];
					$data2[$limit]['survey_question_type'] = $post['survey_question_type_'.$limit];
					$data2[$limit]['survey_question_description'] = $post['survey_question_description_'.$limit];
					$data2[$limit]['survey_question_marks'] = $post['survey_question_marks_'.$limit];				
					$data2[$limit]['is_default'] = '1';
					$data2[$limit]['is_active'] = '1';					
				}
				
			}
			
			if(!empty($data2))
			{
				$data6 = $this->SurveyQuestion->saveAll($data2);				
			}
			
			if(isset($survey_question_id) && !empty($survey_question_id) || isset($data5) && !empty($data5) || isset($data6) && !empty($data6) || isset($data55) && !empty($data55))
				{
					$this->Session->setFlash('Question added successfully.','default',array('class'=>'successmsg'));
			$this->redirect(array('controller'=>'surveys','action'=>'question_view'));
				}
			else
				{
					$this->Session->setFlash('Error ! Unable to add Question.please try again.','default',array('class'=>'successmsg'));
			$this->redirect(array('controller'=>'surveys','action'=>'question_view'));
				}
						
        }
        
		$survey_category = $this->SurveyCategories->find('all',array('fields'=>array(),'conditions'=>array('is_blocked'=>0),'recursive'=>1));
                
        $this->set('survey_category',$survey_category);
		
        $this->layout = 'admin';
        		
	}
	
	
	function edit_question($survey_question_id=null){
		
		$this->layout = 'admin';
		
		$ID = $survey_question_id;
       
        if($this->request->is('post')){
            //$post = $this->request->data;
			//pr($post);exit;
			
			//echo $post['type']; exit;
            $survey_question_id = $this->request->data('survey_question_id');
            $survey_question_description = $this->request->data('survey_question_description');
			$survey_question_marks = $this->request->data('survey_question_marks');
			$survey_question_type = $this->request->data('type');
			$survey_category_id = $this->request->data('survey_category_id');
			//$is_active = '1';
            $post = $this->request->data;
			
            $query = $this->SurveyQuestion->query("UPDATE survey_questions SET survey_question_description='$survey_question_description',survey_question_marks='$survey_question_marks',survey_category_id='$survey_category_id',survey_question_type='$survey_question_type',is_default='1' WHERE survey_question_id = '$survey_question_id'");
            
            if (isset($query)) {
                
				//$count = $post['option_count'];
				
				$type = $post['type'];
				
				$data2 = array();
				
				if($type=='C')
				{
					$query1 = $this->SurveyQuestionOption->query("SELECT *FROM survey_question_options WHERE survey_question_id = '$survey_question_id'");
					
					$number_of_records = sizeof($query1);
					
					if($number_of_records>0)
					{
						$query2 = $this->SurveyQuestionOption->query("DELETE FROM survey_question_options WHERE survey_question_id = '$survey_question_id'");
					}
					
					for($i=1;$i<5;$i++)
					{
						$data1['survey_question_id'] = $survey_question_id;
						$data1['answer_text'] = $post['option_'.$i];
						
						array_push($data2,$data1);
					}
					
					if($this->SurveyQuestionOption->saveAll($data2))
					{
						$this->Session->setFlash('Question updated successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
					}
					else
					{
						$this->Session->setFlash('Error ! Question not updated.please try again.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'surveys','action'=>'question_view'));
					}
				
				}
				
				else
				{
					$this->Session->setFlash('Question updated successfully.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
                exit();
				}
				
                
            } else {
                $this->Session->setFlash('Unable to update Question.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
                exit();
            }
        }
        
		$data = $this->SurveyQuestion->find('all',array('conditions'=>array('survey_question_id'=>$ID)));
		
		$survey_question_type = $data[0]['SurveyQuestion']['survey_question_type'];
		
		if ($survey_question_type == 'C' || $survey_question_type=='R') {
            $this->Session->setFlash('You can\'t edit question with type Checkbox OR Radio.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'surveys', 'action' => 'question_view'));
            exit();
        }
		
		$count = sizeof($data[0]['SurveyQuestionOption']);
		$survey_category = $this->SurveyCategories->find('all',array('fields'=>array(),'conditions'=>array('is_blocked'=>0),'recursive'=>1));
                
        $this->set('survey_category',$survey_category);
        $this->set('singlerecord',$data);
		$this->set('count',$count);
		
    }
	
	public function survey_list() {
                $this->layout = 'admin';
                $test_q = $this->Survey->find('all',array('recursive'=>1));
                
				if(empty($test_q))
				{
					$records = '0';
				}
				
				foreach($test_q as $test)
				{
					$survey_id = $test['Survey']['survey_id'];
					
					$count = $this->SurveyQuestion->find('all',array('conditions'=>array('survey_id'=>$survey_id),'recursive'=>1));
					
					if(!empty($count))
					{
						$records[$survey_id] = count($count);
					}
					else
					{
						$records[$survey_id] = '0';
					}
						
				}
				
                $this->set('listing',$test_q);
				$this->set('count',$records);
	}
	
	function publish_unpublish($survey_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->Survey->id = $survey_id; //exit;
        
        if($this->Survey->saveField('is_blocked',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'survey_list'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'survey_list'));
            exit();
        }
        
    }
	
	function survey_question_add()
	{
		
		$count = $_POST['question_count'];
		
		$survey_category = $this->SurveyCategories->find('all',array('fields'=>array(),'conditions'=>array('is_blocked'=>0),'recursive'=>1));
		
		?>
        
        <input type="hidden" name="questionr<?php echo $count ?>_count" id="questionr<?php echo $count ?>_count" value="3" />
        
        <div id="apply_question<?php echo $count; ?>" >
        
        <h4><i class="halflings-icon user"></i><span class="break"></span>Question <?php echo $count; ?></h4>
        	<div class="input_field col s12 m12" >
                  <label class="col s12 m2">Survey Category</label>
                  
                  <div class="select-wrapper"><i class="mdi-navigation-arrow-drop-down"></i><input type="text" class="select-dropdown" readonly="true" data-activates="select-options-3ec27b65-af13-c54f-e617-1e2aae227884" value="Select Type">
                  
                  <select name="survey_category_id_<?php echo $count; ?>" id="survey_category_id_<?php echo $count; ?>" class="initialized" >
                  	<option value="">Select Type
                    
                    <?php foreach($survey_category as $category) { ?>
                    
                    	<option value="<?php echo $category['SurveyCategories']['survey_category_id']; ?>"><?php echo $category['SurveyCategories']['survey_category_title']; ?>
                    
                    <?php } ?>
                    
                  </select>
                  
                  </div>
                  
                 </div>
                  
                 <div id="error_category_<?php echo $count; ?>" class="error"></div>&nbsp;
                 
                <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Type</label>
                  
                  <div class="select-wrapper"><i class="mdi-navigation-arrow-drop-down"></i><input type="text" class="select-dropdown" readonly="true" data-activates="select-options-3ec27b65-af13-c54f-e617-1e2aae227884" value="Select Type">
                  
                  <select name="survey_question_type_<?php echo $count; ?>" id="survey_question_type_<?php echo $count; ?>" class="initialized" >
                  	<option value="">Select Type</option>                
                    <option value="C">Checkbox</option>
                    <option value="T">Textbox</option>
                    <option value="I">Image</option>
                    <option value="A">Audio</option>
                    <option value="R">Radio</option>
                    
                  </select>
                  
                 </div>
                 </div>
                 
                <div id="error_type_<?php echo $count; ?>" class="error"></div>
                
                &nbsp;
                <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Question Description</label>
                  <input type="text" class="col s12 m4 validate" name="survey_question_description_<?php echo $count; ?>" id="survey_question_description_<?php echo $count; ?>" onkeypress="return lettersOnly(event)" value="">
                  
                 </div>
                <div id="error_questions_<?php echo $count; ?>" class="error"></div>
                
                <div class="input_field col s12 m12" >
                  <label class="col s12 m2">Question Marks</label>
                  <input type="text" class="col s12 m4 validate" name="survey_question_marks_<?php echo $count; ?>" id="survey_question_marks_<?php echo $count; ?>" onkeypress="return onlyNumbers(event)" value="">
                  
                 </div>
                <div id="error_marks_<?php echo $count ?>" class="error"></div>
                 
                 
                 <div id="apply_contentRadio_<?php echo $count ?>" style="display:none;">
                    	
                        <div id="apply_contentRadioo_<?php echo $count ?>" class="radiofor_<?php echo $count ?>">
                            <div class="input_field col s12 m12" id="optionrr_<?php echo $count ?>_1" > 
                                 <label class="col s12 m2">Option 1</label>
                                 <input type="text" name="optionr_<?php echo $count ?>_1" id="optionr_<?php echo $count ?>_1" value="" class="col s12 m4 validate" >
                                 <!--<input type="checkbox" onclick="checkforradio(this.id);" name="optionrrr_<?php echo $count ?>_1" id="optionrrr_<?php echo $count ?>_1" value="1" >Correct Answer-->
                            </div>
                            <div id="error_optionr<?php echo $count ?>_1" class="error"></div>
                            
                            <div class="input_field col s12 m12" id="optionrr_<?php echo $count ?>_2" >
                                 <label class="col s12 m2">Option 2</label>
                                 <input type="text" name="optionr_<?php echo $count ?>_2" id="optionr_<?php echo $count ?>_2" value="" class="col s12 m4 validate" >
                                 <!--<input type="checkbox" onclick="checkforradio(this.id);" name="optionrrr_<?php echo $count ?>_2" id="optionrrr_<?php echo $count ?>_2" value="1" >Correct Answer-->
                            </div>
                            <div id="error_optionr<?php echo $count ?>_2" class="error"></div>
                            
                        </div>
                        
                        <a id="n_<?php echo $count ?>" onClick="add_option(this.id)" class="btn btn-lg btn-custom btn-success" >Add Option</a>
                        
                         <a id="rn_<?php echo $count ?>" onClick="remove_option(this.id)" class="btn btn-lg btn-custom btn-success"  style="display:none;" >Remove Option</a>
                         
                         <div id="error_optionrrr<?php echo $count ?>" class="error"></div>
                            
                 </div>
                 
                 
                 <div id="apply_content_<?php echo $count; ?>" style="display:none;"  >
                     <div class="input_field col s12 m12" >
                         <label class="col s12 m2">Option 1</label>
                         <input type="text" name="option_<?php echo $count; ?>_1" id="option_<?php echo $count; ?>_1" class="col s12 m4 validate" value="">
                         <!--<input type="checkbox" name="option<?php echo $count ?>_1" id="option<?php echo $count ?>_1" value="1" >Correct Answer-->
                     </div>
         	         <div id="error_option<?php echo $count; ?>_1" class="error"></div> 
                     
                     <div class="input_field col s12 m12" >
                         <label class="col s12 m2">Option 2</label>
                         <input type="text" name="option_<?php echo $count; ?>_2" id="option_<?php echo $count; ?>_2" class="col s12 m4 validate" value="">
                         <!--<input type="checkbox" name="option<?php echo $count ?>_2" id="option<?php echo $count ?>_2" value="1" >Correct Answer-->
                     </div>
         	         <div id="error_option<?php echo $count; ?>_2" class="error"></div>
                     
                     <div class="input_field col s12 m12" >
                         <label class="col s12 m2">Option 3</label>
                         <input type="text" name="option_<?php echo $count; ?>_3" id="option_<?php echo $count; ?>_3" class="col s12 m4 validate" value="">
                         <!--<input type="checkbox" name="option<?php echo $count ?>_3" id="option<?php echo $count ?>_3" value="1" >Correct Answer-->
                     </div>
         	         <div id="error_option<?php echo $count; ?>_3" class="error"></div>
                     
                     <div class="input_field col s12 m12" >
                         <label class="col s12 m2">Option 4</label>
                         <input type="text" name="option_<?php echo $count; ?>_4" id="option_<?php echo $count; ?>_4" class="col s12 m4 validate" value="">
                         <!--<input type="checkbox" name="option<?php echo $count ?>_4" id="option<?php echo $count ?>_4" value="1" >Correct Answer-->
                     </div>
         	         <div id="error_option<?php echo $count; ?>_4" class="error"></div>
                     
                     <!--<div id="error_option<?php echo $count; ?>_5" class="error"></div>-->
                                      
                  </div>
                  </div>
        <?php
		exit;
	}
	
	public function survey_question_radiooption()
	{
		
		$questionr1_count = $_POST['questionr1_count'];
		
		$option_count = $_POST['option_count'];
		?>
        	<div class="input_field col s12 m12" id="optionrr_<?php echo $option_count ?>_<?php echo $questionr1_count ?>" >
		        	         <label class="col s12 m2">Option <?php echo $questionr1_count ?></label>
		            	     <input type="text" name="optionr_<?php echo $option_count ?>_<?php echo $questionr1_count ?>" id="optionr_<?php echo $option_count ?>_<?php echo $questionr1_count ?>" value="" class="col s12 m4 validate" >
                             <!--<input type="checkbox" onclick="checkforradio(this.id);" name="optionrrr_<?php echo $option_count ?>_<?php echo $questionr1_count ?>" id="optionrrr_<?php echo $option_count ?>_<?php echo $questionr1_count ?>" value="1" >Correct Answer-->
		    </div>
            
            <div id="error_optionr<?php echo $option_count ?>_<?php echo $questionr1_count ?>" class="error"></div>
            
        <?php
		
		exit;
	}
	
	function delete_question($survey_q_id){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        $this->SurveyQuestion->id = $survey_q_id; //exit;
        
        if($this->SurveyQuestion->delete()){
			$this->SurveyQuestionOption->deleteAll(array('SurveyQuestionOption.survey_question_id' => $survey_q_id), false);
			$this->Session->setFlash('Question deleted successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
            exit();						
        }
		else
		{
			$this->Session->setFlash('Unable to delete the question. Please try again later','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'surveys','action' => 'question_view'));
            exit();	
		}
        
    }
    
     public function view_question_survey($survey_id = null) {
        
        $survey_question = $this->SurveyQuestion->find('all', array('conditions' => array('SurveyQuestion.survey_id' => $survey_id)));        
        $this->set('survey_questions', $survey_question);
        $this->layout = "admin";
    }
	
}

?>