<?php
class AdminsController extends AppController {

	public $uses = array('Admin','AgentUserInfo','Job','TestResult','ClientUserInfo','AdminDashMsg','MinPackage','PackageDetail','AgentRank','ClientNotification','AgentNotification','AgentDevice');
	
    public $paginate = array(
        'limit' => 25,
        'conditions' => array('status' => '1'),
        'order' => array('Admin.username' => 'asc')
    );
		
		
    public function beforeFilter() {
        $this->layout = 'login';
        parent::beforeFilter();
        $this->Auth->allow('login','add','forgot_password','forgot');
		
    }


    function create_subadmin()
    {
		//pr($_SESSION);exit;
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post')){
			
            $post = $this->request->data;
			
            $post['Admin']['ip_address'] = $this->request->clientIp();
			$post['Admin']['username'] = $post['username'];
			$post['Admin']['email_address'] = $post['email'];
			$post['Admin']['mobile_number'] = $post['mobile_no'];
            $post['Admin']['password'] = AuthComponent::password($post['c_password']);
            $post['Admin']['created_by_admin'] = $this->Auth->user('admin_id');
            //$post['Admin']['created_by_name'] = $this->Auth->user('username');
			$post['Admin']['is_active'] = '1';
            $post['Admin']['created'] = date('Y-m-d H:i:s');
            $username_list = $this->Admin->find('all', array('conditions' => array('Admin.username' => $post['Admin']['username'])));
            if(empty($username_list)){
                if($this->Admin->save($post))
                {
                    $this->Session->setFlash('Your Sub Admin user add successfully.','default',array('class'=>'successmsg'));
                    $this->redirect(array('controller'=>'admins','action'=>'profile#test2'));
                }
                else
                {
                    $this->Session->setFlash('Error ! In add your sub admin.please try again.','default',array('class'=>'successmsg'));
                    $this->redirect(array('controller'=>'admins','action'=>'profile#test2'));
                }
            }
            else
            {
                $this->Session->setFlash('Error ! Username already available. Please select new username.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'admins','action'=>'profile#test2'));               
            }
        }        
        $this->layout = 'admin';        
    }
    
    function activate_inactive($admin_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->Admin->id = $admin_id; //exit;
        
        if($this->Admin->saveField('is_active',$status)){
            $this->Session->setFlash('Sub Admin status update successful.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'admins','action' => 'profile'));
            exit();
        } else {
            $this->Session->setFlash('Error ! In change sub admin status.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'admins','action' => 'profile'));
            exit();
        }
        
        
    }
    
    function delete_subadmin($admin_id=null){
		
		//echo $_SESSION['Auth']['User']['type'];	exit;
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        $this->Admin->id = $admin_id; //exit;
        
        if($this->Admin->delete()){
			//echo "firsrt"; exit;
            $this->Session->setFlash('Your Sub Admin delete successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'admins','action' => 'profile'));
            exit();
        } else {
			//echo "second"; exit;
            $this->Session->setFlash('Error ! In delete sub admin.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'admins','action' => 'profile'));
            exit();
        }
    }
    
    function subadmin_list(){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        $this->layout = 'admin';
        $subadmin_data = $this->Admin->find('all',array('conditions'=>array('admin_type'=>0,'created_by_admin'=>$_SESSION['Auth']['User']['admin_id'])));
        $this->set('subadmin_list',$subadmin_data);
		//pr($subadmin_data);exit;
		//$this->render('profile',$data);
        
    }
    
	public function dashboard() {
		$this->layout = 'admin';
		$limit = 3;
		$client_list = $this->ClientUserInfo->find('all',array('fields'=>array('ClientUserInfo.client_id','ClientUserInfo.company_name','ClientUserInfo.email_address','ClientUserInfo.mobile_number','ClientUserInfo.is_active'),'conditions'=>array('ClientUserInfo.is_blocked'=>0),'order'=>array('created DESC'), 'limit' => $limit  ,'recursive'=>-1));
		
		$agent_list = $this->AgentUserInfo->find('all',array('fields'=>array('AgentUserInfo.agent_id','AgentUserInfo.agent_name','AgentUserInfo.email_address','AgentUserInfo.mobile_number','AgentUserInfo.is_active'),'conditions'=>array('AgentUserInfo.is_blocked'=>0),'order'=>array('created DESC'), 'limit' => $limit,'recursive'=>-1));
		
		$display_msgs = $this->AdminDashMsg->find('all',array('order'=>array('created DESC'), 'limit' => 5,'recursive'=>-1));
		
		
		$job_query = "SELECT 
            (SELECT COUNT(*) FROM jobs WHERE job_end_date < '".NOW."' ) AS completedJobs, 
            (SELECT COUNT(*) FROM jobs WHERE job_end_date > '".NOW."' AND job_start_date <'".NOW."' ) AS inProgress,
            (SELECT COUNT(*) FROM jobs WHERE job_start_date > '".NOW."' ) AS futureJobs";
		$jobs_count = $this->Job->Query($job_query);
		
		$test_query = "SELECT 
            (SELECT COUNT(*) FROM test_results WHERE is_passed = 1 ) AS passCount, 
            (SELECT COUNT(*) FROM test_results WHERE is_passed = 0 ) AS failCount";
		$tests_count = $this->TestResult->Query($test_query);
		
		$leaderboard = "SELECT (ui.points_earned * (SELECT AVG(rating_points)
													FROM agent_ratings ar
													WHERE ar.agent_id = ui.agent_id)) as totalPoints, ui.*
						FROM agent_user_infos ui, agent_ratings ar
						WHERE ar.agent_id = ui.agent_id AND ui.is_active = 1 AND ui.is_blocked = 0
						GROUP BY ui.agent_id
						ORDER BY totalPoints DESC
						LIMIT 0, 5;";
		$agentLeaderBoard = $this->AgentUserInfo->Query($leaderboard);
		//pr($client_list);exit;	
		$this->set('jobChart',$jobs_count);
		$this->set('testChart',$tests_count);
		$this->set('leaderboard',$agentLeaderBoard);
		$this->set('clientList',$client_list);
		$this->set('agentList',$agent_list);
		$this->set('displayMsgs',$display_msgs);
	}
	
    public function login() {

        if ($this->Session->check('Auth.User')) {
            $this->redirect(array('controller'=>'Home','action' => 'index'));
        }
		//echo AuthComponent::password($this->request->data['Admin']['password']);exit;
        if ($this->request->is('post')) {
			//echo AuthComponent::password($this->request->data['Admin']['password']);exit;
			//$this->Auth->password($this->request->data['password']);
			//pr($this->request->data);
			if($admin = $this->Admin->find('first',array('conditions'=>array('username'=>$this->request->data['Admin']['username']),'recursive'=>-1)))
			{
				/*pr($admin);
				$this->Session->write($admin);
				pr($_SESSION);*/
				//echo AuthComponent::password($this->request->data['Admin']['password']);exit;
				
				//echo "<pre>"; print_r($admin); exit;
				
				if($admin['Admin']['is_active'] == 1)
				{
					//debug($this->Auth->login()); die();
					//echo "first";exit;
					if ($this->Auth->login()) {
						//echo "second";exit;
						$this->redirect(array('controller' => 'admins','action' => 'dashboard'));
					} else {
						//echo "third";exit;
						$this->Session->setFlash(__('Invalid username or password !'));
					}
				}
				else
				{
					//echo "fourth";exit;
					$this->Session->setFlash(__('You have been blocked by Administrator !'));	
				}
			}
			else
			{
				//echo "fifth";exit;
				$this->Session->setFlash(__('Invalid Username !'));	
			}
        }
    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    public function index() {
        $this->paginate = array(
            'limit' => 6,
            'order' => array('Admin.username' => 'asc')
        );
        $admins = $this->paginate('Admin');
        $this->set(compact('admins'));
    }

    public function add() {
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        if ($this->request->is('post')) {

            $this->Admin->create();
            if ($this->Admin->save($this->request->data)) {
                $this->Session->setFlash(__('The admin has been created'));
                $this->redirect(array('controller' => 'admins', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The admin could not be created. Please, try again.'));
            }
        }
    }

    public function edit($id = null) {

        if (!$id) {
            $this->Session->setFlash('Please provide a admin id');
            $this->redirect(array('action' => 'index'));
        }

        $admin = $this->Admin->findById($id);
        if (!$admin) {
            $this->Session->setFlash('Invalid Admin ID Provided');
            $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Admin->id = $id;
            if ($this->Admin->save($this->request->data)) {
                $this->Session->setFlash(__('The admin has been updated'));
                $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->Session->setFlash(__('Unable to update your admin.'));
            }
        }

        if (!$this->request->data) {
            $this->request->data = $admin;
        }
    }

    public function delete($id = null) {

        if (!$id) {
            $this->Session->setFlash('Please provide a admin id');
            $this->redirect(array('action' => 'index'));
        }

        $this->Admin->id = $id;
        if (!$this->Admin->exists()) {
            $this->Session->setFlash('Invalid admin id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Admin->saveField('status', 0)) {
            $this->Session->setFlash(__('Admin deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Admin was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

   

    public function activate($id = null) {
        if (!$id) {
            $this->Session->setFlash('Please provide a admin id');
            $this->redirect(array('action' => 'index'));
        }

        $this->Admin->id = $id;
        if (!$this->Admin->exists()) {
            $this->Session->setFlash('Invalid admin id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Admin->saveField('status', 1)) {
            $this->Session->setFlash(__('Admin re-activated'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Admin was not re-activated'));
        $this->redirect(array('action' => 'index'));
    }
    
    
    public function profile(){
        
		$this->layout = 'admin';
       
        if($this->request->is('post')){
            
            $username = $this->request->data('username');
            $email_address = $this->request->data('email');
            $mobile_number = $this->request->data('mobile_no');			
            $admin_id = $this->request->data('admin_id');
           
            $query = $this->Admin->query("UPDATE admins SET email_address='$email_address',mobile_number='$mobile_number',username='$username' WHERE admin_id = '$admin_id'");
            
            
            
            if (isset($query)) {
                
                $this->Session->setFlash('Admin details updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'admins','action' => 'profile'));
                exit();
                
            } else {
                $this->Session->setFlash('Unable to update your admin details.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'admins','action' => 'profile'));
                exit();
            }
        }
		
		
		$package_details = $this->PackageDetail->find('all');
		$ranks = $this->AgentRank->find('all');
		
		$min_package = $this->MinPackage->find('first');
		
		
            
        $subadmin_data = $this->Admin->find('all',array('conditions'=>array('admin_type'=>0,'created_by_admin'=>$_SESSION['Auth']['User']['admin_id'])));
        $this->set('subadmin_list',$subadmin_data);
		
        $admin_id = $this->Auth->user('admin_id');
        $admin_id = $this->Auth->user('admin_id');
        $admindata = $this->Admin->find('first',array('conditions'=>array('admin_id'=>$admin_id)));

        $this->set('admin_settings',$admindata);
		$this->set('package_details',$package_details);
		$this->set('ranks',$ranks);
		$this->set('min_package',$min_package);
    }
    
    function changepassword(){
    
        if($this->request->is('post')){
           
            $oldpasstext = $this->request->data('password_old');
            $new_passwordtext = $this->request->data('password_new');
			
			$post = $this->request->data;
            
            $oldpass = AuthComponent::password($oldpasstext);
            $newpass = AuthComponent::password($new_passwordtext);
            
            $admin_id = $this->Auth->user('admin_id');
            
            $option = array(
                'conditions' => array('Admin.admin_id' => $admin_id,'password'=>$oldpass), //array of conditions
                'limit' => 1,
                'fields' => 'admin_id'
            );
            
            $checkpass_db =  $this->Admin->find('first',$option);
			
			$option1 = array(
                'conditions' => array('Admin.admin_id' => $admin_id,'password'=>$newpass), //array of conditions
                'limit' => 1,
                'fields' => 'admin_id'
            );
            
			if($checknewpass_db =  $this->Admin->find('first',$option1))
			{
				$this->Session->setFlash('New password cannot be same as old password.','default',
                array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'admins','action' => 'profile#test4'));
                exit();	
			}
			
			//pr($checkpass_db); exit;
			
            if(!empty($checkpass_db['Admin']['admin_id'])){
                $query = $this->Admin->query("UPDATE admins SET password='$newpass' WHERE admin_id = '$admin_id'");
            	if ($query>0){
					
					
					if(!empty($post['receive_email']))
					{
							$from = array('admin@emissary.com' => 'EMISSARY');
							$to = $this->Session->read('Auth.User.');
							$subject = 'Emmissary ,Password Update';
							
							$msg = 'Your password has been changed.Your new password is '.$new_passwordtext;
							
							$mail = $this->send_email($from,$to,$subject,$msg,$template=null);
					}
					
                    $this->Session->setFlash('Your password changed successfully.','default',
                    array('class' => 'successmsg'));
                    $this->redirect(array('controller'=>'admins','action' => 'profile'));
                    exit();
                } else {
                    $this->Session->setFlash('Error ! In change password.Please Try Again.','default',
                    array('class' => 'errormsg'));
                    $this->redirect(array('controller'=>'admins','action' => 'profile#test4'));
                    exit();
                }
            }else{
                $this->Session->setFlash('Your old password is wrong.','default',
                array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'admins','action' => 'profile#test4'));
                exit();
            }
            
        }
        
    }
	
	function forgot_password() {
        
		if($this->request->is('post') && !empty($this->request->data['email_address']))
		{ 
					//$merdetail = $this->MerchantDetail->findAllByEmail($this->request->data['email']);
					if($admin = $this->Admin->find('first',array('conditions'=>array('email_address' => $this->request->data['email_address'],'is_active' => 1),'recursive'=>-1)))
					{
					
						//pr($merdetail);
						 $ver_code = md5(microtime());
						 $from = array('admin@emissary.com' => 'EMISSARY');
						 $to = $this->request->data['email_address'];
						 $subject = 'Emmissary! Reset Password';
						 $link = ADMIN_ROOT.'admins/forgot/?vercode='.$ver_code.'&email_address='.urlencode($to);
						$linkOnMail = '<a href="'.$link.'" class="link2" target="_blank">Click Here to Update Your password </a>';
						 $msg = $linkOnMail;
						 $template = 'forgot_password';
						 $data['verification_code']= $ver_code;
						 $data['admin_id'] = $admin['Admin']['admin_id']; 
						 if($this->Admin->save($data))
						 {
							$mail = $this->send_email($from,$to,$subject,$msg,$template);	
							
							echo 'Please check your mailbox for reset password.';
							exit;
						 }
						 else
						 {
							 echo 'Try again later!';
							 exit;	 
						 }
							
					
				} else {
					echo 'You are not a registered user !';
					exit;
				}
		}
	}
	
	function forgot()
	{
					$vercode = $_GET['vercode'];
					$email_address = urldecode($_GET['email_address']);
		
					if($this->request->is('post'))
					{
						//pr($this->request->data);exit;
						if($verify = $this->Admin->find('first',array('conditions'=>array('verification_code' => $vercode, 'email_address' => $email_address, 'is_active' =>1))))
						{
							isset($this->request->data['Admin']['new_password'])?$this->request->data['Admin']['new_password'] = $this->Auth->password($this->request->data['Admin']['new_password']):'';
							$data['admin_id'] = $verify['Admin']['admin_id'];
							$data['password'] = $this->request->data['Admin']['new_password'];
							$data['verification_code'] = '';
							if($this->Admin->save($data))
							{
								$this->Session->setFlash('Your Password changed successfully.','default',array('class' => 'errormsg'));
                    			$this->redirect(array('controller'=>'admins','action' => 'login'));
                    			exit();
							}
							else
							{
								$message = 'Please Try again later!';
								
							}	
						}
						else
						{
							$message = 'Verification Code has been expired';
							
						}
					}
					else
					{
						$message = 'Enter new Password';	
					}
				
			$this->set('msg',$message);
	}
	
	public function receive_email()
	{
		$admin_id = $this->Auth->user('admin_id');
		$data['admin_id'] = $admin_id;
		$data['receive_email'] = $this->request->data['receive_email'];
		if($this->Admin->save($data))
		{
			echo "Email Notification changed successfully.";exit;
		}
		else
		{
			echo "Unable to update email notification.Please try again later.";exit;
		}
	}
	
	public function dashboard_copy()
	{
		$this->layout = 'admin';
		$job_query = "SELECT 
            (SELECT COUNT(*) FROM jobs WHERE job_end_date < '".NOW."' ) AS completedJobs, 
            (SELECT COUNT(*) FROM jobs WHERE job_end_date > '".NOW."' AND job_start_date <'".NOW."' ) AS inProgress,
            (SELECT COUNT(*) FROM jobs WHERE job_start_date > '".NOW."' ) AS futureJobs";
		$jobs_count = $this->Job->Query($job_query);
		
		$test_query = "SELECT 
            (SELECT COUNT(*) FROM test_results WHERE is_passed = 1 ) AS passCount, 
            (SELECT COUNT(*) FROM test_results WHERE is_passed = 0 ) AS failCount";
		$tests_count = $this->TestResult->Query($test_query);
		
		$leaderboard = "SELECT (ui.points_earned * (SELECT AVG(rating_points)
													FROM agent_ratings ar
													WHERE ar.agent_id = ui.agent_id)) as totalPoints, ui.*
						FROM agent_user_infos ui, agent_ratings ar
						WHERE ar.agent_id = ui.agent_id AND ui.is_active = 1 AND ui.is_blocked = 0
						GROUP BY ui.agent_id
						ORDER BY totalPoints DESC
						LIMIT 0, 5;";
		$agentLeaderBoard = $this->AgentUserInfo->Query($leaderboard);
		
		$this->set('jobChart',$jobs_count);
		$this->set('testChart',$tests_count);
		$this->set('leaderboard',$agentLeaderBoard);
	}
	
	public function manage_package() 
	{
		$this->layout = 'admin';
		
		if($this->request->is('post'))
		{			
			$post = $this->request->data;
			//pr($post);exit;
			
			if($post['type']=='1')
			{
				
				$data1['min_package_fee'] = $post['min_package_fee'];
			
				$data2 = $this->MinPackage->save($data1);
				
				for($start=0;$start<=5;$start++)
				{
					$data3[$start]['label_name'] = $post['label_name'][$start];
					
					$data3[$start]['cost'] = $post['cost'][$start];
					
				}
			}
			else if($post['type']=='2')
			{
				$data1['min_package_id'] = $post['min_package_id'];
				
				$data1['min_package_fee'] = $post['min_package_fee'];
			
				$data2 = $this->MinPackage->save($data1);
				
				for($start=0;$start<=5;$start++)
				{
					$data3[$start]['package_detail_id'] = $post['package_detail_id'][$start];
					
					$data3[$start]['label_name'] = $post['label_name'][$start];
					
					$data3[$start]['cost'] = $post['cost'][$start];					
				}
			}
			
			$data4 = $this->PackageDetail->saveAll($data3);
			
			if(isset($data2) && !empty($data2) || isset($data4) && !empty($data4))
			{
				$this->Session->setFlash('Package details updated successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'admins','action'=>'profile'));
				exit();
			}
			else
			{
				$this->Session->setFlash('Unable to update Package Details.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'admins','action'=>'profile'));
				exit();
			}
		}
		
		
	}
	
	public function send_notification() 
	{
		$this->layout = 'admin';
		$notification_sent = false;
		if($this->request->is('post'))
		{			
			$post = $this->request->data;
			//pr($post);exit;
			$subject = "EMISSARY NOTIFICATION";
			$message = $post['notification_text'];
			$template = "admin_notification";
			if($post['notification_type']==1)
			{
				$agent_data['agent_rank'] = $post['agent_rank']; 
				$agent_data['last_active'] = $post['last_active']; 
				$agent_data['min_points'] = $post['minimum_points']; 
				$agent_data['test_status'] = $post['test_status']; 
				$agent_data['is_email'] = isset($post['email_notify'])?$post['email_notify']:0; 
				$agent_data['is_push'] = isset($post['push_notify'])?$post['push_notify']:0; 
				$agent_data['notification_content'] = $post['notification_text'];
				if($post['send_option'])
				{ 
					$agent_data['send_time'] = NOW; 
					$agent_data['is_sent'] = 1; 	
				}
				else
				{
					$agent_data['send_time'] = $post['send_time'];
					$agent_data['is_sent'] = 0;  		
				}
				if($this->AgentNotification->save($agent_data))
				{
					//$agents = $this->AgentUserInfo->find('all',array('fields'=>array('DISTINCT AgentUserInfo.email_address','AgentUserInfo.agent_id'),'conditions'=>array('AgentUserInfo.agent_rank'=> $agent_data['agent_rank'], 'AgentUserInfo.last_login <=' => $agent_data['last_active'], 'AgentUserInfo.points_earned >=' => $agent_data['min_points'], 'AgentUserInfo.test_passed' => $agent_data['test_status']),'recursive'=>-1));
					
					$agents = $this->AgentUserInfo->find('all',array('fields'=>array('DISTINCT AgentUserInfo.email_address'),'conditions'=>array('AgentUserInfo.is_active' => 1),'recursive'=>-1));
					
					
					
					$agent_devices = $this->AgentDevice->find('all',array('fields'=>array('AgentDevice.*'),'conditions'=>array('AgentUserInfo.is_active' => 1),'recursive'=>1));
					
					foreach($agent_devices as $device)
					{
						if($device['AgentDevice']['device_type'] == 'android')
						{
							$agent_android[] = 	$device['AgentDevice']['registration_id'];
						}	
						else
						{
							$agent_ios[] = 	$device['AgentDevice']['registration_id'];
						}
					}
					
					$android_devices = array_chunk($agent_android,999);
						/*	
					foreach($android_devices as $android_chunk)
					{	
						$this->send_push_android($android_chunk,$subject,$message);
					}*/
					
					$this->send_push_ios($agent_ios,$subject,$message);
					
					//pr($agent_android);
						pr($agent_ios);exit;	
					
					foreach($clients as $client)
					{
						$client_email[] = $client['ClientUserInfo']['email_address'];
					}
					pr($client_email);exit;	
					$this->send_email(ADMIN_EMAIL,$client_email,$subject,$message,$template);
					
					$notification_sent = true;
						
					/*$agent_query = "SELECT (SELECT GROUP_CONCAT(DISTINCT device_id) FROM agent_devices WHERE device_type='android' AND agent_id=11) AS android, (SELECT GROUP_CONCAT(DISTINCT device_id) FROM agent_devices WHERE device_type='IOS' AND agent_id=11) AS ios,aui.email_address
from agent_devices
left join agent_user_infos aui on aui.agent_id=agent_devices.agent_id
where agent_devices.agent_id=11
GROUP by aui.email_address";*/
					pr($agents);
					pr($agent_devices);exit;
				}
			}
			else if($post['type']=='2')
			{
				$client_data['notify_reason'] = $post['notify_reason']; 
				$client_data['jobs_ending_in'] = $post['jobs_ending_in']; 
				$client_data['notification_content'] = $post['notification_text']; 
				if($post['send_option_c'])
				{ 
					$client_data['send_time'] = NOW; 
					$client_data['is_sent'] = 1; 	
				}
				else
				{
					$client_data['send_time'] = $post['send_time'];
					$client_data['is_sent'] = 0;  		
				}
				
				if($this->ClientNotification->save($client_data))
				{
					//$clients = $this->AgentUserInfo->find('all',array('fields'=>array('DISTINCT AgentUserInfo.email_address','AgentUserInfo.agent_id'),'conditions'=>array('AgentUserInfo.agent_rank'=> $agent_data['agent_rank'], 'AgentUserInfo.last_login <=' => $agent_data['last_active'], 'AgentUserInfo.points_earned >=' => $agent_data['min_points'], 'AgentUserInfo.test_passed' => $agent_data['test_status']),'recursive'=>-1));
					
					$clients = $this->ClientUserInfo->find('all',array('fields'=>array('DISTINCT ClientUserInfo.email_address','ClientUserInfo.client_id'),'conditions'=>array(),'recursive'=>-1));
					
					foreach($clients as $client)
					{
						$client_email[] = $client['ClientUserInfo']['email_address'];
					}
					pr($client_email);exit;	
					$this->send_email(ADMIN_EMAIL,$client_email,$subject,$message,$template);
					
					$notification_sent = true;
						
				}
			}
			
			
			if(isset($data2) && !empty($data2) || isset($data4) && !empty($data4))
			{
				$this->Session->setFlash('Notification sent successfully.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'admins','action'=>'profile'));
				exit();
			}
			else
			{
				$this->Session->setFlash('Unable to send notification.','default',array('class'=>'successmsg'));
                $this->redirect(array('controller'=>'admins','action'=>'profile'));
				exit();
			}
		}
		
		
	}

}

?>