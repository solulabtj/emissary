<?php

class ReportsController extends AppController {

    public $uses = array('AgentUserInfo', 'ClientUserInfo', 'TestResult', 'TestBooking', 'AgentPayment');
    public $helpers = array('Html', 'Form');
    public $components = array('RequestHandler');

    public function view_reports($start_date = '', $end_date='', $type,$rank='',$test_status='',$status='',$booking_test='') {

        //$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
        //	$limit = 15;
        $start_date = date("Y-m-d",strtotime($start_date));
        $end_date = date("Y-m-d",strtotime($end_date));
        $type = $type;
        $rank = $rank;
        $test_status = $test_status;
        $status = $status;
        $booking_test = $booking_test;
        
        if ($type == 1) {
          // $Agents = $this->AgentUserInfo->find('all', array('fields' => array(), 'conditions' => array('created >=' => $start_date, 'created <' => $end_date), 'recursive' => 0, 'order' => array('created' => 'DESC')));
            
            if($rank != "" && $rank != "n" && $rank != "0"){
                $rank_condition = "AND aui.agent_rank = ".$rank; 
            }else{
                $rank_condition = "";
            }
            
            if($start_date != "" && $start_date != "0000-00-00" && $start_date != "1970-01-01"){
                $start_date_condition = "AND aui.last_login >= ".$start_date; 
            }else{
                $start_date_condition = "";
            }
            
            if($test_status != "" && $test_status != "n"){
                $test_status_condition = "AND aui.test_passed = "."'".$test_status."'"; 
            }else{
                $test_status_condition = "";
            }
            
             $Agents = $this->AgentUserInfo->query('SELECT aui.last_login, aui.test_passed, COUNT(aj.agent_id) AS applied_count ,SUM(aj.approved_by_client=1) AS completed_count, aui.agent_rank,aui.agent_name,aui.email_address FROM agent_jobs aj Right JOIN agent_user_infos aui ON  aj.agent_id = aui.agent_id WHERE 1 '.$rank_condition.' '.$test_status_condition.' '.$start_date_condition .' GROUP BY aui.agent_id');
             
             //$log = $this->AgentUserInfo->getDataSource()->getLog(false, false);
             //debug($log);
            $Agent_data = array();
            $x = 0;
            foreach ($Agents as $Agent) {
                $Agent_data[$x] = $Agent;
                $x++;
            }          
            $message = $Agent_data;            
            $status = 'yes';
        } elseif ($type == 2) {
            
            $current_date = date("Y-m-d");
            
            if($status != "" && $status != "n"){
                $status_condition = "AND cui.is_active = "."'".$status."'"; 
            }else{
                $status_condition = "";
            }
            
             if($start_date != "" && $start_date != "0000-00-00" && $start_date != "1970-01-01"){
                $start_date_condition = "AND cui.last_login >= ".$start_date; 
            }else{
                $start_date_condition = "";
            }
            //$clients = $this->ClientUserInfo->find('all', array('fields' => array(), 'conditions' => array('created >=' => $start_date, 'created <' => $end_date), 'recursive' => 0, 'order' => array('created' => 'DESC')));
            $clients = $this->ClientUserInfo->query("Select "
                    . " IFNULL((select COUNT(jbs.client_id) from jobs jbs where jbs.client_id=cui.client_id AND jbs.job_start_date != '0000-00-00 00:00:00' AND jbs.job_start_date <= $current_date AND jbs.job_end_date = '0000-00-00 00:00:00' GROUP BY jbs.client_id),0) as ongoing_jobs,"
                    . "cui.client_name,cui.email_address,cui.is_active as status,COUNT(jb.client_id) as jobs_posted,IFNULL(SUM(jb.submission_received),0) as submission_received,cs.modified from client_user_infos cui LEFT JOIN jobs jb ON cui.client_id = jb.client_id LEFT JOIN client_subscriptions cs ON cui.client_id = cs.client_id WHERE 1 ".$status_condition." ".$start_date_condition." GROUP BY cui.client_id");
           // $log = $this->AgentUserInfo->getDataSource()->getLog(false, false);
           // debug($log);
           // echo "<pre>";
           // pr($clients);
            $Client_data = array();
            $x = 0;
            foreach ($clients as $client) {
                $Client_data[$x] = $client;
                $x++;
            }
            $message = $Client_data;
            //$message['type'] = $type;
            $status = 'yes';
        } elseif ($type == 3) {
            
            if($start_date != "" && $start_date != "0000-00-00" && $start_date != "1970-01-01" && $end_date != "" && $end_date != "0000-00-00" && $end_date != "1970-01-01"){
                $date_condition = "AND tr.created BETWEEN '$start_date' AND '$end_date'" ; 
            }else{
                $date_condition = "";
            }
            
             if($test_status != "" && $test_status != "n"){
                $test_status_condition = "AND tr.is_passed = "."'".$test_status."'"; 
            }else{
                $test_status_condition = "";
            }
            //$test_results = $this->TestResult->find('all', array('fields' => array('TestResult.*', 'AgentUserInfo.agent_name'), 'conditions' => array('TestResult.created >=' => $start_date, 'TestResult.created <' => $end_date, 'AgentUserInfo.agent_id >' => 0), 'recursive' => 1, 'order' => array('TestResult.created' => 'DESC')));
            $test_results = $this->TestResult->query("Select aui.agent_name,tr.agent_scored,tr.maximum_score,tr.questions_answered,tr.total_questions,tr.percentage_earned,tr.is_passed from test_results tr LEFT JOIN agent_user_infos aui ON tr.agent_id=aui.agent_id WHERE tr.agent_id != '0' $test_status_condition $date_condition");
            //$log = $this->AgentUserInfo->getDataSource()->getLog(false, false);
            //debug($log);
            $test_data = array();
            $x = 0;
            foreach ($test_results as $test_result) {
                $test_data[$x] = $test_result;
                $x++;
            }
            $message = $test_data;

            $status = 'yes';
        } elseif ($type == 4) {
            
            if($start_date != "" && $start_date != "0000-00-00" && $start_date != "1970-01-01" && $end_date != "" && $end_date != "0000-00-00" && $end_date != "1970-01-01"){
                $date_condition = "AND tb.created BETWEEN '$start_date' AND '$end_date'" ; 
            }else{
                $date_condition = "";
            }
            
             if($booking_test != "" && $booking_test != "n"){
                $booking_test_condition = "AND tb.test_taken = "."'".$booking_test."'"; 
            }else{
                $booking_test_condition = "";
            }
            
            //$test_bookings = $this->TestBooking->find('all', array('fields' => array('TestBooking.*', 'AgentUserInfo.agent_name'), 'conditions' => array('TestBooking.created >=' => $start_date, 'TestBooking.created <' => $end_date, 'AgentUserInfo.agent_id >' => 0), 'recursive' => 1, 'order' => array('TestBooking.created' => 'DESC')));
            //$test_bookings = $this->TestBooking->find('all', array('fields' => array('TestBooking.*', 'AgentUserInfo.agent_name','AgentUserInfo.email_address'), 'conditions' => array('AgentUserInfo.agent_id >' => 0), 'recursive' => 1, 'order' => array('TestBooking.created' => 'DESC')));
            $test_bookings = $this->TestBooking->query("Select aui.agent_name,aui.email_address,tb.created,tb.test_booking_code,tb.test_cost,tb.test_taken from test_bookings tb LEFT JOIN agent_user_infos aui ON tb.agent_id=aui.agent_id WHERE tb.agent_id != '0' $booking_test_condition $date_condition");
            //$log = $this->TestBooking->getDataSource()->getLog(false, false);
            //debug($log);
            
            $test_data = array();
            $x = 0;
            foreach ($test_bookings as $test_booking) {
                $test_data[$x] = $test_booking;
                $x++;
            }
            $message = $test_data;

            $status = 'yes';
        } elseif ($type == 5) {
            if($start_date != "" && $start_date != "0000-00-00" && $start_date != "1970-01-01" && $end_date != "" && $end_date != "0000-00-00" && $end_date != "1970-01-01"){
                $date_condition = "WHERE ap.payment_time BETWEEN '$start_date' AND '$end_date'" ; 
            }else{
                $date_condition = "";
            }
            //$payments = $this->AgentPayment->find('all', array('conditions' => array('payment_time >=' => $start_date, 'payment_time <' => $end_date), 'recursive' => 1, 'order' => array('payment_time' => 'DESC')));
            //$payments = $this->AgentPayment->find('all', array('recursive' => 1, 'order' => array('payment_time' => 'DESC')));
            $payments = $this->AgentPayment->query("Select aui.agent_name,aui.email_address,ap.transaction_id,ap.payment_time,ap.amount_paid from agent_payments ap  LEFT JOIN agent_user_infos aui ON ap.agent_id = aui.agent_id $date_condition");
            //$log = $this->AgentPayment->getDataSource()->getLog(false, false);
            //debug($log);
            $payment_data = array();
            $x = 0;
            foreach ($payments as $payment) {
                $payment_data[$x] = $payment;
                $x++;
            }
            $message = $payment_data;

            $status = 'yes';
        }elseif ($type == 6) {
            if($start_date != "" && $start_date != "0000-00-00" && $start_date != "1970-01-01" && $end_date != "" && $end_date != "0000-00-00" && $end_date != "1970-01-01"){
                $date_condition = "AND cs.transaction_time BETWEEN '$start_date' AND '$end_date' " ; 
            }else{
                $date_condition = "";
            }
                       
            $subpayments = $this->ClientUserInfo->query("Select cui.client_name,cui.email_address,cs.transaction_id,cs.transaction_time,cs.amount_paid from client_subscriptions cs  LEFT JOIN client_user_infos cui ON cs.client_id = cui.client_id WHERE cs.client_id != '0' $date_condition");
            //$log = $this->AgentPayment->getDataSource()->getLog(false, false);
            //debug($log);
            $subpayment_data = array();
            $x = 0;
            foreach ($subpayments as $spayment) {
                $subpayment_data[$x] = $spayment;
                $x++;
            }
            $message = $subpayment_data;

            $status = 'yes';
        } else {
            $message = 'Invalid parameters';
        }

        return $message;
        //$this->set('data',$data);
    }

    public function reports() {
        $rank = $this->AgentUserInfo->query('SELECT * FROM agent_ranks ');
        $this->set('rank', $rank);
        $this->layout = 'admin';
    }

    public function fetch_report() {
        if ($this->request->is('ajax')) {   
            //echo "<pre>"; pr($_POST);
            $start_date = date('Y-m-d', strtotime($_POST['startdate']));
            $end_date = date('Y-m-d', strtotime("+1 day", strtotime($_POST['enddate'])));
            $type = $_POST['type'];
            $rank = $_POST['rank'];
            $test_status = $_POST['test_status'];
            $status = $_POST['client_status'];
            $booking_test = $_POST['booking_test'];

            $res = $this->view_reports($start_date, $end_date, $type,$rank,$test_status,$status,$booking_test);
            $this->set('res', $res);
            $this->set('type', $type);            
        } else {
            echo "<table><tr><td>No data found</td><tr></table>";
        }
    }
    
    public function getjobSubmissionGraphData() {
        
        $start_date = date('Y-m-d',strtotime('-31 days'));
        $end_date = date('Y-m-d');
        
        $date_condition = "AND cui.created BETWEEN '$start_date' AND '$end_date'" ;
        $date_condition = "" ; 
       
        $clients = $this->ClientUserInfo->query("Select cui.client_name,IFNULL(cs.amount_paid,0) as amount_paid,IFNULL(SUM(jb.submission_received),0) as submission_received from client_user_infos cui LEFT JOIN jobs jb ON cui.client_id = jb.client_id LEFT JOIN client_subscriptions cs ON cui.client_id = cs.client_id WHERE cui.client_name != 'Admin'  ".$date_condition." GROUP BY cui.client_id LIMIT 5");
        
        $farray = array();
        $header = array('Client','Amount Paid','Submission');
        array_push($farray, $header);
        foreach ($clients as $value) {
            $subarray = array();

            array_push($subarray,  $value['cui']['client_name']);
            array_push($subarray, (int) $value['0']['amount_paid']);
            array_push($subarray, (int) $value['0']['submission_received']);            

            array_push($farray, $subarray);
        }


        //$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }
    
    public function getTestBookingGraphData() {
        
        $start_date = date('Y-m-d',strtotime('-7 days'));
        $end_date = date('Y-m-d');
        
        $date_condition = "AND created BETWEEN '$start_date' AND '$end_date'" ;
        //$date_condition = "" ; 
       
        $test = $this->ClientUserInfo->query("SELECT DATE(created) as date, COUNT(*) as test_booked FROM test_bookings WHERE 1 $date_condition  GROUP BY DATE(created) ORDER BY DATE(created)");

      // echo "<pre>";
      // pr($test);
        $farray = array();
        $header = array('Date','Test Booked');
        array_push($farray, $header);
        foreach ($test as $value) {
            $subarray = array();
            if($value['0']['date'] != "0000-00-00")
                $date = date("m-d-Y",  strtotime($value['0']['date']));
            
            array_push($subarray,  $date);
            array_push($subarray, (int) $value['0']['test_booked']);                       
            array_push($farray, $subarray);
        }


        //$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }
    
    public function getTestResultGraphData() {
     
        $job = $this->TestResult->query("SELECT SUM(case when is_passed = 1 then 1 else 0 end) as passed,( select count(test_result_id) from test_results where is_passed = '0' ) as failed FROM  test_results" );

       //$log = $this->AgentJob->getDataSource()->getLog(false, false);
        // debug($log);
        //echo "<pre>";
        //print_r($job);
        $farray = array();
        $header = array('Result', 'Count');
        array_push($farray, $header);        
        foreach ($job as $key => $value) {                                   
            $subarray = array();                         
            array_push($subarray, "Passed");
            array_push($subarray, (int) $value['0']['passed']);            
                  
            
            array_push($farray, $subarray);
            
        }
        
        foreach ($job as $key => $value) {                                   
            $subarray = array();                         
            
            array_push($subarray, "Failed");
            array_push($subarray, (int) $value['0']['failed']);            
            
            array_push($farray, $subarray);
            
        }


        //$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }
    
    public function getTestPaymentGraphData() {
        
        $start_date = date('Y-m-d',strtotime('-7 days'));
        $end_date = date('Y-m-d');
        
        $date_condition = "AND payment_time BETWEEN '$start_date' AND '$end_date'" ;
        //$date_condition = "" ; 
       
        $test = $this->ClientUserInfo->query("SELECT DATE(payment_time) as date, COUNT(*) as test_payment FROM agent_payments WHERE 1 $date_condition  GROUP BY DATE(payment_time) ORDER BY DATE(payment_time)");

      // echo "<pre>";
      // pr($test);
        $farray = array();
        $header = array('Date','Test Payment');
        array_push($farray, $header);
        foreach ($test as $value) {
            $subarray = array();
            if($value['0']['date'] != "0000-00-00")
                $date = date("m-d-Y",  strtotime($value['0']['date']));
            
            array_push($subarray,  $date);
            array_push($subarray, (int) $value['0']['test_payment']);                       
            array_push($farray, $subarray);
        }


        //$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }
    
    public function getSubPaymentGraphData() {
        
        $start_date = date('Y-m-d',strtotime('-7 days'));
        $end_date = date('Y-m-d');
        
        $date_condition = "AND transaction_time BETWEEN '$start_date' AND '$end_date'" ;
        //$date_condition = "" ; 
       
        $test = $this->ClientUserInfo->query("SELECT DATE(transaction_time) as date, COUNT(*) as sub_payment FROM client_subscriptions WHERE 1 $date_condition  GROUP BY DATE(transaction_time) ORDER BY DATE(transaction_time)");

      // echo "<pre>";
      // pr($test);
        $farray = array();
        $header = array('Date','Test Payment');
        array_push($farray, $header);
        foreach ($test as $value) {
            $subarray = array();
            if($value['0']['date'] != "0000-00-00")
                $date = date("m-d-Y",  strtotime($value['0']['date']));
            
            array_push($subarray,  $date);
            array_push($subarray, (int) $value['0']['sub_payment']);                       
            array_push($farray, $subarray);
        }


        //$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }
    
    
     public function getGraphData() {
        // echo "<pre>";

        $start_date = date("Y-m-d");
        $end_date = strtotime("$start_date -1 year");
        $end_date = date("Y-m-d", $end_date);
        $condition_date = "WHERE agent_jobs.modified BETWEEN " . "'" . $end_date . "'" . "AND" . "'" . $start_date . "'";

        $AgentsNames = $this->AgentUserInfo->query('SELECT agent_user_infos.agent_id, agent_user_infos.agent_name
            FROM agent_jobs
            LEFT JOIN agent_user_infos on agent_user_infos.agent_id = agent_jobs.agent_id
            GROUP BY agent_jobs.agent_id
        ');
        //print_r($AgentsNames);
        $finalArr = array();
        foreach ($AgentsNames as $agents) {
            $AgentsJobs = $this->AgentUserInfo->query("SELECT agent_id,COUNT(modified) as completed_job, LPAD(MONTH(modified), 2, '0') as month,YEAR(modified) as year 
                FROM agent_jobs
                WHERE modified > DATE_ADD(Now(), INTERVAL -1 YEAR)
                AND agent_id ={$agents[agent_user_infos][agent_id]}
                AND approved_by_client=1
                GROUP BY monthname(modified)
            ");

            $tempArr = array();
            foreach ($AgentsJobs as $jobs) {
                $tempArr[] = $jobs[0];
            }
            $finalArr[] = array(
                'agent_id' => $agents[agent_user_infos][agent_id],
                'agent_name' => $agents[agent_user_infos][agent_name],
                'jobs' => $tempArr
            );
        }
        /* echo "<pre>";
          print_r($finalArr);
          exit;
         */
        foreach ($finalArr as &$eachArray) {
            $agentName[] = $eachArray['agent_name'];
            for ($i = 0; $i < 12; $i++) {
                $tmp_date = strtotime(date('Y-m-d') . "-$i months");
                $month = date("m", $tmp_date);
                $year = date("Y", $tmp_date);

                $add_date = true;
                foreach ($eachArray['jobs'] as $eachJob) {
                    if ($eachJob['month'] == $month && $eachJob['year'] == $year) {
                        $add_date = FALSE;
                        break;
                    }
                }
                if ($add_date) {
                    $eachArray['jobs'][] = array(
                        'completed_job' => 0,
                        'month' => $month,
                        'year' => $year
                    );
                }
            }
        }
          // echo "<pre>";
          // print_r($finalArr);
          //   exit;

        $month = array();
        for ($i = 1; $i < 13; $i++) {
            $month[$i] = array();
            $month[$i][] = (string) $i;
            //echo'<pre>'; print_r($finalArr);
            foreach ($finalArr as $each) {
                foreach ($each['jobs'] as $eachMonth) {
                    if ($eachMonth['month'] == $i) {
                        $month[$i]['0'] = $eachMonth['month']."/".$eachMonth['year'];
                        $month[$i][] = (int) $eachMonth['completed_job'];
                    }
                }
            }
        }


       // echo "<pre>";
       // print_r($finalArr);
       // print_r($month);
       // echo json_encode($month);
       // exit;

        $response = array('finalArr' => $finalArr, 'month' => $month);
        // print_r($agentName);
        // print_r($finalArr);
        echo json_encode($response, true);
        exit;
    }
    
}
