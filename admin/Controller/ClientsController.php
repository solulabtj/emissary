<?php
class ClientsController extends AppController {

    public $uses = array('ClientUserInfo','Job','Survey','ClientAddress','Coupon');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function client_view() {
                $this->layout = 'admin';
                $allagents = $this->ClientUserInfo->find('all', array('fields' => array('ClientUserInfo.*'),'order' => array('ClientUserInfo.client_id' => 'ASC'), 'recursive' => -1));
                
                $this->set('listing',$allagents);
	}
	
	function activate_inactive($client_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->ClientUserInfo->id = $client_id;
		
        if($this->ClientUserInfo->saveField('is_blocked',$status)){
			//echo "success"; exit;
			
			 $this->ClientAddress->updateAll(array("is_blocked"=>$status),array("ClientAddress.client_id"=>$client_id));
			 $this->Coupon->updateAll(array("is_blocked"=>$status),array("Coupon.client_id"=>$client_id));
			 $this->Job->updateAll(array("is_blocked"=>$status),array("Job.client_id"=>$client_id));
			 $this->Survey->updateAll(array("is_blocked"=>$status),array("Survey.client_id"=>$client_id));
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'clients','action' => 'client_view'));
            exit();
        } else {
			//echo "failed"; exit;
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'clients','action' => 'client_view'));
            exit();
        }
        
        
    }
    
    public function client_details_view() {
                $id = $this->params['pass']['0'];
                $this->layout = 'admin';
                $allclients = $this->ClientUserInfo->find('first', array('fields' => array('ClientUserInfo.*'),'conditions' => array('ClientUserInfo.client_id' => $id),'order' => array('ClientUserInfo.client_id' => 'ASC'), 'recursive' => -1));
                $alljob = $this->Job->find('all', array('conditions' => array('Job.client_id' => $id), 'recursive' => -1));
                /*echo "<pre>";
                pr($alljob);
                exit;*/
                $this->set('listings',$alljob);
                $this->set('listing',$allclients);
	}

}

?>