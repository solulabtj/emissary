<?php
class QuestionsController extends AppController {
	
   public $uses = array('TestQuestion','TestSettings','TestQuestionOption');
	
   public function beforeFilter() {
        
        parent::beforeFilter();
        
    }
	
	public function question_view() {
                $this->layout = 'admin';
                $test_q = $this->TestQuestion->find('all',array('fields'=>array(),'recursive'=>1));
                
                $this->set('listing',$test_q);
	}
	
	function activate_inactive($test_question_id=null,$status = null){
		
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->TestQuestion->id = $test_question_id; //exit;
        
        if($this->TestQuestion->saveField('is_blocked',$status)){
			
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'questions','action' => 'question_view'));
            exit();
        } else {
			
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'questions','action' => 'question_view'));
            exit();
        }
        
        
    }
	
	function question_add()
	{
		if($_SESSION['Auth']['User']['admin_type'] != 1) {
		$this->redirect(array('controller'=>'home','action' => 'index'));	
		exit;
		}
        
        if($this->request->is('post'))
		{			
			$post = $this->request->data;
			//pr($post); exit;
			
			$count = $post['question_count'];	
			$data5 = array();
			for($i=1;$i<$count;$i++)
			{				
				unset($data1);
				unset($data3);
				unset($test_question_id_arr);
				unset($test_question_id);
				
				$data1 = array();
				$data3 = array();
				$test_question_id_arr = array();
				$test_question_id = 0;
				
				$data1['test_question_id'] = 0;
				$data1['test_question_description'] = $post['test_question_description_'.$i];				
				
				if($this->TestQuestion->save($data1))
				{
					
					$test_question_id_arr = $this->TestQuestion->find('first',array('order'=>array('TestQuestion.test_question_id DESC'),'recursive' => -1));
					
					$test_question_id = $test_question_id_arr['TestQuestion']['test_question_id'];
					
				}
				for($j=1;$j<=4;$j++)
				{
					$data3[$j]['test_question_id'] = $test_question_id;
					$data3[$j]['answer_text'] = $post['option_'.$i.'_'.$j];
					
					if(isset($post['option'.$i.'_'.$j]))
					{
						$data3[$j]['is_correct'] = $post['option'.$i.'_'.$j];
					}
					else
					{
						$data3[$j]['is_correct'] = '0';
					}
					
				}
				$data5[$i] = $this->TestQuestionOption->saveAll($data3);
				
			}
			
			if(isset($test_question_id) && !empty($test_question_id) || isset($data5) && !empty($data5))
			{
				$this->Session->setFlash('Test Question added successfully.','default',array('class'=>'successmsg'));
				$this->redirect(array('controller'=>'questions','action'=>'question_view'));
			}
			else
			{
				$this->Session->setFlash('Error ! Unable to add Test Question.please try again.','default',array('class'=>'successmsg'));
				$this->redirect(array('controller'=>'questions','action'=>'question_view'));
			}
			
		}
        
        $this->layout = 'admin';
        		
	}
	
	function test_settings()
	{
		$this->layout = 'admin';
        
		if($this->request->is('post')){
            
			$test_setting_id = $this->request->data('test_setting_id');
			$test_overview = $this->request->data('test_overview');
            $no_of_questions = $this->request->data('no_of_questions');
            $test_cost = $this->request->data('test_cost');
            $test_duration = $this->request->data('test_duration');			
            $passing_percentage = $this->request->data('passing_percentage');
           
            $query = $this->TestSettings->query("UPDATE test_settings SET no_of_questions='$no_of_questions',test_cost='$test_cost',test_overview='$test_overview',test_duration='$test_duration',passing_percentage='$passing_percentage' WHERE test_setting_id = '$test_setting_id'");
            
            if (isset($query)) {
                
                $this->Session->setFlash('Test settings details updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'questions','action' => 'test_settings'));
                exit();
                
            } else {
                $this->Session->setFlash('Unable to update your Test settings details.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'questions','action' => 'test_settings'));
                exit();
            }
        }
		
        $setting = $this->TestSettings->find('all', array('fields' => array('TestSettings.*'),'recursive' => -1));
             
                $this->set('listing',$setting);
	}
	
	function delete_question($test_question_id=null){
		$ID = $test_question_id;
		
		if($this->TestQuestion->delete($ID))
		{
			if($this->TestQuestionOption->deleteAll(array('TestQuestionOption.test_question_id'=>$ID)))
			{
				$this->Session->setFlash('Question deleted successfully.','default',
    array('class' => 'successmsg'));
				$this->redirect(array('controller'=>'questions','action' => 'question_view'));
				exit();
			}
			else
			{
				$this->Session->setFlash('Unable to delete Question.','default',
    array('class' => 'successmsg'));
				$this->redirect(array('controller'=>'questions','action' => 'question_view'));
				exit();
			}
			
		}
		else
		{
			$this->Session->setFlash('Unable to delete Question.','default',
    array('class' => 'successmsg'));
			$this->redirect(array('controller'=>'questions','action' => 'question_view'));
			exit();
		}
	}
	
	
	function edit_question($test_question_id=null){
		
		$this->layout = 'admin';
		
		$ID = $test_question_id;
       
        if($this->request->is('post')){
            
            $test_question_id = $this->request->data('test_question_id');
            $test_question_description = $this->request->data('test_question_description');
            $post = $this->request->data;
			
            $query = $this->TestQuestion->query("UPDATE test_questions SET test_question_description='$test_question_description' WHERE test_question_id = '$test_question_id'");
            
            if ($query) {
                
				$query1 = $this->TestQuestionOption->query("DELETE FROM test_question_options WHERE test_question_id = '$test_question_id'");
				
				$data2 = array();
				
				for($i=1;$i<=4;$i++)
				{
					$data1['test_question_id'] = $test_question_id;
					$data1['answer_text'] = $post['option_'.$i];
					
					if(isset($post['option'.$i]))
					{
						$data1['is_correct'] = $post['option'.$i];
					}
					else
					{
						$data1['is_correct'] = '0';
					}
					
					//$this->TestQuestionOption->save($data1);
					
					array_push($data2,$data1);
				}
				
				if($this->TestQuestionOption->saveAll($data2))
				{
				
                $this->Session->setFlash('Question updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'questions','action' => 'question_view'));
                exit();
				
				}
				
				else
				{
					$this->Session->setFlash('Unable to update Question.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'questions','action' => 'question_view'));
                exit();
				}
				
                
            } else {
                $this->Session->setFlash('Unable to update Question.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'questions','action' => 'question_view'));
                exit();
            }
        }
        
		$data = $this->TestQuestion->find('all',array('conditions'=>array('test_question_id'=>$ID)));
		
        $this->set('singlerecord',$data);
		
    }
	
	function test_question_add()
	{
		$count = $_POST['question_count'];
		
		?>
        
        <div id="apply_question<?php echo $count; ?>" >
        
        <h4><i class="halflings-icon user"></i><span class="break"></span>Question <?php echo $count; ?></h4>
        		<div class="input_field col s12 m12">
                  <label class="col s12 m2">Question Description</label>
                  <input type="text" class="col s12 m4 validate" name="test_question_description_<?php echo $count; ?>" id="test_question_description_<?php echo $count; ?>" value="">
                  
                  </div>
                   <div id="error_question_<?php echo $count; ?>" class="error"></div>
                   
                   <div id="apply_content_<?php echo $count; ?>" >
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option A</label>
                 <input type="text" name="option_<?php echo $count; ?>_1" id="option_<?php echo $count; ?>_1" class="col s12 m4 validate" value="">
                 <input type="checkbox" name="option<?php echo $count; ?>_1" id="option<?php echo $count; ?>_1" value="1" >Correct Answer
                   </div>
                   <div id="error_option<?php echo $count; ?>_1" class="error"></div>
                   
                  <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option B</label>
                 <input type="text" name="option_<?php echo $count; ?>_2" id="option_<?php echo $count; ?>_2" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="">
                 <input type="checkbox" name="option<?php echo $count; ?>_2" id="option<?php echo $count; ?>_2" value="1" >Correct Answer
                   </div>
                   <div id="error_option<?php echo $count; ?>_2" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option C</label>
                 <input type="text" name="option_<?php echo $count; ?>_3" id="option_<?php echo $count; ?>_3" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="" >
                 <input type="checkbox" name="option<?php echo $count; ?>_3" id="option<?php echo $count; ?>_3" value="1" >Correct Answer
                   </div>
                   <div id="error_option<?php echo $count; ?>_3" class="error"></div>
                   
                   <div class="input_field col s12 m12">
                  <label class="col s12 m2">Option D</label>
                 <input type="text" name="option_<?php echo $count; ?>_4" id="option_<?php echo $count; ?>_4" class="col s12 m4 validate" onkeypress="return onlyNumbers(event)" value="" >
                 <input type="checkbox" name="option<?php echo $count; ?>_4" id="option<?php echo $count; ?>_4" value="1" >Correct Answer
                   </div>
                   <div id="error_option<?php echo $count; ?>_4" class="error"></div>
                   
                   <div id="error_option<?php echo $count; ?>_5" class="error"></div>
                   
                   </div>
                   </div>
        <?php
		exit;	
	}
	
}

?>