<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail','Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	
	public $components = array('Session',
        'Auth' => array('authenticate' => array('Form' => array( 'userModel' => 'Admin',
                                                         'fields' => array(
                                                                              'username' => 'username',
                                                                              'password' => 'password',
																		
                                                                              )
                                                            )
                                            ),
                'authorize' => array('Controller'),
                'loginAction' => array('controller' => 'Admins', 'action' => 'login'),
                'loginRedirect' => array('controller' => 'admins','action' => 'dashboard'),
                'logoutRedirect' => array('controller' => 'Home', 'action' => 'index'),
          ),             
    );	
	
	// only allow the login controllers only
	public function beforeFilter() {
		$this->Auth->allow('login');
		//echo $this->action;exit;
		if (!$this->Session->check('Auth.User.admin_id') && $this->action != "login") {
			$this->Auth->logout();
			
            $this->redirect(array('controller'=>'admins','action' => 'login'));
        }
    }
	

	public function send_email($from,$to,$subject,$message,$template)
	{
		
		$this->Email = new CakeEmail();
		//$this->set('data','TJ');
		$this->Email->to ($to);
		$this->Email->subject($subject);
		$this->Email->emailFormat('html');
		//$this->Email->template('default');
		$this->Email->from($from);
		$this->Email->template($template);
		//$this->Email->return = 'gehlot.tejen111@gmail.com';
		//$this->Email->sender('gehlot.tejen111@gmail.com', 'Admin');
		//$this->Email->sendAs('both'); 
		ob_start();
		$this->Email->send($message);
		ob_end_clean();
	}
	
	public function isAuthorized($user) {
		// Here is where we should verify the role and give access based on role
		
		return true;
	}
	
	function send_push_android($id, $title, $message)
	{
		//return "here only";
		// API access key from Google API's Console
		//define( 'API_ACCESS_KEY', PUSH_API_MERCHANT );
		
		
		$registrationIds = $id;
		//pr($registrationIds);
		// prep the bundle
		$msg = array
		(
			'message' 	=> $message,
			'title'		=> $title,
			'subtitle'	=> '',
			'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		);
		
		$fields = array
		(
			'registration_ids' 	=> $registrationIds,
			'data'			=> $msg
		);
		 
		$headers = array
		(
			'Authorization: key=' . PUSH_API_ANDROID,
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		
		echo $result.'<br>';
		//exit;
	}
	
	function send_push_ios($id, $title, $message)
	{
        $ctx = stream_context_create();
		$passphrase	=	'112233';
		$message	=	'Static';
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-dev.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		
		$deviceToken	=	'bf66973f357601189f5f7209ddb133cf74862554e6b1368fd2a03d64335cbaaa';
		$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, 
			$ctx);
		
		if (!$fp)
		exit("Failed to connect amarnew: $err $errstr" . PHP_EOL);
		
		//echo 'Connected to APNS' . PHP_EOL;
		
		// Create the payload body
		$body['aps'] = array(
			'badge' => +1,
			'alert' => "emissary",
			'sound' => 'default'
		);
		
		$payload = json_encode($body);
		
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		
		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered'. PHP_EOL;
			
    }
	
	
}
