<script type="text/javascript">	

	$(document).ready(function(){
		//alert('Active');
		
	    var counter = $("#question_count").val();
		
		var var_count;
		
		$(this).change(function(event){
			
			var type_value = event.target.value;
			var type_id = event.target.id;
			
			var myString = type_id.split("_").pop();
			
			if(type_value=='C')
			{
				$('#apply_content_'+myString).show();
				$('#apply_contentRadio_'+myString).hide();
			}
			else if(type_value=='R')
			{
				$('#apply_contentRadio_'+myString).show();
				$('#apply_content_'+myString).hide();
			}
			else if(type_value=='T' || type_value=='I' || type_value=='A' || type_value=='')
			{
				$('#apply_content_'+myString).hide();
				$('#apply_contentRadio_'+myString).hide();
			}
				
		});
		
		$("#add_question").click(function(){
			
				var question_count = $('#question_count').val();
				
				$("#remove_question").show();
				
				$.ajax({
							type: "POST",		   
							url: "<?php echo ADMIN_ROOT.'surveys/survey_question_add'; ?>",
							data: "question_count="+question_count,
							success: function(data){
									$('#question_count').val((question_count*1+1));
									$('#test1').append(data);
							}
						});
				
		 });
		 
		 $("#remove_question").click(function(){
			
				var question_count = $('#question_count').val();
				
				var question_count1 = (question_count*1-1);
				
				$('#apply_question'+question_count1).remove();
				
				$('#question_count').val((question_count1));
				
				if(question_count1==2)
				{
					$('#remove_question').hide();
				}
				
		 });
		 
		 $("#add_question_test").click(function(){
			
				var question_count = $('#question_count').val();
				
				$('#remove_question_test').show();
				
				$.ajax({
							type: "POST",		   
							url: "<?php echo ADMIN_ROOT.'questions/test_question_add'; ?>",
							data: "question_count="+question_count,
							success: function(data){
									$('#question_count').val((question_count*1+1));
									$('#test1').append(data);
							}
						});
			
		 });
		 
		 $("#remove_question_test").click(function(){
			 
			 					
					var question_count = $('#question_count').val();
					
					var question_count1 = (question_count*1-1);
					
					$('#apply_question'+question_count1).remove();
					
					$('#question_count').val((question_count1));
					
					if(question_count1==2)
					{
						$('#remove_question_test').hide();
					}				
				
		 });  
		
		$('#refund_type').on('change', function() {
      			
				if(this.value=='full')
				{										
					$('#amount_valid').hide();					
				}
				else if(this.value=='Partial')
				{					
					$('#amount_valid').show();
				}
				else
				{
					$('#amount_valid').hide();
				}
				
    	});
				
  });
	

	$("#modal_checkwaitlist").on('click', '#checkforrefund', function(event){
		
		var payment_id = $('#payment_id').val();
		var transactionID = $('#transactionID').val();
		var refund_type = $('#refund_type').val();
		var amount = $('#amount').val();
		var error = false;
		//alert(refund_type);
		
		if(refund_type=='')
		{
			$('#error_type').html('Type selection is required.');
			error = true;
		}
		
		else if(refund_type=='Partial')
		{
			if(amount=='')
			{
				$('#error_amount').html('Amount is required for partial refund.');
				error = true;
			}
			else
			{
				$('#error_amount').html('');
			}
			$('#error_type').html('');
		}
		
		if (error) 
		{
            return false;
		}
		
		if(refund_type=='Partial')
		{
			$.ajax({
					type: "POST",		   
					url: "<?php echo ADMIN_ROOT.'payments/partial_refund'; ?>",
					data:'payment_id='+payment_id+"&transactionID="+transactionID+"&amount="+amount,	
					success: function(data){
							
							if(data==1)
							{
								$.ajax({
										type: "POST",		   
										url: "<?php echo ADMIN_ROOT.'payments/refundpayment'; ?>",
										data:'payment_id='+payment_id+"&transactionID="+transactionID+"&amount="+amount+"&type="+1,	
										success: function(data){
												
												if(data=='Success')
												{
													alert('Partial refund successfully.');
													window.location.reload();
												}
												else
												{
													alert(data);
													window.location.reload();
												}
																		
										}
								});
							}
							else
							{
								alert('Entered amount should be less than amount paid or no data.');
								//window.location.reload();
							}
											
					}
			});
		}
		
		else
		{
			$.ajax({
					type: "POST",		   
					url: "<?php echo ADMIN_ROOT.'payments/refund'; ?>",
					data:'payment_id='+payment_id+"&transactionID="+transactionID,	
					success: function(data){
							
							if(data==1)
							{
								$.ajax({
										type: "POST",		   
										url: "<?php echo ADMIN_ROOT.'payments/refundpayment'; ?>",
										data:'payment_id='+payment_id+"&transactionID="+transactionID+"&type="+2,
										success: function(data){
												
												if(data=='Success')
												{
													alert('Full Refund successfully.');
													window.location.reload();
												}
												else
												{
													alert(data);
													window.location.reload();
												}
																	
										}
								});
							}
							else
							{
								alert('No data...');
								window.location.reload();
							}						
					}
			});
		}
			
		event.preventDefault();
		return false;
				
	});
	

  jQuery(function ($) {
        $("#start_date").mask("99-99-9999", {placeholder: 'dd-mm-yyyy'});
        $("#end_date").mask("99-99-9999", {placeholder: 'dd-mm-yyyy'});
    });

</script>