<script type="text/javascript">	

	$(document).ready(function(){
		//alert('Active');
		
	    var counter = $("#option_count").val();
		
		$('#type_of_box').on('change', function() {
      			
				var option_count = $('#option_count').val();
				var var_count;
				if(this.value=='R' || this.value=='C')
				{
					for(var_count=1;var_count<option_count;var_count++)
					{
						$('#option_div'+var_count).show();
					}
										
					$('#option_button').show();					
				}
				else
				{
					for(var_count=1;var_count<option_count;var_count++)
					{
						$('#option_div'+var_count).hide();
						$("#error_option"+counter).hide();
					}
					
					$('#option_button').hide();
				}
				
    	});
		
	    $("#add-option").click(function(){
			
			if(counter>5){
		        alert("Only 5 textboxes allow.");
		        return false;
		    } 
			
			var Div = document.createElement('div');
						
            Div.setAttribute("class","input_field col s12 m12");
			
			Div.setAttribute("id","option_div"+counter);
			
			var label =  document.createElement("Label");
			
			label.setAttribute("class","col s12 m2");
			
			label.appendChild(document.createTextNode("Option "+counter));
			
			var text = document.createElement('input');
			text.type = "text";
			text.setAttribute("class","col s12 m4 validate");
			text.name = "option_"+counter;
			text.value = "";
			text.id = "option_"+counter;
			
			
			Div.appendChild(label);
			Div.appendChild(text);
			
			var iDiv = document.createElement('div');
			
			iDiv.setAttribute("class","error");
			
			iDiv.setAttribute("id","error_option"+counter);
			
			$('#apply_content').append(Div);
			$('#apply_content').append(iDiv);
			
		    counter++;
			$('#option_count').val(counter);
	    });

	    $("#remove-option").click(function () {
		    if(counter==2){
		        alert("No more Option to remove");
		        return false;
		    }
	       	
			counter--;	
	        $("#option_div"+counter).remove();
			$("#error_option"+counter).remove();
			$('#option_count').val(counter);
		});
		
		$('#refund_type').on('change', function() {
      			
				if(this.value=='full')
				{										
					$('#amount_valid').hide();					
				}
				else if(this.value=='Partial')
				{					
					$('#amount_valid').show();
				}
				else
				{
					$('#amount_valid').hide();
				}
				
    	});
				
  });
	

	$("#modal_checkwaitlist").on('click', '#checkforrefund', function(event){
		
		var payment_id = $('#payment_id').val();
		var transactionID = $('#transactionID').val();
		var refund_type = $('#refund_type').val();
		var amount = $('#amount').val();
		var error = false;
		//alert(refund_type);
		
		if(refund_type=='')
		{
			$('#error_type').html('Type selection is required.');
			error = true;
		}
		
		else if(refund_type=='Partial')
		{
			if(amount=='')
			{
				$('#error_amount').html('Amount is required for partial refund.');
				error = true;
			}
			else
			{
				$('#error_amount').html('');
			}
			$('#error_type').html('');
		}
		
		if (error) 
		{
            return false;
		}
		
		if(refund_type=='Partial')
		{
			$.ajax({
					type: "POST",		   
					url: "<?php echo ADMIN_ROOT.'payments/partial_refund'; ?>",
					data:'payment_id='+payment_id+"&transactionID="+transactionID+"&amount="+amount,	
					success: function(data){
							
							if(data==1)
							{
								$.ajax({
										type: "POST",		   
										url: "<?php echo ADMIN_ROOT.'payments/refundpayment'; ?>",
										data:'payment_id='+payment_id+"&transactionID="+transactionID+"&amount="+amount+"&type="+1,	
										success: function(data){
												
												
												if(data=='Success')
												{
													alert('Partial refund successfully.');
													window.location.reload();
												}
												else
												{
													alert(data);
													window.location.reload();
												}
																		
										}
								});
							}
							else
							{
								alert('Entered amount should be less than amount paid.');
								//window.location.reload();
							}
											
					}
			});
		}
		
		else
		{
			$.ajax({
					type: "POST",		   
					url: "<?php echo ADMIN_ROOT.'payments/refund'; ?>",
					data:'payment_id='+payment_id+"&transactionID="+transactionID,	
					success: function(data){
							
							if(data==1)
							{
								$.ajax({
										type: "POST",		   
										url: "<?php echo ADMIN_ROOT.'payments/refundpayment'; ?>",
										data:'payment_id='+payment_id+"&transactionID="+transactionID+"&type="+2,
										success: function(data){
												
												if(data=='Success')
												{
													alert('Full Refund successfully.');
													window.location.reload();
												}
												else
												{
													alert(data);
													window.location.reload();
												}
																	
										}
								});
							}
							else
							{
								alert('No data...');
								window.location.reload();
							}						
					}
			});
		}
			
		event.preventDefault();
		return false;
				
	});
	

  jQuery(function ($) {
        $("#start_date").mask("99-99-9999", {placeholder: 'dd-mm-yyyy'});
        $("#end_date").mask("99-99-9999", {placeholder: 'dd-mm-yyyy'});
    });

</script>