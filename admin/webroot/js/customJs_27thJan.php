<script type="text/javascript">	

	$(document).ready(function(){
		//alert('Active');
		
	    var counter = $("#option_count").val();
		
		$('#type_of_box').on('change', function() {
      			
				var option_count = $('#option_count').val();
				var var_count;
				if(this.value=='R' || this.value=='C')
				{
					for(var_count=1;var_count<option_count;var_count++)
					{
						$('#option_div'+var_count).show();
					}
										
					$('#option_button').show();					
				}
				else
				{
					for(var_count=1;var_count<option_count;var_count++)
					{
						$('#option_div'+var_count).hide();
						$("#error_option"+counter).hide();
					}
					
					$('#option_button').hide();
				}
				
    	});
		
	    $("#add-option").click(function(){
			
			if(counter>5){
		        alert("Only 5 textboxes allow.");
		        return false;
		    } 
			
			var Div = document.createElement('div');
						
            Div.setAttribute("class","input_field col s12 m12");
			
			Div.setAttribute("id","option_div"+counter);
			
			var label =  document.createElement("Label");
			
			label.setAttribute("class","col s12 m2");
			
			label.appendChild(document.createTextNode("Option "+counter));
			
			var text = document.createElement('input');
			text.type = "text";
			text.setAttribute("class","col s12 m4 validate");
			text.name = "option_"+counter;
			text.value = "";
			text.id = "option_"+counter;
			
			
			Div.appendChild(label);
			Div.appendChild(text);
			
			var iDiv = document.createElement('div');
			
			iDiv.setAttribute("class","error");
			
			iDiv.setAttribute("id","error_option"+counter);
			
			$('#apply_content').append(Div);
			$('#apply_content').append(iDiv);
			
		    counter++;
			$('#option_count').val(counter);
	    });

	    $("#remove-option").click(function () {
		    if(counter==2){
		        alert("No more Option to remove");
		        return false;
		    }
	       	
			counter--;	
	        $("#option_div"+counter).remove();
			$("#error_option"+counter).remove();
			$('#option_count').val(counter);
		});
				
  });


  jQuery(function ($) {
        $("#start_date").mask("99-99-9999", {placeholder: 'dd-mm-yyyy'});
        $("#end_date").mask("99-99-9999", {placeholder: 'dd-mm-yyyy'});
    });

</script>