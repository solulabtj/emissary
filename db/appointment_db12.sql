-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2015 at 06:50 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cakephptest`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` varchar(150) NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `description`, `status`, `created`, `modified`) VALUES
('4d5ae46c-efef-11e4-b85e-3863bbc87416', '554211de-09c8-4d6f-a538-06c061acb3d3', 'Household Essentials', 'details for the home products', 1, '2015-05-01 04:18:07', '2015-05-01 09:37:50'),
('5541cb71-2628-4cdc-bdfa-031061acb3d3', '0', 'Spa', 'This category is for the Spa', 1, '2015-04-30 08:28:01', '2015-04-30 13:44:18'),
('5541cb8e-92dc-47dc-b733-031061acb3d3', '', 'Masag', 'This category is for the Masag', 1, '2015-04-30 08:28:30', '2015-04-30 08:28:30'),
('5541cb97-b858-45c6-9455-031061acb3d3', '', 'Dental', 'This category is for the Dental', 1, '2015-04-30 08:28:39', '2015-04-30 08:28:39'),
('5541cd4c-28f4-46e1-b933-031061acb3d3', '5541cb71-2628-4cdc-bdfa-031061acb3d3', 'Spa 1', 'Spa1 is one of the good services', 1, '2015-04-30 08:35:56', '2015-04-30 08:35:56'),
('5541cda2-75ec-463b-b83b-031061acb3d3', '5541cb8e-92dc-47dc-b733-031061acb3d3', 'Half Body Masag', 'This is for the only half body masag', 1, '2015-04-30 08:37:22', '2015-04-30 08:37:22'),
('5541d1e4-e834-427a-9076-031061acb3d3', '', 'Electronics', 'Details of Electronics products will be shown here', 1, '2015-04-30 08:55:32', '2015-04-30 08:55:32'),
('5541d2a1-eea8-4140-a2aa-031061acb3d3', '', 'Men', 'The products for the men is to be shown here', 1, '2015-04-30 08:58:41', '2015-04-30 08:58:41'),
('5541d596-3474-4adf-b89e-031061acb3d3', '', 'Women', 'This category is for the women products', 1, '2015-04-30 09:11:18', '2015-04-30 09:11:18'),
('5541d609-37a8-4a95-93fb-031061acb3d3', '5541d1e4-e834-427a-9076-031061acb3d3', 'Mobile', 'This is the cat under the Electronics', 1, '2015-04-30 09:13:13', '2015-04-30 09:13:13'),
('5541d610-71b4-4e05-992e-031061acb3d3', '5541d596-3474-4adf-b89e-031061acb3d3', 'Purse', 'These is the purse option under women', 1, '2015-04-30 09:13:20', '2015-04-30 10:57:44'),
('5541d61d-b0d4-4e50-9958-031061acb3d3', '5541d2a1-eea8-4140-a2aa-031061acb3d3', 'Sun Glass', 'This is the cat under the men', 1, '2015-04-30 09:13:33', '2015-04-30 09:13:33'),
('5541ea75-5a14-46f5-8422-06c061acb3d3', '5541d1e4-e834-427a-9076-031061acb3d3', 'Mobile Accessories', 'These is the mobile acces under mobile', 1, '2015-04-30 10:40:21', '2015-04-30 10:40:21'),
('554211de-09c8-4d6f-a538-06c061acb3d3', '', 'House Hold', 'The house hold data is be shown here', 1, '2015-04-30 13:28:30', '2015-04-30 13:28:30');

-- --------------------------------------------------------

--
-- Table structure for table `deals`
--

CREATE TABLE IF NOT EXISTS `deals` (
  `id` varchar(36) NOT NULL,
  `category_id` varchar(36) NOT NULL,
  `merchant_id` varchar(36) NOT NULL,
  `name` mediumtext,
  `description` longtext,
  `terms` longtext,
  `mrp` int(11) DEFAULT NULL,
  `offerprice` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `is_exclusive` tinyint(1) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `lattitude` mediumtext,
  `longitude` mediumtext,
  `status` int(11) DEFAULT NULL,
  `is_blocked` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deals`
--

INSERT INTO `deals` (`id`, `category_id`, `merchant_id`, `name`, `description`, `terms`, `mrp`, `offerprice`, `discount`, `is_exclusive`, `city_id`, `country_id`, `lattitude`, `longitude`, `status`, `is_blocked`, `created`, `modified`) VALUES
('554341d8-452c-4af5-8b81-02dc61acb3d3', '5541d61d-b0d4-4e50-9958-031061acb3d3', '', 'Optima Fashion Track Analog Watches', 'This is the details for the watches for the men category', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2015-05-01 11:05:28', '2015-05-01 11:05:28'),
('55434441-a348-4bf2-a46b-02dc61acb3d3', '5541d61d-b0d4-4e50-9958-031061acb3d3', '', 'Edited Black Time watch', 'This is the timex watch for the men with the best price to use plz have a look on to it.', NULL, 980, 630, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2015-05-01 11:15:45', '2015-05-01 13:00:46'),
('554344be-79fc-4150-9f66-02dc61acb3d3', '5541d609-37a8-4a95-93fb-031061acb3d3', '', 'Grey Samsung Galaxy Grand Prime', 'These is one of the good product we need to check for it', NULL, 15000, 12500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2015-05-01 11:17:50', '2015-05-01 11:17:50'),
('5543454b-efe4-486f-acfc-02dc61acb3d3', '5541d609-37a8-4a95-93fb-031061acb3d3', '', 'Samsung Galaxy Grand in four Colors', 'Details for the samsung will be shown here', NULL, 20000, 17000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2015-05-01 11:20:11', '2015-05-01 11:20:11'),
('554345d8-89c8-4826-ac05-02dc61acb3d3', '5541ea75-5a14-46f5-8422-06c061acb3d3', '', 'Philips Portable Bluetooth Speakers', 'The products details will be shown here.', NULL, 5000, 3500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2015-05-01 11:22:32', '2015-05-01 11:22:32'),
('5543463f-02ec-4106-b46d-02dc61acb3d3', '5541ea75-5a14-46f5-8422-06c061acb3d3', '', 'MFI certified Boat Metallic Lightening Cable', 'The products details for the Boat Metalic Lighting cable is to eb shown here', NULL, 1500, 950, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2015-05-01 11:24:15', '2015-05-01 11:24:15'),
('554357e0-2be4-4f9f-8bd5-02dc61acb3d3', '4d5ae46c-efef-11e4-b85e-3863bbc87416', '', 'Kampfield Spike Guard Extension Board with USB', NULL, NULL, 800, 560, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2015-05-01 12:39:28', '2015-05-01 12:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `merchants`
--

CREATE TABLE IF NOT EXISTS `merchants` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `facebookId` mediumtext,
  `googleId` mediumtext,
  `mobile` int(11) DEFAULT NULL,
  `shopname` mediumtext,
  `shopaddress` longtext,
  `shopphone` int(11) DEFAULT NULL,
  `shoptime` datetime DEFAULT NULL,
  `customersperday` int(11) DEFAULT NULL,
  `bankname` mediumtext,
  `accountno` mediumtext,
  `accountholder` mediumtext,
  `ifsc` tinytext,
  `address` longtext,
  `zip` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `is_blocked` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merchants`
--

INSERT INTO `merchants` (`id`, `parent_id`, `firstname`, `lastname`, `email`, `password`, `confirmed`, `status`, `facebookId`, `googleId`, `mobile`, `shopname`, `shopaddress`, `shopphone`, `shoptime`, `customersperday`, `bankname`, `accountno`, `accountholder`, `ifsc`, `address`, `zip`, `city_id`, `country_id`, `lastLogin`, `is_blocked`, `created`, `modified`) VALUES
('5546f0c6-d15c-45e5-a605-026861acb3d3', NULL, NULL, NULL, NULL, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-05-04 06:08:38', '2015-05-04 06:08:38'),
('5546f0e1-6450-4ea1-990d-026861acb3d3', NULL, NULL, NULL, NULL, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-05-04 06:09:05', '2015-05-04 06:09:05'),
('5546f0f3-8698-492f-9d9c-026861acb3d3', NULL, 'mitesh', 'gupta', NULL, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-05-04 06:09:23', '2015-05-04 06:09:23'),
('5546f1b9-5c84-4242-b449-026861acb3d3', NULL, 'kalpesh', 'gupta', 'kalpesh@yahoo.com', '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-05-04 06:12:41', '2015-05-04 06:12:41'),
('5546f20d-4630-4bbe-85cd-026861acb3d3', NULL, 'kalpesh', 'gupta', 'kalpesh123@yahoo.com', '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-05-04 06:14:05', '2015-05-04 06:14:05'),
('5546f47f-6950-4fa9-a7a5-026861acb3d3', NULL, 'mathew', 'donald', 'mathew@yahoo.com', '44122a80bb6f4e7f285d1233900e1134c46b5e01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-05-04 06:24:31', '2015-05-04 06:24:31');

-- --------------------------------------------------------

--
-- Table structure for table `merchants_old`
--

CREATE TABLE IF NOT EXISTS `merchants_old` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `userid` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merchants_old`
--

INSERT INTO `merchants_old` (`id`, `name`, `userid`, `password`, `created`, `modified`) VALUES
(4, 'maddy', 'maddy12548', '963258', '2015-04-29 09:15:02', '2015-04-29 09:21:14'),
(6, 'mukeshpawwr', 'mukesh8569', '254136', '2015-04-29 09:20:05', '2015-04-29 09:20:05'),
(10, 'samir', 'samir123', '987654', '2015-04-29 09:28:20', '2015-04-29 09:28:20'),
(19, 'Malik', 'malik123', '123456', '2015-04-29 12:02:42', '2015-04-29 12:02:42'),
(22, 'Subhash', 'subh123', '44122a80bb6f4e7f285d1233900e1134c46b5e01', '2015-04-29 12:26:13', '2015-04-29 12:26:13'),
(23, 'Mahesh', 'mahes985', '44122a80bb6f4e7f285d1233900e1134c46b5e01', '2015-04-29 12:26:42', '2015-04-29 12:26:42'),
(24, 'Darshan', 'darsh123', '44122a80bb6f4e7f285d1233900e1134c46b5e01', '2015-04-29 12:44:49', '2015-04-29 12:44:49'),
(25, 'manek', 'manek123', 'ad2e2da8f9d050056f8cbb4c2932be9808ea4710', '2015-04-29 13:30:43', '2015-04-29 13:30:43'),
(26, 'manishee', 'mansh123d', '123456', '2015-04-29 13:37:13', '2015-04-29 13:37:13'),
(27, 'nitin', 'nitin', '44122a80bb6f4e7f285d1233900e1134c46b5e01', '2015-04-29 13:37:40', '2015-04-29 13:37:40'),
(28, 'sdfks', 'sdf4445', '8799a538abc8f110f8146213322b966ac7f56172', '2015-04-30 07:01:33', '2015-04-30 07:01:33'),
(29, 'wewerew', 'sdf444558', '8799a538abc8f110f8146213322b966ac7f56172', '2015-04-30 07:18:24', '2015-04-30 07:18:24'),
(30, 'kalpesh', 'kalpesh', '123456', '2015-05-04 06:06:46', '2015-05-04 06:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE IF NOT EXISTS `phones` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`id`, `name`, `manufacturer`, `description`, `created`, `modified`) VALUES
(1, 'iPhone 5s', 'Apple', 'The iPhone 5s may look a lot like its predecessor. But with a faster new processor, a fingerprint sensor, and an improved camera flash, it is a serious upgrade.', '2015-04-28 12:18:14', NULL),
(2, 'Nexus 5', 'Google', 'Running the latest version of Android, the Nexus 5 really shows off the best aspects of Googles mobile OS. There are few reasons not to pick the Nexus 5 as your next Android phone.', '2015-04-28 12:18:14', NULL),
(3, 'One', 'HTC', 'With its stellar design, great camera, and hardy processor, the HTC One is the phone to beat.', '2015-04-28 12:18:14', NULL),
(4, 'Galaxy S4', 'Samsung', 'The Samsung Galaxy S4 is a stellar Android phone held back by boring design and half-baked features.', '2015-04-28 12:18:14', NULL),
(5, 'Gionie2', 'Gionie1', 'This phone company is from china & is a good company & is better company', '2015-04-28 08:49:54', '2015-04-28 08:50:21'),
(13, 'edited data', 'new manu data', 'new details for ading', '2015-04-29 08:19:34', '2015-04-29 08:22:13'),
(14, 'myphonename', 'nokia company manu', 'details for the myphone', '2015-04-29 08:19:51', '2015-04-29 08:19:51'),
(15, 'nokia', 'nokia a compnay', 'details for the nokia', '2015-04-29 08:39:10', '2015-04-29 08:39:10');

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `manufacturer`, `description`, `created`, `modified`) VALUES
(1, 'iPhone 5s', 'Apple', 'The iPhone 5s may look a lot like its predecessor. But with a faster new processor, a fingerprint sensor, and an improved camera flash, it is a serious upgrade.', '2015-04-28 12:18:14', '2015-04-28 12:33:05'),
(2, 'Nexus 5', 'Google', 'Running the latest version of Android, the Nexus 5 really shows off the best aspects of Googles mobile OS. There are few reasons not to pick the Nexus 5 as your next Android phone.', '2015-04-28 12:18:14', NULL),
(3, 'One', 'HTC', 'With its stellar design, great camera, and hardy processor, the HTC One is the phone to beat.', '2015-04-28 12:18:14', '2015-04-28 12:35:29'),
(4, 'Galaxy S4', 'Samsung', 'The Samsung Galaxy S4 is a stellar Android phone held back by boring design and half-baked features.', '2015-04-28 12:18:14', NULL),
(5, 'Xolo', 'Xolo is the indian company', 'This phone is one of the good phone', '2015-04-28 08:49:54', '2015-04-28 12:57:11'),
(6, 'phonename', 'manusdflk', 'sdfsdf', '2015-04-28 14:14:57', '2015-04-28 14:14:57'),
(7, 'name', 'manusdflk', 'sdfsdf', '2015-04-28 14:30:16', '2015-04-28 14:30:16'),
(8, 'mukesh', 'pawar', 'sdfsdf', '2015-04-29 06:21:19', '2015-04-29 06:21:19'),
(9, 'sdsd', 'sdfsdf', 'sdfs', '2015-04-29 07:39:49', '2015-04-29 07:39:49'),
(10, 'sdsdddd', 'sdfsdf', 'sdfs', '2015-04-29 07:40:36', '2015-04-29 07:40:36'),
(11, 'dsfsdf', 'safdsdfasd', 'sadfsadfsdfdsdf', '2015-04-29 07:46:02', '2015-04-29 07:46:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deals`
--
ALTER TABLE `deals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchants`
--
ALTER TABLE `merchants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchants_old`
--
ALTER TABLE `merchants_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `merchants_old`
--
ALTER TABLE `merchants_old`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `phones`
--
ALTER TABLE `phones`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
