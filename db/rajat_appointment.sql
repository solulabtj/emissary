-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2015 at 06:51 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rajat_appointment`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `name` mediumtext,
  `email` mediumtext,
  `password` mediumtext,
  `status` int(11) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `ipAddress` tinytext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `margin_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE IF NOT EXISTS `appointment` (
  `id` int(11) NOT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `mrp` mediumtext,
  `offerPrice` mediumtext,
  `discount` mediumtext,
  `description` longtext,
  `status` int(11) DEFAULT NULL,
  `no_of_appointment` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `appointment_id` int(11) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `transactionId` mediumtext,
  `amount` mediumtext,
  `bookingTime` datetime DEFAULT NULL,
  `user_paid_to_admin` mediumtext,
  `admin_paid_to_merchant` mediumtext,
  `user_paid_to_merchant` mediumtext,
  `user_will_pay` mediumtext,
  `promoCode_id` tinytext,
  `firstname` mediumtext,
  `lastname` mediumtext,
  `email` mediumtext,
  `mobile` mediumtext,
  `booking_code` mediumtext,
  `is_merchant_cancelled` tinyint(1) DEFAULT NULL,
  `is_user_cancelled` tinyint(1) DEFAULT NULL,
  `no_show` tinyint(1) DEFAULT NULL,
  `no_avail` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `category_id` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created` varchar(45) DEFAULT NULL,
  `modified` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` mediumtext,
  `lattitude` mediumtext,
  `longitude` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL,
  `name` mediumtext,
  `lattitude` mediumtext,
  `longitude` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deal`
--

CREATE TABLE IF NOT EXISTS `deal` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `name` mediumtext,
  `description` longtext,
  `terms` longtext,
  `mrp` int(11) DEFAULT NULL,
  `offerPrice` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `is_exclusive` tinyint(1) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `lattitude` mediumtext,
  `longitude` mediumtext,
  `status` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_blocked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealimage`
--

CREATE TABLE IF NOT EXISTS `dealimage` (
  `id` int(11) NOT NULL,
  `deal_id` mediumtext,
  `name` mediumtext,
  `filePath` longtext,
  `is_default` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

CREATE TABLE IF NOT EXISTS `issues` (
  `id` int(11) NOT NULL,
  `description` longtext,
  `user_id` int(11) DEFAULT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `magrin`
--

CREATE TABLE IF NOT EXISTS `magrin` (
  `id` int(11) NOT NULL,
  `percentage` mediumtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE IF NOT EXISTS `merchant` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `firstName` tinytext,
  `lastName` tinytext,
  `email` mediumtext,
  `password` mediumtext,
  `confirmed` tinyint(1) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `facebookId` mediumtext,
  `googleId` mediumtext,
  `mobile` int(11) DEFAULT NULL,
  `shopName` mediumtext,
  `shopAddress` longtext,
  `shopPhone` int(11) DEFAULT NULL,
  `shopTime` datetime DEFAULT NULL,
  `customersPerDay` int(11) DEFAULT NULL,
  `bankName` mediumtext,
  `accountNo` mediumtext,
  `accountHolder` mediumtext,
  `ifsc` tinytext,
  `address` longtext,
  `zip` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_blocked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promocode`
--

CREATE TABLE IF NOT EXISTS `promocode` (
  `id` int(11) NOT NULL,
  `name` tinytext,
  `description` longtext,
  `code` tinytext,
  `value` mediumtext,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `count_used` mediumtext,
  `no_of_coupon` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recent`
--

CREATE TABLE IF NOT EXISTS `recent` (
  `id` int(11) NOT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ipaddress` tinytext,
  `datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `startRating` int(11) DEFAULT NULL,
  `description` longtext,
  `title` mediumtext,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `firstName` mediumtext,
  `lastName` mediumtext,
  `email` mediumtext,
  `mobile` int(11) DEFAULT NULL,
  `password` mediumtext,
  `confirmed` tinyint(1) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `facebookId` mediumtext,
  `googleId` mediumtext,
  `notification` tinyint(1) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `ipAddress` tinytext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_blocked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deal`
--
ALTER TABLE `deal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dealimage`
--
ALTER TABLE `dealimage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issues`
--
ALTER TABLE `issues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `magrin`
--
ALTER TABLE `magrin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchant`
--
ALTER TABLE `merchant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promocode`
--
ALTER TABLE `promocode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recent`
--
ALTER TABLE `recent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
