<?php
class Job extends AppModel {
	var $name = 'Job'; 
	var $primaryKey = 'job_id';
	
	public $belongsTo = array(
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
    );
	public $hasMany = array(
		'AgentJob'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),	
		'AgentPayment'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),
		'JobLocation'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),	
			
    );
	
}