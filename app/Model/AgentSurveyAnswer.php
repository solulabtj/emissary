<?php
class AgentSurveyAnswer extends AppModel {
	var $name = 'AgentSurveyAnswer'; 
	var $primaryKey = 'agent_survey_answer_id';
	
	public $belongsTo = array(
		'AgentUserInfo'=>array('className'=>'AgentUserInfo',
		'foreignKey'=>'agent_id'
		),		
		'AgentJob'=>array('className'=>'AgentJob',
		'foreignKey'=>'agent_job_id'
		),
		
    );
	public $hasMany = array(
		
    );
	
}