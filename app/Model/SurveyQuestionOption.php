<?php
class SurveyQuestionOption extends AppModel {
	var $name = 'SurveyQuestionOption'; 
	var $primaryKey = 'survey_option_id';
	
	
	public $belongsTo = array(
		'SurveyQuestion'=>array('className'=>'SurveyQuestion',
		'foreignKey'=>'survey_question_id'
		),
    );
		

}