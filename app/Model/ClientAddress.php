<?php
class ClientAddress extends AppModel {
	var $name = 'ClientAddress'; 
	var $primaryKey = 'client_address_id';
	var $useTable = 'client_address';
	public $hasMany = array(
		'JobLocation'=> array(
			'foreignKey' => 'client_address_id',
			'dependent' => false
		),	
    );
	
		

}