<?php
class Coupon extends AppModel {
	var $name = 'Coupon'; 
	var $primaryKey = 'coupon_id';
	public $hasMany = array(
		'AgentCoupon'=> array(
			'foreignKey' => 'coupon_id',
			'dependent' => false
		),	
			
    );
	public $belongsTo = array(
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
    );
		

}