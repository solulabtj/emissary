<?php
class SurveyCategory extends AppModel {
	var $name = 'SurveyCategory'; 
	var $primaryKey = 'survey_category_id';
	public $hasMany = array(
		'SurveyQuestion'=> array(
			'foreignKey' => 'survey_category_id',
			'dependent' => false
		),	
    );
	
		

}