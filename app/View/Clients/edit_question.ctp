<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">    
	
	function validate_settings()
	{
			var error = false;
            
			var survey_question_description = $('#survey_question_description').val();
			
			var survey_question_marks = $('#survey_question_marks').val();
			
			var survey_question_type = $('#survey_question_type').val();
			
			var survey_category_id = $('#survey_category_id').val();
			
			var survey_id = $('#survey_id').val();
			
			if(survey_id=='')
			{
				$('#error_survey').html('Survey is required.');
				error = true;
			}
			else
			{
				$('#error_survey').html('');
			}
			
			if(survey_category_id=='')
			{
				$('#error_category').html('Survey Category is required.');
				error = true;
			}
			else
			{
				$('#error_category').html('');
			}
			
			if(survey_question_type=='')
			{
				$('#error_type').html('Type selection is required.');
				error = true;
			}			
			
			else if(survey_question_type=='C')
			{
					for(option_limit=1;option_limit<=5;option_limit++)
					{
						var ID = $('#option_'+option_limit).val();
						if(ID=='')
						{
							$('#error_option'+option_limit).html('Option Entry is required.');
							error = true;
						}			
						else
						{
							$('#error_option'+option_limit).html('');
						}
					}	
					$('#error_type').html('');			
			}
			
			else
			{
				$('#error_type').html('');				
			}
			
			if(survey_question_description=='')
			{
				$('#error_questions').html('Question Description is required.');
				error = true;
			}			
			else
			{
				$('#error_questions').html('');						
			}
			
			if(survey_question_marks=='' || survey_question_marks=='0')
			{
				$('#error_marks').html('Question Marks is required OR should be not Zero.');
				error = true;
			}			
			else
			{
				$('#error_marks').html('');				
			}
			
			
            if (error) {
                return false;
            }
			
     }
	 
	 function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
	}
	
     
</script>

<div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Edit Survey Question</h1>
                <div class="center" align="center"><?php echo $this->Session->flash(); ?></div>
            	<form method="post" enctype="multipart/form-data" action="<?php echo WEBSITE_ROOT.'clients/edit_question'; ?>">
                <input type="hidden" name="survey_question_id" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_id']; ?>" />
                
                <input type="hidden" name="survey_question_type" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_type']; ?>" />
                
                	<div class="col-md-6">                		
                		
                        <div class="input-box">
	                		<label class="input-name"><span>Survey Category</span>
                            
                            <select name="survey_category_id" id="survey_category_id"  >
                  				<option value="" >Select Type
                    
								<?php foreach($survey_category as $category) { ?>
                                
                                    <option value="<?php echo $category['SurveyCategories']['survey_category_id']; ?>"<?php if($category['SurveyCategories']['survey_category_id']==$singlerecord[0]['SurveyQuestion']['survey_category_id']) { echo "selected='selected'"; } ?> ><?php echo $category['SurveyCategories']['survey_category_title']; ?>
                                
                                <?php } ?>
                    
                  			</select>
                            
                            </label>
                            
                		</div>
                        <div id="error_category" class="error"></div>
                        
                		<div class="input-box">
	                		<label class="input-name"><span>Type</span>
                            
                            <select name="survey_question_type" id="survey_question_type" disabled >
                                <option value="">Select Type                    
                                <option value="C"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='C') { echo "selected='selected'";  } ?> >Checkbox
                                <option value="T"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='T') { echo "selected='selected'";  } ?> >Textbox
                                <option value="I"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='I') { echo "selected='selected'";  } ?> >Image
                                <option value="A"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='A') { echo "selected='selected'";  } ?> >Audio
                                <option value="R"<?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='R') { echo "selected='selected'";  } ?> >Radio
                                
                             </select>
                            
                            </label>
                		</div>
                		<div id="error_type" class="error"></div>
                        
                        <div class="input-box">
	                		<label class="input-name"><span>Select Survey</span>
                            
                            <select name="survey_id" id="survey_id" >
                                <option value="">Select Survey                    
                                
                                <?php foreach($survey_lists as $survey_list) { ?>
                                
                                    <option value="<?php echo $survey_list['Survey']['survey_id']; ?>"<?php if($survey_list['Survey']['survey_id']==$singlerecord[0]['SurveyQuestion']['survey_id']) { echo "selected='selected'"; } ?> ><?php echo $survey_list['Survey']['survey_name']; ?>
                                
                                <?php } ?>
                                
                             </select>
                            
                            </label>
                		</div>
                        <div id="error_survey" class="error"></div>
                        
                        <div class="input-box">
	                		<label class="input-name"><span>Question Description</span></label>
                            <input type="text" name="survey_question_description" id="survey_question_description" value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_description']; ?>" />
                		</div>
                        <div id="error_questions" class="error"></div>
                        
                        <div class="input-box">
	                		<label class="input-name"><span>Question Marks</span></label>
                            <input type="text" name="survey_question_marks" id="survey_question_marks" onkeypress="return onlyNumbers(event);"  value="<?php echo $singlerecord[0]['SurveyQuestion']['survey_question_marks']; ?>" />
                		</div>
                        <div id="error_marks" class="error"></div>                   
                        
                        
                 <?php if($singlerecord[0]['SurveyQuestion']['survey_question_type']=='C') { ?>		
					<div id="apply_content">
					<?php 
					$j=0;
					for($i=1;$i<=$count;$i++) 
					{		
						?>
                 		 <div class="input-box">
                             <label class="input-name">Option <?php echo $i; ?></label>
                             <input type="text" name="option_<?php echo $i; ?>" id="option_<?php echo $i; ?>" value="<?php echo $singlerecord[0]['SurveyQuestionOption'][$j]['answer_text']; ?>" >
                         </div>
                         <div id="error_option<?php echo $i; ?>" class="error"></div>                  
                 <?php $j++; } ?> 
                 
                 	</div>
                 
                 <?php } ?>
                                      		
                	</div>
                	
                		
                		<div class="clearfix"></div>

                		<button type="submit" onclick="return validate_settings();" class="btn btn-lg btn-custom btn-success">Update Question</button>

                	</div>
                </div>
                
                </form>
                
            </div>

</div>


	<?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			
	?>
	

	<!-- jQuery -->
   <?php /*?> <script src="<?php echo WEBSITE_ROOT; ?>js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/sb-admin-2.js"></script><?php */?>
</body>
</html>