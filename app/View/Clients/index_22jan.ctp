<div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Edit Profile</h1>
            
                <div class="edit_profsection">
                <div class="pic-banner shadow">
            	<div class="prof-pic">
            			<img src="<?php echo WEBSITE_ROOT; ?>images/prof-pic.jpg">
            			<div class="fileUpload">
						    <span>Upload Photo</span>
						    <input type="file" class="upload" />
						</div>
            		</div>
                </div>
                	<div class="col-md-6">                		
                	<div class="input-box">
	                		<label class="input-name"><span>Company Name</span><input type="text" class="input-field"></label>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Overview</span><textarea class="input-field" rows="3"></textarea></label>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Phone Number</span><input type="text" class="input-field"></label>
                		</div>                		
                	</div>
                	<div class="col-md-6">
                		<div class="input-box">
	                		<label class="input-name"><span>Agency Name</span><input type="text" class="input-field"></label>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Address</span><textarea class="input-field" rows="3"></textarea></label>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Paypal ID</span><input type="text" class="input-field"></label>
                		</div>
                	</div>
                	<div class="col-md-12">
                		<h2 class="reg-header"><i class="fa fa-picture-o"></i>Pictures</h2>
                		<div class="editprof-images">
                			<ul>
                				<li>
                					<div class="gal-img">
                					<img src="<?php echo WEBSITE_ROOT; ?>images/image-1.jpg">
                					<span class="remove">
	                					<a href=""><i class="fa fa-times"></i></a>
                					</span>
                					</div>
                				</li>
                				<li>
                					<div class="gal-img">
                					<img src="<?php echo WEBSITE_ROOT; ?>images/image-2.jpg">
                					<span class="remove">
	                					<a href=""><i class="fa fa-times"></i></a>
                					</span>
                					</div>
                				</li>
                				<li>
                					<div class="gal-img">
                					<img src="<?php echo WEBSITE_ROOT; ?>images/image-3.jpg">
                					<span class="remove">
	                					<a href=""><i class="fa fa-times"></i></a>
                					</span>
                					</div>
                				</li>
                				<li>
                					<div class="gal-img">
                					<img src="<?php echo WEBSITE_ROOT; ?>images/image-4.jpg">
                					<span class="remove">
	                					<a href=""><i class="fa fa-times"></i></a>
                					</span>
                					</div>
                				</li>
                				<li class="addmore">
                					<div class="gal-img">
                						<a href="">
                							<i class="fa fa-plus"></i>
                							<span>Add More</span>
            							</a>
                					</div>
                				</li>
                			</ul>
                		</div>

                		<div class="clearfix"></div>

                		<h2 class="reg-header"><i class="fa fa-clock-o"></i>Working Hours</h2>
                		<div class="work-detil">

                        <div class="col-md-3">
                            <div class="input-box">
                            <label class="input-name"><span>From</span><input type="number" class="input-field" ></label>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-box">
                            <label class="input-name"><span>To</span><input type="number" class="input-field" ></label>
                        </div>
                        </div>
                		<div class="clearfix"></div>	
                		<div class="col-md-12">
                            <div class="wrk-dys">
                				<ul class="list-unstyled list-inline">
                					<li><label><input type="checkbox">Mon</label></li>
                					<li><label><input type="checkbox">Tue</label></li>
                					<li><label><input type="checkbox">Wed</label></li>
                					<li><label><input type="checkbox">Thu</label></li>
                					<li><label><input type="checkbox">Fri</label></li>
                					<li><label><input type="checkbox">Sat</label></li>
                					<li><label><input type="checkbox">Sun</label></li>
                				</ul>
                			</div>
                        </div>
                		</div>
                		<div class="clearfix"></div>

                		<button type="button" class="btn btn-lg btn-custom btn-success">Update</button>

                	</div>
                </div>
            </div>

        </div>


	<?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			
	?>
	

	<!-- jQuery -->
   <?php /*?> <script src="<?php echo WEBSITE_ROOT; ?>js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/sb-admin-2.js"></script><?php */?>
</body>
</html>