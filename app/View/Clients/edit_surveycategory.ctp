<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
		
	function validate_settings()
	{
			var error = false;
            
			var survey_category_title = $('#survey_category_title').val();
			
			if(survey_category_title=='')
			{
				$('#error_title').html('Survey Category Title is required.');
				error = true;
			}			
			else
			{
				$('#error_title').html('');
			}
			
			
            if (error) {
                return false;
            }
	}
		
		function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
		}		
		
		
		function lettersOnly(evt) {
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		  ((evt.which) ? evt.which : 0));
		if (charCode == 32)
			return true;
		if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		  (charCode < 97 || charCode > 122)) {
			return false;
		}
		else
			return true;
	}

</script>

<style>
    .error{
        color: red;
    }
</style>

<div id="page-wrapper">
    <div class="row">
	    <h1 class="page-header">Edit Survey Category</h1>
	    <div class="center" align="center"><?php echo $this->Session->flash(); ?></div>
		<form method="post" enctype="multipart/form-data" action="<?php echo WEBSITE_ROOT.'clients/edit_surveycategory'; ?>">
	    	<input type="hidden" name="survey_category_id" id="survey_category_id" value="<?php echo $singlerecord[0]['SurveyCategories']['survey_category_id']; ?>" />
	       	<div id="apply_formdata" class="col-md-6">  
	            
	    		    
	                <div class="input-box">
	        			<label class="input-name"><span>Survey Category Title</span></label>
	                	<input style="width: 60%;" type="text" name="survey_category_title" id="survey_category_title" onkeypress="return lettersOnly(event)"  value="<?php echo $singlerecord[0]['SurveyCategories']['survey_category_title']; ?>" />
	    			</div>
	            	<div id="error_title" class="error"></div>
                    
	            	<div class="input-box">
	        			<label class="input-name"><span>Survey Category Image</span></label>
	                	<input type="file" name="survey_category_image" id="survey_category_image"  />
	    			</div>
	            	<div id="error_image" class="error"></div>
                    
                    <?php if(!empty($singlerecord[0]['SurveyCategories']['survey_category_image'])) { ?>
                    <img src="<?php echo WEBSITE_ROOT.'media/img/'.$singlerecord[0]['SurveyCategories']['survey_category_image']; ?>" height="200px" width="200px"/>
                  	
                    <?php } ?>
                    
                    <input type="hidden" name="survey_category_image_old" id="survey_category_image_old" value="<?php echo $singlerecord[0]['SurveyCategories']['survey_category_image']; ?>" />
                    
	   		</div>
        	<div class="clearfix"></div>
        	<button type="submit" onclick="return validate_settings();" class="btn btn-lg btn-custom btn-success">Update Category</button>
        	
		</form>
   	</div>
</div>



	<?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			
	?>
	
    <script type="text/javascript">
	
		$(document).ready(function(){
			
			});

	</script>

	
</body>
</html>