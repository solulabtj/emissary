
        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Add Coupons</h1>
                
                <div class="add-surveys-box shadow">
                <div class="row">
                    <div class="col-md-6">
                    <form method="post" action="<?php echo WEBSITE_ROOT."clients/add_coupons";?>" enctype="multipart/form-data" >
                    <input type="text" class="form-control" name="coupon_name" id="coupon_name" placeholder="coupon name">
                    <div id="error_name"></div>
                    <br>
                    <input type="text" class="form-control" name="coupon_des" id="coupon_des" placeholder="coupon description">
                    <div id="error_des"></div>
                    <br>
                    <!--<input type="text" class="form-control" name="coupon_image" placeholder="coupon image">-->
                    <input type="file" name="coupon_image" id="coupon_image" size="40">
                    <div id="error_img"></div><br>
                    <!--<input type="text" class="form-control" name="" placeholder="coupon qr code image">-->
                    <input type="file" name="qr_image" id="qr_image"  size="40">
                    <div id="error_qrimg"></div><br>
                    <input type="text" class="form-control" id="quantity"  name="quantity" placeholder="coupon quantity">
                    <div id="error_qyt"></div><br>
                    <input type="text" class="form-control" id="exp_date"  name="exp_date" placeholder="coupon expiry date">
                    <div id="error_expdate"></div><br>
                    <button  class="btn-custom1 create-job" type="submit" onclick="return validate();">Submit</button>
                    </form>
                    </div>
                </div>
                </div>
                
            </div>

        </div>

<script type="text/javascript">
    function validate()
    {
          var error = false;

          var coupon_name = $.trim($('#coupon_name').val());
          var coupon_des = $.trim($('#coupon_des').val());
          var coupon_image = $.trim($('#coupon_image').val());
          var qr_image = $.trim($('#qr_image').val());
          var quantity = $.trim($('#quantity').val());
          var exp_date = $.trim($('#exp_date').val());

          if(coupon_name=='')
          {
            $('#error_name').html('Coupon name is required.');
            error = true;
          }
          else
          {
            $('#error_name').html('');
          }
          if(coupon_des=='')
          {
            $('#error_des').html('Coupon description is required.');
            error = true;
          }
          else
          {
            $('#error_des').html('');
          }
          if(coupon_image=='')
          {
            $('#error_img').html('Coupon image is required.');
            error = true;
          }
          else
          {
            $('#error_img').html('');
          }
          if(qr_image=='')
          {
            $('#error_qrimg').html('Coupon QR code is required.');
            error = true;
          }
          else
          {
            $('#error_qrimg').html('');
          }
          if(quantity=='')
          {
            $('#error_qyt').html('quantity is required.');
            error = true;
          }
          else
          {
            $('#error_qyt').html('');
          }
          if(exp_date=='')
          {
            $('#error_expdate').html('expiry date is required.');
            error = true;
          }
          else
          {
            $('#error_expdate').html('');
          }
          if(error)
          {
               return false; 
          }
    }
</script>


