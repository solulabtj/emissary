<script>

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
<style>
    .dropdown1{    
        word-wrap: break-word;
    }
    .red{
        color: red;
    }
</style>

<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Create Alerts</h1>
        <?php echo $this->Session->flash(); ?>
        <div class="add-surveys-box shadow">            
            <div class="row">                
                <div class="col-md-24">
                    <form id="myForm" method="post" action="<?php echo WEBSITE_ROOT . "clients/save_alert"; ?>" enctype="multipart/form-data" >
                        <input style="width: 25%;" type="text" class="form-control" name="alert_name" id="alert_name" placeholder="Alert Name">
                        <div id="error_alert_name" class="red"></div>
                        <br>
                        <div style="float: left;width: 25%;">
                            <label style="margin-left: 0px;">Alert Type</label><br>
                            <input type="radio" class="alerttype" name="alert_type" value="1">  Job Alerts <br>
                            <input type="radio" class="alerttype" name="alert_type" value="2">  Payment Alerts <br>
                            <input type="radio" class="alerttype" name="alert_type" value="3">  Survey Category Alerts <br>
                            <input type="radio" class="alerttype" name="alert_type" value="4">  Survey Question Alerts <br>
                            <div id="error_alert_type" class="red"></div>
                        </div>

                        <div style="float: left;width: 25%;">
                            <label style="margin-left: 0px;">Alert Action</label><br>
                            <div id="job_alert" style="display:none;">
                                <input type="radio" name="alert_action" value="1">  Job Requests <br>
                                <input type="radio" name="alert_action" value="2">  Job Submissions <br>
                                <div class="error_alert_action red"></div>
                            </div>
                            <div id="pay_alert" style="display:none;">
                                <input type="radio" name="alert_action" value="1" id="agent_pay">  Agent Payment Due <br>
                                <div class="error_alert_action red" ></div>
                            </div>
                            <div id="sur_cat_alert" style="display:none;">
                                <input type="radio" name="alert_action" value="1">  Category Submissions <br>
                                <div class="error_alert_action red" ></div>
                            </div>
                            <div id="sur_que_alert" style="display:none;">
                                <input type="radio" name="alert_action" value="1">  Question Submission <br>
                                <input type="radio" name="alert_action" value="2">  Submitted a Particular Answer <br>
                                <div class="error_alert_action red" ></div>
                            </div>                            
                        </div>

                        <div style="float: left;width: 50%;" id="selloc">
                            <label style="margin-left: 0px;">Select Location</label><br>                            
                            <?php if (!empty($add)) { ?>
                                <select class='form-control' id="locsel" name="location">
                                    <option>Select Location</option>
                                    <?php foreach ($add as $value) { ?>
                                        <option value="<?php echo $value['client_address']['client_address_id']; ?>"><?php echo $value['client_address']['address']; ?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>                            
                            <div id="error_loc" class="red"></div>
                        </div>

                        <div class="clearfix shadow"></div>


                        <div style="margin-top: 20px;">
                            <div style="float: left;width: 25%;" id="seljob" >
                                <label style="margin-left: 0px;">Select Jobs</label><br>
                                <div style="height: 200px; overflow-y: scroll" id="showjob">                                 
                                </div>
                                <div id="error_job" class="red"></div>
                            </div>
                        </div>

                        <div style="float: left;width: 25%; display: none;" id="selcat">
                            <label style="margin-left: 0px;">Select Category</label><br>
                            <div style="height: 200px; overflow-y: scroll">
                                <?php if (!empty($cat)) { ?>
                                    <?php foreach ($cat as $value) { ?>
                                        <input type="checkbox" class="cat" name="cat[]" value="<?php echo $value['survey_categories']['survey_category_id']; ?>"> <?php echo $value['survey_categories']['survey_category_title']; ?> <br>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <div id="error_cat" class="red"></div>
                        </div>

                        <div class="form-group" style="float: left;width: 25%; display: none; margin-left: 40px;" id="selque"><div id="selques"></div><div id="error_que" class="red"></div></div>
                        <div class="form-group" style="float: left;width: 20%; display: none; margin-left: 40px;" id="ans"><div id="anss"></div><div id="error_ans" class="red"></div></div>

                        <div style="float: right;width: 20%;">
                            <label style="margin-left: 0px; display: none;" id="nosub">Total Number of Submissions</label><label style="margin-left: 0px; display: none;" id="paydue">Number of Payments Due</label><label style="margin-left: 0px;" id="reql">Total Number of Requests</label><br>
                            <input id="req" type="number" name="req"  min="0" onkeypress="return isNumberKey(event)" />
                            <div id="error_req" class="red"></div>
                        </div>
                        
                        <div style="margin-top:280px;">
                            <label style="margin-left: 0px;">Alert Content</label><br>
                            <textarea style="width: 500px;height: 100px;" name="alert_content" id="con"></textarea>
                            <div id="error_con" class="red"></div>
                        </div>

                        <br>
                        <div style="margin-top: 100px;float: right;">
                            <button  class="btn-custom1 create-job" type="submit" onclick="return validate();">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>                
    </div>
</div>                
<?php
echo $this->Html->script('jquery.min');
echo $this->Html->script('bootstrap.min');
echo $this->Html->script('metisMenu.min');
echo $this->Html->script('sb-admin-2');
echo $this->Html->script('bootstrap-datepicker.js');
?>

<script type="text/javascript">

    $("#locsel").on("change", function () {
        $("#showjob").html(" ");
        var chValues = $("#locsel option:selected").val();
        var al = $('input[name=alert_type]:checked', '#myForm').val();
        if (al === "1") {

            $.ajax({
                url: '<?php echo WEBSITE_ROOT . "clients/get_job"; ?>',
                type: 'post',
                data: {ids: chValues},
                success: function (data) {
                    if (data) {
                        $("#showjob").html(data);
                    }
                }
            });
        }
    });



    $(".cat").on("change", function () {
        var checkValues = $("input[type='checkbox'][name='cat[]']:checked").map(function ()
        {
            return $(this).val();
        }).get();
        var al = $('input[name=alert_type]:checked', '#myForm').val();

        if (al === "4") {

            $.ajax({
                url: '<?php echo WEBSITE_ROOT . "clients/get_questions"; ?>',
                type: 'post',
                data: {ids: checkValues},
                success: function (data) {
                    if (data) {
                        $("#selques").html(data);
                    }
                }
            });
        }
    });


    $(document).on("change", "#quesel", function () {
        var selValues = $(this).val();

        var al = $('input[name=alert_type]:checked', '#myForm').val();
        var aa = $('input[name=alert_action]:checked', '#myForm').val();
        if (al === "4" && aa === "2") {

            $.ajax({
                url: '<?php echo WEBSITE_ROOT . "clients/get_ans"; ?>',
                type: 'post',
                data: {ids: selValues},
                success: function (data) {
                    if (data) {
                        $("#anss").html(data);
                    }
                }
            });
        }
    });

    $(".alerttype").on("change", function () {
        var value = $(this).val();
        $('.cat').attr('checked', false);
        if (value == "1") {
            $("#job_alert").css("display", "block");
            $("#pay_alert").css("display", "none");
            $("#sur_cat_alert").css("display", "none");
            $("#sur_que_alert").css("display", "none");
            $("#selloc").css("display", "block");
            $("#seljob").css("display", "block");
            $("#selcat").css("display", "none");
            $("#paydue").css("display", "none");
            $("#nosub").css("display", "none");
            $("#reql").css("display", "block");
            $("#ans").css("display", "none");
            $("#selque").css("display", "none");
            $("#selques").html('');
            $("#anss").html('');
        } else if (value == "2") {
            $("#job_alert").css("display", "none");
            $("#pay_alert").css("display", "block");
            $("#sur_cat_alert").css("display", "none");
            $("#sur_que_alert").css("display", "none");
            $("#selloc").css("display", "none");
            $("#seljob").css("display", "none");
            $("#selcat").css("display", "none");
            $("#paydue").css("display", "block");
            $("#paydue").css("margin-top", "-140px");
            $("#nosub").css("display", "none");
            $("#selque").css("display", "none");
            $("#selques").html('');
            $("#anss").html('');
            $("#reql").css("display", "none");
            $("#ans").css("display", "none");
        } else if (value == 3) {
            $("#job_alert").css("display", "none");
            $("#pay_alert").css("display", "none");
            $("#sur_cat_alert").css("display", "block");
            $("#sur_que_alert").css("display", "none");
            $("#selloc").css("display", "block");
            $("#seljob").css("display", "none");
            $("#selcat").css("display", "block");
            $("#paydue").css("display", "none");
            $("#nosub").css("display", "block");
            $("#reql").css("display", "none");
            $("#selque").css("display", "none");
            $("#ans").css("display", "none");
            $("#selques").html('');
            $("#anss").html('');
        } else if (value == 4) {
            $("#job_alert").css("display", "none");
            $("#pay_alert").css("display", "none");
            $("#sur_cat_alert").css("display", "none");
            $("#sur_que_alert").css("display", "block");
            $("#selcat").css("display", "block");
            $("#selloc").css("display", "block");
            $("#seljob").css("display", "none");
            $("#paydue").css("display", "none");
            $("#nosub").css("display", "block");
            $("#reql").css("display", "none");
            $("#selque").css("display", "block");
            $("#ans").css("display", "block");
        } else {
            $("#job_alert").css("display", "none");
            $("#pay_alert").css("display", "none");
            $("#sur_cat_alert").css("display", "none");
            $("#sur_que_alert").css("display", "none");
            $("#selloc").css("display", "block");
            $("#seljob").css("display", "block");
            $("#ans").css("display", "none");
        }
    });


    function validate()
    {
        var error = false;
        var aa = $('input[name=alert_action]:checked', '#myForm').val();
        var alert_name = $.trim($('#alert_name').val());
        var req = $('#req').val();
        var con = $("#con").val();


        if (alert_name === '')
        {
            $('#error_alert_name').html('Alert Name is required.');
            error = true;
        } else
        {
            $('#error_alert_name').html('');
        }
        
        if (con === '')
        {
            $('#error_con').html('Alert Content is required.');
            error = true;
        } else
        {
            $('#error_con').html('');
        }

        if (req === '' || req === '0')
        {
            $('#error_req').html('Please enter number.');
            error = true;
        } else
        {
            $('#error_alert_name').html('');
        }

        if ($('input[name=alert_type]:checked').length <= 0)
        {
            $('#error_alert_type').html('Alert Type is required.');
            error = true;
        } else {
            $('#error_alert_type').html('');
        }

        if ($('input[name=alert_action]:checked').length <= 0)
        {
            $('.error_alert_action').html('Alert Action is required.');
            error = true;
        } else {
            $('.error_alert_action').html('');
        }

        if ($("#selloc").css("display") !== "none") {
            if ($("#locsel").val() == "Select Location")
            {
                $('#error_loc').html('Location is required.');
                error = true;
            } else {
                $('#error_loc').html('');
            }
        }

        if ($("#seljob").css("display") !== "none") {
            if ($("input[type='checkbox'][name='job[]']:checked").length <= 0)
            {
                $('#error_job').html('Job is required.');
                error = true;
            } else {
                $('#error_job').html('');
            }
        }

        if ($("#selcat").css("display") !== "none") {
            if ($("input[type='checkbox'][name='cat[]']:checked").length <= 0)
            {
                $('#error_cat').html('Category is required.');
                error = true;
            } else {
                $('#error_cat').html('');
            }
        }

        if ($("#selque").css("display") !== "none") {
            if ($("#quesel").val() == "Select Question")
            {
                $('#error_que').html('Question is required.');
                error = true;
            } else {
                $('#error_que').html('');
            }
        }

        if (aa === "2") {
            if ($("#ans").css("display") !== "none") {

                if ($('input[name=ans]:checked').length <= 0)
                {
                    $('#error_ans').html('Answer is required.');
                    error = true;
                } else {
                    $('#error_ans').html('');
                }
            }
        }

        if (error)
        {
            return false;
        } else {
            return true;
        }

    }
</script>


