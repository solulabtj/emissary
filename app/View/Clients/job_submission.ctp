<script>
    $(document).ready(function(){
     
        $('#requestTable').DataTable({"bSort": false});

    });
</script>

        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Job Submission</h1>
                
                <?php echo $this->Session->flash(); ?>
                
                <div class="clearfix"></div>

                <div class="table-custom shadow">
                    <table id="requestTable" class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-user"></i>Agent Name</th>
                                <th><i class="fa fa-th-list"></i>Job Id</th>
                                <th><i class="fa fa"></i>Completed %</th>
                                <th><i class="fa fa-book"></i>Submitted</th>
                                <th><i class="fa fa-hourglass-half"></i>Status</th>
                                <th><i class="fa fa-bullhorn"></i>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            //pr($job_submission_list);exit;
                                foreach ($job_submission_list as $list) { 
									$request_status = '';

                                    if($list['AgentJob']['completed_by_agent'] == 1 && $list['AgentJob']['disapproved_by_client'] == 0 && $list['AgentJob']['approved_by_client'] == 0)
                                    {
                                        $request_status = 'Pending Approval';
                                    }
                                    elseif ($list['AgentJob']['completed_by_agent'] == 1 && $list['AgentJob']['disapproved_by_client'] == 1 && $list['AgentJob']['approved_by_client'] == 0)
                                    {
                                        $request_status = 'Disapproved';
                                    }
                                    elseif ($list['AgentJob']['completed_by_agent'] == 1 && $list['AgentJob']['disapproved_by_client'] == 0 && $list['AgentJob']['approved_by_client'] == 1)
                                    {
                                        $request_status = 'Approved';
                                    }

                                ?>
                                <tr>
                                    <td><?php echo $list['AgentUserInfo']['agent_name'];?></td>
                                    <td><?php echo $list['AgentJob']['job_id'];?></td>
                                    <td><?php echo $list['AgentJob']['percentage_earned'];?>%</td>
                                    <td>
																		
                                    <?php 
							
									 //$now = time(); 
									 //$your_date = strtotime($list['AgentJob']['created']);
									 //$datediff = $now - $your_date;
									 //echo floor($datediff/(60*60*24)).' days ago';
									 
									 echo date('m-d-Y',strtotime($list['AgentJob']['created']));
									
									?>
                                    
                                    </td>
                                    <td><?php echo $request_status; ?></td>
                                    <td><a href="<?php echo WEBSITE_ROOT.'clients/view_answer/'.$list['AgentJob']['agent_job_id'] ?>">View Answers</a></td>
                                </tr>
                                    
                                <?php }

                            ?>
                           
                        </tbody>
                    </table>
                    
                </div>
                
            </div>

        </div>


</body>
</html>