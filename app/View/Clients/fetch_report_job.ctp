<?php

echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->css('jquery.dataTables.min');
?>
<script>
    $(document).ready(function () {

        $('#settelmentTable').DataTable({"bSort": false});

    });
</script>
<style>
.table-custom .table>thead>tr>th{
            text-align: left !important;
    }  
    
 .table-custom .table>tbody>tr>td{
            text-align: left !important;
    }     
</style>
<?php //echo "<pre>"; print_r($res); exit; ?>
<div class="clearfix"></div>
<div class="table-custom shadow">
    <table id="settelmentTable" class="table text-center">
        <thead>
            <tr>
                <th>Job ID</th>
                <th>Survey</th>                        
                <th>Start Date</th>
                <th>End Date</th>
                <th>Request Received</th>
                <th>Submission Received</th>
                <th>Approved Jobs</th>
            </tr>
        </thead>
        <tbody>
             <?php foreach ($res as $job_list) { //echo "<pre>";  print_r($job_list);?>             
            <?php 
                if($job_list['jobs']['job_start_date'] != "0000-00-00 00:00:00"){ $job_start_date = date("d-m-Y",strtotime($job_list['jobs']['job_start_date']));}else{ $job_start_date = "";}
                if($job_list['jobs']['job_end_date'] != "0000-00-00 00:00:00"){ $job_end_date = date("d-m-Y",strtotime($job_list['jobs']['job_end_date']));}else{ $job_end_date = "";}
            ?>
        <td><?php echo $job_list['jobs']['job_id']; ?></td>
        <td><?php echo $job_list['surveys']['survey_name']; ?></td>
        <td><?php echo $job_start_date; ?></td>
        <td><?php echo $job_end_date; ?></td>
        <td><?php echo $job_list['jobs']['request_received']; ?></td>
        <td><?php echo $job_list['jobs']['submission_received']; ?></td>
        <td><?php echo $job_list['jobs']['approved_count']; ?></td>
        </tr>
                 <?php }  ?>
        </tbody>
    </table>
    <footer class="table-footer">
        <div class="row">                   
        </div>
    </footer>
</div>
<?php exit(); ?>