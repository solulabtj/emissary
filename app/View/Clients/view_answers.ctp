<script type="text/javascript">
		
		function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
		}		

</script>

<style>
    .error{
        color: red;
    }
</style>

<div id="page-wrapper">
    <div class="row">
	    <h1 class="page-header">Add Survey Questions</h1>
	    <div class="center" align="center"><?php echo $this->Session->flash(); ?></div>
		<form method="post" enctype="multipart/form-data" action="">
	    	<input type="hidden" name="question_count" id="question_count" value="2" />
	       	<div id="apply_formdata" class="col-md-6">  
	            
	    		    <h2 class="page-header">View Answers</h2>
                    
                    <?php foreach($survey_question_ids as $survey_question_id) { 
							//pr($question_lists);
						  foreach($question_lists[$survey_question_id] as $question_list) {
						
						  $aryData1 = array();
				  		  $aryData2 = array(); 
						  foreach($data[$survey_question_id] as $data_select) 
						  {
							  $aryData1[] = $data_select['AgentSurveyAnswer']['survey_question_answer'];
					  		  $aryData2 = $aryData1;
						  }
						  
					?>
                               	
	    			<div class="input-box">
	        			<label class="input-name"><span>Question -- </span>
	                        <?php echo $question_list['SurveyQuestion']['survey_question_description']; ?>
	                    </label>
	           		</div>
	        	    
                    <?php 
						
						if($question_list['SurveyQuestion']['survey_question_type']=='C') {
						foreach($question_list['SurveyQuestionOption'] as $question_option) { 
						//pr($question_option['survey_option_id']);
						?>
                    <div class="input-box">
	        			<input type="checkbox" name="" value="<?php echo $question_option['answer_text']; ?>"<?php if(in_array($question_option['survey_option_id'],$aryData2)) { echo "checked='checked'"; } ?> /><?php echo $question_option['answer_text']; ?>
	           		</div>
                    <?php } } else if($question_list['SurveyQuestion']['survey_question_type']=='T') { ?>
					
					<div class="input-box">
	        			<?php foreach($aryData2 as $aryData) { ?>	
                        	<input type="text" name="" value="<?php echo $aryData; ?>" disabled />
                        <?php } ?>
	           		</div>
					
					<?php } else if($question_list['SurveyQuestion']['survey_question_type']=='I') { ?>
					
					<div class="input-box">
	        			<?php foreach($aryData2 as $aryData) { ?>	
                        	<img src="<?php echo WEBSITE_ROOT.'media/img/'.$aryData; ?>" height="200px" width="200px" />
                        <?php } ?>
	           		</div>
					
					<?php }  else if($question_list['SurveyQuestion']['survey_question_type']=='A') { ?>
					
                    <div class="input-box">
	        			<?php foreach($aryData2 as $aryData) { ?>	
                        	<audio controls>
                              <source src="<?php echo WEBSITE_ROOT.'media/img/'.$aryData; ?>" type="audio/mpeg">                            
                            </audio>
                        <?php } ?>
	           		</div>
					
					<?php } } } ?>
                    
	   		</div>
        	<div class="clearfix"></div>
        	<a href="<?php echo WEBSITE_ROOT.'clients/job_submission' ?>" class="btn btn-lg btn-custom btn-success">Back</a>
        	
		</form>
   	</div>
</div>



	<?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			
	?>
	
    <script type="text/javascript">
	
		$(document).ready(function(){
		
	    
		});

	</script>

	
</body>
</html>