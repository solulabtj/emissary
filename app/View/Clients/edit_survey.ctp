<style>
    .error{
        color: red;
    }
</style>


<div id="page-wrapper">
  <div class="row">
    <h1 class="page-header">Edit Survey</h1>
    <div class="clearfix"></div>
    <div class="add-surveys-top-round">
      <div class="col-md-12 surveys-top" page-id='1'>Survey Information
        <!--<div class="round-box1 round-box-bg-hover">1</div>-->
      </div>
      <!--<div class="col-md-4 surveys-top" page-id='2'>Survey Questions
        <div class="round-box1 round-box-bg">2</div>
      </div>-->
      <!--<div class="col-md-6 surveys-top" page-id='2'>Summary
        <div class="round-box1 round-box-bg">2</div>
      </div>-->
    </div>
    <div class="clearfix"></div>
    <div class="add-surveys-box shadow">
    
   <?php //pr($single); ?>
      <form method="post" id="myform" action="<?php echo WEBSITE_ROOT."clients/edit_survey";?>" >
      
      <input type="hidden" name="survey_id" value="<?php echo $single['Survey']['survey_id'] ?>" />
      
        <div id="survay-1" class="survey_div">
          <input type="text" id="survey_name" name="survey_name" onkeypress="return lettersOnly(event);" id="survey_name" value="<?php echo $single['Survey']['survey_name']; ?>" placeholder="Survey Name" class="form-control ng-pristine ng-valid ng-touched">
          
          <div id="error_survey" class="error"></div>
          
          <br/>
          <textarea name="editor1" id="editor1" rows="10" cols="80"><?php echo $single['Survey']['survey_overview']; ?></textarea>
          
          <div id="error_editor" class="error"></div>
          
          <br/>
          Check-in Via <br/>
          <input name="gps_check" id="gps_check" type="checkbox" value="1"<?php if($single['Survey']['gps_check']=='1') { echo "checked='checked'"; } ?> >
          GPS<br>
          <input name="qr_check" id="qr_check" type="checkbox" value="1"<?php if($single['Survey']['qr_check']=='1') { echo "checked='checked'"; } ?> >
          QR Code<br>
          
          <div id="error_check" class="error"></div>
          
          <br>
          Minimum Completion Threshold
          <div class="clearfix"></div>
          <div class="col-md-2" style="padding-left:0px;">
            <input type="text" name="min_threshold" id="min_threshold" value="<?php echo $single['Survey']['min_threshold']; ?>" onkeypress="return  onlyNumbers(event);"  placeholder="%" class="form-control ng-pristine ng-valid ng-touched">
            
             <div id="error_threshold" class="error"></div>
          </div>
          <div class="clearfix"></div>
          <!-- <div class="col-md-10"> <a class="btn-custom1 create-job pull-right" href="#" id="next">Next</a> 
            <input type="button";="" onclick="setVisibility('survay-1');" value="Hide Layer" onclick="setVisibility('survay-2');" value="show Layer" id="bt1" name="type"> 
          </div> -->
        </div>
        <?php /*?><div id="survay-2" class="survey_div">
          <div><a href="#news" data-toggle="modal" data-target="#modalContact" class="btn-custom1 create-job pull-right">Add Category</a> </div>
          <div class="clearfix"></div>
          <br>
          <div class="col-xs-3" style="position:relative; z-index:9;"> <!-- required for floating --> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs tabs-left">
              <li class="active"><a data-toggle="tab" href="#home">Category 1</a></li>
              <li class=""><a data-toggle="tab" href="#profile">Category 2</a></li>
              <li class=""><a data-toggle="tab" href="#messages">Category 3</a></li>
            </ul>
          </div>
          <div class="col-xs-9" style="padding-left:0px; padding-right:0px;"> 
            <!-- Tab panes -->
            <div class="tab-content" style="border:1px solid #ccc; margin-left:-17px; padding-right:15px; padding-top:15px; text-align:center;">
              <div id="home" class="tab-pane active">
                <div class="tab-content">
                  <div style="background:#f4f4f4; margin-left:0px; padding-right:15px;"> <br>
                    <strong>Add a Question</strong><br>
                    <br>
                    <ul class="nav nav-tabs">
                      <li class="col-md-3"><a data-toggle="tab" href="#mcq" onclick="show('mcq')"><i class="fa fa-list"></i><br>
                        MCQ</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#text" onclick="show('text')"><i class="fa fa-font"></i><br>
                        Text</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#image" onclick="show('image')"><i class="fa fa-file-image-o"></i><br>
                        Image</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#audio" onclick="show('audio')"><i class="fa fa-file-audio-o"></i><br>
                        Audio</a></li>
                    </ul>
                  </div>
                  <div class="clearfix"></div>
                  <div id="mcq" class="" > <a class="btn-custom1 create-job pull-left" href="#">Add From Predefined Questions</a>
                    <div class="clearfix"></div>
                    <br>
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-6" style="padding-left:0px;">
                        <input name="Option 1" type="checkbox" value="">
                        Option 1<br>
                        <input name="Option 2" type="checkbox" value="">
                        Option 2<br>
                        <input name="Option 3" type="checkbox" value="">
                        Option 3<br>
                        <input name="Option 4" type="checkbox" value="">
                        Option 4<br>
                      </div>
                      <div class="col-md-6">
                        <input name="GPS" type="checkbox" value="">
                        Mendatory<br>
                        <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                        <div class="clearfix"></div>
                        <a class="btn-custom1 create-job pull-left" href="#">Save</a> </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="text" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="image" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="audio" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <br>
                </div>
              </div>
              <div id="profile" class="tab-pane">
                <div class="tab-content">
                  <div style="background:#f4f4f4; margin-left:0px; padding-right:15px;"> <br>
                    <strong>Add a Question</strong><br>
                    <br>
                    <ul class="nav nav-tabs">
                      <li class="col-md-3"><a data-toggle="tab" href="#mcq1" onclick="show('mcq1')"><i class="fa fa-list"></i><br>
                        MCQ</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#text1" onclick="show('text1')"><i class="fa fa-font"></i><br>
                        Text</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#image1" onclick="show('image1')"><i class="fa fa-file-image-o"></i><br>
                        Image</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#audio1" onclick="show('audio1')"><i class="fa fa-file-audio-o"></i><br>
                        Audio</a></li>
                    </ul>
                  </div>
                  <div class="clearfix"></div>
                  <div id="mcq1" class="" > <a class="btn-custom1 create-job pull-left" href="#">Add From Predefined Questions</a>
                    <div class="clearfix"></div>
                    <br>
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-6" style="padding-left:0px;">
                        <input name="Option 1" type="checkbox" value="">
                        Option 1<br>
                        <input name="Option 2" type="checkbox" value="">
                        Option 2<br>
                        <input name="Option 3" type="checkbox" value="">
                        Option 3<br>
                        <input name="Option 4" type="checkbox" value="">
                        Option 4<br>
                      </div>
                      <div class="col-md-6">
                        <input name="GPS" type="checkbox" value="">
                        Mendatory<br>
                        <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                        <div class="clearfix"></div>
                        <a class="btn-custom1 create-job pull-left" href="#">Save</a> </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="text1" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="image1" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="audio1" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <br>
                </div>
              </div>
              <div id="messages" class="tab-pane">
                <div class="tab-content">
                  <div style="background:#f4f4f4; margin-left:0px; padding-right:15px;"> <br>
                    <strong>Add a Question</strong><br>
                    <br>
                    <ul class="nav nav-tabs">
                      <li class="col-md-3"><a data-toggle="tab" href="#mcq2" onclick="show('mcq2')"><i class="fa fa-list"></i><br>
                        MCQ</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#text2" onclick="show('text2')"><i class="fa fa-font"></i><br>
                        Text</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#image2" onclick="show('image2')"><i class="fa fa-file-image-o"></i><br>
                        Image</a></li>
                      <li class="col-md-3"><a data-toggle="tab" href="#audio2" onclick="show('audio2')"><i class="fa fa-file-audio-o"></i><br>
                        Audio</a></li>
                    </ul>
                  </div>
                  <div class="clearfix"></div>
                  <div id="mcq2" class="" > <a class="btn-custom1 create-job pull-left" href="#">Add From Predefined Questions</a>
                    <div class="clearfix"></div>
                    <br>
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <div class="col-md-6" style="padding-left:0px;">
                        <input name="Option 1" type="checkbox" value="">
                        Option 1<br>
                        <input name="Option 2" type="checkbox" value="">
                        Option 2<br>
                        <input name="Option 3" type="checkbox" value="">
                        Option 3<br>
                        <input name="Option 4" type="checkbox" value="">
                        Option 4<br>
                      </div>
                      <div class="col-md-6">
                        <input name="GPS" type="checkbox" value="">
                        Mendatory<br>
                        <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                        <div class="clearfix"></div>
                        <a class="btn-custom1 create-job pull-left" href="#">Save</a> </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="text2" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="image2" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div id="audio2" class="">
                    <div class="question-box-border">
                      <input placeholder="Lorem ipsum dolor sit amet, tincidunt neque placerat." type="text" value="" style="width:100%;">
                      <div class="clearfix"></div>
                      <br>
                      <input name="GPS" type="checkbox" value="">
                      Mendatory<br>
                      <input placeholder="Score" type="text" value="" style="margin:10px 0px;">
                      <div class="clearfix"></div>
                      <a class="btn-custom1 create-job pull-left" href="#">Save</a>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <br>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <br>
          <!-- <div class="col-md-12"> <a class="btn-custom1 create-job pull-right" href="add-surveys3.html">Next</a> </div> -->
        </div><?php */?>
        <div class="survey_div">
          <!--<h3 style="margin-top:0px; text-align:center;">Survey Overview</h3>
          <h4>Topic of the survey</h4>
          <p>Lorem ipsum dolor sit amet, tincidunt neque placerat. Eleifend sed, tempor metus amet interdum inceptos faucibus tincidunt, senectus nec tristique. Faucibus faucibus at lorem urna mi, risus sodales penatibus mauris amet metus sodales, mauris odio integer, phasellus blandit scelerisque morbi faucibus sed, et feugiat orci aliquam convallis mauris. Ut amet ut aenean libero tincidunt amet, ac porttitor volutpat elit non. Volutpat a tempus elit in, habitasse porta, vestibulum sed nunc wisi mauris lectus ultrices. Dolor ligula erat, et varius vel libero, suspendisse quis egestas dui ullamcorper est. </p>
          <div class="clearfix"></div>-->
          <!--<h4>Survey Categories</h4>
          Category One<br>
          Category Two<br>
          Category Three<br>
          Category Four<br>
          Category Five<br>-->
          <br>
          <h4>Survey Status</h4>
          <select name="is_active" id="is_active" >
          
            <option value="" >Survey Status</option>
            <option value="1"<?php if($single['Survey']['is_active']=='1') { echo "selected='selected'"; } ?> >Active</option>
            <option value="0"<?php if($single['Survey']['is_active']=='0') { echo "selected='selected'"; } ?> >Inactive</option>
            
          </select>
          
          <div id="error_active" class="error"></div>
          
          <div id="error_alldata" class="error"></div>
          <br>
          <br>
          <div style="text-align:center; padding:25px 0px 0px 0px;">
                
                    <a class="btn-custom1 create-job" href="">Cancel</a>
                
                    <button type="submit" onclick="return validate_block1();" class="btn-custom1 create-job"  >Save and Publish</button>
                
                </div>
          <div class="clearfix"></div>
        </div>
        <!--<div>
          <a class="btn-custom1 create-job pull-left back-btn hidden" data-id="" href="#">Back</a>
          <a class="btn-custom1 create-job pull-right next-btn" href="#" data-id="2">Next</a>
        </div>-->
      </form>
    </div>
  </div>
</div>

<!-- Make sure the path to CKEditor is correct. --> 
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript" src="<?php echo WEBSITE_ROOT ?>js/ckeditor/ckeditor.js"></script> 
<script>
  function show(name){
    document.getElementById(name).style.display='block';
  }
  $(document).ready(function() {	
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace( 'editor1' );

      $(".next-btn").click(function(e){
        e.preventDefault();
        var data_id = $(this).attr('data-id');
        var data_plus = parseInt(data_id)+1;
        var data_minus = parseInt(data_id)-1;
        $('.survey_div').hide();
        $('#survay-'+data_id).show();
        if(data_plus > 2) $(this).addClass('hidden');
        if(data_minus >= 1) $('.back-btn').removeClass('hidden');
        $(this).attr("data-id",data_plus);
        $('.back-btn').attr("data-id",data_minus);

        $('.surveys-top div').removeClass('round-box-bg-hover').addClass('round-box-bg');
        $('.surveys-top[page-id='+data_id+'] div').removeClass('round-box-bg').addClass('round-box-bg-hover');

      })

      $(".back-btn").click(function(e){
        e.preventDefault();
        var data_id = $(this).attr('data-id');
        var data_plus = parseInt(data_id)+1;
        var data_minus = parseInt(data_id)-1;
        $('.survey_div').hide();
        $('#survay-'+data_id).show();
        if(data_minus == 0) $(this).addClass('hidden');
        if(data_plus >= 1) $('.next-btn').removeClass('hidden');
        $(this).attr("data-id",data_minus);
        $('.next-btn').attr("data-id",data_plus);

        $('.surveys-top div').removeClass('round-box-bg-hover').addClass('round-box-bg');
        $('.surveys-top[page-id='+data_id+'] div').removeClass('round-box-bg').addClass('round-box-bg-hover');
      })

  });   


  $("#myform").validate(
      {
        rules: 
        {
          survey_name: 
          {
            required: true
          },
          editor1: 
          {
            required: true,
            
          }
        },
        messages: 
        {
          survey_name: 
          {
            required: "Please enter survey name"
          },
          editor1: 
          {
            required: "Please enter overview"
          }
        },
        submitHandler: function(form) {
        form.submit();
     }

      });   
	
	
	function lettersOnly(evt) {
		//alert('hello');
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		  ((evt.which) ? evt.which : 0));
		if (charCode == 32)
			return true;
		if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		  (charCode < 97 || charCode > 122)) {
			return false;
		}
		else
			return true;
	}
	
	function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		{			
			return false;
		}
	
		return true;
	}
	
	function validate_block1()
	{
		var error = false;
		
		var survey_name = $('#survey_name').val();
		
		//var editor1 = $.trim($("textarea#editor1").val());
		
		var check = $('input[type=checkbox]:checked').length;
		
		var min_threshold = $('#min_threshold').val();
		
		var is_active = $('#is_active').val();
		
		if(survey_name=='')
		{
			$('#error_survey').html('Survey Name is required.');
			error = true;
		}
		else
		{
			$('#error_survey').html('');
		}
		
		//if(editor1=='')
//		{
//			$('#error_editor').html('Survey Description is required.');
//			error = true;
//		}
//		else
//		{
//			$('#error_editor').html('');
//		}
//		
		if(check==0)
		{
			$('#error_check').html('Select at least one checkbox.');
			error = true;
		}
		else
		{
			$('#error_check').html('');
		}
		
		if(min_threshold=='' || min_threshold=='0')
		{
			$('#error_threshold').html('Threshold is required and should be not Zero.');
			error = true;
		}
		else if(min_threshold>=100)
		{
			$('#error_threshold').html('Threshold should be less than 100.');
			error = true;
		}
		else
		{
			$('#error_threshold').html('');
		}
		
		if(is_active=='')
		{
			$('#error_active').html('Select survey status.');
			error = true;
		}
		else
		{
			$('#error_active').html('');
		}
		
		if(error)
		{
			$('#error_alldata').html('Please fill all proper data.');
			return false;
		}
	}
	

</script>

</body>
</html>