<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Agent Reports</h1>      
        <div class="col-md-6">                       
            <form action="<?php echo WEBSITE_ROOT."clients/reports";  ?>" method="post">
                <div class="group-job">
                    <h2 class="sml-heading">Active From</h2>
                    <div class="row">
                        <div class="input-box col-md-6">                            
                            <input type="text" class="input-field" value="" id="dpd11" name="date_from" >                                
                        </div>                      
                    </div>                    
                    <h2 class="sml-heading">Rank</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="rank"  name = "rank" class="input-field">
                                <option value="0">Select Rank</option>                                
                                    <?php foreach ($rank as $value) { ?>                                    
                                <option value="<?php echo $value['agent_ranks']['rank']; ?>"><?php echo $value['agent_ranks']['rank_title']; ?></option>
                                    <?php } ?> 
                            </select>
                        </label>
                    </div>
                    <h2 class="sml-heading">Test Status</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="test_status"  name = "test_status" class="input-field">
                                <option value="n">Select Status</option>
                                <option value="1">Passed</option>
                                <option value="0">Failed</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-md-12 text-center">
                        <input type="button" id="genrate" onclick="report_generate();" value="Generate" class="btn btn-lg btn-custom btn-success">
                        <input type="button"  onclick="export_report();" value="Export" class="btn btn-lg btn-custom btn-success">
                        <!--<a href="javascript(void:0);" id="genrate" onclick="report_generate();" class="btn btn-lg btn-custom btn-success" >Genrate</a>-->
                    </div>
                </div>
            </form>
        </div>
        <div style="margin-top: 40%;" id="reportsData"></div>
        <div id="agent_report_div"></div>
    </div>
</div>
<?php
    echo $this->Html->script('jquery.js');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('metisMenu.min');
    echo $this->Html->script('sb-admin-2');
    echo $this->Html->script('bootstrap-datepicker.js');    
    echo $this->Html->script('googleChart');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#dpd11').datepicker({
            format: 'dd-mm-yyyy'
        });

        $('#dpd11').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        google.charts.load('current', {packages: ['corechart', 'line']});
        google.charts.setOnLoadCallback(agent_report);
    });
    function report_generate()
    {
        startdate = $("#dpd11").val();
        rank = $("#rank").val();
        test_status = $("#test_status").val();
        type = $("#type").val();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/fetch_report'; ?>",
            data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
            success: function (data) {
                $('#reportsData').replaceWith($('#reportsData').html(data));
            }

        });
        event.preventDefault();
        return false; //stop the actual form post !important!
    }


    function export_report()
    {
        startdate = $("#dpd11").val();
        rank = $("#rank").val();
        type = $("#type").val();
        test_status = $("#test_status").val
        console.log(startdate);
        if(startdate !== "undefined"){
        var parts = startdate.split('/');
        var startdate = parts[2] + '-' + parts[0] + '-' + parts[1];
        }else{
            startdate = "";
        }
        window.open(
                '<?php echo WEBSITE_ROOT.'clients/export_report'; ?>' + '/' + startdate + '/' + rank + '/' + test_status,
                '_blank' // <- This is what makes it open in a new window.
                );
    }



    function agent_report() {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/getGraphData'; ?>",
            //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
            success: function (response) {
                // console.log(response.toString());
                var data = new google.visualization.DataTable();
                response = JSON.parse(response);
                data.addColumn('string', 'X');
                $.each(response.finalArr, function (index, value) {
                    // alert(value.agent_name);
                    data.addColumn('number', value.agent_name);
                });

                $.each(response.month, function (index, value) {
                    data.addRows([value]);
                });
//                                        data.addRows([
//                                            [0, 0, 0], [1, 10, 5], [2, 23, 15], [3, 17, 9], [4, 18, 10], [5, 9, 5],
//                                            [6, 11, 3], [7, 27, 19], [8, 33, 25], [9, 40, 32], [10, 32, 24], [11, 35, 27]
//                                        ]);

                var options = {
                    hAxis: {
                        title: 'Month'
                    },
                    vAxis: {
                        title: 'Completed Jobs',
                        viewWindowMode: "explicit",
                        viewWindow: {min: 0}
                    },
                    series: {
                        1: {curveType: 'function'}
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById('agent_report_div'));
                chart.draw(data, options);
            }

        });
    }
</script>
</body>
</html>