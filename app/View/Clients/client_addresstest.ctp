<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<?php 
    echo $this->Html->script('locationpicker.jquery.min');
?>

    <div id="page-wrapper">
        <div class="row">
            <h1 class="page-header">Add Location</h1>
            <form>
                <input type="hidden" name="question_count" id="question_count" value="2" />
                <div id="apply_formdata" class="col-md-6"> 
                    <div id="apply_question1" >
                        <h2 class="page-header">Location 1</h2>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Location:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control addrs" id="address"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Radius:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control rad" id="radius"/>
                            </div>
                        </div>
                        <div id="us3"  class="map" style="width: 550px; height: 400px;"></div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="m-t-small">
                            <label class="p-r-small col-sm-1 control-label">Lat.:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control latt" style="width: 110px" id="lat"/>
                                </div>
                            <label class="p-r-small col-sm-2 control-label">Long.:</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control long" style="width: 110px" id="lon"/>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="clearfix"></div>
                <button type="submit" onclick="return validate_settings();" class="btn btn-lg btn-custom btn-success">Save All</button>
                <a id="add_location" class="btn btn-lg btn-custom btn-success">Add Location</a>
            </form>
        </div>
    </div>                  
                    

<script type="text/javascript">
    
    $(".map").locationpicker({
        location: {latitude: 46.15242437752303, longitude: 2.7470703125},
        radius: 300,
        inputBinding: {
            latitudeInput: $('.latt'),
            longitudeInput: $('.long'),
            radiusInput: $('.rad'),
            locationNameInput: $('.addrs')
        },
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            // Uncomment line below to show alert on each Location Changed event
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
        }
    });


    $("#add_location").click(function(){
                
                    var question_count = $('#question_count').val();
                    $.ajax({
                                type: "POST",          
                                url: "<?php echo WEBSITE_ROOT.'clients/add_client_address'; ?>",
                                data: "question_count="+question_count,
                                success: function(data){
                                    console.log(data);
                                        $('#question_count').val((question_count*1+1));
                                        $('#apply_formdata').append(data);
                                }
                            });
                
             });
</script>

</body>
</html>