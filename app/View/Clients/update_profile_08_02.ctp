<script type="text/javascript">
		
		function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
		}
		

        function showimagepreview(input) {
            if (input.files && input.files[0]) {
            var filerdr = new FileReader();
            filerdr.onload = function(e) {
            $('#imgprvw').attr('src', e.target.result);
            }
            filerdr.readAsDataURL(input.files[0]);
            }
        }



         function show_comp_imagepreview(input) {
            if (input.files && input.files[0]) {
            var filerdr = new FileReader();
            filerdr.onload = function(e) {
            $('#comp_img_pr').attr('src', e.target.result);
            }
            filerdr.readAsDataURL(input.files[0]);
            }
        }


</script>

<?php
if(!empty($listing['ClientUserInfo']['company_logo']))
{
	$company_logo = $listing['ClientUserInfo']['company_logo'];	
}
else
{
	$company_logo = 'prof-pic.jpg';	
}

if(!empty($listing['ClientImage']['image_file_path']))
{
    $company_image = $listing['ClientImage']['image_file_path']; 
    $client_image_id = $listing['ClientImage']['client_image_id']; 
}
else
{
    $company_image = 'prof-pic.jpg';
    $client_image_id = 0;  
}
?>


<div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Edit Profile</h1>
                <div class="center" align="center"><?php echo $this->Session->flash(); ?></div>
            	<form method="post" enctype="multipart/form-data" action="<?php echo WEBSITE_ROOT.'clients/update_profile'; ?>">
                
                <input type="hidden" name="client_id" value="<?php echo $this->Session->read('Auth.User.client_id'); ?>" />
                
                <div class="edit_profsection">
                <div class="pic-banner shadow">
            	<div class="prof-pic">
            			<img id = "imgprvw"src="<?php echo WEBSITE_ROOT."img/".$company_logo ?>">
            			<div class="fileUpload">
						    <span>Upload Photo</span>
						    <input type="file" name="company_logo" class="upload" onchange="showimagepreview(this)" />
						</div>
            		</div>
                </div>
                	<div class="col-md-6">                		
                	<div class="input-box">
	                		<label class="input-name"><span>Company Name</span><input type="text" name="company_name" class="input-field" value="<?php echo $listing['ClientUserInfo']['company_name']; ?>" ></label>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Overview</span><textarea name="company_overview" class="input-field" rows="3"><?php echo $listing['ClientUserInfo']['company_overview']; ?></textarea></label>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Phone Number</span><input type="text" name="mobile_number" class="input-field" onkeypress="return onlyNumbers(event)" value="<?php echo $listing['ClientUserInfo']['mobile_number']; ?>" ></label>
                		</div>                		
                	</div>
                	<div class="col-md-6">
                		<div class="input-box">
	                		<label class="input-name"><span>Agency Name</span><input type="text" name="agency_name" class="input-field" value="<?php echo $listing['ClientUserInfo']['agency_name']; ?>" ></label>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Address</span><textarea name="client_address" class="input-field" rows="3"><?php echo $listing['ClientUserInfo']['client_address']; ?></textarea></label>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Paypal ID</span><input type="text" name="paypal_id" class="input-field" value="<?php echo $listing['ClientUserInfo']['paypal_id']; ?>" ></label>
                		</div>
                	</div>
                	<div class="col-md-12">
                		<h2 class="reg-header"><i class="fa fa-picture-o"></i>Pictures</h2>
                		<div class="editprof-images">
                			<ul>
                				
                				<li class="addmore">
                					<div class="gal-img">
                                        <div class="prof-pic">
                                            <input type="hidden" name="client_image_id" value="<?php echo $client_image_id; ?>">
                                                <img id = "comp_img_pr"src="<?php echo WEBSITE_ROOT."img/".$company_image ?>">
                                                <div class="fileUpload">
                                                    <span>Upload Photo</span>
                                                    <input type="file" name="company_image" class="upload" onchange="show_comp_imagepreview(this)" />
                                                </div>
                                            </div>
                                        </div>
                						<!--<a href=""> 
                							<i class="fa fa-plus"></i>
                							<span>Add More</span>
            							</a> -->
                					</div>
                				</li>
                			</ul>
                		</div>

                		<div class="clearfix"></div>

                		<h2 class="reg-header"><i class="fa fa-clock-o"></i>Working Hours</h2>
                		<div class="work-detil">

                        <div class="col-md-3">
                            <div class="input-box">
                            <label class="input-name"><span>From</span><input type="number" name="working_hour_start" class="input-field" value="<?php echo $listing['ClientUserInfo']['working_hour_start']; ?>" ></label>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-box">
                            <label class="input-name"><span>To</span><input type="number" name="working_hour_end" class="input-field" value="<?php echo $listing['ClientUserInfo']['working_hour_end']; ?>" ></label>
                        </div>
                        </div>
                		<div class="clearfix"></div>	
                		<div class="col-md-12">
                            <div class="wrk-dys">
                				<ul class="list-unstyled list-inline">
                					<li><label><input type="checkbox" name="monday" value="1"<?php if($listing['ClientUserInfo']['monday']=="1") { echo "checked='checked'"; }; ?> >Mon</label></li>
                					<li><label><input type="checkbox" name="tuesday" value="1"<?php if($listing['ClientUserInfo']['tuesday']=="1") { echo "checked='checked'"; }; ?> >Tue</label></li>
                					<li><label><input type="checkbox" name="wednesday" value="1"<?php if($listing['ClientUserInfo']['wednesday']=="1") { echo "checked='checked'"; }; ?> >Wed</label></li>
                					<li><label><input type="checkbox" name="thursday" value="1"<?php if($listing['ClientUserInfo']['thursday']=="1") { echo "checked='checked'"; }; ?> >Thu</label></li>
                					<li><label><input type="checkbox" name="friday" value="1"<?php if($listing['ClientUserInfo']['friday']=="1") { echo "checked='checked'"; }; ?> >Fri</label></li>
                					<li><label><input type="checkbox" name="saturday" value="1"<?php if($listing['ClientUserInfo']['saturday']=="1") { echo "checked='checked'"; }; ?> >Sat</label></li>
                					<li><label><input type="checkbox" name="sunday" value="1"<?php if($listing['ClientUserInfo']['sunday']=="1") { echo "checked='checked'"; }; ?> >Sun</label></li>
                				</ul>
                			</div>
                        </div>
                		</div>
                		<div class="clearfix"></div>

                		<button type="submit" class="btn btn-lg btn-custom btn-success">Update</button>

                	</div>
                </div>
                
                </form>
                
            </div>

</div>


	<?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			
	?>
	

	<!-- jQuery -->
   <?php /*?> <script src="<?php echo WEBSITE_ROOT; ?>js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/sb-admin-2.js"></script><?php */?>
</body>
</html>