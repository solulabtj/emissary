<?php

echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->css('jquery.dataTables.min');
?>
<script>
    $(document).ready(function () {

        $('#settelmentTable').DataTable();

    });
</script>
<style>
.table-custom .table>thead>tr>th{
            text-align: left !important;
    }  
    
 .table-custom .table>tbody>tr>td{
            text-align: left !important;
    }     
</style>
<?php //echo "<pre>"; print_r($res); exit; ?>
<div class="clearfix"></div>
<div class="table-custom shadow">
    <table id="settelmentTable" class="table text-center">
        <thead>
            <tr>
                <th>Agent Name</th>
                <th>Job ID</th>                        
                <th>Job Completed</th>
                <th>Submitted</th>
                <th>Payment Status</th>
                <th>Reward</th>
            </tr>
        </thead>
        <tbody>
                    <?php foreach ($res as $job_list) { ?>
                    <?php  
                        $now = time(); // or your date as well
                        $your_date = strtotime($job_list['agent_jobs']['job_end_time']);
                        $datediff = $now - $your_date;
                        $submitted =  floor($datediff/(60*60*24));
                        
                        if($job_list['agent_jobs']['is_paid'] == "1"){
                            $status = "Paid";
                        }else{
                            $status = "Pending";
                        }
                        
                        if($status != "Pending"){
                            if($job_list['agent_jobs']['coupon_id'] != "" && $job_list['agent_jobs']['coupon_id'] != "0"){
                                $reward = $job_list['coupons']['coupon_name']." KWD"; 
                            }else{
                                $reward = $job_list['agent_jobs']['amount_paid']." KWD"; 
                            }
                        }
                    ?>
            <tr>
                <td><?php echo $job_list['agent_user_infos']['agent_name']; ?></td>
                <td><?php echo $job_list['agent_jobs']['job_id']; ?></td>
                <td><?php echo $job_list['agent_jobs']['percentage_earned']."%"; ?></td>                
                <td><?php echo $submitted." Days Ago"; ?></td>
                <td><?php echo $status; ?></td>
                <td><?php echo $reward; ?></td>
            </tr>
                 <?php }  ?>
        </tbody>
    </table>
    <footer class="table-footer">
        <div class="row">                   
        </div>
    </footer>
</div>
<?php exit(); ?>