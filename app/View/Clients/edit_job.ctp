<style>
    .error{
        color: red;
    }
</style>


<div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Edit Job</h1>
                
                <?php echo $this->Session->flash();
						
					  $message = isset($message)?$message:'';
					 
					  echo $message;
				?>
                
                <div class="col-md-6">
                    <form action="<?php echo WEBSITE_ROOT."clients/edit_job";  ?>" method="post">
                        <div class="group-job">
                        <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Date Span</h2>
                            <div class="row">
                            <div class="input-box col-md-6">
                                <input type="hidden" value="<?php  echo $edit_job_data['job_id']; ?>" name="job_ref">
                                <input type="hidden" value="<?php  echo $edit_job_data['survey_id']; ?>" name="survey_list">
                                <input type="hidden" value="<?php  echo $edit_job_data['job_cat_id']; ?>" name="job_cat">
                                
                                <label class="input-name"><span>Date From</span><input type="text" class="input-field" value="<?php echo $edit_job_data['start_date'] ?>" id="dpd1" name="date_from" readonly="readonly" ></label>
                            </div>
                            <div class="input-box col-md-6">
                                <label class="input-name"><span>Date To</span><input type="text" class="input-field" value="<?php echo $edit_job_data['end_date'] ?>" id="dpd2" name="date_to" readonly="readonly" ></label>
                            </div>
                            <div class="error" id="error_start" ></div>                            
                            <div class="error" id="error_end" ></div>
                            
                            </div>
                        </div>


                        

                        <div class="group-job">
                        <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Location <a class="add" href="<?php echo WEBSITE_ROOT.'clients/client_address' ?>">Manage</a></h2>
                        
                        <?php $job_location_count = count($job_location);
							
							if($job_location_count>0) {
						?>
                        
                        <input type="checkbox" name="ckbCheckAll" id="ckbCheckAll"  />Select All Locations
                        
                        <?php } ?>
                        
                            <div class="input-box" id="selectId" class="checkBoxClass" >
                                
                                <?php  
//pr($client_add_ids);exit();
									if($job_location_count>0)
									{
									foreach ($job_location as $job_locality) {                                     
                                    ?>

                                <input style="width: 3%;" type="checkbox" <?php if(in_array($job_locality['ClientAddress']['client_address_id'], $client_add_ids)) {echo "checked";} ?>    name = "location[]" value = "<?php echo $job_locality['ClientAddress']['client_address_id']; ?>" class="input-field  check checkBoxAddress">
                                <?php echo $job_locality['ClientAddress']['address'];?><br />
                                <?php } } else {
                                ?>
                                <div style="color:red;">Please add atleast one location by clicking on manage button.</div>
                                
                                <?php } ?>
                                
                                 <div class="error" id="error_location" ></div>
                            </div>
                        </div>




                        <div class="group-job">
                        <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Overview</h2>
                            <div class="input-box">
                                <label class="input-name"><textarea name="job_overview" id="job_overview" class="input-field" rows="4"><?php echo $edit_job_data['job_overview'] ?></textarea></label>
                                
                                <div class="error" id="error_job_overview" ></div>
                            </div>
                        </div>

                        <div class="group-job">
                        <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Associated Survey<a class="add" href="<?php echo WEBSITE_ROOT.'clients/surveyList' ?>">Manage</a></h2>
                            <div class="input-box">
                                <label class="input-name"><span>Survey List</span>
                                <select name="survey_list" id="survey_list" disabled class="input-field" >
                                    <?php   

                                    foreach ($survey_option as $survey_id_name) { ?>
                                        <option value = "<?php echo $survey_id_name['Survey']['survey_id']; ?>" <?php if($survey_id_name['Survey']['survey_id'] == $edit_job_data['survey_id']) { echo "selected"; }  ?> ><?php  echo $survey_id_name['Survey']['survey_name']; ?></option>
                                   <?php }

                                    ?>
                                </select>
                                </label>
                                
                                <div id="error_survey" class="error"></div>
                            </div>
                            <div class="group-job">
                                <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Category</h2>
                                <div class="input-box">
                                    <label class="input-name"><span>Category List</span>
                                    <select name="job_cat" disabled id="job_cat" class="input-field">

                                          <?php   

                                            foreach ($cat_option as $cat_id_name) { ?>
                                                <option value = "<?php echo $cat_id_name['JobCategory']['job_category_id']; ?>" <?php if($cat_id_name['JobCategory']['job_category_id'] == $edit_job_data['job_cat_id']) { echo "selected"; }  ?> ><?php  echo $cat_id_name['JobCategory']['job_category_title']; ?></option>
                                   <?php }

                                    ?>

                                    </select>
                                    </label>
                                    
                                    <div id="error_category" class="error"></div>
                                </div>
                            </div>
                        </div>

                    </div><!-- LEFT -->


                    <div class="col-md-6">

                    <div class="group-job">
                    <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Time Span</h2>
                    <div class="row">
                        <div class="input-box col-md-6">
                            <label class="input-name"><span>From</span><input type="text" maxlength="2" class="input-field" name="time_span_from" onkeypress="return onlyNumbers(event);" value="<?php echo $edit_job_data['start_time'] ?>"></label>
                        </div>
                        <div class="input-box col-md-6">
                            <label class="input-name"><span>To</span><input type="text" maxlength="2" class="input-field" name="time_span_to" onkeypress="return onlyNumbers(event);" value="<?php echo $edit_job_data['end_time'] ?>" ></label>
                        </div>
                        
                        <div class="error" id="error_start_time" ></div>                            
                        <div class="error" id="error_end_time" ></div>
                        <div class="error" id="error_common" ></div>
                        
                    </div>
                    </div>

                    
                    <div class="group-job">
                    <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Reward</h2>
                        <div class="input-box">




                            <label class="input-name dis-inline"><input type="radio" <?php if($edit_job_data['job_reward'] == 1) { echo "checked"; }?>  id="cash-box" value="1" name="reward"  class="input-field pd20 dis-inline">Cash Reward</label>
                            <label class="input-name dis-inline"><input type="radio" <?php if($edit_job_data['job_reward'] == 2) { echo "checked"; }?> value="2" id="watch-me" name="reward" class="input-field pd20 dis-inline">Coupon Reward</label>
                            
                            <div class="error" id="error_reward" ></div>
                            
                            <label class="input-name">
                            <select name="coupon_value" id="coupon_value" class="input_field">

                            <?php   

                                foreach ($coupon_data as $coupon) { ?>
                                    <option value="<?php echo $coupon['Coupon']['coupon_id'] ?>" <?php if($coupon['Coupon']['coupon_id'] == $edit_job_data['coupon_id']) { echo "selected";} else  ?> ><?php echo $coupon['Coupon']['coupon_name'] ?></option>
                                 <?php   }

                            ?>

                               
                            </select></label>
                            <label class="input-name"><input value="<?php echo $edit_job_data['job_compensation']; ?>" onkeypress="return onlyNumbers(event);" type="text" name="cash_value" id="cash_value" class="input-field"></label>
                            
                            <div class="error" id="error_cash" ></div>
                            
                        </div>
                    </div>


                    <div class="group-job">
                    <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Requirements</h2>
                        <div class="input-box">
                            <label class="input-name"><span>Maximum requests</span><input name="request_number" type="text" onkeypress="return onlyNumbers(event);" value="<?php echo $edit_job_data['max_allowed_requests'] ?>" class="input-field" ></label>
                            <div class="error" id="error_request" ></div>
                            
                        </div>
                        <div class="input-box">
                            <label class="input-name"><span>Minimum required agent rank</span>
                            <select name="agent_rank" class="input-field">
                            <?php    

                                foreach ($agent_rank as $rank) { ?>
                                   <option value="<?php echo $rank['AgentRank']['rank']?>" <?php if($rank['AgentRank']['agent_rank_id'] == $edit_job_data['min_agent_rank']) { echo "selected";}  ?> ><?php echo $rank['AgentRank']['rank_title']  ?> </option>

                               <?php }

                            ?>
                              
                            </select>
                            </label>
                        </div>
                        <div class="input-box">
                            <label class="input-name dis-inline">
                           
                                <input type="checkbox"<?php if($edit_job_data['test_status'] == 1) { echo "checked"; }?>  name="test_status" class="input-field pd20 dis-inline">Test Passed
                            </label>
                        </div>
                    </div>

                    <div class="group-job">
                    <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Status</h2>
                        <div class="input-box">
                            
                            
                                    <label class="input-name dis-inline">
                                        <input type="radio"<?php if($edit_job_data['job_status'] == 1) { echo "checked"; } ?> value="1" name="job_status" class="input-field pd20 dis-inline">Active
                                    </label>

                                    <label class="input-name dis-inline">
                                        <input type="radio"  <?php if($edit_job_data['job_status'] == 0) { echo "checked"; } ?> value="0" name="job_status" class="input-field pd20 dis-inline">inactive</label>
                                 
                                <div class="error" id="error_status" ></div>
                            
                        </div>
                    </div>

                    </div><!-- right -->        
                    
                    <div class="col-md-12 text-center">
                        <input type="submit" onclick="return job_post();" value="Publish" class="btn btn-lg btn-custom btn-success">
                    </div>
                </form>
            </div>

        </div>

	
    <?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			echo $this->Html->script('bootstrap-datepicker.js');
			
	?>


	<?php /*?><!-- jQuery -->
    <script src="scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="scripts/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="scripts/bootstrap-datepicker.js"></script>
	
    <script src="scripts/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="scripts/sb-admin-2.js"></script><?php */?>
    
<script>
	
	$("#ckbCheckAll").click(function () {
            $(".checkBoxAddress").prop("checked", $(this).is(":checked"));
        });
	
	function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
		}
	
	function job_post()
	{
		var error = false;
		var reward = $("input[name='reward']:checked").val();
		var job_status = $("input[name='job_status']:checked").val();
		var start_date = $('#dpd1').val();
		var end_date = $('#dpd2').val();
		var time_span_from = $('#time_span_from').val();
		var time_span_to = $('#time_span_to').val();
		var count_address = $('#count_address').val();
		var request_number = $('#request_number').val();
                var job_overview = $('#job_overview').val();
		var location = $('[name="location[]"]:checked').length;
		
		var job_cat = $('#job_cat').val();
		var survey_list = $('#survey_list').val();
		//alert(survey_list);
		
		if(request_number=='')
		{
			$('#error_request').html('Maximum request is required.');
			error = true;
		}
		else
		{
			$('#error_request').html('');
		}
                
                if(job_overview=='')
		{
			$('#error_job_overview').html('Job overview is required.');
			error = true;
		}
		else
		{
			$('#error_job_overview').html('');
		}
		
		if(location==0)
		{
			$('#error_location').html('Location address selection is required.');
			error = true;
		}
		else
		{
			$('#error_location').html('');
		}
		
		if(start_date=='')
		{
			$('#error_start').html('Start date is required.');			
			error = true;
		}
		else
		{
			$('#error_start').html('');
		}
		
		if(end_date=='')
		{
			$('#error_end').html('End date is required.');			
			error = true;
		}
		else
		{
			$('#error_end').html('');
		}
		
		
		if(survey_list==0)
		{
			$('#error_survey').html('Survey selection is required.');			
			error = true;
		}
		else
		{
			$('#error_survey').html('');
		}
		
		if(job_cat==0)
		{
			$('#error_category').html('Job category selection is required.');
			error = true;
		}
		else
		{
			$('#error_category').html('');
		}
		
		if(time_span_from=='')
		{
			$('#error_start_time').html('Time span from is required.');
			error = true;
		}
		else
		{
			$('#error_start_time').html('');
		}
		
		if(time_span_to=='' || time_span_to==0)
		{
			$('#error_end_time').html('Time span To is required or should not be Zero.');
			error = true;
		}
		else
		{
			$('#error_end_time').html('');
		}
		
		if(time_span_from!='' && time_span_to!='')
		{
			
			if(time_span_to-time_span_from<=0)
			{
				$('#error_common').html('Time span from should be less than Time span To.');
				error = true;
			}
			else if(time_span_to>=24 || time_span_from>=24 )
			{
				$('#error_common').html('Time span from and Time span To should be between 0 and 23.');
				error = true;
			}
			else
			{
				$('#error_common').html('');
			}
		}
		else
		{
			$('#error_common').html('');
		}
		
		if(reward===undefined)
		{
			$('#error_reward').html('Reward selection is required.');
			error = true;
		}
		else if(reward==1)
		{
			var cash_value = $('#cash_value').val();
			if(cash_value=='' || cash_value==0)
			{
				$('#error_cash').html('Cash value is required for cash reward or must be greater than Zero.');
				$('#error_reward').html('');
				error = true;
			}
			else
			{
				$('#error_cash').html('');
			}
		}
		else if(reward==2)
		{
			var coupon_value = $('#coupon_value').val();
			if(coupon_value=='')
			{
				$('#error_cash').html('Coupon selection is required for coupon reward.');
				$('#error_reward').html('');
				error = true;
			}
			else
			{
				$('#error_cash').html('');
			}
		}
		else
		{
			$('#error_reward').html('');
		}
		
		if(job_status===undefined)
		{
			$('#error_status').html('Please select status.');			
			error = true;
		}
		else
		{
			$('#error_status').html('');
		}
		
		if(error)
		{
			return false;
		}
	}
	
    if (top.location != location) {
    top.location.href = document.location.href ;
  }
        $(function(){
            window.prettyPrint && prettyPrint();
            $('#dp1').datepicker({
                format: 'dd-mm-yyyy'
            });
            $('#dp2').datepicker({
            format: 'dd-mm-yyyy'
      });
            
            

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
        });

<?php  

    if($edit_job_data['job_reward'] == 1) { ?>

        $(document).ready(function() {
     $('#coupon_value').hide();
}); <?php } else { ?>
             $(document).ready(function() {
     $('#cash_value').hide();
     });
        <?php }

?>

    
   $('input[name=reward]').click(function () {

    if (this.id == "watch-me") {
        $("#coupon_value").show('slow');
        $('#cash_value').hide('slow');
    } else if(this.id == "cash-box" ) {
        $("#coupon_value").hide('slow');
        $("#cash_value").show('slow');
    }
    
});



</script>

    
</body>
</html>