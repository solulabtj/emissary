<?php

echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->css('jquery.dataTables.min');
?>
<script>
    $(document).ready(function () {

        $('#settelmentTable').DataTable({"bSort": false});

    });
</script>
<style>
.table-custom .table>thead>tr>th{
            text-align: left !important;
    }  
    
 .table-custom .table>tbody>tr>td{
            text-align: left !important;
    }     
</style>
<?php //echo "<pre>"; print_r($res); exit; ?>
<div class="clearfix"></div>
<div class="table-custom shadow">
    <table id="settelmentTable" class="table text-center">
        <thead>
            <tr>
                <th>Job ID</th>
                <th>Agent Name</th>                        
                <th>Completed</th>                
                <th>Submitted On</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($res as $job_list) { ?>  
            <?php 
                        $accepted_by_client  = $job_list['agent_jobs']['accepted_by_client'];
                        $declined_by_client  = $job_list['agent_jobs']['declined_by_client'];
                        $completed_by_agent  = $job_list['agent_jobs']['completed_by_agent'];
                        $cancelled_by_agent  = $job_list['agent_jobs']['cancelled_by_agent'];
                        $disapproved_by_client = $job_list['agent_jobs']['disapproved_by_client'];
                        $approved_by_client  = $job_list['agent_jobs']['approved_by_client'];
                        
                        if($accepted_by_client == "1" && $declined_by_client == "0" && $completed_by_agent =="1" && $cancelled_by_agent=="0" && $disapproved_by_client == "0" && $approved_by_client == "0"){
                            $job_status = "Pending";                                                       
                        }
                        else if($accepted_by_client == "1" && $declined_by_client == "0" && $completed_by_agent =="1" && $cancelled_by_agent=="0" && $disapproved_by_client == "0" && $approved_by_client == "1"){
                            $job_status = "Accepted";                            
                        }
                        else if($accepted_by_client == "1" && $declined_by_client == "0" && $completed_by_agent =="1" && $cancelled_by_agent=="0" && $disapproved_by_client == "1" && $approved_by_client == "0"){
                            $job_status = "Declined";
                        }
                        else{
                            $job_status = "no";
                        }
                        
                        if($job_status == "no"){
                            continue;
                        }
                        
                        if($job_list['agent_jobs']['job_start_time'] != "0000-00-00 00:00:00"){ $date = date("d-m-Y",strtotime($job_list['agent_jobs']['job_start_time']));}else{ $date = "";}
                  
            ?>
        <td><?php echo $job_list['agent_jobs']['job_id']; ?></td>
        <td><?php echo $job_list['agent_user_infos']['agent_name']; ?></td>
        <td><?php echo $job_list['agent_jobs']['percentage_earned']."%"; ?></td>
        <td><?php echo $date; ?></td>
        <td><?php echo $job_status; ?></td>       
        </tr>
         <?php }  ?>
        </tbody>
    </table>
    <footer class="table-footer">
        <div class="row">                   
        </div>
    </footer>
</div>
<?php exit(); ?>