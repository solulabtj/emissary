<style>
	
	.error
	{  color:#F00;  }

</style>

    <div id="page-wrapper">
        <div class="row">
            <h1 class="page-header">Edit Location</h1>
            <form method="post" action="<?php echo WEBSITE_ROOT.'clients/edit_address' ?>">
            <input type="hidden" name="id" value="<?php echo $get_list_address['ClientAddress']['client_address_id']; ?>">
                <div class="col-md-6"> 
                    <div class="input-box">
                       <label class="input-name"><span>Company Name</span>
                                <input type="text" value="<?php echo $get_list_address['ClientAddress']['address']; ?>" name="location" class="form-control addrs" id="address"/></label>
                            
                        </div>
                        
                         <div class="input-box">
                       <label class="input-name"><span>Postal Code</span>
                                <input type="text" value="<?php echo $get_list_address['ClientAddress']['postal_code']; ?>" name="postal_code" id="postal_code" onkeypress="return onlyNumbers(event);" placeholder = "Postal code" class="form-control" /></label>
                                
                                <div id="error_postal" class="error" ></div>
                            
                        </div>
                        
                        <div class="input-box">
                       <label class="input-name"><span>Conatct Number</span>
                                <input type="text" value="<?php echo $get_list_address['ClientAddress']['phone_number']; ?>" name="contact_number" id="contact_number" onkeypress="return onlyNumbers(event);" placeholder = "Contact Number" class="form-control" /></label>
                                
                                <div id="error_contact" class="error" ></div>
                               
                        </div>
                        <!--<div class="form-group">
                            <label class="col-sm-2 control-label">Radius:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control rad" id="radius"/>
                            </div>
                        </div>-->
                        <div id="us3"  class="map" style="width: 550px; height: 400px;"></div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="m-t-small">
                            
                                <div class="col-sm-3">
                                    <input type="hidden" value="<?php echo $get_list_address['ClientAddress']['lattitude']; ?>" name="lattitude" class="form-control latt" style="width: 110px" id="lat"/>
                                </div>
                            
                            <div class="col-sm-3">
                                <input type="hidden" value="<?php echo $get_list_address['ClientAddress']['longitude']; ?>" name="longitude" class="form-control long" style="width: 110px" id="lon"/>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="clearfix"></div>
                <button type="submit" onclick="return validate();" class="btn btn-lg btn-custom btn-success">Edit Loaction</button>
                
            </form>
        </div>
    </div>                  
                    
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<?php 
    echo $this->Html->script('locationpicker.jquery.min');
?>
<script type="text/javascript">

	function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
		}
	
	function validate()
    {
          var error = false;
		  
		  var postal_code = $('#postal_code').val();
		  
		  var contact_number = $('#contact_number').val();
		            
		  if(postal_code=='')
          {
            $('#error_postal').html('Please Enter Postal code.');
            error = true;
          }		  
          else
          {
            $('#error_postal').html('');
          }
		  
		  if(contact_number=='')
          {
            $('#error_contact').html('Please Enter Contact No.');
            error = true;
          }	
		  else if(contact_number.length>25)
          {
            $('#error_contact').html('Contact No must be less than 25 digits.');
            error = true;
          }	 
          else
          {
            $('#error_contact').html('');
          }
		  
          if(error)
          {
               return false; 
          }		  		 
		  
    }
	
    var lat = 23.0300;
    var lng = 72.5800;  
    $(".map").locationpicker({
        location: {latitude: lat, longitude: lng},
        radius: 300,
        inputBinding: {
            latitudeInput: $('.latt'),
            longitudeInput: $('.long'),
            radiusInput: $('.rad'),
            locationNameInput: $('.addrs')
        },
        enableAutocomplete: true,
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            // Uncomment line below to show alert on each Location Changed event
            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
        }
    });



</script>

</body>
</html>