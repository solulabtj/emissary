<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
		
	function validate_settings()
	{
		var error = false;
            
			var file = $('input[type="file"]').val();
			
			var survey_category_title = $('#survey_category_title').val();
			
			if(survey_category_title=='')
			{
				$('#error_title').html('Survey Category Title is required.');
				error = true;
			}			
			else
			{
				$('#error_title').html('');
			}
			
			if(file=='')
			{
				$('#error_image').html('Survey Category image is required.');
				error = true;
			}			
			else
			{
				$('#error_image').html('');
			}
			
            if (error) {
                return false;
            }
	}
		
		function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
		}		
		
		
		function lettersOnly(evt) {
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		  ((evt.which) ? evt.which : 0));
		if (charCode == 32)
			return true;
		if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		  (charCode < 97 || charCode > 122)) {
			return false;
		}
		else
			return true;
	}
	
	function showimagepreview() {		
			var ext = $('#survey_category_image').val().split('.').pop().toLowerCase();
			if($.inArray(ext,['gif','png','jpg','jpeg']) == -1) {
				$('#survey_category_image').val("");
				alert('invalid file type! Please Select only image.');
				return false;
			}		
        }

</script>

<style>
    .error{
        color: red;
    }
</style>

<div id="page-wrapper">
    <div class="row">
	    <h1 class="page-header">Add Survey Category</h1>
	    <div class="center" align="center"><?php echo $this->Session->flash(); ?></div>
		<form method="post" enctype="multipart/form-data" action="<?php echo WEBSITE_ROOT.'clients/category_add'; ?>">
	    	
	       	<div id="apply_formdata" class="col-md-6">  
	            
	    		    
	                <div class="input-box">
	        			<label class="input-name"><span>Survey Category Title</span></label>
	                	<input type="text" name="survey_category_title" id="survey_category_title" onkeypress="return lettersOnly(event)"  />
	    			</div>
	            	<div id="error_title" class="error"></div>
                    
	            	<div class="input-box">
	        			<label class="input-name"><span>Survey Category Image</span></label>
	                	<input type="file" name="survey_category_image" id="survey_category_image" onchange="showimagepreview();"  />
	    			</div>
	            	<div id="error_image" class="error"></div>
	              	
	        	
	   		</div>
        	<div class="clearfix"></div>
        	<button type="submit" onclick="return validate_settings();" class="btn btn-lg btn-custom btn-success">Add Category</button>
        	
		</form>
   	</div>
</div>



	<?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			
	?>
	
    <script type="text/javascript">
	
		$(document).ready(function(){
			
			});

	</script>

	
</body>
</html>