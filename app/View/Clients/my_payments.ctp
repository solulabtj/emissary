<script>
    $(document).ready(function () {

        $('#payTable').DataTable({"bSort": false});

    });
</script>

<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">My Payments</h1>

        <div class="clearfix"></div>

        <?php echo $this->Session->flash(); ?>
        <br/><br/>
        <div class="table-custom shadow">
            <table id="payTable" class="table text-center">
                <thead>
                    <tr>
                        <th>Subscription For</th>
                        <th>Transaction Id</th>
                        <th>Transaction Status</th>                        
                        <th>Amount</th>
                        <th>Paid On</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($subs as $sub) {
                        ?>
                        <tr>
                            <td><?php  echo date("F Y",strtotime("-30 days",strtotime($sub['ClientSubscription']['created']))); ?></td>
                            <td><?php if($sub['ClientSubscription']['transaction_id'] != "00000000"){echo $sub['ClientSubscription']['transaction_id'];}else{ echo "Payment Due";} ?></td>
                            <td><?php echo $sub['ClientSubscription']['transaction_status']; ?></td>
                            <td><?php echo $sub['ClientSubscription']['amount_paid']." KWD"; ?></td>
                            <td><?php if($sub['ClientSubscription']['transaction_time'] != "0000-00-00 00:00:00"){echo $sub['ClientSubscription']['transaction_time'];}else{echo "Payment Due";} ?></td>
                            <td>
                                <?php if($sub['ClientSubscription']['transaction_status'] == "pending"){ ?>
                                    <a href="<?php echo WEBSITE_ROOT . 'clients/tap/' . $sub['ClientSubscription']['amount_paid']; ?>">Pay Now</a>
                                <?php }else{ 
                                    echo "Paid";
                                }
                                ?>                                
                            </td>
                        </tr>

                    <?php } ?>                   
                </tbody>
            </table>
            <footer class="table-footer">
                <div class="row">                    
                </div>
            </footer>
        </div>
    </div>
</div>
</body>
</html>