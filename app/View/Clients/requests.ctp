<script>
    $(document).ready(function(){
     
        $('#requestTable').DataTable({"bSort": false});

    });
</script>

        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Job Requests</h1>
                
                <div class="clearfix"></div>
                
                <?php echo $this->Session->flash(); ?>

                <div class="table-custom shadow">
                    <table id="requestTable" class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-user"></i>Agent Name</th>
                                <th><i class="fa fa-th-list"></i>Job Id</th>
                                <th><i class="fa fa"></i>Agent Rank</th>
                                <th><i class="fa fa-book"></i>Requested</th>
                                <th><i class="fa fa-hourglass-half"></i>Status</th>
                                <th><i class="fa fa-bullhorn"></i>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            //pr($request_list);exit;
                                foreach ($request_list as $list) { 
                               

                                    if($list['AgentJob']['accepted_by_client'] == 0 && $list['AgentJob']['declined_by_client'] == 0)
                                    {
                                        $request_status = 'Pending';
                                        $actions = "<a href='".WEBSITE_ROOT."clients/change_request_status/".$list['AgentJob']['agent_job_id']."/A'>Accept<a>"." "."<a href='".WEBSITE_ROOT."clients/change_request_status/".$list['AgentJob']['agent_job_id']."/D'>Decline<a>";
                                    }
                                    elseif ($list['AgentJob']['accepted_by_client'] == 0 && $list['AgentJob']['declined_by_client'] == 1) {
                                        $request_status = 'Declined';
                                        $actions = "<a href='".WEBSITE_ROOT."clients/change_request_status/".$list['AgentJob']['agent_job_id']."/A'>Accept<a>";
                                    }
                                    elseif ($list['AgentJob']['accepted_by_client'] == 1 && $list['AgentJob']['declined_by_client'] == 0) {
                                        $request_status = 'Accepted';
                                        $actions = "<a href='".WEBSITE_ROOT."clients/change_request_status/".$list['AgentJob']['agent_job_id']."/R'>Remove from Agent<a>";
                                    }

                                    ?>
                                <tr>
                                    <td><?php echo $list['AgentUserInfo']['agent_name'];?></td>
                                    <td><?php echo $list['AgentJob']['job_id'];?></td>
                                    <td><?php echo $list['AgentUserInfo']['agent_rank_title'];?></td>
                                    <td>
																		
                                    <?php 
							
									 //$now = time(); 
									 //$your_date = strtotime($list['AgentJob']['created']);
									 //$datediff = $now - $your_date;
									 //echo floor($datediff/(60*60*24)).' days ago';
									 echo date('m-d-Y',strtotime($list['AgentJob']['created']));
									
									?>
                                    
                                    </td>	
                                    <td><?php echo $request_status;?></td>
                                    <td><?php echo $actions;?></td>
                                </tr>
                                    
                                <?php }

                            ?>
                           
                        </tbody>
                    </table>
                    
                </div>
                
            </div>

        </div>


</body>
</html>