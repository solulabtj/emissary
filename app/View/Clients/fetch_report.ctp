<?php

echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->css('jquery.dataTables.min');
?>
<script>
    $(document).ready(function () {

        $('#settelmentTable').DataTable();

    });
</script>
<style>
.table-custom .table>thead>tr>th{
            text-align: left !important;
    }  
    
 .table-custom .table>tbody>tr>td{
            text-align: left !important;
    }     
</style>
<?php //echo "<pre>"; print_r($res); exit; ?>
<div class="clearfix"></div>
<div class="table-custom shadow">
    <table id="settelmentTable" class="table text-center">
        <thead>
            <tr>
                <th>Agent Name</th>
                <th>Agent Email</th>                        
                <th>Agent Rank</th>
                <th>Applied Jobs</th>
                <th>Completed Jobs</th>
                <th>Test Status</th>
            </tr>
        </thead>
        <tbody>
                    <?php foreach ($res as $job_list) { ?>
                    <?php  
                        $rank_id  = $job_list['aui']['agent_rank']; 
                        if($rank_id == "1"){
                            $rank = "Pro";
                        }
                        if($rank_id == "2"){
                            $rank = "Expert";
                        }
                        if($rank_id == "3"){
                            $rank = "Advanced";
                        }
                        if($rank_id == "4"){
                            $rank = "Normal";
                        }
                        if($rank_id == "5"){
                            $rank = "Beginner";
                        }
                        if($rank_id >= "6"){
                            $rank = "Beginner";
                        }
                        
                         if($rank_id == "0"){
                            $rank = "Beginner";
                        }
                        
                        if($job_list['aui']['test_passed'] == "1"){
                            $test = "Passed";
                        }else{
                            $test = "Failed";
                        }
                        
                      
                    ?>
            <tr>
                <td><?php echo $job_list['aui']['agent_name']; ?></td>
                <td><?php echo $job_list['aui']['email_address']; ?></td>
                <td><?php echo $rank; ?></td>
                <td><?php echo $job_list[0]['applied_count']; ?></td>
                <td><?php echo $job_list[0]['completed_count']; ?></td>
                <td><?php echo $test; ?></td>
            </tr>
                 <?php }  ?>
        </tbody>
    </table>
    <footer class="table-footer">
        <div class="row">                   
        </div>
    </footer>
</div>
<?php exit(); ?>