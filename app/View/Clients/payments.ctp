<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="width: 425px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pay By Coupon</h4>
            </div>
            <form method="post" action="<?php echo WEBSITE_ROOT . "clients/payByCoupon" ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" name="coupon_id" id="hid">
                    <input type="hidden" name="job_id" id="jid">
                    <input type="hidden" name="agent_id" id="haid">
                    <input type="hidden" name="agent_job_id" id="ajid">
                    <input type="hidden" name="price" id="hprice">
                    <table >
                        <tr>
                            <td id="label" align="center">
                                Coupon Price 
                            </td>
                            <td>
                                <span id="price" style="margin-left: -175px;"></span><br>
                            </td>
                        </tr>
                        <tr>
                            <td id="label" align="center">
                                Coupon Name 
                            </td>
                            <td id="content">
                                <input type="text" name="coupon_name" id="coupon_name"><br>
                                <div class="error" id="error_name"></div><br>
                            </td>
                        </tr>
                        <tr>
                            <td id="label" align="center">
                                Coupon Description 
                            </td>
                            <td id="content">
                                <input type="text" id="coupon_des" name="coupon_description"><br>
                                <div class="error" id="error_des"></div> <br>
                            </td>
                        </tr>
                        <tr>
                            <td id="label" align="center">
                                Coupon Expiry Date 
                            </td>
                            <td id="content">
                                <input type="text" name="coupon_expiration_date" id="coupon_expiration_date" readonly="readonly"><br>
                                <div class="error" id="error_expdate"></div><br>
                            </td>
                        </tr>
                        <tr>
                            <td id="label" align="center">
                                Coupon Image 
                            </td>
                            <td id="content">
                                <input type="file" id="coupon_image" name="coupon_image" onchange="chkCouponImage(this)">
                                <div class="error" id="error_img"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button  class="btn-custom1 create-job" type="submit" onclick="return validate();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Payments</h1>
        <div class="jobpost-top">
            <div class="col-md-4">                
                <?php echo $this->Session->flash(); ?>
            </div>
            <div class="col-md-5 filter-result-info">
                <!--<span class="ng-binding">
                    Showing 25/25 entries
                </span>   -->           
            </div>
            <div class="successmsg" align="center"><?php echo $this->Session->flash(); ?></div>
            <div class="col-md-3">
                <!--<?php //echo $this->Html->link('Add Address', array('action' =>'add_address'),array( 'class' => 'btn-custom1 create-job pull-right'));                   ?>  -->
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="table-custom shadow">
            <table id="settelmentTable" class="table text-center">
                <thead>
                    <tr>
                        <th>Agent</th>
                        <th>Job ID</th>
                        <th>Completed %</th>                                
                        <th>Submitted</th>
                        <th>Payment Status</th>
                        <th>Reward</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listings as $listing) { ?>
                        <tr>
                            <td><?php echo $listing['AgentUserInfo']['agent_name']; ?></td>
                            <td><?php echo $listing['Job']['job_id']; ?></td>                                    <td><?php echo $listing['AgentJob']['percentage_earned']; ?></td>

                            <td>
                                <?php
                                if ($listing['AgentJob']['job_end_time'] != '0000-00-00 00:00:00' && $listing['AgentJob']['job_end_time'] != null) {
                                    $now = time();
                                    $your_date = strtotime($listing['AgentJob']['job_end_time']);
                                    $datediff = $now - $your_date;
                                    echo floor($datediff / (60 * 60 * 24)) . ' days ago';
                                }
                                ?>
                            </td>

                            <td>
                                <div id="paid_unpaid<?php echo $listing['AgentJob']['agent_job_id']; ?>" >
                                    <?php
                                    if ($listing['AgentJob']['is_paid'] == '1') {
                                        echo "Paid";
                                    } else if ($listing['AgentJob']['is_paid'] == '0') {
                                        echo "Unpaid";
                                    }
                                    ?>
                                </div>
                            </td>

                            <td>
                                <?php
                                if ($listing['Job']['reward_type'] == '1') {
                                    echo $listing['Job']['job_compensation'];
                                } else if ($listing['Job']['reward_type'] == '2') {
                                    foreach ($coupon_name as $coupon_names) {
                                        echo $coupon_names[$listing['AgentJob']['agent_job_id']];
                                    }
                                }
                                ?>
                            </td>

                            <td>
                                <?php
                                if ($listing['AgentJob']['is_paid'] == '1') {
                                    echo "Paid";
                                } else if ($listing['AgentJob']['is_paid'] == '0') {
                                    ?>
                                    <div id="status_<?php echo $listing['AgentJob']['agent_job_id']; ?>" ></div>

                                    <?php
                                    if ($listing['Job']['reward_type'] == '1') { ?>
                                       <a id="status_old<?php echo $listing['AgentJob']['agent_job_id']; ?>" href="javascript:;" onclick="mark_as_paid_cash('<?php echo $listing['AgentJob']['agent_job_id']; ?>', '<?php echo $listing['Job']['reward_type']; ?>', '<?php echo $listing['Job']['job_compensation']; ?>', '<?php echo $listing['AgentJob']['coupon_id']; ?>');">Mark As Paid</a>
                                    <?php } else if ($listing['Job']['reward_type'] == '2') { ?>
                                     <a id="status_old<?php echo $listing['AgentJob']['agent_job_id']; ?>" href="javascript:;" onclick="mark_as_paid('<?php echo $listing['AgentJob']['agent_job_id']; ?>', '<?php echo $listing['Job']['reward_type']; ?>', '<?php echo $listing['Job']['job_compensation']; ?>', '<?php echo $listing['AgentJob']['coupon_id']; ?>', '<?php echo $listing['coupons']['coupon_price']; ?>', '<?php echo $listing['AgentJob']['agent_id']; ?>', '<?php echo $listing['Job']['job_id']; ?>');">Mark As Paid</a>
                                   <?php  }
                                    ?>                                  
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>
            <footer class="table-footer">
                <div class="row">                 
                </div>
            </footer>
        </div>
    </div>
</div>
</body>
<?php
echo $this->Html->script('jquery.min');
echo $this->Html->script('bootstrap.min');
echo $this->Html->script('metisMenu.min');
echo $this->Html->script('sb-admin-2');
echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->css('jquery.dataTables.min');
echo $this->Html->script('fh');
echo $this->Html->css('fh');
echo $this->Html->script('bootstrap-datepicker.js');
?>
<style>
    .datepicker{z-index: 99999 !important;}
    .error{color: red; float: right;}
    #label{ display:inline-block;width: 50%; }
    #content{display:inline-block;width: 50%;float: right;}
</style>
<script>
    $(document).ready(function () {

        // $('#settelmentTable').DataTable();

        var table = $('#settelmentTable').DataTable({
            responsive: true
        });
        new $.fn.dataTable.FixedHeader(table);
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        $('#coupon_expiration_date').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            $('#coupon_expiration_date').datepicker('hide');
        });
    });

    function chkCouponImage(input) {

        var ext = $('#coupon_image').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'pdf']) == -1) {
            $('#coupon_image').val("");
            alert('Invalid file type! Please Select only image.');
            return false;
        }

    }

    function mark_as_paid(agent_job_id, reward_type, job_compensation, coupon_id, coupon_price, agent_id, job_id)
    {


        $("#price").text(coupon_price + " KWD");
        $("#hprice").val(coupon_price);
        $("#hid").val(coupon_id);
        $("#ajid").val(agent_job_id);
        $("#jid").val(job_id);
        $("#haid").val(agent_id);
        $('#myModal').modal('show');
    }



    function mark_as_paid_cash(agent_job_id, reward_type, job_compensation, coupon_id)
    {
        $.ajax({
            type: 'POST',
            data: 'agent_job_id=' + agent_job_id + '&reward_type=' + reward_type + '&job_compensation=' + job_compensation + '&coupon_id=' + coupon_id,
            url: '<?php echo WEBSITE_ROOT . 'clients/mark_as_paid' ?>',
            success: function (data)
            {
                if (data == 1)
                {
                    $('#status_' + agent_job_id).html('Paid');
                    $('#paid_unpaid' + agent_job_id).html('Paid');
                    $('#status_old' + agent_job_id).hide();
                    alert('Mark as paid Successfully.');
                } else
                {
                    alert('Unable to mark as paid.Please Try again later.');
                }
            }
        });
    }

    function validate()
    {
        var error = false;

        var coupon_name = $.trim($('#coupon_name').val());
        var coupon_des = $.trim($('#coupon_des').val());
        var coupon_image = $.trim($('#coupon_image').val());
        var exp_date = $.trim($('#coupon_expiration_date').val());

        if (coupon_name == '')
        {
            $('#error_name').html('Coupon name is required.');
            error = true;
        } else
        {
            $('#error_name').html('');
        }
        if (coupon_des == '')
        {
            $('#error_des').html('Coupon description is required.');
            error = true;
        } else
        {
            $('#error_des').html('');
        }
        if (coupon_image == '')
        {
            $('#error_img').html('Coupon image is required.');
            error = true;
        } else
        {
            $('#error_img').html('');
        }

        if (exp_date == '')
        {
            $('#error_expdate').html('Coupon expiry date is required.');
            error = true;
        } else
        {
            $('#error_expdate').html('');
        }
        if (error)
        {
            return false;

        }
    }

</script>


</html>