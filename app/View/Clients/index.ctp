<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/custom-style.css"/>
<?php
// pr($locaSubmission);exit;
$approvedJobs = $submissionChart[0][0]['approvedJobs'];
$disapprovedJobs = $submissionChart[0][0]['disapprovedJobs'];
$pendingJobs = $submissionChart[0][0]['pendingJobs'];

$currentJobJson = json_encode($currentJobChart, JSON_NUMERIC_CHECK);

$locationJson = json_encode($locSubmission, JSON_NUMERIC_CHECK);

//echo $locationJson;exit;
//pr($submissionList);exit;
$completedJobs = $jobChart[0][0]['completedJobs'];
$inProgress = $jobChart[0][0]['inProgress'];
$futureJobs = $jobChart[0][0]['futureJobs'];

$styleArr = array();
$styleArr['role'] = 'style';
//pr($styleArr);exit;
$elem = array('Agent Name', 'Points Earned', $styleArr);
$agentLead = array();
array_push($agentLead, $elem);

foreach ($leaderboard as $key => $lead) {
    $elemPush = array();

    $agentPoint = (int) $lead[0]['totalPoints'];

    array_push($elemPush, $lead['ui']['agent_name']);
    array_push($elemPush, $agentPoint);
    array_push($elemPush, 'red');
    array_push($agentLead, $elemPush);
}
//pr($agentLead);

$agentLeadJson = json_encode($agentLead, true);
//echo $agentLeadJson; exit;
?>
<div class="modal fade" id="myModal" role="dialog" data-keyboard="false"  data-backdrop="static" style="z-index:999999;">
                                                                    <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                                                                <h4 class="modal-title">Your subscription period is expired!!</h4>
                                                                            </div>
                                                                            <div class="modal-body" >
                                                                                <p>Please make payment for below shown amount to continue using Emissary services.</p>
                                                                                <b><?php echo $amount." KWD"; ?></b>
                                                                                <br><br>
                                                                                <div style="padding-left: 100px;">
                                                                                <button type="button" class="btn btn-default"  onclick="subs('tap');">Pay Now</button>
                                                                                <button type="button" class="btn btn-default"  onclick="subs('logout');">Logout</button>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">                                                                                
                                                                                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

<div id="page-wrapper">

    <div class="row">
        <div class="col-md-8">

            <div class="pd300">
                <h4 class="md_title">New Job Requests</h4>
                <div class="errClass" align="center"></div>
                <table id="" class="responsive-table bordered custom hoverable display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Agent Name</th>
                            <th>Job ID</th>
                            <th>Requested</th>
                            <th>Actions</th>
                        </tr>
                    </thead>               
                    <tbody>
                        <?php
                        foreach ($requestList as $key => $request) {
                            $accept = "A";
                            $decline = "D";
                            ?>
                            <tr id = "request_<?php echo $request['AgentJob']['agent_job_id'] ?>">
                                <td><?php echo $request['AgentUserInfo']['agent_name']; ?></td>
                                <td><?php echo $request['AgentJob']['job_id']; ?></td>
                                <td>
                                    <?php
                                    $now = time();
                                    $your_date = strtotime($request['AgentJob']['created']);
                                    $datediff = $now - $your_date;
                                    echo floor($datediff / (60 * 60 * 24)) . ' days ago';
                                    ?>
                                </td>
                                <td><?php
                                    if ($request['AgentJob']['accepted_by_client'] == 0 && $request['AgentJob']['declined_by_client'] == 0) {
                                        $request_status = 'Pending';
                                        ?>
                                        <a href='javascript:changeRequestStatus("<?php echo $request['AgentJob']['agent_job_id'] ?>","<?php echo $accept ?>");'>Accept<a><a href='javascript:changeRequestStatus("<?php echo $request['AgentJob']['agent_job_id'] ?>","<?php echo $decline ?>");'>Decline<a>
                                                        <?php
                                                    } elseif ($request['AgentJob']['accepted_by_client'] == 0 && $request['AgentJob']['declined_by_client'] == 1) {
                                                        $request_status = 'Declined';
                                                        ?>
                                                        <a href='javascript:changeRequestStatus("<?php echo $request['AgentJob']['agent_job_id'] ?>","<?php echo $accept ?>");'>Accept<a>
                                                                <?php
                                                            } elseif ($request['AgentJob']['accepted_by_client'] == 1 && $request['AgentJob']['declined_by_client'] == 0) {
                                                                $request_status = 'Accepted';
                                                                ?>
                                                                <a href='javascript:changeRequestStatus("<?php echo $request['AgentJob']['agent_job_id'] ?>","<?php echo $decline ?>");'>Remove from Agent<a>
                                                                        <?php
                                                                    }
                                                                    ?></td>                   
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                                </tbody>
                                                                </table>
                                                                <div class="btn custom-green" style = "float:right; margin-top:10px;">
                                                                    <a href="<?php echo WEBSITE_ROOT ?>clients/requests">View All</a>
                                                                </div>
                                                                </div>

                                                                <div class="pd300">
                                                                    <h4 class="md_title">New Job Submissions</h4>
                                                                    <table id="" class="responsive-table bordered custom hoverable display" cellspacing="0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Agent Name</th>
                                                                                <th>JobID</th>
                                                                                <th>Completed (%)</th>
                                                                                <th>Submitted</th>
                                                                                <th>Actions</th>
                                                                            </tr>
                                                                        </thead>               
                                                                        <tbody>
                                                                            <?php
                                                                            foreach ($submissionList as $submission) {
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?php echo $submission['AgentUserInfo']['agent_name']; ?></td>
                                                                                    <td><?php echo $submission['AgentJob']['job_id']; ?></td>
                                                                                    <td><?php echo $submission['AgentJob']['percentage_earned']; ?></td>
                                                                                    <td>
                                                                                        <?php
                                                                                        $now = time();
                                                                                        $your_date = strtotime($submission['AgentJob']['created']);
                                                                                        $datediff = $now - $your_date;
                                                                                        echo floor($datediff / (60 * 60 * 24)) . ' days ago';
                                                                                        ?>
                                                                                    </td>    
                                                                                    <td><a href="<?php echo WEBSITE_ROOT . 'clients/view_answer/' . $submission['AgentJob']['agent_job_id'] . '/' . $submission['AgentJob']['agent_id'] ?>">View Answers</a></td>                   
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="btn custom-green" style = "float:right; margin-top:10px;">
                                                                        <a href="<?php echo WEBSITE_ROOT ?>clients/job_submission">View All</a>
                                                                    </div>
                                                                </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="pd300">
                                                                        <h4 class="md_title">What's Happening</h4>
                                                                        <ul class="activity_notification">
                                                                            <?php
                                                                            foreach ($displayMsgs as $msg) {
                                                                                if ($row['AgentUserInfo']['last_login'] != "0000-00-00 00:00:00" && $row['AgentUserInfo']['last_login'] != "")
                                                                                    $msgTime = date("m-d-Y", strtotime($row['AgentUserInfo']['last_login']));
                                                                                ?>
                                                                                <li>
                                                                                    <div class="activity">
                                                                                        <?php echo $msg['ClientDashMsg']['display_msg']; ?>
                                                                                    </div>
                                                                                    <span class="activity_time"><i class="mdi-action-schedule"></i><?php echo $msgTime; ?></span>
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                </div>



                                                                <div class="row">

                                                                    <div id="location_submissions" style="width: 50%; height: 500px;"></div>
                                                                    <div id="total_submissions" style="width: 50%; height: 500px; "></div>
                                                                    <div id="current_job_performance" style="width: 50%;"></div>
                                                                       
                                                                </div>
                                                                </div>                                                                                                                                

                                                                <script type="text/javascript">
                                                                    
                                                                     $(window).load(function(){
                                                                         
                                                                         var pop = '<?php echo $popup; ?>';
                                                                         
                                                                         if(pop == 1){
                                                                            $('#myModal').modal('show');
                                                                        }
                                                                     });
                                                                    
                                                                    function subs(val){
                                                                        
                                                                        if(val == "logout"){
                                                                            window.location.href = "<?php echo WEBSITE_ROOT.'users/logout'; ?>";
                                                                        }else{
                                                                            window.location.href = "<?php echo WEBSITE_ROOT.'clients/tap'; ?>";
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                    function changeRequestStatus(agent_job_id, activity) {

                                                                        $.ajax({
                                                                            type: "POST",
                                                                            dataType: "html",
                                                                            url: "<?php echo WEBSITE_ROOT . 'clients/change_request_status_from_dash'; ?>",
                                                                            data: {agent_job_id: agent_job_id, activity: activity},
                                                                            success: function (data) {
                                                                                if (data == 1)
                                                                                {
                                                                                    location.reload();
                                                                                } else
                                                                                {
                                                                                    $('#responseError').html("Unable to process your request.");
                                                                                }
                                                                            }

                                                                        });
                                                                        event.preventDefault();
                                                                        return false; //stop the actual form post !important!
                                                                    }
                                                                </script>

                                                                <script type="text/javascript">



                                                                    $(document).ready(function (e) {
                                                                        var approvedJobs = 0;
                                                                        var disapprovedJobs = 0;
                                                                        var pendingJobs = 0;
                                                                        var inProgress = 0;
                                                                        var futureJobs = 0;
                                                                        var agentLead = '';

                                                                        approvedJobs = <?php echo $approvedJobs; ?>;
                                                                        disapprovedJobs = <?php echo $disapprovedJobs; ?>;
                                                                        pendingJobs = <?php echo $pendingJobs; ?>;

                                                                        currentJobs = <?php echo $currentJobJson; ?>;
                                                                        locationJson = <?php echo $locationJson; ?>;
                                                                        // google.charts.load("current", {packages:["corechart"]});
                                                                        google.charts.load('current', {'packages': ['bar', 'corechart']});
                                                                        if (currentJobs != null) {
                                                                            google.charts.setOnLoadCallback(currentJobPerformance);
                                                                        } else {
                                                                            $("#current_job_performance").html("No Data Found For Current Job Peformance");
                                                                        }
                                                                        google.charts.setOnLoadCallback(locationSubmissions);
                                                                        google.charts.setOnLoadCallback(totalSubmissions);




                                                                        function currentJobPerformance() {
                                                                            var data = new google.visualization.DataTable();
                                                                            data.addColumn('number', 'X');

                                                                            firstElem = 0;
                                                                            firstArr = [0];
                                                                            currJobArr = [];
                                                                            currSubArr = [];
                                                                            job_ids = [];
                                                                            nextArr = [];
                                                                            old_days = [];
                                                                            column = [];
                                                                            job_id = 0;

                                                                            for (i = 0; i <= 7; i++)
                                                                            {
                                                                                nextArr[i] = [i];
                                                                            }

                                                                            console.log(nextArr);
                                                                            if (currentJobs !== 'undefined')
                                                                            {

                                                                                // console.log(currentJobs);
                                                                                jKey = 1;
                                                                                first = 0;
                                                                                $.each(currentJobs, function (jobIndex, jobValue) {
                                                                                    data.addColumn('number', 'JOB :' + jobIndex);

                                                                                    for (i = 0; i <= 7; i++)
                                                                                    {
                                                                                        nextArr[i][jKey] = 0;
                                                                                    }

                                                                                    $.each(jobValue, function (jIndex, jValue) {
                                                                                        //alert(jValue['day']);
                                                                                        currentDay = jValue['day'];
                                                                                        currentSubTotal = jValue['subCount'];
                                                                                        //console.log(currentSubTotal);
                                                                                        nextArr[currentDay][jKey] += currentSubTotal;
                                                                                    });
                                                                                    jKey++;
                                                                                });
                                                                                //console.log(nextArr);
                                                                            }


                                                                            data.addRows(nextArr);

                                                                            var options = {
                                                                                hAxis: {
                                                                                    title: 'Time'
                                                                                },
                                                                                vAxis: {
                                                                                    title: 'Popularity'
                                                                                },
                                                                                series: {
                                                                                    1: {curveType: 'function'}
                                                                                }
                                                                            };

                                                                            var chart = new google.visualization.LineChart(document.getElementById('current_job_performance'));
                                                                            chart.draw(data, options);
                                                                        }

                                                                        function locationSubmissions() {
                                                                            /* var data = new google.visualization.arrayToDataTable([
                                                                             ['Location', 'Requests', 'Submissions'],
                                                                             ['Canis Major Dwarf', 8, 23.3],
                                                                             ['Sagittarius Dwarf', 2, 4.5],
                                                                             ['Ursa Major II Dwarf', 3, 14.3],
                                                                             ['Lg. Magellanic Cloud', 5, 0.9],
                                                                             ['Bootes I', 6, 13.1]
                                                                             ]);*/

                                                                            var data = new google.visualization.arrayToDataTable(locationJson);

                                                                            var options = {
                                                                                width: 900,
                                                                                chart: {
                                                                                    title: 'Location Submissions',
                                                                                    subtitle: 'distance on the left, brightness on the right'
                                                                                },
                                                                                series: {
                                                                                    0: {axis: 'distance'}, // Bind series 0 to an axis named 'distance'.
                                                                                    1: {axis: 'brightness'} // Bind series 1 to an axis named 'brightness'.
                                                                                },
                                                                                axes: {
                                                                                    y: {
                                                                                        distance: {label: 'Total Requests'}, // Left y-axis.
                                                                                        brightness: {side: 'right', label: 'Total Submissions'} // Right y-axis.
                                                                                    }
                                                                                }
                                                                            };

                                                                            var chart = new google.charts.Bar(document.getElementById('location_submissions'));
                                                                            chart.draw(data, options);
                                                                        }

                                                                        function totalSubmissions() {
                                                                            var data = google.visualization.arrayToDataTable([
                                                                                ['Status', 'No. of Jobs'],
                                                                                ['Approved Jobs', approvedJobs],
                                                                                ['Disapproved Jobs', disapprovedJobs],
                                                                                ['Pending Jobs', pendingJobs]
                                                                            ]);

                                                                            var options = {
                                                                                title: 'Total Submissions',
                                                                                is3D: true,
                                                                            };

                                                                            var chart = new google.visualization.PieChart(document.getElementById('total_submissions'));
                                                                            chart.draw(data, options);
                                                                        }
                                                                    });
                                                                </script>