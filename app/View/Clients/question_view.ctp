<script>
    $(document).ready(function(){
     
        $('#settelmentTable').DataTable();

    });
</script>
        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Surveys</h1>
                
                <div class="jobpost-top">
                <div class="col-md-4">
                    
                </div>
                <div class="col-md-5 filter-result-info">
                    <!--<span class="ng-binding">
                        Showing 25/25 entries
                    </span>-->              
                </div>
                
                <div class="col-md-3">
                    <a href="<?php echo WEBSITE_ROOT.'clients/question_add'; ?>" class="btn-custom1 create-job pull-right">Add Survey Question</a>
                </div>
                
                <?php echo $this->Session->flash(); ?>
                
                </div>
                <div class="clearfix"></div>

                <div class="table-custom shadow">
                    <table id="settelmentTable" class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-user"></i>Survey Question Name</th>
                                <th><i class="fa fa-th-list"></i>Survey Question Marks</th>
                                <th><i class="fa fa-hourglass-half"></i>Status</th>
                                <th><i class="fa fa-bullhorn"></i>Actions</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                           <?php foreach($listing as $row) {  ?>  
                        <tr>
                            <td><?php echo $row['SurveyQuestion']['survey_question_description']; ?></td>
                            
                           	<td><?php echo $row['SurveyQuestion']['survey_question_marks']; ?></td>
                           	
                            <?php  $survey_status = $row['SurveyQuestion']['is_blocked']  ?>
                                    <td><?php if($survey_status == 1) {
                                        echo $this->Html->link('Unblock', array('action'=> 'activate_inactive_view/'.$row['SurveyQuestion']['survey_question_id'].'/'.$row['SurveyQuestion']['is_blocked']), array('is_blocked' => '0', 'escape' => false));
                                        }  

                                        else{

                                            echo $this->Html->link('Block', array('action'=> 'activate_inactive_view/'.$row['SurveyQuestion']['survey_question_id'].'/'.$row['SurveyQuestion']['is_blocked']), array('is_blocked' => '1', 'escape' => false));
                                        }

                                        ?> </td>


                                    
                                    <td>
                                    <?php if($row['SurveyQuestion']['survey_question_type']=='C' || $row['SurveyQuestion']['survey_question_type']=='R') { echo "You can't edit question with type checkbox OR radio."; } else { ?>
                                    <a href="<?php echo WEBSITE_ROOT.'clients/edit_question/'.$row['SurveyQuestion']['survey_question_id'] ?>" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a>
                                    <?php } ?>
                                    </td>
                            
                        </tr>
					<?php } ?>	
                        </tbody>
                    </table>
                    
                </div>
                
            </div>

        </div>
<?php


     echo $this->Html->script('jquery.dataTables.min');
    echo $this->Html->css('jquery.dataTables.min');

?>


</body>
</html>