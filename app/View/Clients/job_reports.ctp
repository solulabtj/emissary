<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Job Reports</h1>        
        <div class="col-md-6">                       
            <form action="<?php echo WEBSITE_ROOT."clients/fetch_report_job";  ?>" method="post">
                <div class="group-job">
                    <div class="row">
                        <div class="input-box col-md-6">
                            <label class="input-name"><span>Date From</span><input type="text" class="input-field" value="" id="dpd1" name="date_from" readonly="readonly" ></label>
                        </div>

                        <div class="input-box col-md-6">
                            <label class="input-name"><span>Date To</span><input type="text" class="input-field" value="" id="dpd2" name="date_to" readonly="readonly" ></label>
                        </div>

                        <div class="error" id="error_start" ></div>                            
                        <div class="error" id="error_end" ></div>

                    </div>                    
                    <h2 class="sml-heading">Survey Name</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="survey_name"  name = "survey_name" class="input-field">
                                <option value="0">Select Survey Name</option>
                                    <?php foreach ($survey as $value) { ?>
                                <option value="<?php echo $value['surveys']['survey_id']; ?>"><?php echo $value['surveys']['survey_name']; ?></option>
                                    <?php } ?>
                            </select>
                        </label>
                    </div>
                    <h2 class="sml-heading">Location</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="location"  name = "location" class="input-field">                                    
                                <option value="n">Select Location</option>                                    
                                    <?php foreach ($address as $value) { ?>                                    
                                <option value="<?php echo $value['client_address']['client_address_id']; ?>"><?php echo $value['client_address']['address']; ?></option>
                                    <?php } ?>    
                            </select>
                        </label>
                    </div>
                    <div class="col-md-12 text-center">
                        <input type="button" id="genrate" onclick="report_generate_job();" value="Generate" class="btn btn-lg btn-custom btn-success">
                        <input type="button"  onclick="export_report();" value="Export" class="btn btn-lg btn-custom btn-success">
                        <!--<a href="javascript(void:0);" id="genrate" onclick="report_generate();" class="btn btn-lg btn-custom btn-success" >Genrate</a>-->
                    </div>
                </div>
            </form>
        </div>
        <div style="margin-top: 40%;" id="reportsDataJob"></div>        
    </div>
    <div id="chart_div" style="margin-top: 10%;"></div>
</div>

<?php
    echo $this->Html->script('jquery.js');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('metisMenu.min');
    echo $this->Html->script('sb-admin-2');
    echo $this->Html->script('bootstrap-datepicker.js');
    echo $this->Html->script('googleChart');
?>
<script>
    if (top.location != location) {
        top.location.href = document.location.href;
    }

// Script for the job report

    $(function () {

        $('#dp1').datepicker({
            format: 'dd-mm-yyyy'
        });
        $('#dp2').datepicker({
            format: 'dd-mm-yyyy'
        });

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? '' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? '' : '';
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');


        google.charts.load('current', {'packages': ['bar']});
        google.charts.setOnLoadCallback(drawChart);


    });

    function drawChart() {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/getjobGraphData'; ?>",
            //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
            success: function (response) {
                response = JSON.parse(response);
                var data = google.visualization.arrayToDataTable(response);

                var options = {
                    chart: {
                        title: 'Jobs Report',
                        subtitle: 'Requests Submissions Approved',
                    },
                    bars: 'vertical',
                    vAxis: {format: 'decimal'},
                    height: 400,
                    colors: ['#1b9e77', '#d95f02', '#7570b3']
                };

                var chart = new google.charts.Bar(document.getElementById('chart_div'));

                chart.draw(data, google.charts.Bar.convertOptions(options));
            }

        });
    }

    function export_report()
    {
        startdate = $("#dpd1").val();
        enddate = $("#dpd2").val();
        survey_name = $("#survey_name").val();
        location_data = $("#location").val();

        var parts = startdate.split('/');
        var startdate = parts[2] + '-' + parts[0] + '-' + parts[1];

        var parts = enddate.split('/');
        var enddate = parts[2] + '-' + parts[0] + '-' + parts[1];
        window.open(
                '<?php echo WEBSITE_ROOT.'clients/export_job_report'; ?>' + '/' + startdate + '/' + enddate + '/' + survey_name + '/' + location_data,
                '_blank' // <- This is what makes it open in a new window.
                );
    }

    function report_generate_job()
    {

        startdate = $("#dpd1").val();
        enddate = $("#dpd2").val();
        survey_name = $("#survey_name").val();
        location_data = $("#location").val();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/fetch_report_job'; ?>",
            data: {startdate: startdate, enddate: enddate, location_data: location_data, survey_name: survey_name},
            success: function (data) {
                $('#reportsDataJob').replaceWith($('#reportsDataJob').html(data));
            }

        });
        event.preventDefault();
        return false; //stop the actual form post !important!
    }
</script>
</body>
</html>