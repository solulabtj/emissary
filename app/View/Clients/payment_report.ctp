<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Payment Report</h1>        
        <div class="col-md-6">                       
            <form action="<?php echo WEBSITE_ROOT."clients/job_reports_request";  ?>" method="post">
                <div class="group-job">
                    <div class="row">
                        <div class="input-box col-md-6">
                            <label class="input-name"><span>Date From</span><input type="text" class="input-field" value="" id="dpd1" name="date_from" readonly="readonly" ></label>
                        </div>

                        <div class="input-box col-md-6">
                            <label class="input-name"><span>Date To</span><input type="text" class="input-field" value="" id="dpd2" name="date_to" readonly="readonly" ></label>
                        </div>

                        <div class="error" id="error_start" ></div>                            
                        <div class="error" id="error_end" ></div>

                    </div>                    
                    <h2 class="sml-heading">Job ID</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="job_id"  name = "job_id" class="input-field">
                                <option value="0">Select Job ID</option>
                                   <?php foreach ($job as $value) { ?>                                    
                                <option value="<?php echo $value['jobs']['job_id']; ?>"><?php echo $value['jobs']['job_id']; ?></option>
                                    <?php } ?>
                            </select>
                        </label>
                    </div>
                    <h2 class="sml-heading">Status</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="status"  name = "status" class="input-field">
                                <option value="n">Select Status</option>
                                <option value="0">Pending</option>
                                <option value="1">Paid</option>                                
                            </select>
                        </label>
                    </div>
                    <h2 class="sml-heading">Reward Type</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="reward"  name = "reward" class="input-field">
                                <option value="n">Select Reward Type</option>
                                <option value="1">Coupon</option>
                                <option value="2">Cash</option>     
                            </select>
                        </label>
                    </div>                   
                    <div class="col-md-12 text-center">
                        <input type="button" id="genrate" onclick="report_generate_job();" value="Generate" class="btn btn-lg btn-custom btn-success">
                        <input type="button"  onclick="export_report();" value="Export" class="btn btn-lg btn-custom btn-success">
                        <!--<a href="javascript(void:0);" id="genrate" onclick="report_generate();" class="btn btn-lg btn-custom btn-success" >Genrate</a>-->
                    </div>
                </div>
            </form>
        </div>
        <div style="margin-top: 40%;" id="reportsDataJob"></div>        
    </div>
    <div id="piechart" style="margin-top: 10%;width: 500px; height: 300px;float:left;"></div> 
    <div id="piechart2" style="margin-top: 10%;width: 500px; height: 300px;float:left;"></div> 
</div>

<?php
    echo $this->Html->script('jquery.js');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('metisMenu.min');
    echo $this->Html->script('sb-admin-2');
    echo $this->Html->script('bootstrap-datepicker.js');
    echo $this->Html->script('googleChart');    
?>
<script>
    if (top.location != location) {
        top.location.href = document.location.href;
    }

// Script for the job report

    $(function () {

        $('#dp1').datepicker({
            format: 'dd-mm-yyyy'
        });
        $('#dp2').datepicker({
            format: 'dd-mm-yyyy'
        });

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? '' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? '' : '';
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');
    });

    function report_generate_job()
    {
        startdate = $("#dpd1").val();
        enddate = $("#dpd2").val();
        job_id = $("#job_id").val();
        status = $("#status").val();
        reward = $("#reward").val();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/fetch_payment_report'; ?>",
            data: {startdate: startdate, enddate: enddate, job_id: job_id, status: status, reward: reward},
            success: function (data) {
                $('#reportsDataJob').replaceWith($('#reportsDataJob').html(data));
            }

        });
        event.preventDefault();
        return false; //stop the actual form post !important!
    }

    function export_report()
    {
        startdate = $("#dpd1").val();
        enddate = $("#dpd2").val();
        job_id = $("#job_id").val();
        status = $("#status").val();
        reward = $("#reward").val();

        var parts = startdate.split('/');
        var startdate = parts[2] + '-' + parts[0] + '-' + parts[1];

        var parts = enddate.split('/');
        var enddate = parts[2] + '-' + parts[0] + '-' + parts[1];
        window.open(
                '<?php echo WEBSITE_ROOT.'clients/export_payment_report'; ?>' + '/' + startdate + '/' + enddate + '/' + job_id + '/' + status + '/' + reward,
                '_blank' // <- This is what makes it open in a new window.
                );
    }
    $(function () {
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(currentJobPerformance);
        google.charts.setOnLoadCallback(currentpayment);
    });
    function currentJobPerformance() {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/getPaymentGraphData'; ?>",
            //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
            success: function (res) {
                result = JSON.parse(res);
                var data = google.visualization.arrayToDataTable(result);

                var options = {
                    title: 'Reward Type'
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                chart.draw(data, options);


            }

        });
    }

    function currentpayment() {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/getPaymentSGraphData'; ?>",
            //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
            success: function (res) {
                result = JSON.parse(res);
                var data = google.visualization.arrayToDataTable(result);

                var options = {
                    title: 'Payment Status'
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

                chart.draw(data, options);
            }

        });
    }
</script>
</body>
</html>