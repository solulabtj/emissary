<script>
    $(document).ready(function(){
     
        $('#settelmentTable').DataTable({"bSort": false});

    });
</script>

<div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Job Posts</h1>
                
                <div class="jobpost-top">
                <div class="col-md-4">
                    <!--<form class="ng-pristine ng-valid">
                        <input type="text" placeholder="search surveys" class="form-control ng-pristine ng-valid ng-touched" data-ng-model="searchKeywords" data-ng-keyup="search()">
                    </form>-->
                </div>
                <div class="col-md-5 filter-result-info">
                    <!--<span class="ng-binding">
                        Showing 25/25 entries
                    </span> -->             
                </div>
                
                <div class="col-md-3">
                    <a href="<?php echo WEBSITE_ROOT.'clients/new_job_post' ?>" class="btn-custom1 create-job pull-right">Add New Job</a>
                </div>
                
                <?php echo $this->Session->flash(); 
				
					  if(empty($get_job_list))
					  {
						  echo "You have not posted any jobs yet.";
					  }
				?>
                
                </div>
                <div class="clearfix"></div>

                <div class="table-custom shadow">
                    <table id="settelmentTable" class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-briefcase"></i>Job Id</th>
                                <th><i class="fa fa-calendar"></i>Date Range</th>
                                <th><i class="fa fa-clock-o"></i>Time</th>
                                <!--<th><i class="fa fa-book"></i>Assignments</th>-->
                                <th><i class="fa fa-money"></i>Reward</th>
                                <th><i class="fa fa-hourglass-half"></i>Status</th>
                                <th><i class="fa fa-bullhorn"></i>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
							//pr($get_job_list);

                                foreach ($get_job_list as $job_list) { ?>
                                <tr>
                                    <td><?php echo $job_list['Job']['job_id']; ?></td>
                                    <td><?php echo date('m-d-Y',strtotime($job_list['Job']['job_start_date'])).' to '.date('m-d-Y',strtotime($job_list['Job']['job_end_date'])); ?></td>
                                    <td><?php echo date('H:i A',strtotime($job_list['Job']['job_start_date'])).' to '.date('H:i A',strtotime($job_list['Job']['job_end_date'])); ?></td>
                                    <!--<td><?php echo $job_list['Job']['job_id']; ?></td>-->
                                    <td><?php echo $job_list['Job']['job_compensation']." KWD"; ?></td>
                                    <?php  $job_status = $job_list['Job']['is_active']  ?>

                                    <td><?php
										
										if($job_list['Job']['is_locked']=='1') 
										{ ?>
											<a href="javascript:;" >Locked</a>
										<?php }
										else
										{
										if($job_status == 1) {
                                        echo $this->Html->link('Active', array('action'=> 'activate_inactive/'.$job_list['Job']['job_id'].'/'.$job_list['Job']['is_active']), array('is_active' => '0', 'escape' => false));
                                        }  

                                        else{

                                            echo $this->Html->link('Inactive', array('action'=> 'activate_inactive/'.$job_list['Job']['job_id'].'/'.$job_list['Job']['is_active']), array('is_active' => '1', 'escape' => false));
                                        } }
										
                                        ?> </td>
                                    <td>
									
									<?php 
							
									 
									 if($job_list['Job']['is_locked']=='1')
									 {
										 ?> <a href="javascript:;" >Locked</a> <?php 
									 }
									 else
									 {
										 echo $this->Html->link('Edit', array('action' => 'edit_job/'.$job_list['Job']['job_id']));
									 }
									 
									?>
									
                                    </td>
                                    <!--<td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></ td>-->
                                    
                                </tr>
                                    
                                <?php }

                            ?>
                            
                        </tbody>
                    </table>


                    <footer class="table-footer">
                        <div class="row">
                            <!--<div class="col-md-6 page-num-info">
                                <span>
                                    Show 
                                    <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()" class="ng-valid ng-dirty ng-valid-parse ng-touched"><option value="0" label="3">3</option><option value="1" label="5">5</option><option value="2" selected="selected" label="10">10</option><option value="3" label="20">20</option></select> 
                                    entries per page
                                </span>
                            </div>
                            <div class="col-md-6 text-right pagination-container">
                                <ul class="pagination-sm pagination ng-isolate-scope ng-valid">
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">First</a>
                                    </li>
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">Previous</a>
                                    </li>
                                    <li class="ng-scope active">
                                      <a href="" class="ng-binding">1</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">2</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">3</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Next</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Last</a>
                                    </li>
                                </ul>
                            </div>-->
                        </div>
                    </footer>
                </div>
                
            </div>

        </div>


	<?php
			
			echo $this->Html->script('jquery.dataTables.min');
   			echo $this->Html->css('jquery.dataTables.min');
			
	?>


	<?php /*?><!-- jQuery -->
    <script src="scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="scripts/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="scripts/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="scripts/sb-admin-2.js"></script><?php */ ?>
</body>
</html>