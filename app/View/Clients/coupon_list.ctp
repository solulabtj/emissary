<script>
    $(document).ready(function(){
     
        $('#requestTable').DataTable();

    });
</script>

        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Coupon List</h1>
                
                <div class="clearfix"></div>
				
                <?php echo $this->Session->flash(); ?>
                
                <div class="col-md-12">
                    <a href="<?php echo WEBSITE_ROOT.'clients/add_coupons' ?>" class="btn-custom1 create-job pull-right">Add Coupon</a>
                </div> &nbsp;
                
                <div class="table-custom shadow">
                    <table id="requestTable" class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-user"></i>Code</th>
                                <th><i class="fa fa-th-list"></i>Offer</th>
                                <th><i class="fa fa"></i>Valid Upto</th>
                                <th><i class="fa fa-book"></i>Quantity</th>
                                <th><i class="fa fa-hourglass-half"></i>Status</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            //pr($job_submission_list);exit;
                                foreach ($coupon_list as $list) { 

                                    if($list['Coupon']['is_active'] == 1){
                                        $status = $this->Html->link('Active', array('action'=> 'change_coupon_status/'.$list['Coupon']['coupon_id'].'/'.$list['Coupon']['is_active']), array('is_active' => '0', 'escape' => false));
                                    }
                                    else{
                                        $status = $this->Html->link('Inactive', array('action'=> 'change_coupon_status/'.$list['Coupon']['coupon_id'].'/'.$list['Coupon']['is_active']), array('is_active' => '1', 'escape' => false));
                                    }

                                ?>

                                <tr>
                                    <td><?php echo $list['Coupon']['coupon_name'];?></td>
                                    <td><?php echo $list['Coupon']['coupon_description'];?></td>
                                    <td><?php echo date('m-d-Y',strtotime($list['Coupon']['coupon_expiration_date']));?></td>
                                    <td><?php echo $list['Coupon']['coupon_quantity'];?></td>
                                    <td><?php echo $status;?></td>
                                    
                                </tr>
                                    
                                <?php }

                            ?>
                           
                        </tbody>
                    </table>
                    
                </div>
                
            </div>

        </div>


</body>
</html>