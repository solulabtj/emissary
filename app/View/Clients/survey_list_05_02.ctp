
        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Surveys</h1>
                
                <div class="jobpost-top">
                <div class="col-md-4">
                    <form class="ng-pristine ng-valid">
                        <input type="text" placeholder="search surveys" class="form-control ng-pristine ng-valid ng-touched" data-ng-model="searchKeywords" data-ng-keyup="search()">
                    </form>
                </div>
                <div class="col-md-5 filter-result-info">
                    <span class="ng-binding">
                        Showing 25/25 entries
                    </span>              
                </div>
                <div class="col-md-3">
                    <a href="add-surveys.html" class="btn-custom1 create-job pull-right">Add Surveys</a>
                </div>
                </div>
                <div class="clearfix"></div>

                <div class="table-custom shadow">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-user"></i>Survet Name</th>
                                <th><i class="fa fa-th-list"></i>Categories</th>
                                <th><i class="fa fa-question"></i>Questions</th>
                                <th><i class="fa fa-book"></i>Created</th>
                                <th><i class="fa fa-hourglass-half"></i>Status</th>
                                <th><i class="fa fa-bullhorn"></i>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <footer class="table-footer">
                        <div class="row">
                            <div class="col-md-6 page-num-info">
                                <span>
                                    Show 
                                    <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()" class="ng-valid ng-dirty ng-valid-parse ng-touched"><option value="0" label="3">3</option><option value="1" label="5">5</option><option value="2" selected="selected" label="10">10</option><option value="3" label="20">20</option></select> 
                                    entries per page
                                </span>
                            </div>
                            <div class="col-md-6 text-right pagination-container">
                                <ul class="pagination-sm pagination ng-isolate-scope ng-valid">
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">First</a>
                                    </li>
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">Previous</a>
                                    </li>
                                    <li class="ng-scope active">
                                      <a href="" class="ng-binding">1</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">2</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">3</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Next</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Last</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </footer>
                </div>
                
            </div>

        </div>



</body>
</html>