<div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Post New Job</h1>
                

                <div class="col-md-6">
                    <form action="<?php echo WEBSITE_ROOT."clients/new_job_post";  ?>" method="post">
                        <div class="group-job">
                        <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Date Span</h2>
                            <div class="row">
                            <div class="input-box col-md-6">
                                <label class="input-name"><span>Date From</span><input type="text" class="input-field" value="" id="dpd1" name="date_from" ></label>
                            </div>
                            <div class="input-box col-md-6">
                                <label class="input-name"><span>Date To</span><input type="text" class="input-field" value="" id="dpd2" name="date_to" ></label>
                            </div>
                            </div>
                        </div>


                        <div class="group-job">
                        <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Location <a class="add" href="">Add more</a></h2>
                            <div class="input-box">
                                <label class="input-name"><span>Location 1</span>
                                <select  name = "location"class="input-field">
                                  <option>Ahmedabad</option>
                                  <option>Kutch</option>
                                  <option>Surendranagar</option>
                                  <option>Rajkot</option>
                                  <option>Morbi</option>
                                </select>
                                </label>
                            </div>
                        </div>


                        <div class="group-job">
                        <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Overview</h2>
                            <div class="input-box">
                                <label class="input-name"><textarea name="job_overview" class="input-field" rows="4"></textarea></label>
                            </div>
                        </div>

                        <div class="group-job">
                        <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Associated Survey<a class="add" href="">Manage</a></h2>
                            <div class="input-box">
                                <label class="input-name"><span>Survey List</span>
                                <select name="survey_list" class="input-field">
                                  <option>Survey 1</option>
                                  <option>Survey 2</option>
                                  <option>Survey 3</option>
                                  <option>Survey 4</option>
                                  <option>Survey 5</option>
                                </select>
                                </label>
                            </div>
                            <div class="input-box">
                                <label class="input-name"><span>Survey List</span>
                                <select name="1" class="input-field">
                                  <option>Survey 1</option>
                                  <option>Survey 2</option>
                                  <option>Survey 3</option>
                                  <option>Survey 4</option>
                                  <option>Survey 5</option>
                                </select>
                                </label>
                            </div>
                        </div>

                    </div><!-- LEFT -->


                    <div class="col-md-6">

                    <div class="group-job">
                    <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Time Span</h2>
                    <div class="row">
                        <div class="input-box col-md-6">
                            <label class="input-name"><span>From</span><input type="number" class="input-field" name="time_span_from" ></label>
                        </div>
                        <div class="input-box col-md-6">
                            <label class="input-name"><span>To</span><input type="number" class="input-field" name="time_span_to" ></label>
                        </div>
                    </div>
                    </div>

                    
                    <div class="group-job">
                    <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Reward</h2>
                        <div class="input-box">
                            <label class="input-name dis-inline"><input type="radio" value="Cash" name="reward" class="input-field pd20 dis-inline">Cash Reward</label>
                            <label class="input-name dis-inline"><input type="radio" value="Coupon" name="reward" class="input-field pd20 dis-inline">Coupon Reward</label
                            >
                            <label class="input-name"><input type="text" name="cash_value" class="input-field"></label>
                        </div>
                    </div>


                    <div class="group-job">
                    <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Requirements</h2>
                        <div class="input-box">
                            <label class="input-name"><span>Maximum requests</span><input name="request_number" type="number" class="input-field" ></label>
                        </div>
                        <div class="input-box">
                            <label class="input-name"><span>Minimum required agent rank</span>
                            <select name="agent_rank" class="input-field">
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                            </select>
                            </label>
                        </div>
                        <div class="input-box">
                            <label class="input-name dis-inline"><input type="checkbox" name="test_status" class="input-field pd20 dis-inline">Test Passed</label>
                        </div>
                    </div>

                    <div class="group-job">
                    <h2 class="sml-heading"><i class="fa fa-clock-o"></i>Job Status</h2>
                        <div class="input-box">
                            <label class="input-name dis-inline"><input type="radio" value="1" name="job_status" class="input-field pd20 dis-inline">Active</label>
                            <label class="input-name dis-inline"><input type="radio" value="0" name="job_status" class="input-field pd20 dis-inline">inactive</label
                            >
                        </div>
                    </div>

                    </div><!-- right -->        
                    
                    <div class="col-md-12 text-center">
                        <input type="submit" value="Publish" class="btn btn-lg btn-custom btn-success">
                    </div>
                </form>
            </div>

        </div>

	
    <?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			echo $this->Html->script('bootstrap-datepicker.js');
			
	?>


	<?php /*?><!-- jQuery -->
    <script src="scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="scripts/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="scripts/bootstrap-datepicker.js"></script>
	
    <script src="scripts/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="scripts/sb-admin-2.js"></script><?php */?>
    
    <script>
    if (top.location != location) {
    top.location.href = document.location.href ;
  }
        $(function(){
            window.prettyPrint && prettyPrint();
            $('#dp1').datepicker({
                format: 'dd-mm-yyyy'
            });
            $('#dp2').datepicker({
            format: 'dd-mm-yyyy'
      });
            
            

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
        });
    </script>

    
</body>
</html>