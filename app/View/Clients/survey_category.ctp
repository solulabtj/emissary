<script>
    $(document).ready(function(){
     
        $('#settelmentTable').DataTable();

    });
	
	
	function delete_jobcategory(survey_category_id)
	{
		
		var valid_del = window.confirm('Are you sure want to delete this record?');
		
		if(valid_del===true)
		{
			window.location='<?php echo WEBSITE_ROOT; ?>clients/delete_surveycategory/'+survey_category_id;
		}
		
	}
	
</script>
        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Survey Categories</h1>
                
                <div class="jobpost-top">
                <div class="col-md-4">
                    <!--<form class="ng-pristine ng-valid">
                        <input type="text" placeholder="search surveys" class="form-control ng-pristine ng-valid ng-touched" data-ng-model="searchKeywords" data-ng-keyup="search()">
                    </form>-->
                </div>
                <div class="col-md-5 filter-result-info">
                    <!--<span class="ng-binding">
                        Showing 25/25 entries
                    </span> -->             
                </div>
                
               
                
                <div class="col-md-3">
                    <a href="<?php echo WEBSITE_ROOT.'clients/category_add'; ?>" class="btn-custom1 create-job pull-right">Add Survey Category</a>
                    
                </div>
                
                 <?php echo $this->Session->flash(); ?>
                
                </div>
                <div class="clearfix"></div>
                

                <div class="table-custom shadow">
                    <table id="settelmentTable" class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-user"></i>Survey Category Title</th>
                                <th>Survey Category Image</th>
                                <th><i class="fa fa-bullhorn"></i>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
							
                                foreach ($listing as $survey_list) { ?>
                            <tr>
                                <td><?php echo $survey_list['SurveyCategories']['survey_category_title']; ?></td>
                                <td><?php 
									
									if(file_exists(WEBSITE_WEBROOT.'media/img/'.$survey_list['SurveyCategories']['survey_category_image']))
									{ ?>
										<img src="<?php echo WEBSITE_ROOT.'media/img/'.$survey_list['SurveyCategories']['survey_category_image'] ?>" width="40px" height="40px"  /> 
									<?php }
									
									else {
									
									?>
                                    	
                                        <div>Image is not available to show.</div>
                                        
                                    <?php } ?>
                                    
                                </td>
                                
                                <td>
                                
                                <a href="<?php echo WEBSITE_ROOT.'clients/edit_surveycategory/'.$survey_list['SurveyCategories']['survey_category_id']; ?>" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a>					
                                
                                <!--<a href="javascript:delete_jobcategory('<?php echo $survey_list['SurveyCategories']['survey_category_id'] ?>');" class="edit-job">Delete</a>-->
                                
                                </td>
                            
                            <?php }

                            ?>
                              
                        </tbody>
                    </table>
                    
                </div>
                
            </div>

        </div>
<?php


     echo $this->Html->script('jquery.dataTables.min');
    echo $this->Html->css('jquery.dataTables.min');

?>


</body>
</html>