<script>
    $(document).ready(function(){
     
        $('#settelmentTable').DataTable({"bSort": false});

    });
</script>
        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Surveys</h1>
                
                <div class="jobpost-top">
                <div class="col-md-4">
                    <!--<form class="ng-pristine ng-valid">
                        <input type="text" placeholder="search surveys" class="form-control ng-pristine ng-valid ng-touched" data-ng-model="searchKeywords" data-ng-keyup="search()">
                    </form>-->
                </div>
                <div class="col-md-5 filter-result-info">
                    <!--<span class="ng-binding">
                        Showing 25/25 entries
                    </span> -->             
                </div>
                
               
                
                <div class="col-md-3">
                    <a href="<?php echo WEBSITE_ROOT.'clients/add_survey'; ?>" class="btn-custom1 create-job pull-right">Add Surveys</a>
                    
                </div>
                
                <div class="col-md-3">
                    <a href="<?php echo WEBSITE_ROOT.'clients/question_add'; ?>" class="btn-custom1 create-job pull-right">Add Survey Questions</a>
                    
                </div>
                
                 <?php echo $this->Session->flash(); ?>
                
                </div>
                <div class="clearfix"></div>
                

                <div class="table-custom shadow">
                    <table id="settelmentTable" class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-user"></i>Survey Name</th>
                                <th>No. of Categories</th>
                                <th>No. of Questions</th>
                                <th><i class="fa fa-book"></i>Created Time</th>
                                <!--<th><i class="fa fa-book"></i>Assigned Job</th>-->
                                <th><i class="fa fa-hourglass-half"></i>Status</th>
                                <th><i class="fa fa-bullhorn"></i>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                foreach ($get_survey_list as $survey_list) { ?>
                                <tr>
                                    <td><?php echo $survey_list['Survey']['survey_name']; ?></td>
                                    <td><?php echo $survey_list['Survey']['CategoryCount']; ?></td>
                                    <td><?php echo $survey_list['Survey']['QuestionCount'] ?></td>
                                    
                                    <td>
																		
                                    <?php 
							
									 $now = time(); 
									 $your_date = strtotime($survey_list['Survey']['created']);
									 $datediff = $now - $your_date;
									 //echo floor($datediff/(60*60*24)).' days ago';
									echo date('m-d-Y',strtotime($survey_list['Survey']['created']));
									?>
                                    
                                    </td>
                                    
                                    <!--<td>
                                    <?php
								/*	if(isset($survey_list['Survey']['job_id']) && !empty($survey_list['Survey']['job_id']))
									{
										$job_id = $survey_list['Survey']['job_id'];
										$job_link = WEBSITE_ROOT.'clients/edit_job/'.$job_id;
										if($survey_list['Survey']['is_locked'])
										{
											echo "Locked (".$job_id.")";	
										}
										else
										{
											echo '<a href='.$job_link.'>'.$job_id.'</a>';
										}
									}
									else
									{
										$job_id = "Not asssigned yet";	
										echo $job_id;
									}
									*/
									?>
                                    </td> -->                                   
                                     <?php  $survey_status = $survey_list['Survey']['is_active']  ?>
                                    <td>
									<?php 
									
									if($survey_list['Survey']['is_locked']=='1')
									{ ?>
										<a href="javascript:;" >Locked</a>
									<?php }
									else
									{
									if($survey_status == 1) {
                                        echo $this->Html->link('Publish', array('action'=> 'survey_status/'.$survey_list['Survey']['survey_id'].'/'.$survey_list['Survey']['is_active']), array('is_active' => '0', 'escape' => false));
                                        }  

                                        else{

                                            echo $this->Html->link('Unpublished', array('action'=> 'survey_status/'.$survey_list['Survey']['survey_id'].'/'.$survey_list['Survey']['is_active']), array('is_active' => '1', 'escape' => false));
                                        }
									}

                                        ?> </td>


                                    <!--<td><?php echo $survey_list['survey']['is_active']; ?></td>-->
                                    <td>
                                    <?php 
                                    if($survey_list['Survey']['is_locked']=='1')
									{ ?>
										<a href="javascript:;" >Locked</a>
									<?php }
									else
									{ ?>
                                    <a href="<?php echo WEBSITE_ROOT.'clients/edit_survey/'.$survey_list['Survey']['survey_id']; ?>" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a>
									<?php } ?>
                                    
                                    <?php echo $this->Html->link('View Questions',array('action'=> 'view_question_survey/'.$survey_list['Survey']['survey_id']),array('escape' => false)); ?>
                                    
                                    </td>

                                <?php }

                            ?>
                               <!-- <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>
                            <tr>
                                <td>Feedback Survey</td>
                                <td>3</td>
                                <td>45</td>
                                <td>2 Days ago</td>
                                <td class="active">Published</td>
                                <td><a href="" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a></td>
                            </tr>-->
                        </tbody>
                    </table>
                    <!--<footer class="table-footer">
                        <div class="row">
                            <div class="col-md-6 page-num-info">
                                <span>
                                    Show 
                                    <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()" class="ng-valid ng-dirty ng-valid-parse ng-touched"><option value="0" label="3">3</option><option value="1" label="5">5</option><option value="2" selected="selected" label="10">10</option><option value="3" label="20">20</option></select> 
                                    entries per page
                                </span>
                            </div>
                            <div class="col-md-6 text-right pagination-container">
                                <ul class="pagination-sm pagination ng-isolate-scope ng-valid">
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">First</a>
                                    </li>
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">Previous</a>
                                    </li>
                                    <li class="ng-scope active">
                                      <a href="" class="ng-binding">1</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">2</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">3</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Next</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Last</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </footer>-->
                </div>
                
            </div>

        </div>
<?php


     echo $this->Html->script('jquery.dataTables.min');
    echo $this->Html->css('jquery.dataTables.min');

?>


</body>
</html>