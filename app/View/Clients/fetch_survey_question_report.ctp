<?php

echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->css('jquery.dataTables.min');
?>
<script>
    $(document).ready(function () {

        $('#settelmentTable').DataTable();

    });
</script>
<style>
.table-custom .table>thead>tr>th{
            text-align: left !important;
    }  
    
 .table-custom .table>tbody>tr>td{
            text-align: left !important;
    }     
</style>
<?php  //echo "<pre>"; print_r($res); exit; ?>
<div class="clearfix"></div>
<div class="table-custom shadow">
    <table id="settelmentTable" class="table text-center">
        <thead>
            <tr>
                <th>Survey Name</th>
                <th>Category Name</th>                                        
                <th>Question</th>
                <th>Question Type</th>
                <th>Answer Received</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($res as $job_list) { ?>
            <?php
                
            if($job_list['survey_questions']['survey_question_type']== "C"){
                $question_type="MCQ";
            }else if($job_list['survey_questions']['survey_question_type']== "T"){
                $question_type="TEXT";
            }else if($job_list['survey_questions']['survey_question_type']== "I"){
                $question_type="IMAGE";
            }else if($job_list['survey_questions']['survey_question_type']== "A"){
                $question_type="AUDIO";
            }else if($job_list['survey_questions']['survey_question_type']== "R"){
                $question_type="Radio";
            }else {
                $question_type="";
            }
            
            ?>
        <td><?php echo $job_list['surveys']['survey_name']; ?></td>
        <td><?php echo $job_list['survey_categories']['survey_category_title']; ?></td>        
        <td><?php echo $job_list['survey_questions']['survey_question_description']; ?></td>
        <td><?php echo $question_type; ?></td>
        <td><?php echo $job_list['0']['answer']; ?></td>
        </tr>
         <?php }  ?>
        </tbody>
    </table>
    <footer class="table-footer">
        <div class="row">                   
        </div>
    </footer>
</div>
<?php exit(); ?>