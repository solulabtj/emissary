<script>
    $(document).ready(function () {

        $('#settelmentTable').DataTable();

    });
</script>
<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Surveys</h1>

        <div class="jobpost-top">
            <div class="col-md-4">
                <!--<form class="ng-pristine ng-valid">
                    <input type="text" placeholder="search surveys" class="form-control ng-pristine ng-valid ng-touched" data-ng-model="searchKeywords" data-ng-keyup="search()">
                </form>-->
            </div>
            <div class="col-md-5 filter-result-info">
                <!--<span class="ng-binding">
                    Showing 25/25 entries
                </span> -->             
            </div>



            <div class="col-md-3">
                <a href="<?php echo WEBSITE_ROOT . 'clients/surveyList'; ?>" class="btn-custom1 create-job pull-right">Back to Surveys</a>

            </div>

            <div class="col-md-3">
                <a href="<?php echo WEBSITE_ROOT . 'clients/question_add'; ?>" class="btn-custom1 create-job pull-right">Add Survey Questions</a>

            </div>

            <?php echo $this->Session->flash(); ?>

        </div>
        <div class="clearfix"></div>

        <div class="table-custom shadow" style="font-size: 20px;">

            <?php
            foreach ($survey_questions as $survey_question) {
                $count = count($survey_question['SurveyQuestionOption']);
                ?>

                <div class="input-box"><?php echo "<b>Question</b> : "; ?><?php
                        echo $survey_question['SurveyQuestion']['survey_question_description'];
                        ?><br><?php
                    if ($survey_question['SurveyQuestion']['survey_question_type'] == 'C' || $survey_question['SurveyQuestion']['survey_question_type'] == 'R') {
                       if($count == 0){
                        echo "<b>Option</b> : No option available . <br>";   
                       }else{ 
                        for ($i = 0; $i < $count; $i++) {
                            echo "<b>Option</b> : " . $survey_question['SurveyQuestionOption'][$i]['answer_text'] . "<br>";
                        }
                       }
                    } else {
                        echo "This type questions dont have options. <br>";
                    }
                    ?>
                        
    <?php echo "<b>Question Marks : </b>".$survey_question['SurveyQuestion']['survey_question_marks']; ?>

                        <br>
                    <?php
                    if ($survey_question['SurveyQuestion']['is_locked'] == '1') {
                        ?>
                        <a href="javascript:;" >Locked</a>
                        <?php
                    } else {
                        ?>

                        <?php
                        if ($survey_question['SurveyQuestion']['survey_question_type'] == 'C' || $survey_question['SurveyQuestion']['survey_question_type'] == 'R') {
                            echo "You can't edit question with type checkbox OR radio.";
                        } else {
                            ?>
                            <a href="<?php echo WEBSITE_ROOT . 'clients/edit_question/' . $survey_question['SurveyQuestion']['survey_question_id']; ?>" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a>
                        <?php } ?>

    <?php } ?>                            
                </div>
            <br>
<?php } ?>

        </div>


       <!-- <div class="table-custom shadow">
            <table id="settelmentTable" class="table text-center">
                <thead>
                    <tr>
                        <th><i class="fa fa-user"></i>Survet Question Description</th>
                        <th><i class="fa fa-book"></i>Survey Question Option</th>
                        <th><i class="fa fa-book"></i>Survey Question Marks</th>
                        <th><i class="fa fa-bullhorn"></i>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($survey_questions as $survey_question) {
                        $count = count($survey_question['SurveyQuestionOption']);
                        ?>
                        <tr>
                            <td><?php echo $survey_question['SurveyQuestion']['survey_question_description']; ?></td>
                            <td><?php
                                if ($survey_question['SurveyQuestion']['survey_question_type'] == 'C' || $survey_question['SurveyQuestion']['survey_question_type'] == 'R') {
                                    for ($i = 0; $i <= $count; $i++) {
                                        echo $survey_question['SurveyQuestionOption'][$i]['answer_text'] . "<br>";
                                    }
                                } else {
                                    echo "This type questions dont have options.";
                                }
                                ?>
                            </td>
                            <td><?php echo $survey_question['SurveyQuestion']['survey_question_marks']; ?></td>
                            <td>

                                <?php
                                if ($survey_question['SurveyQuestion']['is_locked'] == '1') {
                                    ?>
                                    <a href="javascript:;" >Locked</a>
                                    <?php
                                } else {
                                    ?>

                                    <?php
                                    if ($survey_question['SurveyQuestion']['survey_question_type'] == 'C' || $survey_question['SurveyQuestion']['survey_question_type'] == 'R') {
                                        echo "You can't edit question with type checkbox OR radio.";
                                    } else {
                                        ?>
                                        <a href="<?php echo WEBSITE_ROOT . 'clients/edit_question/' . $survey_question['SurveyQuestion']['survey_question_id']; ?>" class="edit-job"><i class="fa fa-pencil-square"></i>Edit</a>
                                    <?php } ?>

                            <?php } ?>
                            </td>

<?php }
?>
                </tbody>
            </table>
        </div>-->

    </div>

</div>
<?php
echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->css('jquery.dataTables.min');
?>


</body>
</html>