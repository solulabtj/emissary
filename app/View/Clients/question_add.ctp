<script type="text/javascript">
		
	function validate_settings()
	{
			var error = false;
            
			var question_count = $('#question_count').val();
			var question_validate;
			for(question_validate=1;question_validate<question_count;question_validate++)
			{
				var survey_category_id = $('#survey_category_id_'+question_validate).val(); 
				
				var survey_question_type = $('#survey_question_type_'+question_validate).val();
				
				var survey_id = $('#survey_id_'+question_validate).val(); 
				
				var survey_question_description=$('#survey_question_description_'+question_validate).val(); 				
				var survey_question_marks = $('#survey_question_marks_'+question_validate).val();
				
				if(survey_category_id=='')
				{
					$('#error_category_'+question_validate).html('Survey Category is required.');
					error = true;
				}
				else
				{
					$('#error_category_'+question_validate).html('');
				}
				
				if(survey_id=='')
				{
					$('#error_survey_'+question_validate).html('Survey Selection is required.');
					error = true;
				}
				else
				{
					$('#error_survey_'+question_validate).html('');
				}
				
				if(survey_question_description=='')
				{
					$('#error_questions_'+question_validate).html('Survey Question Description is required.');
					error = true;
				}
				else
				{
					$('#error_questions_'+question_validate).html('');
				}
				
				if(survey_question_marks=='' || survey_question_marks=='0')
				{
					$('#error_marks_'+question_validate).html('Survey Question Mark is required OR should not be Zero.');
					error = true;
				}
				else
				{
					$('#error_marks_'+question_validate).html('');
				}
				
				if(survey_question_type=='')
				{
					$('#error_type_'+question_validate).html('Question Type is required.');
					error = true;
				}
				else if(survey_question_type=='C')
				{
					$('#error_type_'+question_validate).html('');
					
					var limit;
					for(limit=1;limit<5;limit++)
					{
						var option_1 = $('#option_'+ question_validate +'_1').val();
						
						var option_2 = $('#option_'+ question_validate +'_2').val();
						
						var option_3 = $('#option_'+ question_validate +'_3').val();
						
						var option_4 = $('#option_'+ question_validate +'_4').val();
						
						if(option_1=='')
						{
							$('#error_option'+question_validate+'_1').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+question_validate+'_1').html('');
						}
						
						if(option_2=='')
						{
							$('#error_option'+question_validate+'_2').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+question_validate+'_2').html('');
						}
						
						if(option_3=='')
						{
							$('#error_option'+question_validate+'_3').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+question_validate+'_3').html('');
						}
						
						if(option_4=='')
						{
							$('#error_option'+question_validate+'_4').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+question_validate+'_4').html('');
						}
						
					}
				}
				
				else if(survey_question_type=='R')
				{
					var questionr1_count = $('#questionr'+question_validate+'_count').val();
					var limit;
					
					for(limit=1;limit<questionr1_count;limit++)
					{
						var option = $('#optionr_'+ question_validate +'_'+limit).val();
						if(option=='')
						{
							$('#error_optionr'+question_validate+'_'+limit).html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_optionr'+question_validate+'_'+limit).html('');
						}
					}
				}
				
				else
				{
					$('#error_type_'+question_validate).html('');
				}
				
			}
			
            if (error) {
                return false;
            }
			
     }
		
		function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
		}
		
		function validate_predefine()
		{
			var error = false;
		
			var values = $('input:checkbox:checked.predefined_question').map(function () {
			  return this.value;
			}).get();
			
			if(values=='')
			{
				alert('Select at least one predefined Question.');
				error = true;
			}
			
			$.each(values,function(i){
				var survey_category_id = $("#survey_category_id_"+values[i]).find(":selected").text();
				
				var survey_question_type = $('#survey_question_type_'+values[i]).val();
				
				var survey_id = $('#survey_id_'+values[i]).find(":selected").text(); 
				
				var survey_question_description = $('#survey_question_description_'+values[i]).val();
				 				
				var survey_question_marks = $('#survey_question_marks_'+values[i]).val();
				
				if(survey_category_id == "Select Type")
				{
					$("#error_category_"+values[i]).html("Please select survey category of question.");
					error = true;
				}
				else
				{
					$("#error_category_"+values[i]).html();
				}
				
				if(survey_question_type == "")
				{
					$("#error_type_"+values[i]).html("Please select survey Question Type.");
					error = true;
				}
				
				else if(survey_question_type=='C')
				{
					$('#error_type_'+values[i]).html('');
					
					var limit;
					for(limit=1;limit<5;limit++)
					{
						var option_1 = $('#option_'+ values[i] +'_1').val();
						
						var option_2 = $('#option_'+ values[i] +'_2').val();
						
						var option_3 = $('#option_'+ values[i] +'_3').val();
						
						var option_4 = $('#option_'+ values[i] +'_4').val();
						
						if(option_1=='')
						{
							$('#error_option'+values[i]+'_1').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+values[i]+'_1').html('');
						}
						
						if(option_2=='')
						{
							$('#error_option'+values[i]+'_2').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+values[i]+'_2').html('');
						}
						
						if(option_3=='')
						{
							$('#error_option'+values[i]+'_3').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+values[i]+'_3').html('');
						}
						
						if(option_4=='')
						{
							$('#error_option'+values[i]+'_4').html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_option'+values[i]+'_4').html('');
						}
						
					}
				}
				
				else if(survey_question_type=='R')
				{
					var questionr1_count = $('#questionr'+values[i]+'_count').val();
					var limit;
					
					for(limit=1;limit<questionr1_count;limit++)
					{
						var option = $('#optionr_'+ values[i] +'_'+limit).val();
						if(option=='')
						{
							$('#error_optionr'+values[i]+'_'+limit).html('Option is required.');
							error = true;
						}
						else
						{
							$('#error_optionr'+values[i]+'_'+limit).html('');
						}
					}
				}
				
				else
				{
					$("#error_type_"+values[i]).html();
				}
				
				if(survey_id == "Select Survey")
				{
					$("#error_survey_"+values[i]).html("Please select survey of question.");
					error = true;
				}
				else
				{
					$("#error_survey_"+values[i]).html();
				}
				
				if(survey_question_description=='')
				{
					$('#error_questions_'+values[i]).html('Survey Question Description is required.');
					error = true;
				}
				else
				{
					$('#error_questions_'+values[i]).html('');
				}
				
				if(survey_question_marks=='' || survey_question_marks=='0')
				{
					$('#error_marks_'+values[i]).html('Survey Question Mark is required OR should not be Zero.');
					error = true;
				}
				else
				{
					$('#error_marks_'+values[i]).html('');
				}
				
			});
			
			if(error)
			{
				return false;
			}
		}
		
		function questionadd_change(add_value)
		{
			if(add_value==1)
			{
				$('#manually_add').hide();
				$('#predefined_add').show();
			}
			else if(add_value==2)
			{
				$('#manually_add').show();
				$('#predefined_add').hide();
			}
			else
			{
				$('#manually_add').hide();
				$('#predefined_add').hide();
			}
		}
		
</script>

<style>
    .error{
        color: red;
    }
</style>

<div id="page-wrapper">
    <div class="row">
	    <h1 class="page-header">Add Survey Questions</h1>
	    <div class="center" align="center"><?php echo $this->Session->flash(); ?></div>
		
		<select name="questionadd_type" id="questionadd_type" onchange="questionadd_change(this.value);" >
			
			<option value="0">Select</option>
			<option value="1">Predefined Questions</option>
			<option value="2">Manually Add Questions</option>
		
		</select>
		
		<div id="manually_add" style="display:none;" >
		<form method="post" enctype="multipart/form-data" action="<?php echo WEBSITE_ROOT.'clients/question_add'; ?>">
		
		<input type="hidden" name="pre_question" value="1" />
		
	    	<input type="hidden" name="question_count" id="question_count" value="2" />
            
            <input type="hidden" name="questionr1_count" id="questionr1_count" value="3" />
            
	       	<div id="apply_formdata" class="col-md-6">  
	            <div id="apply_question1" > 
	    		    <h2 class="page-header">Question 1</h2>            		
	    			<div class="input-box">
	        			<label class="input-name"><span>Survey Category</span>
	                        <select name="survey_category_id_1" id="survey_category_id_1" >
	      						<option value="">Select Type</option>
	        					<?php foreach($survey_category as $category) { ?>
	                            <option value="<?php echo $category['SurveyCategories']['survey_category_id']; ?>"><?php echo $category['SurveyCategories']['survey_category_title']; ?>
	                             <?php } ?></option>
	              			</select>
	                    </label>
	           		</div>
	        	    <div id="error_category_1" class="error"></div>
	            	<div class="input-box">
	        			<label class="input-name"><span>Type</span>
	                        <select name="survey_question_type_1" id="survey_question_type_1" >
	                    		<option value="">Select Type</option>                   
	                    		<option value="C">Checkbox</option>
	                    		<option value="T">Textbox</option>
	                    		<option value="I">Image</option>
	                    		<option value="A">Audio</option>
                                <option value="R">Radio</option>
	                        </select>
	                    </label>
	    			</div>
	    			<div id="error_type_1" class="error"></div>
	                <div class="input-box">
	        			<label class="input-name"><span>Select Survey</span>
	                        <select name="survey_id_1" id="survey_id_1" >
	                    	    <option value="">Select Survey</option>                    
	                            <?php foreach($survey_lists as $survey_list) { ?>
	                            <option value="<?php echo $survey_list['Survey']['survey_id']; ?>"><?php echo $survey_list['Survey']['survey_name']; ?>
	                            <?php } ?>
	                            </option>
	                	    </select>
	                	</label>
	    			</div>
	            	<div id="error_survey_1" class="error"></div>
	                <div class="input-box">
	        			<label class="input-name"><span>Question Description</span></label>
	                	<input type="text" name="survey_question_description_1" id="survey_question_description_1"  />
	    			</div>
	            	<div id="error_questions_1" class="error"></div>
	            	<div class="input-box">
	        			<label class="input-name"><span>Question Marks</span></label>
	                	<input type="text" name="survey_question_marks_1" id="survey_question_marks_1" onkeypress="return onlyNumbers(event);"  />
	    			</div>
	            	<div id="error_marks_1" class="error"></div>
                    
                    <div id="apply_contentRadio_1" style="display:none;">
                    	
                        <div id="apply_contentRadioo_1" >
                            <div class="input-box" id="optionrr_1_1" >
                                 <label class="input-name">Option 1</label>
                                 <input type="text" name="optionr_1_1" id="optionr_1_1" value="">
								 
                            </div>
                            <div id="error_optionr1_1" class="error"></div>
                            
                            <div class="input-box" id="optionrr_1_2" >
                                 <label class="input-name">Option 2</label>
                                 <input type="text" name="optionr_1_2" id="optionr_1_2" value="">
								 
                            </div>
                            <div id="error_optionr1_2" class="error"></div>
                        </div>
                        
                        <a id="n_1" onClick="add_option(this.id)" class="btn btn-lg btn-custom btn-success" >Add Option</a>
						<a id="rn_1" onClick="remove_option(this.id)" class="btn btn-lg btn-custom btn-success" style="display:none;" >Remove Option</a>
                          
                    </div>
                    
	              	<div id="apply_content_1" style="display:none;">
		                <div class="input-box">
		        	         <label class="input-name">Option 1</label>
		            	     <input type="text" name="option_1_1" id="option_1_1" value="">
		             	</div>
		             	<div id="error_option1_1" class="error"></div> 
		             	<div class="input-box">
		                	<label class="input-name">Option 2</label>
		                 	<input type="text" name="option_1_2" id="option_1_2" >
		             	</div>
		             	<div id="error_option1_2" class="error"></div>
		                <div class="input-box">
		                	<label class="input-name">Option 3</label>
		                 	<input type="text" name="option_1_3" id="option_1_3" >
		             	</div>
		             	<div id="error_option1_3" class="error"></div>
		                <div class="input-box">
		                	<label class="input-name">Option 4</label>
		                	 <input type="text" name="option_1_4" id="option_1_4" value="">
		             	</div>
		             	<div id="error_option1_4" class="error"></div>
		                
	            	</div>
	        	</div>
	   		</div>
        	<div class="clearfix"></div>
        	<button type="submit" onclick="return validate_settings();" class="btn btn-lg btn-custom btn-success">Save All</button>
        	<a id="add_question" class="btn btn-lg btn-custom btn-success" >Add More Question</a>
        	<a id="remove_question" class="btn btn-lg btn-custom btn-success" style="display:none;" >Remove Question</a>
			</form>
		</div>

		
		<div id="predefined_add" style="display:none;" >
	
		<form method="post" enctype="multipart/form-data" action="<?php echo WEBSITE_ROOT.'clients/question_add'; ?>">
		
		<input type="hidden" name="pre_question" value="2" />
		
		<?php $i = 1; foreach($predefined_question_list as $question_list) { ?>
	    	
	       	<div id="apply_formdata" class="col-md-6">  
	            <div id="apply_question<?php echo $i ?>"  >
	    		    <h2 class="page-header"><input type="checkbox" name="question_count_<?php echo $i ?>" id="question_count_<?php echo $i ?>" value="<?php echo $i ?>" class="predefined_question" />Question <?php echo $i ?></h2>
					           		
	    			<div class="input-box">
	        			<label class="input-name"><span>Survey Category</span>
	                        <select name="survey_category_id_<?php echo $i ?>" id="survey_category_id_<?php echo $i ?>" >
	      						<option value="">Select Type</option>
	        					<?php foreach($survey_category as $category) { ?>
	                            <option value="<?php echo $category['SurveyCategories']['survey_category_id']; ?>"><?php echo $category['SurveyCategories']['survey_category_title']; ?>
	                             <?php } ?></option>
	              			</select>
	                    </label>
	           		</div>
	        	    <div id="error_category_<?php echo $i ?>" class="error"></div>
					
					<input type="hidden" name="survey_question_type_<?php echo $i ?>" id="survey_question_type_<?php echo $i ?>" value="<?php echo $question_list['SurveyQuestion']['survey_question_type'] ?>" />
					
	            	<div class="input-box">
	        			<label class="input-name"><span>Type</span>
	                        <select name="survey_question_type_<?php echo $i ?>" id="survey_question_type_<?php echo $i ?>" disabled >
	                    		<option value=""<?php if($question_list['SurveyQuestion']['survey_question_type']==''){ echo "selected='selected'"; } ?> >Select Type</option>                   
	                    		<option value="C"<?php if($question_list['SurveyQuestion']['survey_question_type']=='C'){ echo "selected='selected'"; } ?>>Checkbox</option>
	                    		<option value="T"<?php if($question_list['SurveyQuestion']['survey_question_type']=='T'){ echo "selected='selected'"; } ?>>Textbox</option>
	                    		<option value="I"<?php if($question_list['SurveyQuestion']['survey_question_type']=='I'){ echo "selected='selected'"; } ?>>Image</option>
	                    		<option value="A"<?php if($question_list['SurveyQuestion']['survey_question_type']=='A'){ echo "selected='selected'"; } ?>>Audio</option>
                                <option value="R"<?php if($question_list['SurveyQuestion']['survey_question_type']=='R'){ echo "selected='selected'"; } ?>>Radio</option>
	                        </select>
	                    </label>
	    			</div>
	    			<div id="error_type_<?php echo $i ?>" class="error"></div>
	                <div class="input-box">
	        			<label class="input-name"><span>Select Survey</span>
	                        <select name="survey_id_<?php echo $i ?>" id="survey_id_<?php echo $i ?>" >
	                    	    <option value="">Select Survey</option>                    
	                            <?php foreach($survey_lists as $survey_list) { ?>
	                            <option value="<?php echo $survey_list['Survey']['survey_id']; ?>"><?php echo $survey_list['Survey']['survey_name']; ?>
	                            <?php } ?>
	                            </option>
	                	    </select>
	                	</label>
	    			</div>
	            	<div id="error_survey_<?php echo $i ?>" class="error"></div>
	                <div class="input-box">
	        			<label class="input-name"><span>Question Description</span></label>
	                	<input type="text" name="survey_question_description_<?php echo $i ?>" id="survey_question_description_<?php echo $i ?>" value="<?php echo $question_list['SurveyQuestion']['survey_question_description']; ?>"  />
	    			</div>
	            	<div id="error_questions_<?php echo $i ?>" class="error"></div>
	            	<div class="input-box">
	        			<label class="input-name"><span>Question Marks</span></label>
	                	<input type="text" name="survey_question_marks_<?php echo $i ?>" id="survey_question_marks_<?php echo $i ?>" onkeypress="return onlyNumbers(event);" value="<?php echo $question_list['SurveyQuestion']['survey_question_marks']; ?>"  />
	    			</div>
	            	<div id="error_marks_<?php echo $i ?>" class="error"></div>
                    
					<?php $j = 0; if($question_list['SurveyQuestion']['survey_question_type']=='R') { ?>
                    <div id="apply_contentRadio_<?php echo $i ?>" >
                    	
                        <div id="apply_contentRadioo_<?php echo $i ?>" >
						
						<?php $j=1; foreach($question_list['SurveyQuestionOption'] as $option) { ?>
						
                            <div class="input-box" id="optionrr_<?php echo $i ?>_<?php echo $j ?>" >
                                 <label class="input-name">Option <?php echo $j ?></label>
                                 <input type="text" name="optionr_<?php echo $i ?>_<?php echo $j ?>" id="optionr_<?php echo $i ?>_<?php echo $j ?>" value="<?php echo $option['answer_text'] ?>">
								 
                            </div>
                            <div id="error_optionr<?php echo $i ?>_<?php echo $j ?>" class="error"></div>
                        <?php $j++; } ?>
							
                        </div>
                          
                    </div>
                    <?php } ?>
					
					<input type="hidden" name="questionr<?php echo $i ?>_count" id="questionr<?php echo $i ?>_count" value="<?php echo $j; ?>" />
					
					<?php $k = 0; if($question_list['SurveyQuestion']['survey_question_type']=='C') { ?>
					
	              	<div id="apply_content_<?php echo $i ?>" >
						
						<?php $k=1; foreach($question_list['SurveyQuestionOption'] as $option) { ?>
		                <div class="input-box">
		        	         <label class="input-name">Option <?php echo $k; ?></label>
		            	     <input type="text" name="option_<?php echo $i ?>_<?php echo $k ?>" id="option_<?php echo $i ?>_<?php echo $k ?>" value="<?php echo $option['answer_text'] ?>">
		             	</div>
		             	<div id="error_option<?php echo $i ?>_<?php echo $k ?>" class="error"></div> 
		             	<?php $k++; } ?>
						
	            	</div>
					
					<?php } ?>
					
	        	</div>
	   		</div>
        	<div class="clearfix"></div>
			
			<?php $i++; } ?>
			
			<input type="hidden" name="question_count" id="question_count" value="<?php echo $i; ?>" />
			
        	<button type="submit" onclick="return validate_predefine();" class="btn btn-lg btn-custom btn-success">Save All</button>
        
		
		</form>	
		</div>
		
   	</div>
</div>



	<?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			
	?>
	
    <script type="text/javascript">
		
		function add_option(clicked_id)
		{
			var option_count = clicked_id.split("_").pop();
			
			var questionr1_count = $('#questionr'+option_count+'_count').val();
			
			if(questionr1_count>=3)
			{
				$('#rn_'+option_count).show();
			}
			
			$.ajax({
						type: "POST",		   
						url: "<?php echo WEBSITE_ROOT.'clients/survey_question_radiooption'; ?>",
						data: "questionr1_count="+questionr1_count+"&option_count="+option_count,
						success: function(data){
								$('#questionr'+option_count+'_count').val((questionr1_count*1+1));
								$('#apply_contentRadioo_'+option_count).append(data);								
						}
					});
		}
		
		function remove_option(clicked_id)
		{
			var option_count = clicked_id.split("_").pop();
			
			var questionr1_count = $('#questionr'+option_count+'_count').val();
			
			var option_count1 = (questionr1_count*1-1);
				
			$('#optionrr_'+option_count+'_'+option_count1).remove();
			
			$('#questionr'+option_count+'_count').val((option_count1));
			
			if(option_count1==3)
			{
				$('#rn_'+option_count).hide();
			}
		}
		
		$(document).ready(function(){
		
	    var counter = $("#question_count").val();
		var var_count;
		
			$(this).change(function(event){
				
				var type_value = event.target.value;
				
				var type_id = event.target.id;
				
				var myString = type_id.split("_").pop();
				
				if(type_value=='C')
				{
					$('#apply_content_'+myString).show();
					$('#apply_contentRadio_'+myString).hide();
				}
				else if(type_value=='R')
				{
					$('#apply_contentRadio_'+myString).show();
					$('#apply_content_'+myString).hide();
				}
				else if(type_value=='T' || type_value=='I' || type_value=='A')
				{
					$('#apply_content_'+myString).hide();
					$('#apply_contentRadio_'+myString).hide();
				}
					
			});
			
			$("#add_question").click(function(){
				
					var question_count = $('#question_count').val();
					if(question_count>=2)
					{
						$('#remove_question').show();
					}
					$.ajax({
								type: "POST",		   
								url: "<?php echo WEBSITE_ROOT.'clients/survey_question_add'; ?>",
								data: "question_count="+question_count,
								success: function(data){
										$('#question_count').val((question_count*1+1));
										$('#apply_formdata').append(data);
										
								}
							});
				
			 });
		 
			 $("#remove_question").click(function(){
				
					var question_count = $('#question_count').val();
					
					var question_count1 = (question_count*1-1);
					
					$('#apply_question'+question_count1).remove();
					
					$('#question_count').val((question_count1));
					
					if(question_count1==2)
					{
						$("#remove_question").hide();						
					}
					
			 });
		});

	</script>

	
</body>
</html>