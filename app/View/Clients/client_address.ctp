<script>
    $(document).ready(function(){
     
        $('#settelmentTable').DataTable();

    });
</script>



<div id="page-wrapper">
            <div class="row">
                <h1 class="page-header">Location List</h1>
                
                <div class="jobpost-top">
                <div class="col-md-4">
                   <!-- <form class="ng-pristine ng-valid">
                        <input type="text" placeholder="search" class="form-control ng-pristine ng-valid ng-touched" data-ng-model="searchKeywords" data-ng-keyup="search()">
                    </form> -->
                </div>
                <div class="col-md-5 filter-result-info">
                    <!--<span class="ng-binding">
                        Showing 25/25 entries
                    </span> -->             
                </div>
                <div class="successmsg" align="center"><?php echo $this->Session->flash(); ?></div>
                <div class="col-md-3">
                  <?php echo $this->Html->link('Add Address', array('action' =>'add_address'),array( 'class' => 'btn-custom1 create-job pull-right')); ?>  
                </div>
                </div>
                <div class="clearfix"></div>

                <div class="table-custom shadow">
                    <table id="settelmentTable" class="table text-center">
                        <thead>
                            <tr>
                                <!--<th><i class="fa fa-briefcase"></i>QR Image</th>-->
                                <th><i class="fa fa-calendar"></i>Address(Postal Code)</th>
                                <th><i class="fa fa-clock-o"></i>Contact No.</th>
                                <!--<th><i class="fa fa-hourglass-half"></i>Status</th>-->
                                <th><i class="fa fa-bullhorn"></i>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 

                                foreach ($address_list as $list) { ?>
                                <tr>
                                    <!--<td><img src="<?php echo WEBSITE_ROOT.'media/qr_code/'.$list['ClientAddress']['qr_code_image']; ?>"></td>-->
                                    <td><?php echo $list['ClientAddress']['address'].'('.$list['ClientAddress']['postal_code'].')'; ?></td>
                                    <td><?php echo $list['ClientAddress']['phone_number']; ?></td>
                                    <?php  $status = $list['ClientAddress']['is_active'] ?>

                                    <?php /*?><td><?php if($status == 1) {
                                        echo $this->Html->link('Active', array('action'=> 'address_status_change/'.$list['ClientAddress']['client_address_id'].'/'.$list['ClientAddress']['is_active']), array('is_active' => '0', 'escape' => false));
                                        }  

                                        else{

                                            echo $this->Html->link('Inactive', array('action'=> 'address_status_change/'.$list['ClientAddress']['client_address_id'].'/'.$list['ClientAddress']['is_active']), array('is_active' => '1', 'escape' => false));
                                        }

                                        ?> </td><?php */?>
                                    <td><?php echo $this->Html->link('Edit', array('action' => 'edit_address/'.$list['ClientAddress']['client_address_id']));  ?></td>

                                    
                                </tr>
                                    
                                <?php }

                            ?>
                            
                        </tbody>
                    </table>


                    <footer class="table-footer">
                        <div class="row">
                            <!--<div class="col-md-6 page-num-info">
                                <span>
                                    Show 
                                    <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()" class="ng-valid ng-dirty ng-valid-parse ng-touched"><option value="0" label="3">3</option><option value="1" label="5">5</option><option value="2" selected="selected" label="10">10</option><option value="3" label="20">20</option></select> 
                                    entries per page
                                </span>
                            </div>
                            <div class="col-md-6 text-right pagination-container">
                                <ul class="pagination-sm pagination ng-isolate-scope ng-valid">
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">First</a>
                                    </li>
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">Previous</a>
                                    </li>
                                    <li class="ng-scope active">
                                      <a href="" class="ng-binding">1</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">2</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">3</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Next</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Last</a>
                                    </li>
                                </ul>
                            </div>-->
                        </div>
                    </footer>
                </div>
                
            </div>

        </div>


    <?php
            echo $this->Html->script('jquery.min');
            echo $this->Html->script('bootstrap.min');
            echo $this->Html->script('metisMenu.min');
            echo $this->Html->script('sb-admin-2');
            echo $this->Html->script('jquery.dataTables.min');
            echo $this->Html->css('jquery.dataTables.min');
            
    ?>


    <?php /*?><!-- jQuery -->
    <script src="scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="scripts/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="scripts/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="scripts/sb-admin-2.js"></script><?php */ ?>
</body>
</html>