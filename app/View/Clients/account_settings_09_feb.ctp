
  <div id="page-wrapper">
    <div class="row">
      <h1 class="page-header">Account Settings</h1>
      <div class="clearfix"></div>
      <div class="add-surveys-box shadow" id="account-setting">
        
            <div class="wrapper">
        <ul>
            <li class=""><a href="#tab-1">Account Settings</a></li>
            <li class=""><a href="#tab-2">Alerts Settings</a></li>
        </ul>
        <div class="clearfix"></div>
        <div class="tabscontent">
            <div id="tab-1" style="display: none;">
                <div class="col-md-5">
                    <h4>Change Password</h4><br/>
                    <input class="form-control ng-pristine ng-valid ng-touched" type="password" placeholder="Old Password"><br/>
                    <input class="form-control ng-pristine ng-valid ng-touched" type="password" placeholder="New Password"><br/>
                    <input class="form-control ng-pristine ng-valid ng-touched" type="password" placeholder="Confirm Password"><br/>
                    <button type="submit" class="btn-custom1 create-job pull-right" href="">Submit</button>
                </div>
                <div class="col-md-5">
                    <h4>Recive Notification Via</h4><br/>
                    <lable><input type="checkbox"> Email</lable>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="tab-2" style="display: none;">
            <button type="submit" class="btn-custom1 create-job pull-right" href="">Create New</button>
                <h4>Recive Alerts Via</h4>
                
                <lable><input type="checkbox"> Push Notifications</lable><br/>
                <lable><input type="checkbox"> Email</lable>

                <h4>Created Alerts</h4>
            <div class="">
                <div class="jobpost-top">
                <div class="col-md-4">
                    <form class="ng-pristine ng-valid">
                        <input type="text" placeholder="search" class="form-control ng-pristine ng-valid ng-touched" data-ng-model="searchKeywords" data-ng-keyup="search()">
                    </form>
                </div>
                <div class="col-md-5 filter-result-info">
                    <span class="ng-binding">
                        Showing 25/25 entries
                    </span>              
                </div>
                
                </div>
                <div class="clearfix"></div>

                <div class="table-custom shadow">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th><i class="fa fa-briefcase"></i>Alerts Name</th>
                                <th><i class="fa fa-calendar"></i>Alerts Pendding</th>
                                <th><i class="fa fa-clock-o"></i>Alerts Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            <tr>
                                <td>Alert One</td>
                                <td>Pendding</td>
                                <td>Created</td>
                            </tr>
                            
                        </tbody>
                    </table>
                    <footer class="table-footer">
                        <div class="row">
                            <div class="col-md-6 page-num-info">
                                <span>
                                    Show 
                                    <select data-ng-model="numPerPage" data-ng-options="num for num in numPerPageOpt" data-ng-change="onNumPerPageChange()" class="ng-valid ng-dirty ng-valid-parse ng-touched"><option value="0" label="3">3</option><option value="1" label="5">5</option><option value="2" selected="selected" label="10">10</option><option value="3" label="20">20</option></select> 
                                    entries per page
                                </span>
                            </div>
                            <div class="col-md-6 text-right pagination-container">
                                <ul class="pagination-sm pagination ng-isolate-scope ng-valid">
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">First</a>
                                    </li>
                                    <li class="ng-scope disabled">
                                      <a href="" class="ng-binding">Previous</a>
                                    </li>
                                    <li class="ng-scope active">
                                      <a href="" class="ng-binding">1</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">2</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">3</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Next</a>
                                    </li>
                                    <li class="ng-scope">
                                      <a href="" class="ng-binding">Last</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </footer>
                </div>

        </div>

            </div>
            
        </div>
        
    </div>
        
      </div>
    </div>
  </div>
</div>


<!--<script>
            function show(name){
    document.getElementById(name).style.display='block';
}
</script>-->
<script>
        var tabs;
        jQuery(function($) {
            tabs = $('.tabscontent').tabbedContent({loop: true}).data('api');

            // switch to tab...
            $('a[href=#click-to-switch]').on('click', function(e) {
                var tab = prompt('Tab to switch to (number or id)?');
                if (!tabs.switchTab(tab)) {
                    alert('That tab does not exist :\\');
                }
                e.preventDefault();
            });

            // Next and prev actions
            $('.controls a').on('click', function(e) {
                var action = $(this).attr('href').replace('#', '');
                tabs[action]();
                e.preventDefault();
            });
            
            
        });
    
    /* Tabs plugin for jQuery created by �scar Casajuana < elboletaire at underave dot net >
 * Copyright 2013-2015 �scar Casajuana (MIT License) */
;eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}(';(2($,g,h,i){"18 19";v j=2(c,d){v f={6:c.w().x(\'a\').k?c.w().x(\'a\'):\'.1a a\',t:\'.1b-1c\',C:m,o:m,D:m,u:\'1d\',10:\'1e-1f\',K:m},L=p,5=c.5(),n=h.n,q=g.11,7=M;d=$.1g(f,d);3(!(d.6 12 $)){d.6=$(d.6)}2 N(a){4 1h(5.9(a).k)}2 O(){4 7===0}2 13(a){4 a%1===0}2 P(){4 7===5.k-1}2 E(a){4 $(l).F(\'G\')===a}2 y(a){3(a 12 $){4{z:a,A:d.6.Q(a.R())}}3(13(a)){4{z:5.Q(a),A:d.6.Q(a)}}3(5.9(a).k){4{z:5.9(a),A:d.6.9(2(){4 E.S(l,[a])})}}4{z:5.9(\'#\'+a),A:d.6.9(2(){4 E.S(l,[\'#\'+a])})}}2 H(){4 d.6.r().R($(\'.\'+d.u))}2 T(a){++7;3(a===i)a=d.K;3(7<5.k){4 8(7,p)}s 3(a&&7>=5.k){4 8(0,p)}4 m}2 w(a){--7;3(a===i)a=d.K;3(7>=0){4 8(7,p)}s 3(a&&7<0){4 8(5.k-1,p)}4 m}2 o(a){3(L&&n!==i&&(\'U\'V n)){L=m;h.1i(2(){n.1j(M,\'\',a)},14)}7=H();3(d.o&&15 d.o===\'2\'){d.o(a,B())}}2 8(a,b){3(!a.1k().1l(/^#/)){a=\'#\'+y(a).z.F(\'W\')}3(!N(a)){4 m}d.6.r().1m(d.u);d.6.9(2(){4 E.S(l,[a])}).r().16(d.u);5.1n();3(b){3(n!==i&&(\'U\'V n)){n.U(M,\'\',a)}s{h.11.I=a}}5.9(a).1o(d.C,2(){3(d.C){o(a)}});3(!d.C){o(a)}4 p}2 X(a){4 8(a,p)}2 Y(e){8(q.I)}2 17(){3(N(q.I)){8(q.I)}s 3(d.6.r().9(\'.\'+d.u).k){8(d.6.r().9(\'.\'+d.u).R())}s 3(d.t&&5.x(d.t).k){5.Z(2(){3($(l).x(d.t).k){8("#"+$(l).F("W"));4 m}})}s{8("#"+5.9(":1p-1q").F("W"))}3(d.t){5.x(d.t).Z(2(){v a=y($(l).r());a.A.r().16(d.10)})}3(\'1r\'V h){$(h).1s(\'1t\',Y)}s{v b=q.G;h.1u(2(){3(b!==q.G){Y.1v(h.1w);b=q.G}},14)}3(d.D&&15 d.D===\'2\'){d.D(B())}}2 B(){4{\'1x\':X,\'8\':X,\'H\':H,\'y\':y,\'T\':T,\'w\':w,\'O\':O,\'P\':P}}17();4 B()};$.1y.1z=2(b){4 l.Z(2(){v a=1A j($(l),b);$(l).1B(\'B\',a)})}})(J.1C||J.1D||J.$,1E,J);',62,103,'||function|if|return|children|links|current|switchTab|filter|||||||||||length|this|false|history|onSwitch|true|loc|parent|else|errorSelector|currentClass|var|prev|find|getTab|tab|link|api|speed|onInit|filterTab|attr|href|getCurrent|hash|window|loop|firstTime|null|tabExists|isFirst|isLast|eq|index|apply|next|pushState|in|id|apiSwitch|hashSwitch|each|tabErrorClass|location|instanceof|isInt|100|typeof|addClass|init|use|strict|tabs|error|message|active|has|errors|extend|Boolean|setTimeout|replaceState|toString|match|removeClass|hide|show|first|child|onhashchange|bind|hashchange|setInterval|call|event|switch|fn|tabbedContent|new|data|jQuery|Zepto|document'.split('|'),0,{}));


    </script>
</body>
</html>