<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Survey Questions Report</h1>        
        <div class="col-md-6">                       
            <form action="<?php echo WEBSITE_ROOT."clients/job_reports_request";  ?>" method="post">
                <div class="group-job">                                      
                    <h2 class="sml-heading">Survey Name</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="survey_name"  name = "survey_name" class="input-field">
                                <option value="0">Select Survey Name</option>
                                    <?php foreach ($survey as $value) { ?>
                                <option value="<?php echo $value['surveys']['survey_id']; ?>"><?php echo $value['surveys']['survey_name']; ?></option>
                                    <?php } ?>
                            </select>
                        </label>
                    </div>     
                    <h2 class="sml-heading">Category Name</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="cat_name"  name = "cat_name" class="input-field">
                                <option value="0">Select Category Name</option>
                                    <?php foreach ($cat as $value) { ?>
                                <option value="<?php echo $value['survey_categories']['survey_category_id']; ?>"><?php echo $value['survey_categories']['survey_category_title']; ?></option>
                                    <?php } ?>
                            </select>
                        </label>
                    </div> 
                    <h2 class="sml-heading">Question Type</h2>
                    <div class="input-box">
                        <label class="input-name ">                            
                            <select id="que"  name = "que" class="input-field">
                                <option value="0">Select Question Type</option>
                                <option value="C">MCQ</option>
                                <option value="T">TEXT</option>
                                <option value="I">IMAGE</option>
                                <option value="A">AUDIO</option>
                                <option value="R">Radio</option>
                            </select>
                        </label>
                    </div> 
                    <div class="col-md-12 text-center">
                        <input type="button" id="genrate" onclick="survey_que_report();" value="Generate" class="btn btn-lg btn-custom btn-success">
                        <input type="button"  onclick="export_report();" value="Export" class="btn btn-lg btn-custom btn-success">
                        <!--<a href="javascript(void:0);" id="genrate" onclick="report_generate();" class="btn btn-lg btn-custom btn-success" >Genrate</a>-->
                    </div>
                </div>
            </form>
        </div>
        <div style="margin-top: 40%;" id="reportsDataJob"></div>        
    </div>
    <div id="chart_div" style="margin-top: 10%;"></div>
</div>

<?php
    echo $this->Html->script('jquery.js');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('metisMenu.min');
    echo $this->Html->script('sb-admin-2');
    echo $this->Html->script('bootstrap-datepicker.js');
echo $this->Html->script('googleChart');    
?>

<script>
    if (top.location != location) {
        top.location.href = document.location.href;
    }


    function survey_que_report()
    {
        survey_name = $("#survey_name").val();
        cat_name = $("#cat_name").val();
        que = $("#que").val();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/fetch_survey_question_report'
                . ''; ?>",
            data: {cat_name: cat_name, survey_name: survey_name, que: que},
            success: function (data) {
                $('#reportsDataJob').replaceWith($('#reportsDataJob').html(data));
            }

        });
        event.preventDefault();
        return false; //stop the actual form post !important!
    }
    function export_report()
    {
        survey_name = $("#survey_name").val();
        cat_name = $("#cat_name").val();
        que = $("#que").val();
        window.open(
                '<?php echo WEBSITE_ROOT.'clients/export_survey_que_report'; ?>' + '/' + survey_name + '/' + cat_name + '/' + que,
                '_blank' // <- This is what makes it open in a new window.
                );
    }

    $(function () {
        google.charts.load('current', {'packages': ['bar']});
        google.charts.setOnLoadCallback(drawChart);
    });

    function drawChart() {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "<?php echo WEBSITE_ROOT.'clients/getSurveyQuesGraphData'; ?>",
            //data: {startdate: startdate, type: type, rank: rank, test_status: test_status},
            success: function (response) {
                response = JSON.parse(response);
                var data = google.visualization.arrayToDataTable(response);

                var options = {
                    chart: {
                        title: 'Survey Question',
                        subtitle: 'Question Type',
                    },
                    bars: 'vertical',
                    vAxis: {format: 'decimal'},
                    height: 400,
                    colors: ['#1b9e77', '#d95f02', '#7570b3']
                };

                var chart = new google.charts.Bar(document.getElementById('chart_div'));

                chart.draw(data, google.charts.Bar.convertOptions(options));
            }

        });
    }
</script>
</body>
</html>