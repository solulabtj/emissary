<!DOCTYPE html>

<html lang="en">
<head>

<!-- Html Page Specific -->
<meta charset="utf-8">
<title>Emissary</title>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

<!-- CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css"/>
<link rel="stylesheet" href="css/animate.css"/>
<link rel="stylesheet" href="css/metisMenu.min.css">
<link rel="stylesheet" href="css/sb-admin-2.css"/>
<link rel="stylesheet" href="css/font-awesome.min.css">


<!-- Favicons -->
<!-- <link rel="icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->
<link rel="stylesheet" href="css/custom.css"/>



</head>

<body data-spy="scroll" data-target=".navMenuCollapse">

<!-- PRELOADER -->
<div id="preloader">
	<div class="battery inner">
		<div class="load-line"></div>
	</div>
</div>

<div id="wrapper">

        <!-- Navigation -->
	<nav class="navbar navbar-fixed-top navbar-slide innerpagetop">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand goto" href="index.html#wrap"> <img src="./images/logo_nav.png" alt="Your logo"/> </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right innerpagetop-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar nav-sidemenu" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li><a href="index.html"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a></li>                        
                        <li class="on"><a href="#"><i class="fa fa-briefcase fa-fw"></i>Jobs<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="panels-wells.html">Panels and Wells</a></li>
                                <li><a href="buttons.html">Buttons</a></li>
                                <li><a href="notifications.html">Notifications</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="icons.html"> Icons</a></li>
                                <li><a href="grid.html">Grid</a></li>
                            </ul>
                        </li>                        
                        <li><a href="index.html"><i class="fa fa-bell fa-fw"></i>Requests</a></li>
                        <li><a href="index.html"><i class="fa fa-file-text-o fa-fw"></i>Submissions</a></li>
                        <li><a href="index.html"><i class="fa fa-list-alt fa-fw"></i>Survey</a></li>
                        <li><a href="index.html"><i class="fa fa-wrench fa-fw"></i>Settings</a></li>
                        <li><a href="index.html"><i class="fa fa-credit-card fa-fw"></i>Payments</a></li>
                        <li><a href="index.html"><i class="fa fa-bars fa-fw"></i>Reports</a></li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Submission</h1>
                <div class="clearfix"></div>

                <div class="col-md-6">
                <div class="agent-details">
                    <div class="prof-pic">
                        <img src="images/prof-pic.jpg">
                    </div>
                    <ul>
                        <li><label>Agent Name</label>Amit Kothari</li>
                        <li><label>Job Id</label>54H107EF</li>
                        <li><label>Submitted On</label>10th January 2016, 03:50PM</li>
                        <li><label>Status</label>Pending Approval</li>
                    </ul>
                </div>
                </div>

                <div class="col-md-6">
                    <div class="input-box">
                        <label class="input-name"><span>Write a Review</span><textarea class="input-field" rows="2"></textarea></label>
                    </div>
                    <a href="" class="mybuttons grn">Approve</a>
                    <a href="" class="mybuttons red">Decline</a>
                </div>

                

                <div class="clearfix"></div>
                </div>

                <div class="submittion-section shadow">
                    <div class="col-md-3">
                    <h3>Answer Categories</h3>
                    <ul class="nav nav-tabs" role="tablist">
                         <li role="presentation" class="active"><a href="#1" role="tab" data-toggle="tab">Food Quality</a></li>
                         <li role="presentation"><a href="#2" role="tab" data-toggle="tab">Services</a></li>
                         <li role="presentation"><a href="#3" role="tab" data-toggle="tab">Response</a></li>
                         <li role="presentation"><a href="#4" role="tab" data-toggle="tab">Price</a></li>
                         <li role="presentation"><a href="#5" role="tab" data-toggle="tab">Location</a></li>
                         <li role="presentation"><a href="#6" role="tab" data-toggle="tab">Atmosphare</a></li>
                         <li role="presentation"><a href="#7" role="tab" data-toggle="tab">Delivery</a></li>
                    </ul>
                    </div>

                    <div class="col-md-9">
                        <div class="tab-content">
                    
                            <div role="tabpanel" class="tab-pane active" id="1">
                                <div class="sub-content">
                                    <p class="que">
                                        Lorem ipsum dolor sit amet, eos id malorum tincidunt. Cum eu fierent eleifend?
                                    </p>
                                    <div class="ans opt">
                                        <ul class="list-inline">
                                            <li><label><input name="option" type="radio">Option 1</label></li>
                                            <li><label><input name="option" type="radio">Option 2</label></li>
                                            <li><label><input name="option" type="radio">Option 3</label></li>
                                            <li><label><input name="option" type="radio">Option 4</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="sub-content">
                                    <p class="que">
                                        Lorem ipsum dolor sit amet, eos id malorum tincidunt?
                                    </p>
                                    <div class="ans">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                                    </div>
                                </div>
                                <div class="sub-content">
                                    <p class="que">
                                        Torquatos instructior eu ius, ut soleat scribentur consectetuer est. Tale utamur per ne?
                                    </p>
                                    <div class="ans">
                                        <audio controls>
                                          <source src="horse.mp3" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                        </audio>
                                    </div>
                                </div>
                                <div class="sub-content">
                                    <p class="que">
                                        Torquatos instructior eu ius, ut soleat scribentur consectetuer est. Tale utamur per ne?
                                    </p>
                                    <div class="ans">
                                        <a href="#" class="images-custom" data-toggle="modal" data-target="#lightbox"> 
                                            <img src="https://s3.amazonaws.com/ooomf-com-files/lqCNpAk3SCm0bdyd5aA0_IMG_4060_1%20copy.jpg" alt="...">
                                        </a>

                                        <a href="#" class="images-custom" data-toggle="modal" data-target="#lightbox"> 
                                            <img src="https://s3.amazonaws.com/ooomf-com-files/8H0UdTsvRFqe03hZkNJr_New%20York%20-%20On%20the%20rock%20-%20Empire%20State%20Building.jpg" alt="...">
                                        </a>

                                        <a href="#" class="images-custom" data-toggle="modal" data-target="#lightbox"> 
                                            <img src="https://s3.amazonaws.com/ooomf-com-files/Z3LXxzFMRe65FC3Dmhnp_woody_unsplash_DSC0129.jpg" alt="...">
                                        </a>

                                        

                                        <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <img src="" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                           

                        </div> <!--tab-content-->
                    </div> <!--col-md-9-->
                </div> <!--submittion-section-->


            </div>
        </div>





	<!-- jQuery -->
    <script src="scripts/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="scripts/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="scripts/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="scripts/sb-admin-2.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    var $lightbox = $('#lightbox');
    
    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };
    
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
});
</script>
</body>
</html>