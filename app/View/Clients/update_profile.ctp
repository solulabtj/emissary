<style>
    .error{
        color: red;
    }
</style>

<script type="text/javascript">
		
		function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	
		return true;
		}
		

        function showimagepreview(input) {
		
			var ext = $('#company_logo').val().split('.').pop().toLowerCase();
			if($.inArray(ext,['gif','png','jpg','jpeg']) == -1) {
			$('#company_logo').val("");
				alert('invalid file type! Please Select only image.');
				return false;
			}
		
            if (input.files && input.files[0]) {
            var filerdr = new FileReader();
            filerdr.onload = function(e) {
            $('#imgprvw').attr('src', e.target.result);
            }
            filerdr.readAsDataURL(input.files[0]);
            }
        }



         function show_comp_imagepreview(input) {
		 var ext = $('#company_image').val().split('.').pop().toLowerCase();
		 if($.inArray(ext,['gif','png','jpg','jpeg']) == -1) {
		 	$('#company_image').val("");
				alert('invalid file type! Please Select only image.');
				return false;
			}
		 
            if (input.files && input.files[0]) {
            var filerdr = new FileReader();
            filerdr.onload = function(e) {
            $('#comp_img_pr').attr('src', e.target.result);
            }
            filerdr.readAsDataURL(input.files[0]);
            }
        }
		
		function validate_update()
		{
			var error = false;
			
			var company_name = $('#company_name').val();
			var agency_name = $('#agency_name').val();
			var company_overview = $('#company_overview').val();
			var client_address = $('#client_address').val();
			var mobile_number = $('#mobile_number').val();
			
			//var paypal_id = $('#paypal_id').val();
			
			//var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			//var valid_email = regex.test(paypal_id);
			
			//alert(valid_email);
			
			var working_hour_start = $('#working_hour_start').val();
			var working_hour_end = $('#working_hour_end').val();
			
			var checked = $("input[type='checkbox']:checked").length;
			
			if(company_name=='')
			{
				$('#error_companyn').html('Company Name is required.');
				error = true;
			}
			else
			{
				$('#error_companyn').html('');
			}
			
			if(agency_name=='')
			{
				$('#error_agency').html('Agency Name is required.');
				error = true;
			}
			else
			{
				$('#error_agency').html('');
			}
			
			if(company_overview=='')
			{
				$('#error_companyo').html('Company Overview is required.');
				error = true;
			}
			else
			{
				$('#error_companyo').html('');
			}
			
			if(client_address=='')
			{
				$('#error_address').html('Company Address is required.');
				error = true;
			}
			else
			{
				$('#error_address').html('');
			}
			
			if(mobile_number=='')
			{
				$('#error_mobile').html('Mobile No. is required.');
				error = true;
			}
			else if(mobile_number.length<10 || mobile_number.length>10 )
			{
				$('#error_mobile').html('Mobile No. must be 10 digits.');
				error = true;
			}
			else
			{
				$('#error_mobile').html('');
			}
			
			/*if(valid_email===false)
			{
				$('#error_paypal').html('Enter valid email ID.');
				error = true;
			}
			else
			{
				$('#error_paypal').html('');
			}*/
			
			if(working_hour_start=='')
			{
				$('#error_workings').html('Start Working hour is required.');
				error = true;
			}
			else
			{
				$('#error_workings').html('');
			}
			
			if(working_hour_end=='' || working_hour_end==0)
			{
				$('#error_workinge').html('End Working hour is required or should not be Zero.');
				error = true;
			}
			else
			{
				$('#error_workinge').html('');
			}
			
			if(working_hour_end!='' && working_hour_start!='')
			{	
				
				if(working_hour_end-working_hour_start<=0)
				{
					$('#error_workingh').html('Start working hour should be less than End Working hour.');
					error = true;
				}
				else if(working_hour_end>=24 || working_hour_start>=24 )
				{
					$('#error_workingh').html('Start working hour and End Working hour should be between 0 and 23.');		
					error = true;
				}
				else
				{
					$('#error_workingh').html('');
				}
			}
			else
			{
				$('#error_workingh').html('');
			}
			
			if(checked==0)
			{
				$('#error_checked').html('Select at least one checkbox.');
				error = true;
			}
			else
			{
				$('#error_checked').html('');
			}
			
			if(error)
			{
				return false;
			}
		}
		
</script>

<?php
if(!empty($listing['ClientUserInfo']['company_logo']) && file_exists(WEBSITE_WEBROOT."media/img/".$listing['ClientUserInfo']['company_logo']))
{
	$company_logo = WEBSITE_ROOT."media/img/".$listing['ClientUserInfo']['company_logo'];	
}
else
{
	$company_logo = WEBSITE_ROOT."media/img/".'prof-pic.jpg';
}

	//echo $listing['ClientImage']['image_file_path']; echo "here";

if(!empty($listing['ClientImage']['image_file_path']) && file_exists(WEBSITE_WEBROOT."media/img/".$listing['ClientImage']['image_file_path']) )
{
    $company_image = WEBSITE_ROOT."media/img/".$listing['ClientImage']['image_file_path']; 
    $client_image_id = $listing['ClientImage']['client_image_id']; 
}
else
{
    $company_image = WEBSITE_ROOT."media/img/".'prof-pic.jpg';
    $client_image_id = 0;  
}
?>


<div id="page-wrapper">
            <div class="row">
	            <h1 class="page-header">Company Profile</h1>
                <div class="center" align="center">
				
				<?php echo $this->Session->flash(); 
					
					 $message = isset($message)?$message:'';
					 
					 echo $message;
				?>
                </div>
            	<form method="post" enctype="multipart/form-data" action="<?php echo WEBSITE_ROOT.'clients/update_profile'; ?>">
                
                <input type="hidden" name="client_id" value="<?php echo $this->Session->read('Auth.User.client_id'); ?>" />
                
                <?php //pr($listing); ?>
                
                <div class="edit_profsection">
                <div class="pic-banner shadow">
            	<div class="prof-pic">
            			<img id = "imgprvw" src="<?php echo $company_logo ?>">
            			<div class="fileUpload">
						    <span>Upload Photo</span>
						    <input type="file" id="company_logo" name="company_logo" class="upload" onchange="showimagepreview(this)" />
						</div>
            		</div>
                </div>
                	<div class="col-md-6">                		
                	<div class="input-box">
	                		<label class="input-name"><span>Company Name</span><input type="text" name="company_name" id="company_name" class="input-field" value="<?php echo $listing['ClientUserInfo']['company_name']; ?>" ></label>
                            
                            <div class="error" id="error_companyn" ></div>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Overview</span><textarea name="company_overview" id="company_overview" class="input-field" rows="3"><?php echo $listing['ClientUserInfo']['company_overview']; ?></textarea></label>
                            <div class="error" id="error_companyo" ></div>
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Phone Number</span><input type="text" name="mobile_number" id="mobile_number" class="input-field" onkeypress="return onlyNumbers(event)" value="<?php echo $listing['ClientUserInfo']['mobile_number']; ?>" ></label>
                            <div class="error" id="error_mobile" ></div>  
                		</div>       
                               		
                	</div>
                	<div class="col-md-6">
                		<div class="input-box">
	                		<label class="input-name"><span>Agency Name</span><input type="text" name="agency_name" id="agency_name" class="input-field" value="<?php echo $listing['ClientUserInfo']['agency_name']; ?>" ></label>
                            <div class="error" id="error_agency" ></div>  
                		</div>
                		<div class="input-box">
	                		<label class="input-name"><span>Address</span><textarea name="client_address" id="client_address" class="input-field" rows="3"><?php echo $listing['ClientUserInfo']['client_address']; ?></textarea></label>
                            <div class="error" id="error_address" ></div>  
                		</div>
                		<!--<div class="input-box">
	                		<label class="input-name"><span>Paypal ID</span><input type="text" name="paypal_id" id="paypal_id" class="input-field" value="<?php echo $listing['ClientUserInfo']['paypal_id']; ?>" ></label>
                            <div class="error" id="error_paypal" ></div>  
                		</div>-->
                        
                	</div>
                    
                    <div class="col-md-12">
                    		
                           <div class="input-box">
                           		<label >Company Policy</label>
                           		<textarea name="editor1" id="editor1" rows="10" cols="80"><?php echo $listing['ClientUserInfo']['company_policy']; ?></textarea>
                           
                           </div> 
                            
                    </div>
                    
                	<div class="col-md-12">
                		<h2 class="reg-header"><i class="fa fa-picture-o"></i>Pictures</h2>
                		<div class="editprof-images">
                			<ul>
                				
                				<li class="addmore">
                					<div class="gal-img">
                                        <div class="prof-pic">
                                            <input type="hidden" name="client_image_id" id="client_image_id" value="<?php echo $client_image_id; ?>">
                                                <img id ="comp_img_pr" src="<?php echo $company_image ?>">
                                                <div class="fileUpload">
                                                    <span>Upload Photo</span>
                                                    <input type="file" id="company_image" name="company_image" class="upload" onchange="show_comp_imagepreview(this)" />
                                                </div>
                                            </div>
                                        </div>
                						<!--<a href=""> 
                							<i class="fa fa-plus"></i>
                							<span>Add More</span>
            							</a> -->
                					</div>
                				</li>
                			</ul>
                		</div>

                		<div class="clearfix"></div>

                		<h2 class="reg-header"><i class="fa fa-clock-o"></i>Working Hours</h2>
                		<div class="work-detil">

                        <div class="col-md-3">
                            <div class="input-box">
                            <label class="input-name"><span>From</span><input type="text" name="working_hour_start" id="working_hour_start" maxlength="2" onkeypress="return onlyNumbers(event);" class="input-field" value="<?php echo $listing['ClientUserInfo']['working_hour_start']; ?>" ></label>
                            <div class="error" id="error_workings" ></div>  
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-box">
                            <label class="input-name"><span>To</span><input type="text" name="working_hour_end" id="working_hour_end" class="input-field" maxlength="2"  onkeypress="return onlyNumbers(event);" value="<?php echo $listing['ClientUserInfo']['working_hour_end']; ?>" ></label>
                            <div class="error" id="error_workinge" ></div>
                            
                            <div class="error" id="error_workingh" ></div>
                              
                        </div>
                        </div>
                		<div class="clearfix"></div>	
                		<div class="col-md-12">
                            <div class="wrk-dys">
                				<ul class="list-unstyled list-inline">
                					<li><label><input type="checkbox" name="monday" value="1"<?php if($listing['ClientUserInfo']['monday']=="1") { echo "checked='checked'"; }; ?> >Mon</label></li>
                					<li><label><input type="checkbox" name="tuesday" value="1"<?php if($listing['ClientUserInfo']['tuesday']=="1") { echo "checked='checked'"; }; ?> >Tue</label></li>
                					<li><label><input type="checkbox" name="wednesday" value="1"<?php if($listing['ClientUserInfo']['wednesday']=="1") { echo "checked='checked'"; }; ?> >Wed</label></li>
                					<li><label><input type="checkbox" name="thursday" value="1"<?php if($listing['ClientUserInfo']['thursday']=="1") { echo "checked='checked'"; }; ?> >Thu</label></li>
                					<li><label><input type="checkbox" name="friday" value="1"<?php if($listing['ClientUserInfo']['friday']=="1") { echo "checked='checked'"; }; ?> >Fri</label></li>
                					<li><label><input type="checkbox" name="saturday" value="1"<?php if($listing['ClientUserInfo']['saturday']=="1") { echo "checked='checked'"; }; ?> >Sat</label></li>
                					<li><label><input type="checkbox" name="sunday" value="1"<?php if($listing['ClientUserInfo']['sunday']=="1") { echo "checked='checked'"; }; ?> >Sun</label></li>
                				</ul>
                			</div>
                            <div class="error" id="error_checked" ></div>
                        </div>
                		</div>
                		<div class="clearfix"></div>

                		<button type="submit" onclick="return validate_update();" class="btn btn-lg btn-custom btn-success">Update</button>

                	</div>
                </div>
                
                </form>
                
            </div>

</div>


	<?php
			echo $this->Html->script('jquery.min');
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('metisMenu.min');
			echo $this->Html->script('sb-admin-2');
			
	?>
	
	
	
	<!-- jQuery -->
   <?php /*?> <script src="<?php echo WEBSITE_ROOT; ?>js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="<?php echo WEBSITE_ROOT; ?>js/sb-admin-2.js"></script><?php */?>
    
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

        <script type="text/javascript" src="<?php echo WEBSITE_ROOT ?>js/ckeditor/ckeditor.js"></script>
        
        <script>
        $(document).ready(function() { 
			
			CKEDITOR.replace('editor1');
			
		});
		
		</script>
    
</body>
</html>