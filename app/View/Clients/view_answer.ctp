<?php
/* pr($survey_category_ids);
  pr($survey_questions_alls);exit; */
?>

<script>
    $(document).ready(function () {

        $('#settelmentTable').DataTable();

    });
</script>

<style>

    .error { color:red; }

    div.stars {
        float: right;
    }

    input.star { display: none; }

    label.star {
        float: right;
        padding: 0;
        font-size: 20px;
        color: #444;
        transition: all .2s;
        margin: 0 5px;
        line-height: 1;
    }

    input.star:checked ~ label.star:before {
        content: '\f005';
        color: #FFA344;
        transition: all .25s;
    }

    input.star-5:checked ~ label.star:before {
        color: #FFA344;
    }

    input.star-1:checked ~ label.star:before { color: #FFA344; }

    label.star:hover { transform: rotate(-15deg) scale(1.3); }

    label.star:before {
        content: '\f005';
        font-family: FontAwesome;
    }
</style>

<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">User Answers</h1>
        <div class="clearfix"></div>
        <?php echo $this->Session->flash(); ?>
        <div class="col-md-6">
            <div class="agent-details">

                <?php foreach ($agent as $agent_data) { //pr($agent_data); ?>
                    <div class="prof-pic">
                        <?php if (empty($agent_data['AgentUserInfo']['profile_image'])) { ?>
                            <img src="<?php echo WEBSITE_ROOT . 'media/img/' . $agent_data['AgentUserInfo']['profile_image']; ?>">
                        <?php } else { ?>
                            <img src="<?php echo WEBSITE_ROOT ?>images/prof-pic.jpg">
                        <?php } ?>
                    </div>
                    <ul>
                        <li><label>Agent Name</label><?php echo $agent_data['AgentUserInfo']['agent_name']; ?></li>
                    <?php } ?>

                    <?php
                    foreach ($job_submission as $job_data) { //pr($job_data); 
                        $request_status = '';

                        if ($job_data['AgentJob']['completed_by_agent'] == 1 && $job_data['AgentJob']['disapproved_by_client'] == 0 && $job_data['AgentJob']['approved_by_client'] == 0) {
                            $request_status = 'Pending Approval';
                        } elseif ($job_data['AgentJob']['completed_by_agent'] == 1 && $job_data['AgentJob']['disapproved_by_client'] == 1 && $job_data['AgentJob']['approved_by_client'] == 0) {
                            $request_status = 'Disapproved';
                        } elseif ($job_data['AgentJob']['completed_by_agent'] == 1 && $job_data['AgentJob']['disapproved_by_client'] == 0 && $job_data['AgentJob']['approved_by_client'] == 1) {
                            $request_status = 'Approved';
                        }
                        ?>
                        <li><label>Job Id</label><?php echo $job_data['AgentJob']['job_id']; ?></li>
                        <li><label>Submitted On</label><?php echo date('m-d-Y', strtotime($job_data['AgentJob']['created'])); ?></li>
                        <li><label>Status</label><?php echo $request_status; ?></li>
                    <?php } ?>    
                </ul>
            </div>
        </div>

        <div class="col-md-6">
            <div class="input-box" id="after_change_approve_div" style="display:none;">
                <label class="input-name"><span>Write a Review
                    </span>
            </div>

            <?php
            foreach ($job_submission as $job_data) { //pr($job_data); 
                if ($job_data['AgentJob']['approved_by_client'] == '0' && $job_data['AgentJob']['disapproved_by_client'] == '0') {
                    ?>
                    <input type="hidden" name="agent_job_id" id="agent_job_id" value="<?php echo $job_data['AgentJob']['agent_job_id']; ?>" />
                    <input type="hidden" name="agent_id" id="agent_id" value="<?php echo $job_data['AgentJob']['agent_id']; ?>" />

                    <div class="input-box" id="before_change_approve_div" >
                        <label class="input-name"><span>Write a Review
                                <div class="stars"  >
                                    <input class="star star-1 rating_star" id="star-1" type="radio" name="rating_points" value="1"/>
                                    <label class="star star-1" for="star-1"></label>
                                    <input class="star star-2 rating_star" id="star-2" type="radio" name="rating_points" value="2"/>
                                    <label class="star star-2" for="star-2"></label>
                                    <input class="star star-3 rating_star" id="star-3" type="radio" name="rating_points" value="3"/>
                                    <label class="star star-3" for="star-3"></label> 
                                    <input class="star star-4 rating_star" id="star-4" type="radio" name="rating_points" value="4"/>
                                    <label class="star star-4" for="star-4"></label>
                                    <input class="star star-5 rating_star" id="star-5" type="radio" name="rating_points" value="5"/>
                                    <label class="star star-5" for="star-5"></label>
                                    <div id="error_rating" class="error"></div>
                                </div>
                            </span>
                            <textarea class="input-field" rows="2" name="rating_description" id="rating_description" ></textarea>
                        </label>
                        <div id="error_description" class="error"></div>
                    </div>

                    <a href="javascript:;" id="aprove_client" class="mybuttons grn">Approve</a>
                    <a href="javascript:;" id="disaprove_client" class="mybuttons red">Decline</a>

                    <div id="aproved_client" class="mybuttons grn" style="display:none;" ><?php echo "Accepted"; ?></div>
                    <div id="disaproved_client" class="mybuttons red" style="display:none;" ><?php echo "Declined"; ?></div>

                <?php } else if ($job_data['AgentJob']['approved_by_client'] == '1' && $job_data['AgentJob']['disapproved_by_client'] == '0') { ?>
                    <div class="mybuttons grn"><?php echo "Accepted"; ?></div>
                <?php } else if ($job_data['AgentJob']['approved_by_client'] == '0' && $job_data['AgentJob']['disapproved_by_client'] == '1') { ?>
                    <div class="mybuttons red" ><?php echo "Declined"; ?></div>
                <?php }
            }
            ?>
               <button name="print_pdf" id="print_pdf" class="mybuttons grn">Print PDF</button>
               <button name="print_excel" id="print_excel" class="mybuttons grn">Print Excel</button>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="submittion-section shadow">
        <div class="col-md-3">
            <h3>Answer Categories</h3>
            <ul class="nav nav-tabs" role="tablist">

                <!--<li role="presentation" class="active"><a href="#1" role="tab" data-toggle="tab">Food Quality</a></li>
                <li role="presentation"><a href="#2" role="tab" data-toggle="tab">Services</a></li>
                <li role="presentation"><a href="#3" role="tab" data-toggle="tab">Response</a></li>
                <li role="presentation"><a href="#4" role="tab" data-toggle="tab">Price</a></li>
                <li role="presentation"><a href="#5" role="tab" data-toggle="tab">Location</a></li>
                <li role="presentation"><a href="#6" role="tab" data-toggle="tab">Atmosphare</a></li>
                <li role="presentation"><a href="#7" role="tab" data-toggle="tab">Delivery</a></li>-->
                <?php
                foreach ($survey_category_ids as $survey_category_id) {

                    foreach ($category_list_singles[$survey_category_id] as $category_list_single) {
                        ?>
                        <li role="presentation" ><a href="#tab<?php echo $survey_category_id ?>" role="tab" data-toggle="tab"><?php echo $category_list_single['SurveyCategory']['survey_category_title']; ?></a></li>
                    <?php
                    }
                }
                ?>

            </ul>
        </div>

        <div class="col-md-9">
            <div class="tab-content">

                <?php /* foreach($survey_category_ids as $key=>$survey_category_id) { 

                  if($key == 1)
                  {
                  $active = "active";
                  }
                  else
                  {
                  $active = "";
                  } */
                ?>


                <?php
                $i = 1;
                foreach ($survey_questions_alls as $key => $survey_questions_all) {

                    if ($i == 1) {
                        $active = "active";
                    } else {
                        $active = "";
                    }
                    ?> 
                    <div role="tabpanel" class="tab-pane <?php echo $active ?>" id="tab<?php echo $key ?>">
                        <?php
                        foreach ($survey_questions_all as $question_list) {
                           // pr($question_list);
                            $aryData1 = array();
                            $aryData2 = array();

                            foreach ($data as $data_select) {
                                //echo $question_list['SurveyQuestion']['survey_question_id'];
                                foreach ($data_select[$question_list['SurveyQuestion']['survey_question_id']] as $data_select1) {
                                    // pr($data_select1);

                                    $aryData1[] = $data_select1['AgentSurveyAnswer']['survey_question_answer'];
                                    $aryData2 = $aryData1;
                                }
                                //pr($aryData2);
                                //exit;
                            }
                            //pr($aryData2);
                            ?>
                            <div class="sub-content">
                                <p class="que">
                    <?php echo $question_list['SurveyQuestion']['survey_question_description']; ?>
                                </p>

                                <?php
                                if ($question_list['SurveyQuestion']['survey_question_type'] == 'C') {
                                    ?>
                                    <div class="ans opt">
                                        <ul class="list-inline">
                                            <?php
                                            
                                            foreach ($question_list['SurveyQuestionOption'] as $question_option) {
                                               
                                                ?>


                                                <li><label><input name="option" type="checkbox" value="<?php echo $question_option['survey_option_id']; ?>"<?php
                                                        if (in_array($question_option['survey_option_id'], $aryData2)) {
                                                            echo "checked='checked'";
                                                        }
                                                        ?> >

                                            <?php echo $question_option['answer_text']; ?></label></li>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                
                                                 <?php
                                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'R') {
                                    
                                    ?>
                                    <div class="ans opt">
                                        <ul class="list-inline">
                                            <?php
                                            foreach ($question_list['SurveyQuestionOption'] as $question_option) {   
                                                //pr($question_option['survey_option_id']);
                                               // pr($aryData2);
                                                ?>                                               
                                                <li><label><input name="options<?php echo $question_option['survey_question_id'] ?>" type="radio" value="<?php echo $question_option['survey_option_id']; ?>"<?php
                                                        if (in_array($question_option['survey_option_id'], $aryData2)) {
                                                            echo "checked='checked'";
                                                        }
                                                        ?> >

                                            <?php echo $question_option['answer_text']; ?></label></li>
                                        <?php } ?>
                                        </ul>
                                    </div>

                                    <?php } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'T') { ?>

                                    <div class="ans">
                                        <?php foreach ($aryData2 as $aryData) { ?>	
                <?php echo $aryData; ?>
            <?php } ?>
                                    </div>


                                    <?php } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'I') { ?>

                                    <div class="ans">
            <?php foreach ($aryData2 as $aryData) { ?>
                                            <a href="#" class="images-custom" data-toggle="modal" data-target="#lightbox"> 
                                                <img src="<?php echo WEBSITE_ROOT . 'media/img/' . $aryData; ?>" alt="...">
                                            </a>
            <?php } ?>
                                        <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <img src="" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <?php } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'A') { ?>
                                    <div class="ans">
            <?php foreach ($aryData2 as $aryData) { ?>
                                            <audio controls>
                                                <source src="<?php echo WEBSITE_ROOT . 'media/img/' . $aryData; ?>" type="audio/mpeg">
                                                Your browser does not support the audio element.
                                            </audio>
                                    <?php } ?>
                                    </div>
                            <?php } ?>
                            </div>
                    <?php } ?>

                    </div>
    <?php $i++;
} ?>

            </div> <!--tab-content-->
        </div> <!--col-md-9-->
    </div> <!--submittion-section-->
</div>
</div>

<!-- jQuery -->
<script src="scripts/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="scripts/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="scripts/metisMenu.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="scripts/sb-admin-2.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        
        
        $("#print_pdf").on('click',function(){
            var id = '<?php echo $this->params['pass']['0']; ?>';
           window.open(
                '<?php echo WEBSITE_ROOT.'clients/printPdf'; ?>'+'/'+id,
                '_blank' // <- This is what makes it open in a new window.
                ); 
        });
        
        $("#print_excel").on('click',function(){
            var id = '<?php echo $this->params['pass']['0']; ?>';
           window.open(
                '<?php echo WEBSITE_ROOT.'clients/printExcel'; ?>'+'/'+id,
                '_blank' // <- This is what makes it open in a new window.
                ); 
        });
        
        
        var $lightbox = $('#lightbox');

        $('[data-target="#lightbox"]').on('click', function (event) {
            var $img = $(this).find('img'),
                    src = $img.attr('src'),
                    alt = $img.attr('alt'),
                    css = {
                        'maxWidth': $(window).width() - 100,
                        'maxHeight': $(window).height() - 100
                    };

            $lightbox.find('.close').addClass('hidden');
            $lightbox.find('img').attr('src', src);
            $lightbox.find('img').attr('alt', alt);
            $lightbox.find('img').css(css);
        });

        $lightbox.on('shown.bs.modal', function (e) {
            var $img = $lightbox.find('img');

            $lightbox.find('.modal-dialog').css({'width': $img.width()});
            $lightbox.find('.close').removeClass('hidden');
        });

        $("#aprove_client").click(function () {

            var ratingCount = 5;
            var agent_job_id = $('#agent_job_id').val();
            var agent_id = $('#agent_id').val();
            var error = false;

            if ($('input[name="rating_points"]:checked').length <= 0)
            {
                ratingCount = 0;
                $("#error_rating").html("Please Select rating.");
                error = true;
            } else
            {
                $('.rating_star').each(function () {

                    if ($(this).is(':checked'))
                    {
                        return false;
                    }
                    ratingCount--;
                    //alert(ratingCount);
                });
                $("#error_rating").html("");
            }

            var rating_description = $.trim($('#rating_description').val());

            if (rating_description == '')
            {
                $("#error_description").html("Please Write a review.");
                error = true;
            } else
            {
                $("#error_description").html("");
            }

            if (error)
            {
                return false;
            }

            $.ajax({
                type: "POST",
                url: "<?php echo WEBSITE_ROOT . 'clients/change_aproval'; ?>",
                data: "agent_job_id=" + agent_job_id + "&type=" + 1 + "&rating_description=" + rating_description + "&rating_points=" + ratingCount + "&agent_id=" + agent_id,
                success: function (data) {
                    if (data == 1)
                    {
                        $('#aprove_client').hide();
                        $('#disaprove_client').hide();
                        $('#before_change_approve_div').hide();
                        $('#after_change_approve_div').show();
                        $('#after_change_approve_div').html(rating_description);
                        $('#aproved_client').show();
                        alert('Accepted successfully.');
                    } else
                    {
                        alert('Error ! Try again.');
                    }
                }
            });

        });

        $("#disaprove_client").click(function () {

            var ratingCount = 5;
            var agent_job_id = $('#agent_job_id').val();
            var agent_id = $('#agent_id').val();
            var error = false;

            if ($('input[name="rating_points"]:checked').length <= 0)
            {
                ratingCount = 0;
                $("#error_rating").html("Please Select rating.");
                error = true;
            } else
            {
                $('.rating_star').each(function () {

                    if ($(this).is(':checked'))
                    {
                        return false;
                    }
                    ratingCount--;
                    //alert(ratingCount);
                });
                $("#error_rating").html("");
            }

            var rating_description = $.trim($('#rating_description').val());

            if (rating_description == '')
            {
                $("#error_description").html("Please Write a review.");
                error = true;
            } else
            {
                $("#error_description").html("");
            }

            if (error)
            {
                return false;
            }

            $.ajax({
                type: "POST",
                url: "<?php echo WEBSITE_ROOT . 'clients/change_aproval'; ?>",
                data: "agent_job_id=" + agent_job_id + "&type=" + 2 + "&rating_description=" + rating_description + "&rating_points=" + ratingCount + "&agent_id=" + agent_id,
                success: function (data) {
                    if (data == 1)
                    {
                        $('#aprove_client').hide();
                        $('#disaprove_client').hide();
                        $('#before_change_approve_div').hide();
                        $('#after_change_approve_div').show();
                        $('#after_change_approve_div').html(rating_description);
                        $('#disaproved_client').show();
                        alert('Declined successfully.');
                    } else
                    {
                        alert('Error ! Try again.');
                    }
                }
            });

        });

    });
</script>
</body>
</html>