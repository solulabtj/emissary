<!doctype html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<link rel="shortcut icon" href="<?php echo Router::url('/')?>img/favicon.ico" type="image/x-icon" />
<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo 'Solulab - Administration'//echo $title_for_layout; ?>
	</title>
	<?php
	echo $this->Html->scriptBlock('var webroot = "'.Router::url('/').'"');

	echo $this->Html->css('bootstrap');
	echo $this->Html->css('bootstrap.min');	
	echo $this->Html->css('style');	
	echo $this->Html->css('style-forms');	
	echo $this->Html->css('style-responsive');
	echo $this->Html->css('uniform.default');	
	echo $this->Html->css('halflings');	
	
	
	echo $this->Html->script('bootstrap');
	echo $this->Html->script('bootstrap.min');	
		
?>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
<script type="text/javascript">
	var mon = <?php echo isset($mon)?$mon:date('m');?>;
	var webroot = '<?php echo Router::url('/');?>';
	function pageLoad(){
		$('.accordion .head').click(function() {
			$(this).next().toggle('slow');
			return false;
		}).next().hide();
		
		$('.accordion .head1').click(function() {
			$(this).next().toggle('slow');
			return false;
		}).next().hide();
		$('.accordion .head').next().toggle('slow');
		$('#CouponEndDate').datepicker({
			showOn: 'both', buttonImage: '<?php echo Helper::url('/img/calendar.gif');?>',
			buttonImageOnly: true,
			dateFormat:'mm-dd-yy',
			onSelect: function( selectedDate ) {
				$( "#CouponStartDate" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
		$('#CouponStartDate').datepicker({
			showOn: 'both', buttonImage: '<?php echo Helper::url('/img/calendar.gif');?>',
			buttonImageOnly: true,
			minDate: new Date(<?php echo date('Y'); ?>, <?php echo ((int) date('m'))-1; ?>,<?php echo (int) date('d'); ?>),
			dateFormat:'mm-dd-yy',
			onSelect: function( selectedDate ) {
				$( "#CouponEndDate" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		$('#DiscountEndDate').datepicker({
			showOn: 'both', buttonImage: '<?php echo Helper::url('/img/calendar.gif');?>',
			buttonImageOnly: true,
			dateFormat:'mm-dd-yy',
			onSelect: function( selectedDate ) {
				$( "#DiscountStartDate" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
		$('#DiscountStartDate').datepicker({
			showOn: 'both', buttonImage: '<?php echo Helper::url('/img/calendar.gif');?>',
			buttonImageOnly: true,
			minDate: new Date(<?php echo date('Y'); ?>, <?php echo ((int) date('m'))-1; ?>,<?php echo (int) date('d'); ?>),
			dateFormat:'mm-dd-yy',
			onSelect: function( selectedDate ) {
				$( "#DiscountEndDate" ).datepicker( "option", "minDate", selectedDate );
			}
		});
	}
$(function (){pageLoad();});
	</script>


</head>
<body>
<?php
 	if($this->Session->check('Auth.Admin')){?>
<div id="btmbg">
  <div id="wrapper">
  	<div class="welcome_block"><b class="last_login">Last Login Date:</b> <b class="day_time"><?php echo $this->Time->format('l, F j ,Y h:i A',$this->Session->read('Auth.Admin.lastlogin2'));?></b>   <b class="last_login">&nbsp;&nbsp;|&nbsp;&nbsp;Welcome <?php echo ucfirst($this->Session->read('Auth.Admin.name'));?>&nbsp;&nbsp;</b></div>
    <!-- Header Part -->
    <div id="headerpart">
      <div class="headerleft">&nbsp;</div>
      <div class="navipart">
        <div class="mainttl bold">Rental Cluster - Admin Panel</div>
        <div class="navigation">
          <ul class="navi">
            <li><span>Dash Board</span></li>
				<li><?php echo $this->Html->link('<span class="selected">Operations</span>',array('plugin'=>null,'controller'=>'sitesettings','action'=>'index#0:0','web'=>true),array('escape'=>false,'class'=>'selected')); ?></li>	
				<li><span>Reports</span></li>
            <li style="float:right;"><?php echo $this->Html->link('<span class="selected">Logout</span>',array('plugin'=>null,'controller'=>'admins','action'=>'logout','web'=>true),array('class'=>'selected','escape'=>false));?></li>
          </ul>
        </div>
		
      </div>
      <div class="headerright">&nbsp;</div>
    </div>
    <!-- Header Part -->
    <!-- Center Part -->
    <div id="centerpart">
      <div class="leftpart">
        <div class="headerttl">
          <div class="headingleft">&nbsp;</div>
          <div class="ttl">Administration Menu</div>
          <div class="headingright">&nbsp;</div>
        </div>
        <div class="lettcontent">
        	<?php echo $this->element('admin_menu');?>
          
          <div class="clear"><br />
            &nbsp;</div>
        </div>
      </div>
      <div class="rightpart">
        <div class="headerttl">
          <div class="headingleft">&nbsp;</div>
          <div class="ttl">Administration Panel</div>
          <div class="headingright">&nbsp;</div>
        </div>
        <div class="centercontent ajaxdiv2">
			<?php
			echo $content_for_layout; ?>
          <div class="clear"><br />&nbsp;</div>
        </div>
      </div>
    </div>
  
  <!-- Center Part -->
  <!-- Center Part -->
  <div id="footerpart" class="bold">
    <div class="footerleft">
    <div class="footerright">
    	<div class="footerttl">&copy; <?php echo date('Y'); ?> www.RentalCluster.com. All rights reserved. Powered By Avista Technologies, LLC.</div>
    </div>
	</div>
  </div>
  <!-- Center Part -->
  </div>
</div>
<?php }else if(isset($isForgetPassword) && $isForgetPassword){?>
<div class="bg"> <b class="bg_top"></b>
  <div class="bg_middle">
	<div class="login_bg">
  		<b class="login_bg_top"></b>

        <div class="login_bg_middle">
        	<b class="signin_title">RESET PASSWORD</b>
			<?php
   if ($this->Session->check('Message.flash')):
       $this->Session->flash();
	endif;
?>
			<?php echo (isset($errors['auth'])?('<div class="message">'.$errors['auth'].'</div>'):'');?>
            <div class="login_box">
            	<?php
    echo $this->Form->create('Admin',array('plugin'=>null,'controller'=>'admins','action' => 'forgetpassword', 'web'=>true));
?>
                	<b class="login_label">Email : </b><b class="login_text_field"><?php echo $this->Form->text('Admin.email', array('class'=>'element text medium','maxlength'=>'255','tabindex'=>'1'));?></b>                    
					<b class="login_label"></b><b class="login_btn"><?php echo $this->Form->submit('admin/send.jpg',array('tabindex'=>'3'));?></b>
					<b class="login_label"><?php echo $this->Form->submit('admin/back.jpg',array('tabindex'=>'3','onclick'=>'self.location.href=\''.Router::url('/').'web/\';return false;'));?></b>
                <?php echo $this->Form->end();?>
                <div class="clear"></div>
            </div>
        </div>
        <b class="login_bg_bottom"></b>
	</div>
  </div>
  <b class="bg_bottom"></b> </div>
  <script type="text/javascript">document.getElementById('AdminEmail').focus();</script>
<?php }else{
//if not logged in
if($this->Session->check('Auth.Admin')){
	$errors['auth'] = 'You are not authorised to login to Admin panel';
}
?>

<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
				<div class="login-box">
					<div class="icons">
						<a href="index.html"><i class="halflings-icon home"></i></a>
						<a href="#"><i class="halflings-icon cog"></i></a>
					</div>
					<h2>SIGN IN TO ADMIN PANEL sdfsd</h2>
					
					<?php //echo $this->Form->create('Admin',array('controller'=>'admins','action' => 'admin_login', 'web'=>false),array('class'=>'form-horizontal')); ?>
						<form class="form-horizontal" action="admins/login" method="post">
						<fieldset>
							
							<div class="input-prepend" title="Username">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<?php echo $this->Form->text('Admin.email', array("name"=>"username",'id'=>"username",'class'=>'input-large span10','placeholder'=>"type username",'maxlength'=>'255','tabindex'=>'1'));?>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
							</div>
							<div class="clearfix"></div>
					
							<div class="button-login">	
								<?php echo $this->Form->submit('Login',array('class'=>'btn btn-primary','type'=>'submit'));?>
							</div>
							<div class="clearfix"></div>
					<?php echo $this->Form->end();?>
					
				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div>

<div class="bg"> <b class="bg_top"></b>
  <div class="bg_middle">
	<div class="login_bg">
  		<b class="login_bg_top"></b>
        <div class="login_bg_middle">
        	<b class="signin_title">SIGN IN TO ADMIN PANEL</b>
        	<?php echo $this->Session->flash('auth'); ?>
			<?php echo (isset($errors['auth'])?('<div class="message">'.$errors['auth'].'</div>'):'');?>
			<?php echo (isset($errors['email'])?('<div class="message">'.$errors['email'][0].'</div>'):'');?>
			<?php echo (isset($errors['password'])?('<div class="message">'.$errors['password'].'</div>'):'');?>
            <div class="login_box">
            	<?php /* echo $this->Form->create('Admin',array('plugin'=>null,'controller'=>'admins','action' => 'login', 'web'=>true)); ?>
                	<b class="login_label">Email : </b><b class="login_text_field"><?php echo $this->Form->text('Admin.email', array('class'=>'element text medium','maxlength'=>'255','tabindex'=>'1', 'style'=>'font-size:17px;'));?></b>
                    <b class="login_label">Password : </b><b class="login_text_field"><?php echo $this->Form->password('Admin.password', array('class'=>'element text medium','maxlength'=>'16','tabindex'=>'2', 'style'=>'font-size:17px;'));?></b>
					<b class="login_label"></b><b class="login_text_field"><?php echo $this->Html->link('Forgot your password',array('plugin'=>null,'controller'=>'admins','action'=>'forgetpassword','web'=>true));?></b>
                    <b class="login_label"></b><b class="login_btn"><?php echo $this->Form->submit('admin/login_btn.jpg',array('tabindex'=>'3'));?></b>                    
                <?php echo $this->Form->end(); */ ?>
                <div class="clear"></div>
            </div>
        </div>
        <b class="login_bg_bottom"></b>
	</div>
  </div>
  <b class="bg_bottom"></b> </div>
  <script type="text/javascript">//document.getElementById('AdminEmail').focus();</script>
<?php }?>

</body>
</html>
<?php echo $this->element('sql_dump'); ?>