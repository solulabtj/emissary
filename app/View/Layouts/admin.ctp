<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeDescription = 'Solulab - Adminpanel';
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

	//echo $this->Html->css('bootstrap');
	echo $this->Html->css('bootstrap.min');	
	echo $this->Html->css('style');	
	echo $this->Html->css('style-forms');	
	echo $this->Html->css('style-responsive');
	echo $this->Html->css('uniform.default');	
	echo $this->Html->css('halflings');	
	echo $this->Html->css('font-awesome.min');
        
        
        echo $this->Html->css('jquery.dataTables.min');
	
	echo $this->Html->script('jquery-1.11.3');
        
        
        echo $this->Html->script('jquery.dataTables.min');
        
        
	//echo $this->Html->script('jquery-1.9.1.min');
	//echo $this->Html->script('jquery-migrate-1.0.0.min');
	//echo $this->Html->script('jquery-ui-1.10.0.custom.min');
	//echo $this->Html->script('custom');
	//echo $this->Html->script('bootstrap');
	echo $this->Html->script('bootstrap.min');	
	echo $this->Html->script('jquery.validate');
        echo $this->Html->script('mask');
        
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
                
                
                
                
	?>
</head>
<body>
    <style>
        .errormsg{
            color: red;
            text-align: center;
        }
        
        .successmsg{
            color: green;
            text-align: center;
        }
        
    </style>    
    
    
<?php if($this->Session->check('Auth.User')){  ?>
	<?php echo $this->element('header');?>
<?php } ?>	
		<div class="container-fluid-full">
		<div class="row-fluid">
		<?php if($this->Session->check('Auth.User')){?>
			<?php echo $this->element('leftmenu');?>	
			
		<?php } ?>	
			<?php echo $this->fetch('content'); ?>			
		  </div>
		</div>
		<div class="clearfix"></div>
		
                <?php
                if($this->Session->check('Auth.User')){
                    ?>
                        <footer>
                            <?php 
                                echo $this->element('footer');
                            ?>
                        </footer>	
                    <?php
                }
                ?>
                
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
