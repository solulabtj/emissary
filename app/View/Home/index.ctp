
	<!-- INTRO BEGIN -->
	<header id="full-intro" class="intro-block bg-color-grad" >
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<!-- <img class="logo" src="./images/logo.png" alt="Your Logo" height="70" width="144" /> -->
					<h1 class="slogan">We pay for your shopping experience.</h1>
					<p class="sub-text">Mystery shoppers give valuable feedback to companies by rating and reviewing their customer service experience. Get paid to shop? How do I sign up? Emissary connects shoppers directly to opportunities, and brands directly to the feedback they need to improve their sales performance and strategy.</p>
					<a class="download-btn ios-btn" href="#">
						<img src="<?php echo WEBSITE_ROOT?>images/playstore_btn.png"/>
					</a>
					<a class="download-btn ios-btn" href="#">
						<img src="<?php echo WEBSITE_ROOT?>images/appstore_btn.png">
					</a>
				</div>
				<div class="col-md-4 hidden-sm hidden-xs">
					<img class="intro-screen wow bounceInUp" data-wow-delay="0.5s" src="<?php echo WEBSITE_ROOT ?>images/intro_screen.png" alt="" />
				</div>
			</div>
		</div>
		<div class="block-bg" data-stellar-ratio="0.4"></div>
	</header>
	<!-- INTRO END -->
	
    

	<!-- FEATURES BEGIN -->
	<section id="innovations">
		<div class="container">
			<div class="title">
				<h2>Your Technology Solution</h2>
				<p class="sub-heading">Emissary is an innovative new platform for brands to collect and analyze feedback straight from the consumer.  What services do we offer? Here are a few:</p>				
				

				<ul class="facts-list">
				<li class="wow bounceIn icn-1">
					<div class="facts-list-icon"><img src="<?php echo WEBSITE_ROOT ?>images/icon-1.png"></div>
					<h4>Experience Audits</h4>
					<p>Are your customers receiving the level and type of customer service experience you associate with your brand?</p>
				</li>
				<li class="wow bounceIn icn-2" data-wow-delay="0.4s">
					<div class="facts-list-icon"><img src="<?php echo WEBSITE_ROOT ?>images/icon-2.png"></div>
					<h4>Price Checks</h4>
					<p>Determine whether there is pricing accuracy in your stores.</p>
				</li>
				<li class="wow bounceIn icn-3" data-wow-delay="0.8s">
					<div class="facts-list-icon"><img src="<?php echo WEBSITE_ROOT ?>images/icon-3.png"></div>
					<h4>Promotion Audits</h4>
					<p>Ensure that promotions are being displayed and highlighted properly by your sales team.</p>
				</li>
				<li class="wow bounceIn icn-4" data-wow-delay="1.2s">
					<div class="facts-list-icon"><img src="<?php echo WEBSITE_ROOT ?>images/icon-4.png"></div>
					<h4>Phone & Email audits</h4>
					<p>Are your customers receiving the quality customer service and technical assistance by phone and email that you strive for?</p>
				</li>
			</ul>
			</div>
		</div>
	</section>
	<!-- FEATURES END -->


	<!-- BENEFITS1 BEGIN -->
	<section id="benefits1" class="img-block-2col bg-color2">
		<div class="">
			<div class="row">
				
				<div class="col-md-6 bg-2 wow bounceIn">
					<div class="bgblack-overlay"></div>
					<div class="lg-icon">
						<div class="benefits_l">
							<img src="<?php echo WEBSITE_ROOT ?>images/icon-5.png" class="static_image">
						</div>
					</div>
					<h2 class="front-title">Complete the mystery shopping & audits task to earn money</h2>
					<a href="" class="lg-button">Signup as agent</a>
					<a href="" class="lg-button">Become an agent</a>
				</div>
				
				<div class="col-sm-6 process">
					<!-- <div class="title">
						<h2>OUR Benefits</h2>
					</div> -->
					
					<ul class="item-list-left">
						<li class="wow fadeInRightBig">
							<div class="benefits_r"><img src="<?php echo WEBSITE_ROOT ?>images/icon-6.png" class="static_image"></div>
							<h4 class="color">Find Jobs</h4>
							<p>Have some spare time? Become an agent. Use the Emissary app on your phone to find jobs in your area.</p>
						</li>
						<li class="wow fadeInRightBig">
							<div class="benefits_r"><img src="<?php echo WEBSITE_ROOT ?>images/icon-7.png" class="static_image"></div>
							<h4 class="color">Complete Tasks</h4>
							<p>Completing tasks has never been so easy. Rate and fill out simple questionnairesstraight from your phone after completing easy tasks such as: taking photos; asking questions of the sales associates; or confirming the price of certain products.</p>
						</li>
						<li class="wow fadeInRightBig">
							<div class="benefits_r"><img src="<?php echo WEBSITE_ROOT ?>images/icon-8.png" class="static_image"></div>
							<h4 class="color">Get paid</h4>
							<p>Track your earnings through your phone. When you complete a job, your work will be reviewed and a payment credited to your account.</p>
						</li>
					</ul>
				</div>				
			</div>
		</div>
	</section>
	<!-- BENEFITS1 END -->
	
	
	
	
	<!-- BENEFITS2 BEGIN -->
	<section id="benefits2" class="img-block-2col">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="title small">
						<h2>MYSTERY SHOPPING industries</h2>
						<p class="text-center">As a leading mystery shopping company in India, we leverage our panel of Mystery Shoppers to execute nation-wide brand audits. If you are looking to put your marketing operations to some serious tests, make us your watchdogs and we will help you see what your customers see on the shop floor.</p>
					</div>

				<div class="industries">
					<div class="col-md-4">
						<div class="bg-4">
							<div class="industries_icons"><img src="<?php echo WEBSITE_ROOT ?>images/industries_1.png" class="static_image"></div>
							<p>Hospitality</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="bg-4">
							<div class="industries_icons"><img src="<?php echo WEBSITE_ROOT ?>images/industries_2.png" class="static_image"></div>
							<p>Retail</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="bg-4">
							<div class="industries_icons"><img src="<?php echo WEBSITE_ROOT ?>images/industries_3.png" class="static_image"></div>
							<p>Telecom</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="bg-4">
							<div class="industries_icons"><img src="<?php echo WEBSITE_ROOT ?>images/industries_4.png" class="static_image"></div>
							<p>Healthcare</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="bg-4">
							<div class="industries_icons"><img src="<?php echo WEBSITE_ROOT ?>images/industries_5.png" class="static_image"></div>
							<p>Wellness & Beauty</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="bg-4">
							<div class="industries_icons"><img src="<?php echo WEBSITE_ROOT ?>images/industries_6.png" class="static_image"></div>
							<p>Entertainment</p>
						</div>
					</div>
				</div>
					
				</div>
				<div class="col-md-5 col-md-offset-1 col-sm-6">
					<div class="screen-couple-right wow fadeInRightBig">
						<div class="flare">
							<img class="base wow" src="<?php echo WEBSITE_ROOT ?>images/flare_base.png" alt="" />
							<img class="shapes wow" src="<?php echo WEBSITE_ROOT ?>images/flare_shapes.png" alt="" />
						</div>
						<img class="highlight-left wow" src="<?php echo WEBSITE_ROOT ?>images/light.png" height="192" width="48" alt="" />
						<img class="highlight-right wow" src="<?php echo WEBSITE_ROOT ?>images/light.png" height="192" width="48" alt="" />
						<img class="screen above" src="<?php echo WEBSITE_ROOT ?>images/screen_couple_above.png" alt="" height="484" width="250" />						
						<img class="screen beyond wow fadeInRight" data-wow-delay="0.5s" src="<?php echo WEBSITE_ROOT ?>images/screen_couple_beyond.png" alt="" height="407" width="210" />
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- BENEFITS2 END --> 
	
	
	
	
	<!-- TESTIMONIALS BEGIN -->
	<section id="testimonials" class="bg-color3">
		<div class="container-fluid">
			
			<div class="test-tit">
				<div class="bg-white">
					<h2>Testimonials</h2>
					<h4>This is what our Clients say about us</h4>
				</div>
			</div>

			<div class="testi">
				<div class="qt-icon">
					<img src="<?php echo WEBSITE_ROOT ?>images/testimonials_icon.png" class="static_image">
				</div>
				
				<div id="testimonials-slider" class="owl-carousel">

					<div class="item container">
						<div class="talk">This is a great product. It reveals an individual approach to each customer. I liked the level of the provided service.</div>
						<div class="name">John Doe</div>
						<div class="ocupation">First customer</div>
					</div>	
					<div class="item container">
						<div class="talk">I always thought that people used to pay much for quality. But these guys changed my opinion. The quality exceeds the price many times. I recommend it to everybody.</div>
						<div class="name">John Doe</div>
						<div class="ocupation">First customer</div>
					</div>
					<div class="item container">
						<div class="talk">My colleague recommended them to me. I hesitated for a long time, but than I tried and understood what I had paid off for.</div>
						<div class="name">John Doe</div>
						<div class="ocupation">First customer</div>
					</div>
					<div class="item container">
						<div class="talk">This is a great product. It reveals an individual approach to each customer. I liked the level of the provided service.</div>
						<div class="name">John Doe</div>
						<div class="ocupation">First customer</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="block-bg"></div>
	</section>
	<!-- TESTIMONIALS END -->

<script type="text/javascript">

	

</script>