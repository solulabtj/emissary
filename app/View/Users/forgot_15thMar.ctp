<style>
	
	.error
	{  color:#FFF;  }

</style>


<!-- Modal -->
    <div class="container" style="margin-top: 75px;">
    <div class="col-md-6 col-md-offset-3">
        <div class="tab-pane fgt-block">
            <div class="loginmodal-container">
                <h1>Reset Password</h1><br>
                
              <form method="post" >
              
              <input type="hidden" name="user_type" id="user_type" value="<?php echo $user_type; ?>" />
              
              <input type="hidden" name="vercode" id="vercode" value="<?php echo $vercode; ?>" />
              
              <input type="hidden" name="email_address" id="email_address" value="<?php echo $email_address; ?>" />
			  
               
                <input class="form-control" type="password" name="new_password" id="new_password" placeholder="New Password">
                
                <div id="error_password1" class="error" ></div>
                
                <input class="form-control" type="password" name="c_password" id="c_password" placeholder="Confirm Password">
                <div id="error_password2" class="error" ></div>
                
                <div id="error_confirm" class="error" ></div>
                
                <div id="error_main" ></div>
                		
                <input type="submit" onclick="return validate();" name="login"  class="login loginmodal-submit"  value="Submit">
              </form>                
            </div>
            <div class="disclaimer">
              
            </div>
        </div>
        </div>
    </div>
	
<script type="text/javascript">

	function validate()
    {
          var error = false;
		  
		  var vercode = $('#vercode').val();
		  
		  var user_type = $('#user_type').val();
		  
		  var email_address = $('#email_address').val();
		  
		  var new_password = $('#new_password').val();
		  
		  var c_password = $('#c_password').val();
		            
		  if(new_password=='')
          {
            $('#error_password1').html('Please Enter New password.');
            error = true;
          }	
		  else if(new_password.length<8)
          {
            $('#error_password1').html('New Password must be 8 characters.');
            error = true;
          }	 
          else
          {
            $('#error_password1').html('');
          }
		  
		  if(c_password=='')
          {
            $('#error_password2').html('Please Enter confirm password.');
            error = true;
          }	
		  else if(c_password.length<8)
          {
            $('#error_password2').html('Confirm Password must be 8 characters.');
            error = true;
          }	 
          else
          {
            $('#error_password2').html('');
          }
		  
		  if(new_password!='' && c_password!='' && new_password!=c_password)
          {
            $('#error_confirm').html('New Password and Confirm Password should be matched.');
            error = true;
          }		 
          else
          {
            $('#error_confirm').html('');
          }
		  
          if(error)
          {
               return false; 
          }
		  
		  $.ajax({
							type: "POST",		   
							url: "<?php echo WEBSITE_ROOT.'users/forgot'; ?>",
							data: "user_type="+user_type+"&vercode="+vercode+"&new_password="+new_password+"&email_address="+email_address,
							success: function(data){							
									$('#error_password').html('');
									$('#error_password1').html('');
									$('#error_password2').html('');
									$('#error_main').html(data);
							}
				});
				
				event.preventDefault();
		  
    }

</script>