<!DOCTYPE html>

<html lang="en">
    <head>

        <!-- Html Page Specific -->
        <meta charset="utf-8">
        <title>Emissary</title>

        <!-- Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

        <!--[if lt IE 9]>
            <script type="text/javascript" src="scripts/html5shiv.js"></script>
        <![endif]-->

        <!-- CSS -->

        <?php
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('animate');
        echo $this->Html->css('simple-line-icons');
        echo $this->Html->css('icomoon-soc-icons');
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('magnific-popup');
        echo $this->Html->css('style');
        echo $this->Html->css('custom');
        echo $this->Html->css('datepicker');
        ?>

        <!-- Favicons -->
        <!-- <link rel="icon" href="images/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->


        <style>	

            .error { color:red; }

        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script>

            function validate_forgot()
            {
                var error = false;

                var username = $.trim($('#username_forgot').val());

                var checked = $("input[name='user_type']:checked").length;

                var user_type = $("input[name='user_type']:checked").val();


                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                var username_valid = regex.test(username);

                if (checked === 0)
                {
                    $('#error_typef').html('Please select Type.');
                    error = true;
                } else
                {
                    $('#error_typef').html('');
                }

                if (username == '')
                {
                    $('#error_username').html('Please Enter Email Address.');
                    error = true;
                } else if (username_valid === false)
                {
                    $('#error_username').html('Please Enter valid Email Address.');
                    error = true;
                } else
                {
                    $('#error_username').html('');
                }


                if (error)
                {
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: "<?php echo WEBSITE_ROOT . 'users/forgot_password'; ?>",
                    data: "user_type=" + user_type + "&username=" + username,
                    success: function (data) {
                        $('#error_type').html('');
                        $('#error_username').html('');
                        $('#notify').html(data);
                    }
                });

                event.preventDefault();

            }

            function validate_login(value)
            {
               

                var username = $("#username_login").val();
                var password = $("#password_login").val();
                //var loginType = $('input[name="loginType"]:checked').val();
                var loginType = "";
                var error = false;
                
                if(value == "c"){
                    loginType="company";
                }else{
                    loginType="agent";
                }

                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                var username_valid = regex.test(username);

                if (username == '')
                {
                    $('#username_error').html('Please Enter email Address.');
                    error = true;
                } else if (username_valid === false)
                {
                    $('#username_error').html('Please Enter valid email Address.');
                    error = true;
                } else
                {
                    $('#username_error').html('');
                }

                if (password == '')
                {
                    $('#password_error').html('Please Enter Password.');
                    error = true;
                } else
                {
                    $('#password_error').html('');
                }

               /* if (loginType === undefined)
                {
                    $('#error_type_login').html('Please Select user Type.');
                    error = true;
                } else
                {
                    $('#error_type_login').html('');
                }*/

                if (error)
                {
                    return false;
                }

                //alert(loginType);
                $.ajax({
                    type: "POST",
                    url: "<?php echo WEBSITE_ROOT . 'users/login'; ?>",
                    data: 'username=' + username + "&password=" + password + "&loginType=" + loginType,
                    success: function (data) {
                        //alert(data);

                        if ((data == "success" && loginType == "company") && (data = "redirectClient"))
                        {
                            //alert('login');
                            $("#login_error").html('');
                            window.location.href = "<?php echo WEBSITE_ROOT ?>clients/index";
                        } else if ((data == "success" && loginType == "agent") && (data = "redirectAgent"))
                        {
                            //alert('login');
                            $("#login_error").html('');
                            window.location.href = "<?php echo WEBSITE_ROOT ?>agents/availableShops";
                        } else
                        {
                            //alert(data);
                            $("#login_error").html(data);
                            return false;
                        }
                        //alert('outside');
                    }
                });

                return false;

            }

            $(document).ready(function () {
                $("#user-password-reset").hide();
                $("#fgt").click(function () {
                    $("#user-password-reset").show(500);
                    $("#userLogin").hide(500);
                });
                $("#nt_fgt").click(function () {
                    $("#user-password-reset").hide(500);
                    $("#userLogin").show(500);
                });
            });
        </script>
    </head>

    <body class="login">
        <div class="overlay"></div>
        <div class="login_wraper">

            <div class="login_card" style="width:500px;margin-top:150px;">
                <a href="<?php echo WEBSITE_ROOT ?>">
                    <img class="login-Logo" src="<?php echo WEBSITE_ROOT ?>images/logo_nav_dark.png" alt="">
                </a>

                <form novalidate method="post" id="userLogin" class="login-form" autocomplete="off">
                   <!-- <label class="style-inline"><input id="agent" value="agent" name="loginType" type="radio" class="">Agent</label>
                    <label class="style-inline"><input id="company" value="company" name="loginType" type="radio" class="">Company</label>-->
                    
                    <div class="error" id="error_type_login" ></div>

                    <input id="username_login" name="username_login" type="text" placeholder="Email Address" class="input_custom">

                    <div id="username_error" class="error" ></div>

                    <input id="password_login" name="password_login" type="password" placeholder="Password" class="input_custom">

                    <div id="password_error" class="error" ></div>

                    <div id="login_error" class="error"></div>

                    <a id="fgt" class="password-reset" onclick="">Forgot your password?</a>
                    <br>
                    <div>
                    <input style="width: 48%;float:left;" id="login" name="login" class="login-button" onclick="return validate_login('c');" type="submit" value="LOGIN AS A COMPANY" margin-bottom="5px">
                    <input style="width: 48%; float:left;margin-left: 8px;margin-top: 20px;" id="clogin" name="clogin" class="login-button" onclick="return validate_login('a');" type="submit" value="LOGIN AS AN AGENT" margin-bottom="5px">
                    </div>
                    <span class="create-account-info" style="display: inline;">Don't have an account? <a id="create-account-link" href="<?php echo WEBSITE_ROOT . 'users/signup' ?>">Create one now</a>.</span>
                </form>
                <form method="post" action="" id="user-password-reset" class="login-form" autocomplete="off">
                    <div id="password-reset-msg"></div>

                    <label class="style-inline"><input id="agent" value="agent" name="user_type" type="radio" class="">Agent</label>
                    <label class="style-inline"><input id="company" value="company" name="user_type" type="radio" class="">Company</label>

                    <div id="error_typef" class="error" ></div>

                    <input id="username_forgot" name="username_forgot" type="email" placeholder="Email Address" class="input_custom">
                    <div id="error_username" class="error" ></div>

                    <div id="notify" ></div>

                    <a id="nt_fgt" class="password-reset" onclick="">Login</a>
                    <input id="login" onclick="return validate_forgot();" name="login" class="login-button login-button-submit password-reset-submit" type="submit" value="Reset Password" margin-bottom="5px">
                </form>
            </div>	

        </div>
    </body>
</html>