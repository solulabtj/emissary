<!DOCTYPE html>

<html lang="en">
<head>

<!-- Html Page Specific -->
<meta charset="utf-8">
<title>Emissary</title>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

<!-- CSS -->

<?php
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('animate');
	echo $this->Html->css('simple-line-icons');
	echo $this->Html->css('icomoon-soc-icons');
	echo $this->Html->css('font-awesome.min');
	echo $this->Html->css('magnific-popup');
	echo $this->Html->css('style');
	echo $this->Html->css('custom');
	echo $this->Html->css('datepicker');
?>

<!-- Favicons -->
<!-- <link rel="icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->


<style>	
	
	.error { color:red; }

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>
	
	function validate_forgot()
    {
          var error = false;
		  
		  var vercode = $('#vercode').val();
		  
		  var user_type = $('#user_type').val();
		  
		  var email_address = $('#email_address').val();
		  
		  var new_password = $('#new_password').val();
		  
		  var c_password = $('#c_password').val();
		            
		  if(new_password=='')
          {
            $('#error_password1').html('Please Enter New password.');
            error = true;
          }	
		  else if(new_password.length<8)
          {
            $('#error_password1').html('New Password must be 8 characters.');
            error = true;
          }	 
          else
          {
            $('#error_password1').html('');
          }
		  
		  if(c_password=='')
          {
            $('#error_password2').html('Please Enter confirm password.');
            error = true;
          }	
		  else if(c_password.length<8)
          {
            $('#error_password2').html('Confirm Password must be 8 characters.');
            error = true;
          }	 
          else
          {
            $('#error_password2').html('');
          }
		  
		  if(new_password!='' && c_password!='' && new_password!=c_password)
          {
            $('#error_confirm').html('New Password and Confirm Password should be matched.');
            error = true;
          }		 
          else
          {
            $('#error_confirm').html('');
          }
		  
          if(error)
          {
               return false; 
          }
		  
		  $.ajax({
							type: "POST",		   
							url: "<?php echo WEBSITE_ROOT.'users/forgot'; ?>",
							data: "user_type="+user_type+"&vercode="+vercode+"&new_password="+new_password+"&email_address="+email_address,
							success: function(data){							
									$('#error_password').html('');
									$('#error_password1').html('');
									$('#error_password2').html('');
									$('#error_main').html(data);
							}
				});
				
				event.preventDefault();
		  
    }
	
</script>
</head>

<body class="login">
<div class="overlay"></div>
	<div class="login_wraper">
		
		<div class="login_card">
			<a href="<?php echo WEBSITE_ROOT ?>">
				<img class="login-Logo" src="<?php echo WEBSITE_ROOT ?>images/logo_nav_dark.png" alt="">
			</a>

			<form novalidate method="post" id="userLogin" class="login-form" autocomplete="off">
				
              <input type="hidden" name="user_type" id="user_type" value="<?php echo $user_type; ?>" />
              
              <input type="hidden" name="vercode" id="vercode" value="<?php echo $vercode; ?>" />
              
              <input type="hidden" name="email_address" id="email_address" value="<?php echo $email_address; ?>" />
                
            <input id="new_password" name="new_password" type="password" placeholder="New Password" class="input_custom">
            
            <div id="error_password1" class="error" ></div>
            
            <input id="c_password" name="c_password" type="password" placeholder="Confirm Password" class="input_custom">
            
            <div id="error_password2" class="error" ></div>
            
            <div id="error_confirm" class="error"></div>
            
            <div id="error_main" ></div>
                
                
            <input id="login" name="login" class="login-button" onclick="return validate_forgot();" type="submit" value="Reset Password" margin-bottom="5px">
                
            </form>
            
		</div>	

	</div>
</body>
</html>