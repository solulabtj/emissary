<!DOCTYPE html>

<html lang="en">
<head>

<!-- Html Page Specific -->
<meta charset="utf-8">
<title>Emissary</title>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

<!-- CSS -->

<?php
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('animate');
	echo $this->Html->css('simple-line-icons');
	echo $this->Html->css('icomoon-soc-icons');
	echo $this->Html->css('font-awesome.min');
	echo $this->Html->css('magnific-popup');
	echo $this->Html->css('style');
	echo $this->Html->css('custom');
	echo $this->Html->css('datepicker');
?>

<!-- Favicons -->
<!-- <link rel="icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
	
	function validate_signup()
	{			
		var f_name = $("#f_name").val();
		var l_name = $("#l_name").val();
		var email = $("#email").val();
		var mobile = $("#mobile").val();
		var password = $("#password").val();
		var register_type = $('input[name="loginType"]:checked').val();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  		var email_valid = regex.test(email);
		var error = false;
		var errorMsg = '';
		//alert(register_type);
		
		if(register_type===undefined)
		{
			$('#error_type').html('Please Select Type.');
			error=true;
		}
		else
		{
			$('#error_type').html('');
		}
		
		if(f_name =='')
		{
			$('#error_fname').html('Please Enter First Name.');
			error=true;
		}
		else
		{
			$('#error_fname').html('');
		}
		
		if(l_name =='')
		{
			$('#error_lname').html('Please Enter Last Name.');
			error=true;
		}
		else
		{
			$('#error_lname').html('');
		}
		
		if(email =='')
		{
			$('#error_email').html('Please Enter Email Address.');
			error=true;
		}
		else if(email_valid===false)
		{
			$('#error_email').html('Please Enter Valid Email Address.');
			error=true;
		}
		else
		{
			$('#error_email').html('');
		}

		if(mobile =='')
		{
			$('#error_mobile').html('Please Enter Mobile No.');
			error=true;
		}		
		else if(mobile.length != 10)
		{			
			$('#error_mobile').html('Mobile number must be exaclty 10 digits.');
			error=true;
		}
		else
		{
			$('#error_mobile').html('');
		}
		if(password =='')
		{			
			$('#error_password').html('Please enter your desired password.');
			error=true;
		}
		else if (password.length < 8) {			
			$('#error_password').html('Password length must be greater than 8.');
			error=true;
		}
		else
		{
			$('#error_password').html('');
		}
		
		if(error)
		{
			return false;
		}
	}
	
	function lettersOnly(evt) {
		//alert('hello');
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		  ((evt.which) ? evt.which : 0));
		if (charCode == 32)
			return true;
		if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		  (charCode < 97 || charCode > 122)) {
			return false;
		}
		else
			return true;
	}
	
	function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		{			
			return false;
		}
	
		return true;
	}

</script>

<style type="text/css">

	.error { color:red; }
	
</style>

</head>


<body class="fgt" data-spy="scroll" data-target=".navMenuCollapse">



<div id="wrap"> 

	<!-- NAVIGATION BEGIN -->
	<nav class="navbar navbar-fixed-top navbar-slide">
			<div class="container"> 
				<a class="navbar-brand goto" href="<?php echo WEBSITE_ROOT ?>"> <img src="<?php echo WEBSITE_ROOT; ?>images/logo_nav.png" alt="Your logo"/> </a>
				<!-- <a class="contact-btn icon-envelope" data-toggle="modal" data-target="#modalContact"></a> -->
				<button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<div class="collapse navbar-collapse navMenuCollapse">
					<ul class="nav">
						<li><a href="#features">About</a> </li>
						<li><a href="#benefits1">Agents</a></li>
						<li><a href="#screenshots">Brands</a></li>
						<li><a href="#pricing-table">Download</a></li>
						<li><a href="#testimonials">Contact Us</a></li>
						<li><a href="<?php echo WEBSITE_ROOT.'users/login' ?>" >Login / Register</a></li>
					</ul>
				</div>
			</div>
	</nav>
	<!-- NAVIGAION END -->
<div class="signup_wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
            <?php $new_message = isset($message)?$message:''; echo $new_message; ?>
            
            
			<h1 class="post-title">Create an account to start collecting fast, affordable, location-specific information and insights today</h1>
			
			<form method="post" action="<?php echo WEBSITE_ROOT.'users/signup' ?>" id="userLogin" class="login-form" autocomplete="off">
				<div class="col-md-6">
					<label>First Name</label>
					<input id="f_name" name="f_name" onkeypress="return lettersOnly(event);" type="text" class="input_custom">
                    <div class="error" id="error_fname" ></div>
				</div>
                
				<div class="col-md-6">
					<label>Last Name</label>
					<input id="l_name" name="l_name" onkeypress="return lettersOnly(event);" type="text" class="input_custom">
                    <div class="error" id="error_lname" ></div>
				</div>		
                	
				<div class="col-md-6">
					<label>Email Address</label>
					<input type="text" id="email" name="emailid" class="input_custom">
                    <div class="error" id="error_email" ></div>
				</div>
                
                <div class="col-md-6">
					<label>Mobile No</label>
					<input id="mobile" name="mobile" onkeypress="return onlyNumbers(event);" type="text" class="input_custom">
                    <div class="error" id="error_mobile" ></div>
				</div>
                
				<div class="col-md-6">
					<label>Password</label>
					<input id="password" name="password" type="password" class="input_custom">
                    <div class="error" id="error_password" ></div>
				</div>
                
				<!--<div class="col-md-6">
					<label>Confirm Password</label>
					<input id="Confirm-Password" name="f_name" type="password" class="input_custom">
				</div>-->
				<div class="col-md-12">
				<label>Choose which type of account you'd like to create.</label>
					<label><input id="agent" name="loginType" value="agent" type="radio" class="">Agent</label>
					<label><input id="provider" name="loginType" value="provider" type="radio" class="">Company</label>
                    <div class="error" id="error_type" ></div>				
				</div>
                
				<div class="col-md-6">
					<input id="login" onclick="return validate_signup();" name="login" class="login-button" type="submit" value="CREATE AN ACCOUNT" margin-bottom="5px">
				</div>
			</form>


			</div>
			<div class="col-md-4">
				<h1 class="post-title">Looking to make money with your smartphone?</h1>

				<div class="col-md-12">
				
				<a href="" class="download_img">
					<img src="<?php echo WEBSITE_ROOT ?>images/playstore_btn.png">
				</a>
				<a href="" class="download_img">
					<img src="<?php echo WEBSITE_ROOT ?>images/appstore_btn.png">
				</a>
				<p>Download the Emissary app.&nbsp;<a href="" target="_blank">Click here to learn more.</a></p>
				</div>
			</div>
		</div>
	</div>
</div>	
	

</body>

</html>