<style>
	
	.error
	{  color:#FFF;  }

</style>


<!-- Modal -->
    <div class="container" style="margin-top: 75px;">
    <div class="col-md-6 col-md-offset-3">
        <div class="tab-pane fgt-block">
            <div class="loginmodal-container">
                <h1>Forgot Password</h1><br>
                <p class="sm-p">Enter your email address and we’ll send you an email with instructions to reset your password.</p>
              <form method="post" >
			  
			  	<input type="radio" name="user_type" value="agent" >Agent &nbsp;
				
				<input type="radio" name="user_type" value="client" >Client
				
				<div id="error_type" class="error" ></div>
				
                <input class="form-control" type="text" name="username" id="username" placeholder="Email Id">
				
				<div id="error_username" class="error" ></div>
				
				<div id="notify" ></div>
				
                <input type="submit" onclick="return validate();" name="login" class="login loginmodal-submit"  value="Submit">
              </form>                
            </div>
            <div class="disclaimer">
              
              <!--<p>If you don't receive an email from us within a few minutes, check your spam filter as sometimes they end up in there. The email will be from <strong>help@emissary.com</strong>.</p>-->
            </div>
        </div>
        </div>
    </div>
	
<script type="text/javascript">

	function validate()
    {
          var error = false;

          var username = $.trim($('#username').val());
		  
		  var checked = $("input[name='user_type']:checked").length;
		  
		  var user_type = $("input[name='user_type']:checked").val();
		  
		  
		  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  var username_valid = regex.test(username);
          
		  if(checked===0)
          {
            $('#error_type').html('Please select Client or Agent.');
            error = true;
          }
		  else
		  {
		  	$('#error_type').html('');
		  }
		  
          if(username=='')
          {
            $('#error_username').html('Please Enter Email Address.');
            error = true;
          }
		  else if(username_valid===false)
		  {
		  	$('#error_username').html('Please Enter valid Email Address.');
            error = true;
		  }
          else
          {
            $('#error_username').html('');
          }
          
		  
          if(error)
          {
               return false; 
          }
		  
		  $.ajax({
							type: "POST",		   
							url: "<?php echo WEBSITE_ROOT.'users/forgot_password'; ?>",
							data: "user_type="+user_type+"&username="+username,
							success: function(data){							
									$('#error_type').html('');
									$('#error_username').html('');
									$('#notify').html(data);
							}
				});
				
				event.preventDefault();
		  
    }

</script>