<!DOCTYPE html>

<html lang="en">
<head>

<!-- Html Page Specific -->
<meta charset="utf-8">
<title>Emissary | Setting</title>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">


<?php
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('animate');
	echo $this->Html->css('metisMenu.min');
	echo $this->Html->css('sb-admin-2');
	echo $this->Html->css('font-awesome.min');
	echo $this->Html->css('datepicker');
	//echo $this->Html->css('jquery.mobile-1.4.5.min');
	echo $this->Html->css('custom');
?>

<?php
	echo $this->Html->script('jquery.min');
	//echo $this->Html->script('jquery.mobile-1.4.5.min');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('bootstrap-datepicker');
	echo $this->Html->script('metisMenu.min');
	echo $this->Html->script('sb-admin-2');
	
	require('js/customJS.php');
	//echo $this->action;exit;
	$view = $this->action;
	$jsFileName = $view . '_js.php';
	$jsFilePath = 'js/view_js/' . $jsFileName;
	if(file_exists($jsFilePath)) {	
		include_once($jsFilePath);
	}
	//pr($_SERVER);exit;
	
?>

<!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

<!-- CSS -->
<!--<link rel="stylesheet" href="css/bootstrap.min.css"/>
<link rel="stylesheet" href="css/animate.css"/>
<link rel="stylesheet" href="css/metisMenu.min.css">
<link rel="stylesheet" href="css/sb-admin-2.css"/>
<link rel="stylesheet" href="css/font-awesome.min.css">-->

<!-- Favicons -->
<!-- <link rel="icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->

</head>

<body data-spy="scroll" data-target=".navMenuCollapse">

<!-- PRELOADER -->
<div id="preloader">
  <div class="battery inner">
    <div class="load-line"></div>
  </div>
</div>
<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <nav class="navbar navbar-fixed-top navbar-slide innerpagetop">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand goto" href="<?php echo WEBSITE_ROOT.'agents/availableShops' ?>"> <img src="<?php echo WEBSITE_ROOT ?>/images/logo_nav.png" alt="Your logo"/> </a> </div>
    <!-- /.navbar-header -->
    
    <ul class="nav navbar-top-links navbar-right innerpagetop-nav">
      <!--<li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i> </a>
        <ul class="dropdown-menu dropdown-messages">
          <li> <a href="#">
            <div> <strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em> </span> </div>
            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div> <strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em> </span> </div>
            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div> <strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em> </span> </div>
            <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
            </a> </li>
          <li class="divider"></li>
          <li> <a class="text-center" href="#"> <strong>Read All Messages</strong> <i class="fa fa-angle-right"></i> </a> </li>
        </ul>
        <!-- /.dropdown-messages --> 
      </li>
      <!-- /.dropdown -->
      <!--<li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i> </a>
        <ul class="dropdown-menu dropdown-tasks">
          <li> <a href="#">
            <div>
              <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
              <div class="progress progress-striped active">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
              </div>
            </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div>
              <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
              <div class="progress progress-striped active">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
              </div>
            </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div>
              <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
              <div class="progress progress-striped active">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
              </div>
            </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div>
              <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
              <div class="progress progress-striped active">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
              </div>
            </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a> </li>
        </ul>
        <!-- /.dropdown-tasks --> 
      </li>
      <!-- /.dropdown -->
      <!--<li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i> </a>
        <ul class="dropdown-menu dropdown-alerts">
          <li> <a href="#">
            <div> <i class="fa fa-comment fa-fw"></i> New Comment <span class="pull-right text-muted small">4 minutes ago</span> </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div> <i class="fa fa-twitter fa-fw"></i> 3 New Followers <span class="pull-right text-muted small">12 minutes ago</span> </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div> <i class="fa fa-envelope fa-fw"></i> Message Sent <span class="pull-right text-muted small">4 minutes ago</span> </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div> <i class="fa fa-tasks fa-fw"></i> New Task <span class="pull-right text-muted small">4 minutes ago</span> </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a href="#">
            <div> <i class="fa fa-upload fa-fw"></i> Server Rebooted <span class="pull-right text-muted small">4 minutes ago</span> </div>
            </a> </li>
          <li class="divider"></li>
          <li> <a class="text-center" href="#"> <strong>See All Alerts</strong> <i class="fa fa-angle-right"></i> </a> </li>
        </ul>
        <!-- /.dropdown-alerts --> 
      </li>
      <!-- /.dropdown -->
      <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i> </a>
        <ul class="dropdown-menu dropdown-user">
          <!--<li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a> </li>
          <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a> </li>
          <li class="divider"></li>-->
          <li><a href="<?php echo WEBSITE_ROOT.'users/logout' ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a> </li>
        </ul>
        <!-- /.dropdown-user --> 
      </li>
      <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->