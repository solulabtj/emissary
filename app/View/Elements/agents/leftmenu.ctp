<?php
$actionnm = $this->request->params['action'];
$controllernm = $this->params['controller'];

$availableShops = "";
$takeTest = "";
$updateProfile = "";
$accountSettings = "";
$leaderBoard = "";


if($controllernm == 'agents')
{
	if($actionnm == "availableShops")
	{
		$availableShops = "on";
	}
	if($actionnm == "takeTest")
	{
		$takeTest = "on";
	}
	if($actionnm == "updateProfile")
	{
		$updateProfile = "on";
	}
	if($actionnm == "accountSettings")
	{
		$accountSettings = "on";
	}
        if($actionnm == "leaderBoard")
	{
		$leaderBoard = "on";
	}
}

?>
    
    <div class="navbar-default sidebar nav-sidemenu" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <!--<input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>-->
              </span> </div>
            <!-- /input-group --> 
          </li>          
          <li class= "<?php echo $availableShops;?>"><a href="<?php echo WEBSITE_ROOT.'agents/availableShops' ?>"><i class="fa fa-dashboard fa-fw"></i>Available Shops</a></li>
          <li class= "<?php echo $leaderBoard;?>"><a href="<?php echo WEBSITE_ROOT.'agents/leaderBoard' ?>"><i class="fa fa-line-chart fa-fw"></i>Leader Board</a></li>
          <li class= "<?php echo $takeTest;?>"><a href="<?php echo WEBSITE_ROOT.'agents/takeTest' ?>"><i class="fa fa-bell fa-fw"></i>Payments for Test</a></li>
         <!-- <li class= "<?php echo $accountSettings;?>"><a href="<?php echo WEBSITE_ROOT.'agents/takeTest' ?>"><i class="fa fa-list-alt fa-fw"></i>Test Result</a></li>-->
          <li class= "<?php echo $updateProfile;?>"><a href="<?php echo WEBSITE_ROOT.'agents/updateProfile' ?>"><i class="fa fa-bars fa-fw"></i>My Profile</a></li>
          <li class= "<?php echo $accountSettings;?>"><a href="<?php echo WEBSITE_ROOT.'agents/accountSettings' ?>"><i class="fa fa-wrench fa-fw"></i>Settings</a></li>
          
        </ul>
      </div>
      <!-- /.sidebar-collapse --> 
    </div>
    <!-- /.navbar-static-side --> 
  </nav>