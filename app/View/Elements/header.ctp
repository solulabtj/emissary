<!DOCTYPE html>

<html lang="en">
<head>

<!-- Html Page Specific -->
<meta charset="utf-8">
<title>Emissary</title>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

<!-- CSS -->

<?php
echo $this->Html->css('bootstrap.min');
echo $this->Html->css('animate');
echo $this->Html->css('simple-line-icons');
echo $this->Html->css('icomoon-soc-icons');
echo $this->Html->css('font-awesome.min');
echo $this->Html->css('magnific-popup');
echo $this->Html->css('style');
echo $this->Html->css('custom');
echo $this->Html->css('datepicker');
?>
<!--<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/animate.css"/>
<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/simple-line-icons.css"/>
<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/icomoon-soc-icons.css"/>
<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/magnific-popup.css"/>
<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/style.css"/>
<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/custom.css"/>-->



<!-- Favicons -->
<!-- <link rel="icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->


</head>
<?php 
	  
	  $url_assign = $_SERVER['REQUEST_URI'];
	  
	  $url_assign1 = explode('/',$url_assign);
	  
	  $uri_assign = basename($url_assign);
	  
	  if($uri_assign=='forgot_password' || $uri_assign=='forgot_password/' || $uri_assign=='forgot' || $uri_assign=='forgot/' || $url_assign1[3]=='forgot' || $uri_assign=='signup' )
	  {
 ?>
		
        <body class="fgt" data-spy="scroll" data-target=".navMenuCollapse">
        
<?php } else if($uri_assign=='login') { ?>
		
        <body class="login" >
      	  
<?php }        
		else { ?>
		
        <body class="home" data-spy="scroll" data-target=".navMenuCollapse">

<?php } ?>
<!-- PRELOADER -->
<div id="preloader">
	<div class="battery inner">
		<div class="load-line"></div>
	</div>
</div>

<div id="wrap"> 

	<!-- NAVIGATION BEGIN -->
	<nav class="navbar navbar-fixed-top navbar-slide">
			<div class="container"> 
				<a class="navbar-brand goto" href="<?php echo WEBSITE_ROOT; ?>"> <img src="<?php echo WEBSITE_ROOT?>images/logo_nav.png" alt="Your logo"/> </a>
				<!-- <a class="contact-btn icon-envelope" data-toggle="modal" data-target="#modalContact"></a> -->
				<button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<div class="collapse navbar-collapse navMenuCollapse">
					<ul class="nav">
						<li><a href="#features">About</a> </li>
						<li><a href="#benefits1">Agents</a></li>
						<li><a href="#screenshots">Brands</a></li>
						<li><a href="#pricing-table">Download</a></li>
						<li><a href="#testimonials">Contact Us</a></li>
                        <?php if ($this->Session->check('Auth.User')) { ?>
						<li><a href="<?php echo WEBSITE_ROOT ?>users/logout">Logout</a></li>
                        <?php } else{ ?>
						<li><a href="<?php echo WEBSITE_ROOT.'users/login' ?>" >Login / Register</a></li>
                        <?php } ?>
                        
                        <li class="lang arab"><a href=""><img src="<?php echo WEBSITE_ROOT?>images/lg_arb.png" /></a></li>
                        <li class="lang english"><a href=""><img src="<?php echo WEBSITE_ROOT?>images/lg_eng.png" /></a></li>
					</ul>
				</div>
			</div>
	</nav>
	<!-- NAVIGAION END -->
	