<?php
/*echo "<pre>";
print_r($_SESSION);
echo "</pre>";
exit();*/

?>



<!-- start: Main Menu -->

<style>
   li.active > a {
        background: #888888 !important;
    }
    
    li.active > a {
        color: #FFFFFF !important;
    }
    
/*    #content{
        min-height: 750px;
    }*/
</style>

			<div id="sidebar-left" class="span2">
                            <?php
                            $actionnm = $this->request->params['action'];
                            $controllernm = $this->params['controller'];
                            ?>
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
                                                <li <?php if($actionnm=='maincategory' || $actionnm=='edit_maincat' || $actionnm=='add_category'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Category",array('controller'=>'categorydetails','action'=>'maincategory/')); ?></li>
						<li <?php if($actionnm=='subcategory' || $actionnm=='edit_subcat' || $actionnm=='add_subcategory'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Sub Category",array('controller'=>'categorydetails','action'=>'subcategory/')); ?></li>
						<li <?php if($controllernm=='merchants'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Merchant",array('controller'=>'merchants','action'=>'merchant_view')); ?></li>
						<li <?php if($controllernm=='deals' && $actionnm=='listing'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Deal",array('controller'=>'deals','action'=>'listing')); ?></li>
						<li <?php if($controllernm=='deals' && $actionnm=='service_listing'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Services",array('controller'=>'deals','action'=>'service_listing')); ?></li>
                                                
						<li <?php if($controllernm=='issues'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Issues",array('controller'=>'issues','action'=>'listing')); ?></li>
						<li <?php if($controllernm=='users'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Users",array('controller'=>'users','action'=>'listing')); ?></li>
                                                
						<li <?php if($controllernm=='banners'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Promotional Banner",array('controller'=>'banners','action'=>'listing')); ?></li>
						<li <?php if($controllernm=='margins'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Deal and service Settings",array('controller'=>'margins','action'=>'listing')); ?></li>
						
						<li <?php if($actionnm=='subadmin_list' || $actionnm=='create_subadmin' || $actionnm=='profile' || $actionnm=='changepassword'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Admin Settings",array('controller'=>'admins','action'=>'profile')); ?></li>
						<li <?php if($controllernm=='cities'){ echo "class='active'"; } ?>><?php echo $this->Html->link("City",array('controller'=>'cities','action'=>'listing')); ?></li>
      						<li <?php if($controllernm=='notifications'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Notifications",array('controller'=>'notifications','action'=>'listing')); ?></li>

                                                
						<li <?php if($controllernm=='promocodes'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Promo Code",array('controller'=>'promocodes','action'=>'listing')); ?></li>
						<li <?php if($controllernm=='reports'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Reports",array('controller'=>'reports','action'=>'reports')); ?></li>
                        <?php if($_SESSION['Auth']['User']['type'] == 1) {?>
      						<li <?php if($controllernm=='settlements' && $actionnm=='o_listing'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Open settlements",array('controller'=>'settlements','action'=>'o_listing')); ?></li>
                            <li <?php if($controllernm=='settlements' && $actionnm=='c_listing'){ echo "class='active'"; } ?>><?php echo $this->Html->link("Closed settlements",array('controller'=>'settlements','action'=>'c_listing')); ?></li>
                            <?php }?>
                                                
                                        </ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			