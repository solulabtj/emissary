<!-- SOCIAL BEGIN -->
	<section id="social" class="bg-color5">
		<div class="container-fluid">
			<div class="title">
				<h2>Stay tuned</h2>
				<p>Follow us in social networks. You can also subscribe for our news.</p>
			</div>
			
			<ul class="soc-list wow flipInX">
				<li><a href="#"><i class="icon soc-icon-twitter"></i></a></li>
				<li><a href="#"><i class="icon soc-icon-facebook"></i></a></li>
				<li><a href="#"><i class="icon soc-icon-googleplus"></i></a></li>
				<li><a href="#"><i class="icon soc-icon-instagram"></i></a></li>
				<li><a href="#"><i class="icon soc-icon-pinterest"></i></a></li>
			</ul>
			
			
			<form action="scripts/subscribe.php" method="post" id="subscribe_form">
            	<div class="input-group">
                	<input class="form-control" type="email" name="email" id="subscribe_email" placeholder="Subscribe">
                    <div class="input-group-btn">
                    	<button type="submit" id="subscribe_submit" data-loading-text="&bull;&bull;&bull;"><i class="icon icon-paper-plane"></i></button>
                    </div>
                </div>
            </form>
			
		</div>
	</section>
	<!-- SOCIAL END -->
	

	
	<!-- FOOTER BEGIN -->
	<footer id="footer">
		<div class="container"> 
			<p class="copyright">&copy; 2015 by Emissary</p>
		</div>
	</footer>
	<!-- FOOTER END --> 
	
</div>


<!-- MODALS BEGIN--> 

<!-- contact modal-->
<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			
		<div class="popup-custom">
		  
		  <div class="card"></div>
		  <div class="card">
		    <h1 class="title">Login</h1>
		    <form method="post" >
			<div class="input-container cat-type">
		        <span>
			        <input type="radio" id="agent" name="agent" />
			        <label for="agent">Agent</label>
		        </span>

		        <span>
		        	<input type="radio" id="Company" name="Company"/>
		        	<label for="Company">Company</label>
		        </span>
		      </div>
			  
		      <div class="input-container">
		        <input type="text" name="username_login" id="username_login" required/>
		        <label for="Username">Username</label>
		        <div id="username_error" class="bar"></div>
		      </div>
		      <div class="input-container">
		        <input type="password" name="password_login" id="password_login" required/>
		        <label for="Password">Password</label>
		        <div id="password_error" class="bar"></div>
		      </div>
		      <div class="button-container">
		        <button onclick="login_validate(event);" ><span>Go</span></button>
		      </div>
		      <div class="footer"><a href="#">Forgot your password?</a></div>
		    </form>
		  </div>

		  <div class="card alt">
		    <div class="toggle"></div>
		    <h1 class="title">Register
		      <div class="close"></div>
		    </h1>
		    <form>
		    <div class="input-container cat-type">
		        <span>
			        <input type="radio" id="agent" name="cat" />
			        <label for="agent">Agent</label>
		        </span>

		        <span>
		        	<input type="radio" id="Provider" name="cat"/>
		        	<label for="Provider">Provider</label>
		        </span>
		      </div>
		      <div class="input-container">
		        <input type="text" id="fullname" required/>
		        <label for="fullname">Full Name</label>
		        <div class="bar"></div>
		      </div>
		      <div class="input-container">
		        <input type="text" id="email" required/>
		        <label for="email">Email Id</label>
		        <div class="bar"></div>
		      </div>
		      <div class="input-container">
		        <input type="text" id="mobile" required/>
		        <label for="mobile">Mobile Number</label>
		        <div class="bar"></div>
		      </div>
		      <div class="input-container">
		        <input type="password" id="Password" required/>
		        <label for="Password">Password</label>
		        <div class="bar"></div>
		      </div>
		      <div class="button-container">
		        <button><span>Register</span></button>
		      </div>
		    </form>
		  </div>

		</div>

		</div>
	</div>
</div>

<!-- MODALS END-->


<!-- JavaScript --> 


<?php
echo $this->Html->script('jquery-1.8.2.min');
echo $this->Html->script('bootstrap.min');
echo $this->Html->script('owl.carousel.min');
echo $this->Html->script('jquery.validate.min');
echo $this->Html->script('wow.min');
echo $this->Html->script('smoothscroll');
echo $this->Html->script('jquery.smooth-scroll.min');
echo $this->Html->script('jquery.superslides.min');
echo $this->Html->script('placeholders.jquery.min');
echo $this->Html->script('jquery.magnific-popup.min');
echo $this->Html->script('jquery.stellar.min');
echo $this->Html->script('retina.min');
echo $this->Html->script('typed');
echo $this->Html->script('custom');
?>


<!--<script src="<?php echo WEBSITE_ROOT; ?>js/jquery-1.8.2.min.js"></script> 
<script src="<?php echo WEBSITE_ROOT; ?>js/bootstrap.min.js"></script> 
<script src="<?php echo WEBSITE_ROOT; ?>js/owl.carousel.min.js"></script> 
<script src="<?php echo WEBSITE_ROOT; ?>js/jquery.validate.min.js"></script> 
<script src="<?php echo WEBSITE_ROOT; ?>js/wow.min.js"></script> 
<script src="<?php echo WEBSITE_ROOT; ?>js/smoothscroll.js"></script> 
<script src="<?php echo WEBSITE_ROOT; ?>js/jquery.smooth-scroll.min.js"></script> 
<script src="<?php echo WEBSITE_ROOT; ?>js/jquery.superslides.min.js"></script>
<script src="<?php echo WEBSITE_ROOT; ?>js/placeholders.jquery.min.js"></script>
<script src="<?php echo WEBSITE_ROOT; ?>js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo WEBSITE_ROOT; ?>js/jquery.stellar.min.js"></script>
<script src="<?php echo WEBSITE_ROOT; ?>js/retina.min.js"></script>
<script src="<?php echo WEBSITE_ROOT; ?>js/typed.js"></script> 
<script src="<?php echo WEBSITE_ROOT; ?>js/custom.js"></script> -->

<script type="text/javascript">
	
	
	function login_validate(event)
	{
		event.preventDefault();
		var username = $("#username_login").val();
		var password = $("#password_login").val();
		alert(username);
		alert(password);
		
		$.ajax({
				   type: "POST",		   
				   url: "<?php echo WEBSITE_ROOT.'users/login'; ?>",
				   data:'username='+username+"&password="+password,	
				   success: function(data){
						alert(data);
						alert(password);
				   }
			
				 });
			
		 	return false;
		
		//if(username=='')
//		{
//			$('#username_error').html('Username is required.');
//			return false;
//		}
//		else
//		{
//			$('#username_error').html('');
//			
//		}
//		
//		if(password=='')
//		{
//			$('#password_error').html('Password is required.');
//			return false;
//		}
//		else
//		{
//			$('#password_error').html('');
//		}
		
		
	}
	
	
	$('.toggle').on('click', function() {
	  $('.popup-custom').stop().addClass('active');
	});

	$('.close').on('click', function() {
	  $('.popup-custom').stop().removeClass('active');
	});
</script>
<!--[if lte IE 9]>
	<script src="<?php echo WEBSITE_ROOT; ?>scripts/respond.min.js"></script>
<![endif]-->
</body>
</html>