  </nav>
<div class="navbar-default sidebar nav-sidemenu" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <!--<input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>-->
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/index' ?>"><i class="fa fa-dashboard fa-fw"></i>Company Dashboard</a></li> 
						<li><a href="<?php echo WEBSITE_ROOT.'clients/update_profile' ?>"><i class="fa fa-dashboard fa-fw"></i>Company Profile</a></li>  
						<li><a href="<?php echo WEBSITE_ROOT.'clients/job_post' ?>"><i class="fa fa-dashboard fa-fw"></i>Jobs</a></li>                        
                       <!-- <li><a href="#"><i class="fa fa-briefcase fa-fw"></i>Jobs<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#">Panels and Wells</a></li>
                                <li><a href="#">Buttons</a></li>
                                <li><a href="#">Notifications</a></li>
                                <li><a href="#">Typography</a></li>
                                <li><a href="#"> Icons</a></li>
                                <li><a href="#">Grid</a></li>
                            </ul>
                        </li> -->
						                      
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/requests' ?>"><i class="fa fa-bell fa-fw"></i>Job Requests</a></li>                        
                        <li><a href="#"><i class="fa fa-line-chart fa-fw"></i>Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/reports' ?>">Agent Report</a></li>
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/job_reports' ?>">Job Report</a></li>
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/job_reports_request' ?>">Job Request Report</a></li>
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/job_reports_submission' ?>">Job Submission Report</a></li>
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/survey_category_report' ?>">Survey Category Report</a></li>
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/survey_question_report' ?>">Survey Questions Report</a></li>
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/payment_report' ?>">Payments Report</a></li>
                                
                            </ul>
                        </li>
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/job_submission' ?>"><i class="fa fa-file-text-o fa-fw"></i>Submissions</a></li>
                        <!--<li><a href="<?php //echo WEBSITE_ROOT.'clients/surveyList' ?>"><i class="fa fa-list-alt fa-fw"></i>Survey</a></li>-->
                        
                        <li><a href="#"><i class="fa fa-briefcase fa-fw"></i>Survey<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/surveyList' ?>">Survey Management</a></li>
                                <li><a href="<?php echo WEBSITE_ROOT.'clients/survey_category' ?>">Survey  Categories</a></li>
                                
                            </ul>
                        </li>
                        
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/coupon_list' ?>"><i class="fa fa-wrench fa-fw"></i>Coupons Management</a></li>
                        
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/client_address' ?>"><i class="fa fa-wrench fa-fw"></i>Location Management</a></li>
                        
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/my_payments' ?>"><i class="fa fa-money fa-fw"></i>My Payments</a></li>
                        
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/payments' ?>"><i class="fa fa-wrench fa-fw"></i>Payments</a></li>
                        
                        <!--<li><a href="<?php echo WEBSITE_ROOT.'clients/accountSettings' ?>"><i class="fa fa-wrench fa-fw"></i>Settings</a></li>
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/accountSettings' ?>"><i class="fa fa-credit-card fa-fw"></i>Payments</a></li>
                        <li><a href="<?php echo WEBSITE_ROOT.'clients/accountSettings' ?>"><i class="fa fa-bars fa-fw"></i>Reports</a></li>-->
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
			
			 <!-- /.navbar-static-side -->
      