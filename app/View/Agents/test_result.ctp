
    
    
    <link rel="stylesheet" href="<?php echo WEBSITE_ROOT?>css/reset-progress.css">

    
        <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      .progress-pie-chart {
  width: 200px;
  height: 200px;
  border-radius: 50%;
  background-color: #E5E5E5;
  position: relative;
}
.progress-pie-chart.gt-50 {
  background-color: #127ebd;
}

.ppc-progress {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  width: 200px;
  height: 200px;
  clip: rect(0, 200px, 200px, 100px);
}
.ppc-progress .ppc-progress-fill {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  width: 200px;
  height: 200px;
  clip: rect(0, 100px, 200px, 0);
  background: #127ebd;
  transform: rotate(60deg);
}
.gt-50 .ppc-progress {
  clip: rect(0, 100px, 200px, 0);
}
.gt-50 .ppc-progress .ppc-progress-fill {
  clip: rect(0, 200px, 200px, 100px);
  background: #E5E5E5;
}

.ppc-percents {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 173.91304px/2);
  top: calc(50% - 173.91304px/2);
  width: 173.91304px;
  height: 173.91304px;
  background: #fff;
  text-align: center;
  display: table;
}
.ppc-percents span {
  display: block;
  font-size: 2.6em;
  font-weight: bold;
  color: #127ebd;
}

.pcc-percents-wrapper {
  display: table-cell;
  vertical-align: middle;
}

.progress-pie-chart {
  margin: 50px auto 0;
}

    </style>

    
        <script src="<?php echo WEBSITE_ROOT?>js/prefixfree.min.js"></script>
        <script src="<?php echo WEBSITE_ROOT?>js/progress.js"></script>

    
    
  	 <div id="page-wrapper">
		<div class="row">
			<div id = "job_details">
				<h1 class="page-header" id = "company_name">ABC Shop </h1>
					<div class="row">
					
					<h3 style="text-align:center">Test Result : PASS</h3>
					  <div class="progress-pie-chart" data-percent="39">
						  <div class="ppc-progress">
							<div class="ppc-progress-fill"></div>
						  </div>
						  <div class="ppc-percents">
							<div class="pcc-percents-wrapper">
							  <span>%</span>
							</div>
						  </div>
						</div>
					</div>
				<div class="col-md-12 text-center">
			</div>
		</div>
	</div> 
  
</body>
</html>

