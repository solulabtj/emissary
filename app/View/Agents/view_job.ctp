<style type="text/css">
    form label {
        margin-left: 0px;
    }

</style>

<script src="http://maps.google.com/maps/api/js" type="text/javascript"></script>

<div id="page-wrapper">
    <div class="row">
        <div id = "job_details">
            <h1 class="page-header" id = "company_name">ABC Shop </h1>
            <form>
                <div class="row">
                    <form>

                        <input type="hidden" name="agent_email_id" id="agent_email_id" value="<?php echo $this->Session->read('Auth.User.email_address') ?>"  />

                        <div class="col-md-7">
                            <div class="col-md-5" style="padding-left:0px;"><img id="company_cover" src="<?php echo WEBSITE_ROOT ?>images/prof-pic.jpg" alt="Custmore photo" style="width:100%;" />

                            </div>
                            <div class="col-md-7">
                                <p id="company_overview">Lorem ipsum dolor sit amet, pellentesque est sodales ipsum aliquam. Nunc felis pharetra urna, neque diam ante amet ut blandit, sit nullam sit mattis tincidunt, est accumsan, ipsum scelerisque tempus. Sed euismod nulla dolor nonummy iaculis, wisi tempor nunc volutpat. Sed euismod nulla dolor nonummy iaculis, wisi tempor nunc volutpat. </p>                                        

                            </div>
                            <div class="clearfix"></div>
                            <br>
                            <div class="col-md-5">
                                <h4 id="company_agency"><i class="fa fa-codepen"></i> ABC Agnecy</h4>
                                <i class="fa fa-gift"></i> <label id="job_compensation">  25 $</label><br>
                                <i class="fa fa-calendar"></i> <label id="job_time_span">  </label><br>
                                <i class="fa fa-clock-o"></i> <label id="company_agency"> </label>
                            </div>
                            <div class="col-md-7">
                                <h4>Select Job Location</h4>
                                <div id="job_address">
                                    <label><input type="radio" name="job_location"> Address 1</label><br>
                                    <label><input type="radio" name="job_location"> Address 2</label><br>
                                    <label><input type="radio" name="job_location"> Address 3</label><br>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="" name="job_id" id="job_id">

                        <div class="col-md-5">
                                           <!--<img src="<?php echo WEBSITE_ROOT ?>images/map1.jpg" alt="map" style="width:100%;" /> -->    
                            <div id="map" style="width: 100%; height: 400px;"></div> 
                        </div>

                    </form>
                </div>
                <div class="col-md-12 text-center">
                    <div class="errClass" align:center></div>
                    <br>
                    <!--<button onclick="requestJob();" type="button" class="btn-custom1 create-job">Request</button>-->
                     <button type="button" class=" btn-lg btn-custom1 create-job" data-toggle="modal" data-target="#myModal">Request</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Company Policy</h4>
            </div>
            <div class="modal-body">
                <p id="policy"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"  onclick="requestJob('agree');">Agree</button>
                <button type="button" class="btn btn-default"  onclick="requestJob('disagree');">Cancel</button>
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            </div>
        </div>
    </div>
</div>
<script>
    if (top.location != location) {
        top.location.href = document.location.href;
    }
    $(function () {
        window.prettyPrint && prettyPrint();
        $('#dp1').datepicker({
            format: 'dd-mm-yyyy'
        });
        $('#dp2').datepicker({
            format: 'dd-mm-yyyy'
        });



        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');
    });
</script>

</body>
</html>