<?php

echo $this->Html->script('jquery.dataTables.min');
echo $this->Html->css('jquery.dataTables.min');
?>
<script>
    $(document).ready(function () {

        $('#settelmentTable').DataTable();

    });
</script>
<style>
.table-custom .table>thead>tr>th{
            text-align: left !important;
    }  
    
 .table-custom .table>tbody>tr>td{
            text-align: left !important;
    }     
</style>
<?php //echo "<pre>"; print_r($res); exit; ?>
<div class="clearfix"></div>
<div class="table-custom shadow" >
    <table id="settelmentTable" class="table text-center">
        <thead>
            <tr>
                <th>Image</th>
                <th>Agent Name</th>                                       
                <th>Agent Rank</th>                                
                <th>Points</th>
            </tr>
        </thead>
        <tbody>
                    <?php foreach ($res as $aui) { ?>
                    <?php  
                        $rank_id  = $aui['aui']['agent_rank']; 
                        if($rank_id == "1"){
                            $rank = "Pro";
                        }
                        if($rank_id == "2"){
                            $rank = "Expert";
                        }
                        if($rank_id == "3"){
                            $rank = "Advanced";
                        }
                        if($rank_id == "4"){
                            $rank = "Normal";
                        }
                        if($rank_id == "5"){
                            $rank = "Beginner";
                        }
                        if($rank_id >= "6"){
                            $rank = "Beginner";
                        }
                        
                         if($rank_id == "0"){
                            $rank = "Beginner";
                        }
                        
                      
                        
                      
                    ?>
            <tr>                
                <?php if($aui['aui']['profile_image'] != "" && file_exists(WEBSITE_WEBROOT."media/img/".$aui['aui']['profile_image'])){  ?>
                <td><img src="<?php echo WEBSITE_ROOT."media/img/".$aui['aui']['profile_image']; ?>" width="50" height="50" /></td>
                <?php }else{ ?>
                <td> No Profile Picture set</td>
                <?php } ?>
                <td><?php echo $aui['aui']['agent_name']; ?></td>                
                <td><?php echo $rank; ?></td>
                <td><?php echo $aui['aui']['points_earned']; ?></td>
            </tr>
                 <?php }  ?>
        </tbody>
    </table>
    <footer class="table-footer">
        <div class="row">                   
        </div>
    </footer>
</div>
<?php exit(); ?>