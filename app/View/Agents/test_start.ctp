    <link href="<?php echo WEBSITE_ROOT?>css/timer-style.css" rel="stylesheet" type="text/css" media="all">
  
 
 
 <script src="<?php echo WEBSITE_ROOT?>js/jquery.simple.timer.js"></script>
 <script src="<?php echo WEBSITE_ROOT?>js/prefixfree.min.js"></script>
        <script src="<?php echo WEBSITE_ROOT?>js/progress.js"></script>
 

  <div id="page-wrapper">
    <div class="row">
      <h1 class="page-header">Take a Test to take part in Surveys <div style="position:absolute; top:0px; right:0px;">
	  
	  <h2 class='timer' data-minutes-left=5></h2>
      <section class='actions'></section>
	  </div></h1>
      <div class="clearfix"></div>
      <div class="add-surveys-box shadow" id="account-setting">
        
            <div class="wrapper">
        
        <div class="clearfix"></div>
          <form id="submitTest" >
          
          <input type="hidden" name="agent_email_id" id="agent_email_id" value="<?php echo $this->Session->read('Auth.User.email_address') ?>"  />
          
        <div class="tabscontent questionList" id="question-box-main">
            <div class="question-box"><p>Lorem ipsum dolor sit amet, consectetur odipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua?</p>
              <label><input type="checkbox" name=""> ABX</label><br>
              <label><input type="checkbox" name=""> CDA</label><br>
              <label><input type="checkbox" name=""> ADA</label><br>
              <label><input type="checkbox" name=""> asdasdas</label><br>
            </div>

            <div class="question-box"><p>Lorem ipsum dolor sit amet, consectetur odipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua?</p>
              <label><input type="checkbox" name=""> ABX</label><br>
              <label><input type="checkbox" name=""> CDA</label><br>
              <label><input type="checkbox" name=""> ADA</label><br>
              <label><input type="checkbox" name=""> asdasdas</label><br>
            </div>

            <div class="question-box"><p>Lorem ipsum dolor sit amet, consectetur odipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua?</p>
              <label><input type="checkbox" name=""> ABX</label><br>
              <label><input type="checkbox" name=""> CDA</label><br>
              <label><input type="checkbox" name=""> ADA</label><br>
              <label><input type="checkbox" name=""> asdasdas</label><br>
            </div>

            <div class="question-box"><p>Lorem ipsum dolor sit amet, consectetur odipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua?</p>
              <label><input type="checkbox" name=""> ABX</label><br>
              <label><input type="checkbox" name=""> CDA</label><br>
              <label><input type="checkbox" name=""> ADA</label><br>
              <label><input type="checkbox" name=""> asdasdas</label><br>
            </div>
    </div>
    <div class="clearfix"></div>
    <br>
      <div style="text-align:center;">
      <button href="" class="btn-custom1 create-job" type="submit">Submit Test</button>
      </div>
          </form>
          
      </div>
    </div>
  </div>
</div>




    
        <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      .progress-pie-chart {
  width: 200px;
  height: 200px;
  border-radius: 50%;
  background-color: #E5E5E5;
  position: relative;
}
.progress-pie-chart.gt-50 {
  background-color: #127ebd;
}

.ppc-progress {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  width: 200px;
  height: 200px;
  clip: rect(0, 200px, 200px, 100px);
}
.ppc-progress .ppc-progress-fill {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  width: 200px;
  height: 200px;
  clip: rect(0, 100px, 200px, 0);
  background: #127ebd;
  transform: rotate(60deg);
}
.gt-50 .ppc-progress {
  clip: rect(0, 100px, 200px, 0);
}
.gt-50 .ppc-progress .ppc-progress-fill {
  clip: rect(0, 200px, 200px, 100px);
  background: #E5E5E5;
}

.ppc-percents {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 173.91304px/2);
  top: calc(50% - 173.91304px/2);
  width: 173.91304px;
  height: 173.91304px;
  background: #fff;
  text-align: center;
  display: table;
}
.ppc-percents span {
  display: block;
  font-size: 2.6em;
  font-weight: bold;
  color: #127ebd;
}

.pcc-percents-wrapper {
  display: table-cell;
  vertical-align: middle;
}

.progress-pie-chart {
  margin: 50px auto 0;
}

    </style>






<!-- contact modal-->
<div class="result_popup">
<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			
		<div class="popup-custom">
		  
		  <div class="card"></div>
		  <div class="card">
		    <h1 class="title">Test Result</h1>
		     <div class="progress-pie-chart"  id="testResult" data-percent="39">
              <div class="ppc-progress">
                <div class="ppc-progress-fill"></div>
              </div>
              <div class="ppc-percents">
                <div class="pcc-percents-wrapper">
                  <span  id="testResultSpan" >%</span>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <br>
            <div>
            	<h3 style="text-align:center" id="finalResult">PASS</h3>
		  	</div>
            <div class="button-container">
                <a href="<?php echo WEBSITE_ROOT?>agents/updateProfile"><span>Go to Profile</span></a>
              </div>
          </div>

		  
		</div>

		</div>
	</div>
</div>
</div>
<!-- MODALS END-->




<!-- Timer Script Functions -->
<script>


</script>

</body>
</html>