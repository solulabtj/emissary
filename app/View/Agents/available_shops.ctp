<script src="http://maps.google.com/maps/api/js" type="text/javascript"></script>
<script src="<?php echo WEBSITE_ROOT . 'js/bootstrap-slider.min.js' ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo WEBSITE_ROOT; ?>css/bootstrap-slider.min.css"/>
<style type="text/css">
    .form-input input, .form-input select{
        margin-bottom: 25px;
    }
    .form-input label, .form-input input[type="radio"] {
        margin: 0px; 
    }
    .input-name {
        position: relative;
    }
    .input-name i {
        position: absolute; right: 0px; margin: 15px 15px 0px 0px;
    }


    $fabric: url(http://i.imgur.com/9kxBTzr.jpg);
    $noise: url(http://i.imgur.com/3JZchqt.png);

    $track-w: 28em;
    $track-h: 1.5em;
    $track-bg: #7aa9b3;
    $track-pad: .25em;

    $fill-perc: 46;
    $fill-max-w: $track-w - 2*$track-pad;
    $fill-r: $track-h/2 - $track-pad;
    $fill-sh: 
    inset 0 -1px 1px #09313d, 
    inset 0 1px 1px #c0edf3;
    $fill: $noise, 
    linear-gradient(0deg, #249ac0, #70d1e2);

    $thumb-d: 2.25em;
    $thumb-refl: #777;
    $thumb-refl-a: rgba(#777, 0);

    $tip-fs: 2 .75;
    $tip-w: 8em/nth($tip-fs, 1);
    $tip-h: 3.75em/nth($tip-fs, 1);
    $tip-arrow-l: .75em/nth($tip-fs, 1);
    $tip-msg: 
    'Not even started'
    'Barely started' 
    'Almost half way' 
    'More than half done' 
    'Almost there' 
    'Done' 
    'Half way there';
    $tip-r: .25em/nth($tip-fs, 1);
    $tip-offset: (
    x: $tip-r $tip-r $tip-w - $tip-r $tip-w - $tip-r, 
    y: $tip-h - $tip-r $tip-r $tip-r $tip-h - $tip-r
    );
    $tip-k: $tip-r*nth($tip-fs, 1)/.0625em;
    $tip-cp: 
    $tip-w/2 + $tip-arrow-l $tip-h, 
    $tip-w/2                $tip-h + $tip-arrow-l, 
    $tip-w/2 - $tip-arrow-l $tip-h;
    $tip-base-a: 90deg/$tip-k;

    @for $i from 1 through 4 {
        $xoff: nth(map-get($tip-offset, x), $i);
        $yoff: nth(map-get($tip-offset, y), $i);

        @for $j from 0 through $tip-k {
            $a: $j*$tip-base-a - (4 - $i)*90deg;
            $tip-cp: $tip-cp, 
                $xoff + $tip-r*cos($a) $yoff + $tip-r*sin($a);
        }
    }

    @mixin track($flag: false) {
        box-sizing: border-box;
        padding: if($flag, 0, $track-pad);
        width: $track-w; height: $track-h;
        border-radius: $track-h/2;
        box-shadow: 0 1px 1px #def3f8,
            inset 0 .125em .125em #0d1112;
        background: $track-bg;
    }

    @mixin fill() {
        height: $track-h - 2*$track-pad;
        border-radius: 
            #{$fill-r} #{$track-h/8} #{$track-h/8} #{$fill-r} / 50%;
        box-shadow: $fill-sh;
        background: $fill;
        background-clip: padding-box;
    }

    @mixin thumb() {
        box-sizing: border-box;
        border: solid .125em rgba(#cde0e6, .5);
        width: $thumb-d; height: $thumb-d;
        border-radius: 50%;
        box-shadow: 
            0 .125em .125em #3b4547;
        background: 
            radial-gradient($thumb-refl, $thumb-refl-a 70%) 
            50% 50%, 
            radial-gradient($thumb-refl, $thumb-refl-a 70%) 
            50% 50%, 
            radial-gradient(#{at 50% 0}, $thumb-refl, $thumb-refl-a 71%) 
        50% 0, 
        radial-gradient(#{at 50% 100%}, $thumb-refl, $thumb-refl-a 71%) 
        50% 100%, 
        radial-gradient(#{at 0 50%}, $thumb-refl, $thumb-refl-a 71%) 
        0 50%, 
        radial-gradient(#{at 100% 50%}, $thumb-refl, $thumb-refl-a 71%) 
        100% 50%, 
        radial-gradient(rgba(#3b4547, 0) 50%, #3b4547 71%) 
        50% -.25em #fff;
        background-clip: border-box;
        background-origin: border-box;
        background-repeat: no-repeat;
        background-size: 
            100% .125em, .125em  100%, 
            50% 50%, 50% 50%, 50% 50%, 50% 50%, 
            115% 115%;
    }

    html {
        height: 100%;
        background: 
            radial-gradient(rgba(#a1def0, .75), rgba(#408193, .75)), 
            $noise, $fabric;
    }

    input[type='range'] {
        &, 
        &::-webkit-slider-runnable-track, 
        &::-webkit-slider-thumb {
            -webkit-appearance: none;
        }

        position: absolute;
        top: 50%; left: 50%;
        padding: $track-pad 0;
        width: $track-w; height: 2*$track-h;
        /*transform: translate(-50%, -50%);*/
        opacity: .75;
        /*background: transparent;*/
        font-size: 1em;
        cursor: pointer;

        &::-webkit-slider-runnable-track {
            position: relative;
            @include track(true);
        }
        &::-moz-range-track {
            @include track();
        }
        &::-ms-track {
            border: none;
            @include track();
            color: transparent;
        }

        &::-moz-range-progress {
            box-sizing: border-box;
            border-left: solid $track-pad transparent;
            @include fill();
            border-radius: #{$track-h/2} #{$track-h/8} #{$track-h/8} #{$track-h/2} / 50%;
            background-clip: padding-box;
        }
        &::-ms-fill-lower {
            @include fill();
        }

        &::-webkit-slider-thumb {
            position: relative;
            margin-top: ($track-h - $thumb-d)/2;
            @include thumb();
            filter: drop-shadow(0 1px #516570);
        }
        &::-moz-range-thumb {
            @include thumb();
        }
        &::-ms-thumb {
            @include thumb();
        }

        &::-webkit-slider-runnable-track, /deep/ #track {
            .js &:before {
                position: absolute;
                top: $track-pad; left: $track-pad;
                width: $fill-perc*$fill-max-w/100;
                @include fill();
                content: '';
            }
        }

        .js & {
            &::-webkit-slider-thumb, /deep/ #thumb {
                &:before, &:after {
                    position: absolute;
                    bottom: 100%; left: 50%;
                    color: #ebeef3;
                    text-align: center;
                    text-shadow: 0 1px 1px #4d5d7e;
                }
                &:before {
                    width: $tip-w; height: $tip-h + $tip-arrow-l;
                    border-radius: $tip-r;
                    transform: translate(-50%, -.5em);
                    box-shadow: inset 0 1px 1px #d1e4f9;
                    background: $noise, 
                        linear-gradient(#b9d7f9, #a0a8d6);
                    clip-path: polygon($tip-cp);
                    font: #{nth($tip-fs, 1)*1em}/#{.75*$tip-h} trebuchet ms, verdana, arial, sans-serif;
                    content: '#{$fill-perc}%';
                }
                &:after {
                    width: .9*$tip-w*nth($tip-fs, 1)/nth($tip-fs, 2);
                    border-radius: 50%;
                    transform: translate(-50%, -2.75em);
                    box-shadow: 0 2.25em .625em -.5em rgba(#37545c, .75);
                    font: #{nth($tip-fs, 2)*1em} trebuchet ms, verdana, arial, sans-serif;
                    white-space: nowrap;

                    @for $i from 0 to 4 {
                        @if $fill-perc > $i*25 and $fill-perc <= ($i + 1)*25 {
                            content: '#{nth($tip-msg, if($fill-perc == 100, 6, $i + 2))}';
                        }
                    }
                    @if $fill-perc == 0 { content: '#{nth($tip-msg, 1)}'; }
                }
            }
        }

        &:focus {
            outline: none;
            opacity: .99;
        }
    }


</style>


<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Available Shops Near Your Area </h1>
        <form class="form-input">

        </form>




        <div class="row">
            <div class="col-md-6">
                <div>
                    <p>Lorem ipsum dolor sit amet, pellentesque est sodales ipsum aliquam. Nunc felis pharetra urna, neque diam ante amet ut blandit, sit nullam sit mattis tincidunt, est accumsan, ipsum scelerisque tempus. Sed euismod nulla dolor nonummy iaculis, wisi tempor nunc volutpat. Lacus suscipit, tempor ut libero sit nec. Fringilla dignissim at metus, sit sed metus lacus. Mi non nec nunc sollicitudin, donec ullamcorper in aptent, viverra nulla, vulputate lorem dignissim.</p>
                </div>
                <div id = "available_jobs">


                    <!--<div style="border-bottom:1px solid #ccc; padding-bottom:15px; margin-bottom:15px;">
                    
                    <br>
                    <div class="col-md-4" style="padding-left:0px;"><img src="<?php echo WEBSITE_ROOT ?>images/image-1.jpg" alt="Custmore photo" style="width:100%;" /></div>
                    <div class="col-md-8">
                        <div >
                        <h5 style="margin-top:0px;">ABC Shop</h5>
                        <p><i class="fa fa-map-pin"></i> 123, ABC Complex, LA <i class="fa fa-spoon"></i> Food<br>
                            <i class="fa fa-calendar"></i> 12th June 2016 to 15th July 2016<br>
                             <i class="fa fa-map-pin"></i> 2 KM Away<br>
                             <i class="fa fa-gift"></i> 25 $</p>
                             <a href="#" class="pull-right">View Details</a>
                        </div>     
                    </div>
                    <div class="clearfix"></div>
                    </div>
                
                <div class="clearfix"></div>
                 
                 <div style="border-bottom:1px solid #ccc; padding-bottom:15px; margin-bottom:15px;">
                    <div class="col-md-4" style="padding-left:0px;"><img src="<?php echo WEBSITE_ROOT ?>images/image-1.jpg" alt="Custmore photo" style="width:100%;" /></div>
                    <div class="col-md-8">
                        <div style="padding-bottom:15px; margin-bottom:15px;">
                        <h5 style="margin-top:0px;">ABC Shop</h5>
                        <p><i class="fa fa-map-pin"></i> 123, ABC Complex, LA <i class="fa fa-spoon"></i> Food<br>
                            <i class="fa fa-calendar"></i> 12th June 2016 to 15th July 2016<br>
                             <i class="fa fa-map-pin"></i> 2 KM Away<br>
                             <i class="fa fa-gift"></i> 25 $</p>
                             <a href="#" class="pull-right">View Details</a>
                        </div>     
                    </div>
                    <div class="clearfix"></div>
                    </div>

                 <div class="clearfix"></div>
                 
                 <div style="border-bottom:1px solid #ccc; padding-bottom:15px; margin-bottom:15px;">
                    <div class="col-md-4" style="padding-left:0px;"><img src="<?php echo WEBSITE_ROOT ?>images/image-1.jpg" alt="Custmore photo" style="width:100%;" /></div>
                    <div class="col-md-8">
                        <div style="padding-bottom:15px; margin-bottom:15px;">
                        <h5 style="margin-top:0px;">ABC Shop</h5>
                        <p><i class="fa fa-map-pin"></i> 123, ABC Complex, LA <i class="fa fa-spoon"></i> Food<br>
                            <i class="fa fa-calendar"></i> 12th June 2016 to 15th July 2016<br>
                             <i class="fa fa-map-pin"></i> 2 KM Away<br>
                             <i class="fa fa-gift"></i> 25 $</p>
                             <a href="#" class="pull-right">View Details</a>
                        </div>     
                    </div>
                    <div class="clearfix"></div>
                    </div>-->
                </div>

                <div id="printError"></div>
                <div class="col-md-12">
                    <input type="hidden" value="0" id="job_count">
                    <button type="button" onclick = "return getAvailableJobs();" class="btn-custom1 create-job pull-right">View More</button>
                </div>
            </div>

            <div class="col-md-6">
                <!--<img src="<?php echo WEBSITE_ROOT ?>images/map1.jpg" alt="map" style="width:100%;" /> -->
                <div id="map" style="width: 100%; height: 400px;"></div> 
                <br><br>

                <div id = "updateResponse" align="center"> </div>

                <form id = "updateFilterForm">

                    <input type="hidden" value = "0" id = "hidFilterId">
                    <h4>Filter By</h4>
                    <p>Distance</p>
                    <b id="defaultMinDistance" class="value_custom">10</b>

                    <input id="distance_slider" type="range" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> 


                    <b id="defaultMaxDistance" class="value_custom">1000</b>
<!--<input type='range' value='100'>-->
                    <p>Rewards</p>
                    <b id="defaultMinCompensation" class="value_custom">10</b> 


                    <input id="compensation_slider" type="range" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/>


                    <b id="defaultMaxCompensation" class="value_custom">1000</b>
<!--<input type='range' value='100'>-->
                    <br>

                    <div class="input-box">
                        <label class="input-name"><span>Submission Due Date</span>
                            <select name="due_date" id="due_date" class="input-field">
                                <option value = "1">TODAY</option>
                                <option value = "7">THIS WEEK</option>
                                <option value = "30">THIS MONTH</option>
                                <option value = "365">THIS YEAR</option>
                            </select>
                        </label>
                    </div>

<!--<p>Submission Due Date <input class="" type="text" name="Address Line 1" placeholder="Address Line 1"> <i class="fa fa-calendar"></i></p>-->
                    <p>Category</p>
                    <div id = "category_list">
                        <label><input type="checkbox" name="job_categories[]"> Not Selected</label>
                        <label><input type="checkbox" name="job_categories[]"> Not Selected</label>
                        <label><input type="checkbox" name="job_categories[]"> Not Selected</label>
                        <label><input type="checkbox" name="job_categories[]"> Not Selected</label>
                        <label><input type="checkbox" name="job_categories[]"> Not Selected</label>
                        <label><input type="checkbox" name="job_categories[]"> Not Selected</label>
                        <label><input type="checkbox" name="job_categories[]"> Not Selected</label>
                        <label><input type="checkbox" name="job_categories[]"> Not Selected</label>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn-custom1 create-job pull-right">Filter</button>
                    </div>
                </form>
            </div>



        </div>
    </div>

</div>



<script>



    if (top.location != location) {
        top.location.href = document.location.href;
    }
    $(function () {
        window.prettyPrint && prettyPrint();
        $('#dp1').datepicker({
            format: 'dd-mm-yyyy'
        });
        $('#dp2').datepicker({
            format: 'dd-mm-yyyy'
        });



        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');
    


    var range_el = document.querySelector('input[type=range]'),
            style_el = document.createElement('style'),
            range_style = getComputedStyle(range_el),
            pad = range_style.paddingTop.split('px')[0],
            w = range_style.width.split('px')[0],
            fill_max_w = w - 2 * pad,
            messages = [
                'Not even started',
                'Barely started',
                'Almost half way',
                'More than half done',
                'Almost there',
                'Done',
                'Half way there'
            ];

    document.body.appendChild(style_el);

    range_el.addEventListener('input', function () {
        var perc = this.value, dec = perc / 100,
                fill_w = Math.round(dec * fill_max_w), msg = messages[0];

        for (var i = 0; i < 4; i++) {
            if (perc > i * 25 && i <= (i + 1) * 25) {
                msg = messages[(perc == 100) ? 5 : ((perc == 50) ? 6 : (i + 1))];
            }
        }

        style_el.textContent =
                '.js input[type=range]::-webkit-slider-runnable-track:before,' +
                '.js input[type=range] /deep/ #track:before{width:' + fill_w + 'px}' +
                '.js input[type=range]::-webkit-slider-thumb:before,' +
                '.js input[type=range] /deep/ #thumb:before{content:"' + perc + '%"}' +
                '.js input[type=range]::-webkit-slider-thumb:after,' +
                '.js input[type=range] /deep/ #thumb:after{content:"' + msg + '"}';
    }, false);
    
    });

    
</script>

</body>
</html>