<style>
	
	.error { color:#F00; }
	
	.error1 { color:#0F3; }

</style>
  <div id="page-wrapper">
    <div class="row">
      <h1 class="page-header">Setting</h1>
      <div class="clearfix"></div>
      <div class="add-surveys-box shadow" id="account-setting">
        
            <div class="wrapper">
        
        <div class="clearfix"></div>
        <div class="tabscontent">
		<div><?php echo $this->Session->flash(); ?></div>
            <form method="post" action = "<?php echo WEBSITE_ROOT.'agents/accountSettings' ?>">
                <div class="col-md-5">
				   <h4>Change Password</h4><br/>
                    <input class="form-control ng-pristine ng-valid ng-touched" name="oldPassword" id="oldPassword" type="password" placeholder="Old Password">
                    <div class="error" id="error_old" ></div>
                    <br/>
                                                            
                    <input class="form-control ng-pristine ng-valid ng-touched" name="newPassword" id="newPassword" type="password" placeholder="New Password">
                    <div class="error" id="error_new" ></div>
                    <br/>                                        
                    
                    <input class="form-control ng-pristine ng-valid ng-touched" name="confirmPassword" id="confirmPassword" type="password" placeholder="Confirm Password">
                    <div class="error" id="error_confirm" ></div>
                    <br/>
                    
					<div id = "changePwdError1" class = "error"></div>
                    <div id = "changePwdError2" class = "error1"></div>
                    
                    <input onclick="return checkPasswordMatch(event);" type="submit" class="btn-custom1 create-job pull-right" name="submit" value="Submit">
                </div>
                
                <div class="col-md-5">
            
				<h4>Receive Notification Via</h4><br/>
				<lable><input type="checkbox" name="receive_email" id="receive_email" value="1"<?php if($agent_settings['AgentUserInfo']['receive_email']=='1') { echo "checked='checked'"; } ?> onclick="receive_email_change();" > E-mail</lable><br>
				<lable><input type="checkbox" name="receive_push"  id="receive_push" value="1"<?php if($agent_settings['AgentUserInfo']['receive_push']=='1') { echo "checked='checked'"; } ?> onclick="receive_push_change();" > Push Notifications</lable>
			</div>
                
            </form>
			
			<div class="clearfix"></div>
    </div>
        
      </div>
    </div>
  </div>
</div>


<script>

function receive_email_change()
{
	var receive_email = $('#receive_email').val();
	$.ajax({
				type: "POST",		   
				url: "<?php echo WEBSITE_ROOT.'agents/receive_email'; ?>",
				data:'receive_email='+receive_email,
				success: function(data){
						alert(data);
						//window.location.reload();
				}
		});
}

function receive_push_change()
{
	var receive_push = $('#receive_push').val();
	$.ajax({
				type: "POST",		   
				url: "<?php echo WEBSITE_ROOT.'agents/receive_push'; ?>",
				data:'receive_push='+receive_push,
				success: function(data){
						alert(data);
						//window.location.reload();	
				}
		});
}

function checkPasswordMatch(event) {
	var oldPassword = $("#oldPassword").val();
    var password = $("#newPassword").val();
    var confirmPassword = $("#confirmPassword").val();
	var error = false;
	
	if(oldPassword=='')
	{
		$('#error_old').html('Old password is required.');
		error = true;
	}
	else
	{
		$('#error_old').html('');
	}
	
	if(password=='')
	{
		$('#error_new').html('New password is required.');
		error = true;
	}
	else if(password.length<8)
	{
		$('#error_new').html('New password must be greater than 8 characters.');
		error = true;
	}
	else
	{
		$('#error_new').html('');
	}
	
	if(confirmPassword=='')
	{
		$('#error_confirm').html('Confirm password is required.');
		error = true;
	}
	else
	{
		$('#error_confirm').html('');
	}
	
	if(password!='' && confirmPassword!='')
	{
			if (password != confirmPassword)
			{
				event.preventDefault();
				$("#changePwdError1").html("Passwords do not match!");
				$("#changePwdError2").html("");	
			}
			else
			{
				$("#changePwdError2").html("Passwords match.");	
				$("#changePwdError1").html("");
			}
	}
	else
	{
		event.preventDefault();	
	}	
	
	if(error)
	{
		return false;
	}
	
}




$(document).ready(function () {
   $("#confirmPassword").keyup(checkPasswordMatch);
});
</script>


</body>
</html>