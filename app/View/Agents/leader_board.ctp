<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Leader Board </h1>        
        <div class="row">          
            <div class="col-md-12">
            <div id="leaderboard_data"></div>
            </div>
        </div>
    </div>
</div>
<script>
    if (top.location != location) {
        top.location.href = document.location.href;
    }
</script>

</body>
</html>