<script type="text/javascript">

    function onlyNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }


    function showimagepreview(input) {

        var ext = $('#profile_image_real').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            alert('invalid file type! Please Select only image.');
            return false;
        }

        if (input.files && input.files[0]) {
            var filerdr = new FileReader();
            filerdr.onload = function (e) {
                $('#profile_image').attr('src', e.target.result);
            }
            filerdr.readAsDataURL(input.files[0]);
        }
    }

</script>

<style type="text/css">
    .form-input input, .form-input select{
        margin-bottom: 25px;
    }
    .form-input label, .form-input input[type="radio"] {
        margin: 0px; 
    }
    .input-name {
        position: relative;
    }
    .input-name i {
        position: absolute; right: 0px; margin: 15px 15px 0px 0px;
    }
    .error{
        margin-top: -25px;
        margin-bottom: 25px;
        color: red;
    }
    
    .errors{
        margin-top: -25px;
        
        color: red;
    }
</style>



<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">Profile </h1>
        <div class="errClass" id="updateResponse" align="center"></div>
        <form class="form-input" id="updateProfileForm">
            <div class="row">
                <div class="col-md-3"><img src="<?php echo WEBSITE_ROOT ?>images/prof-pic.jpg" id="profile_image" alt="Customer photo" style="width:100%;" />
                    <span>Upload Photo</span>
                    <input type="file" name="profile_image" id="profile_image_real" class="upload" onchange="showimagepreview(this)" />
                    <input type="hidden" name="old_profile_image" id="old_profile_image" value="" />
                </div>


                <div class="col-md-9">
                    <div class="col-md-6">
                        <input class="input-field input-box" type="text" name="agent_name" id="agent_name"  placeholder="Full Name">
                        <div class="error" id="error_name" ></div>
                        <div class="">
                            <label class="input-name"><i class="fa fa-calendar"></i><input type="text" id="agent_dob"  class="input-field" value="" id="dpd1" name="agent_dob" ></label><div class="error" id="error_dob" ></div>
                        </div>
                        <input class="form-control ng-pristine ng-valid ng-touched" type="text" id="paypal_id"  name="paypal_email_address" placeholder="Pay Pal Id">
                        <div class="errors" id="error_pay" ></div><br>
                        <input class="form-control ng-pristine ng-valid ng-touched" type="text" id="address_1"  name="agent_address1" placeholder="Address Line 1">
                        <div class="errors" id="error_add1" ></div><br>
                        <div class="input-box">
                            <input class="form-control ng-pristine ng-valid ng-touched" type="text" id="country"  name="country_name" placeholder="Country">
                            <div class="errors" id="error_country" ></div><br>
                            <!--<label class="input-name">
                            <select class="form-control ng-pristine ng-valid ng-touched">
                                    <option>Select Country</option>
                              <option>Ahmedabad</option>
                              <option>Kutch</option>
                              <option>Surendranagar</option>
                              <option>Rajkot</option>
                              <option>Morbi</option>
                            </select>
                            </label>-->
                        </div>
                        <div class="input-box">
                            <input class="form-control ng-pristine ng-valid ng-touched" type="text" id="city_name"  name="city_name" placeholder="City">
                            <div class="errors" id="error_city" ></div><br>
                            <!--<label class="input-name">
                            <select class="form-control ng-pristine ng-valid ng-touched">
                              <option>Select City</option>
                              <option>Ahmedabad</option>
                              <option>Kutch</option>
                              <option>Surendranagar</option>
                              <option>Rajkot</option>
                              <option>Morbi</option>
                            </select>
                            </label>-->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <strong>Gender</strong> <br><br>
                        <label><input type="radio" name="gender" id="gender_male" value="M"> Male</label><br>
                        <label><input type="radio" name="gender" id="gender_female" value="F"> Female</label><br><br><br>

                        <input class="form-control ng-pristine ng-valid ng-touched" type="text" id="phone_number"  name="mobile_number" placeholder="Phone Number">
                        <div class="errors" id="error_phn" ></div><br>
                        <input class="form-control ng-pristine ng-valid ng-touched" type="text" id="address_2"  name="agent_address2" placeholder="Address Line 2">
                        <div class="errors" id="error_add2" ></div><br>
                        <div class="input-box">
                            <input class="form-control ng-pristine ng-valid ng-touched" type="text" id="state_name"  name="state_name" placeholder="State">
                            <div class="errors" id="error_state" ></div><br>
                            <!--<label class="input-name">
                            <select class="form-control ng-pristine ng-valid ng-touched">
                              <option>Select State</option>
                              <option>Ahmedabad</option>
                              <option>Kutch</option>
                              <option>Surendranagar</option>
                              <option>Rajkot</option>
                              <option>Morbi</option>
                            </select>
                            </label>-->
                        </div>
                        <input class="form-control ng-pristine ng-valid ng-touched" type="text" id="postal_code"  name="postal_code" placeholder="Postal Code">
                        <div class="errors" id="error_pcode" ></div><br>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <button type="submit" onclick="return update_profile();" class="btn-custom1 create-job pull-right">Submit</button>
            </div>
        </form>


        <!-- 
         <div class="col-md-12 text-center">
             <button type="button" class="btn-custom1 create-job">View All</button>
         </div>-->
        <div class="row">
            <div class="col-md-6">
                <h4>Past Jobs</h4>
                <div style="border-bottom:1px solid #ccc; padding-bottom:15px; margin-bottom:15px;" id = "past_jobs">

                    <!-- <br>
                     <div class="col-md-4" style="padding-left:0px;"><img src="<?php echo WEBSITE_ROOT ?>/images/image-1.jpg" alt="Custmore photo" style="width:100%;" /></div>
                     <div class="col-md-8">
                         <div >
                         <h5 style="margin-top:0px;">ABC Shop</h5>
                         <p>123, ABC Complex, LA <i class="fa fa-spoon"></i> Food<br>
                             12th June 2016 to 15th July 2016<br>
                              <i class="fa fa-map-pin"></i> 2 KM Away<br>
                              <i class="fa fa-gift"></i> 25 $</p>
                              <a href="#" class="pull-right">View Details</a>
                         </div>     
                     </div>
                     <div class="clearfix"></div>-->
                </div>
            </div>
            <div class="clearfix"></div>


        </div>

    </div>

</div>



<script>
    if (top.location != location) {
        top.location.href = document.location.href;
    }


    function update_profile() {
        
        var error = false;
        
        if ($("#agent_name").val() == "") {
            $("#error_name").html("Please enter agent name");
            error = true;
        }
        
        if($("#agent_dob").val() == ""){
            $("#error_dob").html("Please enter Date of Birth");
            error = true;
        }
        
        if($("#paypal_id").val() == ""){
            $("#error_pay").html("Please enter your paypal id");
            error = true;
        }
        
        if($("#address_1").val() == ""){
            $("#error_add1").html("Please enter address1");
            error = true;
        }
        
        if($("#country").val() == ""){
            $("#error_country").html("Please enter your country");
            error = true;
        }
        
        if($("#city_name").val() == ""){
            $("#error_city").html("Please enter your city");
            error = true;
        }
        
        if($("#phone_number").val() == ""){
            $("#error_phn").html("Please enter your contact number");
            error = true;
        }
        
        if($("#address_2").val() == ""){
            $("#error_add2").html("Please enter your address2");
            error = true;
        }
        
        if($("#state_name").val() == ""){
            $("#error_state").html("Please enter your state");
            error = true;
        }
        
        if($("#postal_code").val() == ""){
            $("#error_pcode").html("Please enter your postal code");
            error = true;
        }
        
        if(error){
            return false;
        }else{
            return true;
        }
        
    }

    $(function () {
        window.prettyPrint && prettyPrint();
        $('#dp1').datepicker({
            format: 'dd-mm-yyyy'
        });
        $('#dp2').datepicker({
            format: 'dd-mm-yyyy'
        });



        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');
    });
</script>

</body>
</html>