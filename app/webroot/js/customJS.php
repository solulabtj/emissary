<?php
//echo $website_root = WEBSITE_ROOT;
//exit;
if (isset($_SESSION['current_lat']) && isset($_SESSION['current_lng']) && $_SESSION['current_lat'] > 0 && $_SESSION['current_lng'] > 0) {
    $currentLat = $_SESSION['current_lat'];
    $currentLng = $_SESSION['current_lng'];
} else {
    $currentLat = 0;
    $currentLng = 0;
}
$url =  basename($_SERVER['REQUEST_URI']);

//pr($_SERVER);
?>

<script type="text/javascript">
    //alert('testing');
    //var value = getElementById(email);
    //alert(value);
    var website_root = "<?php echo $website_root; ?>";
    var currentLat = "<?php echo $currentLat; ?>";
    var currentLng = "<?php echo $currentLng; ?>";
    //alert(website_root);
    window.onload = function () {
        if (currentLat == 0 || currentLng == 0)
        {
            getUserLocation();
        }
    };

    function getUserLocation() {
        var startPos;
        var geoOptions = {
            enableHighAccuracy: true
        }

        var geoSuccess = function (position) {
            startPos = position;
            //alert(website_root);
            var pathname = '<?php echo $url; ?>';
            var current_latitude = startPos.coords.latitude;
            var current_longitude = startPos.coords.longitude;
            if (current_latitude > 0 && current_longitude > 0)
            {
                $.ajax({
                    url: website_root + "users/setLatLongInSession",
                    data: "current_latitude=" + current_latitude + "&current_longitude=" + current_longitude,
                    type: "POST",
                    async: false,
                    success: function (data) {
                      if(data == "set" && pathname == "availableShops"){  
                          window.location.href = website_root+"agents/availableShops";
                    }
                        //alert(pathname);
                        //alert(data);
                    }
                });
            }

        };
        var geoError = function (error) {
            console.log('Error occurred. Error code: ' + error.code);
            // error.code can be:
            //   0: unknown error
            //   1: permission denied
            //   2: position unavailable (error response from location provider)
            //   3: timed out
        };

        navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    }

    function onlyNumbers(event) {
        //alert('hello');
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function onlyNumbersAndDashes(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45)
            return false;

        return true;
    }

    function lettersOnly(evt) {
        //alert('hello');
        evt = (evt) ? evt : event;
        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
        if (charCode == 32)
            return true;
        if (charCode > 31 && (charCode < 65 || charCode > 90) &&
                (charCode < 97 || charCode > 122)) {
            return false;
        } else
            return true;
    }

    // Except only numbers and dot (.) for salary textbox
    function onlyDotsandNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode
        if (charCode == 46) {
            return true;
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function email_validate(elementValue, errorDivId) {

        var exp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var result = exp.test(elementValue);
        if (result) {
            $("#errorDivId").html('');
            return true;
        } else {
            $("#errorDivId").html('Please enter a valid email address');
            return false;
        }
        return result;

    }

    function formatDate(dateString)
    {
        var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

        dateString = dateString.replace(/-/g, '/');
        var d = new Date(dateString);
        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        var newDate = curr_date + " " + m_names[curr_month]
                + " " + curr_year;

        return newDate;
    }

    function format(date) {
        var formattedDate = new Date(date);
        var d = formattedDate.getDate();
        var m = formattedDate.getMonth();
        m += 1;  // JavaScript months are 0-11
        var y = formattedDate.getFullYear();
        
        if(m > 10){
            m = m;
        }else{
            m = "0"+m;
        }
        
        if(d > 10){
            d = d;
        }else{
            d = "0"+d;
        }
        
        var finalDate = m+"-"+d+"-"+y
        return finalDate;
    }

</script>