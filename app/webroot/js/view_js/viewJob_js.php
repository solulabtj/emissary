<?php


$access_code = $_SESSION['Auth']['User']['access_code'];

$currentLat = isset($_SESSION['current_lat'])?$_SESSION['current_lat']:0;

$currentLng = isset($_SESSION['current_lng'])?$_SESSION['current_lng']:0;

$api_root = API_ROOT;

$website_root = WEBSITE_ROOT;

$job_id = basename($this->here);

//pr($_SERVER);exit;

?>


<script type="text/javascript">
	var access_code = "<?php echo $access_code; ?>";
	var api_root = "<?php echo $api_root; ?>";
	var job_id  = "<?php echo $job_id; ?>";
	
	var accessToken = "<?php print ACCESSTOKEN; ?>";
	var accessTokenSalt = "<?php print ACCESSTOKENSALT; ?>";
	
	
	var currentLat = "<?php print $currentLat; ?>";
	var currentLng = "<?php print $currentLng; ?>";
	
	
	$(document).ready(function(){
		$.ajax({
			url: api_root + "jobs/view_job",  
			data:"job_id="+job_id+"&current_lat="+currentLat+"&current_lng="+currentLng,
			method:"POST",
			beforeSend: function (request) {
				request.setRequestHeader("Access-Token", accessToken);
				request.setRequestHeader("Access-Code", accessTokenSalt);
				request.setRequestHeader("Access-Type-Web", "1");
			},
			async:false,
			success: function(data) {
				var jobCount = 0;
				var listAddress = "";
				var mapPins = [];
				if(data === undefined) {
					var listJobs = "<div>No results found.</div>";	
				}
				
				if(data['Success'] !== undefined && data['Success']['success'] !== undefined) {
					var arrSuccess = data['Success']['success'];
					console.log(arrSuccess);
					rewardType = arrSuccess['Job']['reward_type'];
					if($.isNumeric(rewardType) && rewardType == 1)
					{
						compensation = arrSuccess['Job']['job_compensation']+" KWD";
					}
					else
					{
						compensation = arrSuccess['Coupon']['coupon_name'];	
					}
						
					$("#job_id").val(arrSuccess['Job']['job_id']);
					$("#company_cover").attr("src", arrSuccess['ClientUserInfo']['company_cover']);
					$("#company_name").html(arrSuccess['ClientUserInfo']['company_name']);
					$("#company_overview").html(arrSuccess['ClientUserInfo']['company_overview']);
					$("#company_agency").html(arrSuccess['ClientUserInfo']['agency_name']);
                                        $("#policy").html(arrSuccess['ClientUserInfo']['company_policy']);
					$("#job_compensation").html(compensation);
					$("#job_time_span").html(arrSuccess['ClientUserInfo']['agency_name']);
					
					
					mapPins[0] = [ ' Your Location ' , currentLat , currentLng ,1];
					
                                      var cou = arrSuccess['JobAddress'].length;
                                        if(cou > 1){
                                            $.each(arrSuccess['JobAddress'], function(addressIndex, addressValue){
                                                    mapPins[addressIndex+1]= [ addressValue['address']  , addressValue['lattitude'] , addressValue['longitude'] , (addressIndex+2)];	

                                                    listAddress += '<label><input type="radio" name="job_location" value = "' + addressValue['job_location_id'] + '"> ' + addressValue['address'] + '</label><br>';

                                            });
                                        }else{
                                            $.each(arrSuccess['JobAddress'], function(addressIndex, addressValue){
                                                    mapPins[addressIndex+1]= [ addressValue['address']  , addressValue['lattitude'] , addressValue['longitude'] , (addressIndex+2)];	

                                                    listAddress += '<label><input type="radio" name="job_location" value = "' + addressValue['job_location_id'] + '" checked> ' + addressValue['address'] + '</label><br>';

                                            });
                                        }
					//alert(listAddress);
					
					$("#job_address").html(listAddress);
				}
				else if(data['Error'] !== undefined && data['Error']['error'] !== undefined) {
					var error = data['Error']['error'];
					if(error === "invalid request")
					{
						var listJobs = "<div>Please share your location to view this job.</div>";	
					}
					else
					{
						var listJobs = "<div>" + error + "</div>";		
					}
				}
				else {
					var listJobs = "<div>No more jobs to show.</div>";	
				}
				showMap(mapPins);
			}
		});
		
		
		return false;
	});
	
	function requestJob(req)
	{
		var job_id = $("#job_id").val();
		var agent_email_id = $("#agent_email_id").val();
		var job_location_id = $('input[name=job_location]:checked').val();
		$('#myModal').modal('hide');
                
                if(req == "agree"){
                
		if(job_id>0 && job_location_id >0)
		{   
			$.ajax({
				url: api_root + "agents/apply_job",  
				data:"job_id="+job_id+"&job_location_id="+job_location_id+"&agent_email_id="+agent_email_id,
				method:"POST",
				beforeSend: function (request) {
					request.setRequestHeader("Access-Token", accessToken);
					request.setRequestHeader("Access-Code", accessTokenSalt);
					request.setRequestHeader("Access-Type-Web", "1");
				},
				async:false,
				success: function(data) {
					var jobCount = 0;
					var listJobs = "";
					if(data === undefined) {
						alert('Unable to process your request. Please try again later');
						window.location.replace('<?php echo $website_root."agents/availableShops" ?>');
					}
					
					if(data['Success'] !== undefined && data['Success']['success'] !== undefined) {
						var arrSuccess = data['Success']['success'];
						//console.log(arrSuccess);
							alert(arrSuccess);
							window.location.replace('<?php echo $website_root."agents/availableShops" ?>');
						}
					else if(data['Error'] !== undefined && data['Error']['error'] !== undefined) {
						var error = data['Error']['error'];
						if(error === "invalid request")
						{
							$(".errClass").html('Please select address to apply for job.');	
						}
						else
						{
							//alert(error);
							$(".errClass").html(error);	
						}
					}
					else {
						alert('Unable to process your request. Please try again later');
						window.location.replace(website_root+'agents/availableShops');
					}
				}
			});
		}
		else
		{
			$(".errClass").html('Please select address to apply for job.');	
		}
                
                }
		else
		{
			$(".errClass").html('Please agree company policy to send request.');	
		}
                
	}

	function showMap(location)
	{
		/*var locations = [
		  ['Bondi, Beach', -33.890542, 151.274856, 4],
		  ['Coogee Beach', -33.923036, 151.259052, 5],
		  ['Cronulla Beach', -34.028249, 151.157507, 3],
		  ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
		  ['Maroubra Beach', -33.950198, 151.259302, 1],
		];*/
		
		var locations = location;
		//alert(locations);
		var map = new google.maps.Map(document.getElementById('map'), {
		  zoom: 10,
		  center: new google.maps.LatLng(currentLat,currentLng),
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		});
	
		var infowindow = new google.maps.InfoWindow();
	
		var marker, i;
	
		for (i = 0; i < locations.length; i++) {  
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map
		  });
	
		  google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
			  infowindow.setContent(locations[i][0]);
			  infowindow.open(map, marker);
			}
		  })(marker, i));
		}	
	}
	

</script>