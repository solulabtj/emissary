<?php
$access_code = $_SESSION['Auth']['User']['access_code'];

$currentLat = isset($_SESSION['current_lat']) ? $_SESSION['current_lat'] : 0;

$currentLng = isset($_SESSION['current_lng']) ? $_SESSION['current_lng'] : 0;

$api_root = API_ROOT;
$website_root = WEBSITE_ROOT;
//pr($_SESSION);exit;
?>


<script type="text/javascript">
    var access_code = "<?php echo $access_code; ?>";
    var api_root = "<?php echo $api_root; ?>";
    var website_root = "<?php echo $website_root; ?>";

    var accessToken = "<?php print ACCESSTOKEN; ?>";
    var accessTokenSalt = "<?php print ACCESSTOKENSALT; ?>";


    var currentLat = "<?php print $currentLat; ?>";
    var currentLng = "<?php print $currentLng; ?>";

    var jobListCount = 0;

    var tempPins = [];
    var mapPins = [];
    mapPins[0] = [' Your Location ', currentLat, currentLng, 1];

    $(document).ready(function () {

        if ($.isNumeric(currentLat) && $.isNumeric(currentLng) && currentLat > 0 && currentLng > 0)
        {
            getAvailableJobs();
        }


        return false;
    });



    function getAvailableJobs()
    {
        //alert(jobListCount);
        //alert(starting_number);
        $.ajax({
            url: api_root + "jobs/get_available_shops",
            data: "current_lat=" + currentLat + "&current_lng=" + currentLng + "&starting_number=" + jobListCount,
            method: "POST",
            beforeSend: function (request) {
                request.setRequestHeader("Access-Token", accessToken);
                request.setRequestHeader("Access-Code", accessTokenSalt);
                request.setRequestHeader("Access-Type-Web", "1");
            },
            async: false,
            success: function (data) {

                var listJobs = "";
                var errorMessage = "";
                var catDisplay = "";
                var distanceRangeValue = "";
                var compensationRangeValue = "";
                var defaultMinDistance = 0;
                var defaultMaxDistance = 0;
                var defaultMinCompensation = 0;
                var defaultMaxCompensation = 0;

                var filterMinDistance = 0;
                var filterMaxDistance = 0;
                var filterMinCompensation = 0;
                var filterMaxCompensation = 0;
                if (data === undefined) {
                    var errorMessage = "<div>No results found.</div>";
                }
                // if(data['Success']['success']['JobList'].length !== 0 && data['Success'] !== undefined && data['Success']['success'] !== undefined){
                if (data['Success'] !== undefined && data['Success']['success'] !== undefined) {
                    var arrSuccess = data['Success']['success'];

                    defaultMaxDistance = arrSuccess['AgentFilter']['JobFilter']['default_max_distance'];
                    defaultMaxCompensation = arrSuccess['AgentFilter']['JobFilter']['default_max_compensation'];

                    filterMinDistance = parseInt(arrSuccess['AgentFilter']['JobFilter']['min_distance']);
                    filterMaxDistance = parseInt(arrSuccess['AgentFilter']['JobFilter']['max_distance']);
                    filterMinCompensation = parseInt(arrSuccess['AgentFilter']['JobFilter']['minimum_compensation']);
                    filterMaxCompensation = parseInt(arrSuccess['AgentFilter']['JobFilter']['maximum_compensation']);




                    /*distanceRangeValue = arrSuccess['AgentFilter']['JobFilter']['min_distance']+","+arrSuccess['AgentFilter']['JobFilter']['max_distance'];
                     compensationRangeValue = arrSuccess['AgentFilter']['JobFilter']['minimum_compensation']+","+arrSuccess['AgentFilter']['JobFilter']['maximum_compensation'];*/

                    $('#defaultMinDistance').html(defaultMinDistance);
                    $('#defaultMaxDistance').html(defaultMaxDistance);
                    $('#defaultMinCompensation').html(defaultMinCompensation);
                    $('#defaultMaxCompensation').html(defaultMaxCompensation);
                    $('#hidFilterId').val(arrSuccess['AgentFilter']['JobFilter']['job_filter_id']);


                    $("#distance_slider").slider({id: "slider12b", min: defaultMinDistance, max: defaultMaxDistance, range: true, value: [filterMinDistance, filterMaxDistance]});
                    $("#compensation_slider").slider({id: "slider12b", min: defaultMinCompensation, max: defaultMaxCompensation, range: true, value: [filterMinCompensation, filterMaxCompensation]});




                    $('select[name^="due_date"] option[value="' + arrSuccess['AgentFilter']['JobFilter']['due_date_span'] + '"]').attr("selected", "selected");


                    $.each(arrSuccess['JobCategories'], function (catIndex, catValue) {
                        if (catValue['JobCategory']['is_checked'] == true)
                        {
                            catDisplay += '<label><input type="checkbox" checked class="jobCategories" name="job_categories[]" data-cat_id = "' + catValue['JobCategory']['job_category_id'] + '" data-filter_cat_id = "' + catValue['JobCategory']['job_filter_category_id'] + '"> ' + catValue['JobCategory']['job_category_title'] + '</label>';
                        } else
                        {
                            catDisplay += '<label><input type="checkbox" class="jobCategories" name="job_categories[]" data-cat_id = "' + catValue['JobCategory']['job_category_id'] + '" data-filter_cat_id = "' + catValue['JobCategory']['job_filter_category_id'] + '"> ' + catValue['JobCategory']['job_category_title'] + '</label>';
                        }

                    });

                    $("#category_list").html(catDisplay);

                    if (data['Success']['success']['JobList'].length === 0)
                    {
                        var errorMessage = "<div>No jobs found for specified filters.</div>";
                    } else
                    {

                        $.each(arrSuccess['JobList'], function (jobIndex, jobValue) {
                            rewardType = jobValue['Job']['reward_type'];
                            if ($.isNumeric(rewardType) && rewardType == 1)
                            {
                                compensation = jobValue['Job']['job_compensation'];
                            } else
                            {
                                compensation = jobValue['Coupon']['coupon_name'];
                            }

                            startDate = format(jobValue['Job']['job_start_date']);
                            endDate = format(jobValue['Job']['job_end_date']);
                           
                           // startDate = jobValue['Job']['job_start_date'];
                            //endDate = jobValue['Job']['job_end_date'];

                            listJobs += ' <div style="border-bottom:1px solid #ccc; padding-bottom:15px; margin-bottom:15px;"> ' +
                                    ' <br> ' +
                                    ' <div class="col-md-4" style="padding-left:0px;"><img src="' + jobValue['ClientDetails']['ClientUserInfo']['company_logo_image'] + '" alt="Custmore photo" style="width:100%;" /></div> ' +
                                    ' <div class="col-md-8"> ' +
                                    ' <div > ' +
                                    ' <h5 style="margin-top:0px;">' + jobValue['ClientDetails']['ClientUserInfo']['company_name'] + '</h5> ' +
                                    ' <p><i class="fa fa-map-pin"></i> ' + jobValue['PrimaryJobAddress']['address'] + ' <i class="fa fa-spoon"></i> Food<br> ' +
                                    ' <i class="fa fa-calendar"></i> ' + startDate + ' to ' + endDate + '<br> ' +
                                    ' <i class="fa fa-map-pin"></i> ' + jobValue['Job']['distance'] + ' Miles Away<br> ' +
                                    ' <i class="fa fa-gift"></i> ' + compensation + ' </p> ' +
                                    ' <a href=" ' + website_root + "agents/viewJob/" + jobValue['Job']['job_id'] + ' " class="pull-right">View Details</a> ' +
                                    ' </div> ' +
                                    ' </div> ' +
                                    ' <div class="clearfix"></div> ' +
                                    ' </div>';
                            jobListCount++;


                            tempPins = [jobValue['PrimaryJobAddress']['address'], jobValue['PrimaryJobAddress']['lattitude'], jobValue['PrimaryJobAddress']['longitude'], (jobIndex + 2)];
                            //console.log(tempPins);

                            mapPins.push(tempPins);
                            //console.log(mapPins);			

                        });
                        showMap(mapPins);
                    }
                } else if (data['Error'] !== undefined && data['Error']['error'] !== undefined) {
                    var error = data['Error']['error'];
                    if (error === "invalid request")
                    {
                        var errorMessage = "<div>Please share your location to see completed jobs.</div>";
                    } else
                    {
                        var errorMessage = "<div>" + error + "</div>";
                    }
                } else {
                    var errorMessage = "<div>No more jobs to show.</div>";
                }
                if (jobListCount == 0)
                {
                    $("#available_jobs").html(listJobs);
                } else
                {
                    $("#available_jobs").append(listJobs);
                }
                /* }else{
                 var errorMessage = "<div>No Jobs are Available.</div>";
                 }
                 */
                $("#printError").html(errorMessage);
            }
        });



        $("#updateFilterForm").on('submit', (function (e) {
            e.preventDefault();
            var selectedCategories = [];
            var filterId = 0;
            var dueDateSpan = 0;
            var distanceRange = 0;
            var compensationRange = 0;
            var distanceArr = [];
            var compensationArr = [];
            var minDistance = 0;
            var maxDistance = 0;
            var minCompensation = 0;
            var maxCompensation = 0;
            //alert('sadasdasd');
            $(".jobCategories").each(function () {

                var jobFilter = {};
                jobFilter['job_category_id'] = $(this).attr('data-cat_id');
                jobFilter['job_filter_category_id'] = $(this).attr('data-filter_cat_id');

                //alert($(this).is(':checked') );
                if ($(this).is(':checked'))
                {
                    jobFilter['is_checked'] = 1;
                } else
                {
                    jobFilter['is_checked'] = 0;
                }
                selectedCategories.push(jobFilter);
            });

            filterString = JSON.stringify(selectedCategories);
            filterId = $('#hidFilterId').val();
            dueDateSpan = $('#due_date').val();

            distanceRange = $('#distance_slider').val();
            compensationRange = $('#compensation_slider').val();

            distanceArr = distanceRange.split(',');
            compensationArr = compensationRange.split(',');

            //console.log(compensationArr);
            minDistance = distanceArr[0];
            maxDistance = distanceArr[1];
            minCompensation = compensationArr[0];
            maxCompensation = compensationArr[1];


            $.ajax({
                url: api_root + "agents/update_job_filter",
                data: "job_filter_id=" + filterId + "&due_date_span=" + dueDateSpan + "&min_distance=" + minDistance + "&max_distance=" + maxDistance + "&minimum_compensation=" + minCompensation + "&maximum_compensation=" + maxCompensation + "&job_categories=" + filterString,
                method: "POST",
                beforeSend: function (request) {
                    request.setRequestHeader("Access-Token", accessToken);
                    request.setRequestHeader("Access-Code", accessTokenSalt);
                    request.setRequestHeader("Access-Type-Web", "1");
                },
                async: false,
                success: function (data) {
                    if (data === undefined) {
                        alert("Unable to process your request, please try again.");
                        location.reload();
                    }
                    var listJobs = "";
                    if (data['Success'] !== undefined && data['Success']['success'] !== undefined) {
                        var arrSuccess = data['Success']['success'];
                        var message = arrSuccess;
                        $("#updateResponse").html(message);
                        location.reload();
                        //console.log(arrSuccess);
                    } else if (data['Error'] !== undefined && data['Error']['error'] !== undefined) {
                        var error = data['Error']['error'];
                        var message = error;
                        $("#updateResponse").html(message);
                    } else {
                        alert("Unable to process your request, please try again.");
                        location.reload();
                    }

                }
            });
        }));


    }

    function showMap(location)
    {
        /*var locations = [
         ['Bondi, Beach', -33.890542, 151.274856, 4],
         ['Coogee Beach', -33.923036, 151.259052, 5],
         ['Cronulla Beach', -34.028249, 151.157507, 3],
         ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
         ['Maroubra Beach', -33.950198, 151.259302, 1],
         ];*/

        var locations = location;
        //alert(locations);
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(currentLat, currentLng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }


</script>