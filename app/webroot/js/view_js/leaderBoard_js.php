<?php
echo $this->Html->script('jquery.js');
echo $this->Html->script('jquery.min');

$access_code = $_SESSION['Auth']['User']['access_code'];

$currentLat = isset($_SESSION['current_lat']) ? $_SESSION['current_lat'] : 0;

$currentLng = isset($_SESSION['current_lng']) ? $_SESSION['current_lng'] : 0;

$api_root = API_ROOT;
$website_root = WEBSITE_ROOT;
//pr($_SESSION);exit;
?>


<script type="text/javascript">
    var access_code = "<?php echo $access_code; ?>";
    var api_root = "<?php echo $api_root; ?>";
    var website_root = "<?php echo $website_root; ?>";

    var accessToken = "<?php print ACCESSTOKEN; ?>";
    var accessTokenSalt = "<?php print ACCESSTOKENSALT; ?>";

    $(document).ready(function () {
        getLeaderBorad();

    });

    function getLeaderBorad()
    {
        //alert(jobListCount);
        //alert(starting_number);
        $.ajax({
            url: website_root + "agents/fetch_data",
            method: "POST",
            beforeSend: function (request) {
                request.setRequestHeader("Access-Token", accessToken);
                request.setRequestHeader("Access-Code", accessTokenSalt);
                request.setRequestHeader("Access-Type-Web", "1");
            },
            async: false,
            success: function (data) {
                $('#leaderboard_data').replaceWith($('#leaderboard_data').html(data));
            }
        });

    }


</script>