<?php


$access_code = $_SESSION['Auth']['User']['access_code'];

$currentLat = isset($_SESSION['current_lat'])?$_SESSION['current_lat']:0;

$currentLng = isset($_SESSION['current_lng'])?$_SESSION['current_lng']:0;

$api_root = API_ROOT;
//pr($_SESSION);exit;

?>


<script type="text/javascript">
	var access_code = "<?php echo $access_code; ?>";
	var api_root = "<?php echo $api_root; ?>";
	
	
	var accessToken = "<?php print ACCESSTOKEN; ?>";
	var accessTokenSalt = "<?php print ACCESSTOKENSALT; ?>";
	
	
	var currentLat = "<?php print $currentLat; ?>";
	var currentLng = "<?php print $currentLng; ?>";
	
	
	$(document).ready(function(){
		$.ajax({
			url: api_root + "agents/view_profile",  
			//data:"access_code="+access_code,
			method:"POST",
			beforeSend: function (request) {
                request.setRequestHeader("Access-Token", accessToken);
				request.setRequestHeader("Access-Code", accessTokenSalt);
				request.setRequestHeader("Access-Type-Web", "1");
            },
			async:false,
			success: function(data) {
				if(data === undefined) {
					alert("Unable to process your request, please try again.");
					location.reload();
				}
				
				if(data['Success'] !== undefined && data['Success']['success'] !== undefined) {
					var arrSuccess = data['Success']['success'];
					
					if(arrSuccess['AgentUserInfo']['gender'] == 'M')
					{
						$('#gender_male').prop('checked',true);
					}
					else
					{
						$('#gender_female').prop('checked',true);	
					}
					
					$("#profile_image").attr("src", arrSuccess['AgentUserInfo']['profile_image']);
					$("#old_profile_image").attr("src", arrSuccess['AgentUserInfo']['profile_image']);
					$("#agent_name").val(arrSuccess['AgentUserInfo']['agent_name']);
					$("#agent_dob").val(arrSuccess['AgentUserInfo']['agent_dob']);
					$("#paypal_id").val(arrSuccess['AgentUserInfo']['paypal_email_address']);
					$("#phone_number").val(arrSuccess['AgentUserInfo']['mobile_number']);
					$("#address_1").val(arrSuccess['AgentUserInfo']['agent_address1']);
					$("#address_2").val(arrSuccess['AgentUserInfo']['agent_address2']);
					$("#country").val(arrSuccess['AgentUserInfo']['country_name']);
					$("#state_name").val(arrSuccess['AgentUserInfo']['state_name']);
					$("#city_name").val(arrSuccess['AgentUserInfo']['city_name']);
					$("#postal_code").val(arrSuccess['AgentUserInfo']['postal_code']);
					
					console.log(arrSuccess['AgentUserInfo']['agent_name']);
					
				}
				else if(data['Error'] !== undefined && data['Error']['error'] !== undefined) {
					error = data['Error']['error'];
					alert(error);
					location.reload();
				}
				else {
					alert("Unable to process your request, please try again.");
					location.reload();
				}
			}
		});	
		
		$.ajax({
			url: api_root + "agents/view_completed_jobs",  
			data:"current_lat="+currentLat+"&current_lng="+currentLng,
			method:"POST",
			beforeSend: function (request) {
                request.setRequestHeader("Access-Token", accessToken);
				request.setRequestHeader("Access-Code", accessTokenSalt);
				request.setRequestHeader("Access-Type-Web", "1");
            },
			async:false,
			success: function(data) {
				if(data === undefined) {
					alert("Unable to process your request, please try again.");
					location.reload();
				}
				
				var count_jobs_completed = 0;
				
				var listJobs = "";
				if(data['Success'] !== undefined && data['Success']['success'] !== undefined) {
					var arrSuccess = data['Success']['success'];
					console.log(arrSuccess);
					
					$.each(arrSuccess, function(jobIndex, jobValue){
							
						rewardType = jobValue['Job']['reward_type'];
						if($.isNumeric(rewardType) && rewardType == 1)
						{
							compensation = jobValue['Job']['job_compensation'];
						}
						else
						{
							compensation = jobValue['Coupon']['coupon_name'];	
						}
						
						startDate = formatDate(jobValue['Job']['job_start_date']);
						endDate = formatDate(jobValue['Job']['job_end_date']);
									
						listJobs += '<br>'+
										'<div class="col-md-4" style="padding-left:0px;"><img src="' + jobValue['ClientDetails']['ClientUserInfo']['company_logo_image'] + '" alt="Custmore photo" style="width:100%;" /></div>'+
										'<div class="col-md-8">'+
											'<div >'+
											'<h5 style="margin-top:0px;"> ' + jobValue['ClientDetails']['ClientUserInfo']['company_name'] + ' </h5>'+
											'<p> ' +jobValue['AgentJob']['JobAddress']['address'] + ' <i class="fa fa-spoon"></i> ' + jobValue['JobCategory']['job_category_title'] + '<br>'+ startDate +' to '+ endDate +'<br> '+
												 '<i class="fa fa-map-pin"></i> ' + jobValue['Job']['distance'] + ' Miles Away<br>' +
												 '<i class="fa fa-gift"></i> ' + compensation + '</p>'+
											'</div>'+
										'</div>'+
										'<div class="clearfix"></div>';
							count_jobs_completed++;
					});
				}
				else if(data['Error'] !== undefined && data['Error']['error'] !== undefined) {
					var error = data['Error']['error'];
					if(error === "invalid request")
					{
						var listJobs = "<div>Please share your location to see completed jobs.</div>";	
						
					}
					else
					{
						var listJobs = "<div>" + error + "</div>";
						
					}
				}
				else {
					alert("Unable to process your request, please try again.");
					location.reload();
				}
				
				if(count_jobs_completed==0)
				{					
					$('#past_jobs').text('You have not completed any job yet.');
				}
				
				else
				{
					$("#past_jobs").html(listJobs);
				}
			}
		});	
		
		$("#updateProfileForm").on('submit',(function(e) {
			e.preventDefault();
			$.ajax({
				url: api_root + "agents/update_profile",  
				data: new FormData(this),
				method:"POST",
				beforeSend: function (request) {
					request.setRequestHeader("Access-Token", accessToken);
					request.setRequestHeader("Access-Code", accessTokenSalt);
					request.setRequestHeader("Access-Type-Web", "1");
				},
				contentType: false,
				cache: false,
				processData:false,
				async:false,
				success: function(data) {
					if(data === undefined) {
						alert("Unable to process your request, please try again.");
					location.reload();
					}
					var listJobs = "";
					if(data['Success'] !== undefined && data['Success']['success'] !== undefined) {
						var arrSuccess = data['Success']['success'];
						var message = arrSuccess;
						$("#updateResponse").html(message);
						location.reload();
						//console.log(arrSuccess);
					}
					else if(data['Error'] !== undefined && data['Error']['error'] !== undefined) {
						var error = data['Error']['error'];
						var message = error;
						$("#updateResponse").html(message);
					}
					else {
						alert("Unable to process your request, please try again.");
						location.reload();
					}
					
				}
			});	
		}));
		return false;
	});
	

</script>