<?php


$access_code = $_SESSION['Auth']['User']['access_code'];

$currentLat = isset($_SESSION['current_lat'])?$_SESSION['current_lat']:0;

$currentLng = isset($_SESSION['current_lng'])?$_SESSION['current_lng']:0;

$api_root = API_ROOT;
//pr($_SESSION);exit;

?>


<script type="text/javascript" language="JavaScript">
	var access_code = "<?php echo $access_code; ?>";
	var api_root = "<?php echo $api_root; ?>";
	
	
	var accessToken = "<?php print ACCESSTOKEN; ?>";
	var accessTokenSalt = "<?php print ACCESSTOKENSALT; ?>";
	
	
	var currentLat = "<?php print $currentLat; ?>";
	var currentLng = "<?php print $currentLng; ?>";
	
	window.onbeforeunload = function(){
		alert("asdfasdasdf");
	};
	
	$(document).ready(function(){
		$.ajax({
			url: api_root + "tests/start_test",  
			//data:"current_lat="+currentLat+"&current_lng="+currentLng,
			method:"POST",
			beforeSend: function (request) {
                request.setRequestHeader("Access-Token", accessToken);
				request.setRequestHeader("Access-Code", accessTokenSalt);
				request.setRequestHeader("Access-Type-Web", "1");
            },
			async:false,
			success: function(data) {
				if(data === undefined) {
					alert("Unable to process your request, please try again.");
					location.reload();
				}
				var listQuestions = "";
				if(data['Success'] !== undefined && data['Success']['success'] !== undefined) {
					var arrSuccess = data['Success']['success'];
					
					var testTime = 0;
					var question_no = 1;
					console.log(arrSuccess);
					testTime = arrSuccess['TestConfig']['test_duration'];
					$.each(arrSuccess['Test'], function(qIndex, qValue){	
							//console.log(qValue);
						listQuestions += '<input type = "hidden" class="questionId" value="' + qValue['TestQuestion']['test_question_id'] + '">';
						listQuestions += '<div class="question-box"><p>(' + question_no + ')' + qValue['TestQuestion']['test_question_description'] + ' </p>';
						
						$.each(qValue['TestQuestionOption'], function(qOptionIndex, qOptionValue){	
							listQuestions += '<label><input type="checkbox" class="question_'+qOptionValue['test_question_id']+'" name="test_options" value="'+qOptionValue['test_option_id']+'"> ' + qOptionValue['answer_text'] + ' </label><br>';
						});
						listQuestions += '</div>';
						
						question_no++;
					});
				}
				else if(data['Error'] !== undefined && data['Error']['error'] !== undefined) {
					var error = data['Error']['error'];
					alert(error);
					window.location.replace("<?php echo WEBSITE_ROOT?>agents/updateProfile");
				}
				else {
					alert("Unable to process your request, please try again.");
					location.reload();
				}
				$(".questionList").html(listQuestions);
				$(".timer").attr('data-minutes-left',testTime);
			}
		});	
		
		$("#submitTest").on('submit',(function(e) {
			var selectedAnswers = [];
			e.preventDefault();
			//alert('sadasdasd');
			$(".questionId").each( function (aIndex,aValue) {
				
				var questionObj = {};
				questionId = $(this).val();
				
				var option_ids = [];
				
				$(".question_"+questionId+"").each( function (oIndex,oValue) {
				
					
					//alert($(this).is(':checked') );
					if($(this).is(':checked'))
					{
						optionId = $(this).val();
						option_ids.push(optionId);
					}
					
				});
				if(option_ids.length === 0)
				{
					delete questionObj;	
				}
				else
				{
					questionObj['question_id'] = questionId;
					questionObj['option_ids'] = option_ids;
					selectedAnswers.push(questionObj);
				}
			});
			
			answerString = JSON.stringify(selectedAnswers);
			
			//console.log(answerString);
			
			//alert(answerString);
			filterId = $('#hidFilterId').val();
			dueDateSpan = $('#due_date').val();
			var agent_email_id = $("#agent_email_id").val();
			
			$.ajax({
				url: api_root + "tests/submit_test",  
				data:"test_answers="+answerString+"&agent_email_id="+agent_email_id,
				method:"POST",
				beforeSend: function (request) {
					request.setRequestHeader("Access-Token", accessToken);
					request.setRequestHeader("Access-Code", accessTokenSalt);
					request.setRequestHeader("Access-Type-Web", "1");
				},
				async:false,
				success: function(data) {
					
					
					if(data === undefined) {
						alert("Unable to process your request, please try again later.");
						location.reload();
					}
					var listJobs = "";
					if(data['Success'] !== undefined && data['Success']['success'] !== undefined) {
						var arrSuccess = data['Success']['success'];
						var message = arrSuccess;
						if(arrSuccess['is_passed'] == 1)
						{
							finalResult = "PASS";	
						}
						else
						{
							finalResult = "FAIL";	
						}
						
						$('#testResult').attr('data-percent',arrSuccess['percentage_earned']);
						$('#testResultSpan').html(arrSuccess['percentage_earned']+'%');
						$('#finalResult').html(finalResult);
						//$('#myModal').modal('toggle');
						$('head').append('<link rel="stylesheet" href="<?php echo WEBSITE_ROOT?>css/reset-progress.css">');
						$('#modalContact').modal({
							backdrop: 'static',
							keyboard: false
						})
						//$('#myModal').modal('hide');
						//location.reload();
						//console.log(arrSuccess);
					}
					else if(data['Error'] !== undefined && data['Error']['error'] !== undefined) {
						var error = data['Error']['error'];
						var message = error;
						alert(error);
						window.location.replace("<?php echo WEBSITE_ROOT?>agents/updateProfile");
						//$("#updateResponse").html(message);
					}
					else {
						alert("Unable to process your request, please try again.");
						location.reload();
					}
					
				}
			});	
		}));
		
		
		
		return false;
	});
	
	$(function(){
	  $('.timer').startTimer({
		onComplete: function(element){
		  alert('Time up! Click ok to sumit the test.');
		  $("#submitTest").submit();
			element.addClass('is-complete');
		}
	  }).click(function(){ location.reload() });
	})
	
	$(window).load(function() {
		
			window.onbeforeunload = function (event) {
				var message = 'Important: Please click on \'Save\' button to leave this page.';
				if (typeof event == 'undefined') {
					//alert('sdfsdfsdf');
					event = window.event;
				}
				if (event) {
					event.returnValue = message;
				}
				return message;
			};
	});
	
	$(function () {
		$("a").not('#lnkLogOut').click(function () {
			window.onbeforeunload = null;
		});
		$(".btn").click(function () {
			window.onbeforeunload = null;
	});
	});

	

</script>