<?php

class ClientsController extends AppController {

    public $uses = array('ClientUserInfo', 'Admin', 'AgentJob', 'Coupon', 'ClientAddress', 'AgentUserInfo', 'ClientImage', 'Job', 'Survey', 'SurveyQuestion', 'Coupon', 'JobLocation', 'SurveyCategory', 'JobCategory', 'ClientAddress', 'AgentRank', 'SurveyCategories', 'SurveyQuestionOption', 'AgentSurveyAnswer', 'ClientDashMsg', 'AgentRating', 'ClientSubscription', 'ClientAlerts', 'Coupon');

//public $components = array('Session');
    public function beforeFilter() {
        parent::beforeFilter();
//pr($this->Session->read('Auth.User'));exit;
        if (!$this->Session->check('Auth.User.client_id')) {
            $this->redirect(array('controller' => 'home', 'action' => 'index'));
        }
        App::import('Vendor', 'phpqrcode' . DS . 'qrlib');
        App::import('Vendor', 'tcpdf/tcpdf');
        App::uses('PHPExcel', 'Vendor/excel');

//require_once(APP . 'Vendor' . DS . 'phpqrcode' . DS . 'qrlib.php');

        $client_id = $this->Session->read('Auth.User.client_id');
    }

    public function index() {
        $this->layout = 'client';
        $limit = 3;
        $client_id = $this->Session->read('Auth.User.client_id');

        $to = NOW;
        $from = date("Y-m-d H:i:s", strtotime("-1 week"));


        $display_msgs = $this->ClientDashMsg->find('all', array('order' => array('created DESC'), 'limit' => 5, 'recursive' => -1));

        $submission_query = "SELECT 
            (SELECT COUNT(*) FROM agent_jobs WHERE client_id = '" . $client_id . "' and approved_by_client = 1) AS approvedJobs, 
            (SELECT COUNT(*) FROM agent_jobs WHERE client_id = '" . $client_id . "' and disapproved_by_client = 1) AS disapprovedJobs,
            (SELECT COUNT(*) FROM agent_jobs WHERE client_id = '" . $client_id . "' and approved_by_client = 0 and disapproved_by_client = 0 and completed_by_agent = 1) AS pendingJobs";
        $submission_count = $this->AgentJob->Query($submission_query);

        $request_list = $this->AgentJob->find('all', array('fields' => array('AgentUserInfo.agent_name', 'AgentJob.*'), 'conditions' => array('AgentJob.client_id' => $client_id, 'AgentJob.completed_by_agent' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.disapproved_by_client' => 0, 'AgentJob.approved_by_client' => 0), 'order' => array('AgentJob.created DESC'), 'limit' => $limit));
//pr($request_list);exit;
        $job_submission_list = $this->AgentJob->find('all', array('fields' => array('AgentUserInfo.agent_name', 'AgentJob.*'), 'conditions' => array('AgentJob.client_id' => $client_id, 'AgentJob.accepted_by_client' => 1, 'AgentJob.declined_by_client' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.completed_by_agent' => 1), 'order' => array('AgentJob.created DESC'), 'limit' => $limit));

        $current_jobsqw = $this->AgentJob->find('all', array('fields' => array('AgentJob.*', 'Job.job_id', 'Job.submission_received'), 'conditions' => array('AgentJob.client_id' => $client_id, 'AgentJob.accepted_by_client' => 1, 'AgentJob.declined_by_client' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.completed_by_agent' => 1, 'Job.job_end_date >=' => $from, 'Job.job_end_date <=' => $to, 'Job.is_active' => 1, 'Job.is_blocked' => 0), 'group' => array('AgentJob.modified'), 'order' => array('Job.job_end_date ASC')));

        $current_job_query = "SELECT count(aj.agent_job_id) AS submissionThisDay,DATE(aj.job_end_time) AS submissionDate,j.* FROM agent_jobs aj
								RIGHT JOIN jobs j ON j.job_id=aj.job_id
								WHERE aj.client_id = " . $client_id . " AND aj.accepted_by_client = 1 AND aj.declined_by_client = 0 AND aj.cancelled_by_agent = 0 AND aj.completed_by_agent = 1 AND j.job_end_date >= '" . $from . "' AND j.job_end_date <= '" . $to . "' AND j.is_active = 1 AND j.is_blocked = 0
								GROUP BY DATE(aj.job_end_time)
								ORDER BY submissionDate ASC";


        if ($current_jobs = $this->ClientAddress->query($current_job_query)) {
//pr($current_jobs);exit;
            $currArr = array();
            $currPreArr = array();
            $dayNumber = 0;
            $oldSubmissionDate = '';
            /* $currPreArr[0] = "Location";
              $currPreArr[1] = "Total Requests";
              $currPreArr[2] = "Total Submissions"; */

//array_push($locArr, $locPreArr);
            foreach ($current_jobs as $job_key => $jobs) {
                $job_id = $jobs['j']['job_id'];
                if (!isset($currArr[$job_id])) {
                    $currArr[$job_id] = array();
                }
                $currSubArr = array();
//$currSubArrTemp = array();
                if ($oldSubmissionDate == $jobs[0]['submissionDate']) {
                    $dayNumber;
                } else {
                    $dayNumber++;
                }

                $currSubArrTemp['jobId'] = $jobs['j']['job_id'];
                $currSubArrTemp['subCount'] = $jobs[0]['submissionThisDay'];
                $currSubArrTemp['subDate'] = $jobs[0]['submissionDate'];
                $currSubArrTemp['day'] = $dayNumber;

//array_push($currSubArr,$currSubArrTemp);
//pr($currSubArr);
                $currArr[$job_id];
                array_push($currArr[$job_id], $currSubArrTemp);
                $oldSubmissionDate = $jobs[0]['submissionDate'];
            }
            ksort($currArr);
//pr($currArr);exit;	
        }

        $loc_query = "SELECT ca.client_address_id AS location, sum(j.request_received) AS totalRequest, sum(j.submission_received) as totalSubmission FROM client_address ca
						
						LEFT JOIN job_locations jl ON jl.client_address_id = ca.client_address_id
						LEFT JOIN jobs j ON j.job_id=jl.job_id
						WHERE ca.client_id = " . $client_id . " AND ca.is_active = 1 AND ca.is_blocked = 0
						GROUP BY ca.client_address_id
						ORDER BY rand()";

        if ($job_locations = $this->ClientAddress->query($loc_query)) {
            $locArr = array();
            $locPreArr = array();
            $locPreArr[0] = "Location";
            $locPreArr[1] = "Total Requests";
            $locPreArr[2] = "Total Submissions";

            array_push($locArr, $locPreArr);
            foreach ($job_locations as $locations) {
                $locSubArr = array();
                array_push($locSubArr, $locations['ca']['address']);
                array_push($locSubArr, $locations[0]['totalRequest']);
                array_push($locSubArr, $locations[0]['totalSubmission']);

                array_push($locArr, $locSubArr);
            }
//pr($locArr);exit;	
        }
//pr($currArr);exit;
        $popup = 0;
        $amount = 0;
        $getDate = $this->ClientSubscription->query("Select * from client_subscriptions where client_id = $client_id and transaction_status = 'pending'");
        if (!empty($getDate)) {
            $cdate = $getDate[0]['client_subscriptions']['created'];
            $amount = $getDate[0]['client_subscriptions']['amount_paid'];
            $current_date = date("Y-m-d");
            //$current_date = "2016-04-21";
            $date1 = date_create($current_date);
            $date2 = date_create($cdate);
            $diff = date_diff($date1, $date2);
            $days = $diff->format("%a");
            if ($days > 7) {
                $popup = 1;
            }
        }
        $this->set('amount', $amount);
        $this->set('popup', $popup);
        $this->set('submissionList', $job_submission_list);
        $this->set('locSubmission', $locArr);
        $this->set('requestList', $request_list);
        $this->set('currentJobChart', $currArr);
        $this->set('submissionChart', $submission_count);
        $this->set('displayMsgs', $display_msgs);
    }

    public function update_profile() {
        $this->layout = "client";
        $client_id = $this->Session->read('Auth.User.client_id');

        if ($this->request->is('post')) {
            $post = $this->request->data;
//pr($post);exit;
            $data['client_id'] = $post['client_id'];
            $data['company_name'] = $post['company_name'];
            $data['company_overview'] = $post['company_overview'];
            $data['company_policy'] = $post['editor1'];
            $data['mobile_number'] = $post['mobile_number'];
            $data['agency_name'] = $post['agency_name'];
            $data['client_address'] = $post['client_address'];
            $data['paypal_id'] = $post['paypal_id'];
            $data['working_hour_start'] = abs($post['working_hour_start']);
            $data['working_hour_end'] = abs($post['working_hour_end']);

            if ($post['working_hour_start'] > $post['working_hour_end'] || $post['working_hour_start'] == $post['working_hour_end']) {
                $message = 'Start working hour should be less than End Working hour.';
            } else if ($post['working_hour_start'] >= 24 || $post['working_hour_end'] >= 24 || $post['working_hour_end'] == '0') {
                $message = 'Start working hour and End Working hour should be between 0 and 23.';
            } else {

                if (isset($post['monday'])) {
                    $data['monday'] = $post['monday'];
                } else {
                    $data['monday'] = '0';
                }

                if (isset($post['tuesday'])) {
                    $data['tuesday'] = $post['tuesday'];
                } else {
                    $data['tuesday'] = '0';
                }

                if (isset($post['wednesday'])) {
                    $data['wednesday'] = $post['wednesday'];
                } else {
                    $data['wednesday'] = '0';
                }

                if (isset($post['thursday'])) {
                    $data['thursday'] = $post['thursday'];
                } else {
                    $data['thursday'] = '0';
                }

                if (isset($post['friday'])) {
                    $data['friday'] = $post['friday'];
                } else {
                    $data['friday'] = '0';
                }

                if (isset($post['saturday'])) {
                    $data['saturday'] = $post['saturday'];
                } else {
                    $data['saturday'] = '0';
                }

                if (isset($post['sunday'])) {
                    $data['sunday'] = $post['sunday'];
                } else {
                    $data['sunday'] = '0';
                }

//pr($_FILES);exit;
                if (!empty($_FILES['company_logo']['name'])) {
                    $imageExt = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                    $pathi = "";
                    $target_dir = WEBSITE_WEBROOT . 'media/img/';
//echo $target_dir;exit;
//$pathv = "";

                    $microtime = microtime();

                    $fileName = $_FILES['company_logo']["name"];
                    $extension = explode(".", $fileName);
                    $Path = "";

                    if (in_array($extension[1], $imageExt)) {
                        $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                        $target_dir2 = $target_dir . $Pathi;
                        move_uploaded_file($_FILES['company_logo']["tmp_name"], $target_dir2);
                    }/* else {
                      $Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                      $target_dir2 = $target_dir . $Pathv;
                      move_uploaded_file($val["tmp_name"], $target_dir2);
                      } */
                    $data['company_logo'] = $Pathi;
                }



                /* Company image update */


                if (!empty($_FILES['company_image']['name'])) {
//echo 'sdfsdfsdfds';exit;
                    $imageExt = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                    $pathi = "";
                    $target_dir = WEBSITE_WEBROOT . 'media/img/';
//echo $target_dir;exit;
//$pathv = "";

                    $microtime = microtime();

                    $fileName = $_FILES['company_image']["name"];
                    $extension = explode(".", $fileName);
                    $Path = "";

                    if (in_array($extension[1], $imageExt)) {
                        $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                        $target_dir2 = $target_dir . $Pathi;
                        move_uploaded_file($_FILES['company_image']["tmp_name"], $target_dir2);
                    }/* else {
                      $Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                      $target_dir2 = $target_dir . $Pathv;
                      move_uploaded_file($val["tmp_name"], $target_dir2);
                      } */
                    $comp_img['image_file_path'] = $Pathi;
                }



                $this->ClientUserInfo->id = $data['client_id'];
                if ($this->ClientUserInfo->save($data)) {
                    $comp_img['client_id'] = $data['client_id'];
                    if (isset($post['client_image_id']) && !empty($post['client_image_id'])) {
                        $comp_img['client_image_id'] = $post['client_image_id'];
                    } else {
                        $comp_img['client_image_id'] = 0;
                    }

                    $comp_img['is_default'] = 1;
//$this->ClientImage->client_image_id = $this->request->data['client_image_id'];
                    if ($this->ClientImage->save($comp_img)) {
                        $this->Session->setFlash('Client details updated successfully.', 'default', array('class' => 'successmsg'));
                        $this->redirect(array('controller' => 'clients', 'action' => 'update_profile'));
                        exit();
                    } else {
                        $this->Session->setFlash('Unable to update client data.', 'default', array('class' => 'successmsg'));
                        $this->redirect(array('controller' => 'clients', 'action' => 'update_profile'));
                        exit();
                    }
                } else {
                    $this->Session->setFlash('Unable to update client details.', 'default', array('class' => 'errormsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'update_profile'));
                    exit();
                }
            }
        }


        $singleclient = $this->ClientUserInfo->find('first', array('fields' => array('ClientUserInfo.*'), 'conditions' => array('client_id' => $client_id), 'recursive' => -1));
        $clientimage = $this->ClientImage->find('first', array('conditions' => array('ClientImage.client_id' => $client_id, 'ClientImage.is_default' => 1), 'recursive' => -1));

        $listing['ClientUserInfo'] = $singleclient['ClientUserInfo'];
        if (!empty($clientimage)) {
            $listing['ClientImage'] = $clientimage['ClientImage'];
        }


//pr($listing); exit;
        $this->set('listing', $listing);
        $this->set('message', $message);
    }

    public function job_post() {
        $client_id = $this->Session->read('Auth.User.client_id');

        $get_job_list = $this->Job->find('all', array('conditions' => array('Job.client_id' => $client_id, 'Job.is_blocked' => 0), 'order' => array('Job.created DESC')));

        $this->set('get_job_list', $get_job_list);
        $this->layout = "client";
    }

    function activate_inactive($job_id = null, $job_status = null) {
//echo $job_status;exit;
        if ($job_status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }

        $this->Job->id = $job_id; //exit;

        if ($this->Job->saveField('is_active', $status)) {
//echo "success"; exit;
            $this->Session->setFlash('Status changed successfully.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
            exit();
        } else {
//echo "failed"; exit;
            $this->Session->setFlash('Error ! status not changed.Please Try Again.', 'default', array('class' => 'errormsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
            exit();
        }
    }

    public function new_job_post() {

//error_reporting(E_ALL);

        $client_id = $this->Session->read('Auth.User.client_id');
        $company_name = $this->Session->read('Auth.User.company_name');


        //$survey_option = $this->Survey->find('all', array('fields' => array('Survey.survey_id', 'Survey.survey_name'), 'conditions' => array('Survey.client_id' => $client_id, 'Survey.is_active' => 1)));
        $survey_option = $this->Survey->query("SELECT Survey.survey_name,Survey.survey_id FROM surveys AS Survey LEFT JOIN client_user_infos AS ClientUserInfo ON Survey.client_id = ClientUserInfo.client_id "
                . "WHERE Survey.client_id = $client_id AND Survey.is_active = 1 Group By Survey.survey_name");

        $this->set('survey_option', $survey_option);


        $cat_option = $this->JobCategory->find('all', array('fields' => array('JobCategory.job_category_id', 'JobCategory.job_category_title'), 'conditions' => array('JobCategory.is_active' => 1)));
        $this->set('cat_option', $cat_option);


        $job_location = $this->ClientAddress->find('all', array('fields' => array('ClientAddress.client_address_id', 'ClientAddress.address'), 'conditions' => array('ClientAddress.client_id' => $client_id, 'ClientAddress.is_active' => 1)));
        $this->set('job_location', $job_location);

        $agent_rank = $this->AgentRank->find('all', array('fields' => array('AgentRank.rank', 'AgentRank.rank_title')));
        $this->set('agent_rank', $agent_rank);


        $coupon_data = $this->Coupon->find('all', array('fields' => array('Coupon.coupon_id', 'Coupon.coupon_name'), 'conditions' => array('Coupon.client_id' => $client_id)));
        $this->set('coupon_data', $coupon_data);

        if ($this->request->is('post')) {
            $post = $this->request->data;
//pr($post); exit;

            if ($post['time_span_from'] > $post['time_span_to'] || $post['time_span_from'] == $post['time_span_to']) {
                $message = 'Time span from should be less than Time span To.';
            } else if ($post['time_span_from'] >= 24 || $post['time_span_to'] >= 24 || $post['time_span_to'] == '0') {
                $message = 'Time span from and Time span To should be between 0 and 23.';
            } else {

                $survey_id = $post['survey_list'];

                $aryDataforsurvey = $this->Survey->find('all', array('fields' => array('Survey.*'), 'conditions' => array('Survey.survey_id' => $survey_id)));
                unset($aryDataforsurvey[0]['Survey']['survey_id']);
                $aryDataforsurvey[0]['Survey']['is_job'] = 1;
                $survey_save = $this->Survey->save($aryDataforsurvey[0]['Survey']);

                $new_survey_id = $survey_save['Survey']['survey_id'];

                $aryDataforquestions = $this->SurveyQuestion->find('all', array('conditions' => array('SurveyQuestion.survey_id' => $survey_id)));

                $new_job_post_data['client_id'] = $this->Session->read('Auth.User.client_id');
                $from_date_format = $post['date_from'];
                $to_date_format = $post['date_to'];
                $reward_id = $post['reward'];

                $from_date = new DateTime($from_date_format);
                $to_date = new DateTime($to_date_format);

                $new_job_post_data['job_start_date'] = $from_date->format('Y-m-d') . ' ' . $post['time_span_from'];
                $new_job_post_data['job_end_date'] = $to_date->format('Y-m-d') . ' ' . $post['time_span_to'];
                $new_job_post_data['min_agent_rank'] = $post['agent_rank'];
                $new_job_post_data['coupon_id'] = $post['survey_list'];
                $new_job_post_data['job_category_id'] = $post['job_cat'];

                if ($reward_id == 1) {
                    $new_job_post_data['job_compensation'] = str_replace('$', '', $post['cash_value']);
                    $new_job_post_data['coupon_id'] = 0;
                } else if ($reward_id == 2) {
                    $couponDetails['client_id'] = $client_id;
                    $couponDetails['coupon_price'] = $post['coupon_value'];
                    $couponDetails['is_active'] = 1;
                    $couponDetails['is_blocked'] = 0;
                    $couponDetails['created'] = date("Y-m-d h:i:s");
                    $this->Coupon->save($couponDetails);
                    $coupon_id = $this->Coupon->getLastInsertId();
                    $new_job_post_data['coupon_id'] = $coupon_id;
                    $new_job_post_data['job_compensation'] = 0;
                }

                $new_job_post_data['reward_type'] = $reward_id;
                $new_job_post_data['job_description'] = $post['job_overview'];
                $new_job_post_data['max_allowed_requests'] = abs($post['request_number']);
                $new_job_post_data['is_active'] = $post['job_status'];

                if (isset($post['test_status'])) {
                    $new_job_post_data['test_passed'] = '1';
                } else {
                    $new_job_post_data['test_passed'] = '0';
                }

                $new_job_post_data['survey_id'] = $new_survey_id;

//pr($new_job_post_data); exit;
                if ($data7 = $this->Job->save($new_job_post_data)) {
                    $job_id_arr = $this->Job->find('first', array('fields' => array('Job.job_id'), 'order' => array('Job.job_id DESC'), 'recursive' => -1));
                    $job_id = $job_id_arr['Job']['job_id'];
                    $this->Coupon->query("update coupons set job_id = $job_id where coupon_id = $coupon_id");
                    $job_address = $post['location'];
                    foreach ($job_address as $key => $address) {
                        $job_loc[$key]['job_id'] = $job_id;
                        $job_loc[$key]['client_address_id'] = $address;
                    }

                    $data6 = $this->JobLocation->saveAll($job_loc);
                }

                foreach ($aryDataforquestions as $aryDataforquestion) {
                    if ($aryDataforquestion['SurveyQuestion']['survey_question_type'] == 'C' || $aryDataforquestion['SurveyQuestion']['survey_question_type'] == 'R') {
                        unset($data3);
                        unset($survey_question_id_arr);
                        unset($survey_question_id);
                        unset($aryDataforquestion['SurveyQuestion']['survey_question_id']);
                        unset($aryDataforquestion['SurveyQuestion']['survey_id']);
                        $aryDataforquestion['SurveyQuestion']['survey_question_id'] = 0;
                        $aryDataforquestion['SurveyQuestion']['survey_id'] = $new_survey_id;

                        $this->SurveyQuestion->save($aryDataforquestion['SurveyQuestion']);

                        $survey_question_id_arr = $this->SurveyQuestion->find('first', array('order' => array('SurveyQuestion.survey_question_id DESC'), 'recursive' => -1));

                        $survey_question_id = $survey_question_id_arr['SurveyQuestion']['survey_question_id'];

                        $data3 = array();
                        foreach ($aryDataforquestion['SurveyQuestionOption'] as $aryDataforoption) {
                            unset($aryDataforoption['survey_question_id']);
                            unset($aryDataforoption['survey_option_id']);
                            $aryDataforoption['survey_question_id'] = $survey_question_id;
                            $data3[] = $aryDataforoption;
                        }
                        $data5[] = $this->SurveyQuestionOption->saveAll($data3);
                    } else {
                        unset($aryDataforquestion['SurveyQuestion']['survey_question_id']);
                        unset($aryDataforquestion['SurveyQuestion']['survey_id']);
                        $aryDataforquestion['SurveyQuestion']['survey_id'] = $new_survey_id;
                        $data1[] = $aryDataforquestion;
                    }
                }

                if (isset($data1) && !empty($data1)) {
                    $survey_questions = $this->SurveyQuestion->saveAll($data1);
                }

                if (isset($survey_question_id) && !empty($survey_question_id) || isset($data1) && !empty($data1) || isset($data5) && !empty($data5) || isset($data6) && !empty($data6) || isset($data7) && !empty($data7)) {

                    $from = array('admin@emissary.com' => 'EMISSARY');
                    $to = $this->Session->read('Auth.User.email_address');
                    $to1 = 'admin@emissary.com';
                    $subject = 'New Job';

                    $msg = 'New job has been posted successfully.';
                    $template = '';

                    $mail1 = $this->send_email($from, $to, $subject, $msg, $template);

                    $mail2 = $this->send_email($from, $to1, $subject, $msg, $template);

                    $adminDashMsg = $company_name . "just posted a new Job";
                    $this->saveAdminMsg($adminDashMsg);

                    $this->Session->setFlash('Job added successfully.', 'default', array('class' => 'successmsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
                    exit;
                } else {
                    $this->Session->setFlash('Error ! Unable to add Job.please try again.', 'default', array('class' => 'successmsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
                    exit;
                }
            }
        }
        $this->layout = "client";

        $this->set('message', $message);
    }

    public function send_email($from, $to, $subject, $message, $template) {

        $this->Email = new CakeEmail();
        $this->Email->config('smtp');
        $this->Email->to($to);
        $this->Email->subject($subject);
        $this->Email->emailFormat('html');

        $this->Email->from($from);
        $this->Email->template($template);

        ob_start();
        $this->Email->send($message);
        ob_end_clean();
    }

    public function surveyList() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $get_survey_list = $this->Survey->find('all', array('conditions' => array('Survey.client_id' => $client_id, 'is_job' => 0), 'order' => array('Survey.created DESC')));
//// $request_list = $this->AgentJob->find('all', array('conditions' => array('AgentJob.client_id' => $client_id, 'AgentJob.completed_by_agent' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.disapproved_by_client' => 0, 'AgentJob.approved_by_client' => 0),'order' => array('AgentJob.created DESC')));
//pr($get_survey_list); exit;
//echo $survey_que_no; exit;
        foreach ($get_survey_list as $key => $survey_list) {
            $survey_id = $survey_list['Survey']['survey_id'];
            $category_count = $this->SurveyQuestion->find('count', array('conditions' => array('SurveyQuestion.survey_id' => $survey_id), 'group' => array('SurveyQuestion.survey_category_id')));
            $survey_count = $this->SurveyQuestion->find('count', array('conditions' => array('SurveyQuestion.survey_id' => $survey_id)));

            if ($job_details = $this->Job->find('first', array('fields' => array('job_id', 'is_locked'), 'conditions' => array('Job.survey_id' => $survey_id), 'recurvise' => -1))) {
                $get_survey_list[$key]['Survey']['job_id'] = $job_details['Job']['job_id'];
                $get_survey_list[$key]['Survey']['is_locked'] = $job_details['Job']['is_locked'];
            }
            $get_survey_list[$key]['Survey']['QuestionCount'] = $survey_count;
            $get_survey_list[$key]['Survey']['CategoryCount'] = $category_count;
        }
//pr($get_survey_list);
//exit;
        $this->set('get_survey_list', $get_survey_list);
        $this->layout = "client";
    }

    function survey_status($survey_id = null, $survey_status = null) {
//echo $survey_status;exit;
        if ($survey_status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }




        $this->Survey->id = $survey_id; //exit;

        if ($this->Survey->saveField('is_active', $status)) {
//echo "success"; exit;
            $this->Session->setFlash('Status changed successfully.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
            exit();
        } else {
//echo "failed"; exit;
            $this->Session->setFlash('Error ! status not changed.Please Try Again.', 'default', array('class' => 'errormsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
            exit();
        }
    }

    public function edit_job($edit_job_id = null) {

//error_reporting(E_ALL);

        $client_id = $this->Session->read('Auth.User.client_id');

        if ($this->request->is('post')) {

            $post = $this->request->data;

//pr($post); exit;

            if ($post['time_span_from'] > $post['time_span_to'] || $post['time_span_from'] == $post['time_span_to']) {

                $this->Session->setFlash('Time span from should be less than Time span To.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'edit_job/' . $post['job_ref']));
                exit();
            } else if ($post['time_span_from'] >= 24 || $post['time_span_to'] >= 24 || $post['time_span_to'] == '0') {

                $this->Session->setFlash('Time span from and Time span To should be between 0 and 23.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'edit_job/' . $post['job_ref']));
                exit();
            } else {

                $from_date_format = $post['date_from'];
                $to_date_format = $post['date_to'];
                $reward_id = $post['reward'];
                $from_date = new DateTime($from_date_format);
                $to_date = new DateTime($to_date_format);
                $job_id = $post['job_ref'];
                $update_job_data['job_start_date'] = $from_date->format('Y-m-d') . ' ' . $post['time_span_from'];
                $update_job_data['job_end_date'] = $to_date->format('Y-m-d') . ' ' . $post['time_span_to'];
                $update_job_data['min_agent_rank'] = $post['agent_rank'];
                $update_job_data['survey_id'] = $post['survey_list'];
                $update_job_data['job_category_id'] = $post['job_cat'];

                if ($reward_id == 1) {
                    $update_job_data['job_compensation'] = str_replace('$', '', $post['cash_value']);
                    $update_job_data['coupon_id'] = 0;
                } else if ($reward_id == 2) {
                    $update_job_data['coupon_id'] = $post['coupon_value'];
                    $update_job_data['job_compensation'] = 0;
                }
                $update_job_data['reward_type'] = $reward_id;
                $update_job_data['job_description'] = $post['job_overview'];
                $update_job_data['max_allowed_requests'] = $post['request_number'];
                $update_job_data['is_active'] = $post['job_status'];
                $update_job_data['test_passed'] = $post['test_status'];
                $update_job_data['job_id'] = $post['job_ref'];

                if ($this->Job->save($update_job_data)) {
//$this->JobLocation->id = $job_id;
                    if ($this->JobLocation->deleteAll(array('JobLocation.job_id' => $post['job_ref']))) {
                        $job_address = $post['location'];
                        foreach ($job_address as $key => $address) {
                            $job_loc[$key]['job_id'] = $job_id;
                            $job_loc[$key]['client_address_id'] = $address;
                        }

                        if ($insert_location = $this->JobLocation->saveAll($job_loc)) {
                            $this->Session->setFlash('Data updated successfully.', 'default', array('class' => 'successmsg'));
                            $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
                            exit();
                        } else {

                            $this->Session->setFlash('Please check your data.', 'default', array('class' => 'successmsg'));
                            $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
                            exit();
                        }
                    } else {
                        $this->Session->setFlash('Unable to update data.', 'default', array('class' => 'successmsg'));
                        $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
                        exit();
                    }
                } else {

                    $this->Session->setFlash('Please check your data to edit.', 'default', array('class' => 'successmsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
                    exit();
                }
            }
        }

        $get_job_data = $this->Job->find('first', array('conditions' => array('Job.job_id' => $edit_job_id), 'recursive' => -1));

        $is_locked = $get_job_data['Job']['is_locked'];

        if ($is_locked == '1') {
            $this->Session->setFlash('Job is locked.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'job_post'));
            exit();
        }

        $date_time = $get_job_data['Job']['job_start_date'];
        $timestamp = strtotime($date_time);
        $edit_job_data['start_date'] = date('n/j/Y', $timestamp);
        $edit_job_data['start_time'] = date('H', $timestamp);


        $end_date = $get_job_data['Job']['job_end_date'];
        $end_datetime = strtotime($end_date);
        $edit_job_data['end_date'] = date('n/j/Y', $end_datetime);
        $edit_job_data['end_time'] = date('H', $end_datetime);


        $client_address_id_arr = $this->JobLocation->find('all', array('fields' => ('JobLocation.client_address_id'), 'conditions' => array('JobLocation.job_id' => $edit_job_id), 'recursive' => -1));
//pr($client_address_id);exit;
        $cat_option = $this->JobCategory->find('all', array('fields' => array('JobCategory.job_category_id', 'JobCategory.job_category_title'), 'conditions' => array('JobCategory.is_active' => 1)));
        $this->set('cat_option', $cat_option);

//$survey_option = $this->Survey->find('all', array('fields' => array('Survey.survey_id', 'Survey.survey_name', 'Survey.survey_category_id'), 'conditions' => array('Survey.client_id' => $client_id, 'Survey.is_active' => 1)));
        $survey_option = $this->Survey->find('all', array('fields' => array('Survey.survey_id', 'Survey.survey_name'), 'conditions' => array('Survey.client_id' => $client_id, 'Survey.is_active' => 1)));
        $this->set('survey_option', $survey_option);

        $job_location = $this->ClientAddress->find('all', array('fields' => array('ClientAddress.address'), 'conditions' => array('ClientAddress.client_id' => $client_id, 'ClientAddress.is_active' => 1)));
        $this->set('job_location', $job_location);

        $agent_rank = $this->AgentRank->find('all', array('fields' => array('AgentRank.rank', 'AgentRank.rank_title', 'AgentRank.agent_rank_id')));
        $this->set('agent_rank', $agent_rank);

        $coupon_data = $this->Coupon->find('all', array('fields' => array('Coupon.coupon_id', 'Coupon.coupon_name'), 'conditions' => array('Coupon.client_id' => $client_id)));
        $this->set('coupon_data', $coupon_data);

//$edit_job_data['job_location'] = $get_job_data['Job']['job_location'];
        $edit_job_data['job_reward'] = $get_job_data['Job']['reward_type'];

        if ($edit_job_data['job_reward'] == 1) {
            $edit_job_data['job_compensation'] = $get_job_data['Job']['job_compensation'];
            $edit_job_data['coupon_id'] = 0;
        } else if ($edit_job_data['job_reward'] == 2) {
//$edit_job_data['job_coupon_id']  = $get_job_data['Job']['coupon_id'];
            $edit_job_data['coupon_id'] = $get_job_data['Job']['coupon_id'];
            $edit_job_data['job_compensation'] = 0;
        }

        foreach ($client_address_id_arr as $loc_key => $loc_value) {
            $client_add_ids[$loc_key] = $loc_value['JobLocation']['client_address_id'];
        }
        $edit_job_data['job_overview'] = $get_job_data['Job']['job_description'];
        $edit_job_data['job_id'] = $edit_job_id;
        $edit_job_data['max_allowed_requests'] = $get_job_data['Job']['max_allowed_requests'];
        $edit_job_data['min_agent_rank'] = $get_job_data['Job']['min_agent_rank'];
        $edit_job_data['survey_id'] = $get_job_data['Job']['survey_id'];
        $edit_job_data['job_cat_id'] = $get_job_data['Job']['job_category_id'];
        $edit_job_data['test_status'] = $get_job_data['Job']['test_passed'];
        $edit_job_data['job_status'] = $get_job_data['Job']['is_active'];
        $this->set('edit_job_data', $edit_job_data);

        if (isset($client_add_ids) && !empty($client_add_ids)) {
            $this->set('client_add_ids', $client_add_ids);
        } else {
            $this->set('client_add_ids', array());
        }

        $this->set('message', $message);
//pr($edit_job_data);exit;
        $this->layout = "client";
    }

    public function add_survey() {

        $client_id = $this->Session->read('Auth.User.client_id');
        if ($this->request->is('post')) {

            $post = $this->request->data;

            $survey_data['client_id'] = $client_id;
            $survey_data['survey_name'] = $post['survey_name'];
            $survey_data['survey_overview'] = trim($post['editor1']);

            if (isset($post['gps_check'])) {
                $survey_data['gps_check'] = '1';
            } else {
                $survey_data['gps_check'] = '0';
            }

            if (isset($post['qr_check'])) {
                $survey_data['qr_check'] = '1';
            } else {
                $survey_data['qr_check'] = '0';
            }

            $survey_data['min_threshold'] = $post['min_threshold'];
            $survey_data['is_active'] = $post['is_active'];
//pr($survey_data); exit;
            if ($this->Survey->save($survey_data)) {
                $this->Session->setFlash('Survey added successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
                exit();
            } else {
                $this->Session->setFlash('Unable to add Survey.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
                exit();
            }
        }

        $this->layout = "client";
    }

    public function edit_survey($survey_id = null) {

        $client_id = $this->Session->read('Auth.User.client_id');
        if ($this->request->is('post')) {

            $post = $this->request->data;

            $survey_data['client_id'] = $client_id;
            $survey_data['survey_id'] = $post['survey_id'];
            $survey_data['survey_name'] = $post['survey_name'];
            $survey_data['survey_overview'] = trim($post['editor1']);

            if (isset($post['gps_check'])) {
                $survey_data['gps_check'] = '1';
            } else {
                $survey_data['gps_check'] = '0';
            }

            if (isset($post['qr_check'])) {
                $survey_data['qr_check'] = '1';
            } else {
                $survey_data['qr_check'] = '0';
            }

            $survey_data['min_threshold'] = $post['min_threshold'];
            $survey_data['is_active'] = $post['is_active'];
//pr($survey_data); exit;
            if ($this->Survey->save($survey_data)) {
                $this->Session->setFlash('Survey list has been updated successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
                exit();
            } else {
                $this->Session->setFlash('Unable to update Survey.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
                exit();
            }
        }

//$client_id = $this->Session->read('Auth.User.client_id');
        $get_survey_list = $this->Survey->find('all', array('conditions' => array('Survey.client_id' => $client_id, 'Survey.survey_id' => $survey_id)));

        $is_locked = $get_survey_list[0]['Survey']['is_locked'];

        if ($is_locked == '1') {
            $this->Session->setFlash('Survey is locked.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
            exit();
        }

//pr($get_survey_list); exit;

        $this->set('single', $get_survey_list[0]);

        $this->layout = "client";
    }

    public function accountSettings() {


        $client_id = $this->Session->read('Auth.User.client_id');
        $getoldpassword = $this->ClientUserInfo->field('password', array('ClientUserInfo.client_id' => $client_id));
        $receive_email = $this->ClientUserInfo->field('receive_email', array('ClientUserInfo.client_id' => $client_id));
        $alerts = $this->ClientAlerts->query("Select * from client_alerts where client_id = $client_id");
//pr($getoldpassword);exit;

        if ($this->request->is('post')) {
            $post = $this->request->data;
            $old = AuthComponent::password($post['oldPassword']);
            $resetpassword['password'] = AuthComponent::password($post['newPassword']);

            if ($getoldpassword == $old) {
                $this->ClientUserInfo->id = $client_id;
                if ($this->ClientUserInfo->save($resetpassword)) {

                    if ($receive_email == 1) {
                        $from = array('admin@emissary.com' => 'EMISSARY');
                        $to = $this->Session->read('Auth.User.email_address');
                        $subject = 'Password Change';
                        $msg = 'Your password has been changed successfully.';
                        $template = '';
                        $mail = $this->send_email($from, $to, $subject, $msg, $template);
                    }

                    $this->Session->setFlash('Password changed successfully.', 'default', array('class' => 'successmsg'));
                } else {
                    $this->Session->setFlash('Unable to change password.Try again.', 'default', array('class' => 'successmsg'));
                }
            } else {
                $this->Session->setFlash('Enter proper data.', 'default', array('class' => 'successmsg'));
            }
        }

        $this->set('alerts', $alerts);
        $this->set('receive_email', $receive_email);
        $this->layout = "client";
    }

    public function requests() {

        $client_id = $this->Session->read('Auth.User.client_id');
        $request_list = $this->AgentJob->find('all', array('conditions' => array('AgentJob.client_id' => $client_id, 'AgentJob.completed_by_agent' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.disapproved_by_client' => 0, 'AgentJob.approved_by_client' => 0), 'order' => array('AgentJob.created DESC')));

        foreach ($request_list as $key => $list) {
            $agent_rank_arr = $this->AgentRank->find('first', array('conditions' => array('AgentRank.rank' => $list['AgentUserInfo']['agent_rank']), 'recursive' => -1));
            $agent_rank = $agent_rank_arr['AgentRank']['rank_title'];
            $request_list[$key]['AgentUserInfo']['agent_rank_title'] = $agent_rank;
        }
//pr($request_list);exit;


        $this->set('request_list', $request_list);
        $this->layout = "client";
    }

    public function change_request_status($agent_job_id, $activity) {
        $this->AgentJob->id = $agent_job_id;
        $data['agent_job_id'] = $agent_job_id;
        if ($activity == 'A') {
            $data['accepted_by_client'] = 1;
            $data['declined_by_client'] = 0;
            if ($this->AgentJob->save($data)) {
                $this->Session->setFlash('Status changed successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'requests'));
                exit();
            }
        } elseif ($activity == 'D') {
            $data['accepted_by_client'] = 0;
            $data['declined_by_client'] = 1;
            if ($this->AgentJob->save($data)) {
                $this->Session->setFlash('Status changed successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'requests'));
                exit();
            }
        } elseif ($activity == 'R') {
            $data['accepted_by_client'] = 0;
            $data['declined_by_client'] = 0;
            if ($this->AgentJob->save($data)) {
                $this->Session->setFlash('Status changed successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'requests'));
                exit();
            }
        }
    }

    public function receive_email() {
        $data['client_id'] = $this->Session->read('Auth.User.client_id');

        $data['receive_email'] = $_POST['receive_email'];

        if ($this->ClientUserInfo->save($data)) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    public function job_submission() {

        $client_id = $this->Session->read('Auth.User.client_id');
        $job_submission_list = $this->AgentJob->find('all', array('conditions' => array('AgentJob.client_id' => $client_id, 'AgentJob.accepted_by_client' => 1, 'AgentJob.declined_by_client' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.completed_by_agent' => 1), 'order' => array('AgentJob.created DESC')));
// $request_list = $this->AgentJob->find('all', array('conditions' => array('AgentJob.client_id' => $client_id, 'AgentJob.completed_by_agent' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.disapproved_by_client' => 0, 'AgentJob.approved_by_client' => 0),'order' => array('AgentJob.created DESC')));

        $this->set('job_submission_list', $job_submission_list);
        $this->layout = 'client';
    }

    public function view_answers($agent_job_id = null) {

        $client_id = $this->Session->read('Auth.User.client_id');

        $survey_answers = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'group' => 'AgentSurveyAnswer.survey_question_id', 'conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id), 'recursive' => 1));

        foreach ($survey_answers as $survey_answer) {
            $survey_question_id = $survey_answer['AgentSurveyAnswer']['survey_question_id'];

            $survey_question_ids[] = $survey_question_id;

            $question_lists[$survey_question_id] = $this->SurveyQuestion->find('all', array('fields' => array(), 'conditions' => array('SurveyQuestion.survey_question_id' => $survey_question_id), 'recursive' => 1));

            $data[$survey_question_id] = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id, 'AgentSurveyAnswer.survey_question_id' => $survey_question_id), 'recursive' => 1));
        }

//pr($survey_answers); exit;
        $this->set('survey_question_ids', $survey_question_ids);
        $this->set('question_lists', $question_lists);
        $this->set('data', $data);
        $this->layout = 'client';
    }

    /* public function view_answer($agent_job_id = null, $agent_id = null) {

      $client_id = $this->Session->read('Auth.User.client_id');

      $agent = $this->AgentUserInfo->find('all', array('fields' => array('AgentUserInfo.*'), 'conditions' => array('AgentUserInfo.agent_id' => $agent_id), 'recursive' => 1));

      $job_submission = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.agent_job_id' => $agent_job_id), 'recursive' => 1));

      $survey_answers = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'group' => 'AgentSurveyAnswer.survey_question_id', 'conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id), 'recursive' => 1));

      foreach ($survey_answers as $survey_answer) {
      $survey_question_id = $survey_answer['AgentSurveyAnswer']['survey_question_id'];

      $survey_question_ids[] = $survey_question_id;

      $question_lists[$survey_question_id] = $this->SurveyQuestion->find('all', array('fields' => array(), 'conditions' => array('SurveyQuestion.survey_question_id' => $survey_question_id), 'recursive' => 1));

      $data[$survey_question_id] = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id, 'AgentSurveyAnswer.survey_question_id' => $survey_question_id), 'recursive' => 1));
      }

      //pr($job_submission); exit;

      $this->set('agent', $agent);
      $this->set('job_submission', $job_submission);
      $this->set('survey_question_ids', $survey_question_ids);
      $this->set('question_lists', $question_lists);
      $this->set('data', $data);
      $this->layout = 'client';
      } */

    public function view_answer($agent_job_id = null) {

        $client_id = $this->Session->read('Auth.User.client_id');

        $job_submission = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_job_id' => $agent_job_id), 'recursive' => 1));

        $agent_id = $job_submission[0]['AgentJob']['agent_id'];

        $agent = $this->AgentUserInfo->find('all', array('fields' => array('AgentUserInfo.*'), 'conditions' => array('AgentUserInfo.agent_id' => $agent_id), 'recursive' => -1));

//pr($agent);exit;

        $survey_categories = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'group' => 'AgentSurveyAnswer.survey_category_id', 'conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id), 'recursive' => -1));

//pr($survey_categories); exit;

        foreach ($survey_categories as $survey_category) {

            $survey_category_id = $survey_category['AgentSurveyAnswer']['survey_category_id'];

            $survey_category_ids[] = $survey_category_id;

            $category_list_singles[$survey_category_id] = $this->SurveyCategory->find('all', array('fields' => array(), 'conditions' => array('SurveyCategory.survey_category_id' => $survey_category_id), 'recursive' => -1));

            $survey_questions[$survey_category_id] = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'group' => 'AgentSurveyAnswer.survey_question_id', 'conditions' => array('AgentSurveyAnswer.survey_category_id' => $survey_category_id, 'AgentSurveyAnswer.agent_id' => $agent_id), 'recursive' => -1));

            foreach ($survey_questions[$survey_category_id] as $survey_question) {
                $survey_question_id = $survey_question['AgentSurveyAnswer']['survey_question_id'];

//$survey_questions_all[][$survey_category_id] = $this->SurveyQuestion->find('all', array('fields' => array(),'conditions' => array('SurveyQuestion.survey_question_id' => $survey_question_id), 'recursive' => 1));

                $survey_question = $this->SurveyQuestion->find('all', array('fields' => array(), 'conditions' => array('SurveyQuestion.survey_question_id' => $survey_question_id), 'recursive' => 1));

                $survey_questions_all[$survey_category_id][] = $survey_question[0];

                $data[][$survey_question_id] = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'conditions' => array('AgentSurveyAnswer.survey_question_id' => $survey_question_id), 'recursive' => 1));
            }
        }

        ksort($survey_questions_all);
//pr($data);
//pr($data); exit;
//exit;
        $this->set('agent', $agent);
        $this->set('job_submission', $job_submission);
        $this->set('survey_category_ids', $survey_category_ids);
        $this->set('category_list_singles', $category_list_singles);
        $this->set('data', $data);
        $this->set('survey_questions_alls', $survey_questions_all);
        $this->layout = 'client';
    }

    public function change_aproval() {
        $data['agent_job_id'] = $_POST['agent_job_id'];
        $data1['agent_id'] = $_POST['agent_id'];
        $data1['rating_description'] = $_POST['rating_description'];
        $data1['rating_points'] = $_POST['rating_points'];
        $data1['client_id'] = $this->Session->read('Auth.User.client_id');
        $company_name = $this->Session->read('Auth.User.company_name');
        $type = $_POST['type'];

        if ($type == '1') {
            $data['approved_by_client'] = '1';
            if ($this->AgentJob->save($data)) {

                $agentInfo = $this->AgentUserInfo->find('first', array('conditions' => array('AgentUserInfo.agent_id' => $_POST['agent_id']), 'recursive' => -1));

                $data3['points_earned'] = $agentInfo['AgentUserInfo']['points_earned'] + 10;
                $data3['agent_id'] = $_POST['agent_id'];
                $this->AgentUserInfo->save($data3);

                $this->AgentRating->save($data1);

                $adminDashMsg = $company_name . "has approved a new submission.";
                $this->saveAdminMsg($adminDashMsg);

                echo "1";
                exit;
            } else {
                echo "0";
                exit;
            }
        } else {
            $data['disapproved_by_client'] = '1';
            if ($this->AgentJob->save($data)) {

                $this->AgentRating->save($data1);

                echo "1";
                exit;
            } else {
                echo "0";
                exit;
            }
        }
    }

    public function agent_survey() {

        $client_id = $this->Session->read('Auth.User.client_id');
        $job_submission_list = $this->AgentJob->find('all', ['conditions' => ['AgentJob.client_id' => $client_id, 'AgentJob.is_completed_by_agent' => 1]]);

        $this->set('job_submission_list', $job_submission_list);
        $this->layout = 'client';
    }

    public function coupon_list() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $coupon_list = $this->Coupon->find('all', array('conditions' => array('Coupon.client_id' => $client_id)));
//pr($coupon_list); exit;

        $this->set('coupon_list', $coupon_list);

        $this->layout = 'client';
    }

    public function change_coupon_status($coupon_id = null, $coupon_status = null) {
        if ($coupon_status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }

        $this->Coupon->id = $coupon_id; //exit;

        if ($this->Coupon->saveField('is_active', $status)) {
//echo "success"; exit;
            $this->Session->setFlash('Status changed successfully.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'coupon_list'));
            exit();
        } else {
//echo "failed"; exit;
            $this->Session->setFlash('Error ! status not changed.Please Try Again.', 'default', array('class' => 'errormsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'coupon_list'));
            exit();
        }
    }

    public function add_coupons() {

        $client_id = $this->Session->read('Auth.User.client_id');
        if ($this->request->is('post')) {
            $post = $this->request->data;
//pr($post);exit;
            $coupon_data['client_id'] = $client_id;
            $coupon_data['coupon_name'] = $post['coupon_name'];
//$coupon_data['coupon_image'] = $this->request->data['coupon_image'];
            $coupon_data['coupon_description'] = $post['coupon_des'];
            $coupon_data['coupon_quantity'] = $post['quantity'];
            $coupon_data['coupon_expiration_date'] = date('Y-m-d H:i:s', strtotime($post['exp_date']));
            $coupon_data['is_active'] = 1;
//pr($data);exit;

            /* coupon image */

            if (!empty($_FILES['coupon_image']['name'])) {
                $imageExt = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                $pathi = "";
                $target_dir = WEBSITE_WEBROOT . 'media/img/';
//echo $target_dir;exit;
//$pathv = "";

                $microtime = microtime();

                $fileName = $_FILES['coupon_image']["name"];
                $extension = explode(".", $fileName);
                $Path = "";

                if (in_array($extension[1], $imageExt)) {
                    $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                    $target_dir2 = $target_dir . $Pathi;
                    move_uploaded_file($_FILES['coupon_image']["tmp_name"], $target_dir2);
                }/* else {
                  $Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                  $target_dir2 = $target_dir . $Pathv;
                  move_uploaded_file($val["tmp_name"], $target_dir2);
                  } */
                $coupon_data['coupon_image'] = $Pathi;
            }

            if (!empty($_FILES['qr_image']['name'])) {
                $imageExt = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                $pathi = "";
                $target_dir = WEBSITE_WEBROOT . 'media/img/';
//echo $target_dir;exit;
//$pathv = "";

                $microtime = microtime();

                $fileName = $_FILES['qr_image']["name"];
                $extension = explode(".", $fileName);
                $Path = "";

                if (in_array($extension[1], $imageExt)) {
                    $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                    $target_dir2 = $target_dir . $Pathi;
                    move_uploaded_file($_FILES['coupon_image']["tmp_name"], $target_dir2);
                }/* else {
                  $Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                  $target_dir2 = $target_dir . $Pathv;
                  move_uploaded_file($val["tmp_name"], $target_dir2);
                  } */
                $coupon_data['coupon_qr_code_image'] = $Pathi;
            }
            if ($this->Coupon->save($coupon_data)) {
                $this->Session->setFlash('Your coupons has been added successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'coupon_list'));
                exit();
            } else {
                $this->Session->setFlash('Coupon cann\'t be added.', 'default', array('class' => 'errormsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'coupon_list'));
                exit();
            }
        }


        $this->layout = 'client';
    }

    function question_add() {
        $client_id = $this->Session->read('Auth.User.client_id');
        if ($this->request->is('post')) {
            $post = $this->request->data;
//pr($post);exit;			
//$data['ip_address'] = $this->request->clientIp();
            if ($post['pre_question'] == 1) {
                $count = $post['question_count'];

                $data5 = array();

                $data6 = array();

                for ($limit = 1; $limit < $count; $limit++) {
                    if ($post['survey_question_type_' . $limit] == 'C') {
                        unset($data1);
                        unset($data3);
                        unset($survey_question_id_arr);
                        unset($survey_question_id);

                        $data1 = array();
                        $data3 = array();
                        $survey_question_id_arr = array();
                        $survey_question_id = 0;

                        $data1['survey_question_id'] = 0;
                        $data1['survey_id'] = $post['survey_id_' . $limit];
                        $data1['is_default'] = '0';
                        $data1['survey_category_id'] = $post['survey_category_id_' . $limit];
                        $data1['survey_question_type'] = $post['survey_question_type_' . $limit];
                        $data1['survey_question_description'] = $post['survey_question_description_' . $limit];
                        $data1['survey_question_marks'] = $post['survey_question_marks_' . $limit];

                        $data1['is_active'] = '1';

                        if ($this->SurveyQuestion->save($data1)) {

                            $survey_question_id_arr = $this->SurveyQuestion->find('first', array('order' => array('SurveyQuestion.survey_question_id DESC'), 'recursive' => -1));

                            $survey_question_id = $survey_question_id_arr['SurveyQuestion']['survey_question_id'];
                        }
                        for ($i = 1; $i <= 4; $i++) {
                            $data3[$i]['survey_question_id'] = $survey_question_id;
                            $data3[$i]['answer_text'] = $post['option_' . $limit . '_' . $i];

//array_push($data4,$data3);
                        }
                        $data5[$limit] = $this->SurveyQuestionOption->saveAll($data3);
                    } else if ($post['survey_question_type_' . $limit] == 'R') {

                        unset($data11);
                        unset($data33);
                        unset($survey_question_id_arr);
                        unset($survey_question_id);

                        $data11 = array();
                        $data33 = array();
                        $survey_question_id_arr = array();
                        $survey_question_id = 0;

                        $data11['survey_question_id'] = 0;
                        $data11['survey_id'] = $post['survey_id_' . $limit];
                        $data11['is_default'] = '0';
                        $data11['survey_category_id'] = $post['survey_category_id_' . $limit];
                        $data11['survey_question_type'] = $post['survey_question_type_' . $limit];
                        $data11['survey_question_description'] = $post['survey_question_description_' . $limit];
                        $data11['survey_question_marks'] = $post['survey_question_marks_' . $limit];

                        $data11['is_active'] = '1';

                        $option_count = $post['questionr' . $limit . '_count'];

                        if ($this->SurveyQuestion->save($data11)) {

                            $survey_question_id_arr = $this->SurveyQuestion->find('first', array('order' => array('SurveyQuestion.survey_question_id DESC'), 'recursive' => -1));

                            $survey_question_id = $survey_question_id_arr['SurveyQuestion']['survey_question_id'];
                        }
                        for ($i = 1; $i < $option_count; $i++) {
                            $data33[$i]['survey_question_id'] = $survey_question_id;
                            $data33[$i]['answer_text'] = $post['optionr_' . $limit . '_' . $i];

                            //array_push($data4,$data3);
                        }
                        $data55[$limit] = $this->SurveyQuestionOption->saveAll($data33);
                    } else {
                        $data2[$limit]['is_default'] = '0';
                        $data2[$limit]['survey_id'] = $post['survey_id_' . $limit];
                        $data2[$limit]['survey_category_id'] = $post['survey_category_id_' . $limit];
                        $data2[$limit]['survey_question_type'] = $post['survey_question_type_' . $limit];
                        $data2[$limit]['survey_question_description'] = $post['survey_question_description_' . $limit];
                        $data2[$limit]['survey_question_marks'] = $post['survey_question_marks_' . $limit];

                        $data2[$limit]['is_active'] = '1';
                    }
                }

//pr($data2);exit;

                if (!empty($data2)) {
                    $data6 = $this->SurveyQuestion->saveAll($data2);
                }

                if (isset($survey_question_id) && !empty($survey_question_id) || isset($data5) && !empty($data5) || isset($data6) && !empty($data6) || isset($data55) && !empty($data55)) {
                    $this->Session->setFlash('Question added successfully.', 'default', array('class' => 'successmsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
                } else {
                    $this->Session->setFlash('Error ! Unable to add Question.please try again.', 'default', array('class' => 'successmsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
                }
            } else if ($post['pre_question'] == 2) {

                $count = $post['question_count'];

                $data5 = array();

                $data6 = array();

                for ($limit = 1; $limit < $count; $limit++) {
                    if ($post['survey_question_type_' . $limit] == 'C' && isset($post['question_count_' . $limit])) {
                        unset($data1);
                        unset($data3);
                        unset($survey_question_id_arr);
                        unset($survey_question_id);

                        $data1 = array();
                        $data3 = array();
                        $survey_question_id_arr = array();
                        $survey_question_id = 0;

                        $data1['survey_question_id'] = 0;
                        $data1['survey_id'] = $post['survey_id_' . $limit];
                        $data1['is_default'] = '0';
                        $data1['survey_category_id'] = $post['survey_category_id_' . $limit];
                        $data1['survey_question_type'] = $post['survey_question_type_' . $limit];
                        $data1['survey_question_description'] = $post['survey_question_description_' . $limit];
                        $data1['survey_question_marks'] = $post['survey_question_marks_' . $limit];

                        $data1['is_active'] = '1';

                        if ($this->SurveyQuestion->save($data1)) {

                            $survey_question_id_arr = $this->SurveyQuestion->find('first', array('order' => array('SurveyQuestion.survey_question_id DESC'), 'recursive' => -1));

                            $survey_question_id = $survey_question_id_arr['SurveyQuestion']['survey_question_id'];
                        }
                        for ($i = 1; $i <= 4; $i++) {
                            $data3[$i]['survey_question_id'] = $survey_question_id;
                            $data3[$i]['answer_text'] = $post['option_' . $limit . '_' . $i];

//array_push($data4,$data3);
                        }
                        $data5[$limit] = $this->SurveyQuestionOption->saveAll($data3);
                    } else if ($post['survey_question_type_' . $limit] == 'R' && isset($post['question_count_' . $limit])) {

                        unset($data11);
                        unset($data33);
                        unset($survey_question_id_arr);
                        unset($survey_question_id);

                        $data11 = array();
                        $data33 = array();
                        $survey_question_id_arr = array();
                        $survey_question_id = 0;

                        $data11['survey_question_id'] = 0;
                        $data11['survey_id'] = $post['survey_id_' . $limit];
                        $data11['is_default'] = '0';
                        $data11['survey_category_id'] = $post['survey_category_id_' . $limit];
                        $data11['survey_question_type'] = $post['survey_question_type_' . $limit];
                        $data11['survey_question_description'] = $post['survey_question_description_' . $limit];
                        $data11['survey_question_marks'] = $post['survey_question_marks_' . $limit];

                        $data11['is_active'] = '1';

                        $option_count = $post['questionr' . $limit . '_count'];

                        if ($this->SurveyQuestion->save($data11)) {

                            $survey_question_id_arr = $this->SurveyQuestion->find('first', array('order' => array('SurveyQuestion.survey_question_id DESC'), 'recursive' => -1));

                            $survey_question_id = $survey_question_id_arr['SurveyQuestion']['survey_question_id'];
                        }
                        for ($i = 1; $i < $option_count; $i++) {
                            $data33[$i]['survey_question_id'] = $survey_question_id;
                            $data33[$i]['answer_text'] = $post['optionr_' . $limit . '_' . $i];

                            //array_push($data4,$data3);
                        }
                        $data55[$limit] = $this->SurveyQuestionOption->saveAll($data33);
                    } else if (isset($post['question_count_' . $limit])) {
                        $data2[$limit]['is_default'] = '0';
                        $data2[$limit]['survey_id'] = $post['survey_id_' . $limit];
                        $data2[$limit]['survey_category_id'] = $post['survey_category_id_' . $limit];
                        $data2[$limit]['survey_question_type'] = $post['survey_question_type_' . $limit];
                        $data2[$limit]['survey_question_description'] = $post['survey_question_description_' . $limit];
                        $data2[$limit]['survey_question_marks'] = $post['survey_question_marks_' . $limit];

                        $data2[$limit]['is_active'] = '1';
                    }
                }

//pr($data2);exit;

                if (!empty($data2)) {
                    $data6 = $this->SurveyQuestion->saveAll($data2);
                }

                if (isset($survey_question_id) && !empty($survey_question_id) || isset($data5) && !empty($data5) || isset($data6) && !empty($data6) || isset($data55) && !empty($data55)) {
                    $this->Session->setFlash('Question added successfully.', 'default', array('class' => 'successmsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
                } else {
                    $this->Session->setFlash('Error ! Unable to add Question.please try again.', 'default', array('class' => 'successmsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'surveyList'));
                }
            }
        }

        $predefined_question_list = $this->SurveyQuestion->find('all', array('conditions' => array('SurveyQuestion.is_default' => 1, 'SurveyQuestion.is_locked' => 0, 'SurveyQuestion.is_blocked' => 0)));

        $survey_lists = $this->Survey->find('all', array('conditions' => array('Survey.client_id' => $client_id, 'Survey.is_locked' => 0)));

        $survey_category = $this->SurveyCategories->find('all', array('fields' => array(), 'conditions' => array('is_blocked' => 0, 'is_active' => 1, 'client_id' => $client_id), 'recursive' => 1));

        $this->set('survey_category', $survey_category);

        $this->set('survey_lists', $survey_lists);

        $this->set('predefined_question_list', $predefined_question_list);

        //pr($predefined_question_list); exit;

        $this->layout = 'client';
    }

    public function question_view() {
        $this->layout = 'client';
        $test_q = $this->SurveyQuestion->find('all', array('fields' => array(), 'recursive' => 1));

        $this->set('listing', $test_q);
    }

    public function edit_question($survey_question_id = null) {

        $this->layout = 'client';
        $client_id = $this->Session->read('Auth.User.client_id');
        $ID = $survey_question_id;

        if ($this->request->is('post')) {
//$post = $this->request->data;
//pr($post);exit;
//echo $post['type']; exit;
            $survey_question_id = $this->request->data('survey_question_id');
            $survey_id = $this->request->data('survey_id');
            $survey_question_description = $this->request->data('survey_question_description');
            $survey_question_marks = $this->request->data('survey_question_marks');
            $survey_question_type = $this->request->data('survey_question_type');
            $survey_category_id = $this->request->data('survey_category_id');
//$is_active = '1';
            $post = $this->request->data;

            $query = $this->SurveyQuestion->query("UPDATE survey_questions SET survey_question_description='$survey_question_description',survey_id='$survey_id',survey_question_marks='$survey_question_marks',survey_category_id='$survey_category_id',survey_question_type='$survey_question_type',is_default='1' WHERE survey_question_id = '$survey_question_id'");

            if (isset($query)) {

//$count = $post['option_count'];

                $type = $post['survey_question_type'];

                $data2 = array();

                if ($type == 'C') {
                    $query1 = $this->SurveyQuestionOption->query("SELECT *FROM survey_question_options WHERE survey_question_id = '$survey_question_id'");

                    $number_of_records = sizeof($query1);

                    if ($number_of_records > 0) {
                        $query2 = $this->SurveyQuestionOption->query("DELETE FROM survey_question_options WHERE survey_question_id = '$survey_question_id'");
                    }

                    for ($i = 1; $i <= 4; $i++) {
                        $data1['survey_question_id'] = $survey_question_id;
                        $data1['answer_text'] = $post['option_' . $i];

                        array_push($data2, $data1);
                    }

                    if ($this->SurveyQuestionOption->saveAll($data2)) {
                        $this->Session->setFlash('Question updated successfully.', 'default', array('class' => 'successmsg'));
                        $this->redirect(array('controller' => 'clients', 'action' => 'view_question_survey/' . $survey_id));
                        exit();
                    } else {
                        $this->Session->setFlash('Error ! Question not updated.please try again.', 'default', array('class' => 'successmsg'));
                        $this->redirect(array('controller' => 'clients', 'action' => 'view_question_survey/' . $survey_id));
                        exit();
                    }
                } else {
                    $this->Session->setFlash('Question updated successfully.', 'default', array('class' => 'errormsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'view_question_survey/' . $survey_id));
                    exit();
                }
            } else {
                $this->Session->setFlash('Unable to update Question.', 'default', array('class' => 'errormsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'view_question_survey/' . $survey_id));
                exit();
            }
        }

        $data = $this->SurveyQuestion->find('all', array('conditions' => array('survey_question_id' => $ID)));

        $is_locked = $data[0]['SurveyQuestion']['is_locked'];

        $survey_question_type = $data[0]['SurveyQuestion']['survey_question_type'];

        if ($survey_question_type == 'C' || $survey_question_type == 'R') {
            $this->Session->setFlash('You can\'t edit question with type Checkbox OR Radio.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'question_view'));
            exit();
        }

        if ($is_locked == '1') {
            $this->Session->setFlash('Question is locked.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'question_view'));
            exit();
        }

        $survey_lists = $this->Survey->find('all', array('conditions' => array('Survey.client_id' => $client_id, 'Survey.is_locked' => 0)));

        $survey_category = $this->SurveyCategories->find('all', array('fields' => array(), 'conditions' => array('is_blocked' => 0, 'is_active' => 1, 'client_id' => $client_id), 'recursive' => 1));
        $count = sizeof($data[0]['SurveyQuestionOption']);

        $this->set('survey_lists', $survey_lists);
        $this->set('survey_category', $survey_category);
        $this->set('singlerecord', $data);
        $this->set('count', $count);
    }

    function activate_inactive_view($survey_question_id = null, $status = null) {

        if ($status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }

        $this->SurveyQuestion->id = $survey_question_id; //exit;

        if ($this->SurveyQuestion->saveField('is_blocked', $status)) {

            $this->Session->setFlash('Status changed successfully.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'question_view'));
            exit();
        } else {

            $this->Session->setFlash('Error ! status not changed.Please Try Again.', 'default', array('class' => 'errormsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'question_view'));
            exit();
        }
    }

    public function survey_question_radiooption() {

        $questionr1_count = $_POST['questionr1_count'];

        $option_count = $_POST['option_count'];
        ?>
        <div class="input-box" id="optionrr_<?php echo $option_count ?>_<?php echo $questionr1_count ?>" >
            <label class="input-name">Option <?php echo $questionr1_count ?></label>
            <input type="text" name="optionr_<?php echo $option_count ?>_<?php echo $questionr1_count ?>" id="optionr_<?php echo $option_count ?>_<?php echo $questionr1_count ?>" value="">
        </div>

        <div id="error_optionr<?php echo $option_count ?>_<?php echo $questionr1_count ?>" class="error"></div>

        <?php
        exit;
    }

    public function survey_question_add() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $survey_lists = $this->Survey->find('all', array('conditions' => array('Survey.client_id' => $client_id, 'Survey.is_locked' => 0)));

        $survey_category = $this->SurveyCategories->find('all', array('fields' => array(), 'conditions' => array('is_blocked' => 0, 'is_active' => 1, 'client_id' => $client_id), 'recursive' => 1));

        $count = $_POST['question_count'];
        ?>

        <input type="hidden" name="questionr<?php echo $count ?>_count" id="questionr<?php echo $count ?>_count" value="3" />

        <div id="apply_question<?php echo $count; ?>" > 
            <h2 class="page-header">Question <?php echo $count; ?></h2>            		
            <div class="input-box">
                <label class="input-name"><span>Survey Category</span>

                    <select name="survey_category_id_<?php echo $count; ?>" id="survey_category_id_<?php echo $count; ?>" >
                        <option value="">Select Type

                            <?php foreach ($survey_category as $category) { ?>

                            <option value="<?php echo $category['SurveyCategories']['survey_category_id']; ?>"><?php echo $category['SurveyCategories']['survey_category_title']; ?>

                            <?php } ?>

                    </select>

                </label>

            </div>
            <div id="error_category_<?php echo $count; ?>" class="error"></div>

            <div class="input-box">
                <label class="input-name"><span>Type</span>

                    <select name="survey_question_type_<?php echo $count; ?>" id="survey_question_type_<?php echo $count; ?>" >
                        <option value="">Select Type</option>                    
                        <option value="C">Checkbox</option>
                        <option value="T">Textbox</option>
                        <option value="I">Image</option>
                        <option value="A">Audio</option>
                        <option value="R">Radio</option>

                    </select>

                </label>
            </div>
            <div id="error_type_<?php echo $count; ?>" class="error"></div>

            <div class="input-box">
                <label class="input-name"><span>Select Survey</span>

                    <select name="survey_id_<?php echo $count; ?>" id="survey_id_<?php echo $count; ?>" >
                        <option value="">Select Survey                    

                            <?php foreach ($survey_lists as $survey_list) { ?>

                            <option value="<?php echo $survey_list['Survey']['survey_id']; ?>"><?php echo $survey_list['Survey']['survey_name']; ?>

                            <?php } ?>

                    </select>

                </label>
            </div>
            <div id="error_survey_<?php echo $count; ?>" class="error"></div>

            <div class="input-box">
                <label class="input-name"><span>Question Description</span></label>
                <input type="text" name="survey_question_description_<?php echo $count; ?>" id="survey_question_description_<?php echo $count; ?>"  />
            </div>
            <div id="error_questions_<?php echo $count; ?>" class="error"></div>

            <div class="input-box">
                <label class="input-name"><span>Question Marks</span></label>
                <input type="text" name="survey_question_marks_<?php echo $count; ?>" id="survey_question_marks_<?php echo $count; ?>" onkeypress="return onlyNumbers(event);"  />
            </div>
            <div id="error_marks_<?php echo $count; ?>" class="error"></div>


            <div id="apply_contentRadio_<?php echo $count ?>" style="display:none;">

                <div id="apply_contentRadioo_<?php echo $count ?>" >	
                    <div class="input-box">
                        <label class="input-name">Option 1</label>
                        <input type="text" name="optionr_<?php echo $count ?>_1" id="optionr_<?php echo $count ?>_1" value="">
                    </div>
                    <div id="error_optionr<?php echo $count ?>_1" class="error"></div>

                    <div class="input-box">
                        <label class="input-name">Option 2</label>
                        <input type="text" name="optionr_<?php echo $count ?>_2" id="optionr_<?php echo $count ?>_2" value="">
                    </div>
                    <div id="error_optionr<?php echo $count ?>_2" class="error"></div>
                </div>

                <a id="n_<?php echo $count ?>" onClick="add_option(this.id)" class="btn btn-lg btn-custom btn-success" >Add Option</a>

                <a id="rn_<?php echo $count ?>" onClick="remove_option(this.id)" class="btn btn-lg btn-custom btn-success" style="display:none;" >Remove Option</a>

            </div>


            <div id="apply_content_<?php echo $count; ?>" style="display:none;"  >

                <div class="input-box">
                    <label class="input-name">Option 1</label>
                    <input type="text" name="option_<?php echo $count; ?>_1" id="option_<?php echo $count; ?>_1" value="">
                </div>
                <div id="error_option<?php echo $count; ?>_1" class="error"></div> 

                <div class="input-box">
                    <label class="input-name">Option 2</label>
                    <input type="text" name="option_<?php echo $count; ?>_2" id="option_<?php echo $count; ?>_2" >
                </div>
                <div id="error_option<?php echo $count; ?>_2" class="error"></div>

                <div class="input-box">
                    <label class="input-name">Option 3</label>
                    <input type="text" name="option_<?php echo $count; ?>_3" id="option_<?php echo $count; ?>_3" >
                </div>
                <div id="error_option<?php echo $count; ?>_3" class="error"></div>

                <div class="input-box">
                    <label class="input-name">Option 4</label>
                    <input type="text" name="option_<?php echo $count; ?>_4" id="option_<?php echo $count; ?>_4" value="">
                </div>
                <div id="error_option<?php echo $count; ?>_4" class="error"></div>

            </div>

        </div>
        <?php
        exit;
    }

    public function view_question_survey($survey_id = null) {
        $client_id = $this->Session->read('Auth.User.client_id');
        $survey_question = $this->SurveyQuestion->find('all', array('conditions' => array('SurveyQuestion.survey_id' => $survey_id)));

        $this->set('survey_questions', $survey_question);
        $this->layout = "client";
    }

    public function client_address() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $address_list = $this->ClientAddress->find('all', array('conditions' => array('ClientAddress.client_id' => $client_id)));
        $this->set('address_list', $address_list);


        $this->layout = "client";
    }

    public function add_address() {
        $client_id = $this->Session->read('Auth.User.client_id');
        if ($this->request->is('post')) {

            $post = $this->request->data;

            $replace_this = '/[^,&\/\sA-Za-z0-9_]/';

            $replace_with = '';

            $address_data['address'] = $this->removespecialchar($post['location'], $replace_this, $replace_with);
            $address_data['client_id'] = $client_id;
//$address_data['address']  = $post['location'];
            $address_data['postal_code'] = $post['postal_code'];
            $address_data['phone_number'] = $post['contact_number'];
            $address_data['lattitude'] = $post['lattitude'];
            $address_data['longitude'] = $post['longitude'];

            $qr_data = md5($address_data['postal_code'] . "" . $client_id . "" . $address_data['phone_number'] . microtime());
            $microtime = microtime();
            $qr_img_name = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . "png";
            $qrcode = QRcode::png($qr_data, WEBSITE_WEBROOT . "media/qr_code/" . $qr_img_name);

            $address_data['qr_code_data'] = $qr_data;
            $address_data['qr_code_image'] = $qr_img_name;
//pr($address_data);exit;
            if ($this->ClientAddress->save($address_data)) {
                $this->Session->setFlash('Address added successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'client_address'));
                exit();
            } else {
                $this->Session->setFlash('Unable to add address.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'client_address'));
                exit();
            }
        }


        $this->layout = "client";
    }

    public function edit_address($address_id = null) {
        $client_id = $this->Session->read('Auth.User.client_id');
        if ($this->request->is('post')) {
//$update_address['client_id']  = $client_id;
            $update_address['address'] = $this->request->data['location'];
            $update_address['phone_number'] = $this->request->data['contact_number'];
            $update_address['postal_code'] = $this->request->data['postal_code'];
            $update_address['lattitude'] = $this->request->data['lattitude'];
            $update_address['longitude'] = $this->request->data['longitude'];
            $update_id = $this->request->data['id'];
            $this->ClientAddress->id = $update_id;
            if ($this->ClientAddress->save($update_address)) {
                $this->Session->setFlash('Address editted successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'client_address'));
                exit();
            } else {
                $this->Session->setFlash('Please check your data.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'client_address'));
                exit();
            }
        }


        $get_list_address = $this->ClientAddress->find('first', array('conditions' => array('ClientAddress.client_address_id' => $address_id, 'ClientAddress.client_id' => $client_id), 'recursive' => -1));
        $this->set('get_list_address', $get_list_address);



        $this->layout = 'client';
    }

    public function reports() {
        $rank = $this->AgentJob->query('SELECT * FROM agent_ranks ');
        $this->set('rank', $rank);
        $this->layout = 'client';
    }

    public function job_reports() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $condition = "WHERE client_id = " . $client_id;
        $address = $this->AgentJob->query('SELECT * FROM client_address ' . $condition);
        $survey = $this->AgentJob->query('SELECT * FROM surveys ' . $condition);
        $this->set('address', $address);
        $this->set('survey', $survey);
        $this->layout = 'client';
    }

    public function job_reports_request() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $condition = "WHERE client_id = " . $client_id;
        $address = $this->AgentJob->query('SELECT * FROM client_address ' . $condition);
        $job = $this->AgentJob->query('SELECT * FROM jobs ' . $condition);
        $rank = $this->AgentJob->query('SELECT * FROM agent_ranks ');
        $this->set('address', $address);
        $this->set('rank', $rank);
        $this->set('job', $job);
        $this->layout = 'client';
    }

    public function job_reports_submission() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $condition = "WHERE client_id = " . $client_id;
        $address = $this->AgentJob->query('SELECT * FROM client_address ' . $condition);
        $job = $this->AgentJob->query('SELECT * FROM jobs ' . $condition);
        $this->set('address', $address);
        $this->set('job', $job);
        $this->layout = 'client';
    }

    public function survey_category_report() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $condition = "WHERE client_id = " . $client_id;
        $survey = $this->AgentJob->query('SELECT * FROM surveys ' . $condition);
        $this->set('survey', $survey);
        $this->layout = 'client';
    }

    public function survey_question_report() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $condition = "WHERE client_id = " . $client_id;
        $survey = $this->AgentJob->query('SELECT * FROM surveys ' . $condition);
        $cat = $this->AgentJob->query('SELECT * FROM survey_categories ');
        $this->set('survey', $survey);
        $this->set('cat', $cat);
        $this->layout = 'client';
    }

    public function payment_report() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $condition = "WHERE client_id = " . $client_id;
        $job = $this->AgentJob->query('SELECT * FROM jobs ' . $condition);
        $this->set('job', $job);
        $this->layout = 'client';
    }

    public function fetch_payment_report() {
        if ($this->request->is('ajax')) {
            $start_date = date('Y-m-d', strtotime($_POST['startdate']));
            $end_date = date('Y-m-d', strtotime($_POST['enddate']));
            $job_id = $_POST['job_id'];
            $status = $_POST['status'];
            $reward = $_POST['reward'];
            $res = $this->view_payment_reports($start_date, $end_date, $job_id, $status, $reward);
            $this->set('res', $res);
            $this->set('type', $type);
        } else {
            echo "<table><tr><td>No data found</td><tr></table>";
            exit;
        }
    }

    public function view_payment_reports($start_date = '', $end_date = '', $job_id = '', $status = '', $reward = '') {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE agent_jobs.client_id = " . $client_id;
        $completed_condition = "AND agent_jobs.accepted_by_client = 1 "
                . "AND agent_jobs.declined_by_client = 0 "
                . "AND agent_jobs.completed_by_agent = 1 "
                . "AND agent_jobs.cancelled_by_agent = 0 "
                . "AND agent_jobs.disapproved_by_client = 0 "
                . "AND agent_jobs.approved_by_client = 1 ";


        if ($start_date != 0 && $start_date != "" && $start_date != "1970-01-01") {
            $condition_date = "AND agent_jobs.payment_time >= " . "'" . $start_date . "'" . "AND agent_jobs.payment_time <= " . "'" . $end_date . "'";
        } else {
            $condition_date = "";
        }

        if ($job_id != "0" && $job_id != "n" && $job_id != "") {
            $job_condition = "AND agent_jobs.job_id = " . "'" . $job_id . "'";
        } else {
            $job_condition = "";
        }


        if ($status != "n" && $status != "" && $status != "0") {
            $status_condition = "AND agent_jobs.is_paid = " . $status;
        } else if ($status == "0") {
            $status_condition = "AND agent_jobs.is_paid = 0";
        } else {
            $status_condition = "";
        }

        if ($reward != "n" && $reward != "" && $reward != "2") {
            $reward_condition = "AND agent_jobs.coupon_id = 1";
        } else if ($reward == "2") {
            $reward_condition = "AND agent_jobs.coupon_id = 0";
        } else {
            $reward_condition = "";
        }

        $Agents = $this->AgentJob->query('SELECT coupons.coupon_name,agent_user_infos.agent_name,agent_jobs.job_id,agent_jobs.percentage_earned,agent_jobs.is_paid,agent_jobs.job_end_time,agent_jobs.coupon_id,agent_jobs.amount_paid,agent_jobs.reward_type FROM agent_jobs '
                . 'LEFT JOIN agent_user_infos ON agent_jobs.agent_id = agent_user_infos.agent_id  '
                . 'LEFT JOIN coupons ON agent_jobs.coupon_id = coupons.coupon_id ' . $client_condition . ' ' . $condition_date . ' ' . $completed_condition . ' ' . $job_condition . ' ' . $status_condition . ' ' . $reward_condition);

//$log = $this->AgentJob->getDataSource()->getLog(false, false);
// debug($log);
        $message = $Agents;
        return $message;
    }

    public function getGraphData() {
// echo "<pre>";

        $start_date = date("Y-m-d");
        $end_date = strtotime("$start_date -1 year");
        $end_date = date("Y-m-d", $end_date);
        $condition_date = "WHERE agent_jobs.modified BETWEEN " . "'" . $end_date . "'" . "AND" . "'" . $start_date . "'";

        $AgentsNames = $this->AgentJob->query('SELECT agent_user_infos.agent_id, agent_user_infos.agent_name
            FROM agent_jobs
            LEFT JOIN agent_user_infos on agent_user_infos.agent_id = agent_jobs.agent_id
            GROUP BY agent_jobs.agent_id
        ');
//print_r($AgentsNames);
        $finalArr = array();
        foreach ($AgentsNames as $agents) {
            $AgentsJobs = $this->AgentJob->query("SELECT agent_id,COUNT(modified) as completed_job, LPAD(MONTH(modified), 2, '0') as month,YEAR(modified) as year 
                FROM agent_jobs
                WHERE modified > DATE_ADD(Now(), INTERVAL -1 YEAR)
                AND agent_id ={$agents[agent_user_infos][agent_id]}
                AND approved_by_client=1
                GROUP BY monthname(modified)
            ");

            $tempArr = array();
            foreach ($AgentsJobs as $jobs) {
                $tempArr[] = $jobs[0];
            }
            $finalArr[] = array(
                'agent_id' => $agents[agent_user_infos][agent_id],
                'agent_name' => $agents[agent_user_infos][agent_name],
                'jobs' => $tempArr
            );
        }
        /* echo "<pre>";
          print_r($finalArr);
          exit;
         */
        foreach ($finalArr as &$eachArray) {
            $agentName[] = $eachArray['agent_name'];
            for ($i = 0; $i < 12; $i++) {
                $tmp_date = strtotime(date('Y-m-01') . "-$i months");
                $month = date("m", $tmp_date);
                $year = date("Y", $tmp_date);

                $add_date = true;
                foreach ($eachArray['jobs'] as $eachJob) {
                    if ($eachJob['month'] == $month && $eachJob['year'] == $year) {
                        $add_date = FALSE;
                        break;
                    }
                }
                if ($add_date) {
                    $eachArray['jobs'][] = array(
                        'completed_job' => 0,
                        'month' => $month,
                        'year' => $year
                    );
                }
            }
        }
// echo "<pre>";
// print_r($finalArr);
//   exit;

        $month = array();
        for ($i = 1; $i < 13; $i++) {
            $month[$i] = array();
            $month[$i][] = (string) $i;
//echo'<pre>'; print_r($finalArr);
            foreach ($finalArr as $each) {
                foreach ($each['jobs'] as $eachMonth) {
                    if ($eachMonth['month'] == $i) {
                        $month[$i]['0'] = $eachMonth['month'] . "/" . $eachMonth['year'];
                        $month[$i][] = (int) $eachMonth['completed_job'];
                    }
                }
            }
        }


// echo "<pre>";
// print_r($finalArr);
// print_r($month);
// echo json_encode($month);
// exit;

        $response = array('finalArr' => $finalArr, 'month' => $month);
// print_r($agentName);
// print_r($finalArr);
        echo json_encode($response, true);
        exit;
    }

    public function fetch_report() {
        if ($this->request->is('ajax')) {
            $start_date = date('Y-m-d', strtotime($_POST['startdate']));
            $type = $_POST['type'];
            $rank = $_POST['rank'];
            $test_status = $_POST['test_status'];
            $res = $this->view_reports($start_date, $type, $rank, $test_status);
            $this->set('res', $res);
            $this->set('type', $type);
        } else {
            echo "<table><tr><td>No data found</td><tr></table>";
            exit;
        }
    }

    public function view_reports($start_date = '', $type = '', $rank = '', $test_status = '') {

        if ($start_date != 0 && $start_date != "" && $start_date != "1970-01-01") {
            $condition_date = "WHERE aui.last_login >= " . "'" . $start_date . "'";
        } else {
            $condition_date = "";
        }

        if ($test_status == "1") {
            $test_condition = "WHERE aui.test_passed = " . "'" . $test_status . "'";
        } else if ($test_status == "0") {
            $test_condition = "WHERE aui.test_passed = " . "'" . $test_status . "'";
        } else if ($test_status == "n") {
            $test_condition = "";
        }


        if ($rank != 0) {
            $condition = "WHERE aui.agent_rank = " . $rank;
        } else {
            $condition = "";
        }

// $Agents = $this->AgentJob->query('SELECT agent_user_infos.last_login,agent_user_infos.test_passed, COUNT(agent_jobs.agent_id) AS applied_count ,SUM(agent_jobs.approved_by_client=1) AS completed_count, agent_user_infos.agent_rank,agent_user_infos.agent_name,agent_user_infos.email_address FROM agent_jobs '
//       . 'LEFT JOIN agent_user_infos ON agent_jobs.agent_id = agent_user_infos.agent_id ' . $condition . ' ' . $condition_date . ' ' . $test_condition . ' GROUP BY agent_jobs.agent_id');
        $Agents = $this->AgentUserInfo->query('SELECT aui.last_login, aui.test_passed, COALESCE(SUM(aj.accepted_by_client=0),0) AS applied_count ,COALESCE(SUM(aj.approved_by_client=1),0) AS completed_count, aui.agent_rank,aui.agent_name,aui.email_address FROM agent_jobs aj Right JOIN agent_user_infos aui ON  aj.agent_id = aui.agent_id ' . $condition . ' ' . $condition_date . ' ' . $test_condition . '  GROUP BY aui.agent_id');
// $log = $this->AgentJob->getDataSource()->getLog(false, false);
// debug($log);
        $message = $Agents;
        return $message;
    }

    public function export_payment_report() {

        $start_date = $this->params['pass']['0'];
        $end_date = $this->params['pass']['1'];
        $job_id = $this->params['pass']['2'];
        $status = $this->params['pass']['3'];
        $reward = $this->params['pass']['4'];
        $res = $this->view_payment_reports($start_date, $end_date, $job_id, $status, $reward);


        /*         * ********** code for creating excel *************** */
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        $sheet = $objPHPExcel->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true
            )
        );
        $sheet->setCellValue('A1', 'Agent Name');
        $sheet->getStyle('A1')->applyFromArray($styleArray);
        $sheet->setCellValue('C1', 'Job ID');
        $sheet->getStyle('C1')->applyFromArray($styleArray);
        $sheet->setCellValue('E1', 'Job Completed');
        $sheet->getStyle('E1')->applyFromArray($styleArray);
        $sheet->setCellValue('G1', 'Submitted');
        $sheet->getStyle('G1')->applyFromArray($styleArray);
        $sheet->setCellValue('I1', 'Payment Status');
        $sheet->getStyle('I1')->applyFromArray($styleArray);
        $sheet->setCellValue('K1', 'Reward');
        $sheet->getStyle('K1')->applyFromArray($styleArray);


        $count = 3;

        foreach ($res as $job_list) {

            $now = time(); // or your date as well
            $your_date = strtotime($job_list['agent_jobs']['job_end_time']);
            $datediff = $now - $your_date;
            $submitted = floor($datediff / (60 * 60 * 24));

            if ($job_list['agent_jobs']['is_paid'] == "1") {
                $status = "Paid";
            } else {
                $status = "Pending";
            }

            if ($status != "Pending") {
                if ($job_list['agent_jobs']['coupon_id'] != "" && $job_list['agent_jobs']['coupon_id'] != "0") {
                    $reward = $job_list['coupons']['coupon_name'];
                } else {
                    $reward = $job_list['agent_jobs']['amount_paid'];
                }
            }
// Miscellaneous glyphs, UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$count", $job_list['agent_user_infos']['agent_name'])
                    ->setCellValue("C$count", $job_list['agent_jobs']['job_id'])
                    ->setCellValue("E$count", $job_list['agent_jobs']['percentage_earned'] . "%")
                    ->setCellValue("G$count", $submitted . " Days Ago")
                    ->setCellValue("I$count", $status)
                    ->setCellValue("K$count", $reward);

            $count++;
        }
// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Payment Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Payment Report.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function export_report() {

        $start_date = $this->params['pass']['0'];
        $type = $_POST['type'];
        $rank = $this->params['pass']['1'];
        $test_status = $this->params['pass']['2'];
        $res = $this->view_reports($start_date, $type, $rank, $test_status);


        /*         * ********** code for creating excel *************** */
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        $sheet = $objPHPExcel->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true
            )
        );
        $sheet->setCellValue('A1', 'Agent Name');
        $sheet->getStyle('A1')->applyFromArray($styleArray);
        $sheet->setCellValue('B1', 'Agent Email');
        $sheet->getStyle('B1')->applyFromArray($styleArray);
        $sheet->setCellValue('C1', 'Agent Rank');
        $sheet->getStyle('C1')->applyFromArray($styleArray);
        $sheet->setCellValue('D1', 'Applied Jobs');
        $sheet->getStyle('D1')->applyFromArray($styleArray);
        $sheet->setCellValue('E1', 'Completed Jobs');
        $sheet->getStyle('E1')->applyFromArray($styleArray);
        $sheet->setCellValue('F1', 'Test Status');
        $sheet->getStyle('F1')->applyFromArray($styleArray);


        $count = 3;

        foreach ($res as $job_list) {

            $rank_id = $job_list['aui']['agent_rank'];
            if ($rank_id == "1") {
                $rank = "Pro";
            }
            if ($rank_id == "2") {
                $rank = "Expert";
            }
            if ($rank_id == "3") {
                $rank = "Advanced";
            }
            if ($rank_id == "4") {
                $rank = "Normal";
            }
            if ($rank_id == "5") {
                $rank = "Beginner";
            }
            if ($rank_id >= "6") {
                $rank = "";
            }

            if ($job_list['aui']['test_passed'] == 1) {
                $test = "Passed";
            } else {
                $test = "Failed";
            }

// Miscellaneous glyphs, UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$count", $job_list['aui']['agent_name'])
                    ->setCellValue("B$count", $job_list['aui']['email_address'])
                    ->setCellValue("C$count", $rank)
                    ->setCellValue("D$count", $job_list[0]['applied_count'])
                    ->setCellValue("E$count", $job_list[0]['completed_count'])
                    ->setCellValue("F$count", $test);

            $count++;
        }
// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Agent Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Agent_Report.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function export_job_report() {

        $start_date = $this->params['pass']['0'];
        $end_date = $this->params['pass']['1'];
        $survey_name = $this->params['pass']['2'];
        $location = $this->params['pass']['3'];
        $res = $this->view_reports_job($start_date, $end_date, $survey_name, $location);


        /*         * ********** code for creating excel *************** */
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        $sheet = $objPHPExcel->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true
            )
        );
        $sheet->setCellValue('A1', 'JOB ID');
        $sheet->getStyle('A1')->applyFromArray($styleArray);
        $sheet->setCellValue('B1', 'Survey');
        $sheet->getStyle('B1')->applyFromArray($styleArray);
        $sheet->setCellValue('C1', 'Start Date');
        $sheet->getStyle('C1')->applyFromArray($styleArray);
        $sheet->setCellValue('D1', 'End Date');
        $sheet->getStyle('D1')->applyFromArray($styleArray);
        $sheet->setCellValue('E1', 'Request Received');
        $sheet->getStyle('E1')->applyFromArray($styleArray);
        $sheet->setCellValue('F1', 'Submission Received');
        $sheet->getStyle('F1')->applyFromArray($styleArray);
        $sheet->setCellValue('G1', 'Approved Jobs');
        $sheet->getStyle('G1')->applyFromArray($styleArray);


        $count = 3;

        foreach ($res as $job_list) {
            if ($job_list['jobs']['job_start_date'] != "0000-00-00 00:00:00") {
                $job_start_date = date("d-m-Y", strtotime($job_list['jobs']['job_start_date']));
            } else {
                $job_start_date = "";
            }
            if ($job_list['jobs']['job_end_date'] != "0000-00-00 00:00:00") {
                $job_end_date = date("d-m-Y", strtotime($job_list['jobs']['job_end_date']));
            } else {
                $job_end_date = "";
            }

// Miscellaneous glyphs, UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$count", $job_list['jobs']['job_id'])
                    ->setCellValue("B$count", $job_list['surveys']['survey_name'])
                    ->setCellValue("C$count", $job_start_date)
                    ->setCellValue("D$count", $job_end_date)
                    ->setCellValue("E$count", $job_list['jobs']['request_received'])
                    ->setCellValue("F$count", $job_list['jobs']['submission_received'])
                    ->setCellValue("G$count", $job_list['jobs']['approved_count']);

            $count++;
        }
// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Job Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Job_Report.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function export_job_report_request() {

        $start_date = $this->params['pass']['0'];
        $end_date = $this->params['pass']['1'];
        $job_id = $this->params['pass']['2'];
        $status = $this->params['pass']['3'];
        $rank = $this->params['pass']['4'];
        $joblocation = $this->params['pass']['5'];
        $res = $this->view_reports_job_request($start_date, $end_date, $job_id, $status, $rank, $joblocation);


        /*         * ********** code for creating excel *************** */
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        $sheet = $objPHPExcel->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true
            )
        );
        $sheet->setCellValue('A1', 'JOB ID');
        $sheet->getStyle('A1')->applyFromArray($styleArray);
        $sheet->setCellValue('B1', 'Agent Name');
        $sheet->getStyle('B1')->applyFromArray($styleArray);
        $sheet->setCellValue('C1', 'Agent Rank');
        $sheet->getStyle('C1')->applyFromArray($styleArray);
        $sheet->setCellValue('D1', 'Received On');
        $sheet->getStyle('D1')->applyFromArray($styleArray);
        $sheet->setCellValue('E1', 'Status');
        $sheet->getStyle('E1')->applyFromArray($styleArray);


        $count = 3;

        foreach ($res as $job_list) {

            $accepted_by_client = $job_list['agent_jobs']['accepted_by_client'];
            $declined_by_client = $job_list['agent_jobs']['declined_by_client'];
            $completed_by_agent = $job_list['agent_jobs']['completed_by_agent'];
            $cancelled_by_agent = $job_list['agent_jobs']['cancelled_by_agent'];
            $disapproved_by_client = $job_list['agent_jobs']['disapproved_by_client'];
            $approved_by_client = $job_list['agent_jobs']['approved_by_client'];

            if ($accepted_by_client == "0" && $declined_by_client == "0" && $completed_by_agent == "0" && $cancelled_by_agent == "0" && $disapproved_by_client == "0" && $approved_by_client == "0") {
                $job_status = "Pending";
            } else if ($accepted_by_client == "1" && $declined_by_client == "0" && $completed_by_agent == "0" && $cancelled_by_agent == "0" && $disapproved_by_client == "0" && $approved_by_client == "0") {
                $job_status = "Accepted";
            } else if ($accepted_by_client == "0" && $declined_by_client == "1" && $completed_by_agent == "0" && $cancelled_by_agent == "0" && $disapproved_by_client == "0" && $approved_by_client == "0") {
                $job_status = "Declined";
            } else {
                $job_status = "no";
            }

            if ($job_status == "no") {
                continue;
            }

            if ($job_list['agent_jobs']['created'] != "0000-00-00 00:00:00") {
                $date = date("d-m-Y", strtotime($job_list['agent_jobs']['created']));
            } else {
                $date = "";
            }

// Miscellaneous glyphs, UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$count", $job_list['agent_jobs']['job_id'])
                    ->setCellValue("B$count", $job_list['agent_user_infos']['agent_name'])
                    ->setCellValue("C$count", $job_list['agent_ranks']['rank_title'])
                    ->setCellValue("D$count", $date)
                    ->setCellValue("E$count", $job_status);

            $count++;
        }
// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Job ReQuest Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Job_Request_Report.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function export_job_report_submission() {

        $start_date = $this->params['pass']['0'];
        $end_date = $this->params['pass']['1'];
        $job_id = $this->params['pass']['2'];
        $status = $this->params['pass']['3'];
        $joblocation = $this->params['pass']['4'];
        $res = $this->view_reports_job_submission($start_date, $end_date, $job_id, $status, $joblocation);


        /*         * ********** code for creating excel *************** */
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        $sheet = $objPHPExcel->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true
            )
        );
        $sheet->setCellValue('A1', 'JOB ID');
        $sheet->getStyle('A1')->applyFromArray($styleArray);
        $sheet->setCellValue('B1', 'Agent Name');
        $sheet->getStyle('B1')->applyFromArray($styleArray);
        $sheet->setCellValue('C1', 'Completed');
        $sheet->getStyle('C1')->applyFromArray($styleArray);
        $sheet->setCellValue('D1', 'Submitted On');
        $sheet->getStyle('D1')->applyFromArray($styleArray);
        $sheet->setCellValue('E1', 'Status');
        $sheet->getStyle('E1')->applyFromArray($styleArray);


        $count = 3;

        foreach ($res as $job_list) {

            $accepted_by_client = $job_list['agent_jobs']['accepted_by_client'];
            $declined_by_client = $job_list['agent_jobs']['declined_by_client'];
            $completed_by_agent = $job_list['agent_jobs']['completed_by_agent'];
            $cancelled_by_agent = $job_list['agent_jobs']['cancelled_by_agent'];
            $disapproved_by_client = $job_list['agent_jobs']['disapproved_by_client'];
            $approved_by_client = $job_list['agent_jobs']['approved_by_client'];

            if ($accepted_by_client == "0" && $declined_by_client == "0" && $completed_by_agent == "0" && $cancelled_by_agent == "0" && $disapproved_by_client == "0" && $approved_by_client == "0") {
                $job_status = "Pending";
            } else if ($accepted_by_client == "1" && $declined_by_client == "0" && $completed_by_agent == "0" && $cancelled_by_agent == "0" && $disapproved_by_client == "0" && $approved_by_client == "0") {
                $job_status = "Accepted";
            } else if ($accepted_by_client == "0" && $declined_by_client == "1" && $completed_by_agent == "0" && $cancelled_by_agent == "0" && $disapproved_by_client == "0" && $approved_by_client == "0") {
                $job_status = "Declined";
            } else {
                $job_status = "no";
            }

            if ($job_status == "no") {
                continue;
            }

            if ($job_list['agent_jobs']['job_start_time'] != "0000-00-00 00:00:00") {
                $date = date("d-m-Y", strtotime($job_list['agent_jobs']['job_start_time']));
            } else {
                $date = "";
            }

// Miscellaneous glyphs, UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$count", $job_list['agent_jobs']['job_id'])
                    ->setCellValue("B$count", $job_list['agent_user_infos']['agent_name'])
                    ->setCellValue("C$count", $job_list['agent_jobs']['percentage_earned'])
                    ->setCellValue("D$count", $date)
                    ->setCellValue("E$count", $job_status);

            $count++;
        }
// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Job Submission Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Job_Submission_Report.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function export_survey_cat_report() {

        $survey_name = $this->params['pass']['0'];
        $res = $this->view_survey_category_report($survey_name);


        /*         * ********** code for creating excel *************** */
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        $sheet = $objPHPExcel->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true
            )
        );
        $sheet->setCellValue('A1', 'Category Name');
        $sheet->getStyle('A1')->applyFromArray($styleArray);
        $sheet->setCellValue('B1', 'Survey Name');
        $sheet->getStyle('B1')->applyFromArray($styleArray);
        $sheet->setCellValue('C1', 'Total Question');
        $sheet->getStyle('C1')->applyFromArray($styleArray);

        $count = 3;

        foreach ($res as $job_list) {

// Miscellaneous glyphs, UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$count", $job_list['survey_categories']['survey_category_title'])
                    ->setCellValue("B$count", $job_list['surveys']['survey_name'])
                    ->setCellValue("C$count", $job_list['0']['que_count']);

            $count++;
        }
// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Survey Category Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Survey_Category_Report.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function export_survey_que_report() {

        $survey_name = $this->params['pass']['0'];
        $cat_name = $this->params['pass']['1'];
        $que = $this->params['pass']['2'];
        $res = $this->view_survey_question_report($cat_name, $survey_name, $que);


        /*         * ********** code for creating excel *************** */
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        $sheet = $objPHPExcel->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true
            )
        );
        $sheet->setCellValue('A1', 'Survey Name');
        $sheet->getStyle('A1')->applyFromArray($styleArray);
        $sheet->setCellValue('B1', 'Category Name');
        $sheet->getStyle('B1')->applyFromArray($styleArray);
        $sheet->setCellValue('C1', 'Question');
        $sheet->getStyle('C1')->applyFromArray($styleArray);
        $sheet->setCellValue('D1', 'Question Type');
        $sheet->getStyle('D1')->applyFromArray($styleArray);
        $sheet->setCellValue('E1', 'Answer Received');
        $sheet->getStyle('E1')->applyFromArray($styleArray);

        $count = 3;

        foreach ($res as $job_list) {

            if ($job_list['survey_questions']['survey_question_type'] == "C") {
                $question_type = "MCQ";
            } else if ($job_list['survey_questions']['survey_question_type'] == "T") {
                $question_type = "TEXT";
            } else if ($job_list['survey_questions']['survey_question_type'] == "I") {
                $question_type = "IMAGE";
            } else if ($job_list['survey_questions']['survey_question_type'] == "A") {
                $question_type = "AUDIO";
            } else {
                $question_type = "";
            }

// Miscellaneous glyphs, UTF-8
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$count", $job_list['surveys']['survey_name'])
                    ->setCellValue("B$count", $job_list['survey_categories']['survey_category_title'])
                    ->setCellValue("C$count", $job_list['survey_questions']['survey_question_description'])
                    ->setCellValue("D$count", $question_type)
                    ->setCellValue("E$count", $job_list['0']['answer']);

            $count++;
        }
// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Survey Question Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Survey_Question_Report.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function fetch_report_job() {
        if ($this->request->is('ajax')) {
            $start_date = date('Y-m-d', strtotime($_POST['startdate']));
            $end_date = date('Y-m-d', strtotime($_POST['enddate']));
            $survey_name = $_POST['survey_name'];
            $location = $_POST['location_data'];
            $res = $this->view_reports_job($start_date, $end_date, $survey_name, $location);
            $this->set('res', $res);
        } else {
            echo "<table><tr><td>No data found</td><tr></table>";
            exit;
        }
    }

    public function view_reports_job($start_date = '', $end_date = '', $survey_name = '', $location = '') {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE jobs.client_id = " . $client_id;

        if ($start_date != 0 && $start_date != "" && $start_date != "1970-01-01" && $end_date != "1970-01-01") {
            $condition_date = " AND jobs.job_start_date >= " . "'" . $start_date . "' AND jobs.job_end_date <=" . "'" . $end_date . "'";
        } else {
            $condition_date = "";
        }

        if ($location != "" && $location != "n") {
            $condition_location = "AND job_locations.job_id = " . $location;
        } else {
            $condition_location = "";
        }

        if ($survey_name != "" && $survey_name != "n" && $survey_name != "0") {
            $condition = "AND surveys.survey_id = " . $survey_name;
        } else {
            $condition = "";
        }

        $job = $this->AgentJob->query('SELECT jobs.job_id,surveys.survey_name,jobs.job_start_date,jobs.job_end_date,jobs.request_received,jobs.submission_received,jobs.approved_count FROM jobs '
                . 'LEFT JOIN surveys ON surveys.survey_id = jobs.survey_id '
                . 'LEFT JOIN job_locations ON job_locations.job_id = jobs.job_id ' . $client_condition . ' ' . $condition_location . ' ' . $condition_date . ' ' . $condition . 'ORDER BY jobs.job_start_date DESC');

// $log = $this->AgentJob->getDataSource()->getLog(false, false);
// debug($log);
        return $job;
    }

    public function getPaymentGraphData() {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE agent_jobs.client_id = " . $client_id;



        $job = $this->AgentJob->query("SELECT COUNT(aj.agent_job_id) rewardCount, aj.reward_type FROM `agent_jobs` aj WHERE aj.client_id = $client_id AND aj.accepted_by_client = 1 
            AND aj.declined_by_client = 0 
            AND aj.completed_by_agent = 1
            AND aj.cancelled_by_agent = 0
            AND aj.disapproved_by_client = 0 
            AND aj.approved_by_client = 1 
            AND aj.is_paid = 1
            GROUP BY aj.reward_type");

//$log = $this->AgentJob->getDataSource()->getLog(false, false);
// debug($log);
//echo "<pre>";
//print_r($job);
        $farray = array();
        $header = array('Reward Type', 'Count');
        array_push($farray, $header);
        foreach ($job as $value) {

            if ($value['aj']['reward_type'] == "1") {
                $reward = "Cash";
            } else if ($value['aj']['reward_type'] == "2") {
                $reward = "Coupon";
            } else {
                $reward = "";
            }


            $subarray = array();

            array_push($subarray, $reward);

            if ($reward == "Cash") {
                array_push($subarray, (int) $value['0']['rewardCount']);
            } else {
                array_push($subarray, (int) $value['0']['rewardCount']);
            }


            array_push($farray, $subarray);
        }


//$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }

    public function getPaymentSGraphData() {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE agent_jobs.client_id = " . $client_id;



        $job = $this->AgentJob->query("SELECT sum(case when aj.is_paid = 1 then 1 else 0 end) as paid,sum(case when aj.is_paid = 0 then 1 else 0 end) as unpaid,aj.is_paid as status FROM `agent_jobs` aj WHERE aj.client_id = $client_id AND aj.accepted_by_client = 1 
            AND aj.declined_by_client = 0 
            AND aj.completed_by_agent = 1
            AND aj.cancelled_by_agent = 0
            AND aj.disapproved_by_client = 0 
            AND aj.approved_by_client = 1  GROUP BY aj.is_paid");

//$log = $this->AgentJob->getDataSource()->getLog(false, false);
// debug($log);
//echo "<pre>";
// print_r($job);
        $farray = array();
        $header = array('Status', 'Count');
        array_push($farray, $header);
        foreach ($job as $value) {

            if ($value['aj']['status'] == "") {
                $status = "Pending";
            } else if ($value['aj']['status'] == "1") {
                $status = "Completed";
            } else {
                $status = "";
            }


            $subarray = array();

            array_push($subarray, $status);

            if ($status == "Completed") {
                array_push($subarray, (int) $value['0']['paid']);
            } else {
                array_push($subarray, (int) $value['0']['unpaid']);
            }


            array_push($farray, $subarray);
        }


//$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }

    public function getjobGraphData() {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE jobs.client_id = " . $client_id;

        $job = $this->AgentJob->query('SELECT jobs.job_id,jobs.request_received,jobs.submission_received,jobs.approved_count FROM jobs ' . $client_condition);

// echo "<pre>";

        $farray = array();
        $header = array('JOBS', 'Requests', 'Submissions', 'Approved');
        array_push($farray, $header);
        foreach ($job as $value) {
            $subarray = array();

            array_push($subarray, (int) $value['jobs']['job_id']);
            array_push($subarray, (int) $value['jobs']['request_received']);
            array_push($subarray, (int) $value['jobs']['submission_received']);
            array_push($subarray, (int) $value['jobs']['approved_count']);

            array_push($farray, $subarray);
        }

// print_r($job);
// print_r($farray);
//$farray = array_merge($header,$farray);
        echo json_encode($farray, JSON_NUMERIC_CHECK);
        exit;
    }

    public function fetch_survey_question_report() {
        if ($this->request->is('ajax')) {
            $cat_name = $_POST['cat_name'];
            $survey_name = $_POST['survey_name'];
            $que = $_POST['que'];
            $res = $this->view_survey_question_report($cat_name, $survey_name, $que);
            $this->set('res', $res);
            $this->set('type', $type);
        } else {
            echo "<table><tr><td>No data found</td><tr></table>";
            exit;
        }
    }

    public function view_survey_question_report($cat_name = '', $survey_name = '', $que = '') {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE surveys.client_id = " . $client_id;

        if ($survey_name != "" && $survey_name != "0") {
            $condition_sname = "AND surveys.survey_id = " . $survey_name;
        } else {
            $condition_sname = "";
        }

        if ($cat_name != "" && $cat_name != "0") {
            $condition_cname = "AND survey_categories.survey_category_id = " . $cat_name;
        } else {
            $condition_cname = "";
        }

        if ($que != "" && $que != "0") {
            $condition_que = "AND survey_questions.survey_question_type = " . "'" . $que . "'";
        } else {
            $condition_que = "";
        }





        $job = $this->AgentJob->query('SELECT survey_questions.survey_question_type,survey_questions.survey_question_description,survey_categories.survey_category_title,surveys.survey_name '
                . ',(SELECT COUNT("agent_survey_answers.survey_question_answer")  FROM agent_survey_answers '
                . ' WHERE agent_survey_answers.survey_question_id = survey_questions.survey_question_id) AS answer '
                . 'FROM survey_questions '
                . 'LEFT JOIN surveys ON survey_questions.survey_id = surveys.survey_id '
                . 'LEFT JOIN survey_categories ON survey_questions.survey_category_id = survey_categories.survey_category_id ' . $client_condition . ' ' . $condition_sname . ' ' . $condition_cname . ' ' . $condition_que);




        /* $job = $this->AgentJob->query('SELECT survey_questions.survey_question_type,COUNT(agent_survey_answers.survey_question_answer) as answer,survey_questions.survey_question_description,survey_categories.survey_category_title,surveys.survey_name FROM survey_questions '
          . 'LEFT JOIN surveys ON survey_questions.survey_id = surveys.survey_id '
          . 'LEFT JOIN agent_survey_answers ON survey_questions.survey_question_id = agent_survey_answers.survey_question_id '
          . 'LEFT JOIN survey_categories ON survey_questions.survey_category_id = survey_categories.survey_category_id ' . $client_condition . ' ' . $condition_sname . ' ' . $condition_cname . ' ' . $condition_que . ' GROUP BY survey_questions.survey_category_id');
         */
        //$log = $this->AgentJob->getDataSource()->getLog(false, false);
        //debug($log);
        return $job;
    }

    public function getSurveyQuesGraphData() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE surveys.client_id = " . $client_id;

        $job = $this->AgentJob->query("SELECT count(sq.survey_question_type) as count,sq.survey_question_type  FROM survey_questions sq
                                        LEFT JOIN surveys sc ON sq.survey_id = sc.survey_id						
                                        LEFT JOIN client_user_infos cui ON cui.client_id = sc.client_id
                                        WHERE cui.client_id = $client_id AND cui.is_active = 1 AND cui.is_blocked = 0 GROUP BY sq.survey_question_type"
        );



        $farray = array();
        $header = array('Question Type', 'Total Questions');
        array_push($farray, $header);
        foreach ($job as $value) {

            if ($value['sq']['survey_question_type'] == "C") {
                $question_type = "MCQ";
            } else if ($value['sq']['survey_question_type'] == "T") {
                $question_type = "TEXT";
            } else if ($value['sq']['survey_question_type'] == "I") {
                $question_type = "IMAGE";
            } else if ($value['sq']['survey_question_type'] == "A") {
                $question_type = "AUDIO";
            } else if ($value['sq']['survey_question_type'] == "R") {
                $question_type = "Radio";
            } else {
                $question_type = "";
            }


            $subarray = array();

            array_push($subarray, $question_type);

            if ($value['sq']['survey_question_type'] == "C") {
                array_push($subarray, (int) $value['0']['count']);
            } else if ($value['sq']['survey_question_type'] == "T") {
                array_push($subarray, (int) $value['0']['count']);
            } else if ($value['sq']['survey_question_type'] == "I") {
                array_push($subarray, (int) $value['0']['count']);
            } else if ($value['sq']['survey_question_type'] == "A") {
                array_push($subarray, (int) $value['0']['count']);
            } else if ($value['sq']['survey_question_type'] == "R") {
                array_push($subarray, (int) $value['0']['count']);
            } else {
                
            }


            array_push($farray, $subarray);
        }


//$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }

    public function fetch_survey_category_report() {
        if ($this->request->is('ajax')) {
            $survey_name = $_POST['survey_name'];
            $res = $this->view_survey_category_report($survey_name);
            $this->set('res', $res);
            $this->set('type', $type);
        } else {
            echo "<table><tr><td>No data found</td><tr></table>";
            exit;
        }
    }

    public function view_survey_category_report($survey_name = '') {

        if ($survey_name != "" && $survey_name != "n" && $survey_name != "0") {
            $condition = "AND surveys.survey_id = " . $survey_name;
        } else {
            $condition = "";
        }
        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE surveys.client_id = " . $client_id;
        $job = $this->AgentJob->query('SELECT COUNT(survey_questions.survey_category_id) as que_count,survey_categories.survey_category_title,surveys.survey_name FROM surveys '
                . 'LEFT JOIN survey_questions ON surveys.survey_id = survey_questions.survey_id '
                . 'LEFT JOIN survey_categories ON survey_questions.survey_category_id = survey_categories.survey_category_id ' . $client_condition . ' ' . $condition . ' GROUP BY survey_questions.survey_category_id');

// $log = $this->AgentJob->getDataSource()->getLog(false, false);
// debug($log);
        return $job;
    }

    public function getSurveyCatGraphData() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE surveys.client_id = " . $client_id;
        $job = $this->AgentJob->query('SELECT COUNT(survey_questions.survey_category_id) as que_count,survey_categories.survey_category_title FROM surveys '
                . 'LEFT JOIN survey_questions ON surveys.survey_id = survey_questions.survey_id '
                . 'LEFT JOIN survey_categories ON survey_questions.survey_category_id = survey_categories.survey_category_id ' . $client_condition . ' ' . $condition . ' GROUP BY survey_questions.survey_category_id');


        $farray = array();
        $header = array('Category', 'Questions');
        array_push($farray, $header);
        foreach ($job as $value) {
            $subarray = array();

            array_push($subarray, $value['survey_categories']['survey_category_title']);
            array_push($subarray, (int) $value['0']['que_count']);

            array_push($farray, $subarray);
        }


//$farray = array_merge($header,$farray);
        echo json_encode($farray);
        exit;
    }

    public function fetch_report_job_request() {
        if ($this->request->is('ajax')) {
            $start_date = date('Y-m-d', strtotime($_POST['startdate']));
            $end_date = date('Y-m-d', strtotime($_POST['enddate']));
            $job_id = $_POST['job_id'];
            $status = $_POST['status'];
            $rank = $_POST['rank'];
            $joblocation = $_POST['joblocation'];
            $res = $this->view_reports_job_request($start_date, $end_date, $job_id, $status, $rank, $joblocation);
            $this->set('res', $res);
            $this->set('type', $type);
        } else {
            echo "<table><tr><td>No data found</td><tr></table>";
            exit;
        }
    }

    public function view_reports_job_request($start_date = '', $end_date = '', $job_id = '', $status = '', $rank = '', $joblocation = '') {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE agent_jobs.client_id = " . $client_id;

        if ($start_date != 0 && $start_date != "" && $start_date != "1970-01-01" && $end_date != "1970-01-01") {
            $condition_date = "AND agent_jobs.created >= " . "'" . $start_date . "' AND agent_jobs.created <=" . "'" . $end_date . "'";
        } else {
            $condition_date = "";
        }

        if ($joblocation != "" && $joblocation != "n") {
            $condition_location = "AND job_locations.job_id = " . $joblocation;
        } else {
            $condition_location = "";
        }

        if ($job_id != "" && $job_id != "n" && $job_id != "0") {
            $jobid_condition = "AND agent_jobs.job_id = " . $job_id;
        } else {
            $jobid_condition = "";
        }

        if ($rank != "" && $rank != "n" && $rank != "0") {
            $rank_condition = " AND agent_ranks.rank = " . $rank;
        } else {
            $rank_condition = "";
        }

        if ($status != "" && $status != "n" && $status != "0") {

            if ($status == "1") {
                $status_condition = "AND agent_jobs.accepted_by_client = 0 "
                        . "AND agent_jobs.declined_by_client = 0 "
                        . "AND agent_jobs.completed_by_agent = 0 "
                        . "AND agent_jobs.cancelled_by_agent = 0 "
                        . "AND agent_jobs.disapproved_by_client = 0 "
                        . "AND agent_jobs.approved_by_client = 0 ";
            }

            if ($status == "2") {
                $status_condition = "AND agent_jobs.accepted_by_client = 1 "
                        . "AND agent_jobs.declined_by_client = 0 "
                        . "AND agent_jobs.completed_by_agent = 0 "
                        . "AND agent_jobs.cancelled_by_agent = 0 "
                        . "AND agent_jobs.disapproved_by_client = 0 "
                        . "AND agent_jobs.approved_by_client = 0 ";
            }

            if ($status == "3") {
                $status_condition = "AND agent_jobs.accepted_by_client = 0 "
                        . "AND agent_jobs.declined_by_client = 1 "
                        . "AND agent_jobs.completed_by_agent = 0 "
                        . "AND agent_jobs.cancelled_by_agent = 0 "
                        . "AND agent_jobs.disapproved_by_client = 0 "
                        . "AND agent_jobs.approved_by_client = 0 ";
            }
        } else {
            $status_condition = "";
        }

        $job = $this->AgentJob->query('SELECT agent_ranks.rank_title,agent_jobs.job_id,agent_user_infos.agent_name,agent_user_infos.agent_rank,agent_jobs.created,agent_jobs.accepted_by_client,agent_jobs.declined_by_client,agent_jobs.completed_by_agent,agent_jobs.cancelled_by_agent,agent_jobs.disapproved_by_client,agent_jobs.approved_by_client FROM agent_jobs '
                . 'LEFT JOIN agent_user_infos ON agent_user_infos.agent_id = agent_jobs.agent_id '
                . 'LEFT JOIN job_locations ON job_locations.job_id = agent_jobs.job_id '
                . 'LEFT JOIN agent_ranks ON agent_user_infos.agent_rank = agent_ranks.rank ' . $client_condition . ' ' . $condition_date . ' ' . $condition_location . ' ' . $jobid_condition . ' ' . $rank_condition . ' ' . $status_condition . 'ORDER BY agent_jobs.created DESC');

// $log = $this->AgentJob->getDataSource()->getLog(false, false);
// debug($log);
        return $job;
    }

    public function getjobReqGraphData() {

        $to = NOW;
        $from = date("Y-m-d H:i:s", strtotime("-10 day"));

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE agent_jobs.client_id = " . $client_id;
        /* $current_job_query = "SELECT count(aj.agent_job_id) AS submissionThisDay,DATE(aj.job_end_time) AS submissionDate,j.* FROM agent_jobs aj
          RIGHT JOIN jobs j ON j.job_id=aj.job_id
          WHERE aj.client_id = " . $client_id . " AND aj.accepted_by_client = 0 AND aj.declined_by_client = 0 AND aj.cancelled_by_agent = 0 AND aj.completed_by_agent = 0 AND j.job_end_date >= '" . $from . "' AND j.job_end_date <= '" . $to . "' AND j.is_active = 1 AND j.is_blocked = 0
          GROUP BY DATE(aj.job_end_time)
          ORDER BY submissionDate ASC"; */

        $current_job_query = "SELECT count(aj.agent_job_id) AS requestThisDay,DATE(aj.job_start_time) AS requestDate,j.* FROM agent_jobs aj
								RIGHT JOIN jobs j ON j.job_id=aj.job_id
								WHERE aj.client_id = " . $client_id . " AND aj.accepted_by_client = 0 AND aj.declined_by_client = 0 AND aj.cancelled_by_agent = 0 AND aj.completed_by_agent = 0  AND aj.job_start_time >= '" . $from . "' AND aj.job_start_time <= '" . $to . "'   AND j.is_active = 1 AND j.is_blocked = 0
								GROUP BY requestDate
								ORDER BY requestDate ASC";



        if ($current_jobs = $this->ClientAddress->query($current_job_query)) {

            $currArr = array();
            $currPreArr = array();
            $dayNumber = 0;
            $oldSubmissionDate = '';
            /* $currPreArr[0] = "Location";
              $currPreArr[1] = "Total Requests";
              $currPreArr[2] = "Total Submissions"; */

//array_push($locArr, $locPreArr);
            foreach ($current_jobs as $job_key => $jobs) {
                $job_id = $jobs['j']['job_id'];
                if (!isset($currArr[$job_id])) {
                    $currArr[$job_id] = array();
                }
                $currSubArr = array();
//$currSubArrTemp = array();
                if ($oldSubmissionDate == $jobs[0]['requestDate']) {
                    $dayNumber;
                } else {
                    $dayNumber++;
                }

                $currSubArrTemp['jobId'] = $jobs['j']['job_id'];
                $currSubArrTemp['reqCount'] = $jobs[0]['requestThisDay'];
                $currSubArrTemp['reqDate'] = $jobs[0]['requestDate'];
                $currSubArrTemp['day'] = $dayNumber;

//array_push($currSubArr,$currSubArrTemp);
//pr($currSubArr);
                $currArr[$job_id];
                array_push($currArr[$job_id], $currSubArrTemp);
                $oldSubmissionDate = $jobs[0]['submissionDate'];
            }
            krsort($currArr);
//pr($currArr);exit;	
        }


//$farray = array_merge($header,$farray);
        echo json_encode($currArr, JSON_NUMERIC_CHECK);
        exit;
    }

    public function fetch_report_job_submission() {
        if ($this->request->is('ajax')) {
            $start_date = date('Y-m-d', strtotime($_POST['startdate']));
            $end_date = date('Y-m-d', strtotime($_POST['enddate']));
            $job_id = $_POST['job_id'];
            $status = $_POST['status'];
            $joblocation = $_POST['joblocation'];
            $res = $this->view_reports_job_submission($start_date, $end_date, $job_id, $status, $joblocation);

            $this->set('res', $res);
            $this->set('type', $type);
        } else {
            echo "<table><tr><td>No data found</td><tr></table>";
            exit;
        }
    }

    public function view_reports_job_submission($start_date = '', $end_date = '', $job_id = '', $status = '', $joblocation = '') {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE agent_jobs.client_id = " . $client_id;

        if ($start_date != 0 && $start_date != "" && $start_date != "1970-01-01" && $end_date != "1970-01-01") {
            $condition_date = "AND agent_jobs.job_start_time BETWEEN" . "'" . $start_date . "' AND " . "'" . $end_date . "'";
        } else {
            $condition_date = "";
        }

        if ($joblocation != "" && $joblocation != "n") {
            $condition_location = "AND job_locations.job_id = " . $joblocation;
        } else {
            $condition_location = "";
        }

        if ($job_id != "" && $job_id != "n" && $job_id != "0") {
            $jobid_condition = "AND agent_jobs.job_id = " . $job_id;
        } else {
            $jobid_condition = "";
        }

        if ($status != "" && $status != "n" && $status != "0") {

            if ($status == "1") {
                $status_condition = "AND agent_jobs.accepted_by_client = 1 "
                        . "AND agent_jobs.declined_by_client = 0 "
                        . "AND agent_jobs.completed_by_agent = 1 "
                        . "AND agent_jobs.cancelled_by_agent = 0 "
                        . "AND agent_jobs.disapproved_by_client = 0 "
                        . "AND agent_jobs.approved_by_client = 0 ";
            }

            if ($status == "2") {
                $status_condition = "AND agent_jobs.accepted_by_client = 1 "
                        . "AND agent_jobs.declined_by_client = 0 "
                        . "AND agent_jobs.completed_by_agent = 1 "
                        . "AND agent_jobs.cancelled_by_agent = 0 "
                        . "AND agent_jobs.disapproved_by_client = 0 "
                        . "AND agent_jobs.approved_by_client = 1 ";
            }

            if ($status == "3") {
                $status_condition = "AND agent_jobs.accepted_by_client = 1 "
                        . "AND agent_jobs.declined_by_client = 0 "
                        . "AND agent_jobs.completed_by_agent = 1 "
                        . "AND agent_jobs.cancelled_by_agent = 0 "
                        . "AND agent_jobs.disapproved_by_client = 1 "
                        . "AND agent_jobs.approved_by_client = 0 ";
            }
        } else {
            $status_condition = "";
            $submission_condition = "AND agent_jobs.accepted_by_client = 1 AND agent_jobs.declined_by_client = 0 AND agent_jobs.completed_by_agent = 1 AND agent_jobs.cancelled_by_agent = 0 ";
        }


        $job = $this->AgentJob->query('SELECT agent_jobs.job_id,agent_user_infos.agent_name,agent_jobs.job_start_time,agent_jobs.percentage_earned,agent_jobs.accepted_by_client,agent_jobs.declined_by_client,agent_jobs.completed_by_agent,agent_jobs.cancelled_by_agent,agent_jobs.	disapproved_by_client,agent_jobs.approved_by_client FROM agent_jobs '
                . 'LEFT JOIN agent_user_infos ON agent_user_infos.agent_id = agent_jobs.agent_id '
                . 'LEFT JOIN job_locations ON job_locations.job_id = agent_jobs.job_id ' . $client_condition . ' ' . $condition_date . ' ' . $condition_location . ' ' . $jobid_condition . ' ' . $status_condition . ' ' . $submission_condition . 'ORDER BY agent_jobs.job_start_time DESC');

// $log = $this->AgentJob->getDataSource()->getLog(false, false);
// debug($log);

        return $job;
    }

    public function getjobSubmissionGraphData() {

        $client_id = $this->Session->read('Auth.User.client_id');
        $client_condition = "WHERE agent_jobs.client_id = " . $client_id;
        //$submission_condition = "AND ajj.accepted_by_client = 1 AND ajj.declined_by_client = 0 AND ajj.completed_by_agent = 1 AND ajj.cancelled_by_agent = 0 ";
        $submission_condition = "";
        $job = $this->AgentJob->query("SELECT
                                        (SELECT COUNT('aj.job_id')  FROM agent_jobs aj 
                                        WHERE aj.accepted_by_client = 1 AND aj.declined_by_client = 0 AND aj.completed_by_agent = 1 AND aj.cancelled_by_agent =0 AND aj.approved_by_client = 0 
                                        AND aj.disapproved_by_client = 0 AND aj.job_id=ajj.job_id) AS pendingSubmissions ,

                                        (SELECT COUNT('aj.job_id') FROM agent_jobs aj 
                                        WHERE aj.accepted_by_client = 1 AND aj.declined_by_client = 0 AND aj.completed_by_agent = 1 AND aj.cancelled_by_agent =0 AND aj.approved_by_client = 1 
                                        AND aj.disapproved_by_client = 0 AND aj.job_id=ajj.job_id)  AS acceptedSubmissions ,

                                        (SELECT COUNT('aj.job_id')  FROM agent_jobs aj 
                                        WHERE aj.accepted_by_client = 1 AND aj.declined_by_client = 0 AND aj.completed_by_agent = 1 AND aj.cancelled_by_agent =0 AND aj.approved_by_client = 0 
                                        AND aj.disapproved_by_client = 1 AND aj.job_id=ajj.job_id)  AS declinedSubmissions  ,

                                        ajj.job_id

                                        FROM agent_jobs ajj

                                        WHERE client_id = $client_id" . " " . "$submission_condition

                                        GROUP BY ajj.job_id");


        $farray = array();
        $header = array('JOBS', 'Pending', 'Accepted', 'Declined');
        array_push($farray, $header);
        foreach ($job as $value) {
            $subarray = array();

            array_push($subarray, (int) $value['ajj']['job_id']);
            array_push($subarray, (int) $value['0']['pendingSubmissions']);
            array_push($subarray, (int) $value['0']['acceptedSubmissions']);
            array_push($subarray, (int) $value['0']['declinedSubmissions']);

            array_push($farray, $subarray);
        }


//$farray = array_merge($header,$farray);
        echo json_encode($farray, JSON_NUMERIC_CHECK);
        exit;
    }

    public function add_client_address_test() {
        $count = $_POST['question_count'];
        ?>


        <div id="apply_question<?php echo $count; ?>" >
            <h2 class="page-header">Location 1</h2>
            <div class="form-group">
                <label class="col-sm-2 control-label">Location:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control addrs" id="address"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Radius:</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control rad" id="radius"/>
                </div>
            </div>
            <div id="us3"  class="map" style="width: 550px; height: 400px;"></div>
            <div class="clearfix">&nbsp;</div>
            <div class="m-t-small">
                <label class="p-r-small col-sm-1 control-label">Lat.:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control latt" style="width: 110px" id="lat"/>
                </div>
                <label class="p-r-small col-sm-2 control-label">Long.:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control long" style="width: 110px" id="lon"/>
                </div>
            </div>
        </div> 
        <script type="text/javascript">

            $(".map").locationpicker({
                location: {latitude: 46.15242437752303, longitude: 2.7470703125},
                radius: 300,
                inputBinding: {
                    latitudeInput: $('.latt'),
                    longitudeInput: $('.long'),
                    radiusInput: $('.rad'),
                    locationNameInput: $('.addrs')
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
        </script>
        <?php
        exit;
    }

    public function survey_category() {
        $this->layout = 'client';

        $client_id = $this->Session->read('Auth.User.client_id');

        $test_q = $this->SurveyCategories->find('all', array('fields' => array(), 'conditions' => array('SurveyCategories.is_blocked' => 0, 'SurveyCategories.is_active' => 1, 'SurveyCategories.client_id' => $client_id), 'recursive' => 1));

        $this->set('listing', $test_q);
    }

    function edit_surveycategory($survey_category_id = null) {

        $this->layout = 'client';

        $ID = $survey_category_id;

        $client_id = $this->Session->read('Auth.User.client_id');

        if ($this->request->is('post')) {

            $survey_category_id = $this->request->data('survey_category_id');
            $survey_category_title = $this->request->data('survey_category_title');
//pr($this->request->data); pr($_FILES); exit;
            $extension_allow = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
            $target_dir = WEBSITE_WEBROOT . 'media/img/';
            if (isset($_FILES["survey_category_image"]["name"]) && !empty($_FILES["survey_category_image"]["name"])) {
                $old_image = $this->request->data('survey_category_image_old');

                @unlink($target_dir . $old_image);

                $pathi = "";

                $microtime = microtime();

                $fileName = $_FILES["survey_category_image"]["name"];
                $extension = explode(".", $fileName);
                $Path = "";

                if (in_array($extension[1], $extension_allow)) {
                    $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];

                    $target_dir2 = $target_dir . $Pathi;
                    move_uploaded_file($_FILES["survey_category_image"]["tmp_name"], $target_dir2);
                }

                $survey_category_image = $Pathi;
            } else {
                $survey_category_image = $this->request->data('survey_category_image_old');
            }

            $query = $this->SurveyCategories->query("UPDATE survey_categories SET survey_category_title='$survey_category_title',survey_category_image='$survey_category_image' WHERE survey_category_id = '$survey_category_id' AND client_id='$client_id'");

//pr($query); if($query || empty($query)) { echo "success"; } else { echo "failed"; } exit;

            if (isset($query)) {

                $this->Session->setFlash('Survey category updated successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'survey_category'));
                exit();
            } else {
                $this->Session->setFlash('Unable to update Survey category.', 'default', array('class' => 'errormsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'survey_category'));
                exit();
            }
        }

        $data = $this->SurveyCategories->find('all', array('conditions' => array('survey_category_id' => $ID)));
//pr($data);exit;
        $this->set('singlerecord', $data);
    }

    function category_add() {
        $client_id = $this->Session->read('Auth.User.client_id');
        if ($this->request->is('post')) {
            $post = $this->request->data;

            $extension_allow = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
            $target_dir = WEBSITE_WEBROOT . 'media/img/';
            if (isset($_FILES["survey_category_image"]["name"]) && !empty($_FILES["survey_category_image"]["name"])) {
                $pathi = "";

                $microtime = microtime();

                $fileName = $_FILES["survey_category_image"]["name"];
                $extension = explode(".", $fileName);
                $Path = "";

                if (in_array($extension[1], $extension_allow)) {
                    $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];

                    $target_dir2 = $target_dir . $Pathi;
                    move_uploaded_file($_FILES["survey_category_image"]["tmp_name"], $target_dir2);
                }

                $data['survey_category_image'] = $Pathi;
            }

            $data['client_id'] = $client_id;
            $data['survey_category_title'] = $post['survey_category_title'];
            $data['is_active'] = '1';
//pr($data);exit;
            if ($this->SurveyCategories->save($data)) {

                $this->Session->setFlash('Survey category added successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'survey_category'));
            } else {

                $this->Session->setFlash('Error ! Unable to add survey category.please try again.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'survey_category'));
            }
        }

        $this->layout = 'client';
    }

    function delete_surveycategory($survey_category_id = null) {

        $status = '0';

        $this->SurveyCategories->id = $survey_category_id; //exit;

        if ($this->SurveyCategories->saveField('is_active', $status)) {

            $this->Session->setFlash('Survey Category deleted successfully.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'survey_category'));
            exit();
        } else {

            $this->Session->setFlash('Error ! Unable to delete Survey Category.Please Try Again.', 'default', array('class' => 'errormsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'survey_category'));
            exit();
        }
    }

    public function change_request_status_from_dash() {
        $agent_job_id = $_REQUEST['agent_job_id'];
        $activity = $_REQUEST['activity'];
        $this->AgentJob->id = $agent_job_id;
        $data['agent_job_id'] = $agent_job_id;
        if ($activity == 'A') {
            $data['accepted_by_client'] = 1;
            $data['declined_by_client'] = 0;
        } elseif ($activity == 'D') {
            $data['accepted_by_client'] = 0;
            $data['declined_by_client'] = 1;
        }

        if ($this->AgentJob->save($data)) {
            echo true;
        } else {
            echo false;
        }
        exit;
    }

    public function payments() {
//error_reporting(E_ALL);

        $client_id = $this->Session->read('Auth.User.client_id');
        $conditon = array('AgentJob.client_id' => $client_id, 'AgentJob.accepted_by_client' => 1,
            'AgentJob.declined_by_client' => 0, 'AgentJob.completed_by_agent' => 1,
            'AgentJob.cancelled_by_agent' => 0, 'AgentJob.disapproved_by_client' => 0,
            'AgentJob.approved_by_client' => 1);
        $listings = $this->AgentJob->find('all', array('conditions' => $conditon));
//pr($listings); exit;

        foreach ($listings as $listing) {
            if ($listing['Job']['reward_type'] == '2') {
                $coupon_id = $listing['Job']['coupon_id'];

                $data_coupon = $this->Coupon->find('all', array('conditions' => array('Coupon.coupon_id' => $coupon_id), 'recursive' => -1));

                $coupon_name[][$listing['AgentJob']['agent_job_id']] = $data_coupon[0]['Coupon']['coupon_name'];
            }
        }
//pr($coupon_name);
//exit;
        $this->set('listings', $listings);
        $this->set('coupon_name', $coupon_name);
        $this->layout = 'client';
    }

    public function payByCoupon() {

        if (!empty($_FILES['coupon_image']['name'])) {
            $imageExt = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG','PDF');
            $pathi = "";
            $target_dir = WEBSITE_WEBROOT . 'media/img/';
            $microtime = microtime();
            $fileName = $_FILES['coupon_image']["name"];
            $extension = explode(".", $fileName);
            $Path = "";

            if (in_array($extension[1], $imageExt)) {
                $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                $target_dir2 = $target_dir . $Pathi;
                $img = move_uploaded_file($_FILES['coupon_image']["tmp_name"], $target_dir2);

                if ($img == 1) {
                    $couponDetais = $this->Coupon->query("select * from coupons where coupon_id =" . $_POST['coupon_id'] . "");
                    if ($couponDetais[0]['coupons']['coupon_name'] == "") {
                        $couponDetais = $this->Coupon->query("update coupons set coupon_name = '$_POST[coupon_name]', coupon_description = '$_POST[coupon_description]' ,coupon_image = '$fileName' ,"
                                . "coupon_expiration_date = " ."'". date('Y-m-d h:i:s' , strtotime( $_POST[coupon_expiration_date])) ."'". ",modified = " ."'". date("Y-m-d h:i:s") ."'". ", agent_id= $_POST[agent_id] where coupon_id = $_POST[coupon_id]");
                    } else {
                        $details = array("coupon_name" => $_POST['coupon_name'],
                            "coupon_description" => $_POST['coupon_description'],
                            "coupon_image" => $fileName,
                            "coupon_expiration_date" => $_POST[coupon_expiration_date],
                            "modified" => date("Y-m-d h:i:s"),
                            "agent_id" => $_POST['created'],
                            "coupon_price" => $couponDetais[0]['coupons']['coupon_price'],
                            "is_active" => 1,
                            "is_blocked" => 0
                        );
                        $couponDetais = $this->Coupon->save($details);
                    }

                    $this->AgentJob->query("update agent_jobs set is_paid = 1,amount_paid = $_POST[price] ,payment_time = " ."'". date("Y-m-d h:i:s") ."'". " where agent_job_id = $_POST[agent_job_id]");
                    
                    $this->Session->setFlash('Coupon added successfully.', 'default', array('class' => 'successmsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'payments'));
                    exit();
                } else {
                    $this->Session->setFlash('Error ! Unable to add coupon.Please Try Again.', 'default', array('class' => 'errormsg'));
                    $this->redirect(array('controller' => 'clients', 'action' => 'payments'));
                    exit();
                }
            } else {
                $this->Session->setFlash('Error ! Invalid image type.', 'default', array('class' => 'errormsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'payments'));
                exit();
            }
        } else {
            $this->Session->setFlash('Error ! Select Coupon.', 'default', array('class' => 'errormsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'payments'));
            exit();
        }

        exit;
    }

    public function mark_as_paid() {
        $data['agent_job_id'] = $_POST['agent_job_id'];
        $reward_type = $_POST['reward_type'];
        $job_compensation = $_POST['job_compensation'];
        $coupon_id = $_POST['coupon_id'];

//echo $job_compensation; exit;

        $data['is_paid'] = '1';
        $data['payment_time'] = date("Y-m-d H:i:s");

        if ($reward_type == '1') {
            $data['amount_paid'] = $job_compensation;
        } else if ($reward_type == '2') {
            $data['coupon_id'] = $coupon_id;
        }

        if ($this->AgentJob->save($data)) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    public function printPdf() {
        $agent_job_id = $this->params['pass']['0'];

        $client_id = $this->Session->read('Auth.User.client_id');
        $job_submission = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_job_id' => $agent_job_id), 'recursive' => 1));
        $agent_id = $job_submission[0]['AgentJob']['agent_id'];
        $agent = $this->AgentUserInfo->find('all', array('fields' => array('AgentUserInfo.*'), 'conditions' => array('AgentUserInfo.agent_id' => $agent_id), 'recursive' => -1));
        $survey_categories = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'group' => 'AgentSurveyAnswer.survey_category_id', 'conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id), 'recursive' => -1));

        foreach ($survey_categories as $survey_category) {

            $survey_category_id = $survey_category['AgentSurveyAnswer']['survey_category_id'];

            $survey_category_ids[] = $survey_category_id;

            $category_list_singles[$survey_category_id] = $this->SurveyCategory->find('all', array('fields' => array(), 'conditions' => array('SurveyCategory.survey_category_id' => $survey_category_id), 'recursive' => -1));

            $survey_questions[$survey_category_id] = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'group' => 'AgentSurveyAnswer.survey_question_id', 'conditions' => array('AgentSurveyAnswer.survey_category_id' => $survey_category_id, 'AgentSurveyAnswer.agent_id' => $agent_id), 'recursive' => -1));

            foreach ($survey_questions[$survey_category_id] as $survey_question) {
                $survey_question_id = $survey_question['AgentSurveyAnswer']['survey_question_id'];

                $survey_question = $this->SurveyQuestion->find('all', array('fields' => array(), 'conditions' => array('SurveyQuestion.survey_question_id' => $survey_question_id), 'recursive' => 1));

                $survey_questions_all[$survey_category_id][] = $survey_question[0];

                $data[][$survey_question_id] = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'conditions' => array('AgentSurveyAnswer.survey_question_id' => $survey_question_id), 'recursive' => 1));
            }
        }

        ksort($survey_questions_all);

        $agent;
        /*  echo "<pre>";
          pr($category_list_singles);
          pr($data);
          pr($survey_questions_all);
          exit(); */
        $job_submission;
        $survey_category_ids;
        $category_list_singles;
        $data;
        $survey_questions_alls = $survey_questions_all;

        /*         * ***************** Agent Data for pdf ************************ */

        if (!empty($agent['0']['AgentUserInfo']['profile_image'])) {
            $img = WEBSITE_ROOT . 'media/img/' . $agent['0']['AgentUserInfo']['profile_image'];
        } else {
            $img = WEBSITE_ROOT . 'images/prof-pic.jpg';
        }
        $agent_name = $agent['0']['AgentUserInfo']['agent_name'];
        $agent_gender = $agent['0']['AgentUserInfo']['gender'];
        if ($agent_gender == "M") {
            $agent_gender = "Male";
        } else {
            $agent_gender = "Female";
        }
        $job_id = $job_submission['0']['AgentJob']['job_id'];
        $submitted_on = date('j M Y , h:m:s', strtotime($job_submission['0']['AgentJob']['created']));

        $request_status = '';

        if ($job_submission['0']['AgentJob']['completed_by_agent'] == 1 && $job_submission['0']['AgentJob']['disapproved_by_client'] == 0 && $job_submission['0']['AgentJob']['approved_by_client'] == 0) {
            $request_status = 'Pending Approval';
        } elseif ($job_submission['0']['AgentJob']['completed_by_agent'] == 1 && $job_submission['0']['AgentJob']['disapproved_by_client'] == 1 && $job_submission['0']['AgentJob']['approved_by_client'] == 0) {
            $request_status = 'Disapproved';
        } elseif ($job_submission['0']['AgentJob']['completed_by_agent'] == 1 && $job_submission['0']['AgentJob']['disapproved_by_client'] == 0 && $job_submission['0']['AgentJob']['approved_by_client'] == 1) {
            $request_status = 'Approved';
        }

        $i = 1;


        /*         * ***************** Agent Data for pdf ************************ */

        $pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetFont('helvetica', '', 15);
        $pdf->AddPage();

        $html = '<html><body>'
                . '<div style="text-align:center;font-size:24px;border:dotted;">CUSTOMER SERVICE ASSESSMENT</div>'
                . '<div style="border:dotted;"><br/>'
                . '<table>'
                . '<tr align="center"><td width="250"><b>Agent Name</b> : ' . $agent_name . '</td>'
                . '<td width="380"><b>Submitted ON</b> : ' . $submitted_on . '</td>'
                . '</tr>'
                . '<tr align="center"><td width="200"></td></tr>'
                . '<tr align="center">'
                . '<td width="200"><b>Agent Gender</b> : ' . $agent_gender . '</td>'
                . '<td width="240"><b>Status</b> : ' . $request_status . '</td>'
                . '<td width="200"><b>Job Id</b> : ' . $job_id . '</td>'
                . '</tr>'
                . '</table></div>'
                . '<div>'
                . '</div>';
        $html .= "*Selected answers are highlighted in blue. <br/>";
        foreach ($survey_questions_alls as $key => $survey_questions_all) {
            foreach ($survey_questions_all as $question_list) {
                $cat = $question_list['SurveyCategory']['survey_category_title'];
                $aryData1 = array();
                $aryData2 = array();
                foreach ($data as $data_select) {
                    foreach ($data_select[$question_list['SurveyQuestion']['survey_question_id']] as $data_select1) {
                        $aryData1[] = $data_select1['AgentSurveyAnswer']['survey_question_answer'];
                        $aryData2 = $aryData1;
                    }
                }

                if ($i == 1) {
                    $html .= '<div style="font-size:26px;"><b><u>' . $question_list['SurveyCategory']['survey_category_title'] . '</u></b></div>';
                    $catlist = $question_list['SurveyCategory']['survey_category_title'];
                }

                if ($cat != $catlist) {
                    $html .= '<div style="font-size:26px;"><b><u>' . $cat . '</u></b></div>';
                    $catlist = $cat;
                }
                $html .= "<br/><br/>";
                $html .= $i . " . " . $question_list['SurveyQuestion']['survey_question_description'] . "";
                $html .= "<ul>";

                if ($question_list['SurveyQuestion']['survey_question_type'] == 'C') {
                    foreach ($question_list['SurveyQuestionOption'] as $question_option) {
                        if (in_array($question_option['survey_option_id'], $aryData2)) {
                            $html .= '<li style="color:blue;">' . $question_option['answer_text'] . '<br/></li>';
                        } else {
                            $html .= '<li style="color:black;">' . $question_option['answer_text'] . '<br/></li>';
                        }
                    }
                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'R') {
                    foreach ($question_list['SurveyQuestionOption'] as $question_option) {
                        if (in_array($question_option['survey_option_id'], $aryData2)) {
                            $html .= '<li style="color:blue;">' . $question_option['answer_text'] . '<br/></li>';
                        } else {
                            $html .= '<li style="color:black;">' . $question_option['answer_text'] . '<br/></li>';
                        }
                    }
                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'T') {

                    foreach ($aryData2 as $aryData) {
                        $html .= "<li>" . $aryData . "</li><br/>";
                    }
                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'I') {

                    foreach ($aryData2 as $aryData) {
                        $path = WEBSITE_ROOT . 'media/img/' . $aryData;
                        $html .= '<img src=' . '"' . $path . '"' . ' alt="" width="100" height="100"  />';
                    }
                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'A') {
                    $html .= "<li>" . "Audio Clip<br/></li>";
                }
                $html .= "</ul>";
                $i++;
            }
        }



        $html .= '</body></html>';

        /* foreach ($posts as $post) {
          $html .= '' . $post['Post']['title'];
          } */

        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();
        $data = $pdf->Output('SubmissionReport.pdf', 'D');
    }

    public function printExcel() {

        $agent_job_id = $this->params['pass']['0'];

        $client_id = $this->Session->read('Auth.User.client_id');
        $job_submission = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_job_id' => $agent_job_id), 'recursive' => 1));
        $agent_id = $job_submission[0]['AgentJob']['agent_id'];
        $agent = $this->AgentUserInfo->find('all', array('fields' => array('AgentUserInfo.*'), 'conditions' => array('AgentUserInfo.agent_id' => $agent_id), 'recursive' => -1));
        $survey_categories = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'group' => 'AgentSurveyAnswer.survey_category_id', 'conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id), 'recursive' => -1));

        foreach ($survey_categories as $survey_category) {

            $survey_category_id = $survey_category['AgentSurveyAnswer']['survey_category_id'];

            $survey_category_ids[] = $survey_category_id;

            $category_list_singles[$survey_category_id] = $this->SurveyCategory->find('all', array('fields' => array(), 'conditions' => array('SurveyCategory.survey_category_id' => $survey_category_id), 'recursive' => -1));

            $survey_questions[$survey_category_id] = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'group' => 'AgentSurveyAnswer.survey_question_id', 'conditions' => array('AgentSurveyAnswer.survey_category_id' => $survey_category_id, 'AgentSurveyAnswer.agent_id' => $agent_id), 'recursive' => -1));

            foreach ($survey_questions[$survey_category_id] as $survey_question) {
                $survey_question_id = $survey_question['AgentSurveyAnswer']['survey_question_id'];

                $survey_question = $this->SurveyQuestion->find('all', array('fields' => array(), 'conditions' => array('SurveyQuestion.survey_question_id' => $survey_question_id), 'recursive' => 1));

                $survey_questions_all[$survey_category_id][] = $survey_question[0];

                $data[][$survey_question_id] = $this->AgentSurveyAnswer->find('all', array('fields' => array(), 'conditions' => array('AgentSurveyAnswer.survey_question_id' => $survey_question_id), 'recursive' => 1));
            }
        }

        ksort($survey_questions_all);

        $agent;
        /*  echo "<pre>";
          pr($category_list_singles);
          pr($data);
          pr($survey_questions_all);
          exit(); */
        $job_submission;
        $survey_category_ids;
        $category_list_singles;
        $data;
        $survey_questions_alls = $survey_questions_all;

        /*         * ***************** Agent Data for pdf ************************ */

        if (!empty($agent['0']['AgentUserInfo']['profile_image'])) {
            $img = WEBSITE_ROOT . 'media/img/' . $agent['0']['AgentUserInfo']['profile_image'];
        } else {
            $img = WEBSITE_ROOT . 'images/prof-pic.jpg';
        }
        $agent_name = $agent['0']['AgentUserInfo']['agent_name'];
        $agent_gender = $agent['0']['AgentUserInfo']['gender'];
        if ($agent_gender == "M") {
            $agent_gender = "Male";
        } else {
            $agent_gender = "Female";
        }
        $job_id = $job_submission['0']['AgentJob']['job_id'];
        $submitted_on = date('j M Y , h:m:s', strtotime($job_submission['0']['AgentJob']['created']));

        $request_status = '';

        if ($job_submission['0']['AgentJob']['completed_by_agent'] == 1 && $job_submission['0']['AgentJob']['disapproved_by_client'] == 0 && $job_submission['0']['AgentJob']['approved_by_client'] == 0) {
            $request_status = 'Pending Approval';
        } elseif ($job_submission['0']['AgentJob']['completed_by_agent'] == 1 && $job_submission['0']['AgentJob']['disapproved_by_client'] == 1 && $job_submission['0']['AgentJob']['approved_by_client'] == 0) {
            $request_status = 'Disapproved';
        } elseif ($job_submission['0']['AgentJob']['completed_by_agent'] == 1 && $job_submission['0']['AgentJob']['disapproved_by_client'] == 0 && $job_submission['0']['AgentJob']['approved_by_client'] == 1) {
            $request_status = 'Approved';
        }





        /*         * ********** code for creating excel *************** */
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->mergeCells('A1:C2');


        $sheet = $objPHPExcel->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true,
                'size' => '16px'
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $styleArrayDetails = array(
            'font' => array(
                'bold' => true
            ),
        );

        $sheet->setCellValue('A1', 'CUSTOMER SERVICE ASSESSMENT');
        $sheet->getStyle('A1:C2')->applyFromArray($styleArray);

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A4", "Agent Name : $agent_name")
                ->setCellValue("B4", "Agent Gender : $agent_gender")
                ->setCellValue("C4", "Submitted ON : $submitted_on")
                ->setCellValue("A6", "Status : $request_status")
                ->setCellValue("B6", "Job Id : $job_id");

        $sheet->getStyle('A4')->applyFromArray($styleArrayDetails);
        $sheet->getStyle('B4')->applyFromArray($styleArrayDetails);
        $sheet->getStyle('C4')->applyFromArray($styleArrayDetails);
        $sheet->getStyle('A6')->applyFromArray($styleArrayDetails);
        $sheet->getStyle('B6')->applyFromArray($styleArrayDetails);

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A8", "*Selected answers are highlighted in bold.");


        $count = 10;

        foreach (range('A', 'Z') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }


        foreach ($survey_questions_alls as $key => $survey_questions_all) {
            foreach ($survey_questions_all as $question_list) {
                $cat = $question_list['SurveyCategory']['survey_category_title'];
                $aryData1 = array();
                $aryData2 = array();
                foreach ($data as $data_select) {
                    foreach ($data_select[$question_list['SurveyQuestion']['survey_question_id']] as $data_select1) {
                        $aryData1[] = $data_select1['AgentSurveyAnswer']['survey_question_answer'];
                        $aryData2 = $aryData1;
                    }
                }

                if ($count == 8) {
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A$count", $question_list['SurveyCategory']['survey_category_title']);
                    $catlist = $question_list['SurveyCategory']['survey_category_title'];
                    $sheet->getStyle("A$count")->applyFromArray($styleArrayDetails);
                    $count ++;
                    $count ++;
                }

                if ($cat != $catlist) {
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A$count", $cat);
                    $sheet->getStyle("A$count")->applyFromArray($styleArrayDetails);
                    $count ++;
                    $count ++;
                    $catlist = $cat;
                }

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$count", "Que : " . $question_list['SurveyQuestion']['survey_question_description']);
                $count ++;

                if ($question_list['SurveyQuestion']['survey_question_type'] == 'C') {
                    foreach ($question_list['SurveyQuestionOption'] as $question_option) {
                        if (in_array($question_option['survey_option_id'], $aryData2)) {
                            $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValue("A$count", "Ans : " . $question_option['answer_text']);
                            $sheet->getStyle("A$count")->applyFromArray($styleArrayDetails);
                        } else {
                            $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValue("A$count", "Ans : " . $question_option['answer_text']);
                        }


                        $count ++;
                    }
                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'R') {
                    foreach ($question_list['SurveyQuestionOption'] as $question_option) {
                        if (in_array($question_option['survey_option_id'], $aryData2)) {
                            $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValue("A$count", "Ans : " . $question_option['answer_text']);
                            $sheet->getStyle("A$count")->applyFromArray($styleArrayDetails);
                        } else {
                            $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValue("A$count", "Ans : " . $question_option['answer_text']);
                        }


                        $count ++;
                    }
                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'T') {

                    foreach ($aryData2 as $aryData) {
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue("A$count", "Ans : " . $aryData);
                        $count ++;
                    }
                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'I') {

                    foreach ($aryData2 as $aryData) {
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue("A$count", "Ans : " . "Image File");
                        $count ++;
                    }
                } else if ($question_list['SurveyQuestion']['survey_question_type'] == 'A') {
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A$count", "Ans : " . "Audio Clip");
                    $count ++;
                }
                $count ++;
            }
        }





// Miscellaneous glyphs, UTF-8
// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Report.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function my_payments() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $subs = $this->ClientSubscription->find('all', array('conditions' => array('client_id' => $client_id), 'order' => array('created DESC')));
        $this->set('subs', $subs);
        $this->layout = "client";
    }

    public function tap($amount = "") {

        if ($amount == "") {
            $id = $_SESSION['Auth']['User']['client_id'];
            $getAmount = $this->ClientSubscription->query("Select amount_paid from client_subscriptions where client_id = $id and transaction_status = 'pending'");
            $amount = $getAmount[0]['client_subscriptions']['amount_paid'];
        }

        $apiurl = 'http://live.gotapnow.com/webservice/PayGatewayService.svc';  // Development Url
        //$apiurl = 'https://www.gotapnow.com/webservice/PayGatewayService.svc';  // Production Url
        //Customer Details
        $Mobile = $_SESSION['Auth']['User']['mobile_number'];  //Customer Mobile Number
        $CstName = $_SESSION['Auth']['User']['client_name']; //Customer Name
        $Email = $_SESSION['Auth']['User']['email_address'];  //Customer Email Address         
        //Merchant Details
        $ref = "45870225000";  //Your ReferenceID or Order ID
        $MerchantID = "13014";  //Merchant ID Provided by Tap
        $UserName = "test";   //Merchant UserName Provided by Tap
        $ReturnURL = "http://localhost/emissary/Clients/tapReturn";  //After Payment success, customer will be redirected to this url
        $PostURL = "http://localhost/emissary/Clients/tapReturn";  //After Payment success, Payment Data's will be posted to this url
        //Product Details
        $CurrencyCode = "KWD";  //Order Currency Code
        $Total = $amount;  //Total Order Amount
        //Generating the Hash string
        $APIKey = "tap1234";  //Merchant API Key Provided by Tap
        $str = 'X_MerchantID' . $MerchantID . 'X_UserName' . $UserName . 'X_ReferenceID' . $ref . 'X_Mobile' . $Mobile . 'X_CurrencyCode' . $CurrencyCode . 'X_Total' . $Total . '';
        $hashstr = hash_hmac('sha256', $str, $APIKey);
        //echo $hashstr;
        $action = "http://tempuri.org/IPayGatewayService/PaymentRequest";

        $soap_apirequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:tap="http://schemas.datacontract.org/2004/07/Tap.PayServiceContract">
   <soapenv:Header/>
   <soapenv:Body>
      <tem:PaymentRequest>
         <tem:PayRequest>
            <tap:CustomerDC>
               <tap:Email>' . $Email . '</tap:Email>
               <tap:Mobile>' . $Mobile . '</tap:Mobile>
               <tap:Name>' . $CstName . '</tap:Name>
            </tap:CustomerDC>
            <tap:MerMastDC>
               <tap:AutoReturn>Y</tap:AutoReturn>
               <tap:HashString>' . $hashstr . '</tap:HashString>
               <tap:MerchantID>' . $MerchantID . '</tap:MerchantID>
               <tap:ReferenceID>' . $ref . '</tap:ReferenceID>
               <tap:ReturnURL>' . $ReturnURL . '</tap:ReturnURL>
			   <tap:PostURL>' . $PostURL . '</tap:PostURL>
               <tap:UserName>' . $UserName . '</tap:UserName>
            </tap:MerMastDC>
            <tap:lstProductDC>
               <tap:ProductDC>
                  <tap:CurrencyCode>' . $CurrencyCode . '</tap:CurrencyCode>
                  <tap:Quantity>1</tap:Quantity>
                  <tap:TotalPrice>' . $Total . '</tap:TotalPrice>
                  <tap:UnitName>Order - ' . $ref . '</tap:UnitName>
                  <tap:UnitPrice>' . $Total . '</tap:UnitPrice>
               </tap:ProductDC>
            </tap:lstProductDC>
         </tem:PayRequest>
      </tem:PaymentRequest>
   </soapenv:Body>
</soapenv:Envelope>';

        $apiheader = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: " . strlen($soap_apirequest),
        );


        // The HTTP headers for the request (based on image above)
        $apiheader = array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($soap_apirequest),
            'SOAPAction: ' . $action
        );

        // Build the cURL session
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiurl);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $apiheader);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $soap_apirequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        // Send the request and check the response
        if (($result = curl_exec($ch)) === FALSE) {
            die('cURL error: ' . curl_error($ch) . "<br />\n");
        } else {
            //  echo "<br />Success!<br />\n";
        }
        curl_close($ch);

        $xmlobj = simplexml_load_string($result);
        $xmlobj->registerXPathNamespace('a', 'http://schemas.datacontract.org/2004/07/Tap.PayServiceContract');
        $xmlobj->registerXPathNamespace('i', 'http://www.w3.org/2001/XMLSchema-instance');

        $PaymentURL = "";
        $result = $xmlobj->xpath('//a:ReferenceID/text()');
        if (is_array($result)) {
            foreach ($result as $temp) {
                //  echo "<br>ReferenceID : " . $temp;
                $ReferenceID = $temp;
            }
        }

        $result = $xmlobj->xpath('//a:ResponseCode/text()');
        if (is_array($result)) {
            foreach ($result as $temp) {
                //echo "<br>ResponseCode : " . $temp;
            }
        }

        $result = $xmlobj->xpath('//a:ResponseMessage/text()');
        if (is_array($result)) {
            foreach ($result as $temp) {
                // echo "<br>ResponseMessage : " . $temp;
                $ResponseMessage = $temp;
            }
        }

        $result = $xmlobj->xpath('//a:PaymentURL/text()');
        if (is_array($result)) {
            foreach ($result as $temp) {
                //echo "<br>PaymentURL : " . $temp;
                $PaymentURL = $temp;
            }
        }


        header('Location: ' . $PaymentURL);
        die();
    }

    public function tapReturn() {

        $APIKey = 'tap1234';
        $Mobile = $_SESSION['Auth']['User']['mobile_number'];  //Customer Mobile Number
        $CstName = $_SESSION['Auth']['User']['client_name']; //Customer Name
        $Email = $_SESSION['Auth']['User']['email_address'];  //Customer Email Address
        //Merchant Details
        $ref = "45870225000";  //Your ReferenceID or Order ID
        $MerchantID = "13014";  //Merchant ID Provided by Tap
        $UserName = "test";   //Merchant UserName Provided by Tap
        $ReturnURL = "http://localhost/emissary/Clients/tapReturn";  //After Payment success, customer will be redirected to this url
        $PostURL = "http://localhost/emissary/Clients/tapReturn";    //After Payment success, Payment Data's will be posted to this url
        //echo "<br>".$MerchantID;
        //echo "<br>".$ref;
        //$mstr = 'X_ACCOUNT_ID13014X_REF'.$_REQUEST['ref'].'X_RESULT'.$_REQUEST['result'].'X_ReferenceID'.$_REQUEST['trackid'].'';
        $mstr = 'x_account_id' . $MerchantID . 'x_ref' . $_REQUEST['ref'] . 'x_result' . $_REQUEST['result'] . 'x_referenceid' . $_REQUEST['trackid'] . '';
        $Mhashstr = hash_hmac('sha256', $mstr, $APIKey);
        //echo "<br />".$_REQUEST['hash'];
        //echo "<br>".$Mhashstr;

        $payid = $_REQUEST['payid'];
        $crdtype = $_REQUEST['crdtype'];
        $amt = $_REQUEST['amt'];
        $trackid = $_REQUEST['trackid'];
        $crd = $_REQUEST['crd'];

        if ($Mhashstr == $_REQUEST['hash']) {

            $paypal_data = $_REQUEST;
            $status = $paypal_data['result'];
            $txnid = $paypal_data['payid'];
            //Rechecking the product price and currency details
            if ($status == 'SUCCESS') {

                $date = date("Y-m-d h:i:s");
                $cid = $_SESSION['Auth']['User']['client_id'];
                $sub_date = date('Y-m-d h:i:s', strtotime('+30 days'));

                $this->ClientSubscription->query("update client_subscriptions set transaction_id = $payid,transaction_status='Completed',transaction_time = '$date',modified = '$date' where client_id = $cid AND transaction_status = 'pending'");
                $this->ClientUserInfo->query("update client_user_infos set subscription_expiration = '$sub_date' where client_id = $cid ");

                $this->Session->setFlash('Payment Done Successfully.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'my_payments'));
            } else {
                $this->Session->setFlash('Something went wrong try again.', 'default', array('class' => 'successmsg'));
                $this->redirect(array('controller' => 'clients', 'action' => 'my_payments'));
            }
        } else {
            $this->Session->setFlash('Something went wrong try again.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'my_payments'));
            //$this->set('msg', $meg_display);
        }
    }

    public function alerts() {
        $client_id = $this->Session->read('Auth.User.client_id');
        $add = $this->ClientAddress->query("Select * from client_address where client_id = $client_id");
        //$job = $this->Job->query("Select * from jobs where client_id = $client_id");
        $cat = $this->SurveyCategory->query("Select * from survey_categories  where client_id = $client_id");
        $this->set('add', $add);
        //$this->set('job', $job);
        $this->set('cat', $cat);
        $this->layout = "client";
    }

    public function save_alert() {

        $client_id = $this->Session->read('Auth.User.client_id');
        $data['client_id'] = $client_id;
        $data['alert_name'] = $_POST['alert_name'];
        $data['alert_type'] = $_POST['alert_type'];
        $data['alert_action'] = $_POST['alert_action'];
        $data['location'] = isset($_POST['location']) ? $_POST['location'] : 0;
        $data['job'] = isset($_POST['job']) ? json_encode($_POST['job']) : 0;
        $data['category'] = isset($_POST['cat']) ? json_encode($_POST['cat']) : 0;
        $data['question'] = isset($_POST['que']) ? $_POST['que'] : 0;
        $data['answer'] = isset($_POST['ans']) ? $_POST['ans'] : 0;
        $data['req'] = $_POST['req'];
        $data['alert_content'] = $_POST['alert_content'];
        $data['created_date'] = date("Y-m-d");

        if ($this->ClientAlerts->save($data)) {
            $this->Session->setFlash('Alert created successfully.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'accountSettings#tab-2'));
        } else {

            $this->Session->setFlash('Error ! Unable to create alert.please try again.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'alerts'));
        }
    }

    public function get_questions() {
        $id = implode(",", $_POST['ids']);
        $que = $this->SurveyQuestion->query("Select * from survey_questions where survey_category_id IN ($id)");
        $html = "";
        $html .= "<label style='margin-left: 0px;'>Select Question</label><br> <select class='form-control' name='que' id='quesel' style='width:100%;'><option>Select Question</option>";
        foreach ($que as $val) {
            if ($val['survey_questions']['survey_question_type'] == "C" || $val['survey_questions']['survey_question_type'] == "R") {
                $html .= "<option class='dropdown1' value=" . $val['survey_questions']['survey_question_id'] . ">" . $val['survey_questions']['survey_question_description'] . "</option>";
            }
        }
        $html .= "</select>";
        echo $html;
        exit;
    }

    public function get_ans() {
        $id = $_POST['ids'];
        $ans = $this->SurveyQuestion->query("Select survey_questions.survey_question_type,survey_question_options.survey_option_id,survey_question_options.answer_text from survey_questions LEFT JOIN survey_question_options ON survey_questions.survey_question_id = survey_question_options.survey_question_id where survey_question_options.survey_question_id = $id");

        $html = "";
        $html .= "<label style='margin-left: 0px;'>Select Answer</label><br>";
        foreach ($ans as $val) {
            if ($val['survey_questions']['survey_question_type'] == "C" || $val['survey_questions']['survey_question_type'] == "R") {
                $html .= "<input class='answer' data_type='r' type='radio' name='ans' value=" . $val['survey_question_options']['survey_option_id'] . ">" . $val['survey_question_options']['answer_text'] . "<br>";
            }
        }
        echo $html;
        exit;
    }

    public function get_job() {
        $id = $_POST['ids'];
        $job = $this->ClientAddress->query("Select * from client_address LEFT JOIN job_locations ON client_address.client_address_id = job_locations.client_address_id LEFT JOIN jobs ON job_locations.job_id = jobs.job_id where job_locations.client_address_id  = $id");
        $html = "";
        foreach ($job as $value) {
            if ($value['jobs']['job_id'] != "") {
                $html .= "<input type='checkbox' name='job[]' value=" . $value['jobs']['job_id'] . ">" . $value['jobs']['job_title'] . "<br>";
            }
        }
        echo $html;
        exit;
    }

    public function delete_alert() {

        $id = $this->params['pass']['0'];
        $data = $this->ClientAlerts->delete($id);
        if ($data) {
            $this->Session->setFlash('Alert Deleted successfully.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'accountSettings#tab-2'));
        } else {

            $this->Session->setFlash('Error ! Unable to delete alert.please try again.', 'default', array('class' => 'successmsg'));
            $this->redirect(array('controller' => 'clients', 'action' => 'accountSettings#tab-2'));
        }
    }

}
?>
