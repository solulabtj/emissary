<?php
class HomeController extends AppController {

	public $uses = array('Admin');

    public $paginate = array(
        'limit' => 25,
        'conditions' => array('status' => '1'),
        'order' => array('Admin.username' => 'asc')
    );

    public function beforeFilter() {
        parent::beforeFilter();
       	$this->Auth->allow('login');
    }
	
	public function index()
	{
		//pr($this->Session->read('Auth.User'));exit;
		//$this->Auth->logout();
		//unset($_SESSION['Auth']);
		//pr($_SESSION);exit;
	}
	
	
	
	public function logout() {
        $this->redirect($this->Auth->logout());
    }
    
}

?>