<?php
class ClientsController extends AppController {

	public $uses = array('ClientUserInfo','Admin','AgentJob','AgentUserInfo','ClientImage','Job','Survey','SurveyQuestion','Coupon','JobLocation','SurveyCategory','JobCategory','ClientAddress','AgentRank');
	//public $components = array('Session');
    public function beforeFilter() {
        parent::beforeFilter();
		//pr($this->Session->read('Auth.User'));exit;
       	if (!$this->Session->check('Auth.User.client_id')) {
            $this->redirect(array('controller'=>'home','action' => 'index'));
        }
    }
	
	public function index()
	{
		$this->layout = "client";
	}
	
	public function update_profile()
	{
		$this->layout = "client";
		$client_id = $this->Session->read('Auth.User.client_id');
		
		if($this->request->is('post'))
		{			
        	$post = $this->request->data;
			//pr($post);exit;
			$data['client_id'] = $post['client_id'];
			$data['company_name'] = $post['company_name'];
			$data['company_overview'] = $post['company_overview'];
			$data['mobile_number'] = $post['mobile_number'];
			$data['agency_name'] = $post['agency_name'];
			$data['client_address'] = $post['client_address'];
			$data['paypal_id'] = $post['paypal_id'];
			$data['working_hour_start'] = $post['working_hour_start'];
			$data['working_hour_end'] = $post['working_hour_end'];
			
			if(isset($post['monday']))
			{
				$data['monday'] = $post['monday'];
			}
			else
			{
				$data['monday'] = '0';
			}
			
			if(isset($post['tuesday']))
			{
				$data['tuesday'] = $post['tuesday'];
			}
			else
			{
				$data['tuesday'] = '0';
			}
			
			if(isset($post['wednesday']))
			{
				$data['wednesday'] = $post['wednesday'];
			}
			else
			{
				$data['wednesday'] = '0';
			}
			
			if(isset($post['thursday']))
			{
				$data['thursday'] = $post['thursday'];
			}
			else
			{
				$data['thursday'] = '0';
			}
			
			if(isset($post['friday']))
			{
				$data['friday'] = $post['friday'];
			}
			else
			{
				$data['friday'] = '0';
			}
			
			if(isset($post['saturday']))
			{
				$data['saturday'] = $post['saturday'];
			}
			else
			{
				$data['saturday'] = '0';
			}
			
			if(isset($post['sunday']))
			{
				$data['sunday'] = $post['sunday'];
			}
			else
			{
				$data['sunday'] = '0';
			}
			
			//pr($_FILES);exit;
			if (!empty($_FILES['company_logo']['name'])) {
				$imageExt = array('png','jpg','jpeg','PNG','JPG','JPEG');
				$pathi = "";
				$target_dir	=	WEBSITE_WEBROOT.'media/img/';
				//echo $target_dir;exit;
				//$pathv = "";
				
				$microtime = microtime();

				$fileName = $_FILES['company_logo']["name"];
				$extension = explode(".", $fileName);
				$Path = "";

				if (in_array($extension[1], $imageExt)) {
					$Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					$target_dir2 = $target_dir . $Pathi;
					move_uploaded_file($_FILES['company_logo']["tmp_name"], $target_dir2);
				}/* else {
					$Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					$target_dir2 = $target_dir . $Pathv;
					move_uploaded_file($val["tmp_name"], $target_dir2);
				}*/
				$data['company_logo'] = $Pathi;
				
				
			}



			/* Company image update */


			if (!empty($_FILES['company_image']['name'])) {
				//echo 'sdfsdfsdfds';exit;
				$imageExt = array('png','jpg','jpeg','PNG','JPG','JPEG');
				$pathi = "";
				$target_dir	=	WEBSITE_WEBROOT.'media/img/';
				//echo $target_dir;exit;
				//$pathv = "";
				
				$microtime = microtime();

				$fileName = $_FILES['company_image']["name"];
				$extension = explode(".", $fileName);
				$Path = "";

				if (in_array($extension[1], $imageExt)) {
					$Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					$target_dir2 = $target_dir . $Pathi;
					move_uploaded_file($_FILES['company_image']["tmp_name"], $target_dir2);
				}/* else {
					$Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					$target_dir2 = $target_dir . $Pathv;
					move_uploaded_file($val["tmp_name"], $target_dir2);
				}*/
				$comp_img['image_file_path'] = $Pathi;
				
				
			}


			
			$this->ClientUserInfo->id = $data['client_id'];
			if($this->ClientUserInfo->save($data))
			{		
				$comp_img['client_id'] = $data['client_id'];
				$comp_img['client_image_id'] = $this->request->data['client_image_id'];
				$comp_img['is_default'] = 1;
				//$this->ClientImage->client_image_id = $this->request->data['client_image_id'];
				if($this->ClientImage->save($comp_img))
				{
					$this->Session->setFlash('Client details updated successfully.','default',array('class' => 'successmsg'));
	                $this->redirect(array('controller'=>'clients','action' => 'update_profile'));
	                exit();
				}
				else
				{
					$this->Session->setFlash('Unable to update client data.','default',array('class' => 'successmsg'));
	                $this->redirect(array('controller'=>'clients','action' => 'update_profile'));
	                exit();
				}
						
			}
			else
			{
				$this->Session->setFlash('Unable to update client details.','default',array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'clients','action' => 'update_profile'));
                exit();
			}
			
			
		}
		
		
		$singleclient= $this->ClientUserInfo->find('first',array('fields' => array('ClientUserInfo.*'),'conditions'=>array('client_id'=>$client_id), 'recursive' => -1));
        $clientimage = $this->ClientImage->find('first',array('conditions'=>array('ClientImage.client_id' => $client_id,'ClientImage.is_default'=>1),'recursive'=>-1));
        
		$listing['ClientUserInfo'] = $singleclient['ClientUserInfo'];
		if(!empty($clientimage))
		{
			 $listing['ClientImage'] = $clientimage['ClientImage'];
		}
       

        //pr($listing); exit;
        $this->set('listing',$listing);
		
	}
	
    public function job_post()
	{
		$get_job_list = $this->Job->find('all');
		$this->set('get_job_list',$get_job_list);
		$this->layout = "client";
	}
	
	function activate_inactive($job_id=null,$job_status = null){
		//echo $job_status;exit;
        if($job_status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->Job->id = $job_id; //exit;
        
        if($this->Job->saveField('is_active',$status)){
			//echo "success"; exit;
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'clients','action' => 'job_post'));
            exit();
        } else {
			//echo "failed"; exit;
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'clients','action' => 'job_post'));
            exit();
        }
        
        
    }


	public function new_job_post()
	{
		$client_id = $this->Session->read('Auth.User.client_id');


		$survey_option = $this->Survey->find('all', array('fields'=>array('Survey.survey_id','Survey.survey_name','Survey.survey_category_id'),'conditions'=> array('Survey.client_id' => $client_id, 'Survey.is_active'=>1)));
		$this->set('survey_option', $survey_option);	


		$cat_option = $this->JobCategory->find('all', array('fields'=>array('JobCategory.job_category_id', 'JobCategory.job_category_title'),'conditions' => array('JobCategory.is_active' => 1)));
		$this->set('cat_option', $cat_option);


		$job_location = $this->ClientAddress->find('all', array('fields'=>array('ClientAddress.client_address_id', 'ClientAddress.address'),'conditions' => array('ClientAddress.client_id' => $client_id, 'ClientAddress.is_active'=>1)));
		$this->set('job_location', $job_location);

		$agent_rank = $this->AgentRank->find('all', array('fields' => array('AgentRank.rank', 'AgentRank.rank_title')));
		$this->set('agent_rank', $agent_rank);	


		$coupon_data = $this->Coupon->find('all', array('fields'=> array('Coupon.coupon_id','Coupon.coupon_name'),'conditions' => array('Coupon.client_id' => $client_id)));
		$this->set('coupon_data',$coupon_data);



		if($this->request->is('post')) {

			$new_job_post_data['client_id'] = $this->Session->read('Auth.User.client_id');
			$from_date_format =  $this->request->data['date_from'];
			$to_date_format = $this->request->data['date_to'];
			$reward_id = $this->request->data['reward'];

			$from_date = new DateTime($from_date_format);
			$to_date = new DateTime($to_date_format);

			
			$new_job_post_data['job_start_date'] = $from_date->format('Y-m-d').' '.$this->request->data['time_span_from'];
			$new_job_post_data['job_end_date'] =  $to_date->format('Y-m-d').' '.$this->request->data['time_span_to'];
			$new_job_post_data['min_agent_rank'] = $this->request->data['agent_rank'];
			$new_job_post_data['survey_id'] = $this->request->data['survey_list'];
			$new_job_post_data['job_category_id'] = $this->request->data['job_cat'];

			if($reward_id == 1) {
				$new_job_post_data['job_compensation'] = str_replace('$', '', $this->request->data['cash_value']);
				$new_job_post_data['coupon_id'] = 0;
			}
			else if($reward_id == 2) {
				$new_job_post_data['coupon_id'] = $this->request->data['coupon_value'];
				$new_job_post_data['job_compensation'] = 0;
			}


			
			$new_job_post_data['reward_type'] =  $reward_id;
			$new_job_post_data['job_description'] = $this->request->data['job_overview'];
			$new_job_post_data['max_allowed_requests'] = $this->request->data['request_number'];
			$new_job_post_data['is_active'] = $this->request->data['job_status'];
			$new_job_post_data['test_passed'] = $this->request->data['test_status'];
			
			


			//pr($new_job_post_data); exit;
			if($this->Job->save($new_job_post_data)) {
				$job_id_arr = $this->Job->find('first', array('fields'=>array('Job.job_id'),'order' => array('Job.job_id DESC'),'recursive' => -1));
				$job_id = $job_id_arr['Job']['job_id'];
				$job_address = $this->request->data['location'];
				foreach($job_address as $key => $address)
				{
					$job_loc[$key]['job_id'] = $job_id;
					$job_loc[$key]['client_address_id'] = $address;
				}
				//pr($job_loc);exit;

				if($insert_location = $this->JobLocation->saveAll($job_loc)) {
					echo "Data Has been updated";
				}
				else{
					echo "Please chaeck your data";
				}
			}


			else{
					$this->Session->setFlash('Error ! Job cannot be added.','default',
            array('class' => 'errormsg'));
			}
		}
		$this->layout = "client";
	}
	


	
	public function surveyList()
	{
		$client_id = $this->Session->check('Auth.User.client_id');
		$get_survey_list = $this->Survey->find('all', array('conditions' => array('Survey.client_id' => $client_id)));
		//pr($get_survey_list); exit;
		//echo $survey_que_no; exit;
		foreach ($get_survey_list as $key=>$survey_list) {
			$survey_id = $survey_list['Survey']['survey_id'];
			$category_count = $this->SurveyQuestion->find('count', array('conditions' => array('SurveyQuestion.survey_id' => $survey_id),'group'=>array('SurveyQuestion.survey_category_id')));
			$survey_count = $this->SurveyQuestion->find('count', array('conditions'=> array('SurveyQuestion.survey_id' => $survey_id)));

        //echo $suvery_count; exit;
			$get_survey_list[$key]['Survey']['QuestionCount'] = $survey_count;
			$get_survey_list[$key]['Survey']['CategoryCount'] = $category_count;
		}
		//pr($get_survey_list);
		//exit;
		$this->set('get_survey_list', $get_survey_list);
		$this->layout = "client";
	}
	



	function survey_status($survey_id=null,$survey_status = null){
		//echo $survey_status;exit;
        if($survey_status==1){
            $status = 0;
        }else{
            $status = 1;
        }


        
        
        $this->Survey->id = $survey_id; //exit;
        
        if($this->Survey->saveField('is_active',$status)){
			//echo "success"; exit;
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'clients','action' => 'surveyList'));
            exit();
        } else {
			//echo "failed"; exit;
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'clients','action' => 'surveyList'));
            exit();
        }   
    }


    public function edit_job($edit_job_id=null) {

    	$client_id = $this->Session->read('Auth.User.client_id');
    	if($this->request->is('post'))
    	{
    		$from_date_format =  $this->request->data['date_from'];
			$to_date_format = $this->request->data['date_to'];
			$reward_id = $this->request->data['reward'];
			$from_date = new DateTime($from_date_format);
			$to_date = new DateTime($to_date_format);
			$job_id = $this->request->data['job_ref'];
			$update_job_data['job_start_date'] = $from_date->format('Y-m-d').' '.$this->request->data['time_span_from'];
			$update_job_data['job_end_date'] =  $to_date->format('Y-m-d').' '.$this->request->data['time_span_to'];
			$update_job_data['min_agent_rank'] = $this->request->data['agent_rank'];
			$update_job_data['survey_id'] = $this->request->data['survey_list'];
			$update_job_data['job_category_id'] = $this->request->data['job_cat'];
			if($reward_id == 1) 
			{
				$update_job_data['job_compensation'] = str_replace('$', '', $this->request->data['cash_value']);
				$update_job_data['coupon_id'] = 0;
			}
			else if($reward_id == 2) 
			{
				$update_job_data['coupon_id'] = $this->request->data['coupon_value'];
				$update_job_data['job_compensation'] = 0;
			}
			$update_job_data['reward_type'] =  $reward_id;
			$update_job_data['job_description'] = $this->request->data['job_overview'];
			$update_job_data['max_allowed_requests'] = $this->request->data['request_number'];
			$update_job_data['is_active'] = $this->request->data['job_status'];
			$update_job_data['test_passed'] = $this->request->data['test_status'];
			$this->Job->id = $job_id;
    		if($this->Job->save($update_job_data)) 
    		{
    		 	$this->JobLocation->id = $job_id;
    		 	if($this->JobLocation->deleteAll(array('JobLocation.job_id' => $job_id)))
    		 	{
    		 		$job_address = $this->request->data['location'];
					foreach($job_address as $key => $address)
					{
						$job_loc[$key]['job_id'] = $job_id;
						$job_loc[$key]['client_address_id'] = $address;
					}
				
					if($insert_location = $this->JobLocation->saveAll($job_loc)) 
					{
						$this->Session->setFlash('Status changed successfully.','default', array('class' => 'successmsg'));
            			$this->redirect(array('controller'=>'clients','action' => 'job_post'));
            			exit();
					}
					else
					{
						echo "Please chaeck your data";
					}
    		 	}
    		 	else 
    		 	{
    		 		pr($update_job_data); exit;
    		 	}
    				
    			      
    		}
    		else 
    		{
    			echo "Please check your data to edit";
    		}	
    	}

    	$get_job_data = $this->Job->find('first',array('conditions' =>array('Job.job_id' => $edit_job_id), 'recursive' => -1));

    	$date_time = $get_job_data['Job']['job_start_date'];
    	$timestamp = strtotime($date_time);
    	$edit_job_data['start_date'] = date('n/j/Y',$timestamp);
    	$edit_job_data['start_time'] = date('H',$timestamp);

    	
    	$end_date= $get_job_data['Job']['job_end_date'];
    	$end_datetime = strtotime($end_date);
    	$edit_job_data['end_date'] = date('n/j/Y',$end_datetime);
    	$edit_job_data['end_time'] = date('H',$end_datetime);


    	$client_address_id_arr  = $this->JobLocation->find('all',array('fields' =>('JobLocation.client_address_id'),'conditions' =>array('JobLocation.job_id' => $edit_job_id),'recursive'=>-1));
    	//pr($client_address_id);exit;
    	$cat_option = $this->JobCategory->find('all', array('fields'=>array('JobCategory.job_category_id', 'JobCategory.job_category_title'),'conditions' => array('JobCategory.is_active' => 1)));
		$this->set('cat_option', $cat_option);

    	$survey_option = $this->Survey->find('all', array('fields'=>array('Survey.survey_id','Survey.survey_name','Survey.survey_category_id'),'conditions'=> array('Survey.client_id' => $client_id, 'Survey.is_active'=>1)));
		$this->set('survey_option', $survey_option);

		$job_location = $this->ClientAddress->find('all', array('fields'=>array('ClientAddress.address'),'conditions' => array('ClientAddress.client_id' => $client_id, 'ClientAddress.is_active'=>1)));
		$this->set('job_location', $job_location);

		$agent_rank = $this->AgentRank->find('all', array('fields' => array('AgentRank.rank', 'AgentRank.rank_title','AgentRank.agent_rank_id')));
		$this->set('agent_rank', $agent_rank);

		$coupon_data = $this->Coupon->find('all', array('fields'=> array('Coupon.coupon_id','Coupon.coupon_name'),'conditions' => array('Coupon.client_id' => $client_id)));
		$this->set('coupon_data',$coupon_data);

    	//$edit_job_data['job_location'] = $get_job_data['Job']['job_location'];
    	$edit_job_data['job_reward'] = $get_job_data['Job']['reward_type'];

    	if($edit_job_data['job_reward'] == 1) {
    		$edit_job_data['job_compensation'] = $get_job_data['Job']['job_compensation'];
    		$edit_job_data['coupon_id'] = 0;
    	}
    	else if($edit_job_data['job_reward'] == 2) {
    		//$edit_job_data['job_coupon_id']  = $get_job_data['Job']['coupon_id'];
    		$edit_job_data['coupon_id']  = $get_job_data['Job']['coupon_id'];
    		$edit_job_data['job_compensation'] = 0;
    	}

    	foreach($client_address_id_arr as $loc_key => $loc_value)
    	{
    		$client_add_ids[$loc_key] = $loc_value['JobLocation']['client_address_id'];
    	}
    	$edit_job_data['job_overview'] = $get_job_data['Job']['job_description'];
    	$edit_job_data['job_id'] = $edit_job_id;
    	$edit_job_data['max_allowed_requests'] = $get_job_data['Job']['max_allowed_requests'];
    	$edit_job_data['min_agent_rank'] = $get_job_data['Job']['min_agent_rank'];
    	$edit_job_data['survey_id'] = $get_job_data['Job']['survey_id'];
    	$edit_job_data['job_cat_id'] = $get_job_data['Job']['job_category_id'];
    	$edit_job_data['test_status'] = $get_job_data['Job']['test_passed'];
    	$edit_job_data['job_status'] = $get_job_data['Job']['is_active'];
    	$this->set('edit_job_data', $edit_job_data);
    	$this->set('client_add_ids', $client_add_ids);
    	//pr($edit_job_data);exit;
       	$this->layout = "client";
    }




    public function add_survey() {

    	$client_id = $this->Session->read('Auth.User.client_id');
    	if($this->request->is('post')) {
    		$survey_data['client_id'] = $client_id;
    		$survey_data['survey_name'] = $this->request->data['survey_name'];
    		$survey_data['survey_overview'] = $this->request->data['editor1'];
    		$survey_data['min_threshold']  = $this->request->data['threshold'];
    		$survey_data['is_active'] = $this->request->data['Survey'];
    		//pr($survey_data); exit;
    		if($this->Survey->save($survey_data)){
    			$this->Session->setFlash('Survey list has been updated successfully.','default', array('class' => 'successmsg'));
            	$this->redirect(array('controller'=>'clients','action' => 'surveyList'));
            	exit();
    		}
    		else{
    			echo "nhi hoya"; exit;
    		}
     	}

    	$this->layout = "client";

    }
	
	 public function accountSettings() {


	 	$client_id = $this->Session->read('Auth.User.client_id');
	 	$getoldpassword = $this->ClientUserInfo->field('password', array('ClientUserInfo.client_id' => $client_id));


	 	//pr($getoldpassword);exit;

	 	if($this->request->is('post'))
	 	{
	 		$old = AuthComponent::password($this->request->data['oldPassword']);
	 		$resetpassword['password'] = AuthComponent::password($this->request->data['newPassword']);
	 		$email_code = $this->request->data['email_notf'];
	 		if($email_code == 1) {
	 			$resetpassword['receive_email'] = 1;
	 		}
	 		else
	 		{
	 			$resetpassword['receive_email'] =0;
	 		}
	 		//pr($this->request->data);exit;

	 		if($getoldpassword == $old)
	 		{
	 			$this->ClientUserInfo->id = $client_id;
	 			if($this->ClientUserInfo->save($resetpassword)) 
	 			{
	 				$this->Session->setFlash('Password changed successfully.','default', array('class' => 'successmsg'));
	 			}
	 			else
	 			{
	 				echo "Query problem";
	 			}
	 		}
	 		else
	 		{
	 			echo "Please check your  data";
	 		}
	 	}


    	$this->layout = "client";

    }

    public function requests()
	{

		$client_id = $this->Session->read('Auth.User.client_id');
		$request_list = $this->AgentJob->find('all', ['conditions' => ['AgentJob.client_id' => $client_id,'AgentJob.is_completed_by_agent'=>0,'AgentJob.is_verified_by_client'=>0,'AgentJob.cancelled_by_agent'=>0,'AgentJob.cancelled_by_client'=>0]]);
		foreach($request_list as $key => $list)
		{
			$agent_rank_arr = $this->AgentRank->find('first',array('conditions'=>array('AgentRank.rank' => $list['AgentUserInfo']['agent_rank']),'recursive' => -1));
			$agent_rank = $agent_rank_arr['AgentRank']['rank_title'];
			$request_list[$key]['AgentUserInfo']['agent_rank_title'] = $agent_rank;
		}
		//pr($rank);

		
		$this->set('request_list', $request_list);
		$this->layout = "client";
	}

	public function change_request_status($agent_job_id, $activity) {

		if ($activity == 'A') {
			$this->AgentJob->id = $agent_job_id;	
			if($this->AgentJob->saveField('is_confirmed_by_client','1')) {
				$this->redirect(array('controller'=>'clients','action' => 'requests'));
				exit();
			}			
		}
		elseif ($activity == 'D') {
			$this->AgentJob->id = $agent_job_id;	
			if($this->AgentJob->saveField('declined_by_client','1')) {
				$this->redirect(array('controller'=>'clients','action' => 'requests'));
				exit();
			}
		}
		elseif ($activity == 'R') {
			$this->AgentJob->id = $agent_job_id;	
			if($this->AgentJob->saveField('cancelled_by_client','1')) {
				$this->redirect(array('controller'=>'clients','action' => 'requests'));
				exit();
			}
		}
	}


	public function job_submission()
	{
		
		$client_id = $this->Session->read('Auth.User.client_id');
		$job_submission_list = $this->AgentJob->find('all', ['conditions' => ['AgentJob.client_id' => $client_id, 'AgentJob.is_completed_by_agent' => 1]]);
		
		$this->set('job_submission_list', $job_submission_list);
		$this->layout = 'client';

	}
}

?>