<?php
class ClientsController extends AppController {

	public $uses = array('ClientUserInfo','Admin');

    public function beforeFilter() {
        parent::beforeFilter();
		//pr($this->Session->read('Auth.User'));exit;
       	if (!$this->Session->check('Auth.User.client_id')) {
            $this->redirect(array('controller'=>'home','action' => 'index'));
        }
    }
	
	public function index()
	{
		$this->layout = "client";
		
		if($this->request->is('post'))
		{			
        	$post = $this->request->data;
			//pr($post);exit;
			$client_id = $post['client_id'];
			$company_name = $post['company_name'];
			$company_overview = $post['company_overview'];
			$mobile_number = $post['mobile_number'];
			$agency_name = $post['agency_name'];
			$client_address = $post['client_address'];
			$paypal_id = $post['paypal_id'];
			$working_hour_start = $post['working_hour_start'];
			$working_hour_end = $post['working_hour_end'];
			
			if(isset($post['monday']))
			{
				$monday = $post['monday'];
			}
			else
			{
				$monday = '0';
			}
			
			if(isset($post['tuesday']))
			{
				$tuesday = $post['tuesday'];
			}
			else
			{
				$tuesday = '0';
			}
			
			if(isset($post['wednesday']))
			{
				$wednesday = $post['wednesday'];
			}
			else
			{
				$wednesday = '0';
			}
			
			if(isset($post['thursday']))
			{
				$thursday = $post['thursday'];
			}
			else
			{
				$thursday = '0';
			}
			
			if(isset($post['friday']))
			{
				$friday = $post['friday'];
			}
			else
			{
				$friday = '0';
			}
			
			if(isset($post['saturday']))
			{
				$saturday = $post['saturday'];
			}
			else
			{
				$saturday = '0';
			}
			
			if(isset($post['sunday']))
			{
				$sunday = $post['sunday'];
			}
			else
			{
				$sunday = '0';
			}
			
			$query = $this->ClientUserInfo->query("UPDATE client_user_infos 
												   SET company_name='$company_name',
												   company_overview='$company_overview',
												   mobile_number='$mobile_number',
												   agency_name='$agency_name',
												   client_address='$client_address',
												   paypal_id='$paypal_id',
												   working_hour_start='$working_hour_start',
												   working_hour_end='$working_hour_end',
												   monday='$monday',
												   tuesday='$tuesday',
												   wednesday='$wednesday',
												   thursday='$thursday',
												   friday='$friday',
												   saturday='$saturday',
												   sunday='$sunday'
												   WHERE client_id = '$client_id'");
			
			if (isset($query)) 
			{
                
                $this->Session->setFlash('Client details updated successfully.','default',
    array('class' => 'successmsg'));
                $this->redirect(array('controller'=>'clients','action' => 'index'));
                exit();
                
            }
			
			else
			{
                $this->Session->setFlash('Unable to update client details.','default',
    array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'clients','action' => 'index'));
                exit();
            }
			
		}
		
		$singleclient = $this->ClientUserInfo->find('first',array('fields' => array('ClientUserInfo.*'),'conditions'=>array('client_id'=>1), 'recursive' => -1));
        //pr($singleclient); exit;
        $this->set('listing',$singleclient);
		
	}
	
    public function job_post()
	{
		$this->layout = "client";
	}
	
	public function new_job_post()
	{
		$this->layout = "client";
	}
	
}

?>