<?php
class UsersController extends AppController {

	public $uses = array('Admin','ClientUserInfo','AgentUserInfo');

    public function beforeFilter() {
        parent::beforeFilter();
       	$this->Auth->allow('login');
    }
	
	public function login()
	{
		if ($this->Session->check('Auth.User.client_id')) {
            echo "redirectClient";exit;
        }
		if ($this->Session->check('Auth.User.agent_id')) {
            echo "redirectAgent";exit;
        }
		//unset($_SESSION['Auth']);
		//pr($this->Auth->user());
		//pr($_SESSION);exit;
		//echo AuthComponent::password($this->request->data['Admin']['password']);exit;
        if ($this->request->is('post')) {
			//$this->Auth->logout();
			//echo AuthComponent::password($this->request->data['password']);exit;
			//$this->Auth->password($this->request->data['password']);
			//echo 'dfgdfg';pr($this->Auth->user());
			//echo '0';exit;
			if($this->request->data['loginType'] == 'agent')
			{
				$userModel = 'AgentUserInfo';	
			}
			elseif($this->request->data['loginType'] == 'company')
			{
				$userModel = 'ClientUserInfo';	
			}
			else
			{
				echo 'Please select login type';exit;	
			}
			$this->request->data[$userModel] = $this->request->data;
			$user = $this->$userModel->find('first',array('conditions'=>array('username'=>$this->request->data[$userModel]['username']),'recursive'=>-1));
			
			//pr($admin);exit;
			
			if(isset($user) && !empty($user))
			{
				/*pr($admin);
				$this->Session->write($admin);
				pr($_SESSION);*/
				//echo AuthComponent::password($this->request->data['Admin']['password']);exit;
				
				//echo "<pre>"; print_r($admin); exit;
				
				if($user[$userModel]['is_blocked'] == 0)
				{
					//debug($this->Auth->login($admin)); die();
					//echo "first";exit;
					
					//pr($admin); exit;
					
					if ($this->Auth->login()) {
						echo "success";exit;
						//$this->redirect(array('controller' => 'home','action' => 'index'));
						
					} else {
						//echo "third";exit;
						echo 'Invalid Username or Password';exit;
						
					}
				}
				else
				{
					//echo "fourth";exit;
					echo 'You have been blocked by administrator';	exit;
				}
			}
			else
			{
				//echo "fifth";exit;
				echo 'User not found';exit;
			}
        }
	}
	
	public function logout()
    {
		if($this->Auth->logout())
		{
			$this->redirect(array('controller'=>'home','action' => 'index'));	
		}
	}


	public function signup() {
		
		if($this->request->is('post'))  {
			$signup_type = $this->request->data['register_type'];
			if($signup_type == 'agent') {
				$signup_data['agent_name'] = $this->request->data['fullname'];
				$signup_data['username'] = $this->request->data['email'];
				$signup_data['email_address'] = $this->request->data['email'];
				$signup_data['mobile_number'] = $this->request->data['mobile'];
				$signup_data['password'] = AuthComponent::password($this->request->data['password']);
				//pr($signup_data); exit;
				if($this->AgentUserInfo->save($signup_data)) {
					echo 'Please Confirm your email for confirmation mail.';exit;
				}
				else{
					pr($this->AgentUserInfo->validationErrors);exit;
				}
			}
			elseif($signup_type == 'provider') {
				$signup_data['client_name'] = $this->request->data['fullname'];
				$signup_data['username'] = $this->request->data['email'];
				$signup_data['email_address'] = $this->request->data['email'];
				$signup_data['mobile_number'] = $this->request->data['mobile'];
				$signup_data['password'] = AuthComponent::password($this->request->data['password']);
				//pr($signup_data); exit;
				if($this->ClientUserInfo->save($signup_data)) {
					echo 'Please Confirm your email for confirmation mail.';exit;
				}
				else{
					pr($this->ClientUserInfo->validationErrors);exit;
				}
			}
			else{
				echo 'Please check your signup type';
			}
		}
	}


}

?>