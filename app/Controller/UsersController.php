<?php

App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

    public $uses = array('Admin', 'AgentJob', 'MinPackage', 'Package', 'ClientUserInfo', 'AgentUserInfo', 'JobCategory', 'JobFilter', 'JobFilterCategory', 'ClientSubscription', 'ClientAlerts');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'forgot_password', 'forgot', 'signup', 'clientalert', 'subscriptions', 'send_email');
    }

    public function setLatLongInSession() {
        $_SESSION['current_lat'] = $_REQUEST['current_latitude'];
        $_SESSION['current_lng'] = $_REQUEST['current_longitude'];
        echo "set";
        exit;
        //pr($_SESSION);exit;
    }

    public function login() {
        if ($this->Session->check('Auth.User.client_id')) {
            echo "redirectClient";
            exit;
        }
        if ($this->Session->check('Auth.User.agent_id')) {
            echo "redirectAgent";
            exit;
        }
        //unset($_SESSION['Auth']);
        //pr($this->Auth->user());
        //pr($_SESSION);exit;
        //echo AuthComponent::password($this->request->data['Admin']['password']);exit;
        if ($this->request->is('post')) {
            //$this->Auth->logout();
            //echo AuthComponent::password($this->request->data['password']);exit;
            //$this->Auth->password($this->request->data['password']);
            //echo 'dfgdfg';pr($this->Auth->user());
            //echo '0';exit;
            if ($this->request->data['loginType'] == 'agent') {
                $userModel = 'AgentUserInfo';
            } elseif ($this->request->data['loginType'] == 'company') {
                $userModel = 'ClientUserInfo';
            } else {
                echo 'Please select login type';
                exit;
            }
            $this->request->data[$userModel] = $this->request->data;
            $user = $this->$userModel->find('first', array('conditions' => array('username' => $this->request->data[$userModel]['username']), 'recursive' => -1));

            //pr($admin);exit;

            if (isset($user) && !empty($user)) {
                /* pr($admin);
                  $this->Session->write($admin);
                  pr($_SESSION); */
                //echo AuthComponent::password($this->request->data['Admin']['password']);exit;
                //echo "<pre>"; print_r($admin); exit;

                if ($user[$userModel]['is_blocked'] == 0) {
                    //debug($this->Auth->login($admin)); die();
                    //echo "first";exit;
                    //pr($admin); exit;

                    if ($this->Auth->login()) {

                        $logincount = $user[$userModel]['login_count'] + 1;

                        $data['last_login'] = NOW;
                        $data['login_count'] = $logincount;
                        if ($this->request->data['loginType'] == 'agent') {
                            $data['agent_id'] = $user[$userModel]['agent_id'];
                            $access_code = md5($data['agent_id']);
                            $access_code = 'A' . $access_code;
                            $data['access_code'] = $access_code;
                        } elseif ($this->request->data['loginType'] == 'company') {
                            $data['client_id'] = $user[$userModel]['client_id'];
                            $access_code = md5($data['client_id']);
                            $access_code = 'C' . $access_code;
                            $data['access_code'] = $access_code;
                        }
                        $this->$userModel->save($data);

                        echo "success";
                        exit;
                        //$this->redirect(array('controller' => 'home','action' => 'index'));
                    } else {
                        //echo "third";exit;
                        echo 'Invalid Username or Password.';
                        exit;
                    }
                } else {
                    //echo "fourth";exit;
                    echo 'You have been blocked by administrator.';
                    exit;
                }
            } else {
                //echo "fifth";exit;
                echo 'User does not exist.';
                exit;
            }
        }

        $this->layout = 'login';
    }

    public function logout() {
        if ($this->Auth->logout()) {
            $this->redirect(array('controller' => 'home', 'action' => 'index'));
        }
    }

    public function signup() {

        if ($this->request->is('post')) {

            //pr($this->request->data);exit;            

            $message = '';
            $signup_type = $this->request->data['loginType'];
            if ($signup_type == 'agent') {
                $signup_data['agent_name'] = $this->request->data['f_name'] . ' ' . $this->request->data['l_name'];
                $signup_data['username'] = $this->request->data['emailid'];
                $signup_data['email_address'] = $this->request->data['emailid'];
                $signup_data['mobile_number'] = $this->request->data['mobile'];
                $signup_data['password'] = AuthComponent::password($this->request->data['password']);
                //pr($signup_data); exit;
                if ($this->AgentUserInfo->save($signup_data)) {
                    $agent_id = $this->AgentUserInfo->getLastInsertId();
                    $this->create_default_filter($agent_id);

                    $message = 'Please Confirm your email for confirmation mail.';

                    $from1 = array('admin@emissary.com' => 'EMISSARY');
                    $to1 = $this->request->data['emailid'];
                    $subject1 = 'Welcome to Emissary';

                    $msg1 = "<p>Thank you, " . $signup_data['agent_name'] . ", and welcome to the Emissary platform! <br/><br/><br/>
                             In the attachment, you will find all you need to know about Emissary. Please go through it in detail as we want to make sure you and contributing Mystery Shopping Providers have the greatest experience possible. As you finish your shops via the Emissary system, you will also be rewarded with points to improve your Agent Rank and ascend the Emissary Leaderboards.
                             Information about this is provided in the attachment.<br/><br/><br/>
                             Emissary is a platform businesses use to provide and gather feedback.<br/> 
                             The businesses are individually accountable for each element of their shops – including
                             remunerating you. Emissary is NOT a Mystery Shopper Provider - it is a shopper gateway, <br/>
                             assisting shoppers to discover all opportunities in a single place. <br/>
                             You will be required to complete each business’ Independent Contractor Agreement to qualify to take and complete jobs for them. <br/>
                             Please be aware that new opportunities and new businesses are continually being uploaded. <br/>
                             If you cannot find anything close to you, be sure to check in again soon! You will be contacted as Emissary opportunities become available in your region. <br/>
                             Thanks again for signing up with Emissary!<br/><br/><br/>
                             Sincerely,<br/>
                             The Emissary Team<br/><br/>
                            <img src='http://emissaryagency.com/beta/images/logo_nav.png' alt='Your logo' style='background-color: black;'><br/><br/>
                            Powered by: Emissary Software Platform<br/>
                            Find Emissary on Facebook: http://www.facebook.com/Emissaryagency<br/>
                            Follow Emissary on Instagram: @Emissaryagency<br/></p>";

                    $template1 = '';

                    $mail = $this->send_email($from1, $to1, $subject1, $msg1, $template1);

                    //exit;
                } else {
                    $errors = $this->AgentUserInfo->validationErrors;

                    if (isset($errors['mobile_number'][0]) && !empty($errors['mobile_number'][0])) {
                        $str = 'Mobile No. already exists.';
                    }

                    if (isset($errors['email_address'][0]) && !empty($errors['email_address'][0])) {
                        $str .= ' Email Address already exists.';
                    }

                    $message = $str;

                    //exit;
                }
            } elseif ($signup_type == 'provider') {
                $signup_data['client_name'] = $this->request->data['f_name'] . ' ' . $this->request->data['l_name'];
                $signup_data['company_name'] = $this->request->data['f_name'] . ' ' . $this->request->data['l_name'];
                $signup_data['username'] = $this->request->data['emailid'];
                $signup_data['email_address'] = $this->request->data['emailid'];
                $signup_data['mobile_number'] = $this->request->data['mobile'];
                $signup_data['is_active'] = 1;
                $signup_data['is_confirmed'] = 1;
                $signup_data['subscription_expiration'] = date('Y-m-d h:i:s', strtotime('+30 days'));
                $signup_data['password'] = AuthComponent::password($this->request->data['password']);
                //pr($signup_data); exit;
                if ($this->ClientUserInfo->save($signup_data)) {
                    $message = 'Please Confirm your email for confirmation mail.';

                    /* $from1 = array('admin@emissary.com' => 'EMISSARY');
                      $to1 = $this->request->data['emailid'];
                      $subject1 = 'New Provider successfully onboarded ';

                      $msg1 = $signup_data['email_address'] . "|" . $signup_data['mobile_number'];
                      $template1 = '';

                      $mail = $this->send_email($from1, $to1, $subject1, $msg1, $template1);
                     */
                    //exit;
                } else {
                    $errors = $this->ClientUserInfo->validationErrors;

                    if (isset($errors['mobile_number'][0]) && !empty($errors['mobile_number'][0])) {
                        $str = 'Mobile No. already exists.';
                    }

                    if (isset($errors['email_address'][0]) && !empty($errors['email_address'][0])) {
                        $str .= ' Email Address already exists.';
                    }

                    $message = $str;

                    //exit;
                }
            } else {
                $message = 'Please check your signup type';
            }
        }

        $this->set('message', $message);
        $this->layout = 'login';
    }

    public function create_default_filter($agent_id) {
        $i = 'no';

        if (!empty($agent_id) && $agent_id != 0) {
            $data['agent_id'] = $agent_id;
            $data['min_distance'] = MIN_DISTANCE;
            $data['max_distance'] = MAX_DISTANCE;
            $data['minimum_compensation'] = MIN_COMPENSATION;
            $data['maximum_compensation'] = MAX_COMPENSATION;
            $data['due_date_span'] = DUE_DATE_SPAN;
            $data['is_active'] = 1;

            $job_categories = $this->JobCategory->find('all', array('conditions' => array('JobCategory.is_active' => 1, 'JobCategory.is_deleted' => 0), 'recursive' => -1));
            //pr($job_categories);exit;
            if ($this->JobFilter->save($data)) {
                $job_filter = $this->JobFilter->find('first', array('order' => array('JobFilter.job_filter_id DESC'), 'recursive' => -1));
                $job_filter_id = $job_filter['JobFilter']['job_filter_id'];
                $job_filter_data['agent_id'] = $agent_id;
                //pr($job_filter);exit;
                $k = 0;
                foreach ($job_categories as $jcat) {
                    $agent_filter_data[$k]['job_filter_id'] = $job_filter_id;
                    $agent_filter_data[$k]['agent_id'] = $agent_id;
                    $agent_filter_data[$k]['job_category_id'] = $jcat['JobCategory']['job_category_id'];
                    $agent_filter_data[$k]['is_checked'] = 1;
                    $k++;
                }
                if ($this->JobFilterCategory->saveall($agent_filter_data)) {
                    $message = true;
                } else {
                    $message = false;
                }
            } else {
                $message = false;
            }
        } else {
            $message = false;
        }

        return $message;
    }

    function forgot_password() {

        if ($this->request->is('post')) {
            $post = $this->request->data;
            //pr($post); exit;	

            if ($post['user_type'] == 'agent') {
                $useModel = 'AgentUserInfo';
                $ID = 'agent_id';
            } else {
                $useModel = 'ClientUserInfo';
                $ID = 'client_id';
            }

            if ($user = $this->$useModel->find('first', array('conditions' => array('email_address' => $post['username'], 'is_active' => 1), 'recursive' => -1))) {

                //pr($merdetail);
                $ver_code = md5(microtime());
                $from = array('admin@emissary.com' => 'EMISSARY');
                $to = $post['username'];
                $subject = 'Emmissary! Reset Password';
                $link = WEBSITE_ROOT . 'users/forgot/?vercode=' . $ver_code . '&email_address=' . urlencode($to) . '&user_type=' . $post['user_type'];
                $linkOnMail = '<a href="' . $link . '" class="link2" target="_blank">Click Here to Update Your password </a>';
                $msg = $linkOnMail;
                $template = 'forgot_password';
                $data['verification_code'] = $ver_code;
                $data[$ID] = $user[$useModel][$ID];
                if ($this->$useModel->save($data)) {
                    $mail = $this->send_email($from, $to, $subject, $msg, $template);

                    echo 'Please check your mailbox for reset password.
							
							<a href="' . WEBSITE_ROOT . '">Back to Website</a>
							';
                    exit;
                } else {
                    echo 'Try again later!';
                    exit;
                }
            } else {
                echo 'You are not a registered user !';
                exit;
            }
        }
    }

    public function forgot() {

        $vercode = $_GET['vercode'];
        $email_address = urldecode($_GET['email_address']);
        $user_type = $_GET['user_type'];

        if ($this->request->is('post')) {
            $post = $this->request->data;
            //pr($post);exit;
            if ($post['user_type'] == 'agent') {
                $useModel = 'AgentUserInfo';
                $ID = 'agent_id';
            } else {
                $useModel = 'ClientUserInfo';
                $ID = 'client_id';
            }

            $verify = $this->$useModel->find('first', array('conditions' => array('verification_code' => $post['vercode'], 'email_address' => $post['email_address'], 'is_active' => 1)));

            if (isset($verify) && !empty($verify)) {
                isset($post['new_password']) ? $post['new_password'] = $this->Auth->password($post['new_password']) : '';
                $data[$ID] = $verify[$useModel][$ID];
                $data['password'] = $post['new_password'];

                $data['verification_code'] = '';
                if ($this->$useModel->save($data)) {

                    echo 'Your password has been updated successfully. <a href="' . WEBSITE_ROOT . '">Back to Website</a>';
                    exit;
                } else {
                    echo 'Please Try again later!';
                    exit;
                }
            } else {
                echo 'Verification Code has been expired. <a href="' . WEBSITE_ROOT . '">Back to Website</a>';
                exit;
            }
        }


        $this->set('vercode', $vercode);
        $this->set('email_address', $email_address);
        $this->set('user_type', $user_type);

        $this->layout = 'login';
    }

    public function send_email($from, $to, $subject, $message, $template) {

        $this->Email = new CakeEmail();
        $this->Email->config('smtp');
        $this->Email->to($to);
        $this->Email->subject($subject);
        $this->Email->emailFormat('html');

        $this->Email->from($from);
        $this->Email->template($template);

        ob_start();
        $this->Email->send($message);
        ob_end_clean();
    }

    public function subscriptions() {
        $current_date = date("Y-m-d");
        $client_data = $this->ClientUserInfo->query("select * from client_user_infos");
        $min_val = $this->MinPackage->query("select * from min_package");
        foreach ($client_data as $client) {
            $created_date = date("Y-m-d", strtotime($client['client_user_infos']['subscription_expiration']));
            $date1 = date_create($current_date);
            $date2 = date_create($created_date);
            $diff = date_diff($date1, $date2);
            $days = $diff->format("%a");
            if ($days == "" || $days == "0") {
                $client_id = $client['client_user_infos']['client_id'];
                $start_date = date('Y-m-d', strtotime('-30 days'));
                $submission = $this->AgentJob->find('all', array('conditions' => array('AgentJob.client_id' => $client_id, 'AgentJob.modified >' => $start_date, 'AgentJob.modified <' => $current_date, 'AgentJob.accepted_by_client' => 1, 'AgentJob.declined_by_client' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.completed_by_agent' => 1)));
                $total_subs = count($submission);
                $min_package = $min_val[0]['min_package']['min_package_fee'];
                if ($total_subs <= 1000) {
                    $pack_val = $this->Package->query("select cost from package_details where package_detail_id = 1");
                    $pack_cost = $pack_val[0]['package_details']['cost'];
                    $amount = intval($total_subs) * $pack_cost;
                    if ($amount > $min_package) {
                        $final_amount = $amount;
                    } else {
                        $final_amount = $min_package;
                    }
                } else if ($total_subs >= 1001 && $total_subs <= 3000) {
                    $pack_val = $this->Package->query("select cost from package_details where package_detail_id = 2");
                    $pack_cost = $pack_val[0]['package_details']['cost'];
                    $amount = intval($total_subs) * $pack_cost;
                    $final_amount = $amount;
                } else if ($total_subs >= 3001 && $total_subs <= 5000) {
                    $pack_val = $this->Package->query("select cost from package_details where package_detail_id = 3");
                    $pack_cost = $pack_val[0]['package_details']['cost'];
                    $amount = intval($total_subs) * $pack_cost;
                    $final_amount = $amount;
                } else if ($total_subs >= 5001 && $total_subs <= 8000) {
                    $pack_val = $this->Package->query("select cost from package_details where package_detail_id = 4");
                    $pack_cost = $pack_val[0]['package_details']['cost'];
                    $amount = intval($total_subs) * $pack_cost;
                    $final_amount = $amount;
                } else if ($total_subs >= 8001 && $total_subs <= 10000) {
                    $pack_val = $this->Package->query("select cost from package_details where package_detail_id = 5");
                    $pack_cost = $pack_val[0]['package_details']['cost'];
                    $amount = intval($total_subs) * $pack_cost;
                    $final_amount = $amount;
                } else if ($total_subs >= 10000) {
                    $pack_val = $this->Package->query("select cost from package_details where package_detail_id = 6");
                    $pack_cost = $pack_val[0]['package_details']['cost'];
                    $amount = intval($total_subs) * $pack_cost;
                    $final_amount = $amount;
                }

                $sub_data['transaction_id'] = "00000000";
                $sub_data['transaction_status'] = "pending";
                $sub_data['transaction_time'] = "0";
                $sub_data['client_id'] = $client_id;
                $sub_data['amount_paid'] = $final_amount;
                $sub_data['subscription_type'] = "0";
                $sub_data['payer_id'] = "0";
                $sub_data['payer_firstname'] = $client['client_user_infos']['client_name'];
                $sub_data['payer_lastname'] = $client['client_user_infos']['client_name'];
                $sub_data['payer_email'] = $client['client_user_infos']['email_address'];
                $sub_data['created'] = date("Y-m-d h:i:s");
                $sub_data['modified'] = "0";
                //pr($signup_data); exit;
                if ($this->ClientSubscription->save($sub_data)) {
                    echo "Data saved";
                }



                /*                 * ***************** code to send mail *************** */

                /*  $from1 = array('admin@emissary.com' => 'EMISSARY');
                  $to1 = $client['client_user_infos']['email_address'];
                  $subject1 = 'Subscription Invoice';
                  $msg1 = $final_amount;
                  $template1 = '';
                  $mail = $this->send_email($from1, $to1, $subject1, $msg1, $template1); */
            }
        }
        exit;
    }
    
    public function clientalert() {
        $alert_data = $this->ClientAlerts->query("Select * from client_alerts");
        echo "<pre>";
        if (!empty($alert_data)) {
            foreach ($alert_data as $data) {

                $client_id = $data['client_alerts']['client_id'];
                $alert_type = $data['client_alerts']['alert_type'];
                $alert_action = $data['client_alerts']['alert_action'];
                $location = $data['client_alerts']['location'];
                $request = $data['client_alerts']['req'];
                $question = $data['client_alerts']['question'];
                $answer = $data['client_alerts']['answer'];
                $job_id = json_decode($data['client_alerts']['job']);
                $job_count = Count($job_id);
                $cat_id = json_decode($data['client_alerts']['category']);
                $cat_count = Count($cat_id);
                $reqCount = 0;
                $client_details = $this->ClientUserInfo->query("select * from client_user_infos where client_id = $client_id");
                $to = isset($client_details[0]['client_user_infos']['email_address'])?$client_details[0]['client_user_infos']['email_address']:""; 
                $subject = isset($data['client_alerts']['alert_name'])?$data['client_alerts']['alert_name']:"";
                $msg = isset($data['client_alerts']['alert_content'])?$data['client_alerts']['alert_content']:"";
                $template = "";
                if ($alert_type == "1") {

                    if ($alert_action == "1") {
                        $condition = "AND accepted_by_client = 0 "
                                . "AND declined_by_client = 0 "
                                . "AND completed_by_agent = 0 "
                                . "AND cancelled_by_agent = 0 "
                                . "AND disapproved_by_client = 0 "
                                . "AND approved_by_client = 0";
                        for ($i = 0; $i < $job_count; $i++) {

                            $job = $job_id[$i];
                            $getCount = $this->AgentJob->query("Select count(client_id) as Count from agent_jobs WHERE job_location_id = $location AND client_id= $client_id AND job_id = $job $condition GROUP BY client_id ");
                            $reqCount += isset($getCount[0][0]['Count']) ? $getCount[0][0]['Count'] : 0;
                        }

                        if ($reqCount == $request) {
                            $mail = $this->send_email($from, $to, $subject, $msg, $template);
                        }
                    } else {

                        $condition = "AND accepted_by_client = 1 "
                                . "AND declined_by_client = 0 "
                                . "AND completed_by_agent = 1 "
                                . "AND cancelled_by_agent = 0 "
                                . "AND disapproved_by_client = 0 "
                                . "AND approved_by_client = 0";
                        for ($i = 0; $i < $job_count; $i++) {

                            $job = $job_id[$i];
                            $getCount = $this->AgentJob->query("Select count(client_id) as Count from agent_jobs WHERE job_location_id = $location AND client_id= $client_id AND job_id = $job $condition GROUP BY client_id ");
                            $reqCount += isset($getCount[0][0]['Count']) ? $getCount[0][0]['Count'] : 0;
                        }

                        if ($reqCount == $request) {
                            $mail = $this->send_email($from, $to, $subject, $msg, $template);
                        }
                    }
                } else if ($alert_type == "2") {

                    $condition = "AND accepted_by_client = 1 "
                            . "AND declined_by_client = 0 "
                            . "AND completed_by_agent = 1 "
                            . "AND cancelled_by_agent = 0 "
                            . "AND disapproved_by_client = 0 "
                            . "AND approved_by_client = 1 "
                            . "AND is_paid = 0";
                    $getCount = $this->AgentJob->query("Select count(client_id) as Count from agent_jobs WHERE  client_id= $client_id $condition GROUP BY client_id ");
                    $reqCount = isset($getCount[0][0]['Count']) ? $getCount[0][0]['Count'] : 0;

                    if ($reqCount == $request) {
                        $mail = $this->send_email($from, $to, $subject, $msg, $template);
                    }
                } else if ($alert_type == "3") {

                    $condition = "AND agent_jobs.accepted_by_client = 1 "
                            . "AND agent_jobs.declined_by_client = 0 "
                            . "AND agent_jobs.completed_by_agent = 1 "
                            . "AND agent_jobs.cancelled_by_agent = 0 "
                            . "AND agent_jobs.disapproved_by_client = 0 "
                            . "AND agent_jobs.approved_by_client = 0";

                    for ($i = 0; $i < $cat_count; $i++) {
                        $cat = $cat_id[$i];
                        $getCount = $this->AgentJob->query("Select count(agent_jobs.client_id) as Count from agent_jobs "
                                . "LEFT JOIN jobs ON agent_jobs.job_id = jobs.job_id "
                                . "LEFT JOIN surveys ON jobs.survey_id = surveys.survey_id "
                                . "LEFT JOIN survey_questions ON survey_questions.survey_id = surveys.survey_id "
                                . "LEFT JOIN survey_categories ON survey_questions.survey_category_id = survey_categories.survey_category_id "
                                . "WHERE  agent_jobs.client_id= $client_id AND agent_jobs.job_location_id= $location $condition AND survey_categories.survey_category_id = $cat"
                                . " GROUP BY agent_jobs.client_id ");
                        $reqCount += isset($getCount[0][0]['Count']) ? $getCount[0][0]['Count'] : 0;
                    }

                    if ($reqCount == $request) {
                        $mail = $this->send_email($from, $to, $subject, $msg, $template);
                    }
                } else if ($alert_type == "4") {
                    if ($alert_action == "1") {

                        $condition = "AND agent_jobs.accepted_by_client = 1 "
                                . "AND agent_jobs.declined_by_client = 0 "
                                . "AND agent_jobs.completed_by_agent = 1 "
                                . "AND agent_jobs.cancelled_by_agent = 0 "
                                . "AND agent_jobs.disapproved_by_client = 0 "
                                . "AND agent_jobs.approved_by_client = 0";



                        $getCount = $this->AgentJob->query("Select count(agent_jobs.client_id) as Count from agent_jobs "
                                . "LEFT JOIN jobs ON agent_jobs.job_id = jobs.job_id "
                                . "LEFT JOIN surveys ON jobs.survey_id = surveys.survey_id "
                                . "LEFT JOIN survey_questions ON survey_questions.survey_id = surveys.survey_id "
                                . "WHERE  agent_jobs.client_id= $client_id AND agent_jobs.job_location_id= $location $condition AND survey_questions.survey_question_id = $question"
                                . " GROUP BY agent_jobs.client_id ");

                        $reqCount = isset($getCount[0][0]['Count']) ? $getCount[0][0]['Count'] : 0;

                        if ($reqCount == $request) {
                            $mail = $this->send_email($from, $to, $subject, $msg, $template);
                        }
                    } else {

                        $condition = "AND agent_jobs.accepted_by_client = 1 "
                                . "AND agent_jobs.declined_by_client = 0 "
                                . "AND agent_jobs.completed_by_agent = 1 "
                                . "AND agent_jobs.cancelled_by_agent = 0 "
                                . "AND agent_jobs.disapproved_by_client = 0 "
                                . "AND agent_jobs.approved_by_client = 0";



                        $getCount = $this->AgentJob->query("Select count(agent_jobs.client_id) as Count from agent_jobs "
                                . "LEFT JOIN agent_survey_answers ON agent_jobs.agent_job_id = agent_survey_answers.agent_job_id "
                                . "WHERE  agent_jobs.client_id= $client_id AND agent_jobs.job_location_id= $location $condition AND agent_survey_answers.survey_question_answer = $answer"
                                . " GROUP BY agent_jobs.client_id ");

                        $reqCount = isset($getCount[0][0]['Count']) ? $getCount[0][0]['Count'] : 0;

                        if ($reqCount == $request) {
                            $mail = $this->send_email($from, $to, $subject, $msg, $template);
                        }
                    }
                }
            }
        }
        exit;
    }

}

?>