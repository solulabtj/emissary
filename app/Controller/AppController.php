<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller','File', 'Utility');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	public $uses = array('AgentUserInfo','ClientUserInfo','TestBooking','JobImage','ClientImage','JobLocation','AdminDashMsg');
	
	public $components = array('Session',		
		
		  'Auth' => array('authenticate' => array('ClientUserInfo' => array(
													'userModel' => 'ClientUserInfo',
													'fields' => array(
														'username' => 'username',
														'password' => 'password'
													)
												),
												'AgentUserInfo' => array(
													'userModel' => 'AgentUserInfo',
													'fields' => array(
														'username' => 'username',
														'password' => 'password'
													)	
												)),
                'authorize' => array('Controller'),
                'loginAction' => array('controller' => 'home', 'action' => 'index'),
                'loginRedirect' => array('controller' => 'home','action' => 'index'),
                'logoutRedirect' => array('controller' => 'home', 'action' => 'index'),
          ),
    );
	
	//public $components = array('Session','Auth');	
	
	// only allow the login controllers only
	public function beforeFilter() {
		
		//$this->Auth->allow('login');
		if ($this->request->prefix == 'client_user_infos') {
            $this->layout = 'admin';
            // Specify which controller/action handles logging in:
            AuthComponent::$sessionKey = 'Auth.Client'; // solution from http://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
            $this->Auth->loginAction = array('controller'=>'users','action'=>'login');
            $this->Auth->loginRedirect = array('controller'=>'clients','action'=>'index');
            $this->Auth->logoutRedirect = array('controller'=>'home','action'=>'index');
            $this->Auth->authenticate = array(
                'Form' => array('Form' => array( 'userModel' => 'ClientUserInfo',
                                                         'fields' => array(
                                                                              'username' => 'username',
                                                                              'password' => 'password',
																		
                                                                              )
                                                            )
                                            )
            );
            $this->Auth->allow('login');

        } else if ($this->request->prefix == 'agent_user_infos') {
            // Specify which controller/action handles logging in:
            AuthComponent::$sessionKey = 'Auth.Agent'; // solution from http://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
            $this->Auth->loginAction = array('controller'=>'users','action'=>'login');
            $this->Auth->loginRedirect = array('controller'=>'agents','action'=>'index');
            $this->Auth->logoutRedirect = array('controller'=>'home','action'=>'index');

            $this->Auth->authenticate = array(
                'Form' => array('Form' => array( 'userModel' => 'AgentUserInfo',
                                                         'fields' => array(
                                                                              'username' => 'username',
                                                                              'password' => 'password',
																		
                                                                              )
                                                            )
                                            )
            );
        } else {
            // If we get here, it is neither a 'phys' prefixed method, not an 'admin' prefixed method.
            // So, just allow access to everyone - or, alternatively, you could deny access - $this->Auth->deny();
            $this->Auth->allow();           
        }
		$this->adminSessionDestroy();
		$this->generateAccessTokenForAPI();
    }
	
	function adminSessionDestroy()
	{
		if ($this->Session->check('Auth.User.admin_id')) {
			$this->Auth->logout();
            $this->redirect(array('controller'=>'home','action' => 'index'));
        }	
	}
	
	public function generateAccessTokenForAPI() {
		$salt = mt_rand();
		
		$email = "";
		$arrSession = $this->Session->read();
		
		if(isset($arrSession['Auth']) && !empty($arrSession['Auth'])) {
			if(isset($arrSession['Auth']['User']['agent_id']) && !empty($arrSession['Auth']['User']['agent_id'])) {
			 	$arrUserInfo = $arrSession['Auth']['User'];
				
				$email_address = $arrUserInfo['email_address'];
				$agent_id = $arrUserInfo['agent_id'];
				$created = $arrUserInfo['created'];
				
				$strConcat = $email_address . $salt . $agent_id . $created;
				$strAccessTokenHash = md5($strConcat);
				
				if(!DEFINED('ACCESSTOKEN')) {
				 	DEFINE('ACCESSTOKEN', $strAccessTokenHash);
				}
				if(!DEFINED('ACCESSTOKENSALT')) {
				 	DEFINE('ACCESSTOKENSALT', $salt);
				}
			}
		}
	}
	
	
	public function isAuthorized($user) {
		
		return true;
		
	}
	
	function send_email($from,$to,$subject,$message,$template)
	{
		
		$this->Email = new CakeEmail();
		//$this->set('data','TJ');
		$this->Email->to ($to);
		$this->Email->subject($subject);
		$this->Email->emailFormat('html');
		//$this->Email->template('default');
		$this->Email->from($from);
		$this->Email->template ($template);
		//$this->Email->return = 'gehlot.tejen111@gmail.com';
		//$this->Email->sender('gehlot.tejen111@gmail.com', 'Admin');
		//$this->Email->sendAs('both'); 
		ob_start();
		$this->Email->send($message);
		ob_end_clean();
	}
	
	function removespecialchar($str,$replace_this,$replace_with)
	{
		$returnStr = preg_replace($replace_this,$replace_with,$str);
		
		return $returnStr;
		
	}
	
	function saveAdminMsg($msg_text)
	{
		//echo "asasdasd";exit;
		$count = $this->AdminDashMsg->find('count');
		
		$min_query = "SELECT MIN(msg_id) as MsgId
						FROM admin_dash_msgs";
		
		$minIdArr = $this->AdminDashMsg->query($min_query);
		$minId = $minIdArr[0][0]['MsgId'];
		
		$query = "DELETE FROM admin_dash_msgs WHERE msg_id = ".$minId."";
		if($count >= 5)
		{
			$this->AdminDashMsg->query($query);	
		}
		$data['display_msg'] = $msg_text;
		$this->AdminDashMsg->save($data);
		return true;
	}
	
	function saveClientMsg($msg_text)
	{
		//echo "asasdasd";exit;
		$count = $this->ClientDashMsg->find('count');
		
		$min_query = "SELECT MIN(msg_id) as MsgId
						FROM client_dash_msgs";
		
		$minIdArr = $this->ClientDashMsg->query($min_query);
		$minId = $minIdArr[0][0]['MsgId'];
		
		$query = "DELETE FROM client_dash_msgs WHERE msg_id = ".$minId."";
		if($count >= 5)
		{
			$this->ClientDashMsg->query($query);	
		}
		$data['display_msg'] = $msg_text;
		$this->ClientDashMsg->save($data);
		return true;
	}
	
}
