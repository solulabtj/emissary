<?php

class AgentsController extends AppController {

    public $uses = array('Admin', 'AgentUserInfo', 'AgentJob');

    public function beforeFilter() {
        parent::beforeFilter();
        if (!$this->Session->check('Auth.User.agent_id')) {
            $this->redirect(array('controller' => 'home', 'action' => 'index'));
        }
    }

    public function index() {
        $this->layout = "agent";
    }

    public function testStart() {
        $this->layout = "agent";
    }

    public function testResult() {
        $this->layout = "agent";
    }

    public function takeTest() {
        //pr($this->Session->read());exit;

        $data['access_code'] = $this->Session->read('Auth.User.access_code');
        $data['access_mode'] = "WEB";

        $this->redirect(API_ROOT . "activations/book/" . $data['access_mode'] . "/" . $data['access_code']);
        $this->layout = "agent";
    }

    public function availableShops() {
        $this->layout = "agent";
    }

    public function viewJob() {
        $this->layout = "agent";
    }

    public function updateProfile() {
        $this->layout = "agent";
    }

    public function leaderBoard() {
        $this->layout = "agent";
    }

    public function fetch_data() {
        if ($this->request->is('ajax')) {                
                $res = $this->view_data();               
                $this->set('res', $res);                
            }
         else {
            echo "<table><tr><td>No data found</td><tr></table>";
            exit;
        }
    }

    public function view_data() {
        $Agents = $this->AgentUserInfo->query('SELECT aui.agent_name,aui.agent_rank,aui.profile_image,aui.points_earned FROM agent_user_infos aui GROUP BY aui.agent_id ORDER BY aui.points_earned DESC');
        //echo "<pre>";
        //pr($Agents);
        // $log = $this->AgentJob->getDataSource()->getLog(false, false);
        // debug($log);
        $message = $Agents;
        return $message;
    }

    public function accountSettings() {
        $agent_id = $this->Session->read('Auth.User.agent_id');
        if ($this->request->is('post')) {

            $data['password'] = AuthComponent::password($this->request->data['newPassword']);
            $data['agent_id'] = $agent_id;
            $oldPassword = AuthComponent::password($this->request->data['oldPassword']);
            if ($agent = $this->AgentUserInfo->find('first', array('conditions' => array('AgentUserInfo.agent_id' => $agent_id, 'AgentUserInfo.password' => $oldPassword)))) {
                if ($this->AgentUserInfo->save($data)) {
                    if (isset($this->request->data['receive_email'])) {
                        $from = array('admin@emissary.com' => 'EMISSARY');
                        $to = $this->Session->read('Auth.User.email_address');
                        $subject = 'Change Password';

                        $msg = 'Your password has been changed successfully.Your new password is ' . $this->request->data['newPassword'];
						
						$template ='';

                        $mail = $this->send_email($from, $to, $subject, $msg, $template);
                    }

                    $this->Session->setFlash('Password changed Successfully.', 'default', array('class' => 'successmsg'));
                } else {
                    $this->Session->setFlash('Unable to process your request. Please try again later.', 'default', array('class' => 'successmsg'));
                }
            } else {
                $this->Session->setFlash('Passwords do not match from database.', 'default', array('class' => 'successmsg'));
            }
        }

        $this->layout = "agent";
        //$this->redirect(array('controller'=>'agents','action'=>'accountSettings'));

        $agent_settings = $this->AgentUserInfo->find('first', array('fields' => array('AgentUserInfo.agent_id', 'AgentUserInfo.receive_email', 'AgentUserInfo.receive_push'), 'conditions' => array('AgentUserInfo.agent_id' => $agent_id)));


        $this->set('agent_settings', $agent_settings);
    }

    public function receive_email() {
        $agent_id = $this->Session->read('Auth.User.agent_id');

        $agent = $this->AgentUserInfo->find('first', array('fields' => array('AgentUserInfo.agent_id', 'AgentUserInfo.receive_email'), 'conditions' => array('AgentUserInfo.agent_id' => $agent_id)));

        $data['receive_email'] = $agent['AgentUserInfo']['receive_email'];
        $data['agent_id'] = $agent['AgentUserInfo']['agent_id'];

        if ($data['receive_email'] == '1') {
            $data['receive_email'] = '0';
        } else {
            $data['receive_email'] = '1';
        }

        if ($this->AgentUserInfo->save($data)) {
            echo "Email Notification changed successfully.";
            exit;
        } else {
            echo "Unable to update email notification.Please try again later.";
            exit;
        }
    }

    public function receive_push() {
        $agent_id = $this->Session->read('Auth.User.agent_id');

        $agent = $this->AgentUserInfo->find('first', array('fields' => array('AgentUserInfo.agent_id', 'AgentUserInfo.receive_push'), 'conditions' => array('AgentUserInfo.agent_id' => $agent_id)));

        $data['receive_push'] = $agent['AgentUserInfo']['receive_push'];
        $data['agent_id'] = $agent['AgentUserInfo']['agent_id'];

        if ($data['receive_push'] == '1') {
            $data['receive_push'] = '0';
        } else {
            $data['receive_push'] = '1';
        }

        if ($this->AgentUserInfo->save($data)) {
            echo "Push Notification changed successfully.";
            exit;
        } else {
            echo "Unable to update push notification.Please try again later.";
            exit;
        }
    }
	
	public function send_email($from, $to, $subject, $message, $template) {

        $this->Email = new CakeEmail();
		$this->Email->config('smtp');
        $this->Email->to($to);
        $this->Email->subject($subject);
        $this->Email->emailFormat('html');

        $this->Email->from($from);
        $this->Email->template($template);

        ob_start();
        $this->Email->send($message);
        ob_end_clean();
    }

}

?>