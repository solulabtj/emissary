<?php

class JobsController extends AppController {
	public $uses = array('AgentUserInfo','AgentDevice','AgentSocialLogin','AgentRating','TestSetting','ClientUserInfo','Job','TestResult','JobFilter','JobFilterCategory','JobCategory','AgentJob');
    public $helpers = array('Html', 'Form');
	public $components = array('RequestHandler');

	 function beforeFilter()
	 {
		 parent::beforeFilter();
		 $this->RequestHandler->ext = 'json';
	 }	

	// public function index() {
		// //$this->RequestHandler->renderAs($this, 'json');	
		// $users = $this->User->find('all');
        // $this->set(array(
            // 'Success' => $users,
            // '_serialize' => array('Success')
        // ));
    // }
	
	
	
	//New web services
	public function jobs_near_me() {
		$i = 'no';
		
		if($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code']))
		{   
			$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
			$limit = 15;
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$agent_id = $agent['AgentUserInfo']['agent_id'];
				if($test_status = $this->TestResult->find('first',array('conditions'=>array('TestResult.agent_id'=>$agent_id,'TestResult.is_passed'=>1), 'recursive'=>-1)))
				{
					$curr_lat = $this->request->data['current_lat'];
					$curr_lng = $this->request->data['current_lng'];
					
					if($nearby_jobs = $this->Job->query("SELECT *, ( 3959 * acos( cos( radians(".$curr_lat.") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(".$curr_lng.") ) + sin( radians(".$curr_lat.") ) * sin( radians( lattitude ) ) ) ) AS distance FROM jobs AS Job WHERE job_deadline >= '" .NOW. "' AND is_active = 1 AND is_blocked = 0 ORDER BY distance LIMIT ".$starting_number." , ".$limit.";"))
					{
						//pr($nearby_jobs);exit;
						$nearby_jobs = $this->get_distance($nearby_jobs,$curr_lat,$curr_lng);
						$nearby_jobs = array_values(array_filter($nearby_jobs));
						//pr($similardeals);
						if(!empty($nearby_jobs))
						{
							$message = $this->list_jobs($nearby_jobs);
							$status = 'yes';
						}
						else
						{
							$message = array();
						}
							
						$messsage = array_values(array_filter($message));
						$i='yes';
					}
					else 
					{
						$message = 'No jobs are listed near your location !';
					}	
				}
				else
				{
					$message = 'You haven\'t cleared any tests yet. Please clear a test first.';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
	public function jobs_near_home() {
		$i = 'no';
		if($this->request->is('post') && !empty($this->request->data['access_code']))
		{   
			$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
			$limit = 15;
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$agent_id = $agent['AgentUserInfo']['agent_id'];
				if($test_status = $this->TestResult->find('first',array('conditions'=>array('TestResult.agent_id'=>$agent_id,'TestResult.is_passed'=>1), 'recursive'=>-1)))
				{
					$agent_lat = $agent['AgentUserInfo']['lattitude'];
					$agent_lng = $agent['AgentUserInfo']['longitude'];
					
					if($nearby_jobs = $this->Job->query("SELECT *, ( 3959 * acos( cos( radians(".$agent_lat.") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(".$agent_lng.") ) + sin( radians(".$agent_lat.") ) * sin( radians( lattitude ) ) ) ) AS distance FROM jobs AS Job WHERE is_active = 1 AND is_blocked = 0 AND job_deadline > '".NOW."' ORDER BY distance LIMIT ".$starting_number." , ".$limit.";"))
					{
						$nearby_jobs = $this->get_distance($nearby_jobs,$agent_lat,$agent_lng);
						$nearby_jobs = array_values(array_filter($nearby_jobs));
						//pr($similardeals);
						if(!empty($nearby_jobs))
						{
							$message = $this->list_jobs($nearby_jobs);
							$status = 'yes';
						}
						else
						{
							$message = array();
						}
							
						$messsage = array_values(array_filter($message));
						$i='yes';
					}
					else 
					{
						$message = 'No nearby jobs to show';
					}	
				}
				else
				{
					$message = 'You haven\'t cleared any tests yet. Please clear a test first.';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
	public function search_job() {
		$this->RequestHandler->renderAs($this, 'json');
		$status = 'no';
		if($this->request->is('post') && !empty($this->request->data['search_key']) && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']))
		{ 
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$curr_lat = $this->request->data['current_lat'];
				$curr_lng = $this->request->data['current_lng'];
		
				$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
				$limit = 15;
				
				$search_key = $this->request->data['search_key'];
					
				$jobs = $this->Job->find('all', array('fields' => array('Job.*'),'conditions' => array('OR' =>array('Job.job_title LIKE' => '%'.$search_key.'%', 'Job.job_description LIKE' => '%'.$search_key.'%'),  'Job.is_blocked' =>0 , 'Job.is_active' => 1),'order' =>array('Job.modified' => 'DESC'), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));
								
				if(count($jobs) < $limit)
				{
					$clients = $this->ClientUserInfo->find('all', array('fields' => array('ClientUserInfo.client_id'),'conditions' => array('OR' =>array('ClientUserInfo.client_name LIKE' => '%'.$search_key.'%', 'ClientUserInfo.client_address LIKE' => '%'.$search_key.'%')), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));
					if(!empty($clients))
					{
						$j=0;
						foreach($clients as $client)
						{
							$client_jobs[$j] = $this->Job->find('all', array('fields' => array('Job.*'),'conditions' => array('Job.is_blocked' =>0 , 'Job.is_active' => 1, 'Job.client_id' => $clients[$j]['ClientUserInfo']['client_id']),'order' =>array('Job.modified' => 'DESC'), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));
						
						$j++;
						}
					}
					else{}
				
				}
								
					// pr($merchants);
			//	pr($similarshopdeals);
				
				if(!empty($client_jobs))
				{
					$client_jobs = array_filter($client_jobs);
					foreach($client_jobs as $search_clients)
					{
						$client_jobs = $search_clients;	
					}
					$jobs = array_merge($jobs,$client_jobs);
				}
				
				
				$jobs = array_values($jobs);
				$jobs = array_slice($jobs,0,$limit);
				$jobs = array_filter($jobs);
				
				$jobs = $this->get_distance($jobs,$curr_lat,$curr_lng);
				$jobs = array_values(array_filter($jobs));
				
				if(!empty($jobs))
				{
					
					$message = $this->list_jobs($jobs);
					$status = 'yes';
				//	pr($jobs);exit;
				}
				else
				{
					$message = array();
				}
				
					//pr($message);
			}
			else 
			{
				$message = 'Please login Again !';
			}
				
		}
		else{$message = 'invalid request';}	
        if($status == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
    }
	
	public function view_job_details() {
		$this->RequestHandler->renderAs($this, 'json');
		$status = 'no';
		$today = date("Y-m-d H:i:s");
		if($this->request->is('post')  && !empty($this->request->data['job_id'])/* && !empty($this->request->data['agent_job_id'])*/ && !empty($this->request->data['current_lat'])  && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code']))
		{ 
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$agent_id = $agent['AgentUserInfo']['agent_id'];
				$job_id = $this->request->data['job_id'];
				$curr_lat = $this->request->data['current_lat'];
				//$agent_job_id = $this->request->data['agent_job_id'];
				$curr_lng = $this->request->data['current_lng'];
				/*if($test_status = $this->TestResult->find('first',array('conditions'=>array('TestResult.agent_id'=>$agent_id,'TestResult.is_passed'=>1), 'recursive'=>-1)))
				{*/
					if($jobdetail = $this->Job->find('all',array('fields' =>array('Job.*'),'conditions'=>array('Job.job_id' => $this->request->data['job_id'], 'Job.is_blocked' => 0,'Job.is_active' => 1 ),'recursive'=>0)))
					{
						if($agent_job = $this->AgentJob->find('first',array('fields' =>array('AgentJob.*'),'conditions'=>array('AgentJob.job_id' => $job_id,'AgentJob.agent_id'=>$agent_id),'recursive'=>-1)))					
						{
							$joblocations = $this->JobLocation->find('first',array('fields'=>array('JobLocation.*','ClientAddress.*'),'conditions'=>array('JobLocation.job_location_id' => $agent_job['AgentJob']['job_location_id']),'recursive'=>0));
							$jobdetail[0]['AgentJob'] = $agent_job['AgentJob'];
							$jobdetail[0]['AgentJob']['JobAddress'] = $joblocations['ClientAddress'];	
						}
						
						$jobdetail = $this->get_distance($jobdetail,$curr_lat,$curr_lng);
						$jobdetail = array_values(array_filter($jobdetail));
						
						if(!empty($jobdetail))
						{
							
							$message = $this->list_jobs($jobdetail);
							$status = 'yes';
						//	pr($jobs);exit;
						}
						else
						{
							$message = array();
						}
					}
					else 
					{
						$message = 'Job not found. Please try again later';
					}
				/*}
				else 
				{
					$message = 'You haven\'t cleared any tests yet. Please clear a test first.';
				}*/
			}
			else 
			{
				$message = 'Please login Again !';
			}
			
		}
		else{$message = 'invalid request';}	
        if($status == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
    }
	
	public function view_client_details() {
		$this->RequestHandler->renderAs($this, 'json');
		$status = 'no';
		$today = date("Y-m-d H:i:s");
		if($this->request->is('post')  && !empty($this->request->data['client_id'])  && !empty($this->request->data['current_lat'])  && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code']))
		{ 
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$curr_lat = $this->request->data['current_lat'];
				$curr_lng = $this->request->data['current_lng'];
				
				if($clientdetail = $this->ClientUserInfo->find('first',array('fields' =>array('ClientUserInfo.*'),'conditions'=>array('ClientUserInfo.client_id' => $this->request->data['client_id'], 'ClientUserInfo.is_blocked' => 0,'ClientUserInfo.is_active' => 1 ),'recursive'=>-1)))
				{	
					$client_images = $this->ClientImage->find('all',array('fields' =>array('ClientImage.*'),'conditions'=>array('ClientImage.client_id' => $this->request->data['client_id'] ),'recursive'=>-1));
					
					if(!empty($client_images))
					{
						$k=0;
						foreach($client_images as $client_img)
						{
							if(!isset($client_img['ClientImage']['image_file_path']))
							{	
								$img_client[$k]['is_default'] = 0;
								$img_client[$k]['ClientImage']['image_path'] = '';
							}
							else
							{
								
								$image = $client_img['ClientImage']['image_file_path'];
								$img = WEBSITE_ROOT.'img/'.$image;
								$img_client[$k]['image_path'] = $img;
								$img_client[$k]['is_default'] = $client_img['ClientImage']['is_default'];
								
							}
							$k++;	
						}	
					}	
					$clientdetail['ClientImages']  = $img_client;
					//pr($clientdetail);exit;	
					$message = $clientdetail;
					$status='yes';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
        if($status == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
    }
	
	
	public function get_available_shops() {
		$i = 'no';
		
		if($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code']))
		{   
			$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
			$limit = 15;
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$agent_id = $agent['AgentUserInfo']['agent_id'];
				if($test_status = $this->TestResult->find('first',array('conditions'=>array('TestResult.agent_id'=>$agent_id,'TestResult.is_passed'=>1), 'recursive'=>-1)))
				{
					if($job_filter = $this->JobFilter->find('first',array('fields'=> array(),'conditions'=>array('JobFilter.agent_id'=>$agent_id,'JobFilter.is_active'=>1), 'recursive'=>1)))
					{
						
						$min_compensation = $job_filter['JobFilter']['minimum_compensation'];
						$max_compensation = $job_filter['JobFilter']['maximum_compensation'];
						$min_distance = $job_filter['JobFilter']['min_distance'];
						$max_distance = $job_filter['JobFilter']['max_distance'];
						$due_date_span = $job_filter['JobFilter']['due_date_span'];
						$curr_lat = $this->request->data['current_lat'];
						$curr_lng = $this->request->data['current_lng'];
						$start_date = NOW;
						$end_date = date('Y-m-d h:i:s', strtotime($start_date. ' +'.$due_date_span.' days')); //exit;
						
						if($job_categories = $this->JobCategory->find('all',array('fields'=> array(),'conditions'=>array('JobCategory.is_active'=>1), 'recursive'=>-1)))
						{
							foreach($job_filter['JobFilterCategory'] as $jfcat)
							{
								if($jfcat['is_checked']==1)
								{
									$jfcategory[] = $jfcat['job_category_id']; 	
								}	
							}
							//pr($jfcategory);exit;
							$conditions = "WHERE job_deadline >= '" .$start_date. "' AND job_deadline <= '" .$end_date. "' AND job_compensation >= '" .$min_compensation. "' AND job_compensation <= '" .$max_compensation. "' AND is_active = 1 AND is_blocked = 0 HAVING distance >= ".$min_distance." AND distance <= ".$max_distance." ORDER BY distance LIMIT ".$starting_number." , ".$limit."";
							
							if($nearby_jobs = $this->Job->query("SELECT *, ( 3959 * acos( cos( radians(".$curr_lat.") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(".$curr_lng.") ) + sin( radians(".$curr_lat.") ) * sin( radians( lattitude ) ) ) ) AS distance FROM jobs AS Job ".$conditions.";"))
							{
								//pr($nearby_jobs);exit;
								$k=0;
								foreach($nearby_jobs as $jobs)
								{
									if(!in_array($nearby_jobs[$k]['Job']['job_category_id'],$jfcategory))
									{
										unset($nearby_jobs[$k]);
									}	
									$k++;
								}
								$nearby_jobs = array_values(array_filter($nearby_jobs));
								$nearby_jobs = $this->get_distance($nearby_jobs,$curr_lat,$curr_lng);
								$nearby_jobs = array_values(array_filter($nearby_jobs));
								//pr($similardeals);
								if(!empty($nearby_jobs))
								{
									$nearby_jobs = $this->list_jobs($nearby_jobs);
									$nearby_jobs['JobFilter'] = $job_filter;
									$nearby_jobs['JobCategory'] = $job_categories;
									$message = $nearby_jobs;
									$i = 'yes';
								}
								else
								{
									$message = 'No jobs are listed as per your requirements !';
								}
							}
							else 
							{
								$message = 'No jobs are listed near your location !';
							}	
						}
						else
						{
							$message = 'You haven\'t cleared any tests yet. Please clear a test first.';
						}
					}
					else
					{
						$message = 'You haven\'t cleared any tests yet. Please clear a test first.';
					}
				}
				else
				{
					$message = 'You haven\'t cleared any tests yet. Please clear a test first.';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
}