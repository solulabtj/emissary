<?php
class ClientsController extends AppController {

	public $uses = array('ClientUserInfo','Admin','ClientImage','Job');

    public function beforeFilter() {
        parent::beforeFilter();
		//pr($this->Session->read('Auth.User'));exit;
       	if (!$this->Session->check('Auth.User.client_id')) {
            $this->redirect(array('controller'=>'home','action' => 'index'));
        }
    }
	
	public function index()
	{
		$this->layout = "client";
	}
	
	public function update_profile()
	{
		$this->layout = "client";
		$client_id = $this->Session->read('Auth.User.client_id');
		
		if($this->request->is('post'))
		{			
        	$post = $this->request->data;
			//pr($post);exit;
			$data['client_id'] = $post['client_id'];
			$data['company_name'] = $post['company_name'];
			$data['company_overview'] = $post['company_overview'];
			$data['mobile_number'] = $post['mobile_number'];
			$data['agency_name'] = $post['agency_name'];
			$data['client_address'] = $post['client_address'];
			$data['paypal_id'] = $post['paypal_id'];
			$data['working_hour_start'] = $post['working_hour_start'];
			$data['working_hour_end'] = $post['working_hour_end'];
			
			if(isset($post['monday']))
			{
				$data['monday'] = $post['monday'];
			}
			else
			{
				$data['monday'] = '0';
			}
			
			if(isset($post['tuesday']))
			{
				$data['tuesday'] = $post['tuesday'];
			}
			else
			{
				$data['tuesday'] = '0';
			}
			
			if(isset($post['wednesday']))
			{
				$data['wednesday'] = $post['wednesday'];
			}
			else
			{
				$data['wednesday'] = '0';
			}
			
			if(isset($post['thursday']))
			{
				$data['thursday'] = $post['thursday'];
			}
			else
			{
				$data['thursday'] = '0';
			}
			
			if(isset($post['friday']))
			{
				$data['friday'] = $post['friday'];
			}
			else
			{
				$data['friday'] = '0';
			}
			
			if(isset($post['saturday']))
			{
				$data['saturday'] = $post['saturday'];
			}
			else
			{
				$data['saturday'] = '0';
			}
			
			if(isset($post['sunday']))
			{
				$data['sunday'] = $post['sunday'];
			}
			else
			{
				$data['sunday'] = '0';
			}
			
			//pr($_FILES);exit;
			if (!empty($_FILES['company_logo']['name'])) {
				$imageExt = array('png','jpg','jpeg','PNG','JPG','JPEG');
				$pathi = "";
				$target_dir	=	WEBSITE_WEBROOT.'img/';
				//echo $target_dir;exit;
				//$pathv = "";
				
				$microtime = microtime();

				$fileName = $_FILES['company_logo']["name"];
				$extension = explode(".", $fileName);
				$Path = "";

				if (in_array($extension[1], $imageExt)) {
					$Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					$target_dir2 = $target_dir . $Pathi;
					move_uploaded_file($_FILES['company_logo']["tmp_name"], $target_dir2);
				}/* else {
					$Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					$target_dir2 = $target_dir . $Pathv;
					move_uploaded_file($val["tmp_name"], $target_dir2);
				}*/
				$data['company_logo'] = $Pathi;
				
				
			}



			/* Company image update */


			if (!empty($_FILES['company_image']['name'])) {
				//echo 'sdfsdfsdfds';exit;
				$imageExt = array('png','jpg','jpeg','PNG','JPG','JPEG');
				$pathi = "";
				$target_dir	=	WEBSITE_WEBROOT.'img/';
				//echo $target_dir;exit;
				//$pathv = "";
				
				$microtime = microtime();

				$fileName = $_FILES['company_image']["name"];
				$extension = explode(".", $fileName);
				$Path = "";

				if (in_array($extension[1], $imageExt)) {
					$Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					$target_dir2 = $target_dir . $Pathi;
					move_uploaded_file($_FILES['company_image']["tmp_name"], $target_dir2);
				}/* else {
					$Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
					$target_dir2 = $target_dir . $Pathv;
					move_uploaded_file($val["tmp_name"], $target_dir2);
				}*/
				$comp_img['image_file_path'] = $Pathi;
				
				
			}


			
			$this->ClientUserInfo->id = $data['client_id'];
			if($this->ClientUserInfo->save($data))
			{		
				$comp_img['client_id'] = $data['client_id'];
				$comp_img['client_image_id'] = $this->request->data['client_image_id'];
				$comp_img['is_default'] = 1;
				//$this->ClientImage->client_image_id = $this->request->data['client_image_id'];
				if($this->ClientImage->save($comp_img))
				{
					$this->Session->setFlash('Client details updated successfully.','default',array('class' => 'successmsg'));
	                $this->redirect(array('controller'=>'clients','action' => 'update_profile'));
	                exit();
				}
				else
				{
					$this->Session->setFlash('Unable to update client data.','default',array('class' => 'successmsg'));
	                $this->redirect(array('controller'=>'clients','action' => 'update_profile'));
	                exit();
				}
						
			}
			else
			{
				$this->Session->setFlash('Unable to update client details.','default',array('class' => 'errormsg'));
                $this->redirect(array('controller'=>'clients','action' => 'update_profile'));
                exit();
			}
			
			
		}
		
		
		$singleclient= $this->ClientUserInfo->find('first',array('fields' => array('ClientUserInfo.*'),'conditions'=>array('client_id'=>$client_id), 'recursive' => -1));
        $clientimage = $this->ClientImage->find('first',array('conditions'=>array('ClientImage.client_id' => $client_id,'ClientImage.is_default'=>1),'recursive'=>-1));
        
		$listing['ClientUserInfo'] = $singleclient['ClientUserInfo'];
		if(!empty($clientimage))
		{
			 $listing['ClientImage'] = $clientimage['ClientImage'];
		}
       

        //pr($listing); exit;
        $this->set('listing',$listing);
		
	}
	
    public function job_post()
	{
		$get_job_list = $this->Job->find('all');
		$this->set('get_job_list',$get_job_list);
		$this->layout = "client";
	}
	
	function activate_inactive($job_id=null,$job_status = null){
		//echo $job_status;exit;
        if($job_status==1){
            $status = 0;
        }else{
            $status = 1;
        }
        
        $this->Job->id = $job_id; //exit;
        
        if($this->Job->saveField('is_active',$status)){
			//echo "success"; exit;
            $this->Session->setFlash('Status changed successfully.','default',
            array('class' => 'successmsg'));
            $this->redirect(array('controller'=>'clients','action' => 'job_post'));
            exit();
        } else {
			//echo "failed"; exit;
            $this->Session->setFlash('Error ! status not changed.Please Try Again.','default',
            array('class' => 'errormsg'));
            $this->redirect(array('controller'=>'clients','action' => 'job_post'));
            exit();
        }
        
        
    }


	public function new_job_post()
	{


		if($this->request->is('post')) {
			$new_job_post_data['client_id'] = $this->Session->read('Auth.User.client_id');
			$from_date_format =  $this->request->data['date_from'];
			$to_date_format = $this->request->data['date_to'];

			$from_date = new DateTime($from_date_format);
			$to_date = new DateTime($to_date_format);

			//echo $date->format('d.m.Y'); // 31.07.2012
			//echo $date->format('Y-m-d H:i:s'); // 31-07-2012
			$new_job_post_data['job_start_date'] = $from_date->format('Y-m-d').' '.$this->request->data['time_span_from'];
			$new_job_post_data['job_end_date'] =  $to_date->format('Y-m-d').' '.$this->request->data['time_span_to'];
			$new_job_post_data['min_agent_rank'] = $this->request->data['agent_rank'];
			$new_job_post_data['job_compensation'] = str_replace('$', '', $this->request->data['cash_value']);
			$new_job_post_data['reward_type'] =  $this->request->data['reward'];
			$new_job_post_data['job_description'] = $this->request->data['job_overview'];
			$new_job_post_data['max_allowed_requests'] = $this->request->data['request_number'];
			$new_job_post_data['is_active'] = $this->request->data['job_status'];
			$new_job_post_data['test_passed'] = $this->request->data['test_status'];

			

			//pr($new_job_post_data); exit;
			$this->Job->save($new_job_post_data);
		
		}
		$this->layout = "client";
	}
	
	
	public function surveyList()
	{
		$this->layout = "client";
	}
	
}

?>