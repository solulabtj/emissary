<?php
class PaymentsController extends AppController {

	public $uses = array('Admin','ClientSubscription','ClientUserInfo','AgentUserInfo','AgentDevice','AgentSocialLogin','TestSetting','TestBooking');

    public function beforeFilter() {
        parent::beforeFilter();
       /*	if (!$this->Session->check('Auth.User.client_id')) {
            $this->redirect(array('controller'=>'home','action' => 'index'));
        }*/
    }
	
	public function client_subscription()
	{
		$this->layout = "client";
	}
	
	public function ipn_callback_old()
	{
		$paypalmode = 'sandbox';
		$from = 'emissary_payment@gmail.com';
		$to = 'tejendra@solulab.com';
		$subject = "emissary recurring payment";
		$message = 'This is tset';
		$template = 'default';
		//$this->send_email($from,$to,$subject,$message,$template);		
		if($_POST)
		{
			if($paypalmode=='sandbox')
			{
				$paypalmode     =   '.sandbox';
			}
			$req = 'cmd=' . urlencode('_notify-validate');
			foreach ($_POST as $key => $value) {
				$value = urlencode(stripslashes($value));
				$req .= "&$key=$value";
			}
			
			$header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
			$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
			$header .= "Host:www.sandbox.paypal.com\r\n";
			$header .= "Connection:close\r\n";
			$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
			
			$message1 = $req;
			$this->send_email($from,$to,$subject,$message1,$template);
				
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr');
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			$res = curl_exec($ch);
			curl_close($ch);
			
			$message1 = 'resResult ::::::'.$res;
			$this->send_email($from,$to,$subject,$message1,$template);
			
			if(strcmp($res, 'VERIFIED') == 0)
			{
				$post_vars = json_encode($_REQUEST);
				$message1 = $post_vars;
				$this->send_email($from,$to,$subject,$message1,$template);	
				
				$message1 = 'resresult ::::::'.$res;
				$this->send_email($from,$to,$subject,$message1,$template);	
				
				$data['transaction_id'] = $_POST['txn_id'];
				if($existing_transaction = $this->ClientSubscription->find('first',array('conditions'=>array('ClientSubscription.transaction_id'=> $data['transaction_id']),'recursive'=>-1)))
				{	
					$data['payer_id'] = $_POST['payer_id'];
					$data['payer_firstname'] = $_POST['first_name'];
					$data['payer_lastname'] = $_POST['last_name'];
					$data['payer_email'] = $_POST['payer_email'];
					$paymentdate = $_POST['payment_date'];
					$data['transaction_status'] = $_POST['payment_status'];
					$data['transaction_time']= date('Y-m-d h:i:s',strtotime($paymentdate));
					$otherstuff = json_encode($_POST);
					
					$this->ClientSubscription->id = $existing_transaction['ClientSubscription']['client_subscription_id'];
					
					if($renew_sub = $this->ClientSubscription->save($data))
					{
						if($data['transaction_status'] == 'Completed')
						{
							//$subscription_expiration = $data['transaction_time'];
							$subscription_expiration = date("Y-m-d h:i:s", strtotime("+1 month", $data['transaction_time']));
							$client = $this->ClientUserInfo->find('first',array('conditions'=>array('ClientUserInfo.paypal_id'=> $data['payer_email']),'recursive'=>-1));
							$this->ClientUserInfo->id = $client['ClientUserInfo']['client_id'];
							if($renew_sub = $this->ClientUserInfo->saveField('subscription_expiration',$subscription_expiration))
							{
								
							}
							else
							{
								//mail to admin database error occured for this transaction id and transaction status	
							}
						}
						else
						{
							//mail to admin database error occured for this transaction id and transaction status		
						}
						//Transaction Successful
					}
					else
					{
						//mail to admin database error occured for this transaction id and transaction status	
					}
				}
				else
				{
					$paymentdate = $_POST['payment_date'];
					$data['transaction_status'] = $_POST['payment_status'];
					$data['transaction_time']= date('Y-m-d h:i:s',strtotime($paymentdate));
					$otherstuff = json_encode($_POST);
		
					if($renew_sub = $this->ClientSubscription->save($data))
					{
						//Transaction Successful
					}
					else
					{
						//mail to admin database error occured for this transaction id and transaction status	
					}	
				}
			}
			
		}

		
		//$this->layout = "client";
	}
	
	function ipn_callback()
	{
		
		$paypalmode = 'sandbox';
		$from = 'emissary_payment@gmail.com';
		$to = 'tejendra@solulab.com';
		$subject = "emissary recurring payment";
		$message = 'This is tset';
		$template = 'default';
			
		/*	DEBUG MAIL
			
			$message1 = 'in script';
		$this->send_email($from,$to,$subject,$message1,$template);*/		
		
		
		
		// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
		// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
		// Set this to 0 once you go live or don't require logging.
		define("DEBUG", 1);
		// Set to 0 once you're ready to go live
		define("USE_SANDBOX", 0);
		define("LOG_FILE", "./ipn.log");
		// Read POST data
		// reading posted data directly from $_POST causes serialization
		// issues with array data in POST. Reading raw POST data from input stream instead.
		/*DEBUG MAIL

		$message1 = 'read post data';
		$this->send_email($from,$to,$subject,$message1,$template);*/	
		
		
		$raw_post_data = file_get_contents('php://input');
		//$raw_post_data = $_POST;
		
		$this->send_email($from,$to,"Mail1",$raw_post_data,$template);
//		$this->send_email($from,$to,"Mail1",serialize($raw_post_data),$template);
		
		$raw_post_array = explode('&', $raw_post_data);
		
		$message1 = json_encode($raw_post_array);
		$this->send_email($from,$to,"Mail2",$message1,$template);	
		
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
		  $keyval = explode ('=', $keyval);
		  if (count($keyval) == 2)
			 $myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		
		$message1 = json_encode($myPost);
		$this->send_email($from,$to,"Mail3",$message1,$template);	
		
		header('HTTP/1.1 200 OK'); 

		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		foreach ($myPost as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}

		$this->send_email($from,$to,"mail4",$req,$template);
		
		if(USE_SANDBOX == true) {
			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		} else {
			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
		}
		
		
		//file socket
		/*$fp = fsockopen($paypal_url,"80",$err_num,$err_str,30); 
		  if(!$fp) {
			  
			 // could not open the connection.  If loggin is on, the error message
			 // will be in the log.
			 $this->last_error = "fsockopen error no. $errnum: $errstr";
			 $this->log_ipn_results(false);       
			 $this->send_email($from,$to,'error',$this->last_error,$template);
			 return false;
			 
		  } else { 
	 		$ipn_response='';
			 // Post the data back to paypal
			 fputs($fp, "POST /cgi-bin/webscr HTTP/1.1\r\n"); 
			 fputs($fp, "Host: www.sandbox.paypal.com\r\n"); 
			 fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n"); 
			 fputs($fp, "Content-length: ".strlen($req)."\r\n"); 
			 fputs($fp, "Connection: close\r\n\r\n"); 
			 fputs($fp, $req . "\r\n\r\n"); 
	
			 // loop through the response from the server and append to variable
			 while(!feof($fp)) { 
				$ipn_response .= fgets($fp, 1024); 
			 } 
	
			 fclose($fp); // close connection
	
		  }
//		$this->send_email($from,$to,"ipnresponse",$ipn_response,$template);
		*/
		
		
		$ch = curl_init($paypal_url);
		if ($ch == FALSE) {
			return FALSE;
		}
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		if(DEBUG == true) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		}
		// CONFIG: Optional proxy configuration
		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
		// Set TCP timeout to 30 seconds
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
		// of the certificate as shown below. Ensure the file is readable by the webserver.
		// This is mandatory for some environments.
		//$cert = __DIR__ . "./cacert.pem";
		//curl_setopt($ch, CURLOPT_CAINFO, $cert);
		$res = curl_exec($ch);
		

		$this->send_email($from,$to,'Mail5',$ipn_response,$template);
		
		/*if (curl_errno($ch) != 0) // cURL error
			{
			if(DEBUG == true) {	
				error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
			}
			curl_close($ch);
			exit;
		} else {
				// Log the entire HTTP response if debug is switched on.
				if(DEBUG == true) {
					error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
					error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
				}
				curl_close($ch);
		}
		
		$message1 = 'before verified';
		$this->send_email($from,$to,$subject,$message1,$template);*/
		
		// Inspect IPN validation result and act accordingly
		// Split response headers and payload, a better way for strcmp
		
		
		
		
		//$res = $ipn_response;		
		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));
		$this->send_email($from,$to,'Mail6',$res,$template);
		$this->send_email($from,$to,'Mail7',serialize($_REQUEST),$template);
		if (strcmp ($res, "VERIFIED") == 0) {
			
			$post_vars = json_encode($_REQUEST);
				$message1 = $post_vars;
				$message1 = 'in verified';
				$this->send_email($from,$to,$subject,$message1,$template);	
				echo 'VERIFIED';exit;
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment and mark item as paid.
			// assign posted variables to local variables
			//$item_name = $_POST['item_name'];
			//$item_number = $_POST['item_number'];
			//$payment_status = $_POST['payment_status'];
			//$payment_amount = $_POST['mc_gross'];
			//$payment_currency = $_POST['mc_currency'];
			//$txn_id = $_POST['txn_id'];
			//$receiver_email = $_POST['receiver_email'];
			//$payer_email = $_POST['payer_email'];
			
			if(DEBUG == true) {
				error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
			}
			
			
		} else if (strcmp ($res, "INVALID") == 0) {
			$message1 = 'in invalid';
				$this->send_email($from,$to,$subject,$message1,$template);	
				echo 'INVALID';exit;
			// log for manual investigation
			// Add business logic here which deals with invalid IPN messages
			if(DEBUG == true) {
				error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
			}
			
			
		}
		else
		{
			$message1 = 'in elsewhere';
				$this->send_email($from,$to,$subject,$message1,$template);		
				echo 'EMPTY';exit;
		}

	}
	
	
	function subscription()
	{
		$this->layout = 'client';	
	}
	
}

?>