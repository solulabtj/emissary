<?php
class Job extends AppModel {
	var $name = 'Job'; 
	var $primaryKey = 'job_id';
	
	public $belongsTo = array(
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
		'Coupon'=>array('className'=>'Coupon',
		'foreignKey'=>'coupon_id'
		),
		'JobCategory'=>array('className'=>'JobCategory',
		'foreignKey'=>'job_category_id'
		),
    );
	public $hasOne = array(
        'Survey'=> array(
			'foreignKey' => 'survey_id',
			'dependent' => false
		),
    );
	public $hasMany = array(
		'JobImage'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),	
		'AgentJob'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),	
		'Payment'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),
		'JobLocation'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),	
			
    );
	
}