<?php
class JobFilterCategory extends AppModel {
	var $name = 'JobFilterCategory'; 
	var $primaryKey = 'job_filter_category_id';
	
	public $belongsTo = array(
		'JobFilter'=>array('className'=>'JobFilter',
		'foreignKey'=>'job_filter_id'
		),
    );
	
	
}