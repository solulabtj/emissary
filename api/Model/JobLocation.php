<?php
class JobLocation extends AppModel {
	var $name = 'JobLocation'; 
	var $primaryKey = 'job_location_id';
	public $belongsTo = array(
		'Job'=>array('className'=>'Job',
		'foreignKey'=>'job_id'
		),
		'ClientAddress'=>array('className'=>'ClientAddress',
		'foreignKey'=>'client_address_id'
		),
    );
	
	public $hasMany = array(
		'AgentJob'=> array(
			'foreignKey' => 'job_location_id',
			'dependent' => false
		),		
    );
		

}