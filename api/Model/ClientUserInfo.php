<?php
class ClientUserInfo extends AppModel {
	var $name = 'ClientUserInfo'; 
	var $primaryKey = 'client_id';
	public $hasMany = array(
		'Job'=> array(
			'foreignKey' => 'client_id',
			'dependent' => false
		),	
		'Survey'=> array(
			'foreignKey' => 'client_id',
			'dependent' => false
		),	
		'ClientImage'=> array(
			'foreignKey' => 'client_id',
			'dependent' => false
		),	
		'AgentJob'=> array(
			'foreignKey' => 'job_id',
			'dependent' => false
		),
		'AgentCoupon'=> array(
			'foreignKey' => 'client_id',
			'dependent' => false
		),	
		'Payment'=> array(
			'foreignKey' => 'client_id',
			'dependent' => false
		),	
		'AgentRating'=> array(
			'foreignKey' => 'agent_id',
			'dependent' => false
		),
		'ClientAddress'=> array(
			'foreignKey' => 'client_id',
			'dependent' => false
		),			
    );
	
	public $belongsTo = array(
		
    );
		
 	var $validate = array(
             'mobile_number' => array(				
				 array(
					 'allowEmpty' => false,
					 'rule' => 'numeric',
					 'message' => 'Please enter valid mobile no.'),
				 array(
					 'rule' => 'isUnique',
					 
					 'allowEmpty' => false,
					 'on' => 'create', // here
					 'last' => false,
					 'message' => 'This number already exists, please try another')					
			 ),
            'email_address' => array(				
				array(
					'allowEmpty' => false,
					'rule' => 'email',
					'message' => 'Please enter valid email'),
				array(
					'rule' => 'isUnique',
					
					'allowEmpty' => false,
					'on' => 'create', // here
					'last' => false,
					'message' => 'Email already exists, try another')					
			)			
		);	
}