<?php
class AgentCoupon extends AppModel {
	var $name = 'AgentCoupon'; 
	var $primaryKey = 'agent_coupon_id';
	
	public $hasMany = array(
    );
	public $belongsTo = array(
		'AgentUserInfo'=>array('className'=>'AgentUserInfo',
		'foreignKey'=>'agent_id'
		),
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
		'Coupon'=>array('className'=>'Coupon',
		'foreignKey'=>'coupon_id'
		),
    );
		

}