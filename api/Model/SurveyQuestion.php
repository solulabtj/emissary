<?php
class SurveyQuestion extends AppModel {
	var $name = 'SurveyQuestion'; 
	var $primaryKey = 'survey_question_id';
	public $hasMany = array(
		'SurveyQuestionOption'=> array(
			'foreignKey' => 'survey_question_id',
			'dependent' => false
		),	
    );
	
	public $belongsTo = array(
		'SurveyCategory'=>array('className'=>'SurveyCategory',
		'foreignKey'=>'survey_category_id'
		),
    );
		

}