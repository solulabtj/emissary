<?php
class AgentUserInfo extends AppModel {
	var $name = 'AgentUserInfo'; 
	var $primaryKey = 'agent_id';
	public $hasMany = array(
		'AgentDevice'=> array(
			'foreignKey' => 'agent_id',
			'dependent' => false
		),	
		'AgentRating'=> array(
			'foreignKey' => 'agent_id',
			'dependent' => false
		),	
		'TestResult'=> array(
			'foreignKey' => 'agent_id',
			'dependent' => false
		),	
		'AgentJob'=> array(
			'foreignKey' => 'agent_id',
			'dependent' => false
		),	
		'AgentCoupon'=> array(
			'foreignKey' => 'agent_id',
			'dependent' => false
		),	
		'AgentPayment'=> array(
			'foreignKey' => 'agent_id',
			'dependent' => false
		),	
    );
	
	public $belongsTo = array(
    );
		
 	var $validate = array(
             'mobile_number' => array(				
				 array(
					 'allowEmpty' => false,
					 'rule' => 'numeric',
					 'message' => 'Please enter valid mobile no.'),
				 array(
					 'rule' => 'isUnique',
					 
					 'allowEmpty' => false,
					 'on' => 'create', // here
					 'last' => false,
					 'message' => 'This number already exists, please try another')					
			 ),
            'email_address' => array(				
				array(
					'allowEmpty' => false,
					'rule' => 'email',
					'message' => 'Please enter valid email'),
				array(
					'rule' => 'isUnique',
					
					'allowEmpty' => false,
					'on' => 'create', // here
					'last' => false,
					'message' => 'Email already exists, try another')					
			),			
		);	
}