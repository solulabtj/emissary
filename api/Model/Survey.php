<?php
class Survey extends AppModel {
	var $name = 'Survey'; 
	var $primaryKey = 'survey_id';
	
	public $belongsTo = array(
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),
    );
	
	public $hasOne = array(
        'Job'=> array(
			'foreignKey' => 'survey_id',
			'dependent' => false
		),
    );
	public $hasMany = array(
		
			
    );
	
}