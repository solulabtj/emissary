<?php
class JobCategory extends AppModel {
	var $name = 'JobCategory'; 
	var $primaryKey = 'job_category_id';
	public $hasMany = array(
		'Job'=> array(
			'foreignKey' => 'job_category_id',
			'dependent' => false
		),
			
    );
	public $belongsTo = array(
    );
		

}