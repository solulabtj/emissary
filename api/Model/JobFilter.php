<?php
class JobFilter extends AppModel {
	var $name = 'JobFilter'; 
	var $primaryKey = 'job_filter_id';
	
	public $hasMany = array(
		'JobFilterCategory'=> array(
			'foreignKey' => 'job_filter_id',
			'dependent' => false
		),		
    );
	public $belongsTo = array(
    );
	
	
}