<?php
class AgentRating extends AppModel {
	var $name = 'AgentRating'; 
	var $primaryKey = 'agent_rating_id';
	public $belongsTo = array(
		'AgentUserInfo'=>array('className'=>'AgentUserInfo',
		'foreignKey'=>'agent_id'
		),	
		'ClientUserInfo'=>array('className'=>'ClientUserInfo',
		'foreignKey'=>'client_id'
		),	
    );
		
 	
}