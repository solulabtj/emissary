<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Form</title>
<style>
body { height:100px; margin:0 auto; width:680px;}   
.Myform { background:#E9E9E9; padding:10px; margin-top:25%; width:50%; margin-left:17%;}
.form_field { margin-bottom:10px;}
.label { float:left; padding-right:6px; width:200px; text-align:left;}
input {
    border: medium 1px;
    line-height: 24px;
    width: 60%;
}
button { background:#39A9F3; padding:10px; border:none; color:#fff; border-radius:10px;}
</style>

<script>
function checkPasswordMatch() {
    var password = $("#NewPassword").val();
    var confirmPassword = $("#ConfirmPassword").val();

    if (password != confirmPassword)
        $("#divCheckPasswordMatch").html("Passwords do not match!");
    else
        $("#divCheckPasswordMatch").html("Passwords match.");
}


$(document).ready(function () {
   $("#ConfirmPassword").keyup(checkPasswordMatch);
});
</script>

</head>

<body>
<?php isset($msg)?$msg:'';
pr($msg); ?>
<?php echo $this->Form->create('update_password'); ?>
<div class="form_field">
<div class="label">Password</div>
<div class="input"><input type="password" id='NewPassword' required="required"></div>
</div>
<div class="form_field">
<div class="label">Confirm Password</div>
<div class="input"><input type="password" id='ConfirmPassword' name='new_password' required="required"></div>
</div>
 <div class="registrationFormAlert" id="divCheckPasswordMatch">
</div>
<div class="form_field">

 <button type="Submit"  >Update Password</button> 
</div>

<?php echo $this->Form->end(); ?>
</form>

</body>
</html>
