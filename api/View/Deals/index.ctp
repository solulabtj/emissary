<h1>Deals DB</h1>
<p><?php echo $this->Html->link('Add Deal', array('action' => 'add')); ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
		<th>Userid</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $cateogrys array, printing out cateogry info -->

    <?php foreach ($cateogrys as $cateogry): ?>
    <tr>
        <td><?php echo $cateogry['Deal']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $cateogry['Deal']['name'],
                    array('action' => 'view', $cateogry['Deal']['id'])
                );
            ?>
        </td>
		<td><?php echo $cateogry['Deal']['userid']; ?></td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $cateogry['Deal']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $cateogry['Deal']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $cateogry['Deal']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>