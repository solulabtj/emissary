<h1>Add Category</h1>
<?php
echo $this->Form->create('Category');
echo $this->Form->input('name');
echo $this->Form->input('description');
echo $this->Form->input('Category.parent_id',array('type'=>'select','empty'=>'Select Parent Category','options'=>$categorylist)); 
echo $this->Form->end('Save Category');
?>