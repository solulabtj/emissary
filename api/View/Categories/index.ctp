<h1>Categorys DB</h1>
<p><?php echo $this->Html->link('Add Category', array('action' => 'add')); ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
		<th>Userid</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $cateogrys array, printing out cateogry info -->

    <?php foreach ($cateogrys as $cateogry): ?>
    <tr>
        <td><?php echo $cateogry['Category']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $cateogry['Category']['name'],
                    array('action' => 'view', $cateogry['Category']['id'])
                );
            ?>
        </td>
		<td><?php echo $cateogry['Category']['userid']; ?></td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $cateogry['Category']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $cateogry['Category']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $cateogry['Category']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>