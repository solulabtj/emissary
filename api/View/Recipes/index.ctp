
<h1>Phone DB</h1>
<p><?php echo $this->Html->link('Add Phone', array('action' => 'add')); ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
		<th>Manufacturer</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $recipes array, printing out recipe info -->

    <?php foreach ($recipes as $recipe): ?>
    <tr>
        <td><?php echo $recipe['Recipe']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $recipe['Recipe']['name'],
                    array('action' => 'view', $recipe['Recipe']['id'])
                );
            ?>
        </td>
		<td><?php echo $recipe['Recipe']['manufacturer']; ?></td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete/'.$recipe['Recipe']['id'].'.json'),
					//array('action' => 'delete', $recipe['Recipe']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $recipe['Recipe']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $recipe['Recipe']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>