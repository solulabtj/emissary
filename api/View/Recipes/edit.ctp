
<h1>Edit Recipe</h1>
<?php
echo $this->Form->create('Recipe',array('action'=>'edit/'.$phonedata['Recipe']['id'].'.json'));
echo $this->Form->input('name',array('value'=>$phonedata['Recipe']['name']));
echo $this->Form->input('manufacturer',array('value'=>$phonedata['Recipe']['manufacturer']));
echo $this->Form->input('description', array('rows' => '3','value'=>$phonedata['Recipe']['description']));
echo $this->Form->input('id', array('type' => 'hidden','value'=>$phonedata['Recipe']['id']));
echo $this->Form->end('Save Recipe');
?>