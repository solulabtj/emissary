<h1>Merchants DB</h1>
<p><?php echo $this->Html->link('Add Merchant', array('action' => 'add')); ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
		<th>Userid</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $merchants array, printing out merchant info -->

    <?php foreach ($merchants as $merchant): ?>
    <tr>
        <td><?php echo $merchant['Merchant']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $merchant['Merchant']['name'],
                    array('action' => 'view', $merchant['Merchant']['id'])
                );
            ?>
        </td>
		<td><?php echo $merchant['Merchant']['userid']; ?></td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $merchant['Merchant']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $merchant['Merchant']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $merchant['Merchant']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>