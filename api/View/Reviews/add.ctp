<h1>Add Category</h1>
<?php
echo $this->Form->create('Review');
echo $this->Form->input('title',array('type'=>'text'));
echo $this->Form->input('description');
echo $this->Form->input('starRating',array('type'=>'radio','options'=>array('1'=>'Very Bad','2'=>'Bad','3'=>'Average','4'=>'Good','5'=>'Very Good')));

//echo $this->Form->input('Category.parent_id',array('type'=>'select','empty'=>'Select Parent Category','options'=>$categorylist)); 
echo $this->Form->end('Rate it !');
?>