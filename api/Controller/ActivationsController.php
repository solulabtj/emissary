<?php

class ActivationsController extends AppController {

    public $uses = array('AgentUserInfo', 'AgentDevice', 'AgentSocialLogin', 'TestSetting');
    public $helpers = array('Html', 'Form');
    public $components = array('RequestHandler');

    function verify_merchant() {
        $i = 'no';

        if (!empty($_GET['vercode'])) {
            $vercode = $_GET['vercode'];
            //$verify = $this->MerchantDetail->findAllByVerificationCode($this->request->data['vercode']);
            $verify = $this->MerchantDetail->find('first', array('conditions' => array('verification_code' => $vercode, 'is_blocked' => 0)));
            //pr($verify);
            if ($verify) {
                isset($this->request->data['password']) ? $this->request->data['password'] = $this->Auth->password($this->request->data['password']) : '';
                $data['merchant_detail_id'] = $verify['MerchantDetail']['merchant_detail_id'];
                $data['is_confirmed'] = 1;
                $data['verification_code'] = '';
                if ($this->MerchantDetail->save($data)) {

                    $message = 'You account has been activated, you can now login using your credentials';
                    $this->set('msg', $message);
                    $i = 'yes';
                }
            } else {
                $message = 'Verification Code has been expired';
                $this->set('msg', $message);
            }
        } else {
            $message = 'You are not a registered user !';
            $this->set('msg', $message);
        }
    }

    function verify_agent() {
        $i = 'no';

        if (!empty($_GET['vercode'])) {
            $vercode = $_GET['vercode'];
            //$verify = $this->MerchantDetail->findAllByVerificationCode($this->request->data['vercode']);
            $verify = $this->AgentUserInfo->find('first', array('conditions' => array('verification_code' => $vercode, 'is_blocked' => 0)));
            //pr($verify);
            if ($verify) {

                $data['agent_id'] = $verify['AgentUserInfo']['agent_id'];
                $data['is_confirmed'] = 1;
                $data['verification_code'] = '';
                if ($this->AgentUserInfo->save($data)) {

                    $message = 'You account has been activated, you can now login using your credentials';
                    $this->set('msg', $message);
                    $i = 'yes';
                }
            } else {
                $message = 'Verification Code has been expired';
                $this->set('msg', $message);
            }
        } else {
            $message = 'You are not a registered user !';
            $this->set('msg', $message);
        }
    }

    function m_forgot() {
        $vercode = $_GET['vercode'];
        $email_address = urldecode($_GET['email_address']);
        if ($this->request->is('post')) {
            if ($verify = $this->MerchantDetail->find('first', array('conditions' => array('verification_code' => $vercode, 'is_blocked' => 0)))) {
                isset($this->request->data['new_password']) ? $this->request->data['new_password'] = $this->Auth->password($this->request->data['new_password']) : '';
                $data['merchant_detail_id'] = $verify['MerchantDetail']['merchant_detail_id'];
                $data['password'] = $this->request->data['new_password'];
                $data['verification_code'] = '';
                if ($this->MerchantDetail->save($data)) {
                    $message = 'Your password has been updated successfully';
                }
            } else {
                $message = 'Verification Code has been expired';
            }
        } else {
            $message = 'Enter new Password';
        }

        $this->set('msg', $message);
    }

    function a_forgot() {
        $vercode = $_GET['vercode'];
        $email_address = urldecode($_GET['email_address']);

        if ($this->request->is('post')) {

            if ($verify = $this->AgentUserInfo->find('first', array('conditions' => array('verification_code' => $vercode, 'email_address' => $email_address, 'is_blocked' => 0)))) {
                isset($this->request->data['new_password']) ? $this->request->data['new_password'] = $this->Auth->password($this->request->data['new_password']) : '';
                $data['agent_id'] = $verify['AgentUserInfo']['agent_id'];
                $data['password'] = $this->request->data['new_password'];
                $data['verification_code'] = '';
                if ($this->AgentUserInfo->save($data)) {
                    $message = 'Your password has been updated successfully';
                } else {
                    $message = 'Please Try again later!';
                }
            } else {
                $message = 'Verification Code has been expired';
            }
        } else {
            $message = 'Enter new Password';
        }

        $this->set('msg', $message);
    }

    public function ap2() {
        $_SESSION['book_test_data'] = $_REQUEST;
        //print_r($_SESSION['book_test_data']);
    }

    function book($paymentType = NULL, $accessCode = NULL) {
        //$data['access_code'] = $this->request->data['access_code'];
        $data['access_code'] = $accessCode;
        //pr($data);exit;
        if ($paymentType == 'WEB') {
            $data['paymentMode'] = $paymentType;
        } else {
            $data['paymentMode'] = 'APP';
        }
        $jsonData = json_encode($data);

        $this->set('jsonData', $jsonData);
    }

    public function book_test() {
        $i = 'no';
        $this->request->data = $_SESSION['book_test_data'];

        if (!empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                if ($test = $this->TestSetting->find('first', array('recursive' => -1))) {
                    $agent_id = $agent['AgentUserInfo']['agent_id'];
                    $test_cost = $test['TestSetting']['test_cost'];
                    $this->request->data['agent_id'] = $agent_id;
                    $this->request->data['agent_mobile_number'] = $agent['AgentUserInfo']['mobile_number'];
                    $this->request->data['agent_name'] = $agent['AgentUserInfo']['agent_name'];
                    $this->request->data['agent_email'] = $agent['AgentUserInfo']['email_address'];
                    $this->request->data['test_cost'] = $test_cost;
                    $booking_code = $this->generateRandomString();

                    $paypal_amount = $test_cost;
                    //exit;
                    $this->request->data['test_booking_code'] = $booking_code;
                    $postdata = $this->paypal_payment($paypal_amount);

                    $_SESSION['test_data'] = $this->request->data;
                    $this->set('postdata', $postdata);
                    //$this->set('action', PAYPAL_API_URL);
                    $this->set('action', TAP_URL);
                } else {
                    $this->redirect(array('controller' => 'activations', 'action' => 'session_break'));
                }
            } else {
                $this->redirect(array('controller' => 'activations', 'action' => 'session_break'));
            }
        } else {
            $this->redirect(array('controller' => 'activations', 'action' => 'session_break'));
        }
    }

    function session_break() {
        $msg_display = "Something went wrong, Please Contact Administrator !";

        $_SESSION['payment_msg'] = $msg_display;
        //  pr($_SESSION);exit;
        $this->redirect(array('controller' => 'activations', 'action' => 'payment_message'));
        $this->set('msg', $meg_display);
    }

    /* function payment_success() {
      //pr($_REQUEST);exit;
      $paypal_data = $_REQUEST;
      $status = $paypal_data['result'];
      $txnid = $paypal_data['tx'];
      $amount = $_SESSION['test_data']['test_cost'];
      //Rechecking the product price and currency details
      if ($status == 'SUCCESS') {
      $message = $this->confirm_test($paypal_data);

      if ($message) {
      $msg_display = "<h3>Thank You. Your order status is Successfull.</h3><h4>Your Transaction ID for this transaction is " . $txnid . ".</h4><h4>We have received a payment of $. " . $amount . "";
      } else {
      $msg_display = "Something went wrong, Please Contact Administrator !";
      }
      $_SESSION['payment_msg'] = $msg_display;
      //  pr($_SESSION);exit;
      $this->redirect(array('controller' => 'activations', 'action' => 'payment_message'));
      $this->set('msg', $meg_display);
      } else {
      $msg_display = "Invalid Transaction ! Please try again later.";

      $_SESSION['payment_msg'] = $msg_display;
      //  pr($_SESSION);exit;
      $this->redirect(array('controller' => 'activations', 'action' => 'payment_message'));
      $this->set('msg', $meg_display);
      }
      } */

    /* function payment_faliure() {
      $msg_display = "";
      $_SESSION['payment_msg'] = $msg_display;
      //  pr($_SESSION);exit;
      $this->redirect(array('controller' => 'activations', 'action' => 'payment_message'));
      $this->set('msg', $meg_display);
      } */

    public function confirm_test($paypal_data) {
        //$this->RequestHandler->renderAs($this, 'json');
        $i = 'no';

        $this->request->data = $_SESSION['test_data'];

        $agent = $this->grant_access($this->request->data['access_code']);
        $test_booking['agent_id'] = $this->request->data['agent_id'];
        $test_booking['transaction_id'] = $paypal_data['payid'];
        $test_booking['test_booking_code'] = $this->request->data['test_booking_code'];
        $test_booking['test_cost'] = $this->request->data['test_cost'];
        $test_booking['agent_mobile_number'] = $this->request->data['agent_mobile_number'];
        $test_booking['agent_name'] = $agent['AgentUserInfo']['agent_name'];
        $test_booking['payment_status'] = $paypal_data['result'];
        $test_booking['payment_type'] = 'TEST';

        if ($this->TestBooking->save($test_booking)) {
            return true;
        } else {
            return false;
        }
    }

    function payment_success() {
        $msg = $_SESSION['payment_msg'];
        if ($_SESSION['payment_success'] == 1) {
            $this->set('url', API_ROOT . "activations/startTest");
        }
        //echo $msg;
        // exit;
        $this->set('msg', $msg);
        unset($_SESSION['test_data']);
    }

    function payment_message() {
        //pr($_SESSION);
        $msg = $_SESSION['payment_msg'];
        if ($_SESSION['payment_success'] == 1) {
            $this->set('url', API_ROOT . "activations/startTest");
        }
        //echo $msg;
        // exit;
        $this->set('msg', $msg);
        unset($_SESSION['test_data']);
    }

    function exit_webview() {
        //pr($_SESSION);
        //$msg = $_SESSION['payment_msg'];
        //echo $msg;exit;
        //$this->set('msg',$msg);
    }

    function startTest() {
        $this->redirect(WEBSITE_ROOT . "agents/testStart");
        //pr($_SESSION);
        //$msg = $_SESSION['payment_msg'];
        //echo $msg;exit;
        //$this->set('msg',$msg);
    }

    public function testPaymentWeb() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            $data = $this->request->data;
            $json_data = json_encode($data);
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function tap() {

        $apiurl = 'http://live.gotapnow.com/webservice/PayGatewayService.svc';  // Development Url
//$apiurl = 'https://www.gotapnow.com/webservice/PayGatewayService.svc';  // Production Url
//Customer Details
        $Mobile = $_SESSION['test_data']['agent_mobile_number'];  //Customer Mobile Number
        $CstName = $_SESSION['test_data']['agent_name']; //Customer Name
        $Email = $_SESSION['test_data']['agent_email'];  //Customer Email Address
//Merchant Details
        $ref = "45870225000";  //Your ReferenceID or Order ID
        $MerchantID = "13014";  //Merchant ID Provided by Tap
        $UserName = "test";   //Merchant UserName Provided by Tap
        $ReturnURL = "http://localhost/emissary/api/activations/tapReturn";  //After Payment success, customer will be redirected to this url
        $PostURL = "http://localhost/emissary/api/activations/tapReturn";  //After Payment success, Payment Data's will be posted to this url
//Product Details
        $CurrencyCode = "KWD";  //Order Currency Code
        $Total = $_SESSION['test_data']['test_cost'];  //Total Order Amount
//Generating the Hash string
        $APIKey = "tap1234";  //Merchant API Key Provided by Tap
        $str = 'X_MerchantID' . $MerchantID . 'X_UserName' . $UserName . 'X_ReferenceID' . $ref . 'X_Mobile' . $Mobile . 'X_CurrencyCode' . $CurrencyCode . 'X_Total' . $Total . '';
        $hashstr = hash_hmac('sha256', $str, $APIKey);
//echo $hashstr;
        $action = "http://tempuri.org/IPayGatewayService/PaymentRequest";

        $soap_apirequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:tap="http://schemas.datacontract.org/2004/07/Tap.PayServiceContract">
   <soapenv:Header/>
   <soapenv:Body>
      <tem:PaymentRequest>
         <tem:PayRequest>
            <tap:CustomerDC>
               <tap:Email>' . $Email . '</tap:Email>
               <tap:Mobile>' . $Mobile . '</tap:Mobile>
               <tap:Name>' . $CstName . '</tap:Name>
            </tap:CustomerDC>
            <tap:MerMastDC>
               <tap:AutoReturn>Y</tap:AutoReturn>
               <tap:HashString>' . $hashstr . '</tap:HashString>
               <tap:MerchantID>' . $MerchantID . '</tap:MerchantID>
               <tap:ReferenceID>' . $ref . '</tap:ReferenceID>
               <tap:ReturnURL>' . $ReturnURL . '</tap:ReturnURL>
			   <tap:PostURL>' . $PostURL . '</tap:PostURL>
               <tap:UserName>' . $UserName . '</tap:UserName>
            </tap:MerMastDC>
            <tap:lstProductDC>
               <tap:ProductDC>
                  <tap:CurrencyCode>' . $CurrencyCode . '</tap:CurrencyCode>
                  <tap:Quantity>1</tap:Quantity>
                  <tap:TotalPrice>' . $Total . '</tap:TotalPrice>
                  <tap:UnitName>Order - ' . $ref . '</tap:UnitName>
                  <tap:UnitPrice>' . $Total . '</tap:UnitPrice>
               </tap:ProductDC>
            </tap:lstProductDC>
         </tem:PayRequest>
      </tem:PaymentRequest>
   </soapenv:Body>
</soapenv:Envelope>';

        $apiheader = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: " . strlen($soap_apirequest),
        );


        // The HTTP headers for the request (based on image above)
        $apiheader = array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($soap_apirequest),
            'SOAPAction: ' . $action
        );

        // Build the cURL session
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiurl);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $apiheader);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $soap_apirequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        // Send the request and check the response
        if (($result = curl_exec($ch)) === FALSE) {
            die('cURL error: ' . curl_error($ch) . "<br />\n");
        } else {
            //  echo "<br />Success!<br />\n";
        }
        curl_close($ch);

//echo htmlspecialchars($result, ENT_QUOTES, 'UTF-8');exit;

        $xmlobj = simplexml_load_string($result);

        $xmlobj->registerXPathNamespace('a', 'http://schemas.datacontract.org/2004/07/Tap.PayServiceContract');
        $xmlobj->registerXPathNamespace('i', 'http://www.w3.org/2001/XMLSchema-instance');

        $PaymentURL = "";
        $result = $xmlobj->xpath('//a:ReferenceID/text()');
        if (is_array($result)) {
            foreach ($result as $temp) {
                //  echo "<br>ReferenceID : " . $temp;
                $ReferenceID = $temp;
            }
        }

        $result = $xmlobj->xpath('//a:ResponseCode/text()');
        if (is_array($result)) {
            foreach ($result as $temp) {
                //echo "<br>ResponseCode : " . $temp;
            }
        }

        $result = $xmlobj->xpath('//a:ResponseMessage/text()');
        if (is_array($result)) {
            foreach ($result as $temp) {
                // echo "<br>ResponseMessage : " . $temp;
                $ResponseMessage = $temp;
            }
        }

        $result = $xmlobj->xpath('//a:PaymentURL/text()');
        if (is_array($result)) {
            foreach ($result as $temp) {
                //echo "<br>PaymentURL : " . $temp;
                $PaymentURL = $temp;
            }
        }


        header('Location: ' . $PaymentURL);
        die();
    }

    public function tapReturn() {

        $APIKey = 'tap1234';
        $Mobile = $_SESSION['test_data']['agent_mobile_number'];  //Customer Mobile Number
        $CstName = $_SESSION['test_data']['agent_name']; //Customer Name
        $Email = $_SESSION['test_data']['agent_email'];  //Customer Email Address
//Merchant Details
        $ref = "45870225000";  //Your ReferenceID or Order ID
        $MerchantID = "13014";  //Merchant ID Provided by Tap
        $UserName = "test";   //Merchant UserName Provided by Tap
        $ReturnURL = "http://localhost/emissary/api/activations/tapReturn";  //After Payment success, customer will be redirected to this url
        $PostURL = "http://localhost/emissary/api/activations/tapReturn";    //After Payment success, Payment Data's will be posted to this url
//echo "<br>".$MerchantID;
//echo "<br>".$ref;
//$mstr = 'X_ACCOUNT_ID13014X_REF'.$_REQUEST['ref'].'X_RESULT'.$_REQUEST['result'].'X_ReferenceID'.$_REQUEST['trackid'].'';
        $mstr = 'x_account_id' . $MerchantID . 'x_ref' . $_REQUEST['ref'] . 'x_result' . $_REQUEST['result'] . 'x_referenceid' . $_REQUEST['trackid'] . '';
        $Mhashstr = hash_hmac('sha256', $mstr, $APIKey);
//echo "<br />".$_REQUEST['hash'];
//echo "<br>".$Mhashstr;

        $payid = $_REQUEST['payid'];
        $crdtype = $_REQUEST['crdtype'];
        $amt = $_REQUEST['amt'];
        $trackid = $_REQUEST['trackid'];
        $crd = $_REQUEST['crd'];

        if ($Mhashstr == $_REQUEST['hash']) {

            $paypal_data = $_REQUEST;
            $status = $paypal_data['result'];
            $txnid = $paypal_data['payid'];
            $amount = $_SESSION['test_data']['test_cost'];
            //Rechecking the product price and currency details
            if ($status == 'SUCCESS') {
                $message = $this->confirm_test($paypal_data);
                $done = false;
                if ($message) {
                    $_SESSION['payment_success'] = 1;
                    $done = true;
                    $msg_display = "<h3>Thank You. Your order status is Successfull.</h3><h4>Your Transaction ID for this transaction is " . $txnid . ".</h4><h4>We have received a payment of KWD. " . $amount . "";
                } else {
                    $msg_display = "Something went wrong, Please Contact Administrator !";
                }
                $_SESSION['payment_msg'] = $msg_display;

                if ($done) {
                    $this->redirect(array('controller' => 'activations', 'action' => 'payment_success'));
                    $this->set('msg', $msg_display);
                } else {
                    $this->redirect(array('controller' => 'activations', 'action' => 'payment_message'));
                    $this->set('msg', $msg_display);
                }
            } else {
                $msg_display = "Invalid Transaction ! Please try again later.";

                $_SESSION['payment_msg'] = $msg_display;
                //  pr($_SESSION);exit;
                $this->redirect(array('controller' => 'activations', 'action' => 'payment_message'));
                $this->set('msg', $msg_display);
            }
        } else {
            $msg_display = "Invalid Transaction ! Please try again later.";
            $_SESSION['payment_msg'] = $msg_display;
            //  pr($_SESSION);exit;
            $this->redirect(array('controller' => 'activations', 'action' => 'payment_message'));
            $this->set('msg', $meg_display);
        }
    }

}
