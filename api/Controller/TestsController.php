<?php

class TestsController extends AppController {
	public $uses = array('AgentUserInfo','AgentDevice','AgentSocialLogin','AgentRating','TestSetting','TestQuestion','TestQuestionOption','TestResult','AgentTestAnswer');
    public $helpers = array('Html', 'Form');
	public $components = array('RequestHandler');

	 function beforeFilter()
	 {
		 parent::beforeFilter();
		 $this->RequestHandler->ext = 'json';
	 }	

	// public function index() {
		// //$this->RequestHandler->renderAs($this, 'json');	
		// $users = $this->User->find('all');
        // $this->set(array(
            // 'Success' => $users,
            // '_serialize' => array('Success')
        // ));
    // }
	
	
	
	//New web services
	public function book_test() {
		$i = 'no';
		
		if($this->request->is('post') && !empty($this->request->data['access_code']) )
		{   
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				if($test = $this->TestSetting->find('first',array('recursive'=>-1)))
				{
					$agent_id = $agent['AgentUserInfo']['agent_id'];
					$test_cost = $test['TestSetting']['test_cost'];
					$this->request->data['agent_id'] = $agent_id;
					$this->request->data['agent_mobile_number'] = $agent['AgentUserInfo']['mobile_number'];
					$this->request->data['test_cost'] = $test_cost;
					$booking_code = $this->generateRandomString();
					//exit;
					$this->request->data['test_booking_code'] = $booking_code;
					if($this->TestBooking->save($this->request->data))
					{
						$message = 'Test successfully booked.';
						$i='yes';
					}
					else {
						$message = 'Unable to book test !';
					}	
				}
				else
				{
					$message = 'None tests found';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
	public function start_test() {
		$i = 'no';
		
		if($this->request->is('post') && !empty($this->request->data['access_code']) )
		{   
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				if($test = $this->TestSetting->find('first',array('recursive'=>-1)))
				{
					$agent_id = $agent['AgentUserInfo']['agent_id'];
					$no_of_questions = $test['TestSetting']['no_of_questions'];
				
					if($test_pay = $this->TestBooking->find('all',array('fields'=>array('TestBooking.*'),'conditions'=>array('TestBooking.agent_id'=>$agent_id,'TestBooking.test_taken'=>0),'recursive'=>-1)))
					{
						if($test_q = $this->TestQuestion->find('all',array('fields'=>array('TestQuestion.*'),'order'=>'rand()','limit'=>$no_of_questions,'conditions'=>array('TestQuestion.is_active'=>1),'recursive'=>-1)))
						{
							foreach($test_q as $q_no => $q_test)
							{
								$option_q = array();
								$options = $this->TestQuestionOption->find('all',array('fields'=>array('TestQuestionOption.test_option_id','TestQuestionOption.test_question_id','TestQuestionOption.answer_text'),'conditions'=>array('TestQuestionOption.test_question_id' =>$q_test['TestQuestion']['test_question_id']),'recursive'=>1));
								foreach($options as $q_options)
								{
									$option_q[] = $q_options['TestQuestionOption'];
								}
								$test_q[$q_no]['TestQuestionOption'] = $option_q;
							}
							$message['Test'] = $test_q;
							$message['TestConfig'] = $test['TestSetting'];
							$i='yes';
							
							$this->TestBooking->updateAll(
								array('test_taken' => 1),
								array('agent_id' => $agent_id)
							);
						
						}
						else {
							$message = 'Unable to load test !';
						}	
					}
					else {
						$message = 'Please make a payment to start test !';
					}	
				}
				else
				{
					$message = 'None tests found';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
	public function submit_test() {
            
            error_reporting(E_ALL);
            ini_set("display_errors","ON");
            
		$i = 'no';
		$total_marks = 0;
		$earned_marks = 0;
		$op_count = 0;
		$correct_flag = array();
		$answer_count = 0;
		$no_of_questions = 0;
		$passing_percentage = 0;
		$percentage_earned = 0;
		$save_answers = 0;
		
		if($this->request->is('post') && !empty($this->request->data['access_code']))
		{   
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				if($test = $this->TestSetting->find('first',array('recursive'=>-1)))
				{
					$agent_id = $agent['AgentUserInfo']['agent_id'];
					$no_of_questions = $test['TestSetting']['no_of_questions'];
					$passing_percentage = $test['TestSetting']['passing_percentage'];
					
					$answers = json_decode($this->request->data['test_answers'],true);
					//pr($answers);
					if(!empty($answers))
					{
						$j=0;
						foreach($answers as $ans)
						{
							
							//echo $j.'<br>';
							//echo $k.'<br>';
							//echo '<pre>';print_r($option);
							//echo $options[$k].'<br>';
							//exit;
							
							$question_marks = $this->TestQuestion->field('TestQuestion.test_question_marks',array('TestQuestion.test_question_id' => $answers[$j]['question_id'])); 
							
							$correct_options = $this->TestQuestionOption->find('all',array('fields'=>array('TestQuestionOption.test_option_id'),'conditions'=>array('TestQuestionOption.test_question_id'=>$answers[$j]['question_id'], 'TestQuestionOption.is_correct'=>1),'recursive'=>1));
							
							//pr($question_marks);
							
							//pr($correct_options);
							
							if(!empty($question_marks) && !empty($correct_options))
							{
								
								$x=0;
								foreach($correct_options as $cop)
								{
									$corr_ans[$x] = $cop['TestQuestionOption']['test_option_id'];	
									$x++;
								}
								/*
								echo '<pre>';
								print_r($corr_ans);
								echo '<pre>';
								print_r($ans['option_ids']);
								//exit;*/
								
								$agent_ans = $ans['option_ids'];
								
								if($corr_ans === array_intersect($corr_ans, $agent_ans) && $agent_ans === array_intersect($agent_ans, $corr_ans))
								{
									$earned_question_marks = $question_marks;
								}
								else {
									$earned_question_marks = 0;
								}	
								
								foreach($ans['option_ids'] as $option)
								{
									$test_answers[$op_count]['agent_id'] = $agent_id;
									$test_answers[$op_count]['test_question_id'] = $ans['question_id'];
									$test_answers[$op_count]['test_option_id'] = $option;
									$op_count++;
								}
							}
							else
							{                                                            
								$message = 'Unable to process your request, Please try again later.';
								break;
							}
							
							
							$earned_marks = $earned_marks + $earned_question_marks;
							$total_marks = $total_marks + $question_marks;	
							$j++;
						}
						$answer_count = count($answers);
						$save_answers = $this->AgentTestAnswer->saveAll($test_answers);
					}
					//exit;
					$percentage_earned = round(($earned_marks/$total_marks) * 100);
					
					if($percentage_earned >= $passing_percentage)
					{
						$data['is_passed'] = 1;	
					}
					else
					{
						$data['is_passed'] = 0;	
					}
					
					$data['agent_id'] = $agent_id;
					$data['agent_scored'] = $earned_marks;
					$data['maximum_score'] = $total_marks;
					$data['questions_answered'] = $answer_count;
					$data['total_questions'] = $no_of_questions;
					$data['percentage_earned'] = $percentage_earned;
					
					if($this->TestResult->save($data))
					{
						$this->AgentUserInfo->id = $agent_id;
						$this->AgentUserInfo->saveField('test_passed', 1);
                                         
						$adminDashMsg = "Agent".$agent['AgentUserInfo']['agent_name']."just passed the test";
						$this->saveAdminMsg($adminDashMsg);
						
						$message = $data;
						$i='yes';	
						
						 $from1 = $_POST['agent_email_id'];
						 $to1 = array('admin@emissary.com' => 'EMISSARY');
						 //$to1 = "";
						 
						 $subject1 = 'Agent has submitted test.';
						 
						 $msg1 = $agent['AgentUserInfo']['agent_name'].'|'.$agent['AgentUserInfo']['mobile_number'];
						 $template1 = 'new_test_submission';				 
						 
						 $mail = $this->send_email($from1,$to1,$subject1,$msg1,$template1);
					}
					else
					{
						$message = 'Unable to process your request, Please try again later.';	
					}
				}
				else
				{
					$message = 'None tests found';
				}
				
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
              
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
	
	public function send_email($from, $to, $subject, $message, $template) {

        $this->Email = new CakeEmail();
		$this->Email->config('smtp');
        $this->Email->to($to);
        $this->Email->subject($subject);
        $this->Email->emailFormat('html');

        $this->Email->from($from);
        $this->Email->template($template);

        ob_start();
        $this->Email->send($message);
        ob_end_clean();
    }
	
}