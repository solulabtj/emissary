<?php
App::uses('CakeEmail', 'Network/Email');
class AgentsController extends AppController {

    public $uses = array('AgentUserInfo', 'AgentDevice', 'AgentSocialLogin', 'AgentRating', 'ClientUserInfo', 'ClientImage', 'AgentJob', 'Job', 'JobAlert', 'TestResult', 'JobQuestion', 'JobQuestionOption', 'CheckIn', 'TestQuestionOption', 'TestResult', 'JobCategory', 'JobFilter', 'JobFilterCategory', 'Feedback', 'AgentCoupon', 'Coupon', 'AgentPayment', 'Country', 'SurveyQuestion', 'ClientAddress', 'AgentSurveyAnswer', 'AgentRank', 'Survey');
    public $helpers = array('Html', 'Form');
    public $components = array('RequestHandler');

    function beforeFilter() {
        parent::beforeFilter();
        $this->RequestHandler->ext = 'json';
    }
    
    // public function index() {
    // //$this->RequestHandler->renderAs($this, 'json');	
    // $users = $this->User->find('all');
    // $this->set(array(
    // 'Success' => $users,
    // '_serialize' => array('Success')
    // ));
    // }
    //New web services

    public function agent_sign_up() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['mobile_number']) && !empty($this->request->data['email_address']) && !empty($this->request->data['password'])) {
            $today = date("Y-m-d H:i:s");
            $device_id = isset($this->request->data['device_id']) ? $this->request->data['device_id'] : '';
            $registration_id = isset($this->request->data['registration_id']) ? $this->request->data['registration_id'] : '';
            $device_type = isset($this->request->data['device_type']) ? $this->request->data['device_type'] : '';
            isset($this->request->data['password']) ? $this->request->data['password'] = $this->Auth->password($this->request->data['password']) : '';

            $this->request->data['login_count'] = 0;
            $this->request->data['is_confirmed'] = 1;
            $this->request->data['is_blocked'] = 0;
            $this->request->data['verification_code'] = md5(microtime());
            $this->request->data['username'] = $this->request->data['email_address'];
            //$this->MerchantDetail->create();

            if ($this->AgentUserInfo->save($this->request->data)) {
                $agent = $this->AgentUserInfo->find('first', array('order' => array('AgentUserInfo.agent_id DESC')));
                $agent_id = $agent['AgentUserInfo']['agent_id'];

                $access_code = md5($agent_id);
                $access_code = 'A' . $access_code;
                $data['access_code'] = $access_code;
                if ($this->AgentUserInfo->save($data)) {
                    $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);

                    $this->create_default_filter($agent_id);

                    // echo 'in here';
                    /* $from = array('admin@emissary.com' => 'EMISSARY');
                      $to = $this->request->data['email_address'];
                      $subject = 'Welcome to LookForSure community! Cofounder, LookForSure';
                      $link = API_ROOT.'activations/verify_agent/?vercode='.$this->request->data['verification_code'];
                      // $link = 'www.solulab.com';
                      $linkOnMail = '<a href="'.$link.'" class="link2" target="_blank">Click Here to Activate Your Account</a>';
                      $msg = $linkOnMail;
                      $template = 'new_user';
                      $mail = $this->send_email($from,$to,$subject,$msg,$template); */


                    // MAil to admin;
                    $from1 = array('admin@emissary.com' => 'EMISSARY');
                    //$to1 = ADMIN_EMAIL;
                    $to1 = $agent['AgentUserInfo']['email'];
                    $subject1 = 'Welcome to Emissary';

                   // $msg1 = $agent['AgentUserInfo']['email'] . '|' . $user['AgentUserInfo']['mobile_number'];
                    //$template1 = 'new_agent_notify';
                    
                    
                    
                    $msg1 = "<p>Thank you, ".$signup_data['agent_name'].", and welcome to the Emissary platform! <br/><br/><br/>
                             In the attachment, you will find all you need to know about Emissary. Please go through it in detail as we want to make sure you and contributing Mystery Shopping Providers have the greatest experience possible. As you finish your shops via the Emissary system, you will also be rewarded with points to improve your Agent Rank and ascend the Emissary Leaderboards.
                             Information about this is provided in the attachment.<br/><br/><br/>
                             Emissary is a platform businesses use to provide and gather feedback.<br/> 
                             The businesses are individually accountable for each element of their shops – including
                             remunerating you. Emissary is NOT a Mystery Shopper Provider - it is a shopper gateway, <br/>
                             assisting shoppers to discover all opportunities in a single place. <br/>
                             You will be required to complete each business’ Independent Contractor Agreement to qualify to take and complete jobs for them. <br/>
                             Please be aware that new opportunities and new businesses are continually being uploaded. <br/>
                             If you cannot find anything close to you, be sure to check in again soon! You will be contacted as Emissary opportunities become available in your region. <br/>
                             Thanks again for signing up with Emissary!<br/><br/><br/>
                             Sincerely,<br/>
                             The Emissary Team<br/><br/>
                            <img src='http://emissaryagency.com/beta/images/logo_nav.png' alt='Your logo' style='background-color: black;'><br/><br/>
                            Powered by: Emissary Software Platform<br/>
                            Find Emissary on Facebook: http://www.facebook.com/Emissaryagency<br/>
                            Follow Emissary on Instagram: @Emissaryagency<br/></p>";

                            $template1 = '';
                    
                    
                    
                    $mail = $this->send_email($from1, $to1, $subject1, $msg1, $template1);

                    $res['access_code'] = $access_code;
                    $message = $res;
                    //$message = 'Please check your mail and confirm account';
                    $i = 'yes';
                } else {
                    $message = 'Unable to process your request. Try again later !';
                }
            } else {
                $message = $this->AgentUserInfo->validationErrors;
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    function agent_login() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['password']) && !empty($this->request->data['email_address'])) {
            $device_id = isset($this->request->data['device_id']) ? $this->request->data['device_id'] : '';
            $registration_id = isset($this->request->data['registration_id']) ? $this->request->data['registration_id'] : '';
            $device_type = isset($this->request->data['device_type']) ? $this->request->data['device_type'] : '';
            if ($agent = $this->AgentUserInfo->find('first', array('conditions' => array('email_address' => $this->request->data['email_address'])))) {
                //,'password' => $this->Auth->password($this->request->data['password']), 'is_blocked' =>0, 'is_confirmed' => 1)
                //pr($merdetail);
                //pr($this->request->data);
                $this->Auth->password($this->request->data['password']);
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if ($agent['AgentUserInfo']['password'] != AuthComponent::password($this->request->data['password'])) {
                    $message = 'Enter correct password.';
                } elseif ($agent['AgentUserInfo']['is_blocked'] == 1) {
                    $message = 'You have been blocked by Administrator.';
                } elseif ($agent['AgentUserInfo']['is_confirmed'] == 0) {
                    $message = 'Please activate your account first from activation link sent to your registered email.';
                } else {

                    $logincount = $agent['AgentUserInfo']['login_count'] + 1;

                    $data['last_login'] = NOW;
                    $data['login_count'] = $logincount;
                    $data['agent_id'] = $agent_id;


                    if (!empty($agent['AgentUserInfo']['access_code'])) {
                        $this->AgentUserInfo->save($data);
                        $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                        $code['access_code'] = $agent['AgentUserInfo']['access_code'];

                        $message = $code;
                        $i = 'yes';
                    } else {
                        $access_code = md5($agent_id);
                        $access_code = 'A' . $access_code;
                        $data['access_code'] = $access_code;
                        $this->AgentUserInfo->save($data);
                        $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                        $code['access_code'] = $access_code;

                        $message = $code;
                        $i = 'yes';
                    }
                }
            } elseif ($agent = $this->AgentUserInfo->find('first', array('conditions' => array('mobile_number' => $this->request->data['email_address'])))) {
                //,'password' => $this->Auth->password($this->request->data['password']), 'is_blocked' =>0, 'is_confirmed' => 1)
                //pr($merdetail);
                //pr($this->request->data);
                $this->Auth->password($this->request->data['password']);
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if ($agent['AgentUserInfo']['password'] != $this->Auth->password($this->request->data['password'])) {
                    $message = 'Enter correct password.';
                } elseif ($agent['AgentUserInfo']['is_blocked'] == 1) {
                    $message = 'You have been blocked by Administrator.';
                } elseif ($agent['AgentUserInfo']['is_confirmed'] == 0) {
                    $message = 'Please activate your account first from activation link sent to your registered email.';
                } else {
                    $logincount = $agent['AgentUserInfo']['login_count'] + 1;

                    $data['last_login'] = NOW;
                    $data['login_count'] = $logincount;
                    $data['agent_id'] = $agent_id;


                    if (!empty($agent['AgentUserInfo']['access_code'])) {
                        $this->AgentUserInfo->save($data);
                        $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                        $code['access_code'] = $agent['AgentUserInfo']['access_code'];


                        $message = $code;
                        $i = 'yes';
                    } else {
                        $access_code = md5($user_id);
                        $access_code = 'A' . $access_code;
                        $data['access_code'] = $access_code;
                        $this->AgentUserInfo->save($data);
                        $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                        $code['access_code'] = $access_code;

                        $message = $code;
                        $i = 'yes';
                    }
                }
            } else {
                $message = 'Please enter correct Email or Mobile number';
            }
        } else {
            $message = 'Enter Email/Mobile';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('Error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function agent_social_login() {
        $i = 'no';
        if ($this->request->is('post')) {
            $today = date("Y-m-d H:i:s");
            $this->request->data['is_confirmed'] = 1;
            $this->request->data['is_blocked'] = 0;
            $this->request->data['verification_code'] = '';
            $device_id = isset($this->request->data['device_id']) ? $this->request->data['device_id'] : '';
            $registration_id = isset($this->request->data['registration_id']) ? $this->request->data['registration_id'] : '';
            $device_type = isset($this->request->data['device_type']) ? $this->request->data['device_type'] : '';
            $data['is_confirmed'] = 1;
            $data['is_blocked'] = 0;
            $data['verification_code'] = '';
            $data['gender'] = isset($this->request->data['gender']) ? $this->request->data['gender'] : '';
            $this->request->data['username'] = isset($this->request->data['email_address']) ? $this->request->data['email_address'] : '';
            //$data['agent_dob'] = isset($this->request->data['agent_dob'])?$this->request->data['agent_dob']:'';
            //$this->MerchantDetail->create();

            $socialColumns = $this->AgentSocialLogin->schema();

            $socialArr = $socialColumns;
            //print_r($socialArr);
            $commonKey = array_intersect_key($socialArr, $this->request->data);
            if (!empty($commonKey)) {
                //echo "<pre>";print_r($commonKey);exit;
                $keys = array_keys($commonKey);
                $keyId = $keys[0];
                $social[$keyId] = $this->request->data[$keyId];

                if ((!empty($this->request->data[$keyId])) && ($this->request->data[$keyId] != '' || $this->request->data[$keyId] != NULL || $this->request->data[$keyId] != 0) && $social_profile = $this->AgentSocialLogin->find('first', array('conditions' => array('AgentSocialLogin.' . $keyId => $this->request->data[$keyId]), 'recursive' => -1))) {
                    //pr($social_profile);
                    $agent_id = $social_profile['AgentSocialLogin']['agent_id'];

                    if ($agent = $this->AgentUserInfo->find('first', array('conditions' => array('AgentUserInfo.agent_id' => $agent_id, 'is_blocked' => 0, 'is_confirmed' => 1), 'recursive' => -1))) {
                        $logincount = $agent['AgentUserInfo']['login_count'] + 1;
                        $data['last_login'] = NOW;
                        $data['login_count'] = $logincount;
                        $data['agent_id'] = $agent_id;
                        $data['verification_code'] = '';

                        if (!empty($agent['AgentUserInfo']['access_code'])) {
                            $this->AgentUserInfo->save($data);
                            $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                            $code['access_code'] = $agent['AgentUserInfo']['access_code'];
                            $code['first_timer'] = 0;

                            $message = $code;
                            $i = 'yes';
                        } else {
                            $access_code = md5($agent_id);
                            $access_code = 'A' . $access_code;
                            $data['access_code'] = $access_code;
                            $this->AgentUserInfo->save($data);
                            $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                            $code['access_code'] = $access_code;
                            $code['first_timer'] = 0;

                            $message = $code;
                            $i = 'yes';
                        }
                    } else {
                        $message = 'This user has been blocked';
                    }
                } else {
                    if ($existing_mail = $this->AgentUserInfo->find('first', array('conditions' => array('AgentUserInfo.email_address' => $this->request->data['email_address']), 'recursive' => -1))) {
                        if ($existing_mail['AgentUserInfo']['is_blocked'] == 0) {
                            $agent_id = $existing_mail['AgentUserInfo']['agent_id'];

                            $logincount = $existing_mail['AgentUserInfo']['login_count'] + 1;
                            $data['last_login'] = NOW;
                            $data['login_count'] = $logincount;
                            $data['agent_id'] = $agent_id;


                            if ($existing_social = $this->AgentSocialLogin->find('first', array('conditions' => array('AgentSocialLogin.agent_id' => $agent_id), 'recursive' => -1))) {
                                $this->request->data['agent_social_login_id'] = $existing_social['AgentSocialLogin']['agent_social_login_id'];

                                if ($this->AgentSocialLogin->save($this->request->data)) {
                                    if (!empty($existing_mail['AgentUserInfo']['access_code'])) {
                                        $this->AgentUserInfo->save($data);
                                        $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                                        $code['access_code'] = $existing_mail['AgentUserInfo']['access_code'];
                                        $code['first_timer'] = 0;

                                        $message = $code;
                                        $i = 'yes';
                                    } else {
                                        $access_code = md5($agent_id);
                                        $access_code = 'A' . $access_code;
                                        $data['access_code'] = $access_code;
                                        $this->AgentUserInfo->save($data);
                                        $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                                        $code['access_code'] = $access_code;
                                        $code['first_timer'] = 0;

                                        $message = $code;
                                        $i = 'yes';
                                    }
                                } else {
                                    $message = 'Contact Administrator';
                                }
                            } else {
                                $this->request->data['agent_id'] = $agent_id;
                                if ($this->AgentSocialLogin->save($this->request->data)) {
                                    if (!empty($existing_mail['AgentUserInfo']['access_code'])) {
                                        $this->AgentUserInfo->save($data);
                                        $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                                        $code['access_code'] = $existing_mail['AgentUserInfo']['access_code'];
                                        $code['first_timer'] = 0;

                                        $message = $code;
                                        $i = 'yes';
                                    } else {
                                        $access_code = md5($agent_id);
                                        $access_code = 'A' . $access_code;
                                        $data['access_code'] = $access_code;
                                        $this->AgentUserInfo->save($data);
                                        $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                                        $code['access_code'] = $access_code;
                                        $code['first_timer'] = 0;

                                        $message = $code;
                                        $i = 'yes';
                                    }
                                } else {
                                    $message = 'Contact Administrator';
                                }
                            }
                        } else {
                            $message = 'User blocked by Administrator';
                        }
                    } else {
                        if ($this->AgentUserInfo->save($this->request->data)) {
                            //$merchant = $this->Merchant->findById($this->Merchant->getLastInsertId()); 
                            $agent = $this->AgentUserInfo->find('first', array('order' => array('AgentUserInfo.agent_id' => 'DESC'), 'recursive' => 0));

                            $agent_id = $agent['AgentUserInfo']['agent_id'];
                            $data['agent_id'] = $agent_id;
                            $access_code = md5($data['agent_id']);
                            $access_code = 'A' . $access_code;
                            $data['access_code'] = $access_code;
                            $data['login_count'] = $agent['AgentUserInfo']['login_count'] + 1;
                            $data['last_login'] = NOW;
                            $data['is_blocked'] = 0;
                            if ($this->AgentUserInfo->save($data)) {

                                $this->request->data['agent_id'] = $agent_id;
                                if ($this->AgentSocialLogin->save($this->request->data)) {
                                    $this->create_default_filter($agent_id);
                                    $this->store_reg_agent_device($device_id, $registration_id, $agent_id, $device_type);
                                    $code['access_code'] = $access_code;
                                    $code['first_timer'] = 1;
                                    $message = $code;
                                    $i = 'yes';

                                    $from1 = array('admin@emissary.com' => 'EMISSARY');
                                    $to1 = ADMIN_EMAIL;
                                    $subject1 = 'New Agent successfully onboarded ';

                                    $msg1 = $agent['AgentUserInfo']['email'];
                                    $template1 = 'social_agent_notify';

                                    $mail = $this->send_email($from1, $to1, $subject1, $msg1, $template1);
                                }
                            } else {
                                $message = $this->User->validationErrors;
                            }
                        } else {
                            $message = $this->User->validationErrors;
                        }
                    }
                }
            } else {
                $message = 'Social ID not found !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function agent_change_password() {
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['old_password']) && !empty($this->request->data['new_password'])) {
            if ($agent = $this->AgentUserInfo->find('first', array('conditions' => array('access_code' => $this->request->data['access_code'], 'password' => $this->request->data['old_password'], 'is_blocked' => 0, 'is_confirmed' => 1), 'recursive' => -1))) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];

                isset($this->request->data['new_password']) ? $this->request->data['new_password'] = $this->Auth->password($this->request->data['new_password']) : '';
                $this->AgentUserInfo->agent_id = $agent_id;
                $data['password'] = $this->request->data['new_password'];
                $data['agent_id'] = $agent_id;

                if ($agent = $this->AgentUserInfo->save($data)) {
                    //$shopaddress = $shop['ShopDetail']['address'];
                    $message = 'Your password has been updated successfully';
                    $i = 'yes';
                } else {
                    $message = 'Failed to change password';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    function agent_forgot_password() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['email_address'])) {
            //$merdetail = $this->MerchantDetail->findAllByEmail($this->request->data['email']);
            if ($agent = $this->AgentUserInfo->find('first', array('conditions' => array('email_address' => $this->request->data['email_address'], 'is_blocked' => 0, 'is_confirmed' => 1), 'recursive' => -1))) {

                //pr($merdetail);
                $ver_code = md5(microtime());
                $from = array('admin@emissary.com' => 'EMISSARY');
                $to = $this->request->data['email_address'];
                $subject = 'Emmissary! Reset Password';
                $link = API_ROOT . 'activations/a_forgot/?vercode=' . $ver_code . '&email_address=' . urlencode($to);
                $linkOnMail = '<a href="' . $link . '" class="link2" target="_blank">Click Here to Update Your password </a>';
                $msg = $linkOnMail;
                $template = 'forgot_password';
                $data['verification_code'] = $ver_code;
                $data['agent_id'] = $agent['AgentUserInfo']['agent_id'];
                if ($this->AgentUserInfo->save($data)) {
                    $mail = $this->send_email($from, $to, $subject, $msg, $template);
                    $message = 'Please click on the link sent to your registered email to reset password';
                    $i = 'yes';
                } else {
                    $message = 'Try again later!';
                }
            } else {
                $message = 'You are not a registered user !';
            }
        } else {
            $message = 'Invalid Request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('Error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    function agent_logout() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                if ($this->Auth->logout()) {
                    $data['login_count'] = $agent['AgentUserInfo']['login_count'] - 1;
                    $data['agent_id'] = $agent['AgentUserInfo']['agent_id'];
                    if ($data['login_count'] == 0) {
                        $data['access_code'] = 0;
                        if ($this->AgentUserInfo->save($data)) {
                            $message = 'Successfully logged out';
                            $i = 'yes';
                        }
                    } else {
                        if ($this->AgentUserInfo->save($data)) {
                            $message = 'Successfully logged out';
                            $i = 'yes';
                        }
                    }
                }
            } else {
                $message = 'Please Login Again';
            }
        } else {
            $message = 'Invalid Request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('Error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_profile() {
        $i = 'no';
        $rating = 0;
        $count = 0;

        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent_access = $this->grant_access($this->request->data['access_code'])) {
                if ($agent = $this->AgentUserInfo->find('first', array('conditions' => array('access_code' => $this->request->data['access_code'], 'is_blocked' => 0, 'is_confirmed' => 1), 'recursive' => 0))) {
                    $agent_id = $agent['AgentUserInfo']['agent_id'];
                    if (empty($agent['AgentUserInfo']['profile_image'])) {
                        $agent['AgentUserInfo']['profile_image'] = '';
                    } else {
                        $image = $agent['AgentUserInfo']['profile_image'];
                        $img = WEBSITE_ROOT . 'media/img/' . $image;
                        $agent['AgentUserInfo']['profile_image'] = $img;
                    }
                    if ($agent_rating = $this->AgentRating->find('all', array('conditions' => array('AgentRating.agent_id' => $agent_id), 'order' => 'AgentRating.created DESC', 'recursive' => -1))) {
                        $avg = $this->AgentRating->find('all', array('conditions' => array('AgentRating.agent_id' => $agent_id), 'recursive' => 0, 'fields' => array('AVG( AgentRating.rating_points ) AS average')));
                        foreach ($avg as $re) {
                            foreach ($re as $r) {
                                foreach ($r as $rate) {
                                    $rating = round($rate, 1);
                                }
                            }
                        }

                        $count = $this->AgentRating->find('count', array('conditions' => array('AgentRating.agent_id' => $agent_id), 'recursive' => 1));


                        $agent['Rating']['AvgRating'] = $rating;
                        $agent['Rating']['RatingCount'] = $count;
                    } else {
                        $agent['Rating']['AvgRating'] = $rating;
                        $agent['Rating']['RatingCount'] = $count;
                    }
                    $message = $agent;
                    $i = 'yes';
                } else {
                    $message = 'User not found !';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function update_profile() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            //pr($this->request->data);exit;
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                //pr($_FILES);exit;
                /* if(!empty($this->request->data['agent_image']))
                  { */
                $test['Request'] = $this->request->data;
                $test['File'] = $_FILES;

                //pr($_FILES); exit;

                if (isset($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name'])) {
                    $imageExt = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                    $pathi = "";
                    $target_dir = WEBSITE_WEBROOT . 'media/img/';
                    //echo $target_dir;exit;
                    //$pathv = "";
                    foreach ($_FILES as $val) {
                        $microtime = microtime();

                        $fileName = $val["name"];
                        $extension = explode(".", $fileName);
                        $Path = "";

                        if (in_array($extension[1], $imageExt)) {
                            $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                            $target_dir2 = $target_dir . $Pathi;
                            move_uploaded_file($val["tmp_name"], $target_dir2);
                        }/* else {
                          $Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                          $target_dir2 = $target_dir . $Pathv;
                          move_uploaded_file($val["tmp_name"], $target_dir2);
                          } */
                        $this->request->data['profile_image'] = $Pathi;
                    }
                } else {
                    $this->request->data['profile_image'] = $agent['AgentUserInfo']['profile_image'];
                }
                //$profile_image = $this->saveimage($this->request->data['agent_image']);
                //$this->request->data['profile_image'] = $profile_image;
                //}
                $this->request->data['agent_id'] = $agent['AgentUserInfo']['agent_id'];
                $this->AgentUserInfo->agent_id = $agent_id;
                $update_agent = $this->AgentUserInfo->set($this->request->data);
                if ($this->AgentUserInfo->save($this->request->data)) {
                    //$message = $test; 
                    $message = "Profile Details successfully updated.";
                    $i = 'yes';
                } else {
                    $message = 'Unable to process your request';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_rating() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if ($agent_rating = $this->AgentRating->find('all', array('conditions' => array('AgentRating.agent_id' => $agent_id), 'order' => 'AgentRating.created DESC', 'recursive' => -1))) {
                    $avg = $this->AgentRating->find('all', array('conditions' => array('AgentRating.agent_id' => $agent_id), 'recursive' => 0, 'fields' => array('AVG( AgentRating.rating_points ) AS average')));
                    foreach ($avg as $re) {
                        foreach ($re as $r) {
                            foreach ($r as $rate) {
                                $rating = round($rate, 1);
                            }
                        }
                    }

                    $count = $this->AgentRating->find('count', array('conditions' => array('AgentRating.agent_id' => $agent_id), 'recursive' => 1));


                    $agent_rating['Rating']['AvgRating'] = $rating;
                    $agent_rating['Rating']['RatingCount'] = $count;

                    $message = $agent_rating;
                    $i = 'yes';
                } else {
                    $message = 'Rating not found !';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_applied_jobs() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        $today = date("Y-m-d H:i:s");
        if ($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $curr_lat = $this->request->data['current_lat'];
                $curr_lng = $this->request->data['current_lng'];
                $starting_number = isset($this->request->data['starting_number']) ? $this->request->data['starting_number'] : 0;
                $limit = PAGINATION_LIMIT;

                if ($applied_job_ids = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.accepted_by_client' => 0, 'AgentJob.declined_by_client' => 0, 'AgentJob.completed_by_agent' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.disapproved_by_client' => 0, 'AgentJob.approved_by_client' => 0), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => -1))) {

                    $k = 0;
                    foreach ($applied_job_ids as $job_ids) {
                        $joblocations = $this->JobLocation->find('first', array('fields' => array('JobLocation.*', 'ClientAddress.*'), 'conditions' => array('JobLocation.job_location_id' => $applied_job_ids[$k]['AgentJob']['job_location_id']), 'recursive' => 0));

                        $job_ids['AgentJob']['JobAddress'] = $joblocations['ClientAddress'];

                        $applied_jobs[$k] = $this->Job->find('first', array('fields' => array('Job.*', 'Coupon.*', 'JobCategory.job_category_title'), 'conditions' => array('Job.job_id' => $applied_job_ids[$k]['AgentJob']['job_id'], 'Job.is_active' => 1, 'Job.is_blocked' => 0), 'recursive' => 0));
                        $applied_jobs[$k]['AgentJob'] = $job_ids['AgentJob'];
                        $k++;
                    }

                    $applied_jobs = $this->get_distance($applied_jobs, $curr_lat, $curr_lng);
                    $applied_jobs = array_values(array_filter($applied_jobs));
                    //pr($similardeals);
                    if (!empty($applied_jobs)) {
                        $message = $this->list_jobs($applied_jobs);
                        $status = 'yes';
                    } else {
                        $message = array();
                        $status = 'yes';
                    }

                    $messsage = array_values(array_filter($message));
                    $status = 'yes';
                } else {
                    $message = array();
                    $status = 'yes';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_assigned_jobs() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        $today = date("Y-m-d H:i:s");
        if ($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $curr_lat = $this->request->data['current_lat'];
                $curr_lng = $this->request->data['current_lng'];
                $starting_number = isset($this->request->data['starting_number']) ? $this->request->data['starting_number'] : 0;
                $limit = PAGINATION_LIMIT;

                if ($applied_job_ids = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.accepted_by_client' => 1, 'AgentJob.declined_by_client' => 0, 'AgentJob.completed_by_agent' => 0, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.disapproved_by_client' => 0, 'AgentJob.approved_by_client' => 0), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => -1))) {
                    //pr($applied_job_ids);	
                    $k = 0;
                    foreach ($applied_job_ids as $job_ids) {
                        $joblocations = $this->JobLocation->find('first', array('fields' => array('JobLocation.*', 'ClientAddress.*'), 'conditions' => array('JobLocation.job_location_id' => $applied_job_ids[$k]['AgentJob']['job_location_id']), 'recursive' => 0));

                        $job_ids['AgentJob']['JobAddress'] = $joblocations['ClientAddress'];

                        $applied_jobs[$k] = $this->Job->find('first', array('fields' => array('Job.*', 'Coupon.*', 'JobCategory.job_category_title'), 'conditions' => array('Job.job_id' => $applied_job_ids[$k]['AgentJob']['job_id'], 'Job.is_active' => 1, 'Job.is_blocked' => 0), 'recursive' => 0));
                        $applied_jobs[$k]['AgentJob'] = $job_ids['AgentJob'];
                        $k++;
                    }

                    $applied_jobs = $this->get_distance($applied_jobs, $curr_lat, $curr_lng);
                    $applied_jobs = array_values(array_filter($applied_jobs));
                    //pr($similardeals);
                    if (!empty($applied_jobs)) {
                        $message = $this->list_jobs($applied_jobs);
                        $status = 'yes';
                    } else {
                        $message = array();
                        $status = 'yes';
                    }

                    $messsage = array_values(array_filter($message));
                    $status = 'yes';
                } else {
                    $message = array();
                    $status = 'yes';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_completed_jobs() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        $today = date("Y-m-d H:i:s");
        if ($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $curr_lat = $this->request->data['current_lat'];
                $curr_lng = $this->request->data['current_lng'];
                $starting_number = isset($this->request->data['starting_number']) ? $this->request->data['starting_number'] : 0;
                $limit = PAGINATION_LIMIT;

                if ($applied_job_ids = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.accepted_by_client' => 1, 'AgentJob.declined_by_client' => 0, 'AgentJob.completed_by_agent' => 1, 'AgentJob.cancelled_by_agent' => 0), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => -1))) {
                    $k = 0;
                    foreach ($applied_job_ids as $job_ids) {
                        $joblocations = $this->JobLocation->find('first', array('fields' => array('JobLocation.*', 'ClientAddress.*'), 'conditions' => array('JobLocation.job_location_id' => $applied_job_ids[$k]['AgentJob']['job_location_id']), 'recursive' => 0));

                        $job_ids['AgentJob']['JobAddress'] = $joblocations['ClientAddress'];

                        $applied_jobs[$k] = $this->Job->find('first', array('fields' => array('Job.*', 'Coupon.*', 'JobCategory.job_category_title'), 'conditions' => array('Job.job_id' => $applied_job_ids[$k]['AgentJob']['job_id'], 'Job.is_active' => 1, 'Job.is_blocked' => 0), 'recursive' => 0));
                        $applied_jobs[$k]['AgentJob'] = $job_ids['AgentJob'];
                        $k++;
                    }

                    $applied_jobs = $this->get_distance($applied_jobs, $curr_lat, $curr_lng);
                    $applied_jobs = array_values(array_filter($applied_jobs));
                    //pr($similardeals);
                    if (!empty($applied_jobs)) {
                        $message = $this->list_jobs($applied_jobs);
                        $status = 'yes';
                    } else {
                        $message = array();
                        $status = 'yes';
                    }

                    //pr($message);exit;
                    $messsage = array_values(array_filter($message));
                    $status = 'yes';
                } else {
                    $message = array();
                    $status = 'yes';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function apply_job() {
        $i = 'no';
        $assignTestFlag = false;
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_id']) && !empty($this->request->data['job_location_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $job_id = $this->request->data['job_id'];
                $job_location_id = $this->request->data['job_location_id'];

                if ($job = $this->Job->find('first', array('conditions' => array('Job.job_id' => $this->request->data['job_id'], 'is_blocked' => 0, 'is_active' => 1), 'recursive' => -1))) {
                    $client_id = $job['Job']['client_id'];
                    if ($job['Job']['test_passed']) {
                        if ($test_status = $this->TestResult->find('first', array('conditions' => array('TestResult.agent_id' => $agent_id, 'TestResult.is_passed' => 1), 'recursive' => -1))) {
                            $assignTestFlag = true;
                        } else {
                            $assignTestFlag = false;
                        }
                    } else {
                        $assignTestFlag = true;
                    }

                    if ($assignTestFlag) {
                        if ($job['Job']['min_agent_rank'] >= $agent['AgentUserInfo']['agent_rank']) {
                            $total_requests_till_now = $this->AgentJob->find('count', array('conditions' => array('AgentJob.job_id' => $job_id), 'recursive' => -1));
                            $agent_jobs_till_now = $this->AgentJob->find('count', array('conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.completed_by_agent' => 0, 'AgentJob.cancelled_by_agent' => 0), 'recursive' => -1));
							
                            $allowed_agent_jobs = $this->AgentRank->field('allowed_jobs', array('AgentRank.rank' => $agent['AgentUserInfo']['agent_rank']));
							
                            if ($agent_jobs_till_now < $allowed_agent_jobs) {
                                if ($job['Job']['max_allowed_requests'] > $total_requests_till_now) {
                                    $this->request->data['agent_id'] = $agent_id;
                                    $this->request->data['job_id'] = $job_id;
                                    $this->request->data['client_id'] = $client_id;
                                    $this->request->data['job_location_id'] = $job_location_id;
                                    $this->request->data['accepted_by_client'] = 0;
                                    $this->request->data['declined_by_client'] = 0;
                                    $this->request->data['completed_by_agent'] = 0;
                                    $this->request->data['cancelled_by_agent'] = 0;
                                    $this->request->data['disapproved_by_client'] = 0;
                                    $this->request->data['approved_by_client'] = 0;


                                    if ($this->AgentJob->save($this->request->data)) {
                                        $this->Job->id = $job_id;
                                        $this->Job->updateAll(array(
                                            'Job.request_received' => 'Job.request_received + 1'), array('Job.job_id' => $job_id));

                                        $message = 'You have successfully applied to this job.';
                                        $i = 'yes';
										
										$to_client = $this->ClientUserInfo->find('first', array('conditions' => array('ClientUserInfo.client_id' => $client_id), 'recursive' => -1));

                                        $from1 = "solulabtest@gmail.com";
                                        $to1 = $to_client['ClientUserInfo']['email_address'];
										
                                        $subject1 = 'New Job request has been received';

                                        $msg1 = $agent['AgentUserInfo']['agent_name'] . '|' . $user['AgentUserInfo']['mobile_number'];
                                        $template1 = 'new_job_request';

                                        $mail = $this->send_email($from1, $to1, $subject1, $msg1, $template1);
                                    } else {
                                        $message = 'Unable to complete your request, Please try again later.';
                                    }
                                } else {
                                    $message = 'This Job has already reached its maximum number of candidates.';
                                }
                            } else {
                                $message = 'You have reached max limit for applying jobs.';
                            }
                        } else {
                            $message = 'This Job needs a higher rank.';
                        }
                    } else {
                        $message = 'Please clear a test first to apply for this job.';
                    }
                } else {
                    $message = 'Job not found';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function set_job_alert() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['city_name']) && !empty($this->request->data['country_id']) && !empty($this->request->data['minimum_compensation']) && !empty($this->request->data['maximum_compensation'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];

                $this->request->data['agent_id'] = $agent_id;

                if ($this->JobAlert->save($this->request->data)) {
                    $message = 'Your Job alert has been created successfully.';
                    $i = 'yes';
                } else {
                    $message = 'Unable to complete your request, Please try again later.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function my_job_alerts() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if ($job_alerts = $this->JobAlert->find('all', array('conditions' => array('JobAlert.agent_id' => $agent_id, 'JobAlert.is_active' => 1), 'recursive' => -1))) {
                    $message = $job_alerts;
                    $i = 'yes';
                } else {
                    $message = 'Job not found';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_job_alert() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_alert_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $job_alert_id = $this->request->data['job_alert_id'];
                if ($job_alert = $this->JobAlert->find('first', array('conditions' => array('JobAlert.job_alert_id' => $job_alert_id, 'JobAlert.is_active' => 1), 'recursive' => -1))) {
                    $message = $job_alert;
                    $i = 'yes';
                } else {
                    $message = 'Job not found';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function delete_job_alert() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_alert_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $job_alert_id = $this->request->data['job_alert_id'];
                $this->JobAlert->id = $job_alert_id;
                if ($this->JobAlert->saveField('is_active', 0)) {
                    $message = 'Job Alert successfully deleted';
                    $i = 'yes';
                } else {
                    $message = 'Job not found';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function update_job_alert() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_alert_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $job_alert_id = $this->request->data['job_alert_id'];
                if ($job_alert = $this->JobAlert->find('first', array('conditions' => array('JobAlert.job_alert_id' => $job_alert_id, 'JobAlert.is_active' => 1), 'recursive' => -1))) {
                    $this->JobAlert->id = $job_alert_id;
                    if ($this->JobAlert->save($this->request->data)) {
                        $message = 'Your Job alert has been updated successfully.';
                        $i = 'yes';
                    } else {
                        $message = 'Unable to complete your request, Please try again later.';
                    }
                } else {
                    $message = 'Job Alert doesn\'t exist';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function set_preferences() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $this->request->data['agent_id'] = $agent['AgentUserInfo']['agent_id'];

                if ($this->AgentUserInfo->save($this->request->data)) {
                    $message = 'Preferences saved successfully';
                    $i = 'yes';
                } else {
                    $message = 'Unable to process your request, Please try again later.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function get_preferences() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $message = $agent;
                $i = 'yes';
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_test_results() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if ($test_results = $this->TestResult->find('all', array('conditions' => array('TestResult.agent_id' => $agent_id), 'recursive' => -1))) {
                    $message = $test_results;
                    $i = 'yes';
                } else {
                    $message = 'Unable to process your request, Please try again later.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function agent_check_in() {
        $i = 'no';
        $check_in_agent = false;
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_id']) && !empty($this->request->data['check_in_type'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $data['agent_id'] = $agent['AgentUserInfo']['agent_id'];
                $data['job_id'] = $this->request->data['job_id'];
                if ($agent_jobs = $this->AgentJob->find('first', array('conditions' => array('AgentJob.agent_id' => $data['agent_id'], 'AgentJob.job_id' => $data['job_id']), 'recursive' => -1))) {
                    $data['check_in_time'] = NOW;
                    $data['check_in_type'] = $this->request->data['check_in_type'];
                    $data['agent_job_id'] = $agent_jobs['AgentJob']['agent_job_id'];
                    $data['is_checkin_verified'] = 0;

                    $job_location_id = $agent_jobs['AgentJob']['job_location_id'];

                    $client_address_id = $this->JobLocation->field('client_address_id', array('JobLocation.job_location_id' => $job_location_id));

                    $client_id = $this->Job->field('client_id', array('Job.job_id' => $data['job_id']));

                    $data['client_id'] = $client_id;
                    $data['checkin_lattitude'] = isset($this->request->data['current_lat']) ? $this->request->data['current_lat'] : 0;
                    $data['checkin_longitude'] = isset($this->request->data['current_lng']) ? $this->request->data['current_lng'] : 0;
                    $data['qr_code_data'] = isset($this->request->data['qr_data']) ? $this->request->data['qr_data'] : 0;

                    $survey_id = $this->Job->field('survey_id', array('Job.job_id' => $data['job_id']));
                    if ($data['check_in_type'] == 'GPS') {
                        if ($check_in_allowed = $this->Survey->find('first', array('conditions' => array('Survey.survey_id' => $survey_id, 'Survey.gps_check' => 1), 'recursive' => 0))) {
                            if ($checkin_radius = $this->ClientAddress->query("SELECT *, ( 3959 * acos( cos( radians(" . $data['checkin_lattitude'] . ") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(" . $data['checkin_longitude'] . ") ) + sin( radians(" . $data['checkin_lattitude'] . ") ) * sin( radians( lattitude ) ) ) ) AS distance FROM client_address AS ClientAddress HAVING distance < '" . CHECKIN_RADIUS . "' ORDER BY distance")) {
                                $data['is_checkin_verified'] = 1;
                                $check_in_agent = true;
                            }
                        }
                    } elseif ($data['check_in_type'] == 'QR') {
                        if ($check_in_allowed = $this->Survey->find('first', array('conditions' => array('Survey.survey_id' => $survey_id, 'Survey.qr_check' => 1), 'recursive' => 0))) {
                            if ($qr_matched = $this->ClientAddress->find('all', array('conditions' => array('ClientAddress.client_address_id' => $client_address_id, 'ClientAddress.qr_code_data' => $data['qr_code_data'])))) {
                                $data['is_checkin_verified'] = 1;
                                $check_in_agent = true;
                            }
                        }
                    } else {
                        $message = 'Unable to process your request sue to invalid check in type, Please try again later.';
                    }
                    if ($check_in_agent) {
                        if ($this->CheckIn->save($data)) {
                            $check_in['is_checked_in'] = true;
                            $check_in['agent_job_id'] = $data['agent_job_id'];
                            $message = $check_in;
                            $i = 'yes';
                        } else {
                            $message = 'Unable to process your request, Please try again later.';
                        }
                    } else {
                        $message = 'Unable to process your request, Please try again later.';
                    }
                } else {
                    $message = 'Unable to process your request, Please try again later.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    /* public function start_job_survey() {
      $i = 'no';
      if($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_id']) && !empty($this->request->data['agent_job_id']))
      {
      if($agent = $this->grant_access($this->request->data['access_code']))
      {
      $agent_id = $agent['AgentUserInfo']['agent_id'];
      $agent_job_id = $this->request->data['agent_job_id'];
      if($is_cheked_in = $this->CheckIn->find('first',array('conditions'=>array('CheckIn.job_id' => $this->request->data['job_id'], 'CheckIn.agent_id' => $agent_id, 'CheckIn.agent_job_id' => $agent_job_id),'recursive'=>-1)))
      {

      $data['job_id'] = $this->request->data['job_id'];
      $agent_job_id = $is_cheked_in['CheckIn']['agent_job_id'];

      $client_id = $this->Job->field('client_id',array('Job.job_id' => $data['job_id']));
      $survey_id = $this->Job->field('survey_id',array('Job.job_id' => $data['job_id']));

      if($start_job = $this->SurveyQuestion->find('all',array('fields'=>array(),'conditions'=>array('SurveyQuestion.survey_id' => $survey_id),'order' => array('SurveyQuestion.survey_category_id'),'recursive'=>1)))
      {
      $this->AgentJob->id = $agent_job_id;
      if($this->AgentJob->saveField('job_start_time', NOW))
      {
      $message = $start_job;
      $i = 'yes';
      }
      else {
      $message = 'Failed to start Job.';
      }
      }
      else {
      $message = 'Unable to process your request, Please try again later.';
      }
      }
      else {
      $message = 'Please CheckIn before starting a job !';
      }
      }
      else {
      $message = 'Please login Again !';
      }
      }
      else{$message = 'invalid request';}

      if($i == 'no'){
      $this->set(array(
      'Error' => array('error'=>$message),
      '_serialize' => array('Error')));}
      else{$this->set(array(
      'Success' => array('success'=>$message),
      '_serialize' => array('Success')));}

      } */

    public function start_job_survey() {
        $i = 'no';
        $total_answered = 0;
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_id']) && !empty($this->request->data['agent_job_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $agent_job_id = $this->request->data['agent_job_id'];
                if ($is_cheked_in = $this->CheckIn->find('first', array('conditions' => array('CheckIn.job_id' => $this->request->data['job_id'], 'CheckIn.agent_id' => $agent_id, 'CheckIn.agent_job_id' => $agent_job_id), 'recursive' => -1))) {

                    $data['job_id'] = $this->request->data['job_id'];
                    $agent_job_id = $is_cheked_in['CheckIn']['agent_job_id'];

                    $client_id = $this->Job->field('client_id', array('Job.job_id' => $data['job_id']));
                    $survey_id = $this->Job->field('survey_id', array('Job.job_id' => $data['job_id']));

                    if ($start_job = $this->SurveyQuestion->find('all', array('fields' => array('SurveyQuestion.survey_id', 'SurveyQuestion.survey_category_id', 'SurveyCategory.survey_category_title', 'SurveyCategory.survey_category_image'), 'conditions' => array('SurveyQuestion.survey_id' => $survey_id), 'group' => array('SurveyQuestion.survey_category_id'), 'recursive' => 0))) {

                        $total_questions = $this->SurveyQuestion->find('count', array('conditions' => array('SurveyQuestion.survey_id' => $survey_id), 'recursive' => 1));

                        $questions_answered = $this->AgentSurveyAnswer->find('count', array('conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id), 'group' => array('AgentSurveyAnswer.survey_question_id'), 'recursive' => 1));

                        $completion_percentage = ($questions_answered / $total_questions) * 100;

                        $k = 0;
                        foreach ($start_job as $survey_cat) {
                            if (empty($survey_cat['SurveyCategory']['survey_category_image'])) {
                                $start_job[$k]['SurveyQuestion']['survey_category_image'] = '';
                            } else {
                                $image = $survey_cat['SurveyCategory']['survey_category_image'];
                                $img = WEBSITE_ROOT . 'media/img/' . $image;
                                $start_job[$k]['SurveyQuestion']['survey_category_image'] = $img;
                            }

                            $count = $this->SurveyQuestion->find('count', array('conditions' => array('SurveyQuestion.survey_category_id' => $survey_cat['SurveyQuestion']['survey_category_id'], 'SurveyQuestion.survey_id' => $survey_id), 'recursive' => 1));
                            $start_job[$k]['SurveyQuestion']['total_questions'] = $count;
                            $start_job[$k]['SurveyQuestion']['survey_category_title'] = $survey_cat['SurveyCategory']['survey_category_title'];
                            $start_job[$k]['SurveyQuestion']['completion_percentage'] = round($completion_percentage);
                            $start_job[$k]['SurveyQuestion']['agent_job_id'] = $agent_job_id;
                            $start_job[$k]['SurveyCategories'] = $start_job[$k]['SurveyQuestion'];
                            unset($start_job[$k]['SurveyQuestion']);
                            unset($start_job[$k]['SurveyCategory']);

                            if ($total_answered = $this->AgentSurveyAnswer->find('count', array('conditions' => array('AgentSurveyAnswer.survey_category_id' => $survey_cat['SurveyQuestion']['survey_category_id'], 'AgentSurveyAnswer.agent_job_id' => $agent_job_id), 'group' => array('AgentSurveyAnswer.survey_question_id'), 'recursive' => 0))) {
                                $start_job[$k]['SurveyCategories']['is_completed'] = true;
                                $start_job[$k]['SurveyCategories']['total_answered'] = $total_answered;
                            } else {
                                $start_job[$k]['SurveyCategories']['is_completed'] = false;
                                $start_job[$k]['SurveyCategories']['total_answered'] = 0;
                            }

                            $k++;
                        }
                        //pr($start_job);exit;




                        $this->AgentJob->id = $agent_job_id;
                        if ($this->AgentJob->saveField('job_start_time', NOW)) {
                            $message = $start_job;
                            $i = 'yes';
                        } else {
                            $message = 'Failed to start Job.';
                        }
                    } else {
                        $message = 'Unable to process your request, Please try again later.';
                    }
                } else {
                    $message = 'Please CheckIn before starting a job !';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function get_questions_from_category() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['survey_id']) && !empty($this->request->data['survey_category_id']) && !empty($this->request->data['agent_job_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $agent_job_id = $this->request->data['agent_job_id'];
                if ($is_cheked_in = $this->CheckIn->find('first', array('conditions' => array('CheckIn.agent_id' => $agent_id, 'CheckIn.agent_job_id' => $agent_job_id), 'recursive' => -1))) {

                    $survey_id = $this->request->data['survey_id'];
                    $survey_category_id = $this->request->data['survey_category_id'];
                    $agent_job_id = $is_cheked_in['CheckIn']['agent_job_id'];


                    if ($start_job = $this->SurveyQuestion->find('all', array('fields' => array('SurveyQuestion.*'), 'conditions' => array('SurveyQuestion.survey_id' => $survey_id, 'SurveyQuestion.survey_category_id' => $survey_category_id), 'order' => array('SurveyQuestion.survey_category_id'), 'recursive' => 1))) {
                        $message = $start_job;
                        $i = 'yes';
                    } else {
                        $message = 'Unable to process your request, Please try again later.';
                    }
                } else {
                    $message = 'Please CheckIn before starting a job !';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function submit_job() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_id']) && !empty($this->request->data['agent_job_id']) && !empty($this->request->data['job_answers']) && $this->request->data['total_questions'] && $this->request->data['survey_category_id']) {
        //if(1)
            $earnedMarks = 0;
            $totalMarks = 0;
            $request['Request'] = $this->request->data;
            $request['Files'] = $_FILES;

            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $agent_job_id = $this->request->data['agent_job_id'];
                $job_id = $this->request->data['job_id'];

                $data['agent_id'] = $agent['AgentUserInfo']['agent_id'];
                $survey_category_id = $this->request->data['survey_category_id'];
                if ($is_cheked_in = $this->CheckIn->find('first', array('conditions' => array('CheckIn.job_id' => $this->request->data['job_id'], 'CheckIn.agent_id' => $agent_id, 'CheckIn.agent_job_id' => $agent_job_id), 'recursive' => -1))) {
                    $data['agent_job_id'] = $is_cheked_in['CheckIn']['agent_job_id'];

                    $job_details = $this->Job->find('first', array('conditions' => array('Job.job_id' => $this->request->data['job_id']), 'recursive' => -1));
                    $client_id = $job_details['Job']['client_id'];
                    $survey_id = $job_details['Job']['survey_id'];


                    $this->request->data['job_answers'];
                    $answers = json_decode($this->request->data['job_answers'], true);

                    //pr($answers);exit;
                    $j = 0;
                    foreach ($answers as $ans) {
                        if ($ans['survey_question_type'] == 'C' || $ans['survey_question_type'] == 'R' || $ans['survey_question_type'] == 'T') {
                            foreach ($ans['survey_question_answer'] as $option) {
                                $job_answers[$j]['survey_question_id'] = $ans['survey_question_id'];
                                $job_answers[$j]['survey_question_marks'] = $ans['survey_question_marks'];
                                $job_answers[$j]['agent_id'] = $data['agent_id'];
                                $job_answers[$j]['agent_job_id'] = $data['agent_job_id'];
                                $job_answers[$j]['survey_question_type'] = $ans['survey_question_type'];
                                $job_answers[$j]['survey_question_answer'] = $option;
                                //$job_answers[$j]['survey_category_id'] = $ans['survey_category_id'];
                                $job_answers[$j]['survey_category_id'] = $survey_category_id;
                                $j++;
                            }
                        } elseif ($ans['survey_question_type'] == 'I') {
                            $no_of_media = $ans['survey_question_answer'][0];
                            if (is_numeric($no_of_media) && $no_of_media != 0) {
                                for ($l = 1; $l <= $no_of_media; $l++) {
                                    $job_answers[$j]['survey_question_id'] = $ans['survey_question_id'];
                                    $job_answers[$j]['survey_question_marks'] = $ans['survey_question_marks'];
                                    $job_answers[$j]['agent_id'] = $data['agent_id'];
                                    $job_answers[$j]['agent_job_id'] = $data['agent_job_id'];
                                    $job_answers[$j]['survey_question_type'] = $ans['survey_question_type'];
                                    //$job_answers[$j]['survey_category_id'] = $ans['survey_category_id'];
                                    $job_answers[$j]['survey_category_id'] = $survey_category_id;

                                    $survey_question_id = $job_answers[$j]['survey_question_id'];

                                    if (!empty($_FILES[$survey_question_id . '_' . $l]["name"])) {
                                        $imageExt = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                                        $pathi = "";
                                        $target_dir = WEBSITE_WEBROOT . 'media/img/';
                                        //echo $target_dir;exit;
                                        //$pathv = "";
                                        foreach ($_FILES as $val) {
                                            $microtime = microtime();

                                            $fileName = $_FILES[$survey_question_id . '_' . $l]["name"];
                                            $extension = explode(".", $fileName);
                                            $Path = "";

                                            if (in_array($extension[1], $imageExt)) {
                                                $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                                                $target_dir2 = $target_dir . $Pathi;
                                                move_uploaded_file($val["tmp_name"], $target_dir2);
                                            }/* else {
                                              $Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                                              $target_dir2 = $target_dir . $Pathv;
                                              move_uploaded_file($val["tmp_name"], $target_dir2);
                                              } */
                                            $job_answers[$j]['survey_question_answer'] = $Pathi;
                                        }
                                    } else {
                                        $job_answers[$j]['survey_question_answer'] = '';
                                    }
                                    $j++;
                                }
                            } else {
                                foreach ($ans['survey_question_answer'] as $option) {
                                    $job_answers[$j]['survey_question_id'] = $ans['survey_question_id'];
                                    $job_answers[$j]['survey_question_marks'] = $ans['survey_question_marks'];
                                    $job_answers[$j]['agent_id'] = $data['agent_id'];
                                    $job_answers[$j]['agent_job_id'] = $data['agent_job_id'];
                                    $job_answers[$j]['survey_question_type'] = $ans['survey_question_type'];
                                    $job_answers[$j]['survey_question_answer'] = $option;
                                    //$job_answers[$j]['survey_category_id'] = $ans['survey_category_id'];
                                    $job_answers[$j]['survey_category_id'] = $survey_category_id;
                                    $j++;
                                }
                            }
                        } elseif ($ans['survey_question_type'] == 'A') {
                            $no_of_media = $ans['survey_question_answer'][0];
                            if (is_numeric($no_of_media) && $no_of_media != 0) {
                                for ($l = 1; $l <= $no_of_media; $l++) {
                                    $job_answers[$j]['survey_question_id'] = $ans['survey_question_id'];
                                    $job_answers[$j]['survey_question_marks'] = $ans['survey_question_marks'];
                                    $job_answers[$j]['agent_id'] = $data['agent_id'];
                                    $job_answers[$j]['agent_job_id'] = $data['agent_job_id'];
                                    $job_answers[$j]['survey_question_type'] = $ans['survey_question_type'];
                                    //$job_answers[$j]['survey_category_id'] = $ans['survey_category_id'];
                                    $job_answers[$j]['survey_category_id'] = $survey_category_id;

                                    $survey_question_id = $job_answers[$j]['survey_question_id'];

                                    if (!empty($_FILES[$survey_question_id . '_' . $l]["name"])) {
                                        $audioExt = array('m4a', 'wma', 'wav', 'caf');
                                        $pathi = "";
                                        $target_dir = WEBSITE_WEBROOT . 'media/audio/';
                                        //echo $target_dir;exit;
                                        //$pathv = "";
                                        foreach ($_FILES as $val) {
                                            $microtime = microtime();

                                            $fileName = $_FILES[$survey_question_id . '_' . $l]["name"];
                                            $extension = explode(".", $fileName);
                                            $Path = "";

                                            if (in_array($extension[1], $audioExt)) {
                                                $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                                                $target_dir2 = $target_dir . $Pathi;
                                                move_uploaded_file($val["tmp_name"], $target_dir2);
                                            }/* else {
                                              $Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                                              $target_dir2 = $target_dir . $Pathv;
                                              move_uploaded_file($val["tmp_name"], $target_dir2);
                                              } */
                                            $job_answers[$j]['survey_question_answer'] = $Pathi;
                                        }
                                    } else {
                                        $job_answers[$j]['survey_question_answer'] = '';
                                    }
                                    $j++;
                                }
                            } else {
                                foreach ($ans['survey_question_answer'] as $option) {
                                    $job_answers[$j]['survey_question_id'] = $ans['survey_question_id'];
                                    $job_answers[$j]['survey_question_marks'] = $ans['survey_question_marks'];
                                    $job_answers[$j]['agent_id'] = $data['agent_id'];
                                    $job_answers[$j]['agent_job_id'] = $data['agent_job_id'];
                                    $job_answers[$j]['survey_question_type'] = $ans['survey_question_type'];
                                    $job_answers[$j]['survey_question_answer'] = $option;
                                    //$job_answers[$j]['survey_category_id'] = $ans['survey_category_id'];
                                    $job_answers[$j]['survey_category_id'] = $survey_category_id;
                                    $j++;
                                }
                            }
                        }
                        $j++;
                    }

                    if ($this->AgentSurveyAnswer->saveAll($job_answers)) {
                        if (!empty($this->request->data['job_completed']) && $this->request->data['job_completed'] == true) {

                            $total_marks = $this->SurveyQuestion->find('all', array(
                                'fields' => array(
                                    'SUM(SurveyQuestion.survey_question_marks) AS total'
                                ), 'conditions' => array('SurveyQuestion.survey_id' => $survey_id)
                            ));

                            $totalMarks = $total_marks[0][0]['total'];

                            $earned_marks = $this->AgentSurveyAnswer->find('all', array(
                                'fields' => array(
                                    'SUM(AgentSurveyAnswer.survey_question_marks) AS earned'
                                ), 'conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id)
                                , 'group' => array('AgentSurveyAnswer.survey_question_id')
                            ));

                            $total_questions = $this->SurveyQuestion->find('count', array('conditions' => array('SurveyQuestion.survey_id' => $survey_id), 'recursive' => 1));

                            $questions_answered = $this->AgentSurveyAnswer->find('count', array('conditions' => array('AgentSurveyAnswer.agent_job_id' => $agent_job_id), 'group' => array('AgentSurveyAnswer.survey_question_id'), 'recursive' => 1));



                            $earnedMarks = $earned_marks[0][0]['earned'];

                            //echo $totalMarks.'<br>'; echo $earnedMarks.'<br>'; 
                            //echo $total_questions.'<br>'; echo $questions_answered.'<br>'; exit;
                            $data['is_completed_by_agent'] = 1;
                            $data['job_end_time'] = NOW;

                            $data['questions_answered'] = $questions_answered;
                            $data['total_questions'] = $total_questions;

                            $data['agent_scored'] = $earnedMarks;
                            $data['maximum_score'] = $totalMarks;

                            $data['percentage_earned'] = round(($earnedMarks / $totalMarks) * 100);

                            if ($this->AgentJob->save($data)) {
                                $this->Job->id = $job_id;
                                $this->Job->updateAll(array(
                                    'Job.submission_received' => 'Job.submission_received + 1'), array('Job.job_id' => $job_id));

                                $adminDashMsg = "Agent" . $agent['AgentUserInfo']['agent_name'] . "just completed a survey";
                                $this->saveAdminMsg($adminDashMsg);

                                $message = $data;
                                $i = 'yes';

                                $from1 = array('admin@emissary.com' => 'EMISSARY');
                                $to1 = ADMIN_EMAIL;
                                $subject1 = 'New Job has been submitted ';

                                $msg1 = $agent['AgentUserInfo']['agent_name'] . '|' . $user['AgentUserInfo']['mobile_number'];
                                $template1 = 'new_job_submission';

                                $mail = $this->send_email($from1, $to1, $subject1, $msg1, $template1);
                            }
                        } else {
                            $message = 'Survey category successfully submitted';
                            $i = 'yes';
                        }
                    } else {
                        $message = 'Unable to process your request, Please try again later.';
                    }
                } else {
                    $message = 'Please CheckIn before starting a job !';
                }
            } else {
                $message = 'Please login Again !';
            }

            //$message = $request;
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function submit_job_media() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['media_type']) && !empty($this->request->data['survey_question_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $survey_question_id = $this->request->data['survey_question_id'];
                $media_url['survey_question_id'] = $survey_question_id;
                if ($this->request->data['media_type'] == "I") {
                    $extension_allow = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                    $target_dir = WEBSITE_WEBROOT . 'media/img/';
                } else {
                    $extension_allow = array('m4a', 'wma', 'wav', 'caf', 'mp4', 'mp3', 'M4A', 'WMA', 'WAV', 'CAF', 'MP4', 'MP3');
                    $target_dir = WEBSITE_WEBROOT . 'media/audio/';
                }

                if (!empty($_FILES)) {
                    $pathi = "";
                    $j = 0;
                    //echo $target_dir;exit;
                    //$pathv = "";
                    foreach ($_FILES as $val) {
                        $microtime = microtime();

                        $fileName = $val["name"];
                        $extension = explode(".", $fileName);
                        $Path = "";

                        if (in_array($extension[1], $extension_allow)) {
                            $Pathi = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                            $target_dir2 = $target_dir . $Pathi;
                            move_uploaded_file($val["tmp_name"], $target_dir2);
                        }/* else {
                          $Pathv = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', $microtime) . "." . $extension[1];
                          $target_dir2 = $target_dir . $Pathv;
                          move_uploaded_file($val["tmp_name"], $target_dir2);
                          } */
                        $media_url['survey_question_answer'][$j] = $Pathi;

                        $j++;
                    }
                    $message = $media_url;
                    $i = 'yes';
                } else {
                    $message = 'Media not found !';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function my_payments() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $starting_number = isset($this->request->data['starting_number']) ? $this->request->data['starting_number'] : 0;
                $limit = PAGINATION_LIMIT;

                if ($payments = $this->AgentJob->find('all', array('fields' => array('AgentJob.*', 'ClientUserInfo.company_logo', 'ClientUserInfo.agency_name', 'ClientUserInfo.company_name', 'ClientUserInfo.company_overview'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.accepted_by_client' => 1, 'AgentJob.declined_by_client' => 0, 'AgentJob.completed_by_agent' => 1, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.approved_by_client' => 1, 'AgentJob.disapproved_by_client' => 0, 'is_paid' => 1), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => 1))) {
                    //pr($payments);exit;
                    $k = 0;
                    foreach ($payments as $payment) {
                        if (empty($payments[$k]['ClientUserInfo']['company_logo'])) {
                            $payments[$k]['ClientUserInfo']['company_logo_image'] = '';
                        } else {
                            $image = $payments[$k]['ClientUserInfo']['company_logo'];
                            $img = WEBSITE_ROOT . 'media/img/' . $image;
                            $payments[$k]['ClientUserInfo']['company_logo_image'] = $img;
                        }
                        $client_images[$k] = $this->ClientImage->find('first', array('fields' => array('ClientImage.*'), 'conditions' => array('ClientImage.client_id' => $payments[$k]['AgentPayment']['client_id'], 'ClientImage.is_default' => 1), 'recursive' => -1));

                        foreach ($client_images as $client_image) {
                            if (!empty($client_images[$k])) {
                                //echo $payments[$l]['JobImage'][$m]['JobImage']['image_file_path'];
                                if (empty($client_images[$k]['ClientImage']['image_file_path'])) {
                                    $payments[$k]['ClientUserInfo']['company_cover'] = '';
                                } else {
                                    $image = $client_images[$k]['ClientImage']['image_file_path'];
                                    $img = WEBSITE_ROOT . 'media/img/' . $image;
                                    $payments[$k]['ClientUserInfo']['company_cover'] = $img;
                                }
                            } else {
                                $payments[$k]['ClientUserInfo']['company_cover'] = '';
                            }
                        }

                        $k++;
                    }

                    $message = $payments;
                    $i = 'yes';
                } else {
                    $message = array();
                    $i = 'yes';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function pending_payments() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $starting_number = isset($this->request->data['starting_number']) ? $this->request->data['starting_number'] : 0;
                $limit = PAGINATION_LIMIT;

                if ($payments = $this->AgentJob->find('all', array('fields' => array('AgentJob.*', 'ClientUserInfo.company_logo', 'ClientUserInfo.agency_name', 'ClientUserInfo.company_name', 'ClientUserInfo.company_overview'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.accepted_by_client' => 1, 'AgentJob.declined_by_client' => 0, 'AgentJob.completed_by_agent' => 1, 'AgentJob.cancelled_by_agent' => 0, 'AgentJob.approved_by_client' => 0, 'AgentJob.disapproved_by_client' => 1, 'is_paid' => 0), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => 1))) {
                    //pr($payments);exit;
                    $k = 0;
                    foreach ($payments as $payment) {
                        if (empty($payments[$k]['ClientUserInfo']['company_logo'])) {
                            $payments[$k]['ClientUserInfo']['company_logo_image'] = '';
                        } else {
                            $image = $payments[$k]['ClientUserInfo']['company_logo'];
                            $img = WEBSITE_ROOT . 'media/img/' . $image;
                            $payments[$k]['ClientUserInfo']['company_logo_image'] = $img;
                        }
                        $client_images[$k] = $this->ClientImage->find('first', array('fields' => array('ClientImage.*'), 'conditions' => array('ClientImage.client_id' => $payments[$k]['AgentPayment']['client_id'], 'ClientImage.is_default' => 1), 'recursive' => -1));

                        foreach ($client_images as $client_image) {
                            if (!empty($client_images[$k])) {
                                //echo $payments[$l]['JobImage'][$m]['JobImage']['image_file_path'];
                                if (empty($client_images[$k]['ClientImage']['image_file_path'])) {
                                    $payments[$k]['ClientUserInfo']['company_cover'] = '';
                                } else {
                                    $image = $client_images[$k]['ClientImage']['image_file_path'];
                                    $img = WEBSITE_ROOT . 'media/img/' . $image;
                                    $payments[$k]['ClientUserInfo']['company_cover'] = $img;
                                }
                            } else {
                                $payments[$k]['ClientUserInfo']['company_cover'] = '';
                            }
                        }

                        $k++;
                    }

                    $message = $payments;
                    $i = 'yes';
                } else {
                    $message = array();
                    $i = 'yes';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function create_default_filter($agent_id) {
        $i = 'no';

        if (!empty($agent_id) && $agent_id != 0) {
            $data['agent_id'] = $agent_id;
            $data['min_distance'] = MIN_DISTANCE;
            $data['max_distance'] = MAX_DISTANCE;
            $data['minimum_compensation'] = MIN_COMPENSATION;
            $data['maximum_compensation'] = MAX_COMPENSATION;
            $data['due_date_span'] = DUE_DATE_SPAN;
            $data['is_active'] = 1;
            /*

              if(DUE_DATE_SPAN == 'D')
              {
              $data['due_date_start'] = $agent_id;
              $data['due_date_end'] = $agent_id;
              }
              elseif(DUE_DATE_SPAN == 'W')
              {
              $data['due_date_start'] = $agent_id;
              $data['due_date_end'] = $agent_id;
              }
              elseif(DUE_DATE_SPAN == 'M')
              {
              $data['due_date_start'] = $agent_id;
              $data['due_date_end'] = $agent_id;
              }
              elseif(DUE_DATE_SPAN == 'Y')
              {
              $data['due_date_start'] = $agent_id;
              $data['due_date_end'] = $agent_id;
              } */
            $job_categories = $this->JobCategory->find('all', array('conditions' => array('JobCategory.is_active' => 1, 'JobCategory.is_deleted' => 0), 'recursive' => -1));
            //pr($job_categories);exit;
            if ($this->JobFilter->save($data)) {
                $job_filter = $this->JobFilter->find('first', array('order' => array('JobFilter.job_filter_id DESC'), 'recursive' => -1));
                $job_filter_id = $job_filter['JobFilter']['job_filter_id'];
                $job_filter_data['agent_id'] = $agent_id;
                //pr($job_filter);exit;
                $k = 0;
                foreach ($job_categories as $jcat) {
                    $agent_filter_data[$k]['job_filter_id'] = $job_filter_id;
                    $agent_filter_data[$k]['agent_id'] = $agent_id;
                    $agent_filter_data[$k]['job_category_id'] = $jcat['JobCategory']['job_category_id'];
                    $agent_filter_data[$k]['is_checked'] = 1;
                    $k++;
                }
                if ($this->JobFilterCategory->saveall($agent_filter_data)) {
                    $message = true;
                } else {
                    $message = false;
                }
            } else {
                $message = false;
            }
        } else {
            $message = false;
        }

        return $message;
    }

    public function update_job_filter() {
        $i = 'no';

        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['job_filter_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $job_filter_id = $this->request->data['job_filter_id'];
                if (!empty($agent_id) && $agent_id != 0) {
                    $data['job_filter_id'] = $job_filter_id;
                    $data['agent_id'] = $agent_id;
                    $data['min_distance'] = isset($this->request->data['min_distance']) ? $this->request->data['min_distance'] : 0;
                    $data['max_distance'] = isset($this->request->data['max_distance']) ? $this->request->data['max_distance'] : 0;
                    $data['minimum_compensation'] = isset($this->request->data['minimum_compensation']) ? $this->request->data['minimum_compensation'] : 0;
                    $data['maximum_compensation'] = isset($this->request->data['maximum_compensation']) ? $this->request->data['maximum_compensation'] : 0;
                    $data['due_date_span'] = isset($this->request->data['due_date_span']) ? $this->request->data['due_date_span'] : 30;
                    $data['is_active'] = 1;
                    /*

                      if(DUE_DATE_SPAN == 'D')
                      {
                      $data['due_date_start'] = $agent_id;
                      $data['due_date_end'] = $agent_id;
                      }
                      elseif(DUE_DATE_SPAN == 'W')
                      {
                      $data['due_date_start'] = $agent_id;
                      $data['due_date_end'] = $agent_id;
                      }
                      elseif(DUE_DATE_SPAN == 'M')
                      {
                      $data['due_date_start'] = $agent_id;
                      $data['due_date_end'] = $agent_id;
                      }
                      elseif(DUE_DATE_SPAN == 'Y')
                      {
                      $data['due_date_start'] = $agent_id;
                      $data['due_date_end'] = $agent_id;
                      } */

                    /* $job_categories = '[
                      {
                      "job_category_id": 1,
                      "job_filter_category_id": 12,
                      "is_checked": 1
                      },
                      {
                      "job_category_id": 2,
                      "job_filter_category_id": 13,
                      "is_checked": 0
                      },
                      {
                      "job_category_id": 3,
                      "job_filter_category_id": 14,
                      "is_checked": 1
                      }
                      ]';
                     */
                    $job_categories = $this->request->data['job_categories'];
                    $filter_categories = json_decode($job_categories, true);
                    //pr($filter_categories);
                    if ($this->JobFilter->save($data)) {
                        $k = 0;
                        foreach ($filter_categories as $jcat) {
                            $agent_filter_data[$k]['job_filter_id'] = $job_filter_id;
                            $agent_filter_data[$k]['agent_id'] = $agent_id;
                            $agent_filter_data[$k]['job_category_id'] = $jcat['job_category_id'];
                            $agent_filter_data[$k]['job_filter_category_id'] = isset($jcat['job_filter_category_id']) ? $jcat['job_filter_category_id'] : 0;
                            $agent_filter_data[$k]['is_checked'] = isset($jcat['is_checked']) ? $jcat['is_checked'] : 0;
                            $k++;
                        }
                        //pr($agent_filter_data);exit;
                        if ($this->JobFilterCategory->saveAll($agent_filter_data)) {
                            $message = 'Filter successfully saved';
                            $i = 'yes';
                        } else {
                            $message = 'Unable to process your request, Please try again later.';
                        }
                    } else {
                        $message = 'Unable to process your request, Please try again later.';
                    }
                } else {
                    $message = 'Unable to process your request, Please try again later.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function application_feedback() {
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['feedback_description'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $i = 'no';
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if (!empty($agent_id) && $agent_id != 0) {
                    $this->request->data['agent_id'] = $agent_id;
                    if ($this->Feedback->save($this->request->data)) {
                        $message = 'Feedback sent successfully';
                        $i = 'yes';
                    } else {
                        $message = 'Unable to process your request, Please try again later.';
                    }
                } else {
                    $message = 'Unable to process your request, Please try again later.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function delete_account() {
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $i = 'no';
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if (!empty($agent_id) && $agent_id != 0) {
                    $this->request->data['agent_id'] = $agent_id;
                    $this->request->data['is_active'] = 0;
                    if ($this->AgentUserInfo->save($this->request->data)) {
                        $message = 'Your account has been deleted !';
                        $i = 'yes';
                    } else {
                        $message = 'Unable to process your request, Please try again later.';
                    }
                } else {
                    $message = 'Unable to process your request, Please try again later.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function my_coupons() {
        $i = 'no';
        $k = 0;
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if ($coupons = $this->AgentCoupon->find('all', array('fields' => array('AgentCoupon.*', 'Coupon.*', 'ClientUserInfo.company_name', 'AgentUserInfo.agent_name'), 'conditions' => array('AgentCoupon.agent_id' => $agent_id), 'recursive' => 0))) {
                    //pr($coupons);exit;
                    foreach ($coupons as $coupon) {
                        if (empty($coupons[$k]['Coupon']['coupon_image'])) {
                            $coupons[$k]['Coupon']['coupon_cover'] = '';
                        } else {
                            $image = $coupons[$k]['Coupon']['coupon_image'];
                            $img = WEBSITE_ROOT . 'media/img/' . $image;
                            $coupons[$k]['Coupon']['coupon_cover'] = $img;
                        }

                        if (empty($coupons[$k]['Coupon']['coupon_qr_code_image'])) {
                            $coupons[$k]['Coupon']['coupon_qr_image'] = '';
                        } else {
                            $image = $coupons[$k]['Coupon']['coupon_qr_code_image'];
                            $img = WEBSITE_ROOT . 'media/qr_code/' . $image;
                            $coupons[$k]['Coupon']['coupon_qr_image'] = $img;
                        }
                        $k++;
                    }
                    $message = $coupons;
                    $i = 'yes';
                } else {
                    $message = 'No records found';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function agent_coupon_details() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['coupon_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $coupon_id = $this->request->data['coupon_id'];
                if ($coupons = $this->Coupon->find('all', array('conditions' => array('Coupon.coupon_id' => $coupon_id), 'recursive' => 0))) {
                    $message = $coupons;
                    $i = 'yes';
                } else {
                    $message = 'No records found';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function unassign_job() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['agent_job_id'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $agent_job_id = $this->request->data['agent_job_id'];
                $this->AgentJob->id = $agent_job_id;
                if ($this->AgentJob->saveField('cancelled_by_agent', 1)) {
                    $message = 'You have unassigned a job';
                    $i = 'yes';
                } else {
                    $message = 'Unable to process your request, Please try again later.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function country_list() {
        $i = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                if ($countries = $this->Country->find('all', array('conditions' => array('Country.is_active' => 1), 'recursive' => -1))) {
                    $message = $countries;
                    $i = 'yes';
                } else {
                    $message = 'No countries are listed.';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }

        if ($i == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_all_jobs() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        $today = date("Y-m-d H:i:s");
        if ($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $curr_lat = $this->request->data['current_lat'];
                $curr_lng = $this->request->data['current_lng'];

                if ($applied_job_ids = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.cancelled_by_agent' => 0), 'recursive' => -1))) {
                    $k = 0;
                    foreach ($applied_job_ids as $job_ids) {
                        $applied_jobs[$k] = $this->Job->find('first', array('fields' => array('Job.*', 'Coupon.*', 'JobCategory.job_category_title'), 'conditions' => array('Job.job_id' => $applied_job_ids[$k]['AgentJob']['job_id'], 'Job.is_active' => 1, 'Job.is_blocked' => 0), 'recursive' => 0));
                        $applied_jobs[$k]['AgentJob'] = $job_ids['AgentJob'];
                        $k++;
                    }

                    $applied_jobs = $this->get_distance($applied_jobs, $curr_lat, $curr_lng);
                    $applied_jobs = array_values(array_filter($applied_jobs));
                    //pr($similardeals);
                    if (!empty($applied_jobs)) {
                        $message = $this->list_jobs($applied_jobs);
                        $status = 'yes';
                    } else {
                        $message = array();
                    }

                    $messsage = array_values(array_filter($message));
                    $status = 'yes';
                } else {
                    $message = 'You don\'t have any jobs yet';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function view_all_ratings() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        $today = date("Y-m-d H:i:s");
        $rating = 0;
        $count = 0;
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $curr_lat = isset($this->request->data['current_lat']) ? $this->request->data['current_lat'] : 0;
                $curr_lng = isset($this->request->data['current_lng']) ? $this->request->data['current_lng'] : 0;

                if ($ratings = $this->AgentRating->find('all', array('fields' => array('AgentRating.*', 'ClientUserInfo.company_name', 'ClientUserInfo.company_logo', 'ClientUserInfo.agency_name'), 'conditions' => array('AgentRating.agent_id' => $agent_id), 'recursive' => 1))) {
                    $k = 0;
                    foreach ($ratings as $rating) {
                        if (empty($ratings[$k]['ClientUserInfo']['company_logo'])) {
                            $ratings[$k]['ClientUserInfo']['company_logo_image'] = '';
                        } else {
                            $image = $ratings[$k]['ClientUserInfo']['company_logo'];
                            $img = WEBSITE_ROOT . 'media/img/' . $image;
                            $ratings[$k]['ClientUserInfo']['company_logo_image'] = $img;
                        }
                        $k++;
                    }

                    /* $avg = $this->AgentRating->find( 'all', array('conditions' => array('AgentRating.agent_id' => $agent_id), 'recursive' => 0, 'fields'    => array(  'AVG( AgentRating.rating_points ) AS average')));
                      foreach( $avg as $re)
                      {
                      foreach($re as $r)
                      {
                      foreach($r as $rate)
                      {
                      $rating = round($rate,1);
                      }
                      }
                      }

                      $count = $this->AgentRating->find('count',array('conditions' => array('AgentRating.agent_id' => $agent_id),'recursive' => 1  ) );


                      $ratings['Rating']['AvgRating'] = $rating;
                      $ratings['Rating']['RatingCount'] = $count; */

                    $message = $ratings;
                    $status = 'yes';
                } else {
                    $message = 'You don\'t have any jobs yet';
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function search_agent_jobs() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['search_key']) && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $curr_lat = $this->request->data['current_lat'];
                $curr_lng = $this->request->data['current_lng'];
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $starting_number = isset($this->request->data['starting_number']) ? $this->request->data['starting_number'] : 0;
                $limit = PAGINATION_LIMIT;

                $search_key = $this->request->data['search_key'];

                if ($agent_jobs = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.cancelled_by_agent' => 0), 'order' => array('AgentJob.modified' => 'DESC'), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => -1))) {
                    //pr($agent_jobs);exit;
                    $k = 0;
                    foreach ($agent_jobs as $job_agent) {
                        $joblocations = $this->JobLocation->find('first', array('fields' => array('JobLocation.*', 'ClientAddress.*'), 'conditions' => array('JobLocation.job_location_id' => $agent_jobs[$k]['AgentJob']['job_location_id']), 'recursive' => 0));

                        $job_agent['AgentJob']['JobAddress'] = $joblocations['ClientAddress'];

                        $jobs[$k] = $this->Job->find('first', array('fields' => array('Job.*', 'Coupon.*', 'JobCategory.job_category_title'), 'conditions' => array('OR' => array('Job.job_title LIKE' => '%' . $search_key . '%', 'Job.job_description LIKE' => '%' . $search_key . '%'), 'Job.job_id' => $job_agent['AgentJob']['job_id'], 'Job.is_blocked' => 0, 'Job.is_active' => 1), 'order' => array('Job.modified' => 'DESC'), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => 0));
                        $jobs[$k]['AgentJob'] = $job_agent['AgentJob'];
                        $k++;
                    }

                    if (count($jobs) < $limit) {
                        $l = 0;
                        foreach ($jobs as $agent_job) {
                            //pr($jobs);exit;
                            if (!empty($jobs[$l]['AgentJob'])) {
                                $clients[$l] = $this->ClientUserInfo->field('ClientUserInfo.client_id', array('OR' => array('ClientUserInfo.client_name LIKE' => '%' . $search_key . '%', 'ClientUserInfo.company_name LIKE' => '%' . $search_key . '%', 'ClientUserInfo.company_overview LIKE' => '%' . $search_key . '%'), 'ClientUserInfo.client_id' => $jobs[$l]['AgentJob']['client_id']));
                                //pr($clients);exit;
                                /* 	if(!empty($clients))
                                  {

                                  $client_jobs[$l] = $this->Job->find('all', array('fields' => array('Job.*'),'conditions' => array('Job.is_blocked' =>0 , 'Job.is_active' => 1, 'Job.client_id' => $clients['ClientUserInfo']['client_id']),'order' =>array('Job.modified' => 'DESC'), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));

                                  }
                                  else{} */
                            } else {
                                unset($jobs[$l]);
                            }
                            $l++;
                        }
                    }

                    if (!empty($clients)) {
                        $m = 0;
                        foreach ($clients as $client_id) {
                            if ($agent_jobs = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.client_id' => $client_id, 'AgentJob.cancelled_by_agent' => 0), 'order' => array('AgentJob.modified' => 'DESC'), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => -1))) {
                                //pr($agent_jobs);exit;
                                $k = 0;
                                foreach ($agent_jobs as $job_agent) {
                                    $joblocations = $this->JobLocation->find('first', array('fields' => array('JobLocation.*', 'ClientAddress.*'), 'conditions' => array('JobLocation.job_location_id' => $agent_jobs[$k]['AgentJob']['job_location_id']), 'recursive' => 0));

                                    $job_agent['AgentJob']['JobAddress'] = $joblocations['ClientAddress'];

                                    $client_jobs[$k] = $this->Job->find('first', array('fields' => array('Job.*', 'Coupon.*', 'JobCategory.job_category_title'), 'conditions' => array('Job.job_id' => $job_agent['AgentJob']['job_id'], 'Job.is_blocked' => 0, 'Job.is_active' => 1), 'order' => array('Job.modified' => 'DESC'), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => 0));
                                    //pr($client_jobs);exit;
                                    $client_jobs[$k]['AgentJob'] = $job_agent['AgentJob'];
                                    $k++;
                                }
                            }
                        }
                    }

                    //pr($jobs);
                    //pr($client_jobs);exit;
                    if (!empty($client_jobs)) {
                        $jobs = array_merge($jobs, $client_jobs);
                    }
                    $jobs = array_filter($jobs);
                    $jobs = array_values($jobs);
                    $jobs = array_slice($jobs, 0, $limit);
                    $jobs = array_filter($jobs);
                    //pr($jobs);exit;
                    $jobs = $this->get_distance($jobs, $curr_lat, $curr_lng);
                    $jobs = array_values(array_filter($jobs));

                    if (!empty($jobs)) {

                        $message = $this->list_jobs($jobs);
                        $status = 'yes';
                        //pr($jobs);exit;
                    } else {
                        $message = array();
                    }
                } else {
                    $message = 'No records Found';
                }

                //pr($message);
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function search_assigned_jobs() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code']) && !empty($this->request->data['search_key']) && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                $curr_lat = $this->request->data['current_lat'];
                $curr_lng = $this->request->data['current_lng'];
                $agent_id = $agent['AgentUserInfo']['agent_id'];
                $starting_number = isset($this->request->data['starting_number']) ? $this->request->data['starting_number'] : 0;
                $limit = PAGINATION_LIMIT;

                $search_key = $this->request->data['search_key'];

                if ($agent_jobs = $this->AgentJob->find('all', array('fields' => array('AgentJob.*'), 'conditions' => array('AgentJob.agent_id' => $agent_id, 'AgentJob.is_confirmed_by_client' => 1, 'AgentJob.is_completed_by_agent' => 0, 'AgentJob.is_verified_by_client' => 0, 'AgentJob.cancelled_by_agent' => 0), 'recursive' => -1))) {
                    //pr($agent_jobs);exit;
                    $k = 0;
                    foreach ($agent_jobs as $job_agent) {
                        $joblocations = $this->JobLocation->find('first', array('fields' => array('JobLocation.*', 'ClientAddress.*'), 'conditions' => array('JobLocation.job_location_id' => $agent_jobs[$k]['AgentJob']['job_location_id']), 'recursive' => 0));

                        $job_agent['AgentJob']['JobAddress'] = $joblocations['ClientAddress'];

                        $jobs[$k] = $this->Job->find('first', array('fields' => array('Job.*', 'Coupon.*', 'JobCategory.job_category_title'), 'conditions' => array('OR' => array('Job.job_title LIKE' => '%' . $search_key . '%', 'Job.job_description LIKE' => '%' . $search_key . '%'), 'Job.job_id' => $job_agent['AgentJob']['job_id'], 'Job.is_blocked' => 0, 'Job.is_active' => 1), 'order' => array('Job.modified' => 'DESC'), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => 0));
                        $jobs[$k]['AgentJob'] = $job_agent['AgentJob'];
                        $k++;
                    }

                    if (count($jobs) < $limit) {
                        $l = 0;
                        foreach ($jobs as $agent_job) {
                            //pr($jobs);exit;
                            if (!empty($jobs[$l]['AgentJob'])) {
                                $clients = $this->ClientUserInfo->find('first', array('fields' => array('ClientUserInfo.client_id'), 'conditions' => array('OR' => array('ClientUserInfo.client_name LIKE' => '%' . $search_key . '%', 'ClientUserInfo.company_name LIKE' => '%' . $search_key . '%', 'ClientUserInfo.company_overview LIKE' => '%' . $search_key . '%'), 'ClientUserInfo.client_id' => $jobs[$l]['AgentJob']['client_id']), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => 0));
                                //pr($clients);exit;
                                if (!empty($clients)) {

                                    $client_jobs[$l] = $this->Job->find('all', array('fields' => array('Job.*', 'Coupon.*', 'JobCategory.job_category_title'), 'conditions' => array('Job.is_blocked' => 0, 'Job.is_active' => 1, 'Job.client_id' => $clients['ClientUserInfo']['client_id'], 'Job.job_id' => $jobs[$l]['AgentJob']['job_id']), 'order' => array('Job.modified' => 'DESC'), 'limit' => $limit, 'offset' => $starting_number, 'recursive' => 0));
                                } else {
                                    
                                }
                            } else {
                                unset($jobs[$l]);
                            }
                            $l = 0;
                        }
                    }

                    // pr($merchants);
                    //pr($client_jobs);exit;

                    if (!empty($client_jobs)) {
                        $client_jobs = array_filter($client_jobs);
                        foreach ($client_jobs as $search_clients) {
                            $client_jobs = $search_clients;
                        }
                        $jobs = array_merge($jobs, $client_jobs);
                    }

                    $jobs = array_filter($jobs);
                    $jobs = array_values($jobs);
                    $jobs = array_slice($jobs, 0, $limit);
                    $jobs = array_filter($jobs);
                    //pr($jobs);exit;
                    $jobs = $this->get_distance($jobs, $curr_lat, $curr_lng);
                    $jobs = array_values(array_filter($jobs));

                    if (!empty($jobs)) {

                        $message = $this->list_jobs($jobs);
                        $status = 'yes';
                        //pr($jobs);exit;
                    } else {
                        $message = array();
                    }
                } else {
                    $message = 'No records Found';
                }

                //pr($message);
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }

    public function welcome_message() {
        $this->RequestHandler->renderAs($this, 'json');
        $status = 'no';
        if ($this->request->is('post') && !empty($this->request->data['access_code'])) {
            if ($agent = $this->grant_access($this->request->data['access_code'])) {
                if ($allowed_jobs = $this->AgentRank->find('first', array('conditions' => array('AgentRank.rank' => $agent['AgentUserInfo']['agent_rank']), 'recursive' => -1))) {
                    $message = "Hello " . $agent['AgentUserInfo']['agent_name'] . "! Your shopper score is " . $agent['AgentUserInfo']['points_earned'] . " which has earned you a rank of " . $agent['AgentUserInfo']['agent_rank'] . "! you can accept " . $allowed_jobs['AgentRank']['allowed_jobs'] . " shops at a time- Happy Shopping !";
                    $status = 'yes';
                } else {
                    $message = "Unable to process your request. Please try again later";
                }
            } else {
                $message = 'Please login Again !';
            }
        } else {
            $message = 'invalid request';
        }
        if ($status == 'no') {
            $this->set(array(
                'Error' => array('error' => $message),
                '_serialize' => array('Error')));
        } else {
            $this->set(array(
                'Success' => array('success' => $message),
                '_serialize' => array('Success')));
        }
    }
	
	public function send_email($from, $to, $subject, $message, $template) {

        $this->Email = new CakeEmail();
		$this->Email->config('smtp');
        $this->Email->to($to);
        $this->Email->subject($subject);
        $this->Email->emailFormat('html');

        $this->Email->from($from);
        $this->Email->template($template);

        ob_start();
        $this->Email->send($message);
        ob_end_clean();
    }

}
