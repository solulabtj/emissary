<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller','File', 'Utility');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $uses = array('AgentUserInfo','ClientUserInfo','TestBooking','JobImage','ClientImage','JobLocation','AdminDashMsg');
	public $components = array(
		'Session',
        'Auth',
		'Security'
	);
	public function beforeFilter() {
		ini_set('memory_limit', '-1');
		
		$this->checkUserValidityFromWeb();
		
		$this->Auth->allow($this->action);
		$this->Auth->autoRedirect = false;		
		if(in_array($this->params['controller'],array('agents','activations','tests','jobs','home'))){
			// For RESTful web service requests, we check the name of our contoller
			$this->Auth->allow();
			// this line should always be there to ensure that all rest calls are secure
			/* $this->Security->requireSecure(); */
			$this->Security->unlockedActions = array('agent_sign_up','agent_login','agent_social_login','agent_forgot_password','agent_change_password','agent_logout','a_forgot','view_profile','update_profile','view_rating','book_test','start_test','submit_test','jobs_near_me','jobs_near_home','search_job','view_job_details','view_client_details','view_applied_jobs','view_assigned_jobs','view_completed_jobs','apply_job','set_preferences','get_preferences','set_job_alert','view_test_results','agent_check_in','start_job','update_job_alert','view_job_alert','my_job_alerts','delete_job_alert','my_payments','submit_job','create_default_filter','update_job_filter','get_available_shops','application_feedback','delete_account','book_test','book','ap2','payment_success','my_coupons','agent_coupon_details','privacy_policy','terms_of_service','about_us','start_survey','unassign_job','country_list','view_all_jobs','view_all_ratings','search_agent_jobs','start_job_survey','get_questions_from_category','agent_search_jobs','search_assigned_jobs','submit_job_media','view_job','take_test','welcome_message','pending_payments','tap','tapReturn');
			
		}else{
			// setup out Auth
			$this->Auth->allow($this->action);			
		}
    }
	
	
	public function checkUserValidityFromWeb() {
		$arrApacheHeaders = apache_request_headers();
		
		if(isset($arrApacheHeaders['Access-Type-Web']) && $arrApacheHeaders['Access-Type-Web'] == "1") {
			$accessTokenFromClient = $arrApacheHeaders['Access-Token'];
			$accessCodeFromClient = $arrApacheHeaders['Access-Code'];
			
			$qrySel = "	SELECT access_code
						FROM agent_user_infos
						WHERE MD5(CONCAT(email_address, '" . $accessCodeFromClient . "', agent_id, created)) = '" . $accessTokenFromClient . "'";
			$arrObj = $this->AgentUserInfo->query($qrySel);
			$this->request->data['access_code'] = $arrObj[0]['agent_user_infos']['access_code'];
		}
	}
	
	public function isAuthorized($user) {
    // Here is where we should verify the role and give access based on role
     
    return true;
	}
	
	function send_email($from,$to,$subject,$message,$template)
	{
		
		$this->Email = new CakeEmail();
		//$this->set('data','TJ');
		$this->Email->to($to);
		$this->Email->subject($subject);
		$this->Email->emailFormat('html');
		//$this->Email->template('default');
		$this->Email->from($from);
		$this->Email->template($template);
		//$this->Email->return = 'gehlot.tejen111@gmail.com';
		//$this->Email->sender('gehlot.tejen111@gmail.com', 'Admin');
		//$this->Email->sendAs('both'); 
		ob_start();
		$this->Email->send($message);
		ob_end_clean();
	}
	
	
	function send_push_android($id, $title, $message)
	{
		return "here only";
		// API access key from Google API's Console
		//define( 'API_ACCESS_KEY', PUSH_API_MERCHANT );
		
		
		$registrationIds = $id;
		//pr($registrationIds);
		// prep the bundle
		$msg = array
		(
			'message' 	=> $message,
			'title'		=> $title,
			'subtitle'	=> '',
			'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		);
		
		$fields = array
		(
			'registration_ids' 	=> $registrationIds,
			'data'			=> $msg
		);
		 
		$headers = array
		(
			'Authorization: key=' . PUSH_API_ANDROID,
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		
		$result;
		//exit;
	}
	
	function send_push_ios($id, $title, $message)
	{
		$ctx = stream_context_create();
		$passphrase	=	'112233';
		$message	=	'Static';
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-prod.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		
		$deviceToken	=	'bf66973f357601189f5f7209ddb133cf74862554e6b1368fd2a03d64335cbaaa';
		/*$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, 
			$ctx);
*/
$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, 
			$ctx);
		
		//if (!$fp)
		//exit("Failed to connect amarnew: $err $errstr" . PHP_EOL);
		
		//echo 'Connected to APNS' . PHP_EOL;
		
		// Create the payload body
		$body['aps'] = array(
			'badge' => +1,
			'alert' => $message,
			'sound' => 'default'
		);
		
		$payload = json_encode($body);
		
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		
		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered'. PHP_EOL;
		
		// Close the connection to the server	
	}
	
	
	function generateRandomString($length = 12) {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		//return $randomString;
		return $this->check_bookingcode($randomString);
	}
	
	function check_bookingcode($booking_code)
	{
		if($booking = $this->TestBooking->find('first',array('conditions'=>array('TestBooking.test_booking_code'=>$booking_code),'recursive'=>-1)))
		{
				$booking_code = $this->generateRandomString();
				//$this->check_bookingcode($booking_code);
		}
		else
		{
			return $booking_code;
		}
	}
		
	function grant_access($access_code)
	{
		if($agent = $this->AgentUserInfo->find('first',array('conditions'=>array('access_code'=>$access_code, 'is_blocked' => 0, 'is_confirmed' => 1),'recursive'=>-1)))
		{
			$message = $agent;
		}
		elseif($client = $this->ClientUserInfo->find('first',array('conditions'=>array('access_code'=>$access_code, 'is_blocked' => 0, 'is_confirmed' => 1),'recursive'=>-1)))
		{
			$message = $client;
		}
		else
		{
			$message = 0;
		}
		return $message;
	}
		
	
	function list_jobs($nearby_jobs)
	{
		$j=0;
		//pr($nearby_jobs);exit;
		foreach($nearby_jobs as $listjobs)
		{
			$client_id = $listjobs['Job']['client_id'];
			
			$joblocations[$j] = $this->JobLocation->find('all',array('fields'=>array('JobLocation.*','ClientAddress.*'),'conditions'=>array('JobLocation.job_id' => $nearby_jobs[$j]['Job']['job_id']),'recursive'=>0));
			$clientdetails[$j] = $this->ClientUserInfo->find('all',array('fields'=>array('ClientUserInfo.*'),'conditions'=>array('ClientUserInfo.client_id' => $nearby_jobs[$j]['Job']['client_id']),'recursive'=>0));
					
			//Temp assigned variables
			$nearby_jobs[$j]['Job']['job_compensation'] = $nearby_jobs[$j]['Job']['job_compensation'].' KWD';
			
			$j++;
		}
		
		$y=0;
		foreach($clientdetails as $clientdetail)
		{
			foreach($clientdetail as $clients)
			{
			$clientdetails[$y] = $clients;
			$clientimage[$y] = $this->ClientImage->find('all',array('conditions'=>array('ClientImage.client_id' => $nearby_jobs[$y]['Job']['client_id'],'ClientImage.is_default'=>1),'recursive'=>-1));
			$nearby_jobs[$y]['ClientDetails'] = $clients;
			
			if(empty($nearby_jobs[$y]['ClientDetails']['ClientUserInfo']['company_logo']))
			{	
				$nearby_jobs[$y]['ClientDetails']['ClientUserInfo']['company_logo_image'] = '';
			}
			else
			{
				$image = $nearby_jobs[$y]['ClientDetails']['ClientUserInfo']['company_logo'];
				$img = WEBSITE_ROOT.'media/img/'.$image; 
				$nearby_jobs[$y]['ClientDetails']['ClientUserInfo']['company_logo_image'] = $img;
			}
			
			$y++;
				
			}
		}
		
		//pr($joblocations);exit;
		
		$y=0;
		foreach($joblocations as $joblocation)
		{
			$v=0;
			foreach($joblocation as $jobaddress)
			{
				$nearby_jobs[$y]['Job']['JobAddress'][$v] = $jobaddress['ClientAddress'];	
				$nearby_jobs[$y]['Job']['JobAddress'][$v]['job_location_id'] = $jobaddress['JobLocation']['job_location_id'];
				$nearby_jobs[$y]['Job']['JobAddress'][$v]['job_id'] = $jobaddress['JobLocation']['job_id'];	
				$v++;			
			}
			$y++;
		}
		
		$k=0;
		$x=0;
		foreach($nearby_jobs as $jobs)
		{
			foreach($clientimage as $clientimg)
			{
				foreach($clientimg as $clientcover)
					{
							
						if(!isset($clientcover['ClientImage']['image_file_path']))
						{	
							$nearby_jobs[$x]['ClientDetails']['ClientUserInfo']['company_cover'] = '';
						}
						else
						{
							
							$image = $clientcover['ClientImage']['image_file_path'];
							$img = WEBSITE_ROOT.'media/img/'.$image;
							$nearby_jobs[$x]['ClientDetails']['ClientUserInfo']['company_cover'] = $img;
							
						}
						$k++;
					}
				
				$k=0;
			}
			$x++;
		}
		
			
		$n=0;
		
		$message = $nearby_jobs;
	//	pr($message);exit;
		return $message;
	}
	function store_reg_agent_device($device_id,$registration_id,$agent_id,$device_type)
	{
		$data['device_id'] = $device_id;
		$data['registration_id'] = $registration_id;
		$data['agent_id'] = $agent_id;
		$data['device_type'] = $device_type;
		if($device = $this->AgentDevice->find('first',array('conditions'=>array('device_id'=>$device_id,'device_type'=>$device_type),'recursive'=>-1)))
		{
			$data['agent_device_id'] = $device['AgentDevice']['agent_device_id'];
			$this->AgentDevice->save($data);
			$message = 'Device id updated';
		}
		else
		{
			$this->AgentDevice->save($data);
			$message = 'Deviceid added';
		}
		return $message;
	}
		
	function get_distance($jobs,$lat,$lng)	
	{
		//pr($jobs);exit;
		$d=0;
		foreach($jobs as $job)
		{
			if(!empty($jobs[$d]['Job']))
			{
				$job_distance = $this->ClientAddress->query("SELECT *, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lattitude ) ) ) ) AS distance FROM client_address AS ClientAddress WHERE client_address_id = " .$jobs[$d]['AgentJob']['JobAddress']['client_address_id']. " ORDER BY distance;");
				//pr($job_distance);exit;
				$jobs[$d]['Job']['distance'] = number_format($job_distance[0][0]['distance'],1);	
			}
			else
			{
				unset($jobs[$d]);	
			}
			$d++;
		}
		return $jobs;
	}
		
	function paypal_payment($paypal_amount)
	{	
		$posted['paypal_id'] = PAYPAL_AUTH_ID;
		$posted['tx'] = PAYPAL_TX;
		$posted['at'] = PAYPAL_AT;
		$posted['return'] = PAYPAL_SURL;
		$posted['cancel_return'] = PAYPAL_FURL;
		$posted['currency_code'] = PAYPAL_CURRENCY;
		$posted['cmd'] = PAYPAL_CMD;
		$posted['items'] = array("Emissary Online Test"=>$paypal_amount);;

		return $posted;

	}
	
	function agent_notification($user,$msg_text)
	{
		$reg_ids = $this->AgentDevice->find('all',array('fields'=>array('AgentDevice.registration_id','AgentDevice.device_type'),'conditions'=>array('AgentDevice.agent_id'=>$agent_id),'recursive'=>-1));	
		//pr($reg_ids);
//		exit;
		$j=0;
		foreach($reg_ids as $reg_id)
		{
			$reg_ids[$j] = $reg_id['AgentDevice']['registration_id'];
			$j++;
		}
		//$reg_ids = json_encode($reg_ids);
		$title = 'Emissary';
		$this->send_push_user($reg_ids, $title, $msg_text);
		
	}
	
	function saveAdminMsg($msg_text)
	{
		//echo "asasdasd";exit;
		$count = $this->AdminDashMsg->find('count');
		
		$min_query = "SELECT MIN(msg_id) as MsgId
						FROM admin_dash_msgs";
		
		$minIdArr = $this->AdminDashMsg->query($min_query);
		$minId = $minIdArr[0][0]['MsgId'];
		
		$query = "DELETE FROM admin_dash_msgs WHERE msg_id = ".$minId."";
		if($count >= 5)
		{
			$this->AdminDashMsg->query($query);	
		}
		$data['display_msg'] = $msg_text;
		$this->AdminDashMsg->save($data);
		return true;
	}
	
	function saveClientMsg($msg_text)
	{
		//echo "asasdasd";exit;
		$count = $this->ClientDashMsg->find('count');
		
		$min_query = "SELECT MIN(msg_id) as MsgId
						FROM client_dash_msgs";
		
		$minIdArr = $this->ClientDashMsg->query($min_query);
		$minId = $minIdArr[0][0]['MsgId'];
		
		$query = "DELETE FROM client_dash_msgs WHERE msg_id = ".$minId."";
		if($count >= 5)
		{
			$this->ClientDashMsg->query($query);	
		}
		$data['display_msg'] = $msg_text;
		$this->ClientDashMsg->save($data);
		return true;
	}
	
}