<?php

class JobsController extends AppController {
	public $uses = array('AgentUserInfo','AgentDevice','AgentSocialLogin','AgentRating','TestSetting','ClientUserInfo','Job','TestResult','JobFilter','JobFilterCategory','JobCategory','AgentJob','Coupon','ClientAddress');
    public $helpers = array('Html', 'Form');
	public $components = array('RequestHandler');

	 function beforeFilter()
	 {
		 parent::beforeFilter();
		 $this->RequestHandler->ext = 'json';
	 }	

	// public function index() {
		// //$this->RequestHandler->renderAs($this, 'json');	
		// $users = $this->User->find('all');
        // $this->set(array(
            // 'Success' => $users,
            // '_serialize' => array('Success')
        // ));
    // }
	
	
	
	//New web services
	public function jobs_near_me() {
		$i = 'no';
		
		if($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code']))
		{   
			$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
			$limit = PAGINATION_LIMIT;
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$agent_id = $agent['AgentUserInfo']['agent_id'];
				if($test_status = $this->TestResult->find('first',array('conditions'=>array('TestResult.agent_id'=>$agent_id,'TestResult.is_passed'=>1), 'recursive'=>-1)))
				{
					$curr_lat = $this->request->data['current_lat'];
					$curr_lng = $this->request->data['current_lng'];
					
					if($nearby_jobs = $this->Job->query("SELECT *, ( 3959 * acos( cos( radians(".$curr_lat.") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(".$curr_lng.") ) + sin( radians(".$curr_lat.") ) * sin( radians( lattitude ) ) ) ) AS distance FROM jobs AS Job WHERE job_deadline >= '" .NOW. "' AND is_active = 1 AND is_blocked = 0 ORDER BY distance LIMIT ".$starting_number." , ".$limit.";"))
					{
						//pr($nearby_jobs);exit;
						$nearby_jobs = $this->get_distance($nearby_jobs,$curr_lat,$curr_lng);
						$nearby_jobs = array_values(array_filter($nearby_jobs));
						//pr($similardeals);
						if(!empty($nearby_jobs))
						{
							$message = $this->list_jobs($nearby_jobs);
							$status = 'yes';
						}
						else
						{
							$message = array();
						}
							
						$messsage = array_values(array_filter($message));
						$i='yes';
					}
					else 
					{
						$message = 'No jobs are listed near your location !';
					}	
				}
				else
				{
					$message = 'You haven\'t cleared any tests yet. Please clear a test first.';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
	public function jobs_near_home() {
		$i = 'no';
		if($this->request->is('post') && !empty($this->request->data['access_code']))
		{   
			$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
			$limit = PAGINATION_LIMIT;
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$agent_id = $agent['AgentUserInfo']['agent_id'];
				if($test_status = $this->TestResult->find('first',array('conditions'=>array('TestResult.agent_id'=>$agent_id,'TestResult.is_passed'=>1), 'recursive'=>-1)))
				{
					$agent_lat = $agent['AgentUserInfo']['lattitude'];
					$agent_lng = $agent['AgentUserInfo']['longitude'];
					
					if($nearby_jobs = $this->Job->query("SELECT *, ( 3959 * acos( cos( radians(".$agent_lat.") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(".$agent_lng.") ) + sin( radians(".$agent_lat.") ) * sin( radians( lattitude ) ) ) ) AS distance FROM jobs AS Job WHERE is_active = 1 AND is_blocked = 0 AND job_deadline > '".NOW."' ORDER BY distance LIMIT ".$starting_number." , ".$limit.";"))
					{
						$nearby_jobs = $this->get_distance($nearby_jobs,$agent_lat,$agent_lng);
						$nearby_jobs = array_values(array_filter($nearby_jobs));
						//pr($similardeals);
						if(!empty($nearby_jobs))
						{
							$message = $this->list_jobs($nearby_jobs);
							$status = 'yes';
						}
						else
						{
							$message = array();
						}
							
						$messsage = array_values(array_filter($message));
						$i='yes';
					}
					else 
					{
						$message = 'No nearby jobs to show';
					}	
				}
				else
				{
					$message = 'You haven\'t cleared any tests yet. Please clear a test first.';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
	public function search_job() {
		$this->RequestHandler->renderAs($this, 'json');
		$status = 'no';
		if($this->request->is('post') && !empty($this->request->data['search_key']) && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']))
		{ 
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$curr_lat = $this->request->data['current_lat'];
				$curr_lng = $this->request->data['current_lng'];
		
				$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
				$limit = PAGINATION_LIMIT;
				
				$search_key = $this->request->data['search_key'];
					
				$jobs = $this->Job->find('all', array('fields' => array('Job.*'),'conditions' => array('OR' =>array('Job.job_title LIKE' => '%'.$search_key.'%', 'Job.job_description LIKE' => '%'.$search_key.'%'),  'Job.is_blocked' =>0 , 'Job.is_active' => 1),'order' =>array('Job.modified' => 'DESC'), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));
								
				if(count($jobs) < $limit)
				{
					$clients = $this->ClientUserInfo->find('all', array('fields' => array('ClientUserInfo.client_id'),'conditions' => array('OR' =>array('ClientUserInfo.client_name LIKE' => '%'.$search_key.'%', 'ClientUserInfo.client_address LIKE' => '%'.$search_key.'%')), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));
					if(!empty($clients))
					{
						$j=0;
						foreach($clients as $client)
						{
							$client_jobs[$j] = $this->Job->find('all', array('fields' => array('Job.*'),'conditions' => array('Job.is_blocked' =>0 , 'Job.is_active' => 1, 'Job.client_id' => $clients[$j]['ClientUserInfo']['client_id']),'order' =>array('Job.modified' => 'DESC'), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));
						
						$j++;
						}
					}
					else{}
				
				}
								
					// pr($merchants);
			//	pr($similarshopdeals);
				
				if(!empty($client_jobs))
				{
					$client_jobs = array_filter($client_jobs);
					foreach($client_jobs as $search_clients)
					{
						$client_jobs = $search_clients;	
					}
					$jobs = array_merge($jobs,$client_jobs);
				}
				
				
				$jobs = array_values($jobs);
				$jobs = array_slice($jobs,0,$limit);
				$jobs = array_filter($jobs);
				
				$jobs = $this->get_distance($jobs,$curr_lat,$curr_lng);
				$jobs = array_values(array_filter($jobs));
				
				if(!empty($jobs))
				{
					
					$message = $this->list_jobs($jobs);
					$status = 'yes';
				//	pr($jobs);exit;
				}
				else
				{
					$message = array();
				}
				
					//pr($message);
			}
			else 
			{
				$message = 'Please login Again !';
			}
				
		}
		else{$message = 'invalid request';}	
        if($status == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
    }
	
	public function view_client_details() {
		$this->RequestHandler->renderAs($this, 'json');
		$status = 'no';
		$today = date("Y-m-d H:i:s");
		if($this->request->is('post')  && !empty($this->request->data['client_id'])  && !empty($this->request->data['current_lat'])  && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code']))
		{ 
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$curr_lat = $this->request->data['current_lat'];
				$curr_lng = $this->request->data['current_lng'];
				
				if($clientdetail = $this->ClientUserInfo->find('first',array('fields' =>array('ClientUserInfo.*'),'conditions'=>array('ClientUserInfo.client_id' => $this->request->data['client_id'], 'ClientUserInfo.is_blocked' => 0,'ClientUserInfo.is_active' => 1 ),'recursive'=>-1)))
				{	
					$client_images = $this->ClientImage->find('all',array('fields' =>array('ClientImage.*'),'conditions'=>array('ClientImage.client_id' => $this->request->data['client_id'] ),'recursive'=>-1));
					
					if(!empty($client_images))
					{
						$k=0;
						foreach($client_images as $client_img)
						{
							if(!isset($client_img['ClientImage']['image_file_path']))
							{	
								$img_client[$k]['is_default'] = 0;
								$img_client[$k]['ClientImage']['image_path'] = '';
							}
							else
							{
								
								$image = $client_img['ClientImage']['image_file_path'];
								$img = WEBSITE_ROOT.'media/img/'.$image;
								$img_client[$k]['image_path'] = $img;
								$img_client[$k]['is_default'] = $client_img['ClientImage']['is_default'];
								
							}
							$k++;	
						}	
					}	
					$clientdetail['ClientImages']  = $img_client;
					//pr($clientdetail);exit;	
					$message = $clientdetail;
					$status='yes';
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
        if($status == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
    }
	
	
	public function get_available_shops() {
		$i = 'no';
		$jfcategory = array();
		$available_jobs = array();
		if($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code']))
		{   
			$starting_number = isset($this->request->data['starting_number'])?$this->request->data['starting_number']:0;
			$limit = PAGINATION_LIMIT;
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				$agent_id = $agent['AgentUserInfo']['agent_id'];
				
				if($agent_jobs = $this->AgentJob->find('all',array('fields'=> array('AgentJob.job_id'),'conditions'=>array('AgentJob.agent_id'=>$agent_id), 'recursive'=>-1)))
				{
					foreach($agent_jobs as $job_id)
					{
						$agent_job_ids[] = $job_id['AgentJob']['job_id']; 	
					}
				}
				//pr($agent_job_ids);exit;
				if($job_filter = $this->JobFilter->find('first',array('fields'=> array(),'conditions'=>array('JobFilter.agent_id'=>$agent_id,'JobFilter.is_active'=>1), 'recursive'=>1)))
				{
					$job_filter['JobFilter']['default_max_distance'] = MAX_DISTANCE;
					$job_filter['JobFilter']['default_max_compensation'] = MAX_COMPENSATION;
					$min_compensation = $job_filter['JobFilter']['minimum_compensation'];
					$max_compensation = $job_filter['JobFilter']['maximum_compensation'];
					$min_distance = $job_filter['JobFilter']['min_distance'];
					$max_distance = $job_filter['JobFilter']['max_distance'];
					$due_date_span = $job_filter['JobFilter']['due_date_span'];
					$curr_lat = $this->request->data['current_lat'];
					$curr_lng = $this->request->data['current_lng'];
					$start_date = NOW;
					$end_date = date('Y-m-d h:i:s', strtotime($start_date. ' +'.$due_date_span.' days')); //exit;
					
					if($job_categories = $this->JobCategory->find('all',array('fields'=> array(),'conditions'=>array('JobCategory.is_active'=>1), 'recursive'=>-1)))
					{
						foreach($job_filter['JobFilterCategory'] as $jfcat)
						{
							if($jfcat['is_checked']==1)
							{
								$jfcategory[] = $jfcat['job_category_id']; 	
							}	
						}
					}
					
					foreach($job_categories as $key => $job_category)
					{
						foreach($job_filter['JobFilterCategory'] as $job_filter_cat)
						{
							if($job_category['JobCategory']['job_category_id'] == $job_filter_cat['job_category_id'])
							{
								$job_categories[$key]['JobCategory']['job_filter_category_id'] = $job_filter_cat['job_filter_category_id'];
								$job_categories[$key]['JobCategory']['is_checked'] = $job_filter_cat['is_checked'];
								break;
							}
							else
							{
								$job_categories[$key]['JobCategory']['job_filter_category_id'] = 0;
								$job_categories[$key]['JobCategory']['is_checked'] = false;
							}	
						}
					}
					
					if(!empty($job_filter) && !empty($job_categories))
					{
						$available_jobs['AgentFilter'] = $job_filter;
						$available_jobs['JobCategories'] = $job_categories;	
					}
					
					if($nearby_clients = $this->ClientAddress->query("SELECT *, ( 3959 * acos( cos( radians(".$curr_lat.") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(".$curr_lng.") ) + sin( radians(".$curr_lat.") ) * sin( radians( lattitude ) ) ) ) AS distance FROM client_address AS Clients INNER JOIN job_locations AS JobLocation ON Clients.client_address_id=JobLocation.client_address_id group by JobLocation.job_id  HAVING distance >= ".$min_distance." AND distance <= ".$max_distance." ORDER BY distance LIMIT ".$limit." OFFSET ".$starting_number.";"))
					{
						//pr($nearby_clients);exit;
						$l=0;
						
						$client_jobs = array();
						
						foreach($nearby_clients as $clients)
						{
							$nearby_jobs[$l] = $this->Job->find('first', array('fields' => array('Job.*','Coupon.*','JobCategory.job_category_title'),'conditions' => array('Job.job_id'=>$clients['JobLocation']['job_id'],  'Job.is_blocked' =>0 , 'Job.is_active' => 1,'Job.job_compensation >=' => $min_compensation, 'Job.job_compensation <=' => $max_compensation, /*'Job.job_start_date >=' => NOW,*/ 'Job.job_end_date >=' => $start_date, 'Job.job_end_date <=' => $end_date), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));
							
														
							/*$nearby_jobs[$l] = $this->Job->find('first', array('fields' => array('Job.*','Coupon.*','JobCategory.job_category_title'),'conditions' => array('Job.job_id'=>$clients['JobLocation']['job_id']), 'limit' => $limit,'offset' => $starting_number , 'recursive' => 0));*/	
							if(!empty($nearby_jobs[$l]))
							{
								$nearby_jobs[$l]['Job']['distance'] = number_format($clients[0]['distance'],1);
								$l++;
							}
							else
							{
								unset($nearby_jobs[$l]);	
							}
							
						}
						
						/*pr($jfcategory);
						pr($agent_job_ids);
						pr($nearby_jobs);exit;*/
							
						if(!empty($nearby_jobs))
						{
							
							//pr($nearby_jobs);exit;
							$k=0;
							foreach($nearby_jobs as $jobs)
							{
								/*if($nearby_jobs[$k]['Job']['reward_type'] == 2 && !empty($nearby_jobs[$k]['Job']['coupon_id']))
								{
									$coupon = $this->Coupon->find('first',array('fields'=> array(),'conditions'=>array('Coupon.coupon_id'=>$nearby_jobs[$k]['Job']['coupon_id']), 'recursive'=>-1));
									$nearby_jobs[$k]['Coupon'] = $coupon['Coupon'];
									
								}
								$category = $this->JobCategory->find('first',array('fields'=> array('JobCategory.job_category_title'),'conditions'=>array('JobCategory.job_category_id'=>$nearby_jobs[$k]['Job']['job_category_id']), 'recursive'=>-1));
								
									//pr($client_address);exit;
								$nearby_jobs[$k]['JobCategory'] = $category['JobCategory'];*/
								$job_address = $this->JobLocation->find('first',array('fields'=> array('JobLocation.*'),'conditions'=>array('JobLocation.job_id'=>$nearby_jobs[$k]['Job']['job_id']), 'recursive'=>-1));
								$client_address = $this->ClientAddress->find('first',array('fields'=> array('ClientAddress.*'),'conditions'=>array('ClientAddress.client_address_id'=>$job_address['JobLocation']['client_address_id']), 'recursive'=>-1));
								$nearby_jobs[$k]['PrimaryJobAddress'] = $client_address['ClientAddress'];
								//pr($nearby_jobs[$k]['PrimaryJobAddress']);exit;
								if(!in_array($nearby_jobs[$k]['Job']['job_category_id'],$jfcategory) || in_array($nearby_jobs[$k]['Job']['job_id'],$agent_job_ids))
								{
									unset($nearby_jobs[$k]);
								}
							/*	if(in_array($nearby_jobs[$k]['Job']['job_id'],$agent_job_ids))
								{
									unset($nearby_jobs[$k]);
								}*/									
								$k++;
							}
							$nearby_jobs = array_values(array_filter($nearby_jobs));
							//pr($nearby_jobs);exit;
							
							
							/*$nearby_jobs = $this->get_distance($nearby_jobs,$curr_lat,$curr_lng);
							$nearby_jobs = array_values(array_filter($nearby_jobs));*/
							
							if(!empty($nearby_jobs))
							{
								$available_jobs['JobList'] = $this->list_jobs($nearby_jobs);
							}
							else
							{
								$available_jobs['JobList'] = array();
							}
						}
						else 
						{
							$available_jobs['JobList'] = array();
						}
						
					}
					else 
					{
						$available_jobs['JobList'] = array();
					}
				}
				else
				{
					$available_jobs['JobList'] = array();
				}
			}
			else {
				$available_jobs['JobList'] = array();
			}
			$message = $available_jobs;
			$i='yes';
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
	public function view_job() {
		$i = 'no';
		if($this->request->is('post') && !empty($this->request->data['current_lat']) && !empty($this->request->data['job_id']) && !empty($this->request->data['current_lng']) && !empty($this->request->data['access_code']))
		{   
			$curr_lat = $this->request->data['current_lat'];
			$curr_lng = $this->request->data['current_lng'];
			if($agent = $this->grant_access($this->request->data['access_code']))
			{
				if($job_details = $this->Job->find('first', array('fields' => array('Job.*','Coupon.*','JobCategory.job_category_title','ClientUserInfo.*'),'conditions' => array('Job.job_id'=>$this->request->data['job_id']) , 'recursive' => 0)))
				{	
					$job_id = $job_details['Job']['job_id'];
					if($job_distance = $this->ClientAddress->query("SELECT *, ( 3959 * acos( cos( radians(".$curr_lat.") ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(".$curr_lng.") ) + sin( radians(".$curr_lat.") ) * sin( radians( lattitude ) ) ) ) AS distance FROM client_address AS ClientAddress INNER JOIN job_locations AS JobLocation ON ClientAddress.client_address_id=JobLocation.client_address_id WHERE JobLocation.job_id = ". $job_id .";"))
					{
						foreach($job_distance as $key => $job_address)
						{
							$job_details['JobAddress'][$key] = $job_address['ClientAddress'];
							$job_details['JobAddress'][$key]['job_location_id'] = $job_address['JobLocation']['job_location_id'];
							$job_details['JobAddress'][$key]['job_id'] = $job_address['JobLocation']['job_id'];
							$job_details['JobAddress'][$key]['distance'] = number_format($job_address[0]['distance'],1);
						}
						
						$clientimage = $this->ClientImage->find('all',array('conditions'=>array('ClientImage.client_id' => $job_details['ClientUserInfo']['client_id'],'ClientImage.is_default'=>1),'recursive'=>-1));
						if(empty($clientimage['ClientImage']['image_file_path']))
						{	
							$job_details['ClientUserInfo']['company_cover'] = '';
						}
						else
						{
							$image = $clientimage['ClientImage']['image_file_path'];
							$img = WEBSITE_ROOT.'media/img/'.$image; 
							$job_details['ClientUserInfo']['company_cover'] = $img;
						}
						
						if(!empty($job_details))
						{
							$message = $job_details;
							$i = 'yes';
						}
						else 
						{
							$message = 'Job not found';
						}
					}
					else
					{
						$message = 'Please set a job filter first.';
					}
				}
				else
				{
					$message = 'Job not found';	
				}
			}
			else {
				$message = 'Please login Again !';
			}
		}
		else{$message = 'invalid request';}	
		
		if($i == 'no'){		
			$this->set(array(
				'Error' => array('error'=>$message),
				'_serialize' => array('Error')));}
		else{$this->set(array(
				'Success' => array('success'=>$message),
				'_serialize' => array('Success')));}
		
		
    }
	
}